#include <CH549_sdcc.h>	 //ch549


//防止头文件被重复include
#ifndef __OLED_H
#define __OLED_H	

//SSD1306的SPI接口定义
#define OLED_CS     P3_0 //片选
#define OLED_RST    P3_1 //复位
#define OLED_DC     P3_3 //数据/命令控制
#define OLED_SCL    P1_7 //D0（SCLK) 时钟 
#define OLED_SDIN   P1_5 //D1（MOSI) 数据

//定义SSD1306指令
#define u8 unsigned char 
#define u32 unsigned int 
#define OLED_CMD  0	//写命令
#define OLED_DATA 1	//写数据

//SSD1306指令集
//referenced from Adafruit SSD1306 lib
#define SSD1306_MEMORYMODE 0x20          ///< See datasheet
#define SSD1306_COLUMNADDR 0x21          ///< See datasheet
#define SSD1306_PAGEADDR 0x22            ///< See datasheet
#define SSD1306_SETCONTRAST 0x81         ///< See datasheet
#define SSD1306_CHARGEPUMP 0x8D          ///< See datasheet
#define SSD1306_SEGREMAP 0xA0            ///< See datasheet
#define SSD1306_DISPLAYALLON_RESUME 0xA4 ///< See datasheet
#define SSD1306_DISPLAYALLON 0xA5        ///< Not currently used
#define SSD1306_NORMALDISPLAY 0xA6       ///< See datasheet
#define SSD1306_INVERTDISPLAY 0xA7       ///< See datasheet
#define SSD1306_SETMULTIPLEX 0xA8        ///< See datasheet
#define SSD1306_DISPLAYOFF 0xAE          ///< See datasheet
#define SSD1306_DISPLAYON 0xAF           ///< See datasheet
#define SSD1306_COMSCANINC 0xC0          ///< Not currently used
#define SSD1306_COMSCANDEC 0xC8          ///< See datasheet
#define SSD1306_SETDISPLAYOFFSET 0xD3    ///< See datasheet
#define SSD1306_SETDISPLAYCLOCKDIV 0xD5  ///< See datasheet
#define SSD1306_SETPRECHARGE 0xD9        ///< See datasheet
#define SSD1306_SETCOMPINS 0xDA          ///< See datasheet
#define SSD1306_SETVCOMDETECT 0xDB       ///< See datasheet

#define SSD1306_SETLOWCOLUMN 0x00  ///< Not currently used
#define SSD1306_SETHIGHCOLUMN 0x10 ///< Not currently used
#define SSD1306_SETSTARTLINE 0x40  ///< See datasheet

//新增
#define SSD1306_SET_SEG_CURRENT 0xCF	// Set SEG Output Current Brightness
#define SSD1306_SET_SEG_MAP     0xA1    // Set SEG Mapping
#define SSD1306_1_64_DUTY     0x3f    // 1/64 Duty
#define SSD1306_100_DIVIDERATIO   0x80
#define SSD1306_SET_PRECHARGE   0xF1

#define SSD1306_unknown1        0x12  //待查看
#define SSD1306_0x10DISABLE   0x14





//↓ 要用到么????
#define SSD1306_EXTERNALVCC 0x01  ///< External display voltage source
#define SSD1306_SWITCHCAPVCC 0x02 ///< Gen. display voltage from 3.3V

#define SSD1306_RIGHT_HORIZONTAL_SCROLL 0x26              ///< Init rt scroll
#define SSD1306_LEFT_HORIZONTAL_SCROLL 0x27               ///< Init left scroll
#define SSD1306_VERTICAL_AND_RIGHT_HORIZONTAL_SCROLL 0x29 ///< Init diag scroll
#define SSD1306_VERTICAL_AND_LEFT_HORIZONTAL_SCROLL 0x2A  ///< Init diag scroll
#define SSD1306_DEACTIVATE_SCROLL 0x2E                    ///< Stop scroll
#define SSD1306_ACTIVATE_SCROLL 0x2F                      ///< Start scroll
#define SSD1306_SET_VERTICAL_SCROLL_AREA 0xA3             ///< Set scroll range




















#define OLED_SELECT()        OLED_CS=0   //设备选择
#define OLED_DESELECT()      OLED_CS=1   //取消设备选择

#define OLED_RST_Clr()       OLED_RST=0  //清除复位
#define OLED_RST_Set()       OLED_RST=1  //激活复位

#define OLED_MODE_COMMAND()  OLED_DC=0   //SSD1306设置为命令模式
#define OLED_MODE_DATA()     OLED_DC=1   //SSD1306设置为数据模式

#define OLED_SCLK_Clr()      OLED_SCL=0 
#define OLED_SCLK_Set()      OLED_SCL=1

#define OLED_SDIN_Clr()      OLED_SDIN=0
#define OLED_SDIN_Set()      OLED_SDIN=1;
/*
#define OLED_CS_Clr()  OLED_CS=0
#define OLED_CS_Set()  OLED_CS=1

#define OLED_RST_Clr() OLED_RST=0
#define OLED_RST_Set() OLED_RST=1

#define OLED_DC_Clr() OLED_DC=0
#define OLED_DC_Set() (OLED_DC=1)

#define OLED_SCLK_Clr() OLED_SCL=0
#define OLED_SCLK_Set() OLED_SCL=1

#define OLED_SDIN_Clr() OLED_SDIN=0
#define OLED_SDIN_Set() OLED_SDIN=1;
*/


//定义屏幕尺寸
#define SSD1306_LCDWIDTH 128 
#define SSD1306_LCDHEIGHT 64 

//没搞明白的定义
//#define SIZE 16
#define Max_Column	128
#define Max_Row		64
void load_one_command(u8 c);
void load_commandList(const u8 *c, u8 n);
void OLED_DrawBMP(unsigned char x0, unsigned char y0,unsigned char x1, unsigned char y1,unsigned char BMP[]);
void OLED_ShowChar(u8 x,u8 y,u8 chr);

//函数定义
void delay_ms(unsigned int ms);                         //延迟函数

void OLED_Init(void);                                   //OLED初始化
void OLED_WR_Byte(u8 dat,u8 cmd);                       //写入1byte数据
void OLED_Set_Pos(unsigned char x, unsigned char y);    //设置显示位置

void OLED_Display_On(void);                 //显示
void OLED_Display_Off(void);                //关闭显示
void OLED_Clear(void);                      //清屏函数
void OLED_ShowCHinese(u8 x,u8 y,u8 no);     //显示汉字
void OLED_ShowString(u8 x,u8 y, u8 *p);	    //显示字符串
void ssd1306_command(u8 c);



void setFontSize(u8 size);


//drawChar
#endif 