                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 4.0.0 #11528 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module main
                                      6 	.optsdcc -mmcs51 --model-large
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _main
                                     12 	.globl _FaceTrack
                                     13 	.globl _LightTurn
                                     14 	.globl _decode_XenP201S
                                     15 	.globl _UART1Init
                                     16 	.globl _GPIO_Init
                                     17 	.globl _SPIMasterModeSet
                                     18 	.globl _CH549UART1RcvByte
                                     19 	.globl _mDelaymS
                                     20 	.globl _mDelayuS
                                     21 	.globl _CfgFsys
                                     22 	.globl _printf_fast_f
                                     23 	.globl _setFontSize
                                     24 	.globl _OLED_Clear
                                     25 	.globl _OLED_Init
                                     26 	.globl _OLED_ShowChar
                                     27 	.globl _UIF_BUS_RST
                                     28 	.globl _UIF_DETECT
                                     29 	.globl _UIF_TRANSFER
                                     30 	.globl _UIF_SUSPEND
                                     31 	.globl _UIF_HST_SOF
                                     32 	.globl _UIF_FIFO_OV
                                     33 	.globl _U_SIE_FREE
                                     34 	.globl _U_TOG_OK
                                     35 	.globl _U_IS_NAK
                                     36 	.globl _S0_R_FIFO
                                     37 	.globl _S0_T_FIFO
                                     38 	.globl _S0_FREE
                                     39 	.globl _S0_IF_BYTE
                                     40 	.globl _S0_IF_FIRST
                                     41 	.globl _S0_IF_OV
                                     42 	.globl _S0_FST_ACT
                                     43 	.globl _CP_RL2
                                     44 	.globl _C_T2
                                     45 	.globl _TR2
                                     46 	.globl _EXEN2
                                     47 	.globl _TCLK
                                     48 	.globl _RCLK
                                     49 	.globl _EXF2
                                     50 	.globl _CAP1F
                                     51 	.globl _TF2
                                     52 	.globl _RI
                                     53 	.globl _TI
                                     54 	.globl _RB8
                                     55 	.globl _TB8
                                     56 	.globl _REN
                                     57 	.globl _SM2
                                     58 	.globl _SM1
                                     59 	.globl _SM0
                                     60 	.globl _IT0
                                     61 	.globl _IE0
                                     62 	.globl _IT1
                                     63 	.globl _IE1
                                     64 	.globl _TR0
                                     65 	.globl _TF0
                                     66 	.globl _TR1
                                     67 	.globl _TF1
                                     68 	.globl _XI
                                     69 	.globl _XO
                                     70 	.globl _P4_0
                                     71 	.globl _P4_1
                                     72 	.globl _P4_2
                                     73 	.globl _P4_3
                                     74 	.globl _P4_4
                                     75 	.globl _P4_5
                                     76 	.globl _P4_6
                                     77 	.globl _RXD
                                     78 	.globl _TXD
                                     79 	.globl _INT0
                                     80 	.globl _INT1
                                     81 	.globl _T0
                                     82 	.globl _T1
                                     83 	.globl _CAP0
                                     84 	.globl _INT3
                                     85 	.globl _P3_0
                                     86 	.globl _P3_1
                                     87 	.globl _P3_2
                                     88 	.globl _P3_3
                                     89 	.globl _P3_4
                                     90 	.globl _P3_5
                                     91 	.globl _P3_6
                                     92 	.globl _P3_7
                                     93 	.globl _PWM5
                                     94 	.globl _PWM4
                                     95 	.globl _INT0_
                                     96 	.globl _PWM3
                                     97 	.globl _PWM2
                                     98 	.globl _CAP1_
                                     99 	.globl _T2_
                                    100 	.globl _PWM1
                                    101 	.globl _CAP2_
                                    102 	.globl _T2EX_
                                    103 	.globl _PWM0
                                    104 	.globl _RXD1
                                    105 	.globl _PWM6
                                    106 	.globl _TXD1
                                    107 	.globl _PWM7
                                    108 	.globl _P2_0
                                    109 	.globl _P2_1
                                    110 	.globl _P2_2
                                    111 	.globl _P2_3
                                    112 	.globl _P2_4
                                    113 	.globl _P2_5
                                    114 	.globl _P2_6
                                    115 	.globl _P2_7
                                    116 	.globl _AIN0
                                    117 	.globl _CAP1
                                    118 	.globl _T2
                                    119 	.globl _AIN1
                                    120 	.globl _CAP2
                                    121 	.globl _T2EX
                                    122 	.globl _AIN2
                                    123 	.globl _AIN3
                                    124 	.globl _AIN4
                                    125 	.globl _UCC1
                                    126 	.globl _SCS
                                    127 	.globl _AIN5
                                    128 	.globl _UCC2
                                    129 	.globl _PWM0_
                                    130 	.globl _MOSI
                                    131 	.globl _AIN6
                                    132 	.globl _VBUS
                                    133 	.globl _RXD1_
                                    134 	.globl _MISO
                                    135 	.globl _AIN7
                                    136 	.globl _TXD1_
                                    137 	.globl _SCK
                                    138 	.globl _P1_0
                                    139 	.globl _P1_1
                                    140 	.globl _P1_2
                                    141 	.globl _P1_3
                                    142 	.globl _P1_4
                                    143 	.globl _P1_5
                                    144 	.globl _P1_6
                                    145 	.globl _P1_7
                                    146 	.globl _AIN8
                                    147 	.globl _AIN9
                                    148 	.globl _AIN10
                                    149 	.globl _RXD_
                                    150 	.globl _AIN11
                                    151 	.globl _TXD_
                                    152 	.globl _AIN12
                                    153 	.globl _RXD2
                                    154 	.globl _AIN13
                                    155 	.globl _TXD2
                                    156 	.globl _AIN14
                                    157 	.globl _RXD3
                                    158 	.globl _AIN15
                                    159 	.globl _TXD3
                                    160 	.globl _P0_0
                                    161 	.globl _P0_1
                                    162 	.globl _P0_2
                                    163 	.globl _P0_3
                                    164 	.globl _P0_4
                                    165 	.globl _P0_5
                                    166 	.globl _P0_6
                                    167 	.globl _P0_7
                                    168 	.globl _IE_SPI0
                                    169 	.globl _IE_INT3
                                    170 	.globl _IE_USB
                                    171 	.globl _IE_UART2
                                    172 	.globl _IE_ADC
                                    173 	.globl _IE_UART1
                                    174 	.globl _IE_UART3
                                    175 	.globl _IE_PWMX
                                    176 	.globl _IE_GPIO
                                    177 	.globl _IE_WDOG
                                    178 	.globl _PX0
                                    179 	.globl _PT0
                                    180 	.globl _PX1
                                    181 	.globl _PT1
                                    182 	.globl _PS
                                    183 	.globl _PT2
                                    184 	.globl _PL_FLAG
                                    185 	.globl _PH_FLAG
                                    186 	.globl _EX0
                                    187 	.globl _ET0
                                    188 	.globl _EX1
                                    189 	.globl _ET1
                                    190 	.globl _ES
                                    191 	.globl _ET2
                                    192 	.globl _E_DIS
                                    193 	.globl _EA
                                    194 	.globl _P
                                    195 	.globl _F1
                                    196 	.globl _OV
                                    197 	.globl _RS0
                                    198 	.globl _RS1
                                    199 	.globl _F0
                                    200 	.globl _AC
                                    201 	.globl _CY
                                    202 	.globl _UEP1_DMA_H
                                    203 	.globl _UEP1_DMA_L
                                    204 	.globl _UEP1_DMA
                                    205 	.globl _UEP0_DMA_H
                                    206 	.globl _UEP0_DMA_L
                                    207 	.globl _UEP0_DMA
                                    208 	.globl _UEP2_3_MOD
                                    209 	.globl _UEP4_1_MOD
                                    210 	.globl _UEP3_DMA_H
                                    211 	.globl _UEP3_DMA_L
                                    212 	.globl _UEP3_DMA
                                    213 	.globl _UEP2_DMA_H
                                    214 	.globl _UEP2_DMA_L
                                    215 	.globl _UEP2_DMA
                                    216 	.globl _USB_DEV_AD
                                    217 	.globl _USB_CTRL
                                    218 	.globl _USB_INT_EN
                                    219 	.globl _UEP4_T_LEN
                                    220 	.globl _UEP4_CTRL
                                    221 	.globl _UEP0_T_LEN
                                    222 	.globl _UEP0_CTRL
                                    223 	.globl _USB_RX_LEN
                                    224 	.globl _USB_MIS_ST
                                    225 	.globl _USB_INT_ST
                                    226 	.globl _USB_INT_FG
                                    227 	.globl _UEP3_T_LEN
                                    228 	.globl _UEP3_CTRL
                                    229 	.globl _UEP2_T_LEN
                                    230 	.globl _UEP2_CTRL
                                    231 	.globl _UEP1_T_LEN
                                    232 	.globl _UEP1_CTRL
                                    233 	.globl _UDEV_CTRL
                                    234 	.globl _USB_C_CTRL
                                    235 	.globl _ADC_PIN
                                    236 	.globl _ADC_CHAN
                                    237 	.globl _ADC_DAT_H
                                    238 	.globl _ADC_DAT_L
                                    239 	.globl _ADC_DAT
                                    240 	.globl _ADC_CFG
                                    241 	.globl _ADC_CTRL
                                    242 	.globl _TKEY_CTRL
                                    243 	.globl _SIF3
                                    244 	.globl _SBAUD3
                                    245 	.globl _SBUF3
                                    246 	.globl _SCON3
                                    247 	.globl _SIF2
                                    248 	.globl _SBAUD2
                                    249 	.globl _SBUF2
                                    250 	.globl _SCON2
                                    251 	.globl _SIF1
                                    252 	.globl _SBAUD1
                                    253 	.globl _SBUF1
                                    254 	.globl _SCON1
                                    255 	.globl _SPI0_SETUP
                                    256 	.globl _SPI0_CK_SE
                                    257 	.globl _SPI0_CTRL
                                    258 	.globl _SPI0_DATA
                                    259 	.globl _SPI0_STAT
                                    260 	.globl _PWM_DATA7
                                    261 	.globl _PWM_DATA6
                                    262 	.globl _PWM_DATA5
                                    263 	.globl _PWM_DATA4
                                    264 	.globl _PWM_DATA3
                                    265 	.globl _PWM_CTRL2
                                    266 	.globl _PWM_CK_SE
                                    267 	.globl _PWM_CTRL
                                    268 	.globl _PWM_DATA0
                                    269 	.globl _PWM_DATA1
                                    270 	.globl _PWM_DATA2
                                    271 	.globl _T2CAP1H
                                    272 	.globl _T2CAP1L
                                    273 	.globl _T2CAP1
                                    274 	.globl _TH2
                                    275 	.globl _TL2
                                    276 	.globl _T2COUNT
                                    277 	.globl _RCAP2H
                                    278 	.globl _RCAP2L
                                    279 	.globl _RCAP2
                                    280 	.globl _T2MOD
                                    281 	.globl _T2CON
                                    282 	.globl _T2CAP0H
                                    283 	.globl _T2CAP0L
                                    284 	.globl _T2CAP0
                                    285 	.globl _T2CON2
                                    286 	.globl _SBUF
                                    287 	.globl _SCON
                                    288 	.globl _TH1
                                    289 	.globl _TH0
                                    290 	.globl _TL1
                                    291 	.globl _TL0
                                    292 	.globl _TMOD
                                    293 	.globl _TCON
                                    294 	.globl _XBUS_AUX
                                    295 	.globl _PIN_FUNC
                                    296 	.globl _P5
                                    297 	.globl _P4_DIR_PU
                                    298 	.globl _P4_MOD_OC
                                    299 	.globl _P4
                                    300 	.globl _P3_DIR_PU
                                    301 	.globl _P3_MOD_OC
                                    302 	.globl _P3
                                    303 	.globl _P2_DIR_PU
                                    304 	.globl _P2_MOD_OC
                                    305 	.globl _P2
                                    306 	.globl _P1_DIR_PU
                                    307 	.globl _P1_MOD_OC
                                    308 	.globl _P1
                                    309 	.globl _P0_DIR_PU
                                    310 	.globl _P0_MOD_OC
                                    311 	.globl _P0
                                    312 	.globl _ROM_CTRL
                                    313 	.globl _ROM_DATA_HH
                                    314 	.globl _ROM_DATA_HL
                                    315 	.globl _ROM_DATA_HI
                                    316 	.globl _ROM_ADDR_H
                                    317 	.globl _ROM_ADDR_L
                                    318 	.globl _ROM_ADDR
                                    319 	.globl _GPIO_IE
                                    320 	.globl _INTX
                                    321 	.globl _IP_EX
                                    322 	.globl _IE_EX
                                    323 	.globl _IP
                                    324 	.globl _IE
                                    325 	.globl _WDOG_COUNT
                                    326 	.globl _RESET_KEEP
                                    327 	.globl _WAKE_CTRL
                                    328 	.globl _CLOCK_CFG
                                    329 	.globl _POWER_CFG
                                    330 	.globl _PCON
                                    331 	.globl _GLOBAL_CFG
                                    332 	.globl _SAFE_MOD
                                    333 	.globl _DPH
                                    334 	.globl _DPL
                                    335 	.globl _SP
                                    336 	.globl _A_INV
                                    337 	.globl _B
                                    338 	.globl _ACC
                                    339 	.globl _PSW
                                    340 	.globl _yPos
                                    341 	.globl _xPos
                                    342 	.globl _fRecv
                                    343 	.globl _pBuf
                                    344 	.globl _recvBuf
                                    345 	.globl _oled_row
                                    346 	.globl _oled_colum
                                    347 	.globl _putchar
                                    348 	.globl _setCursor
                                    349 	.globl _UART1Interrupt
                                    350 ;--------------------------------------------------------
                                    351 ; special function registers
                                    352 ;--------------------------------------------------------
                                    353 	.area RSEG    (ABS,DATA)
      000000                        354 	.org 0x0000
                           0000D0   355 _PSW	=	0x00d0
                           0000E0   356 _ACC	=	0x00e0
                           0000F0   357 _B	=	0x00f0
                           0000FD   358 _A_INV	=	0x00fd
                           000081   359 _SP	=	0x0081
                           000082   360 _DPL	=	0x0082
                           000083   361 _DPH	=	0x0083
                           0000A1   362 _SAFE_MOD	=	0x00a1
                           0000B1   363 _GLOBAL_CFG	=	0x00b1
                           000087   364 _PCON	=	0x0087
                           0000BA   365 _POWER_CFG	=	0x00ba
                           0000B9   366 _CLOCK_CFG	=	0x00b9
                           0000A9   367 _WAKE_CTRL	=	0x00a9
                           0000FE   368 _RESET_KEEP	=	0x00fe
                           0000FF   369 _WDOG_COUNT	=	0x00ff
                           0000A8   370 _IE	=	0x00a8
                           0000B8   371 _IP	=	0x00b8
                           0000E8   372 _IE_EX	=	0x00e8
                           0000E9   373 _IP_EX	=	0x00e9
                           0000B3   374 _INTX	=	0x00b3
                           0000B2   375 _GPIO_IE	=	0x00b2
                           008584   376 _ROM_ADDR	=	0x8584
                           000084   377 _ROM_ADDR_L	=	0x0084
                           000085   378 _ROM_ADDR_H	=	0x0085
                           008F8E   379 _ROM_DATA_HI	=	0x8f8e
                           00008E   380 _ROM_DATA_HL	=	0x008e
                           00008F   381 _ROM_DATA_HH	=	0x008f
                           000086   382 _ROM_CTRL	=	0x0086
                           000080   383 _P0	=	0x0080
                           0000C4   384 _P0_MOD_OC	=	0x00c4
                           0000C5   385 _P0_DIR_PU	=	0x00c5
                           000090   386 _P1	=	0x0090
                           000092   387 _P1_MOD_OC	=	0x0092
                           000093   388 _P1_DIR_PU	=	0x0093
                           0000A0   389 _P2	=	0x00a0
                           000094   390 _P2_MOD_OC	=	0x0094
                           000095   391 _P2_DIR_PU	=	0x0095
                           0000B0   392 _P3	=	0x00b0
                           000096   393 _P3_MOD_OC	=	0x0096
                           000097   394 _P3_DIR_PU	=	0x0097
                           0000C0   395 _P4	=	0x00c0
                           0000C2   396 _P4_MOD_OC	=	0x00c2
                           0000C3   397 _P4_DIR_PU	=	0x00c3
                           0000AB   398 _P5	=	0x00ab
                           0000AA   399 _PIN_FUNC	=	0x00aa
                           0000A2   400 _XBUS_AUX	=	0x00a2
                           000088   401 _TCON	=	0x0088
                           000089   402 _TMOD	=	0x0089
                           00008A   403 _TL0	=	0x008a
                           00008B   404 _TL1	=	0x008b
                           00008C   405 _TH0	=	0x008c
                           00008D   406 _TH1	=	0x008d
                           000098   407 _SCON	=	0x0098
                           000099   408 _SBUF	=	0x0099
                           0000C1   409 _T2CON2	=	0x00c1
                           00C7C6   410 _T2CAP0	=	0xc7c6
                           0000C6   411 _T2CAP0L	=	0x00c6
                           0000C7   412 _T2CAP0H	=	0x00c7
                           0000C8   413 _T2CON	=	0x00c8
                           0000C9   414 _T2MOD	=	0x00c9
                           00CBCA   415 _RCAP2	=	0xcbca
                           0000CA   416 _RCAP2L	=	0x00ca
                           0000CB   417 _RCAP2H	=	0x00cb
                           00CDCC   418 _T2COUNT	=	0xcdcc
                           0000CC   419 _TL2	=	0x00cc
                           0000CD   420 _TH2	=	0x00cd
                           00CFCE   421 _T2CAP1	=	0xcfce
                           0000CE   422 _T2CAP1L	=	0x00ce
                           0000CF   423 _T2CAP1H	=	0x00cf
                           00009A   424 _PWM_DATA2	=	0x009a
                           00009B   425 _PWM_DATA1	=	0x009b
                           00009C   426 _PWM_DATA0	=	0x009c
                           00009D   427 _PWM_CTRL	=	0x009d
                           00009E   428 _PWM_CK_SE	=	0x009e
                           00009F   429 _PWM_CTRL2	=	0x009f
                           0000A3   430 _PWM_DATA3	=	0x00a3
                           0000A4   431 _PWM_DATA4	=	0x00a4
                           0000A5   432 _PWM_DATA5	=	0x00a5
                           0000A6   433 _PWM_DATA6	=	0x00a6
                           0000A7   434 _PWM_DATA7	=	0x00a7
                           0000F8   435 _SPI0_STAT	=	0x00f8
                           0000F9   436 _SPI0_DATA	=	0x00f9
                           0000FA   437 _SPI0_CTRL	=	0x00fa
                           0000FB   438 _SPI0_CK_SE	=	0x00fb
                           0000FC   439 _SPI0_SETUP	=	0x00fc
                           0000BC   440 _SCON1	=	0x00bc
                           0000BD   441 _SBUF1	=	0x00bd
                           0000BE   442 _SBAUD1	=	0x00be
                           0000BF   443 _SIF1	=	0x00bf
                           0000B4   444 _SCON2	=	0x00b4
                           0000B5   445 _SBUF2	=	0x00b5
                           0000B6   446 _SBAUD2	=	0x00b6
                           0000B7   447 _SIF2	=	0x00b7
                           0000AC   448 _SCON3	=	0x00ac
                           0000AD   449 _SBUF3	=	0x00ad
                           0000AE   450 _SBAUD3	=	0x00ae
                           0000AF   451 _SIF3	=	0x00af
                           0000F1   452 _TKEY_CTRL	=	0x00f1
                           0000F2   453 _ADC_CTRL	=	0x00f2
                           0000F3   454 _ADC_CFG	=	0x00f3
                           00F5F4   455 _ADC_DAT	=	0xf5f4
                           0000F4   456 _ADC_DAT_L	=	0x00f4
                           0000F5   457 _ADC_DAT_H	=	0x00f5
                           0000F6   458 _ADC_CHAN	=	0x00f6
                           0000F7   459 _ADC_PIN	=	0x00f7
                           000091   460 _USB_C_CTRL	=	0x0091
                           0000D1   461 _UDEV_CTRL	=	0x00d1
                           0000D2   462 _UEP1_CTRL	=	0x00d2
                           0000D3   463 _UEP1_T_LEN	=	0x00d3
                           0000D4   464 _UEP2_CTRL	=	0x00d4
                           0000D5   465 _UEP2_T_LEN	=	0x00d5
                           0000D6   466 _UEP3_CTRL	=	0x00d6
                           0000D7   467 _UEP3_T_LEN	=	0x00d7
                           0000D8   468 _USB_INT_FG	=	0x00d8
                           0000D9   469 _USB_INT_ST	=	0x00d9
                           0000DA   470 _USB_MIS_ST	=	0x00da
                           0000DB   471 _USB_RX_LEN	=	0x00db
                           0000DC   472 _UEP0_CTRL	=	0x00dc
                           0000DD   473 _UEP0_T_LEN	=	0x00dd
                           0000DE   474 _UEP4_CTRL	=	0x00de
                           0000DF   475 _UEP4_T_LEN	=	0x00df
                           0000E1   476 _USB_INT_EN	=	0x00e1
                           0000E2   477 _USB_CTRL	=	0x00e2
                           0000E3   478 _USB_DEV_AD	=	0x00e3
                           00E5E4   479 _UEP2_DMA	=	0xe5e4
                           0000E4   480 _UEP2_DMA_L	=	0x00e4
                           0000E5   481 _UEP2_DMA_H	=	0x00e5
                           00E7E6   482 _UEP3_DMA	=	0xe7e6
                           0000E6   483 _UEP3_DMA_L	=	0x00e6
                           0000E7   484 _UEP3_DMA_H	=	0x00e7
                           0000EA   485 _UEP4_1_MOD	=	0x00ea
                           0000EB   486 _UEP2_3_MOD	=	0x00eb
                           00EDEC   487 _UEP0_DMA	=	0xedec
                           0000EC   488 _UEP0_DMA_L	=	0x00ec
                           0000ED   489 _UEP0_DMA_H	=	0x00ed
                           00EFEE   490 _UEP1_DMA	=	0xefee
                           0000EE   491 _UEP1_DMA_L	=	0x00ee
                           0000EF   492 _UEP1_DMA_H	=	0x00ef
                                    493 ;--------------------------------------------------------
                                    494 ; special function bits
                                    495 ;--------------------------------------------------------
                                    496 	.area RSEG    (ABS,DATA)
      000000                        497 	.org 0x0000
                           0000D7   498 _CY	=	0x00d7
                           0000D6   499 _AC	=	0x00d6
                           0000D5   500 _F0	=	0x00d5
                           0000D4   501 _RS1	=	0x00d4
                           0000D3   502 _RS0	=	0x00d3
                           0000D2   503 _OV	=	0x00d2
                           0000D1   504 _F1	=	0x00d1
                           0000D0   505 _P	=	0x00d0
                           0000AF   506 _EA	=	0x00af
                           0000AE   507 _E_DIS	=	0x00ae
                           0000AD   508 _ET2	=	0x00ad
                           0000AC   509 _ES	=	0x00ac
                           0000AB   510 _ET1	=	0x00ab
                           0000AA   511 _EX1	=	0x00aa
                           0000A9   512 _ET0	=	0x00a9
                           0000A8   513 _EX0	=	0x00a8
                           0000BF   514 _PH_FLAG	=	0x00bf
                           0000BE   515 _PL_FLAG	=	0x00be
                           0000BD   516 _PT2	=	0x00bd
                           0000BC   517 _PS	=	0x00bc
                           0000BB   518 _PT1	=	0x00bb
                           0000BA   519 _PX1	=	0x00ba
                           0000B9   520 _PT0	=	0x00b9
                           0000B8   521 _PX0	=	0x00b8
                           0000EF   522 _IE_WDOG	=	0x00ef
                           0000EE   523 _IE_GPIO	=	0x00ee
                           0000ED   524 _IE_PWMX	=	0x00ed
                           0000ED   525 _IE_UART3	=	0x00ed
                           0000EC   526 _IE_UART1	=	0x00ec
                           0000EB   527 _IE_ADC	=	0x00eb
                           0000EB   528 _IE_UART2	=	0x00eb
                           0000EA   529 _IE_USB	=	0x00ea
                           0000E9   530 _IE_INT3	=	0x00e9
                           0000E8   531 _IE_SPI0	=	0x00e8
                           000087   532 _P0_7	=	0x0087
                           000086   533 _P0_6	=	0x0086
                           000085   534 _P0_5	=	0x0085
                           000084   535 _P0_4	=	0x0084
                           000083   536 _P0_3	=	0x0083
                           000082   537 _P0_2	=	0x0082
                           000081   538 _P0_1	=	0x0081
                           000080   539 _P0_0	=	0x0080
                           000087   540 _TXD3	=	0x0087
                           000087   541 _AIN15	=	0x0087
                           000086   542 _RXD3	=	0x0086
                           000086   543 _AIN14	=	0x0086
                           000085   544 _TXD2	=	0x0085
                           000085   545 _AIN13	=	0x0085
                           000084   546 _RXD2	=	0x0084
                           000084   547 _AIN12	=	0x0084
                           000083   548 _TXD_	=	0x0083
                           000083   549 _AIN11	=	0x0083
                           000082   550 _RXD_	=	0x0082
                           000082   551 _AIN10	=	0x0082
                           000081   552 _AIN9	=	0x0081
                           000080   553 _AIN8	=	0x0080
                           000097   554 _P1_7	=	0x0097
                           000096   555 _P1_6	=	0x0096
                           000095   556 _P1_5	=	0x0095
                           000094   557 _P1_4	=	0x0094
                           000093   558 _P1_3	=	0x0093
                           000092   559 _P1_2	=	0x0092
                           000091   560 _P1_1	=	0x0091
                           000090   561 _P1_0	=	0x0090
                           000097   562 _SCK	=	0x0097
                           000097   563 _TXD1_	=	0x0097
                           000097   564 _AIN7	=	0x0097
                           000096   565 _MISO	=	0x0096
                           000096   566 _RXD1_	=	0x0096
                           000096   567 _VBUS	=	0x0096
                           000096   568 _AIN6	=	0x0096
                           000095   569 _MOSI	=	0x0095
                           000095   570 _PWM0_	=	0x0095
                           000095   571 _UCC2	=	0x0095
                           000095   572 _AIN5	=	0x0095
                           000094   573 _SCS	=	0x0094
                           000094   574 _UCC1	=	0x0094
                           000094   575 _AIN4	=	0x0094
                           000093   576 _AIN3	=	0x0093
                           000092   577 _AIN2	=	0x0092
                           000091   578 _T2EX	=	0x0091
                           000091   579 _CAP2	=	0x0091
                           000091   580 _AIN1	=	0x0091
                           000090   581 _T2	=	0x0090
                           000090   582 _CAP1	=	0x0090
                           000090   583 _AIN0	=	0x0090
                           0000A7   584 _P2_7	=	0x00a7
                           0000A6   585 _P2_6	=	0x00a6
                           0000A5   586 _P2_5	=	0x00a5
                           0000A4   587 _P2_4	=	0x00a4
                           0000A3   588 _P2_3	=	0x00a3
                           0000A2   589 _P2_2	=	0x00a2
                           0000A1   590 _P2_1	=	0x00a1
                           0000A0   591 _P2_0	=	0x00a0
                           0000A7   592 _PWM7	=	0x00a7
                           0000A7   593 _TXD1	=	0x00a7
                           0000A6   594 _PWM6	=	0x00a6
                           0000A6   595 _RXD1	=	0x00a6
                           0000A5   596 _PWM0	=	0x00a5
                           0000A5   597 _T2EX_	=	0x00a5
                           0000A5   598 _CAP2_	=	0x00a5
                           0000A4   599 _PWM1	=	0x00a4
                           0000A4   600 _T2_	=	0x00a4
                           0000A4   601 _CAP1_	=	0x00a4
                           0000A3   602 _PWM2	=	0x00a3
                           0000A2   603 _PWM3	=	0x00a2
                           0000A2   604 _INT0_	=	0x00a2
                           0000A1   605 _PWM4	=	0x00a1
                           0000A0   606 _PWM5	=	0x00a0
                           0000B7   607 _P3_7	=	0x00b7
                           0000B6   608 _P3_6	=	0x00b6
                           0000B5   609 _P3_5	=	0x00b5
                           0000B4   610 _P3_4	=	0x00b4
                           0000B3   611 _P3_3	=	0x00b3
                           0000B2   612 _P3_2	=	0x00b2
                           0000B1   613 _P3_1	=	0x00b1
                           0000B0   614 _P3_0	=	0x00b0
                           0000B7   615 _INT3	=	0x00b7
                           0000B6   616 _CAP0	=	0x00b6
                           0000B5   617 _T1	=	0x00b5
                           0000B4   618 _T0	=	0x00b4
                           0000B3   619 _INT1	=	0x00b3
                           0000B2   620 _INT0	=	0x00b2
                           0000B1   621 _TXD	=	0x00b1
                           0000B0   622 _RXD	=	0x00b0
                           0000C6   623 _P4_6	=	0x00c6
                           0000C5   624 _P4_5	=	0x00c5
                           0000C4   625 _P4_4	=	0x00c4
                           0000C3   626 _P4_3	=	0x00c3
                           0000C2   627 _P4_2	=	0x00c2
                           0000C1   628 _P4_1	=	0x00c1
                           0000C0   629 _P4_0	=	0x00c0
                           0000C7   630 _XO	=	0x00c7
                           0000C6   631 _XI	=	0x00c6
                           00008F   632 _TF1	=	0x008f
                           00008E   633 _TR1	=	0x008e
                           00008D   634 _TF0	=	0x008d
                           00008C   635 _TR0	=	0x008c
                           00008B   636 _IE1	=	0x008b
                           00008A   637 _IT1	=	0x008a
                           000089   638 _IE0	=	0x0089
                           000088   639 _IT0	=	0x0088
                           00009F   640 _SM0	=	0x009f
                           00009E   641 _SM1	=	0x009e
                           00009D   642 _SM2	=	0x009d
                           00009C   643 _REN	=	0x009c
                           00009B   644 _TB8	=	0x009b
                           00009A   645 _RB8	=	0x009a
                           000099   646 _TI	=	0x0099
                           000098   647 _RI	=	0x0098
                           0000CF   648 _TF2	=	0x00cf
                           0000CF   649 _CAP1F	=	0x00cf
                           0000CE   650 _EXF2	=	0x00ce
                           0000CD   651 _RCLK	=	0x00cd
                           0000CC   652 _TCLK	=	0x00cc
                           0000CB   653 _EXEN2	=	0x00cb
                           0000CA   654 _TR2	=	0x00ca
                           0000C9   655 _C_T2	=	0x00c9
                           0000C8   656 _CP_RL2	=	0x00c8
                           0000FF   657 _S0_FST_ACT	=	0x00ff
                           0000FE   658 _S0_IF_OV	=	0x00fe
                           0000FD   659 _S0_IF_FIRST	=	0x00fd
                           0000FC   660 _S0_IF_BYTE	=	0x00fc
                           0000FB   661 _S0_FREE	=	0x00fb
                           0000FA   662 _S0_T_FIFO	=	0x00fa
                           0000F8   663 _S0_R_FIFO	=	0x00f8
                           0000DF   664 _U_IS_NAK	=	0x00df
                           0000DE   665 _U_TOG_OK	=	0x00de
                           0000DD   666 _U_SIE_FREE	=	0x00dd
                           0000DC   667 _UIF_FIFO_OV	=	0x00dc
                           0000DB   668 _UIF_HST_SOF	=	0x00db
                           0000DA   669 _UIF_SUSPEND	=	0x00da
                           0000D9   670 _UIF_TRANSFER	=	0x00d9
                           0000D8   671 _UIF_DETECT	=	0x00d8
                           0000D8   672 _UIF_BUS_RST	=	0x00d8
                                    673 ;--------------------------------------------------------
                                    674 ; overlayable register banks
                                    675 ;--------------------------------------------------------
                                    676 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        677 	.ds 8
                                    678 	.area REG_BANK_1	(REL,OVR,DATA)
      000008                        679 	.ds 8
                                    680 ;--------------------------------------------------------
                                    681 ; overlayable bit register bank
                                    682 ;--------------------------------------------------------
                                    683 	.area BIT_BANK	(REL,OVR,DATA)
      000021                        684 bits:
      000021                        685 	.ds 1
                           008000   686 	b0 = bits[0]
                           008100   687 	b1 = bits[1]
                           008200   688 	b2 = bits[2]
                           008300   689 	b3 = bits[3]
                           008400   690 	b4 = bits[4]
                           008500   691 	b5 = bits[5]
                           008600   692 	b6 = bits[6]
                           008700   693 	b7 = bits[7]
                                    694 ;--------------------------------------------------------
                                    695 ; internal ram data
                                    696 ;--------------------------------------------------------
                                    697 	.area DSEG    (DATA)
                                    698 ;--------------------------------------------------------
                                    699 ; overlayable items in internal ram 
                                    700 ;--------------------------------------------------------
                                    701 ;--------------------------------------------------------
                                    702 ; Stack segment in internal ram 
                                    703 ;--------------------------------------------------------
                                    704 	.area	SSEG
      000022                        705 __start__stack:
      000022                        706 	.ds	1
                                    707 
                                    708 ;--------------------------------------------------------
                                    709 ; indirectly addressable internal ram data
                                    710 ;--------------------------------------------------------
                                    711 	.area ISEG    (DATA)
                                    712 ;--------------------------------------------------------
                                    713 ; absolute internal ram data
                                    714 ;--------------------------------------------------------
                                    715 	.area IABS    (ABS,DATA)
                                    716 	.area IABS    (ABS,DATA)
                                    717 ;--------------------------------------------------------
                                    718 ; bit data
                                    719 ;--------------------------------------------------------
                                    720 	.area BSEG    (BIT)
                                    721 ;--------------------------------------------------------
                                    722 ; paged external ram data
                                    723 ;--------------------------------------------------------
                                    724 	.area PSEG    (PAG,XDATA)
                                    725 ;--------------------------------------------------------
                                    726 ; external ram data
                                    727 ;--------------------------------------------------------
                                    728 	.area XSEG    (XDATA)
      000001                        729 _oled_colum::
      000001                        730 	.ds 2
      000003                        731 _oled_row::
      000003                        732 	.ds 2
      000005                        733 _recvBuf::
      000005                        734 	.ds 30
      000023                        735 _pBuf::
      000023                        736 	.ds 1
      000024                        737 _fRecv::
      000024                        738 	.ds 1
      000025                        739 _xPos::
      000025                        740 	.ds 2
      000027                        741 _yPos::
      000027                        742 	.ds 2
                                    743 ;--------------------------------------------------------
                                    744 ; absolute external ram data
                                    745 ;--------------------------------------------------------
                                    746 	.area XABS    (ABS,XDATA)
                                    747 ;--------------------------------------------------------
                                    748 ; external initialized ram data
                                    749 ;--------------------------------------------------------
                                    750 	.area HOME    (CODE)
                                    751 	.area GSINIT0 (CODE)
                                    752 	.area GSINIT1 (CODE)
                                    753 	.area GSINIT2 (CODE)
                                    754 	.area GSINIT3 (CODE)
                                    755 	.area GSINIT4 (CODE)
                                    756 	.area GSINIT5 (CODE)
                                    757 	.area GSINIT  (CODE)
                                    758 	.area GSFINAL (CODE)
                                    759 	.area CSEG    (CODE)
                                    760 ;--------------------------------------------------------
                                    761 ; interrupt vector 
                                    762 ;--------------------------------------------------------
                                    763 	.area HOME    (CODE)
      000000                        764 __interrupt_vect:
      000000 02 00 61         [24]  765 	ljmp	__sdcc_gsinit_startup
      000003 32               [24]  766 	reti
      000004                        767 	.ds	7
      00000B 32               [24]  768 	reti
      00000C                        769 	.ds	7
      000013 32               [24]  770 	reti
      000014                        771 	.ds	7
      00001B 32               [24]  772 	reti
      00001C                        773 	.ds	7
      000023 32               [24]  774 	reti
      000024                        775 	.ds	7
      00002B 32               [24]  776 	reti
      00002C                        777 	.ds	7
      000033 32               [24]  778 	reti
      000034                        779 	.ds	7
      00003B 32               [24]  780 	reti
      00003C                        781 	.ds	7
      000043 32               [24]  782 	reti
      000044                        783 	.ds	7
      00004B 02 0E 94         [24]  784 	ljmp	_UART2Interrupt
      00004E                        785 	.ds	5
      000053 02 0C 24         [24]  786 	ljmp	_UART1Interrupt
      000056                        787 	.ds	5
      00005B 02 0F 14         [24]  788 	ljmp	_UART3Interrupt
                                    789 ;--------------------------------------------------------
                                    790 ; global & static initialisations
                                    791 ;--------------------------------------------------------
                                    792 	.area HOME    (CODE)
                                    793 	.area GSINIT  (CODE)
                                    794 	.area GSFINAL (CODE)
                                    795 	.area GSINIT  (CODE)
                                    796 	.globl __sdcc_gsinit_startup
                                    797 	.globl __sdcc_program_startup
                                    798 	.globl __start__stack
                                    799 	.globl __mcs51_genRAMCLEAR
                                    800 ;	usr/main.c:131: volatile UINT8 pBuf = 0;
      000074 90 00 23         [24]  801 	mov	dptr,#_pBuf
      000077 E4               [12]  802 	clr	a
      000078 F0               [24]  803 	movx	@dptr,a
                                    804 ;	usr/main.c:132: volatile UINT8 fRecv = 0;
      000079 90 00 24         [24]  805 	mov	dptr,#_fRecv
      00007C F0               [24]  806 	movx	@dptr,a
                                    807 	.area GSFINAL (CODE)
      00007D 02 00 5E         [24]  808 	ljmp	__sdcc_program_startup
                                    809 ;--------------------------------------------------------
                                    810 ; Home
                                    811 ;--------------------------------------------------------
                                    812 	.area HOME    (CODE)
                                    813 	.area HOME    (CODE)
      00005E                        814 __sdcc_program_startup:
      00005E 02 09 12         [24]  815 	ljmp	_main
                                    816 ;	return from main will return to caller
                                    817 ;--------------------------------------------------------
                                    818 ; code
                                    819 ;--------------------------------------------------------
                                    820 	.area CSEG    (CODE)
                                    821 ;------------------------------------------------------------
                                    822 ;Allocation info for local variables in function 'UART1Init'
                                    823 ;------------------------------------------------------------
                                    824 ;interrupt                 Allocated to stack - _bp -3
                                    825 ;baudrate                  Allocated to registers r4 r5 r6 r7 
                                    826 ;------------------------------------------------------------
                                    827 ;	usr/main.c:31: void UART1Init(UINT32 baudrate,UINT8 interrupt)
                                    828 ;	-----------------------------------------
                                    829 ;	 function UART1Init
                                    830 ;	-----------------------------------------
      000080                        831 _UART1Init:
                           000007   832 	ar7 = 0x07
                           000006   833 	ar6 = 0x06
                           000005   834 	ar5 = 0x05
                           000004   835 	ar4 = 0x04
                           000003   836 	ar3 = 0x03
                           000002   837 	ar2 = 0x02
                           000001   838 	ar1 = 0x01
                           000000   839 	ar0 = 0x00
      000080 C0 16            [24]  840 	push	_bp
      000082 85 81 16         [24]  841 	mov	_bp,sp
      000085 AC 82            [24]  842 	mov	r4,dpl
      000087 AD 83            [24]  843 	mov	r5,dph
      000089 AE F0            [24]  844 	mov	r6,b
      00008B FF               [12]  845 	mov	r7,a
                                    846 ;	usr/main.c:33: SCON1 &= ~bU1SM0;                    //选择8位数据通讯
      00008C 53 BC 7F         [24]  847 	anl	_SCON1,#0x7f
                                    848 ;	usr/main.c:34: SCON1 |= bU1SMOD;                    //快速模式
      00008F 43 BC 20         [24]  849 	orl	_SCON1,#0x20
                                    850 ;	usr/main.c:35: SCON1 |= bU1REN;                     //使能接收
      000092 43 BC 10         [24]  851 	orl	_SCON1,#0x10
                                    852 ;	usr/main.c:36: SBAUD1 = 0 - FREQ_SYS/16/baudrate; //波特率配置
      000095 C0 04            [24]  853 	push	ar4
      000097 C0 05            [24]  854 	push	ar5
      000099 C0 06            [24]  855 	push	ar6
      00009B C0 07            [24]  856 	push	ar7
      00009D 90 C6 C0         [24]  857 	mov	dptr,#0xc6c0
      0000A0 75 F0 2D         [24]  858 	mov	b,#0x2d
      0000A3 E4               [12]  859 	clr	a
      0000A4 12 1D 10         [24]  860 	lcall	__divulong
      0000A7 AC 82            [24]  861 	mov	r4,dpl
      0000A9 E5 81            [12]  862 	mov	a,sp
      0000AB 24 FC            [12]  863 	add	a,#0xfc
      0000AD F5 81            [12]  864 	mov	sp,a
      0000AF C3               [12]  865 	clr	c
      0000B0 E4               [12]  866 	clr	a
      0000B1 9C               [12]  867 	subb	a,r4
      0000B2 F5 BE            [12]  868 	mov	_SBAUD1,a
                                    869 ;	usr/main.c:37: SIF1 = bU1TI;                        //清空发送完成标志
      0000B4 75 BF 02         [24]  870 	mov	_SIF1,#0x02
                                    871 ;	usr/main.c:38: if(interrupt){                   //开启中断使能
      0000B7 E5 16            [12]  872 	mov	a,_bp
      0000B9 24 FD            [12]  873 	add	a,#0xfd
      0000BB F8               [12]  874 	mov	r0,a
      0000BC E6               [12]  875 	mov	a,@r0
      0000BD 60 04            [24]  876 	jz	00103$
                                    877 ;	usr/main.c:39: IE_UART1 = 1;
                                    878 ;	assignBit
      0000BF D2 EC            [12]  879 	setb	_IE_UART1
                                    880 ;	usr/main.c:40: EA = 1;
                                    881 ;	assignBit
      0000C1 D2 AF            [12]  882 	setb	_EA
      0000C3                        883 00103$:
                                    884 ;	usr/main.c:42: }
      0000C3 D0 16            [24]  885 	pop	_bp
      0000C5 22               [24]  886 	ret
                                    887 ;------------------------------------------------------------
                                    888 ;Allocation info for local variables in function 'decode_XenP201S'
                                    889 ;------------------------------------------------------------
                                    890 ;radar                     Allocated to stack - _bp -5
                                    891 ;buf                       Allocated to stack - _bp +1
                                    892 ;sloc0                     Allocated to stack - _bp +4
                                    893 ;sloc1                     Allocated to stack - _bp +6
                                    894 ;sloc2                     Allocated to stack - _bp +12
                                    895 ;sloc3                     Allocated to stack - _bp +9
                                    896 ;------------------------------------------------------------
                                    897 ;	usr/main.c:65: UINT8 decode_XenP201S( char* buf,RadarData* radar){
                                    898 ;	-----------------------------------------
                                    899 ;	 function decode_XenP201S
                                    900 ;	-----------------------------------------
      0000C6                        901 _decode_XenP201S:
      0000C6 C0 16            [24]  902 	push	_bp
      0000C8 85 81 16         [24]  903 	mov	_bp,sp
      0000CB C0 82            [24]  904 	push	dpl
      0000CD C0 83            [24]  905 	push	dph
      0000CF C0 F0            [24]  906 	push	b
      0000D1 E5 81            [12]  907 	mov	a,sp
      0000D3 24 0B            [12]  908 	add	a,#0x0b
      0000D5 F5 81            [12]  909 	mov	sp,a
                                    910 ;	usr/main.c:66: radar->x1Pos = (((int)buf[2]<<8)+buf[1]);
      0000D7 E5 16            [12]  911 	mov	a,_bp
      0000D9 24 FB            [12]  912 	add	a,#0xfb
      0000DB F8               [12]  913 	mov	r0,a
      0000DC E5 16            [12]  914 	mov	a,_bp
      0000DE 24 0C            [12]  915 	add	a,#0x0c
      0000E0 F9               [12]  916 	mov	r1,a
      0000E1 E6               [12]  917 	mov	a,@r0
      0000E2 F7               [12]  918 	mov	@r1,a
      0000E3 08               [12]  919 	inc	r0
      0000E4 09               [12]  920 	inc	r1
      0000E5 E6               [12]  921 	mov	a,@r0
      0000E6 F7               [12]  922 	mov	@r1,a
      0000E7 08               [12]  923 	inc	r0
      0000E8 09               [12]  924 	inc	r1
      0000E9 E6               [12]  925 	mov	a,@r0
      0000EA F7               [12]  926 	mov	@r1,a
      0000EB A8 16            [24]  927 	mov	r0,_bp
      0000ED 08               [12]  928 	inc	r0
      0000EE 74 02            [12]  929 	mov	a,#0x02
      0000F0 26               [12]  930 	add	a,@r0
      0000F1 FD               [12]  931 	mov	r5,a
      0000F2 E4               [12]  932 	clr	a
      0000F3 08               [12]  933 	inc	r0
      0000F4 36               [12]  934 	addc	a,@r0
      0000F5 FE               [12]  935 	mov	r6,a
      0000F6 08               [12]  936 	inc	r0
      0000F7 86 07            [24]  937 	mov	ar7,@r0
      0000F9 8D 82            [24]  938 	mov	dpl,r5
      0000FB 8E 83            [24]  939 	mov	dph,r6
      0000FD 8F F0            [24]  940 	mov	b,r7
      0000FF 12 1F A0         [24]  941 	lcall	__gptrget
      000102 FB               [12]  942 	mov	r3,a
      000103 7C 00            [12]  943 	mov	r4,#0x00
      000105 A8 16            [24]  944 	mov	r0,_bp
      000107 08               [12]  945 	inc	r0
      000108 74 01            [12]  946 	mov	a,#0x01
      00010A 26               [12]  947 	add	a,@r0
      00010B FD               [12]  948 	mov	r5,a
      00010C E4               [12]  949 	clr	a
      00010D 08               [12]  950 	inc	r0
      00010E 36               [12]  951 	addc	a,@r0
      00010F FE               [12]  952 	mov	r6,a
      000110 08               [12]  953 	inc	r0
      000111 86 07            [24]  954 	mov	ar7,@r0
      000113 8D 82            [24]  955 	mov	dpl,r5
      000115 8E 83            [24]  956 	mov	dph,r6
      000117 8F F0            [24]  957 	mov	b,r7
      000119 12 1F A0         [24]  958 	lcall	__gptrget
      00011C 7F 00            [12]  959 	mov	r7,#0x00
      00011E 2C               [12]  960 	add	a,r4
      00011F FD               [12]  961 	mov	r5,a
      000120 EF               [12]  962 	mov	a,r7
      000121 3B               [12]  963 	addc	a,r3
      000122 FF               [12]  964 	mov	r7,a
      000123 E5 16            [12]  965 	mov	a,_bp
      000125 24 0C            [12]  966 	add	a,#0x0c
      000127 F8               [12]  967 	mov	r0,a
      000128 86 82            [24]  968 	mov	dpl,@r0
      00012A 08               [12]  969 	inc	r0
      00012B 86 83            [24]  970 	mov	dph,@r0
      00012D 08               [12]  971 	inc	r0
      00012E 86 F0            [24]  972 	mov	b,@r0
      000130 ED               [12]  973 	mov	a,r5
      000131 12 1B 01         [24]  974 	lcall	__gptrput
      000134 A3               [24]  975 	inc	dptr
      000135 EF               [12]  976 	mov	a,r7
      000136 12 1B 01         [24]  977 	lcall	__gptrput
                                    978 ;	usr/main.c:67: radar->y1Pos = (((int)buf[4]<<8)+buf[3]);
      000139 E5 16            [12]  979 	mov	a,_bp
      00013B 24 0C            [12]  980 	add	a,#0x0c
      00013D F8               [12]  981 	mov	r0,a
      00013E 74 02            [12]  982 	mov	a,#0x02
      000140 26               [12]  983 	add	a,@r0
      000141 FC               [12]  984 	mov	r4,a
      000142 E4               [12]  985 	clr	a
      000143 08               [12]  986 	inc	r0
      000144 36               [12]  987 	addc	a,@r0
      000145 FB               [12]  988 	mov	r3,a
      000146 08               [12]  989 	inc	r0
      000147 86 02            [24]  990 	mov	ar2,@r0
      000149 A8 16            [24]  991 	mov	r0,_bp
      00014B 08               [12]  992 	inc	r0
      00014C 74 04            [12]  993 	mov	a,#0x04
      00014E 26               [12]  994 	add	a,@r0
      00014F FD               [12]  995 	mov	r5,a
      000150 E4               [12]  996 	clr	a
      000151 08               [12]  997 	inc	r0
      000152 36               [12]  998 	addc	a,@r0
      000153 FE               [12]  999 	mov	r6,a
      000154 08               [12] 1000 	inc	r0
      000155 86 07            [24] 1001 	mov	ar7,@r0
      000157 8D 82            [24] 1002 	mov	dpl,r5
      000159 8E 83            [24] 1003 	mov	dph,r6
      00015B 8F F0            [24] 1004 	mov	b,r7
      00015D 12 1F A0         [24] 1005 	lcall	__gptrget
      000160 FD               [12] 1006 	mov	r5,a
      000161 E5 16            [12] 1007 	mov	a,_bp
      000163 24 04            [12] 1008 	add	a,#0x04
      000165 F8               [12] 1009 	mov	r0,a
      000166 08               [12] 1010 	inc	r0
      000167 A6 05            [24] 1011 	mov	@r0,ar5
      000169 18               [12] 1012 	dec	r0
      00016A 76 00            [12] 1013 	mov	@r0,#0x00
      00016C A8 16            [24] 1014 	mov	r0,_bp
      00016E 08               [12] 1015 	inc	r0
      00016F 74 03            [12] 1016 	mov	a,#0x03
      000171 26               [12] 1017 	add	a,@r0
      000172 FD               [12] 1018 	mov	r5,a
      000173 E4               [12] 1019 	clr	a
      000174 08               [12] 1020 	inc	r0
      000175 36               [12] 1021 	addc	a,@r0
      000176 FE               [12] 1022 	mov	r6,a
      000177 08               [12] 1023 	inc	r0
      000178 86 07            [24] 1024 	mov	ar7,@r0
      00017A 8D 82            [24] 1025 	mov	dpl,r5
      00017C 8E 83            [24] 1026 	mov	dph,r6
      00017E 8F F0            [24] 1027 	mov	b,r7
      000180 12 1F A0         [24] 1028 	lcall	__gptrget
      000183 FD               [12] 1029 	mov	r5,a
      000184 7F 00            [12] 1030 	mov	r7,#0x00
      000186 E5 16            [12] 1031 	mov	a,_bp
      000188 24 04            [12] 1032 	add	a,#0x04
      00018A F8               [12] 1033 	mov	r0,a
      00018B ED               [12] 1034 	mov	a,r5
      00018C 26               [12] 1035 	add	a,@r0
      00018D FD               [12] 1036 	mov	r5,a
      00018E EF               [12] 1037 	mov	a,r7
      00018F 08               [12] 1038 	inc	r0
      000190 36               [12] 1039 	addc	a,@r0
      000191 FF               [12] 1040 	mov	r7,a
      000192 8C 82            [24] 1041 	mov	dpl,r4
      000194 8B 83            [24] 1042 	mov	dph,r3
      000196 8A F0            [24] 1043 	mov	b,r2
      000198 ED               [12] 1044 	mov	a,r5
      000199 12 1B 01         [24] 1045 	lcall	__gptrput
      00019C A3               [24] 1046 	inc	dptr
      00019D EF               [12] 1047 	mov	a,r7
      00019E 12 1B 01         [24] 1048 	lcall	__gptrput
                                   1049 ;	usr/main.c:68: radar->dist1 = (((int)buf[6]<<8)+buf[5]);
      0001A1 E5 16            [12] 1050 	mov	a,_bp
      0001A3 24 0C            [12] 1051 	add	a,#0x0c
      0001A5 F8               [12] 1052 	mov	r0,a
      0001A6 74 04            [12] 1053 	mov	a,#0x04
      0001A8 26               [12] 1054 	add	a,@r0
      0001A9 FC               [12] 1055 	mov	r4,a
      0001AA E4               [12] 1056 	clr	a
      0001AB 08               [12] 1057 	inc	r0
      0001AC 36               [12] 1058 	addc	a,@r0
      0001AD FB               [12] 1059 	mov	r3,a
      0001AE 08               [12] 1060 	inc	r0
      0001AF 86 02            [24] 1061 	mov	ar2,@r0
      0001B1 A8 16            [24] 1062 	mov	r0,_bp
      0001B3 08               [12] 1063 	inc	r0
      0001B4 74 06            [12] 1064 	mov	a,#0x06
      0001B6 26               [12] 1065 	add	a,@r0
      0001B7 FD               [12] 1066 	mov	r5,a
      0001B8 E4               [12] 1067 	clr	a
      0001B9 08               [12] 1068 	inc	r0
      0001BA 36               [12] 1069 	addc	a,@r0
      0001BB FE               [12] 1070 	mov	r6,a
      0001BC 08               [12] 1071 	inc	r0
      0001BD 86 07            [24] 1072 	mov	ar7,@r0
      0001BF 8D 82            [24] 1073 	mov	dpl,r5
      0001C1 8E 83            [24] 1074 	mov	dph,r6
      0001C3 8F F0            [24] 1075 	mov	b,r7
      0001C5 12 1F A0         [24] 1076 	lcall	__gptrget
      0001C8 FD               [12] 1077 	mov	r5,a
      0001C9 E5 16            [12] 1078 	mov	a,_bp
      0001CB 24 04            [12] 1079 	add	a,#0x04
      0001CD F8               [12] 1080 	mov	r0,a
      0001CE 08               [12] 1081 	inc	r0
      0001CF A6 05            [24] 1082 	mov	@r0,ar5
      0001D1 18               [12] 1083 	dec	r0
      0001D2 76 00            [12] 1084 	mov	@r0,#0x00
      0001D4 A8 16            [24] 1085 	mov	r0,_bp
      0001D6 08               [12] 1086 	inc	r0
      0001D7 74 05            [12] 1087 	mov	a,#0x05
      0001D9 26               [12] 1088 	add	a,@r0
      0001DA FD               [12] 1089 	mov	r5,a
      0001DB E4               [12] 1090 	clr	a
      0001DC 08               [12] 1091 	inc	r0
      0001DD 36               [12] 1092 	addc	a,@r0
      0001DE FE               [12] 1093 	mov	r6,a
      0001DF 08               [12] 1094 	inc	r0
      0001E0 86 07            [24] 1095 	mov	ar7,@r0
      0001E2 8D 82            [24] 1096 	mov	dpl,r5
      0001E4 8E 83            [24] 1097 	mov	dph,r6
      0001E6 8F F0            [24] 1098 	mov	b,r7
      0001E8 12 1F A0         [24] 1099 	lcall	__gptrget
      0001EB FD               [12] 1100 	mov	r5,a
      0001EC 7F 00            [12] 1101 	mov	r7,#0x00
      0001EE E5 16            [12] 1102 	mov	a,_bp
      0001F0 24 04            [12] 1103 	add	a,#0x04
      0001F2 F8               [12] 1104 	mov	r0,a
      0001F3 ED               [12] 1105 	mov	a,r5
      0001F4 26               [12] 1106 	add	a,@r0
      0001F5 FD               [12] 1107 	mov	r5,a
      0001F6 EF               [12] 1108 	mov	a,r7
      0001F7 08               [12] 1109 	inc	r0
      0001F8 36               [12] 1110 	addc	a,@r0
      0001F9 FF               [12] 1111 	mov	r7,a
      0001FA 8C 82            [24] 1112 	mov	dpl,r4
      0001FC 8B 83            [24] 1113 	mov	dph,r3
      0001FE 8A F0            [24] 1114 	mov	b,r2
      000200 ED               [12] 1115 	mov	a,r5
      000201 12 1B 01         [24] 1116 	lcall	__gptrput
      000204 A3               [24] 1117 	inc	dptr
      000205 EF               [12] 1118 	mov	a,r7
      000206 12 1B 01         [24] 1119 	lcall	__gptrput
                                   1120 ;	usr/main.c:69: radar->ang1 = (((int)buf[8]<<8)+buf[7]);
      000209 E5 16            [12] 1121 	mov	a,_bp
      00020B 24 0C            [12] 1122 	add	a,#0x0c
      00020D F8               [12] 1123 	mov	r0,a
      00020E E5 16            [12] 1124 	mov	a,_bp
      000210 24 06            [12] 1125 	add	a,#0x06
      000212 F9               [12] 1126 	mov	r1,a
      000213 74 06            [12] 1127 	mov	a,#0x06
      000215 26               [12] 1128 	add	a,@r0
      000216 F7               [12] 1129 	mov	@r1,a
      000217 E4               [12] 1130 	clr	a
      000218 08               [12] 1131 	inc	r0
      000219 36               [12] 1132 	addc	a,@r0
      00021A 09               [12] 1133 	inc	r1
      00021B F7               [12] 1134 	mov	@r1,a
      00021C 08               [12] 1135 	inc	r0
      00021D 09               [12] 1136 	inc	r1
      00021E E6               [12] 1137 	mov	a,@r0
      00021F F7               [12] 1138 	mov	@r1,a
      000220 A8 16            [24] 1139 	mov	r0,_bp
      000222 08               [12] 1140 	inc	r0
      000223 74 08            [12] 1141 	mov	a,#0x08
      000225 26               [12] 1142 	add	a,@r0
      000226 FD               [12] 1143 	mov	r5,a
      000227 E4               [12] 1144 	clr	a
      000228 08               [12] 1145 	inc	r0
      000229 36               [12] 1146 	addc	a,@r0
      00022A FE               [12] 1147 	mov	r6,a
      00022B 08               [12] 1148 	inc	r0
      00022C 86 07            [24] 1149 	mov	ar7,@r0
      00022E 8D 82            [24] 1150 	mov	dpl,r5
      000230 8E 83            [24] 1151 	mov	dph,r6
      000232 8F F0            [24] 1152 	mov	b,r7
      000234 12 1F A0         [24] 1153 	lcall	__gptrget
      000237 FB               [12] 1154 	mov	r3,a
      000238 7C 00            [12] 1155 	mov	r4,#0x00
      00023A A8 16            [24] 1156 	mov	r0,_bp
      00023C 08               [12] 1157 	inc	r0
      00023D 74 07            [12] 1158 	mov	a,#0x07
      00023F 26               [12] 1159 	add	a,@r0
      000240 FD               [12] 1160 	mov	r5,a
      000241 E4               [12] 1161 	clr	a
      000242 08               [12] 1162 	inc	r0
      000243 36               [12] 1163 	addc	a,@r0
      000244 FE               [12] 1164 	mov	r6,a
      000245 08               [12] 1165 	inc	r0
      000246 86 07            [24] 1166 	mov	ar7,@r0
      000248 8D 82            [24] 1167 	mov	dpl,r5
      00024A 8E 83            [24] 1168 	mov	dph,r6
      00024C 8F F0            [24] 1169 	mov	b,r7
      00024E 12 1F A0         [24] 1170 	lcall	__gptrget
      000251 7F 00            [12] 1171 	mov	r7,#0x00
      000253 2C               [12] 1172 	add	a,r4
      000254 FD               [12] 1173 	mov	r5,a
      000255 EF               [12] 1174 	mov	a,r7
      000256 3B               [12] 1175 	addc	a,r3
      000257 FF               [12] 1176 	mov	r7,a
      000258 E5 16            [12] 1177 	mov	a,_bp
      00025A 24 06            [12] 1178 	add	a,#0x06
      00025C F8               [12] 1179 	mov	r0,a
      00025D 86 82            [24] 1180 	mov	dpl,@r0
      00025F 08               [12] 1181 	inc	r0
      000260 86 83            [24] 1182 	mov	dph,@r0
      000262 08               [12] 1183 	inc	r0
      000263 86 F0            [24] 1184 	mov	b,@r0
      000265 ED               [12] 1185 	mov	a,r5
      000266 12 1B 01         [24] 1186 	lcall	__gptrput
      000269 A3               [24] 1187 	inc	dptr
      00026A EF               [12] 1188 	mov	a,r7
      00026B 12 1B 01         [24] 1189 	lcall	__gptrput
                                   1190 ;	usr/main.c:74: radar->ang1 = radar->ang1 / 10.0 - 90;
      00026E 8D 82            [24] 1191 	mov	dpl,r5
      000270 8F 83            [24] 1192 	mov	dph,r7
      000272 12 1D 9D         [24] 1193 	lcall	___sint2fs
      000275 AC 82            [24] 1194 	mov	r4,dpl
      000277 AD 83            [24] 1195 	mov	r5,dph
      000279 AE F0            [24] 1196 	mov	r6,b
      00027B FF               [12] 1197 	mov	r7,a
      00027C E4               [12] 1198 	clr	a
      00027D C0 E0            [24] 1199 	push	acc
      00027F C0 E0            [24] 1200 	push	acc
      000281 74 20            [12] 1201 	mov	a,#0x20
      000283 C0 E0            [24] 1202 	push	acc
      000285 74 41            [12] 1203 	mov	a,#0x41
      000287 C0 E0            [24] 1204 	push	acc
      000289 8C 82            [24] 1205 	mov	dpl,r4
      00028B 8D 83            [24] 1206 	mov	dph,r5
      00028D 8E F0            [24] 1207 	mov	b,r6
      00028F EF               [12] 1208 	mov	a,r7
      000290 12 1E DD         [24] 1209 	lcall	___fsdiv
      000293 AC 82            [24] 1210 	mov	r4,dpl
      000295 AD 83            [24] 1211 	mov	r5,dph
      000297 AE F0            [24] 1212 	mov	r6,b
      000299 FF               [12] 1213 	mov	r7,a
      00029A E5 81            [12] 1214 	mov	a,sp
      00029C 24 FC            [12] 1215 	add	a,#0xfc
      00029E F5 81            [12] 1216 	mov	sp,a
      0002A0 E4               [12] 1217 	clr	a
      0002A1 C0 E0            [24] 1218 	push	acc
      0002A3 C0 E0            [24] 1219 	push	acc
      0002A5 74 B4            [12] 1220 	mov	a,#0xb4
      0002A7 C0 E0            [24] 1221 	push	acc
      0002A9 74 42            [12] 1222 	mov	a,#0x42
      0002AB C0 E0            [24] 1223 	push	acc
      0002AD 8C 82            [24] 1224 	mov	dpl,r4
      0002AF 8D 83            [24] 1225 	mov	dph,r5
      0002B1 8E F0            [24] 1226 	mov	b,r6
      0002B3 EF               [12] 1227 	mov	a,r7
      0002B4 12 16 32         [24] 1228 	lcall	___fssub
      0002B7 AC 82            [24] 1229 	mov	r4,dpl
      0002B9 AD 83            [24] 1230 	mov	r5,dph
      0002BB AE F0            [24] 1231 	mov	r6,b
      0002BD FF               [12] 1232 	mov	r7,a
      0002BE E5 81            [12] 1233 	mov	a,sp
      0002C0 24 FC            [12] 1234 	add	a,#0xfc
      0002C2 F5 81            [12] 1235 	mov	sp,a
      0002C4 8C 82            [24] 1236 	mov	dpl,r4
      0002C6 8D 83            [24] 1237 	mov	dph,r5
      0002C8 8E F0            [24] 1238 	mov	b,r6
      0002CA EF               [12] 1239 	mov	a,r7
      0002CB 12 1D AA         [24] 1240 	lcall	___fs2sint
      0002CE AF 82            [24] 1241 	mov	r7,dpl
      0002D0 AE 83            [24] 1242 	mov	r6,dph
      0002D2 E5 16            [12] 1243 	mov	a,_bp
      0002D4 24 06            [12] 1244 	add	a,#0x06
      0002D6 F8               [12] 1245 	mov	r0,a
      0002D7 86 82            [24] 1246 	mov	dpl,@r0
      0002D9 08               [12] 1247 	inc	r0
      0002DA 86 83            [24] 1248 	mov	dph,@r0
      0002DC 08               [12] 1249 	inc	r0
      0002DD 86 F0            [24] 1250 	mov	b,@r0
      0002DF EF               [12] 1251 	mov	a,r7
      0002E0 12 1B 01         [24] 1252 	lcall	__gptrput
      0002E3 A3               [24] 1253 	inc	dptr
      0002E4 EE               [12] 1254 	mov	a,r6
      0002E5 12 1B 01         [24] 1255 	lcall	__gptrput
                                   1256 ;	usr/main.c:76: radar->x2Pos = (((int)buf[10]<<8)+buf[9]);
      0002E8 E5 16            [12] 1257 	mov	a,_bp
      0002EA 24 0C            [12] 1258 	add	a,#0x0c
      0002EC F8               [12] 1259 	mov	r0,a
      0002ED 74 08            [12] 1260 	mov	a,#0x08
      0002EF 26               [12] 1261 	add	a,@r0
      0002F0 FD               [12] 1262 	mov	r5,a
      0002F1 E4               [12] 1263 	clr	a
      0002F2 08               [12] 1264 	inc	r0
      0002F3 36               [12] 1265 	addc	a,@r0
      0002F4 FE               [12] 1266 	mov	r6,a
      0002F5 08               [12] 1267 	inc	r0
      0002F6 86 07            [24] 1268 	mov	ar7,@r0
      0002F8 A8 16            [24] 1269 	mov	r0,_bp
      0002FA 08               [12] 1270 	inc	r0
      0002FB 74 0A            [12] 1271 	mov	a,#0x0a
      0002FD 26               [12] 1272 	add	a,@r0
      0002FE FA               [12] 1273 	mov	r2,a
      0002FF E4               [12] 1274 	clr	a
      000300 08               [12] 1275 	inc	r0
      000301 36               [12] 1276 	addc	a,@r0
      000302 FB               [12] 1277 	mov	r3,a
      000303 08               [12] 1278 	inc	r0
      000304 86 04            [24] 1279 	mov	ar4,@r0
      000306 8A 82            [24] 1280 	mov	dpl,r2
      000308 8B 83            [24] 1281 	mov	dph,r3
      00030A 8C F0            [24] 1282 	mov	b,r4
      00030C 12 1F A0         [24] 1283 	lcall	__gptrget
      00030F FA               [12] 1284 	mov	r2,a
      000310 E5 16            [12] 1285 	mov	a,_bp
      000312 24 06            [12] 1286 	add	a,#0x06
      000314 F8               [12] 1287 	mov	r0,a
      000315 08               [12] 1288 	inc	r0
      000316 A6 02            [24] 1289 	mov	@r0,ar2
      000318 18               [12] 1290 	dec	r0
      000319 76 00            [12] 1291 	mov	@r0,#0x00
      00031B A8 16            [24] 1292 	mov	r0,_bp
      00031D 08               [12] 1293 	inc	r0
      00031E 74 09            [12] 1294 	mov	a,#0x09
      000320 26               [12] 1295 	add	a,@r0
      000321 FA               [12] 1296 	mov	r2,a
      000322 E4               [12] 1297 	clr	a
      000323 08               [12] 1298 	inc	r0
      000324 36               [12] 1299 	addc	a,@r0
      000325 FB               [12] 1300 	mov	r3,a
      000326 08               [12] 1301 	inc	r0
      000327 86 04            [24] 1302 	mov	ar4,@r0
      000329 8A 82            [24] 1303 	mov	dpl,r2
      00032B 8B 83            [24] 1304 	mov	dph,r3
      00032D 8C F0            [24] 1305 	mov	b,r4
      00032F 12 1F A0         [24] 1306 	lcall	__gptrget
      000332 FA               [12] 1307 	mov	r2,a
      000333 7C 00            [12] 1308 	mov	r4,#0x00
      000335 E5 16            [12] 1309 	mov	a,_bp
      000337 24 06            [12] 1310 	add	a,#0x06
      000339 F8               [12] 1311 	mov	r0,a
      00033A EA               [12] 1312 	mov	a,r2
      00033B 26               [12] 1313 	add	a,@r0
      00033C FA               [12] 1314 	mov	r2,a
      00033D EC               [12] 1315 	mov	a,r4
      00033E 08               [12] 1316 	inc	r0
      00033F 36               [12] 1317 	addc	a,@r0
      000340 FC               [12] 1318 	mov	r4,a
      000341 8D 82            [24] 1319 	mov	dpl,r5
      000343 8E 83            [24] 1320 	mov	dph,r6
      000345 8F F0            [24] 1321 	mov	b,r7
      000347 EA               [12] 1322 	mov	a,r2
      000348 12 1B 01         [24] 1323 	lcall	__gptrput
      00034B A3               [24] 1324 	inc	dptr
      00034C EC               [12] 1325 	mov	a,r4
      00034D 12 1B 01         [24] 1326 	lcall	__gptrput
                                   1327 ;	usr/main.c:77: radar->y2Pos = (((int)buf[12]<<8)+buf[11]);
      000350 E5 16            [12] 1328 	mov	a,_bp
      000352 24 0C            [12] 1329 	add	a,#0x0c
      000354 F8               [12] 1330 	mov	r0,a
      000355 74 0A            [12] 1331 	mov	a,#0x0a
      000357 26               [12] 1332 	add	a,@r0
      000358 FD               [12] 1333 	mov	r5,a
      000359 E4               [12] 1334 	clr	a
      00035A 08               [12] 1335 	inc	r0
      00035B 36               [12] 1336 	addc	a,@r0
      00035C FE               [12] 1337 	mov	r6,a
      00035D 08               [12] 1338 	inc	r0
      00035E 86 07            [24] 1339 	mov	ar7,@r0
      000360 A8 16            [24] 1340 	mov	r0,_bp
      000362 08               [12] 1341 	inc	r0
      000363 74 0C            [12] 1342 	mov	a,#0x0c
      000365 26               [12] 1343 	add	a,@r0
      000366 FA               [12] 1344 	mov	r2,a
      000367 E4               [12] 1345 	clr	a
      000368 08               [12] 1346 	inc	r0
      000369 36               [12] 1347 	addc	a,@r0
      00036A FB               [12] 1348 	mov	r3,a
      00036B 08               [12] 1349 	inc	r0
      00036C 86 04            [24] 1350 	mov	ar4,@r0
      00036E 8A 82            [24] 1351 	mov	dpl,r2
      000370 8B 83            [24] 1352 	mov	dph,r3
      000372 8C F0            [24] 1353 	mov	b,r4
      000374 12 1F A0         [24] 1354 	lcall	__gptrget
      000377 FA               [12] 1355 	mov	r2,a
      000378 E5 16            [12] 1356 	mov	a,_bp
      00037A 24 06            [12] 1357 	add	a,#0x06
      00037C F8               [12] 1358 	mov	r0,a
      00037D 08               [12] 1359 	inc	r0
      00037E A6 02            [24] 1360 	mov	@r0,ar2
      000380 18               [12] 1361 	dec	r0
      000381 76 00            [12] 1362 	mov	@r0,#0x00
      000383 A8 16            [24] 1363 	mov	r0,_bp
      000385 08               [12] 1364 	inc	r0
      000386 74 0B            [12] 1365 	mov	a,#0x0b
      000388 26               [12] 1366 	add	a,@r0
      000389 FA               [12] 1367 	mov	r2,a
      00038A E4               [12] 1368 	clr	a
      00038B 08               [12] 1369 	inc	r0
      00038C 36               [12] 1370 	addc	a,@r0
      00038D FB               [12] 1371 	mov	r3,a
      00038E 08               [12] 1372 	inc	r0
      00038F 86 04            [24] 1373 	mov	ar4,@r0
      000391 8A 82            [24] 1374 	mov	dpl,r2
      000393 8B 83            [24] 1375 	mov	dph,r3
      000395 8C F0            [24] 1376 	mov	b,r4
      000397 12 1F A0         [24] 1377 	lcall	__gptrget
      00039A FA               [12] 1378 	mov	r2,a
      00039B 7C 00            [12] 1379 	mov	r4,#0x00
      00039D E5 16            [12] 1380 	mov	a,_bp
      00039F 24 06            [12] 1381 	add	a,#0x06
      0003A1 F8               [12] 1382 	mov	r0,a
      0003A2 EA               [12] 1383 	mov	a,r2
      0003A3 26               [12] 1384 	add	a,@r0
      0003A4 FA               [12] 1385 	mov	r2,a
      0003A5 EC               [12] 1386 	mov	a,r4
      0003A6 08               [12] 1387 	inc	r0
      0003A7 36               [12] 1388 	addc	a,@r0
      0003A8 FC               [12] 1389 	mov	r4,a
      0003A9 8D 82            [24] 1390 	mov	dpl,r5
      0003AB 8E 83            [24] 1391 	mov	dph,r6
      0003AD 8F F0            [24] 1392 	mov	b,r7
      0003AF EA               [12] 1393 	mov	a,r2
      0003B0 12 1B 01         [24] 1394 	lcall	__gptrput
      0003B3 A3               [24] 1395 	inc	dptr
      0003B4 EC               [12] 1396 	mov	a,r4
      0003B5 12 1B 01         [24] 1397 	lcall	__gptrput
                                   1398 ;	usr/main.c:78: radar->dist2 = (((int)buf[14]<<8)+buf[13]);
      0003B8 E5 16            [12] 1399 	mov	a,_bp
      0003BA 24 0C            [12] 1400 	add	a,#0x0c
      0003BC F8               [12] 1401 	mov	r0,a
      0003BD 74 0C            [12] 1402 	mov	a,#0x0c
      0003BF 26               [12] 1403 	add	a,@r0
      0003C0 FD               [12] 1404 	mov	r5,a
      0003C1 E4               [12] 1405 	clr	a
      0003C2 08               [12] 1406 	inc	r0
      0003C3 36               [12] 1407 	addc	a,@r0
      0003C4 FE               [12] 1408 	mov	r6,a
      0003C5 08               [12] 1409 	inc	r0
      0003C6 86 07            [24] 1410 	mov	ar7,@r0
      0003C8 A8 16            [24] 1411 	mov	r0,_bp
      0003CA 08               [12] 1412 	inc	r0
      0003CB 74 0E            [12] 1413 	mov	a,#0x0e
      0003CD 26               [12] 1414 	add	a,@r0
      0003CE FA               [12] 1415 	mov	r2,a
      0003CF E4               [12] 1416 	clr	a
      0003D0 08               [12] 1417 	inc	r0
      0003D1 36               [12] 1418 	addc	a,@r0
      0003D2 FB               [12] 1419 	mov	r3,a
      0003D3 08               [12] 1420 	inc	r0
      0003D4 86 04            [24] 1421 	mov	ar4,@r0
      0003D6 8A 82            [24] 1422 	mov	dpl,r2
      0003D8 8B 83            [24] 1423 	mov	dph,r3
      0003DA 8C F0            [24] 1424 	mov	b,r4
      0003DC 12 1F A0         [24] 1425 	lcall	__gptrget
      0003DF FA               [12] 1426 	mov	r2,a
      0003E0 E5 16            [12] 1427 	mov	a,_bp
      0003E2 24 06            [12] 1428 	add	a,#0x06
      0003E4 F8               [12] 1429 	mov	r0,a
      0003E5 08               [12] 1430 	inc	r0
      0003E6 A6 02            [24] 1431 	mov	@r0,ar2
      0003E8 18               [12] 1432 	dec	r0
      0003E9 76 00            [12] 1433 	mov	@r0,#0x00
      0003EB A8 16            [24] 1434 	mov	r0,_bp
      0003ED 08               [12] 1435 	inc	r0
      0003EE 74 0D            [12] 1436 	mov	a,#0x0d
      0003F0 26               [12] 1437 	add	a,@r0
      0003F1 FA               [12] 1438 	mov	r2,a
      0003F2 E4               [12] 1439 	clr	a
      0003F3 08               [12] 1440 	inc	r0
      0003F4 36               [12] 1441 	addc	a,@r0
      0003F5 FB               [12] 1442 	mov	r3,a
      0003F6 08               [12] 1443 	inc	r0
      0003F7 86 04            [24] 1444 	mov	ar4,@r0
      0003F9 8A 82            [24] 1445 	mov	dpl,r2
      0003FB 8B 83            [24] 1446 	mov	dph,r3
      0003FD 8C F0            [24] 1447 	mov	b,r4
      0003FF 12 1F A0         [24] 1448 	lcall	__gptrget
      000402 FA               [12] 1449 	mov	r2,a
      000403 7C 00            [12] 1450 	mov	r4,#0x00
      000405 E5 16            [12] 1451 	mov	a,_bp
      000407 24 06            [12] 1452 	add	a,#0x06
      000409 F8               [12] 1453 	mov	r0,a
      00040A EA               [12] 1454 	mov	a,r2
      00040B 26               [12] 1455 	add	a,@r0
      00040C FA               [12] 1456 	mov	r2,a
      00040D EC               [12] 1457 	mov	a,r4
      00040E 08               [12] 1458 	inc	r0
      00040F 36               [12] 1459 	addc	a,@r0
      000410 FC               [12] 1460 	mov	r4,a
      000411 8D 82            [24] 1461 	mov	dpl,r5
      000413 8E 83            [24] 1462 	mov	dph,r6
      000415 8F F0            [24] 1463 	mov	b,r7
      000417 EA               [12] 1464 	mov	a,r2
      000418 12 1B 01         [24] 1465 	lcall	__gptrput
      00041B A3               [24] 1466 	inc	dptr
      00041C EC               [12] 1467 	mov	a,r4
      00041D 12 1B 01         [24] 1468 	lcall	__gptrput
                                   1469 ;	usr/main.c:79: radar->ang2 = (((int)buf[16]<<8)+buf[15]);
      000420 E5 16            [12] 1470 	mov	a,_bp
      000422 24 0C            [12] 1471 	add	a,#0x0c
      000424 F8               [12] 1472 	mov	r0,a
      000425 E5 16            [12] 1473 	mov	a,_bp
      000427 24 09            [12] 1474 	add	a,#0x09
      000429 F9               [12] 1475 	mov	r1,a
      00042A 74 0E            [12] 1476 	mov	a,#0x0e
      00042C 26               [12] 1477 	add	a,@r0
      00042D F7               [12] 1478 	mov	@r1,a
      00042E E4               [12] 1479 	clr	a
      00042F 08               [12] 1480 	inc	r0
      000430 36               [12] 1481 	addc	a,@r0
      000431 09               [12] 1482 	inc	r1
      000432 F7               [12] 1483 	mov	@r1,a
      000433 08               [12] 1484 	inc	r0
      000434 09               [12] 1485 	inc	r1
      000435 E6               [12] 1486 	mov	a,@r0
      000436 F7               [12] 1487 	mov	@r1,a
      000437 A8 16            [24] 1488 	mov	r0,_bp
      000439 08               [12] 1489 	inc	r0
      00043A 74 10            [12] 1490 	mov	a,#0x10
      00043C 26               [12] 1491 	add	a,@r0
      00043D FA               [12] 1492 	mov	r2,a
      00043E E4               [12] 1493 	clr	a
      00043F 08               [12] 1494 	inc	r0
      000440 36               [12] 1495 	addc	a,@r0
      000441 FB               [12] 1496 	mov	r3,a
      000442 08               [12] 1497 	inc	r0
      000443 86 04            [24] 1498 	mov	ar4,@r0
      000445 8A 82            [24] 1499 	mov	dpl,r2
      000447 8B 83            [24] 1500 	mov	dph,r3
      000449 8C F0            [24] 1501 	mov	b,r4
      00044B 12 1F A0         [24] 1502 	lcall	__gptrget
      00044E FE               [12] 1503 	mov	r6,a
      00044F 7F 00            [12] 1504 	mov	r7,#0x00
      000451 A8 16            [24] 1505 	mov	r0,_bp
      000453 08               [12] 1506 	inc	r0
      000454 74 0F            [12] 1507 	mov	a,#0x0f
      000456 26               [12] 1508 	add	a,@r0
      000457 FA               [12] 1509 	mov	r2,a
      000458 E4               [12] 1510 	clr	a
      000459 08               [12] 1511 	inc	r0
      00045A 36               [12] 1512 	addc	a,@r0
      00045B FB               [12] 1513 	mov	r3,a
      00045C 08               [12] 1514 	inc	r0
      00045D 86 04            [24] 1515 	mov	ar4,@r0
      00045F 8A 82            [24] 1516 	mov	dpl,r2
      000461 8B 83            [24] 1517 	mov	dph,r3
      000463 8C F0            [24] 1518 	mov	b,r4
      000465 12 1F A0         [24] 1519 	lcall	__gptrget
      000468 7C 00            [12] 1520 	mov	r4,#0x00
      00046A 2F               [12] 1521 	add	a,r7
      00046B FA               [12] 1522 	mov	r2,a
      00046C EC               [12] 1523 	mov	a,r4
      00046D 3E               [12] 1524 	addc	a,r6
      00046E FC               [12] 1525 	mov	r4,a
      00046F E5 16            [12] 1526 	mov	a,_bp
      000471 24 09            [12] 1527 	add	a,#0x09
      000473 F8               [12] 1528 	mov	r0,a
      000474 86 82            [24] 1529 	mov	dpl,@r0
      000476 08               [12] 1530 	inc	r0
      000477 86 83            [24] 1531 	mov	dph,@r0
      000479 08               [12] 1532 	inc	r0
      00047A 86 F0            [24] 1533 	mov	b,@r0
      00047C EA               [12] 1534 	mov	a,r2
      00047D 12 1B 01         [24] 1535 	lcall	__gptrput
      000480 A3               [24] 1536 	inc	dptr
      000481 EC               [12] 1537 	mov	a,r4
      000482 12 1B 01         [24] 1538 	lcall	__gptrput
                                   1539 ;	usr/main.c:84: radar->ang2 = radar->ang2 / 10.0 - 90;
      000485 8A 82            [24] 1540 	mov	dpl,r2
      000487 8C 83            [24] 1541 	mov	dph,r4
      000489 12 1D 9D         [24] 1542 	lcall	___sint2fs
      00048C AA 82            [24] 1543 	mov	r2,dpl
      00048E AB 83            [24] 1544 	mov	r3,dph
      000490 AC F0            [24] 1545 	mov	r4,b
      000492 FF               [12] 1546 	mov	r7,a
      000493 E4               [12] 1547 	clr	a
      000494 C0 E0            [24] 1548 	push	acc
      000496 C0 E0            [24] 1549 	push	acc
      000498 74 20            [12] 1550 	mov	a,#0x20
      00049A C0 E0            [24] 1551 	push	acc
      00049C 74 41            [12] 1552 	mov	a,#0x41
      00049E C0 E0            [24] 1553 	push	acc
      0004A0 8A 82            [24] 1554 	mov	dpl,r2
      0004A2 8B 83            [24] 1555 	mov	dph,r3
      0004A4 8C F0            [24] 1556 	mov	b,r4
      0004A6 EF               [12] 1557 	mov	a,r7
      0004A7 12 1E DD         [24] 1558 	lcall	___fsdiv
      0004AA AC 82            [24] 1559 	mov	r4,dpl
      0004AC AD 83            [24] 1560 	mov	r5,dph
      0004AE AE F0            [24] 1561 	mov	r6,b
      0004B0 FF               [12] 1562 	mov	r7,a
      0004B1 E5 81            [12] 1563 	mov	a,sp
      0004B3 24 FC            [12] 1564 	add	a,#0xfc
      0004B5 F5 81            [12] 1565 	mov	sp,a
      0004B7 E4               [12] 1566 	clr	a
      0004B8 C0 E0            [24] 1567 	push	acc
      0004BA C0 E0            [24] 1568 	push	acc
      0004BC 74 B4            [12] 1569 	mov	a,#0xb4
      0004BE C0 E0            [24] 1570 	push	acc
      0004C0 74 42            [12] 1571 	mov	a,#0x42
      0004C2 C0 E0            [24] 1572 	push	acc
      0004C4 8C 82            [24] 1573 	mov	dpl,r4
      0004C6 8D 83            [24] 1574 	mov	dph,r5
      0004C8 8E F0            [24] 1575 	mov	b,r6
      0004CA EF               [12] 1576 	mov	a,r7
      0004CB 12 16 32         [24] 1577 	lcall	___fssub
      0004CE AC 82            [24] 1578 	mov	r4,dpl
      0004D0 AD 83            [24] 1579 	mov	r5,dph
      0004D2 AE F0            [24] 1580 	mov	r6,b
      0004D4 FF               [12] 1581 	mov	r7,a
      0004D5 E5 81            [12] 1582 	mov	a,sp
      0004D7 24 FC            [12] 1583 	add	a,#0xfc
      0004D9 F5 81            [12] 1584 	mov	sp,a
      0004DB 8C 82            [24] 1585 	mov	dpl,r4
      0004DD 8D 83            [24] 1586 	mov	dph,r5
      0004DF 8E F0            [24] 1587 	mov	b,r6
      0004E1 EF               [12] 1588 	mov	a,r7
      0004E2 12 1D AA         [24] 1589 	lcall	___fs2sint
      0004E5 AF 82            [24] 1590 	mov	r7,dpl
      0004E7 AE 83            [24] 1591 	mov	r6,dph
      0004E9 E5 16            [12] 1592 	mov	a,_bp
      0004EB 24 09            [12] 1593 	add	a,#0x09
      0004ED F8               [12] 1594 	mov	r0,a
      0004EE 86 82            [24] 1595 	mov	dpl,@r0
      0004F0 08               [12] 1596 	inc	r0
      0004F1 86 83            [24] 1597 	mov	dph,@r0
      0004F3 08               [12] 1598 	inc	r0
      0004F4 86 F0            [24] 1599 	mov	b,@r0
      0004F6 EF               [12] 1600 	mov	a,r7
      0004F7 12 1B 01         [24] 1601 	lcall	__gptrput
      0004FA A3               [24] 1602 	inc	dptr
      0004FB EE               [12] 1603 	mov	a,r6
      0004FC 12 1B 01         [24] 1604 	lcall	__gptrput
                                   1605 ;	usr/main.c:86: radar->x3Pos = (((int)buf[18]<<8)+buf[17]);
      0004FF E5 16            [12] 1606 	mov	a,_bp
      000501 24 0C            [12] 1607 	add	a,#0x0c
      000503 F8               [12] 1608 	mov	r0,a
      000504 74 10            [12] 1609 	mov	a,#0x10
      000506 26               [12] 1610 	add	a,@r0
      000507 FD               [12] 1611 	mov	r5,a
      000508 E4               [12] 1612 	clr	a
      000509 08               [12] 1613 	inc	r0
      00050A 36               [12] 1614 	addc	a,@r0
      00050B FE               [12] 1615 	mov	r6,a
      00050C 08               [12] 1616 	inc	r0
      00050D 86 07            [24] 1617 	mov	ar7,@r0
      00050F A8 16            [24] 1618 	mov	r0,_bp
      000511 08               [12] 1619 	inc	r0
      000512 74 12            [12] 1620 	mov	a,#0x12
      000514 26               [12] 1621 	add	a,@r0
      000515 FA               [12] 1622 	mov	r2,a
      000516 E4               [12] 1623 	clr	a
      000517 08               [12] 1624 	inc	r0
      000518 36               [12] 1625 	addc	a,@r0
      000519 FB               [12] 1626 	mov	r3,a
      00051A 08               [12] 1627 	inc	r0
      00051B 86 04            [24] 1628 	mov	ar4,@r0
      00051D 8A 82            [24] 1629 	mov	dpl,r2
      00051F 8B 83            [24] 1630 	mov	dph,r3
      000521 8C F0            [24] 1631 	mov	b,r4
      000523 12 1F A0         [24] 1632 	lcall	__gptrget
      000526 FA               [12] 1633 	mov	r2,a
      000527 E5 16            [12] 1634 	mov	a,_bp
      000529 24 09            [12] 1635 	add	a,#0x09
      00052B F8               [12] 1636 	mov	r0,a
      00052C 08               [12] 1637 	inc	r0
      00052D A6 02            [24] 1638 	mov	@r0,ar2
      00052F 18               [12] 1639 	dec	r0
      000530 76 00            [12] 1640 	mov	@r0,#0x00
      000532 A8 16            [24] 1641 	mov	r0,_bp
      000534 08               [12] 1642 	inc	r0
      000535 74 11            [12] 1643 	mov	a,#0x11
      000537 26               [12] 1644 	add	a,@r0
      000538 FA               [12] 1645 	mov	r2,a
      000539 E4               [12] 1646 	clr	a
      00053A 08               [12] 1647 	inc	r0
      00053B 36               [12] 1648 	addc	a,@r0
      00053C FB               [12] 1649 	mov	r3,a
      00053D 08               [12] 1650 	inc	r0
      00053E 86 04            [24] 1651 	mov	ar4,@r0
      000540 8A 82            [24] 1652 	mov	dpl,r2
      000542 8B 83            [24] 1653 	mov	dph,r3
      000544 8C F0            [24] 1654 	mov	b,r4
      000546 12 1F A0         [24] 1655 	lcall	__gptrget
      000549 FA               [12] 1656 	mov	r2,a
      00054A 7C 00            [12] 1657 	mov	r4,#0x00
      00054C E5 16            [12] 1658 	mov	a,_bp
      00054E 24 09            [12] 1659 	add	a,#0x09
      000550 F8               [12] 1660 	mov	r0,a
      000551 EA               [12] 1661 	mov	a,r2
      000552 26               [12] 1662 	add	a,@r0
      000553 FA               [12] 1663 	mov	r2,a
      000554 EC               [12] 1664 	mov	a,r4
      000555 08               [12] 1665 	inc	r0
      000556 36               [12] 1666 	addc	a,@r0
      000557 FC               [12] 1667 	mov	r4,a
      000558 8D 82            [24] 1668 	mov	dpl,r5
      00055A 8E 83            [24] 1669 	mov	dph,r6
      00055C 8F F0            [24] 1670 	mov	b,r7
      00055E EA               [12] 1671 	mov	a,r2
      00055F 12 1B 01         [24] 1672 	lcall	__gptrput
      000562 A3               [24] 1673 	inc	dptr
      000563 EC               [12] 1674 	mov	a,r4
      000564 12 1B 01         [24] 1675 	lcall	__gptrput
                                   1676 ;	usr/main.c:87: radar->y3Pos = (((int)buf[20]<<8)+buf[19]);
      000567 E5 16            [12] 1677 	mov	a,_bp
      000569 24 0C            [12] 1678 	add	a,#0x0c
      00056B F8               [12] 1679 	mov	r0,a
      00056C 74 12            [12] 1680 	mov	a,#0x12
      00056E 26               [12] 1681 	add	a,@r0
      00056F FD               [12] 1682 	mov	r5,a
      000570 E4               [12] 1683 	clr	a
      000571 08               [12] 1684 	inc	r0
      000572 36               [12] 1685 	addc	a,@r0
      000573 FE               [12] 1686 	mov	r6,a
      000574 08               [12] 1687 	inc	r0
      000575 86 07            [24] 1688 	mov	ar7,@r0
      000577 A8 16            [24] 1689 	mov	r0,_bp
      000579 08               [12] 1690 	inc	r0
      00057A 74 14            [12] 1691 	mov	a,#0x14
      00057C 26               [12] 1692 	add	a,@r0
      00057D FA               [12] 1693 	mov	r2,a
      00057E E4               [12] 1694 	clr	a
      00057F 08               [12] 1695 	inc	r0
      000580 36               [12] 1696 	addc	a,@r0
      000581 FB               [12] 1697 	mov	r3,a
      000582 08               [12] 1698 	inc	r0
      000583 86 04            [24] 1699 	mov	ar4,@r0
      000585 8A 82            [24] 1700 	mov	dpl,r2
      000587 8B 83            [24] 1701 	mov	dph,r3
      000589 8C F0            [24] 1702 	mov	b,r4
      00058B 12 1F A0         [24] 1703 	lcall	__gptrget
      00058E FA               [12] 1704 	mov	r2,a
      00058F E5 16            [12] 1705 	mov	a,_bp
      000591 24 09            [12] 1706 	add	a,#0x09
      000593 F8               [12] 1707 	mov	r0,a
      000594 08               [12] 1708 	inc	r0
      000595 A6 02            [24] 1709 	mov	@r0,ar2
      000597 18               [12] 1710 	dec	r0
      000598 76 00            [12] 1711 	mov	@r0,#0x00
      00059A A8 16            [24] 1712 	mov	r0,_bp
      00059C 08               [12] 1713 	inc	r0
      00059D 74 13            [12] 1714 	mov	a,#0x13
      00059F 26               [12] 1715 	add	a,@r0
      0005A0 FA               [12] 1716 	mov	r2,a
      0005A1 E4               [12] 1717 	clr	a
      0005A2 08               [12] 1718 	inc	r0
      0005A3 36               [12] 1719 	addc	a,@r0
      0005A4 FB               [12] 1720 	mov	r3,a
      0005A5 08               [12] 1721 	inc	r0
      0005A6 86 04            [24] 1722 	mov	ar4,@r0
      0005A8 8A 82            [24] 1723 	mov	dpl,r2
      0005AA 8B 83            [24] 1724 	mov	dph,r3
      0005AC 8C F0            [24] 1725 	mov	b,r4
      0005AE 12 1F A0         [24] 1726 	lcall	__gptrget
      0005B1 FA               [12] 1727 	mov	r2,a
      0005B2 7C 00            [12] 1728 	mov	r4,#0x00
      0005B4 E5 16            [12] 1729 	mov	a,_bp
      0005B6 24 09            [12] 1730 	add	a,#0x09
      0005B8 F8               [12] 1731 	mov	r0,a
      0005B9 EA               [12] 1732 	mov	a,r2
      0005BA 26               [12] 1733 	add	a,@r0
      0005BB FA               [12] 1734 	mov	r2,a
      0005BC EC               [12] 1735 	mov	a,r4
      0005BD 08               [12] 1736 	inc	r0
      0005BE 36               [12] 1737 	addc	a,@r0
      0005BF FC               [12] 1738 	mov	r4,a
      0005C0 8D 82            [24] 1739 	mov	dpl,r5
      0005C2 8E 83            [24] 1740 	mov	dph,r6
      0005C4 8F F0            [24] 1741 	mov	b,r7
      0005C6 EA               [12] 1742 	mov	a,r2
      0005C7 12 1B 01         [24] 1743 	lcall	__gptrput
      0005CA A3               [24] 1744 	inc	dptr
      0005CB EC               [12] 1745 	mov	a,r4
      0005CC 12 1B 01         [24] 1746 	lcall	__gptrput
                                   1747 ;	usr/main.c:88: radar->dist3 = (((int)buf[22]<<8)+buf[21]);
      0005CF E5 16            [12] 1748 	mov	a,_bp
      0005D1 24 0C            [12] 1749 	add	a,#0x0c
      0005D3 F8               [12] 1750 	mov	r0,a
      0005D4 74 14            [12] 1751 	mov	a,#0x14
      0005D6 26               [12] 1752 	add	a,@r0
      0005D7 FD               [12] 1753 	mov	r5,a
      0005D8 E4               [12] 1754 	clr	a
      0005D9 08               [12] 1755 	inc	r0
      0005DA 36               [12] 1756 	addc	a,@r0
      0005DB FE               [12] 1757 	mov	r6,a
      0005DC 08               [12] 1758 	inc	r0
      0005DD 86 07            [24] 1759 	mov	ar7,@r0
      0005DF A8 16            [24] 1760 	mov	r0,_bp
      0005E1 08               [12] 1761 	inc	r0
      0005E2 74 16            [12] 1762 	mov	a,#0x16
      0005E4 26               [12] 1763 	add	a,@r0
      0005E5 FA               [12] 1764 	mov	r2,a
      0005E6 E4               [12] 1765 	clr	a
      0005E7 08               [12] 1766 	inc	r0
      0005E8 36               [12] 1767 	addc	a,@r0
      0005E9 FB               [12] 1768 	mov	r3,a
      0005EA 08               [12] 1769 	inc	r0
      0005EB 86 04            [24] 1770 	mov	ar4,@r0
      0005ED 8A 82            [24] 1771 	mov	dpl,r2
      0005EF 8B 83            [24] 1772 	mov	dph,r3
      0005F1 8C F0            [24] 1773 	mov	b,r4
      0005F3 12 1F A0         [24] 1774 	lcall	__gptrget
      0005F6 FA               [12] 1775 	mov	r2,a
      0005F7 E5 16            [12] 1776 	mov	a,_bp
      0005F9 24 09            [12] 1777 	add	a,#0x09
      0005FB F8               [12] 1778 	mov	r0,a
      0005FC 08               [12] 1779 	inc	r0
      0005FD A6 02            [24] 1780 	mov	@r0,ar2
      0005FF 18               [12] 1781 	dec	r0
      000600 76 00            [12] 1782 	mov	@r0,#0x00
      000602 A8 16            [24] 1783 	mov	r0,_bp
      000604 08               [12] 1784 	inc	r0
      000605 74 15            [12] 1785 	mov	a,#0x15
      000607 26               [12] 1786 	add	a,@r0
      000608 FA               [12] 1787 	mov	r2,a
      000609 E4               [12] 1788 	clr	a
      00060A 08               [12] 1789 	inc	r0
      00060B 36               [12] 1790 	addc	a,@r0
      00060C FB               [12] 1791 	mov	r3,a
      00060D 08               [12] 1792 	inc	r0
      00060E 86 04            [24] 1793 	mov	ar4,@r0
      000610 8A 82            [24] 1794 	mov	dpl,r2
      000612 8B 83            [24] 1795 	mov	dph,r3
      000614 8C F0            [24] 1796 	mov	b,r4
      000616 12 1F A0         [24] 1797 	lcall	__gptrget
      000619 FA               [12] 1798 	mov	r2,a
      00061A 7C 00            [12] 1799 	mov	r4,#0x00
      00061C E5 16            [12] 1800 	mov	a,_bp
      00061E 24 09            [12] 1801 	add	a,#0x09
      000620 F8               [12] 1802 	mov	r0,a
      000621 EA               [12] 1803 	mov	a,r2
      000622 26               [12] 1804 	add	a,@r0
      000623 FA               [12] 1805 	mov	r2,a
      000624 EC               [12] 1806 	mov	a,r4
      000625 08               [12] 1807 	inc	r0
      000626 36               [12] 1808 	addc	a,@r0
      000627 FC               [12] 1809 	mov	r4,a
      000628 8D 82            [24] 1810 	mov	dpl,r5
      00062A 8E 83            [24] 1811 	mov	dph,r6
      00062C 8F F0            [24] 1812 	mov	b,r7
      00062E EA               [12] 1813 	mov	a,r2
      00062F 12 1B 01         [24] 1814 	lcall	__gptrput
      000632 A3               [24] 1815 	inc	dptr
      000633 EC               [12] 1816 	mov	a,r4
      000634 12 1B 01         [24] 1817 	lcall	__gptrput
                                   1818 ;	usr/main.c:89: radar->ang3 = (((int)buf[24]<<8)+buf[23]);
      000637 E5 16            [12] 1819 	mov	a,_bp
      000639 24 0C            [12] 1820 	add	a,#0x0c
      00063B F8               [12] 1821 	mov	r0,a
      00063C 74 16            [12] 1822 	mov	a,#0x16
      00063E 26               [12] 1823 	add	a,@r0
      00063F F6               [12] 1824 	mov	@r0,a
      000640 E4               [12] 1825 	clr	a
      000641 08               [12] 1826 	inc	r0
      000642 36               [12] 1827 	addc	a,@r0
      000643 F6               [12] 1828 	mov	@r0,a
      000644 A8 16            [24] 1829 	mov	r0,_bp
      000646 08               [12] 1830 	inc	r0
      000647 74 18            [12] 1831 	mov	a,#0x18
      000649 26               [12] 1832 	add	a,@r0
      00064A FA               [12] 1833 	mov	r2,a
      00064B E4               [12] 1834 	clr	a
      00064C 08               [12] 1835 	inc	r0
      00064D 36               [12] 1836 	addc	a,@r0
      00064E FB               [12] 1837 	mov	r3,a
      00064F 08               [12] 1838 	inc	r0
      000650 86 04            [24] 1839 	mov	ar4,@r0
      000652 8A 82            [24] 1840 	mov	dpl,r2
      000654 8B 83            [24] 1841 	mov	dph,r3
      000656 8C F0            [24] 1842 	mov	b,r4
      000658 12 1F A0         [24] 1843 	lcall	__gptrget
      00065B FE               [12] 1844 	mov	r6,a
      00065C 7F 00            [12] 1845 	mov	r7,#0x00
      00065E A8 16            [24] 1846 	mov	r0,_bp
      000660 08               [12] 1847 	inc	r0
      000661 74 17            [12] 1848 	mov	a,#0x17
      000663 26               [12] 1849 	add	a,@r0
      000664 FA               [12] 1850 	mov	r2,a
      000665 E4               [12] 1851 	clr	a
      000666 08               [12] 1852 	inc	r0
      000667 36               [12] 1853 	addc	a,@r0
      000668 FB               [12] 1854 	mov	r3,a
      000669 08               [12] 1855 	inc	r0
      00066A 86 04            [24] 1856 	mov	ar4,@r0
      00066C 8A 82            [24] 1857 	mov	dpl,r2
      00066E 8B 83            [24] 1858 	mov	dph,r3
      000670 8C F0            [24] 1859 	mov	b,r4
      000672 12 1F A0         [24] 1860 	lcall	__gptrget
      000675 7C 00            [12] 1861 	mov	r4,#0x00
      000677 2F               [12] 1862 	add	a,r7
      000678 FA               [12] 1863 	mov	r2,a
      000679 EC               [12] 1864 	mov	a,r4
      00067A 3E               [12] 1865 	addc	a,r6
      00067B FC               [12] 1866 	mov	r4,a
      00067C E5 16            [12] 1867 	mov	a,_bp
      00067E 24 0C            [12] 1868 	add	a,#0x0c
      000680 F8               [12] 1869 	mov	r0,a
      000681 86 82            [24] 1870 	mov	dpl,@r0
      000683 08               [12] 1871 	inc	r0
      000684 86 83            [24] 1872 	mov	dph,@r0
      000686 08               [12] 1873 	inc	r0
      000687 86 F0            [24] 1874 	mov	b,@r0
      000689 EA               [12] 1875 	mov	a,r2
      00068A 12 1B 01         [24] 1876 	lcall	__gptrput
      00068D A3               [24] 1877 	inc	dptr
      00068E EC               [12] 1878 	mov	a,r4
      00068F 12 1B 01         [24] 1879 	lcall	__gptrput
                                   1880 ;	usr/main.c:94: radar->ang3 = radar->ang3 / 10.0 - 90;
      000692 8A 82            [24] 1881 	mov	dpl,r2
      000694 8C 83            [24] 1882 	mov	dph,r4
      000696 12 1D 9D         [24] 1883 	lcall	___sint2fs
      000699 AA 82            [24] 1884 	mov	r2,dpl
      00069B AB 83            [24] 1885 	mov	r3,dph
      00069D AC F0            [24] 1886 	mov	r4,b
      00069F FF               [12] 1887 	mov	r7,a
      0006A0 E4               [12] 1888 	clr	a
      0006A1 C0 E0            [24] 1889 	push	acc
      0006A3 C0 E0            [24] 1890 	push	acc
      0006A5 74 20            [12] 1891 	mov	a,#0x20
      0006A7 C0 E0            [24] 1892 	push	acc
      0006A9 74 41            [12] 1893 	mov	a,#0x41
      0006AB C0 E0            [24] 1894 	push	acc
      0006AD 8A 82            [24] 1895 	mov	dpl,r2
      0006AF 8B 83            [24] 1896 	mov	dph,r3
      0006B1 8C F0            [24] 1897 	mov	b,r4
      0006B3 EF               [12] 1898 	mov	a,r7
      0006B4 12 1E DD         [24] 1899 	lcall	___fsdiv
      0006B7 AC 82            [24] 1900 	mov	r4,dpl
      0006B9 AD 83            [24] 1901 	mov	r5,dph
      0006BB AE F0            [24] 1902 	mov	r6,b
      0006BD FF               [12] 1903 	mov	r7,a
      0006BE E5 81            [12] 1904 	mov	a,sp
      0006C0 24 FC            [12] 1905 	add	a,#0xfc
      0006C2 F5 81            [12] 1906 	mov	sp,a
      0006C4 E4               [12] 1907 	clr	a
      0006C5 C0 E0            [24] 1908 	push	acc
      0006C7 C0 E0            [24] 1909 	push	acc
      0006C9 74 B4            [12] 1910 	mov	a,#0xb4
      0006CB C0 E0            [24] 1911 	push	acc
      0006CD 74 42            [12] 1912 	mov	a,#0x42
      0006CF C0 E0            [24] 1913 	push	acc
      0006D1 8C 82            [24] 1914 	mov	dpl,r4
      0006D3 8D 83            [24] 1915 	mov	dph,r5
      0006D5 8E F0            [24] 1916 	mov	b,r6
      0006D7 EF               [12] 1917 	mov	a,r7
      0006D8 12 16 32         [24] 1918 	lcall	___fssub
      0006DB AC 82            [24] 1919 	mov	r4,dpl
      0006DD AD 83            [24] 1920 	mov	r5,dph
      0006DF AE F0            [24] 1921 	mov	r6,b
      0006E1 FF               [12] 1922 	mov	r7,a
      0006E2 E5 81            [12] 1923 	mov	a,sp
      0006E4 24 FC            [12] 1924 	add	a,#0xfc
      0006E6 F5 81            [12] 1925 	mov	sp,a
      0006E8 8C 82            [24] 1926 	mov	dpl,r4
      0006EA 8D 83            [24] 1927 	mov	dph,r5
      0006EC 8E F0            [24] 1928 	mov	b,r6
      0006EE EF               [12] 1929 	mov	a,r7
      0006EF 12 1D AA         [24] 1930 	lcall	___fs2sint
      0006F2 AF 82            [24] 1931 	mov	r7,dpl
      0006F4 AE 83            [24] 1932 	mov	r6,dph
      0006F6 E5 16            [12] 1933 	mov	a,_bp
      0006F8 24 0C            [12] 1934 	add	a,#0x0c
      0006FA F8               [12] 1935 	mov	r0,a
      0006FB 86 82            [24] 1936 	mov	dpl,@r0
      0006FD 08               [12] 1937 	inc	r0
      0006FE 86 83            [24] 1938 	mov	dph,@r0
      000700 08               [12] 1939 	inc	r0
      000701 86 F0            [24] 1940 	mov	b,@r0
      000703 EF               [12] 1941 	mov	a,r7
      000704 12 1B 01         [24] 1942 	lcall	__gptrput
      000707 A3               [24] 1943 	inc	dptr
      000708 EE               [12] 1944 	mov	a,r6
      000709 12 1B 01         [24] 1945 	lcall	__gptrput
                                   1946 ;	usr/main.c:96: return 0;
      00070C 75 82 00         [24] 1947 	mov	dpl,#0x00
                                   1948 ;	usr/main.c:97: }
      00070F 85 16 81         [24] 1949 	mov	sp,_bp
      000712 D0 16            [24] 1950 	pop	_bp
      000714 22               [24] 1951 	ret
                                   1952 ;------------------------------------------------------------
                                   1953 ;Allocation info for local variables in function 'LightTurn'
                                   1954 ;------------------------------------------------------------
                                   1955 ;Deg                       Allocated to registers r6 r7 
                                   1956 ;Dir                       Allocated to registers b0 
                                   1957 ;STEP                      Allocated to registers r4 r5 
                                   1958 ;i                         Allocated to stack - _bp +1
                                   1959 ;------------------------------------------------------------
                                   1960 ;	usr/main.c:99: void LightTurn(BOOL Dir, UINT16 Deg)
                                   1961 ;	-----------------------------------------
                                   1962 ;	 function LightTurn
                                   1963 ;	-----------------------------------------
      000715                       1964 _LightTurn:
      000715 C0 16            [24] 1965 	push	_bp
      000717 85 81 16         [24] 1966 	mov	_bp,sp
      00071A 05 81            [12] 1967 	inc	sp
      00071C 05 81            [12] 1968 	inc	sp
      00071E AE 82            [24] 1969 	mov	r6,dpl
      000720 AF 83            [24] 1970 	mov	r7,dph
                                   1971 ;	usr/main.c:101: GPIO_Init(PORT0,PIN6,MODE1);
      000722 C0 07            [24] 1972 	push	ar7
      000724 C0 06            [24] 1973 	push	ar6
      000726 C0 21            [24] 1974 	push	bits
      000728 74 01            [12] 1975 	mov	a,#0x01
      00072A C0 E0            [24] 1976 	push	acc
      00072C 74 40            [12] 1977 	mov	a,#0x40
      00072E C0 E0            [24] 1978 	push	acc
      000730 75 82 00         [24] 1979 	mov	dpl,#0x00
      000733 12 13 7C         [24] 1980 	lcall	_GPIO_Init
      000736 15 81            [12] 1981 	dec	sp
      000738 15 81            [12] 1982 	dec	sp
      00073A D0 21            [24] 1983 	pop	bits
      00073C D0 06            [24] 1984 	pop	ar6
      00073E D0 07            [24] 1985 	pop	ar7
                                   1986 ;	usr/main.c:103: STEP = Deg * 5 * 8 / 1.8;
      000740 C0 07            [24] 1987 	push	ar7
      000742 C0 06            [24] 1988 	push	ar6
      000744 C0 21            [24] 1989 	push	bits
      000746 C0 06            [24] 1990 	push	ar6
      000748 C0 07            [24] 1991 	push	ar7
      00074A 90 00 28         [24] 1992 	mov	dptr,#0x0028
      00074D 12 1B 1C         [24] 1993 	lcall	__mulint
      000750 AC 82            [24] 1994 	mov	r4,dpl
      000752 AD 83            [24] 1995 	mov	r5,dph
      000754 15 81            [12] 1996 	dec	sp
      000756 15 81            [12] 1997 	dec	sp
      000758 D0 21            [24] 1998 	pop	bits
      00075A 8C 82            [24] 1999 	mov	dpl,r4
      00075C 8D 83            [24] 2000 	mov	dph,r5
      00075E C0 21            [24] 2001 	push	bits
      000760 12 1D DE         [24] 2002 	lcall	___uint2fs
      000763 AA 82            [24] 2003 	mov	r2,dpl
      000765 AB 83            [24] 2004 	mov	r3,dph
      000767 AC F0            [24] 2005 	mov	r4,b
      000769 FD               [12] 2006 	mov	r5,a
      00076A D0 21            [24] 2007 	pop	bits
      00076C C0 21            [24] 2008 	push	bits
      00076E 74 66            [12] 2009 	mov	a,#0x66
      000770 C0 E0            [24] 2010 	push	acc
      000772 C0 E0            [24] 2011 	push	acc
      000774 74 E6            [12] 2012 	mov	a,#0xe6
      000776 C0 E0            [24] 2013 	push	acc
      000778 74 3F            [12] 2014 	mov	a,#0x3f
      00077A C0 E0            [24] 2015 	push	acc
      00077C 8A 82            [24] 2016 	mov	dpl,r2
      00077E 8B 83            [24] 2017 	mov	dph,r3
      000780 8C F0            [24] 2018 	mov	b,r4
      000782 ED               [12] 2019 	mov	a,r5
      000783 12 1E DD         [24] 2020 	lcall	___fsdiv
      000786 AA 82            [24] 2021 	mov	r2,dpl
      000788 AB 83            [24] 2022 	mov	r3,dph
      00078A AC F0            [24] 2023 	mov	r4,b
      00078C FD               [12] 2024 	mov	r5,a
      00078D E5 81            [12] 2025 	mov	a,sp
      00078F 24 FC            [12] 2026 	add	a,#0xfc
      000791 F5 81            [12] 2027 	mov	sp,a
      000793 D0 21            [24] 2028 	pop	bits
      000795 8A 82            [24] 2029 	mov	dpl,r2
      000797 8B 83            [24] 2030 	mov	dph,r3
      000799 8C F0            [24] 2031 	mov	b,r4
      00079B ED               [12] 2032 	mov	a,r5
      00079C C0 21            [24] 2033 	push	bits
      00079E 12 1D EA         [24] 2034 	lcall	___fs2uint
      0007A1 AC 82            [24] 2035 	mov	r4,dpl
      0007A3 AD 83            [24] 2036 	mov	r5,dph
      0007A5 D0 21            [24] 2037 	pop	bits
                                   2038 ;	usr/main.c:104: Direction = Dir;
                                   2039 ;	assignBit
      0007A7 A2 08            [12] 2040 	mov	c,b0
      0007A9 92 86            [24] 2041 	mov	_P0_6,c
                                   2042 ;	usr/main.c:105: setCursor(0,4);
      0007AB C0 05            [24] 2043 	push	ar5
      0007AD C0 04            [24] 2044 	push	ar4
      0007AF C0 21            [24] 2045 	push	bits
      0007B1 74 04            [12] 2046 	mov	a,#0x04
      0007B3 C0 E0            [24] 2047 	push	acc
      0007B5 E4               [12] 2048 	clr	a
      0007B6 C0 E0            [24] 2049 	push	acc
      0007B8 90 00 00         [24] 2050 	mov	dptr,#0x0000
      0007BB 12 0C 03         [24] 2051 	lcall	_setCursor
      0007BE 15 81            [12] 2052 	dec	sp
      0007C0 15 81            [12] 2053 	dec	sp
      0007C2 D0 21            [24] 2054 	pop	bits
      0007C4 D0 04            [24] 2055 	pop	ar4
      0007C6 D0 05            [24] 2056 	pop	ar5
      0007C8 D0 06            [24] 2057 	pop	ar6
      0007CA D0 07            [24] 2058 	pop	ar7
                                   2059 ;	usr/main.c:106: printf_fast_f("D:%d S:%3d G:%2d",Dir,STEP,Deg);
      0007CC A2 08            [12] 2060 	mov	c,b0
      0007CE E4               [12] 2061 	clr	a
      0007CF 33               [12] 2062 	rlc	a
      0007D0 FA               [12] 2063 	mov	r2,a
      0007D1 7B 00            [12] 2064 	mov	r3,#0x00
      0007D3 C0 05            [24] 2065 	push	ar5
      0007D5 C0 04            [24] 2066 	push	ar4
      0007D7 C0 06            [24] 2067 	push	ar6
      0007D9 C0 07            [24] 2068 	push	ar7
      0007DB C0 04            [24] 2069 	push	ar4
      0007DD C0 05            [24] 2070 	push	ar5
      0007DF C0 02            [24] 2071 	push	ar2
      0007E1 C0 03            [24] 2072 	push	ar3
      0007E3 74 94            [12] 2073 	mov	a,#___str_0
      0007E5 C0 E0            [24] 2074 	push	acc
      0007E7 74 34            [12] 2075 	mov	a,#(___str_0 >> 8)
      0007E9 C0 E0            [24] 2076 	push	acc
      0007EB 12 16 3D         [24] 2077 	lcall	_printf_fast_f
      0007EE E5 81            [12] 2078 	mov	a,sp
      0007F0 24 F8            [12] 2079 	add	a,#0xf8
      0007F2 F5 81            [12] 2080 	mov	sp,a
      0007F4 D0 04            [24] 2081 	pop	ar4
      0007F6 D0 05            [24] 2082 	pop	ar5
                                   2083 ;	usr/main.c:107: for (UINT16 i = 0; i < STEP; i++)
      0007F8 A8 16            [24] 2084 	mov	r0,_bp
      0007FA 08               [12] 2085 	inc	r0
      0007FB E4               [12] 2086 	clr	a
      0007FC F6               [12] 2087 	mov	@r0,a
      0007FD 08               [12] 2088 	inc	r0
      0007FE F6               [12] 2089 	mov	@r0,a
      0007FF                       2090 00103$:
      0007FF A8 16            [24] 2091 	mov	r0,_bp
      000801 08               [12] 2092 	inc	r0
      000802 C3               [12] 2093 	clr	c
      000803 E6               [12] 2094 	mov	a,@r0
      000804 9C               [12] 2095 	subb	a,r4
      000805 08               [12] 2096 	inc	r0
      000806 E6               [12] 2097 	mov	a,@r0
      000807 9D               [12] 2098 	subb	a,r5
      000808 50 51            [24] 2099 	jnc	00105$
                                   2100 ;	usr/main.c:109: Steps = 1;
                                   2101 ;	assignBit
      00080A D2 87            [12] 2102 	setb	_P0_7
                                   2103 ;	usr/main.c:110: mDelayuS(1000000/STEP);
      00080C 8C 02            [24] 2104 	mov	ar2,r4
      00080E 8D 03            [24] 2105 	mov	ar3,r5
      000810 7E 00            [12] 2106 	mov	r6,#0x00
      000812 7F 00            [12] 2107 	mov	r7,#0x00
      000814 C0 05            [24] 2108 	push	ar5
      000816 C0 04            [24] 2109 	push	ar4
      000818 C0 02            [24] 2110 	push	ar2
      00081A C0 03            [24] 2111 	push	ar3
      00081C C0 06            [24] 2112 	push	ar6
      00081E C0 07            [24] 2113 	push	ar7
      000820 90 42 40         [24] 2114 	mov	dptr,#0x4240
      000823 75 F0 0F         [24] 2115 	mov	b,#0x0f
      000826 E4               [12] 2116 	clr	a
      000827 12 1C 13         [24] 2117 	lcall	__divslong
      00082A AA 82            [24] 2118 	mov	r2,dpl
      00082C AB 83            [24] 2119 	mov	r3,dph
      00082E E5 81            [12] 2120 	mov	a,sp
      000830 24 FC            [12] 2121 	add	a,#0xfc
      000832 F5 81            [12] 2122 	mov	sp,a
      000834 8A 82            [24] 2123 	mov	dpl,r2
      000836 8B 83            [24] 2124 	mov	dph,r3
      000838 C0 03            [24] 2125 	push	ar3
      00083A C0 02            [24] 2126 	push	ar2
      00083C 12 0C D1         [24] 2127 	lcall	_mDelayuS
      00083F D0 02            [24] 2128 	pop	ar2
      000841 D0 03            [24] 2129 	pop	ar3
                                   2130 ;	usr/main.c:111: Steps = 0;
                                   2131 ;	assignBit
      000843 C2 87            [12] 2132 	clr	_P0_7
                                   2133 ;	usr/main.c:112: mDelayuS(1000000/STEP);
      000845 8A 82            [24] 2134 	mov	dpl,r2
      000847 8B 83            [24] 2135 	mov	dph,r3
      000849 12 0C D1         [24] 2136 	lcall	_mDelayuS
      00084C D0 04            [24] 2137 	pop	ar4
      00084E D0 05            [24] 2138 	pop	ar5
                                   2139 ;	usr/main.c:107: for (UINT16 i = 0; i < STEP; i++)
      000850 A8 16            [24] 2140 	mov	r0,_bp
      000852 08               [12] 2141 	inc	r0
      000853 06               [12] 2142 	inc	@r0
      000854 B6 00 02         [24] 2143 	cjne	@r0,#0x00,00117$
      000857 08               [12] 2144 	inc	r0
      000858 06               [12] 2145 	inc	@r0
      000859                       2146 00117$:
      000859 80 A4            [24] 2147 	sjmp	00103$
      00085B                       2148 00105$:
                                   2149 ;	usr/main.c:115: }
      00085B 85 16 81         [24] 2150 	mov	sp,_bp
      00085E D0 16            [24] 2151 	pop	_bp
      000860 22               [24] 2152 	ret
                                   2153 ;------------------------------------------------------------
                                   2154 ;Allocation info for local variables in function 'FaceTrack'
                                   2155 ;------------------------------------------------------------
                                   2156 ;A                         Allocated to registers r6 r7 
                                   2157 ;------------------------------------------------------------
                                   2158 ;	usr/main.c:117: void FaceTrack( int A)
                                   2159 ;	-----------------------------------------
                                   2160 ;	 function FaceTrack
                                   2161 ;	-----------------------------------------
      000861                       2162 _FaceTrack:
      000861 AE 82            [24] 2163 	mov	r6,dpl
      000863 AF 83            [24] 2164 	mov	r7,dph
                                   2165 ;	usr/main.c:119: if ( A < 85)
      000865 C3               [12] 2166 	clr	c
      000866 EE               [12] 2167 	mov	a,r6
      000867 94 55            [12] 2168 	subb	a,#0x55
      000869 EF               [12] 2169 	mov	a,r7
      00086A 64 80            [12] 2170 	xrl	a,#0x80
      00086C 94 80            [12] 2171 	subb	a,#0x80
      00086E 50 51            [24] 2172 	jnc	00102$
                                   2173 ;	usr/main.c:121: LightTurn(Counterclockwise, 1+(90-A)*5/9);
      000870 74 5A            [12] 2174 	mov	a,#0x5a
      000872 C3               [12] 2175 	clr	c
      000873 9E               [12] 2176 	subb	a,r6
      000874 FC               [12] 2177 	mov	r4,a
      000875 E4               [12] 2178 	clr	a
      000876 9F               [12] 2179 	subb	a,r7
      000877 FD               [12] 2180 	mov	r5,a
      000878 C0 07            [24] 2181 	push	ar7
      00087A C0 06            [24] 2182 	push	ar6
      00087C C0 04            [24] 2183 	push	ar4
      00087E C0 05            [24] 2184 	push	ar5
      000880 90 00 05         [24] 2185 	mov	dptr,#0x0005
      000883 12 1B 1C         [24] 2186 	lcall	__mulint
      000886 AC 82            [24] 2187 	mov	r4,dpl
      000888 AD 83            [24] 2188 	mov	r5,dph
      00088A 15 81            [12] 2189 	dec	sp
      00088C 15 81            [12] 2190 	dec	sp
      00088E 74 09            [12] 2191 	mov	a,#0x09
      000890 C0 E0            [24] 2192 	push	acc
      000892 E4               [12] 2193 	clr	a
      000893 C0 E0            [24] 2194 	push	acc
      000895 8C 82            [24] 2195 	mov	dpl,r4
      000897 8D 83            [24] 2196 	mov	dph,r5
      000899 12 20 E9         [24] 2197 	lcall	__divsint
      00089C AC 82            [24] 2198 	mov	r4,dpl
      00089E AD 83            [24] 2199 	mov	r5,dph
      0008A0 15 81            [12] 2200 	dec	sp
      0008A2 15 81            [12] 2201 	dec	sp
      0008A4 D0 06            [24] 2202 	pop	ar6
      0008A6 D0 07            [24] 2203 	pop	ar7
      0008A8 0C               [12] 2204 	inc	r4
      0008A9 BC 00 01         [24] 2205 	cjne	r4,#0x00,00116$
      0008AC 0D               [12] 2206 	inc	r5
      0008AD                       2207 00116$:
      0008AD C2 F0            [12] 2208 	clr	b[0]
      0008AF C0 07            [24] 2209 	push	ar7
      0008B1 C0 06            [24] 2210 	push	ar6
      0008B3 85 F0 21         [24] 2211 	mov	bits,b
      0008B6 8C 82            [24] 2212 	mov	dpl,r4
      0008B8 8D 83            [24] 2213 	mov	dph,r5
      0008BA 12 07 15         [24] 2214 	lcall	_LightTurn
      0008BD D0 06            [24] 2215 	pop	ar6
      0008BF D0 07            [24] 2216 	pop	ar7
      0008C1                       2217 00102$:
                                   2218 ;	usr/main.c:123: if ( A > 95)
      0008C1 C3               [12] 2219 	clr	c
      0008C2 74 5F            [12] 2220 	mov	a,#0x5f
      0008C4 9E               [12] 2221 	subb	a,r6
      0008C5 74 80            [12] 2222 	mov	a,#(0x00 ^ 0x80)
      0008C7 8F F0            [24] 2223 	mov	b,r7
      0008C9 63 F0 80         [24] 2224 	xrl	b,#0x80
      0008CC 95 F0            [12] 2225 	subb	a,b
      0008CE 50 41            [24] 2226 	jnc	00105$
                                   2227 ;	usr/main.c:125: LightTurn(Clockwise, 1+(A-90)*5/9);
      0008D0 EE               [12] 2228 	mov	a,r6
      0008D1 24 A6            [12] 2229 	add	a,#0xa6
      0008D3 FE               [12] 2230 	mov	r6,a
      0008D4 EF               [12] 2231 	mov	a,r7
      0008D5 34 FF            [12] 2232 	addc	a,#0xff
      0008D7 FF               [12] 2233 	mov	r7,a
      0008D8 C0 06            [24] 2234 	push	ar6
      0008DA C0 07            [24] 2235 	push	ar7
      0008DC 90 00 05         [24] 2236 	mov	dptr,#0x0005
      0008DF 12 1B 1C         [24] 2237 	lcall	__mulint
      0008E2 AE 82            [24] 2238 	mov	r6,dpl
      0008E4 AF 83            [24] 2239 	mov	r7,dph
      0008E6 15 81            [12] 2240 	dec	sp
      0008E8 15 81            [12] 2241 	dec	sp
      0008EA 74 09            [12] 2242 	mov	a,#0x09
      0008EC C0 E0            [24] 2243 	push	acc
      0008EE E4               [12] 2244 	clr	a
      0008EF C0 E0            [24] 2245 	push	acc
      0008F1 8E 82            [24] 2246 	mov	dpl,r6
      0008F3 8F 83            [24] 2247 	mov	dph,r7
      0008F5 12 20 E9         [24] 2248 	lcall	__divsint
      0008F8 AE 82            [24] 2249 	mov	r6,dpl
      0008FA AF 83            [24] 2250 	mov	r7,dph
      0008FC 15 81            [12] 2251 	dec	sp
      0008FE 15 81            [12] 2252 	dec	sp
      000900 0E               [12] 2253 	inc	r6
      000901 BE 00 01         [24] 2254 	cjne	r6,#0x00,00118$
      000904 0F               [12] 2255 	inc	r7
      000905                       2256 00118$:
      000905 D2 F0            [12] 2257 	setb	b[0]
      000907 85 F0 21         [24] 2258 	mov	bits,b
      00090A 8E 82            [24] 2259 	mov	dpl,r6
      00090C 8F 83            [24] 2260 	mov	dph,r7
                                   2261 ;	usr/main.c:128: }
      00090E 02 07 15         [24] 2262 	ljmp	_LightTurn
      000911                       2263 00105$:
      000911 22               [24] 2264 	ret
                                   2265 ;------------------------------------------------------------
                                   2266 ;Allocation info for local variables in function 'main'
                                   2267 ;------------------------------------------------------------
                                   2268 ;object                    Allocated to stack - _bp +1
                                   2269 ;------------------------------------------------------------
                                   2270 ;	usr/main.c:135: void main()
                                   2271 ;	-----------------------------------------
                                   2272 ;	 function main
                                   2273 ;	-----------------------------------------
      000912                       2274 _main:
      000912 C0 16            [24] 2275 	push	_bp
      000914 E5 81            [12] 2276 	mov	a,sp
      000916 F5 16            [12] 2277 	mov	_bp,a
      000918 24 19            [12] 2278 	add	a,#0x19
      00091A F5 81            [12] 2279 	mov	sp,a
                                   2280 ;	usr/main.c:137: CfgFsys( );           //CH549时钟选择配置
      00091C 12 0C B3         [24] 2281 	lcall	_CfgFsys
                                   2282 ;	usr/main.c:138: mDelaymS(20);
      00091F 90 00 14         [24] 2283 	mov	dptr,#0x0014
      000922 12 0D 07         [24] 2284 	lcall	_mDelaymS
                                   2285 ;	usr/main.c:139: UART1Init(115200,1);
      000925 74 01            [12] 2286 	mov	a,#0x01
      000927 C0 E0            [24] 2287 	push	acc
      000929 90 C2 00         [24] 2288 	mov	dptr,#0xc200
      00092C 75 F0 01         [24] 2289 	mov	b,#0x01
      00092F E4               [12] 2290 	clr	a
      000930 12 00 80         [24] 2291 	lcall	_UART1Init
      000933 15 81            [12] 2292 	dec	sp
                                   2293 ;	usr/main.c:142: SPIMasterModeSet(3);  //SPI主机模式设置，模式3
      000935 75 82 03         [24] 2294 	mov	dpl,#0x03
      000938 12 0F 65         [24] 2295 	lcall	_SPIMasterModeSet
                                   2296 ;	usr/main.c:143: SPI_CK_SET(4);       //设置SPI sclk 时钟信号分频
      00093B 75 FB 04         [24] 2297 	mov	_SPI0_CK_SE,#0x04
                                   2298 ;	usr/main.c:144: OLED_Init();		  //初始化OLED  
      00093E 12 10 59         [24] 2299 	lcall	_OLED_Init
                                   2300 ;	usr/main.c:145: OLED_Clear();         //将OLED屏幕上内容清除
      000941 12 10 AD         [24] 2301 	lcall	_OLED_Clear
                                   2302 ;	usr/main.c:146: setFontSize(8);      //设置文字大小
      000944 75 82 08         [24] 2303 	mov	dpl,#0x08
      000947 12 0F BB         [24] 2304 	lcall	_setFontSize
                                   2305 ;	usr/main.c:148: OLED_Clear();
      00094A 12 10 AD         [24] 2306 	lcall	_OLED_Clear
                                   2307 ;	usr/main.c:149: setCursor(0,0);
      00094D E4               [12] 2308 	clr	a
      00094E C0 E0            [24] 2309 	push	acc
      000950 C0 E0            [24] 2310 	push	acc
      000952 90 00 00         [24] 2311 	mov	dptr,#0x0000
      000955 12 0C 03         [24] 2312 	lcall	_setCursor
      000958 15 81            [12] 2313 	dec	sp
      00095A 15 81            [12] 2314 	dec	sp
                                   2315 ;	usr/main.c:150: printf_fast_f("Radar");
      00095C 74 A5            [12] 2316 	mov	a,#___str_1
      00095E C0 E0            [24] 2317 	push	acc
      000960 74 34            [12] 2318 	mov	a,#(___str_1 >> 8)
      000962 C0 E0            [24] 2319 	push	acc
      000964 12 16 3D         [24] 2320 	lcall	_printf_fast_f
      000967 15 81            [12] 2321 	dec	sp
      000969 15 81            [12] 2322 	dec	sp
                                   2323 ;	usr/main.c:152: while(1){
      00096B                       2324 00114$:
                                   2325 ;	usr/main.c:153: if(fRecv){
      00096B 90 00 24         [24] 2326 	mov	dptr,#_fRecv
      00096E E0               [24] 2327 	movx	a,@dptr
      00096F 60 FA            [24] 2328 	jz	00114$
                                   2329 ;	usr/main.c:154: fRecv = 0;		
      000971 90 00 24         [24] 2330 	mov	dptr,#_fRecv
      000974 E4               [12] 2331 	clr	a
      000975 F0               [24] 2332 	movx	@dptr,a
                                   2333 ;	usr/main.c:155: if (pBuf == 28){
      000976 90 00 23         [24] 2334 	mov	dptr,#_pBuf
      000979 E0               [24] 2335 	movx	a,@dptr
      00097A FF               [12] 2336 	mov	r7,a
      00097B BF 1C 02         [24] 2337 	cjne	r7,#0x1c,00147$
      00097E 80 03            [24] 2338 	sjmp	00148$
      000980                       2339 00147$:
      000980 02 0B 72         [24] 2340 	ljmp	00107$
      000983                       2341 00148$:
                                   2342 ;	usr/main.c:156: pBuf = 0;
      000983 90 00 23         [24] 2343 	mov	dptr,#_pBuf
      000986 E4               [12] 2344 	clr	a
      000987 F0               [24] 2345 	movx	@dptr,a
                                   2346 ;	usr/main.c:157: IE_UART1 = 0;
                                   2347 ;	assignBit
      000988 C2 EC            [12] 2348 	clr	_IE_UART1
                                   2349 ;	usr/main.c:158: setCursor(0,3);
      00098A 74 03            [12] 2350 	mov	a,#0x03
      00098C C0 E0            [24] 2351 	push	acc
      00098E E4               [12] 2352 	clr	a
      00098F C0 E0            [24] 2353 	push	acc
      000991 90 00 00         [24] 2354 	mov	dptr,#0x0000
      000994 12 0C 03         [24] 2355 	lcall	_setCursor
      000997 15 81            [12] 2356 	dec	sp
      000999 15 81            [12] 2357 	dec	sp
                                   2358 ;	usr/main.c:159: if(recvBuf[0]==0XAA && recvBuf[26]==0X55 && recvBuf[27]==0XCC){
      00099B 90 00 05         [24] 2359 	mov	dptr,#_recvBuf
      00099E E0               [24] 2360 	movx	a,@dptr
      00099F FF               [12] 2361 	mov	r7,a
      0009A0 BF AA 02         [24] 2362 	cjne	r7,#0xaa,00149$
      0009A3 80 03            [24] 2363 	sjmp	00150$
      0009A5                       2364 00149$:
      0009A5 02 0B 1C         [24] 2365 	ljmp	00102$
      0009A8                       2366 00150$:
      0009A8 90 00 1F         [24] 2367 	mov	dptr,#(_recvBuf + 0x001a)
      0009AB E0               [24] 2368 	movx	a,@dptr
      0009AC FF               [12] 2369 	mov	r7,a
      0009AD BF 55 02         [24] 2370 	cjne	r7,#0x55,00151$
      0009B0 80 03            [24] 2371 	sjmp	00152$
      0009B2                       2372 00151$:
      0009B2 02 0B 1C         [24] 2373 	ljmp	00102$
      0009B5                       2374 00152$:
      0009B5 90 00 20         [24] 2375 	mov	dptr,#(_recvBuf + 0x001b)
      0009B8 E0               [24] 2376 	movx	a,@dptr
      0009B9 FF               [12] 2377 	mov	r7,a
      0009BA BF CC 02         [24] 2378 	cjne	r7,#0xcc,00153$
      0009BD 80 03            [24] 2379 	sjmp	00154$
      0009BF                       2380 00153$:
      0009BF 02 0B 1C         [24] 2381 	ljmp	00102$
      0009C2                       2382 00154$:
                                   2383 ;	usr/main.c:161: decode_XenP201S(recvBuf,&object);										
      0009C2 A9 16            [24] 2384 	mov	r1,_bp
      0009C4 09               [12] 2385 	inc	r1
      0009C5 89 05            [24] 2386 	mov	ar5,r1
      0009C7 7E 00            [12] 2387 	mov	r6,#0x00
      0009C9 7F 40            [12] 2388 	mov	r7,#0x40
      0009CB C0 01            [24] 2389 	push	ar1
      0009CD C0 05            [24] 2390 	push	ar5
      0009CF C0 06            [24] 2391 	push	ar6
      0009D1 C0 07            [24] 2392 	push	ar7
      0009D3 90 00 05         [24] 2393 	mov	dptr,#_recvBuf
      0009D6 75 F0 00         [24] 2394 	mov	b,#0x00
      0009D9 12 00 C6         [24] 2395 	lcall	_decode_XenP201S
      0009DC 15 81            [12] 2396 	dec	sp
      0009DE 15 81            [12] 2397 	dec	sp
      0009E0 15 81            [12] 2398 	dec	sp
                                   2399 ;	usr/main.c:162: setCursor(0,0);
      0009E2 E4               [12] 2400 	clr	a
      0009E3 C0 E0            [24] 2401 	push	acc
      0009E5 C0 E0            [24] 2402 	push	acc
      0009E7 90 00 00         [24] 2403 	mov	dptr,#0x0000
      0009EA 12 0C 03         [24] 2404 	lcall	_setCursor
      0009ED 15 81            [12] 2405 	dec	sp
      0009EF 15 81            [12] 2406 	dec	sp
      0009F1 D0 01            [24] 2407 	pop	ar1
                                   2408 ;	usr/main.c:163: printf_fast_f("X :%5d%5d%5d",object.x1Pos,object.x2Pos,object.x3Pos);
      0009F3 74 10            [12] 2409 	mov	a,#0x10
      0009F5 29               [12] 2410 	add	a,r1
      0009F6 F8               [12] 2411 	mov	r0,a
      0009F7 86 06            [24] 2412 	mov	ar6,@r0
      0009F9 08               [12] 2413 	inc	r0
      0009FA 86 07            [24] 2414 	mov	ar7,@r0
      0009FC 18               [12] 2415 	dec	r0
      0009FD 74 08            [12] 2416 	mov	a,#0x08
      0009FF 29               [12] 2417 	add	a,r1
      000A00 F8               [12] 2418 	mov	r0,a
      000A01 86 04            [24] 2419 	mov	ar4,@r0
      000A03 08               [12] 2420 	inc	r0
      000A04 86 05            [24] 2421 	mov	ar5,@r0
      000A06 18               [12] 2422 	dec	r0
      000A07 87 02            [24] 2423 	mov	ar2,@r1
      000A09 09               [12] 2424 	inc	r1
      000A0A 87 03            [24] 2425 	mov	ar3,@r1
      000A0C 19               [12] 2426 	dec	r1
      000A0D C0 01            [24] 2427 	push	ar1
      000A0F C0 06            [24] 2428 	push	ar6
      000A11 C0 07            [24] 2429 	push	ar7
      000A13 C0 04            [24] 2430 	push	ar4
      000A15 C0 05            [24] 2431 	push	ar5
      000A17 C0 02            [24] 2432 	push	ar2
      000A19 C0 03            [24] 2433 	push	ar3
      000A1B 74 AB            [12] 2434 	mov	a,#___str_2
      000A1D C0 E0            [24] 2435 	push	acc
      000A1F 74 34            [12] 2436 	mov	a,#(___str_2 >> 8)
      000A21 C0 E0            [24] 2437 	push	acc
      000A23 12 16 3D         [24] 2438 	lcall	_printf_fast_f
      000A26 E5 81            [12] 2439 	mov	a,sp
      000A28 24 F8            [12] 2440 	add	a,#0xf8
      000A2A F5 81            [12] 2441 	mov	sp,a
                                   2442 ;	usr/main.c:164: setCursor(0,1);
      000A2C 74 01            [12] 2443 	mov	a,#0x01
      000A2E C0 E0            [24] 2444 	push	acc
      000A30 E4               [12] 2445 	clr	a
      000A31 C0 E0            [24] 2446 	push	acc
      000A33 90 00 00         [24] 2447 	mov	dptr,#0x0000
      000A36 12 0C 03         [24] 2448 	lcall	_setCursor
      000A39 15 81            [12] 2449 	dec	sp
      000A3B 15 81            [12] 2450 	dec	sp
      000A3D D0 01            [24] 2451 	pop	ar1
                                   2452 ;	usr/main.c:165: printf_fast_f("Y :%5d%5d%5d",object.y1Pos,object.y2Pos,object.y3Pos);
      000A3F 74 12            [12] 2453 	mov	a,#0x12
      000A41 29               [12] 2454 	add	a,r1
      000A42 F8               [12] 2455 	mov	r0,a
      000A43 86 06            [24] 2456 	mov	ar6,@r0
      000A45 08               [12] 2457 	inc	r0
      000A46 86 07            [24] 2458 	mov	ar7,@r0
      000A48 18               [12] 2459 	dec	r0
      000A49 74 0A            [12] 2460 	mov	a,#0x0a
      000A4B 29               [12] 2461 	add	a,r1
      000A4C F8               [12] 2462 	mov	r0,a
      000A4D 86 04            [24] 2463 	mov	ar4,@r0
      000A4F 08               [12] 2464 	inc	r0
      000A50 86 05            [24] 2465 	mov	ar5,@r0
      000A52 18               [12] 2466 	dec	r0
      000A53 74 02            [12] 2467 	mov	a,#0x02
      000A55 29               [12] 2468 	add	a,r1
      000A56 F8               [12] 2469 	mov	r0,a
      000A57 86 02            [24] 2470 	mov	ar2,@r0
      000A59 08               [12] 2471 	inc	r0
      000A5A 86 03            [24] 2472 	mov	ar3,@r0
      000A5C 18               [12] 2473 	dec	r0
      000A5D C0 01            [24] 2474 	push	ar1
      000A5F C0 06            [24] 2475 	push	ar6
      000A61 C0 07            [24] 2476 	push	ar7
      000A63 C0 04            [24] 2477 	push	ar4
      000A65 C0 05            [24] 2478 	push	ar5
      000A67 C0 02            [24] 2479 	push	ar2
      000A69 C0 03            [24] 2480 	push	ar3
      000A6B 74 B8            [12] 2481 	mov	a,#___str_3
      000A6D C0 E0            [24] 2482 	push	acc
      000A6F 74 34            [12] 2483 	mov	a,#(___str_3 >> 8)
      000A71 C0 E0            [24] 2484 	push	acc
      000A73 12 16 3D         [24] 2485 	lcall	_printf_fast_f
      000A76 E5 81            [12] 2486 	mov	a,sp
      000A78 24 F8            [12] 2487 	add	a,#0xf8
      000A7A F5 81            [12] 2488 	mov	sp,a
                                   2489 ;	usr/main.c:166: setCursor(0,2);
      000A7C 74 02            [12] 2490 	mov	a,#0x02
      000A7E C0 E0            [24] 2491 	push	acc
      000A80 E4               [12] 2492 	clr	a
      000A81 C0 E0            [24] 2493 	push	acc
      000A83 90 00 00         [24] 2494 	mov	dptr,#0x0000
      000A86 12 0C 03         [24] 2495 	lcall	_setCursor
      000A89 15 81            [12] 2496 	dec	sp
      000A8B 15 81            [12] 2497 	dec	sp
      000A8D D0 01            [24] 2498 	pop	ar1
                                   2499 ;	usr/main.c:167: printf_fast_f("D :%5d%5d%5d",object.dist1,object.dist2,object.dist3);
      000A8F 74 14            [12] 2500 	mov	a,#0x14
      000A91 29               [12] 2501 	add	a,r1
      000A92 F8               [12] 2502 	mov	r0,a
      000A93 86 06            [24] 2503 	mov	ar6,@r0
      000A95 08               [12] 2504 	inc	r0
      000A96 86 07            [24] 2505 	mov	ar7,@r0
      000A98 18               [12] 2506 	dec	r0
      000A99 74 0C            [12] 2507 	mov	a,#0x0c
      000A9B 29               [12] 2508 	add	a,r1
      000A9C F8               [12] 2509 	mov	r0,a
      000A9D 86 04            [24] 2510 	mov	ar4,@r0
      000A9F 08               [12] 2511 	inc	r0
      000AA0 86 05            [24] 2512 	mov	ar5,@r0
      000AA2 18               [12] 2513 	dec	r0
      000AA3 74 04            [12] 2514 	mov	a,#0x04
      000AA5 29               [12] 2515 	add	a,r1
      000AA6 F8               [12] 2516 	mov	r0,a
      000AA7 86 02            [24] 2517 	mov	ar2,@r0
      000AA9 08               [12] 2518 	inc	r0
      000AAA 86 03            [24] 2519 	mov	ar3,@r0
      000AAC 18               [12] 2520 	dec	r0
      000AAD C0 01            [24] 2521 	push	ar1
      000AAF C0 06            [24] 2522 	push	ar6
      000AB1 C0 07            [24] 2523 	push	ar7
      000AB3 C0 04            [24] 2524 	push	ar4
      000AB5 C0 05            [24] 2525 	push	ar5
      000AB7 C0 02            [24] 2526 	push	ar2
      000AB9 C0 03            [24] 2527 	push	ar3
      000ABB 74 C5            [12] 2528 	mov	a,#___str_4
      000ABD C0 E0            [24] 2529 	push	acc
      000ABF 74 34            [12] 2530 	mov	a,#(___str_4 >> 8)
      000AC1 C0 E0            [24] 2531 	push	acc
      000AC3 12 16 3D         [24] 2532 	lcall	_printf_fast_f
      000AC6 E5 81            [12] 2533 	mov	a,sp
      000AC8 24 F8            [12] 2534 	add	a,#0xf8
      000ACA F5 81            [12] 2535 	mov	sp,a
                                   2536 ;	usr/main.c:168: setCursor(0,3);
      000ACC 74 03            [12] 2537 	mov	a,#0x03
      000ACE C0 E0            [24] 2538 	push	acc
      000AD0 E4               [12] 2539 	clr	a
      000AD1 C0 E0            [24] 2540 	push	acc
      000AD3 90 00 00         [24] 2541 	mov	dptr,#0x0000
      000AD6 12 0C 03         [24] 2542 	lcall	_setCursor
      000AD9 15 81            [12] 2543 	dec	sp
      000ADB 15 81            [12] 2544 	dec	sp
      000ADD D0 01            [24] 2545 	pop	ar1
                                   2546 ;	usr/main.c:169: printf_fast_f("A :%5d%5d%5d",object.ang1,object.ang2,object.ang3);
      000ADF 74 16            [12] 2547 	mov	a,#0x16
      000AE1 29               [12] 2548 	add	a,r1
      000AE2 F8               [12] 2549 	mov	r0,a
      000AE3 86 06            [24] 2550 	mov	ar6,@r0
      000AE5 08               [12] 2551 	inc	r0
      000AE6 86 07            [24] 2552 	mov	ar7,@r0
      000AE8 18               [12] 2553 	dec	r0
      000AE9 74 0E            [12] 2554 	mov	a,#0x0e
      000AEB 29               [12] 2555 	add	a,r1
      000AEC F8               [12] 2556 	mov	r0,a
      000AED 86 04            [24] 2557 	mov	ar4,@r0
      000AEF 08               [12] 2558 	inc	r0
      000AF0 86 05            [24] 2559 	mov	ar5,@r0
      000AF2 18               [12] 2560 	dec	r0
      000AF3 74 06            [12] 2561 	mov	a,#0x06
      000AF5 29               [12] 2562 	add	a,r1
      000AF6 F9               [12] 2563 	mov	r1,a
      000AF7 87 02            [24] 2564 	mov	ar2,@r1
      000AF9 09               [12] 2565 	inc	r1
      000AFA 87 03            [24] 2566 	mov	ar3,@r1
      000AFC 19               [12] 2567 	dec	r1
      000AFD C0 06            [24] 2568 	push	ar6
      000AFF C0 07            [24] 2569 	push	ar7
      000B01 C0 04            [24] 2570 	push	ar4
      000B03 C0 05            [24] 2571 	push	ar5
      000B05 C0 02            [24] 2572 	push	ar2
      000B07 C0 03            [24] 2573 	push	ar3
      000B09 74 D2            [12] 2574 	mov	a,#___str_5
      000B0B C0 E0            [24] 2575 	push	acc
      000B0D 74 34            [12] 2576 	mov	a,#(___str_5 >> 8)
      000B0F C0 E0            [24] 2577 	push	acc
      000B11 12 16 3D         [24] 2578 	lcall	_printf_fast_f
      000B14 E5 81            [12] 2579 	mov	a,sp
      000B16 24 F8            [12] 2580 	add	a,#0xf8
      000B18 F5 81            [12] 2581 	mov	sp,a
      000B1A 80 41            [24] 2582 	sjmp	00103$
      000B1C                       2583 00102$:
                                   2584 ;	usr/main.c:173: setCursor(0,0);
      000B1C E4               [12] 2585 	clr	a
      000B1D C0 E0            [24] 2586 	push	acc
      000B1F C0 E0            [24] 2587 	push	acc
      000B21 90 00 00         [24] 2588 	mov	dptr,#0x0000
      000B24 12 0C 03         [24] 2589 	lcall	_setCursor
      000B27 15 81            [12] 2590 	dec	sp
      000B29 15 81            [12] 2591 	dec	sp
                                   2592 ;	usr/main.c:174: printf_fast_f(" %x %x %x ",recvBuf[0],recvBuf[26],recvBuf[27]);
      000B2B 90 00 20         [24] 2593 	mov	dptr,#(_recvBuf + 0x001b)
      000B2E E0               [24] 2594 	movx	a,@dptr
      000B2F FF               [12] 2595 	mov	r7,a
      000B30 7E 00            [12] 2596 	mov	r6,#0x00
      000B32 90 00 1F         [24] 2597 	mov	dptr,#(_recvBuf + 0x001a)
      000B35 E0               [24] 2598 	movx	a,@dptr
      000B36 FD               [12] 2599 	mov	r5,a
      000B37 7C 00            [12] 2600 	mov	r4,#0x00
      000B39 90 00 05         [24] 2601 	mov	dptr,#_recvBuf
      000B3C E0               [24] 2602 	movx	a,@dptr
      000B3D FB               [12] 2603 	mov	r3,a
      000B3E 7A 00            [12] 2604 	mov	r2,#0x00
      000B40 C0 07            [24] 2605 	push	ar7
      000B42 C0 06            [24] 2606 	push	ar6
      000B44 C0 05            [24] 2607 	push	ar5
      000B46 C0 04            [24] 2608 	push	ar4
      000B48 C0 03            [24] 2609 	push	ar3
      000B4A C0 02            [24] 2610 	push	ar2
      000B4C 74 DF            [12] 2611 	mov	a,#___str_6
      000B4E C0 E0            [24] 2612 	push	acc
      000B50 74 34            [12] 2613 	mov	a,#(___str_6 >> 8)
      000B52 C0 E0            [24] 2614 	push	acc
      000B54 12 16 3D         [24] 2615 	lcall	_printf_fast_f
      000B57 E5 81            [12] 2616 	mov	a,sp
      000B59 24 F8            [12] 2617 	add	a,#0xf8
      000B5B F5 81            [12] 2618 	mov	sp,a
      000B5D                       2619 00103$:
                                   2620 ;	usr/main.c:176: IE_UART1 = 1;
                                   2621 ;	assignBit
      000B5D D2 EC            [12] 2622 	setb	_IE_UART1
                                   2623 ;	usr/main.c:177: FaceTrack(object.ang1);
      000B5F E5 16            [12] 2624 	mov	a,_bp
      000B61 04               [12] 2625 	inc	a
      000B62 24 06            [12] 2626 	add	a,#0x06
      000B64 F9               [12] 2627 	mov	r1,a
      000B65 87 06            [24] 2628 	mov	ar6,@r1
      000B67 09               [12] 2629 	inc	r1
      000B68 87 07            [24] 2630 	mov	ar7,@r1
      000B6A 19               [12] 2631 	dec	r1
      000B6B 8E 82            [24] 2632 	mov	dpl,r6
      000B6D 8F 83            [24] 2633 	mov	dph,r7
      000B6F 12 08 61         [24] 2634 	lcall	_FaceTrack
      000B72                       2635 00107$:
                                   2636 ;	usr/main.c:179: if(recvBuf[0]!=0XAA){
      000B72 90 00 05         [24] 2637 	mov	dptr,#_recvBuf
      000B75 E0               [24] 2638 	movx	a,@dptr
      000B76 FF               [12] 2639 	mov	r7,a
      000B77 BF AA 02         [24] 2640 	cjne	r7,#0xaa,00155$
      000B7A 80 0A            [24] 2641 	sjmp	00109$
      000B7C                       2642 00155$:
                                   2643 ;	usr/main.c:180: P2_2 = 0;
                                   2644 ;	assignBit
      000B7C C2 A2            [12] 2645 	clr	_P2_2
                                   2646 ;	usr/main.c:181: pBuf=0;
      000B7E 90 00 23         [24] 2647 	mov	dptr,#_pBuf
      000B81 E4               [12] 2648 	clr	a
      000B82 F0               [24] 2649 	movx	@dptr,a
      000B83 02 09 6B         [24] 2650 	ljmp	00114$
      000B86                       2651 00109$:
                                   2652 ;	usr/main.c:183: P2_2 =1;
                                   2653 ;	assignBit
      000B86 D2 A2            [12] 2654 	setb	_P2_2
      000B88 02 09 6B         [24] 2655 	ljmp	00114$
                                   2656 ;	usr/main.c:187: }
      000B8B 85 16 81         [24] 2657 	mov	sp,_bp
      000B8E D0 16            [24] 2658 	pop	_bp
      000B90 22               [24] 2659 	ret
                                   2660 ;------------------------------------------------------------
                                   2661 ;Allocation info for local variables in function 'putchar'
                                   2662 ;------------------------------------------------------------
                                   2663 ;a                         Allocated to registers r6 r7 
                                   2664 ;------------------------------------------------------------
                                   2665 ;	usr/main.c:196: int putchar( int a)
                                   2666 ;	-----------------------------------------
                                   2667 ;	 function putchar
                                   2668 ;	-----------------------------------------
      000B91                       2669 _putchar:
      000B91 AE 82            [24] 2670 	mov	r6,dpl
      000B93 AF 83            [24] 2671 	mov	r7,dph
                                   2672 ;	usr/main.c:199: OLED_ShowChar(oled_colum,oled_row,a);
      000B95 8E 05            [24] 2673 	mov	ar5,r6
      000B97 90 00 03         [24] 2674 	mov	dptr,#_oled_row
      000B9A E0               [24] 2675 	movx	a,@dptr
      000B9B FB               [12] 2676 	mov	r3,a
      000B9C A3               [24] 2677 	inc	dptr
      000B9D E0               [24] 2678 	movx	a,@dptr
      000B9E 90 00 01         [24] 2679 	mov	dptr,#_oled_colum
      000BA1 E0               [24] 2680 	movx	a,@dptr
      000BA2 FA               [12] 2681 	mov	r2,a
      000BA3 A3               [24] 2682 	inc	dptr
      000BA4 E0               [24] 2683 	movx	a,@dptr
      000BA5 C0 07            [24] 2684 	push	ar7
      000BA7 C0 06            [24] 2685 	push	ar6
      000BA9 C0 05            [24] 2686 	push	ar5
      000BAB C0 03            [24] 2687 	push	ar3
      000BAD 8A 82            [24] 2688 	mov	dpl,r2
      000BAF 12 11 34         [24] 2689 	lcall	_OLED_ShowChar
      000BB2 15 81            [12] 2690 	dec	sp
      000BB4 15 81            [12] 2691 	dec	sp
      000BB6 D0 06            [24] 2692 	pop	ar6
      000BB8 D0 07            [24] 2693 	pop	ar7
                                   2694 ;	usr/main.c:201: oled_colum+=6;
      000BBA 90 00 01         [24] 2695 	mov	dptr,#_oled_colum
      000BBD E0               [24] 2696 	movx	a,@dptr
      000BBE FC               [12] 2697 	mov	r4,a
      000BBF A3               [24] 2698 	inc	dptr
      000BC0 E0               [24] 2699 	movx	a,@dptr
      000BC1 FD               [12] 2700 	mov	r5,a
      000BC2 90 00 01         [24] 2701 	mov	dptr,#_oled_colum
      000BC5 74 06            [12] 2702 	mov	a,#0x06
      000BC7 2C               [12] 2703 	add	a,r4
      000BC8 F0               [24] 2704 	movx	@dptr,a
      000BC9 E4               [12] 2705 	clr	a
      000BCA 3D               [12] 2706 	addc	a,r5
      000BCB A3               [24] 2707 	inc	dptr
      000BCC F0               [24] 2708 	movx	@dptr,a
                                   2709 ;	usr/main.c:206: if (oled_colum>122){oled_colum=0;oled_row+=1;}
      000BCD 90 00 01         [24] 2710 	mov	dptr,#_oled_colum
      000BD0 E0               [24] 2711 	movx	a,@dptr
      000BD1 FC               [12] 2712 	mov	r4,a
      000BD2 A3               [24] 2713 	inc	dptr
      000BD3 E0               [24] 2714 	movx	a,@dptr
      000BD4 FD               [12] 2715 	mov	r5,a
      000BD5 C3               [12] 2716 	clr	c
      000BD6 74 7A            [12] 2717 	mov	a,#0x7a
      000BD8 9C               [12] 2718 	subb	a,r4
      000BD9 74 80            [12] 2719 	mov	a,#(0x00 ^ 0x80)
      000BDB 8D F0            [24] 2720 	mov	b,r5
      000BDD 63 F0 80         [24] 2721 	xrl	b,#0x80
      000BE0 95 F0            [12] 2722 	subb	a,b
      000BE2 50 1A            [24] 2723 	jnc	00102$
      000BE4 90 00 01         [24] 2724 	mov	dptr,#_oled_colum
      000BE7 E4               [12] 2725 	clr	a
      000BE8 F0               [24] 2726 	movx	@dptr,a
      000BE9 A3               [24] 2727 	inc	dptr
      000BEA F0               [24] 2728 	movx	@dptr,a
      000BEB 90 00 03         [24] 2729 	mov	dptr,#_oled_row
      000BEE E0               [24] 2730 	movx	a,@dptr
      000BEF FC               [12] 2731 	mov	r4,a
      000BF0 A3               [24] 2732 	inc	dptr
      000BF1 E0               [24] 2733 	movx	a,@dptr
      000BF2 FD               [12] 2734 	mov	r5,a
      000BF3 90 00 03         [24] 2735 	mov	dptr,#_oled_row
      000BF6 74 01            [12] 2736 	mov	a,#0x01
      000BF8 2C               [12] 2737 	add	a,r4
      000BF9 F0               [24] 2738 	movx	@dptr,a
      000BFA E4               [12] 2739 	clr	a
      000BFB 3D               [12] 2740 	addc	a,r5
      000BFC A3               [24] 2741 	inc	dptr
      000BFD F0               [24] 2742 	movx	@dptr,a
      000BFE                       2743 00102$:
                                   2744 ;	usr/main.c:207: return(a);
      000BFE 8E 82            [24] 2745 	mov	dpl,r6
      000C00 8F 83            [24] 2746 	mov	dph,r7
                                   2747 ;	usr/main.c:208: }
      000C02 22               [24] 2748 	ret
                                   2749 ;------------------------------------------------------------
                                   2750 ;Allocation info for local variables in function 'setCursor'
                                   2751 ;------------------------------------------------------------
                                   2752 ;row                       Allocated to stack - _bp -4
                                   2753 ;column                    Allocated to registers 
                                   2754 ;------------------------------------------------------------
                                   2755 ;	usr/main.c:216: void setCursor(int column,int row)
                                   2756 ;	-----------------------------------------
                                   2757 ;	 function setCursor
                                   2758 ;	-----------------------------------------
      000C03                       2759 _setCursor:
      000C03 C0 16            [24] 2760 	push	_bp
      000C05 85 81 16         [24] 2761 	mov	_bp,sp
      000C08 AF 83            [24] 2762 	mov	r7,dph
      000C0A E5 82            [12] 2763 	mov	a,dpl
      000C0C 90 00 01         [24] 2764 	mov	dptr,#_oled_colum
      000C0F F0               [24] 2765 	movx	@dptr,a
      000C10 EF               [12] 2766 	mov	a,r7
      000C11 A3               [24] 2767 	inc	dptr
      000C12 F0               [24] 2768 	movx	@dptr,a
                                   2769 ;	usr/main.c:219: oled_row = row;
      000C13 E5 16            [12] 2770 	mov	a,_bp
      000C15 24 FC            [12] 2771 	add	a,#0xfc
      000C17 F8               [12] 2772 	mov	r0,a
      000C18 90 00 03         [24] 2773 	mov	dptr,#_oled_row
      000C1B E6               [12] 2774 	mov	a,@r0
      000C1C F0               [24] 2775 	movx	@dptr,a
      000C1D 08               [12] 2776 	inc	r0
      000C1E E6               [12] 2777 	mov	a,@r0
      000C1F A3               [24] 2778 	inc	dptr
      000C20 F0               [24] 2779 	movx	@dptr,a
                                   2780 ;	usr/main.c:220: }
      000C21 D0 16            [24] 2781 	pop	_bp
      000C23 22               [24] 2782 	ret
                                   2783 ;------------------------------------------------------------
                                   2784 ;Allocation info for local variables in function 'UART1Interrupt'
                                   2785 ;------------------------------------------------------------
                                   2786 ;	usr/main.c:222: void UART1Interrupt(void) __interrupt INT_NO_UART1 __using 1 {
                                   2787 ;	-----------------------------------------
                                   2788 ;	 function UART1Interrupt
                                   2789 ;	-----------------------------------------
      000C24                       2790 _UART1Interrupt:
                           00000F  2791 	ar7 = 0x0f
                           00000E  2792 	ar6 = 0x0e
                           00000D  2793 	ar5 = 0x0d
                           00000C  2794 	ar4 = 0x0c
                           00000B  2795 	ar3 = 0x0b
                           00000A  2796 	ar2 = 0x0a
                           000009  2797 	ar1 = 0x09
                           000008  2798 	ar0 = 0x08
      000C24 C0 21            [24] 2799 	push	bits
      000C26 C0 E0            [24] 2800 	push	acc
      000C28 C0 F0            [24] 2801 	push	b
      000C2A C0 82            [24] 2802 	push	dpl
      000C2C C0 83            [24] 2803 	push	dph
      000C2E C0 07            [24] 2804 	push	(0+7)
      000C30 C0 06            [24] 2805 	push	(0+6)
      000C32 C0 05            [24] 2806 	push	(0+5)
      000C34 C0 04            [24] 2807 	push	(0+4)
      000C36 C0 03            [24] 2808 	push	(0+3)
      000C38 C0 02            [24] 2809 	push	(0+2)
      000C3A C0 01            [24] 2810 	push	(0+1)
      000C3C C0 00            [24] 2811 	push	(0+0)
      000C3E C0 D0            [24] 2812 	push	psw
      000C40 75 D0 08         [24] 2813 	mov	psw,#0x08
                                   2814 ;	usr/main.c:223: pBuf%=28;
      000C43 90 00 23         [24] 2815 	mov	dptr,#_pBuf
      000C46 E0               [24] 2816 	movx	a,@dptr
      000C47 FF               [12] 2817 	mov	r7,a
      000C48 7E 00            [12] 2818 	mov	r6,#0x00
      000C4A 74 1C            [12] 2819 	mov	a,#0x1c
      000C4C C0 E0            [24] 2820 	push	acc
      000C4E E4               [12] 2821 	clr	a
      000C4F C0 E0            [24] 2822 	push	acc
      000C51 8F 82            [24] 2823 	mov	dpl,r7
      000C53 8E 83            [24] 2824 	mov	dph,r6
      000C55 75 D0 00         [24] 2825 	mov	psw,#0x00
      000C58 12 1F BC         [24] 2826 	lcall	__modsint
      000C5B 75 D0 08         [24] 2827 	mov	psw,#0x08
      000C5E AE 82            [24] 2828 	mov	r6,dpl
      000C60 15 81            [12] 2829 	dec	sp
      000C62 15 81            [12] 2830 	dec	sp
      000C64 90 00 23         [24] 2831 	mov	dptr,#_pBuf
      000C67 EE               [12] 2832 	mov	a,r6
      000C68 F0               [24] 2833 	movx	@dptr,a
                                   2834 ;	usr/main.c:224: recvBuf[pBuf] = CH549UART1RcvByte();
      000C69 E0               [24] 2835 	movx	a,@dptr
      000C6A 24 05            [12] 2836 	add	a,#_recvBuf
      000C6C FF               [12] 2837 	mov	r7,a
      000C6D E4               [12] 2838 	clr	a
      000C6E 34 00            [12] 2839 	addc	a,#(_recvBuf >> 8)
      000C70 FE               [12] 2840 	mov	r6,a
      000C71 C0 0F            [24] 2841 	push	ar7
      000C73 C0 0E            [24] 2842 	push	ar6
      000C75 75 D0 00         [24] 2843 	mov	psw,#0x00
      000C78 12 0D FC         [24] 2844 	lcall	_CH549UART1RcvByte
      000C7B 75 D0 08         [24] 2845 	mov	psw,#0x08
      000C7E AD 82            [24] 2846 	mov	r5,dpl
      000C80 D0 0E            [24] 2847 	pop	ar6
      000C82 D0 0F            [24] 2848 	pop	ar7
      000C84 8F 82            [24] 2849 	mov	dpl,r7
      000C86 8E 83            [24] 2850 	mov	dph,r6
      000C88 ED               [12] 2851 	mov	a,r5
      000C89 F0               [24] 2852 	movx	@dptr,a
                                   2853 ;	usr/main.c:225: pBuf++;
      000C8A 90 00 23         [24] 2854 	mov	dptr,#_pBuf
      000C8D E0               [24] 2855 	movx	a,@dptr
      000C8E 04               [12] 2856 	inc	a
      000C8F F0               [24] 2857 	movx	@dptr,a
                                   2858 ;	usr/main.c:226: fRecv = 1;
      000C90 90 00 24         [24] 2859 	mov	dptr,#_fRecv
      000C93 74 01            [12] 2860 	mov	a,#0x01
      000C95 F0               [24] 2861 	movx	@dptr,a
                                   2862 ;	usr/main.c:227: }
      000C96 D0 D0            [24] 2863 	pop	psw
      000C98 D0 00            [24] 2864 	pop	(0+0)
      000C9A D0 01            [24] 2865 	pop	(0+1)
      000C9C D0 02            [24] 2866 	pop	(0+2)
      000C9E D0 03            [24] 2867 	pop	(0+3)
      000CA0 D0 04            [24] 2868 	pop	(0+4)
      000CA2 D0 05            [24] 2869 	pop	(0+5)
      000CA4 D0 06            [24] 2870 	pop	(0+6)
      000CA6 D0 07            [24] 2871 	pop	(0+7)
      000CA8 D0 83            [24] 2872 	pop	dph
      000CAA D0 82            [24] 2873 	pop	dpl
      000CAC D0 F0            [24] 2874 	pop	b
      000CAE D0 E0            [24] 2875 	pop	acc
      000CB0 D0 21            [24] 2876 	pop	bits
      000CB2 32               [24] 2877 	reti
                                   2878 	.area CSEG    (CODE)
                                   2879 	.area CONST   (CODE)
                                   2880 	.area CONST   (CODE)
      003494                       2881 ___str_0:
      003494 44 3A 25 64 20 53 3A  2882 	.ascii "D:%d S:%3d G:%2d"
             25 33 64 20 47 3A 25
             32 64
      0034A4 00                    2883 	.db 0x00
                                   2884 	.area CSEG    (CODE)
                                   2885 	.area CONST   (CODE)
      0034A5                       2886 ___str_1:
      0034A5 52 61 64 61 72        2887 	.ascii "Radar"
      0034AA 00                    2888 	.db 0x00
                                   2889 	.area CSEG    (CODE)
                                   2890 	.area CONST   (CODE)
      0034AB                       2891 ___str_2:
      0034AB 58 20 3A 25 35 64 25  2892 	.ascii "X :%5d%5d%5d"
             35 64 25 35 64
      0034B7 00                    2893 	.db 0x00
                                   2894 	.area CSEG    (CODE)
                                   2895 	.area CONST   (CODE)
      0034B8                       2896 ___str_3:
      0034B8 59 20 3A 25 35 64 25  2897 	.ascii "Y :%5d%5d%5d"
             35 64 25 35 64
      0034C4 00                    2898 	.db 0x00
                                   2899 	.area CSEG    (CODE)
                                   2900 	.area CONST   (CODE)
      0034C5                       2901 ___str_4:
      0034C5 44 20 3A 25 35 64 25  2902 	.ascii "D :%5d%5d%5d"
             35 64 25 35 64
      0034D1 00                    2903 	.db 0x00
                                   2904 	.area CSEG    (CODE)
                                   2905 	.area CONST   (CODE)
      0034D2                       2906 ___str_5:
      0034D2 41 20 3A 25 35 64 25  2907 	.ascii "A :%5d%5d%5d"
             35 64 25 35 64
      0034DE 00                    2908 	.db 0x00
                                   2909 	.area CSEG    (CODE)
                                   2910 	.area CONST   (CODE)
      0034DF                       2911 ___str_6:
      0034DF 20 25 78 20 25 78 20  2912 	.ascii " %x %x %x "
             25 78 20
      0034E9 00                    2913 	.db 0x00
                                   2914 	.area CSEG    (CODE)
                                   2915 	.area CABS    (ABS,CODE)
