                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 4.0.0 #11528 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module CH549_GPIO
                                      6 	.optsdcc -mmcs51 --model-large
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _GPIO_STD1_ISR
                                     12 	.globl _GPIO_STD0_ISR
                                     13 	.globl _GPIO_EXT_ISR
                                     14 	.globl _mDelaymS
                                     15 	.globl _printf
                                     16 	.globl _UIF_BUS_RST
                                     17 	.globl _UIF_DETECT
                                     18 	.globl _UIF_TRANSFER
                                     19 	.globl _UIF_SUSPEND
                                     20 	.globl _UIF_HST_SOF
                                     21 	.globl _UIF_FIFO_OV
                                     22 	.globl _U_SIE_FREE
                                     23 	.globl _U_TOG_OK
                                     24 	.globl _U_IS_NAK
                                     25 	.globl _S0_R_FIFO
                                     26 	.globl _S0_T_FIFO
                                     27 	.globl _S0_FREE
                                     28 	.globl _S0_IF_BYTE
                                     29 	.globl _S0_IF_FIRST
                                     30 	.globl _S0_IF_OV
                                     31 	.globl _S0_FST_ACT
                                     32 	.globl _CP_RL2
                                     33 	.globl _C_T2
                                     34 	.globl _TR2
                                     35 	.globl _EXEN2
                                     36 	.globl _TCLK
                                     37 	.globl _RCLK
                                     38 	.globl _EXF2
                                     39 	.globl _CAP1F
                                     40 	.globl _TF2
                                     41 	.globl _RI
                                     42 	.globl _TI
                                     43 	.globl _RB8
                                     44 	.globl _TB8
                                     45 	.globl _REN
                                     46 	.globl _SM2
                                     47 	.globl _SM1
                                     48 	.globl _SM0
                                     49 	.globl _IT0
                                     50 	.globl _IE0
                                     51 	.globl _IT1
                                     52 	.globl _IE1
                                     53 	.globl _TR0
                                     54 	.globl _TF0
                                     55 	.globl _TR1
                                     56 	.globl _TF1
                                     57 	.globl _XI
                                     58 	.globl _XO
                                     59 	.globl _P4_0
                                     60 	.globl _P4_1
                                     61 	.globl _P4_2
                                     62 	.globl _P4_3
                                     63 	.globl _P4_4
                                     64 	.globl _P4_5
                                     65 	.globl _P4_6
                                     66 	.globl _RXD
                                     67 	.globl _TXD
                                     68 	.globl _INT0
                                     69 	.globl _INT1
                                     70 	.globl _T0
                                     71 	.globl _T1
                                     72 	.globl _CAP0
                                     73 	.globl _INT3
                                     74 	.globl _P3_0
                                     75 	.globl _P3_1
                                     76 	.globl _P3_2
                                     77 	.globl _P3_3
                                     78 	.globl _P3_4
                                     79 	.globl _P3_5
                                     80 	.globl _P3_6
                                     81 	.globl _P3_7
                                     82 	.globl _PWM5
                                     83 	.globl _PWM4
                                     84 	.globl _INT0_
                                     85 	.globl _PWM3
                                     86 	.globl _PWM2
                                     87 	.globl _CAP1_
                                     88 	.globl _T2_
                                     89 	.globl _PWM1
                                     90 	.globl _CAP2_
                                     91 	.globl _T2EX_
                                     92 	.globl _PWM0
                                     93 	.globl _RXD1
                                     94 	.globl _PWM6
                                     95 	.globl _TXD1
                                     96 	.globl _PWM7
                                     97 	.globl _P2_0
                                     98 	.globl _P2_1
                                     99 	.globl _P2_2
                                    100 	.globl _P2_3
                                    101 	.globl _P2_4
                                    102 	.globl _P2_5
                                    103 	.globl _P2_6
                                    104 	.globl _P2_7
                                    105 	.globl _AIN0
                                    106 	.globl _CAP1
                                    107 	.globl _T2
                                    108 	.globl _AIN1
                                    109 	.globl _CAP2
                                    110 	.globl _T2EX
                                    111 	.globl _AIN2
                                    112 	.globl _AIN3
                                    113 	.globl _AIN4
                                    114 	.globl _UCC1
                                    115 	.globl _SCS
                                    116 	.globl _AIN5
                                    117 	.globl _UCC2
                                    118 	.globl _PWM0_
                                    119 	.globl _MOSI
                                    120 	.globl _AIN6
                                    121 	.globl _VBUS
                                    122 	.globl _RXD1_
                                    123 	.globl _MISO
                                    124 	.globl _AIN7
                                    125 	.globl _TXD1_
                                    126 	.globl _SCK
                                    127 	.globl _P1_0
                                    128 	.globl _P1_1
                                    129 	.globl _P1_2
                                    130 	.globl _P1_3
                                    131 	.globl _P1_4
                                    132 	.globl _P1_5
                                    133 	.globl _P1_6
                                    134 	.globl _P1_7
                                    135 	.globl _AIN8
                                    136 	.globl _AIN9
                                    137 	.globl _AIN10
                                    138 	.globl _RXD_
                                    139 	.globl _AIN11
                                    140 	.globl _TXD_
                                    141 	.globl _AIN12
                                    142 	.globl _RXD2
                                    143 	.globl _AIN13
                                    144 	.globl _TXD2
                                    145 	.globl _AIN14
                                    146 	.globl _RXD3
                                    147 	.globl _AIN15
                                    148 	.globl _TXD3
                                    149 	.globl _P0_0
                                    150 	.globl _P0_1
                                    151 	.globl _P0_2
                                    152 	.globl _P0_3
                                    153 	.globl _P0_4
                                    154 	.globl _P0_5
                                    155 	.globl _P0_6
                                    156 	.globl _P0_7
                                    157 	.globl _IE_SPI0
                                    158 	.globl _IE_INT3
                                    159 	.globl _IE_USB
                                    160 	.globl _IE_UART2
                                    161 	.globl _IE_ADC
                                    162 	.globl _IE_UART1
                                    163 	.globl _IE_UART3
                                    164 	.globl _IE_PWMX
                                    165 	.globl _IE_GPIO
                                    166 	.globl _IE_WDOG
                                    167 	.globl _PX0
                                    168 	.globl _PT0
                                    169 	.globl _PX1
                                    170 	.globl _PT1
                                    171 	.globl _PS
                                    172 	.globl _PT2
                                    173 	.globl _PL_FLAG
                                    174 	.globl _PH_FLAG
                                    175 	.globl _EX0
                                    176 	.globl _ET0
                                    177 	.globl _EX1
                                    178 	.globl _ET1
                                    179 	.globl _ES
                                    180 	.globl _ET2
                                    181 	.globl _E_DIS
                                    182 	.globl _EA
                                    183 	.globl _P
                                    184 	.globl _F1
                                    185 	.globl _OV
                                    186 	.globl _RS0
                                    187 	.globl _RS1
                                    188 	.globl _F0
                                    189 	.globl _AC
                                    190 	.globl _CY
                                    191 	.globl _UEP1_DMA_H
                                    192 	.globl _UEP1_DMA_L
                                    193 	.globl _UEP1_DMA
                                    194 	.globl _UEP0_DMA_H
                                    195 	.globl _UEP0_DMA_L
                                    196 	.globl _UEP0_DMA
                                    197 	.globl _UEP2_3_MOD
                                    198 	.globl _UEP4_1_MOD
                                    199 	.globl _UEP3_DMA_H
                                    200 	.globl _UEP3_DMA_L
                                    201 	.globl _UEP3_DMA
                                    202 	.globl _UEP2_DMA_H
                                    203 	.globl _UEP2_DMA_L
                                    204 	.globl _UEP2_DMA
                                    205 	.globl _USB_DEV_AD
                                    206 	.globl _USB_CTRL
                                    207 	.globl _USB_INT_EN
                                    208 	.globl _UEP4_T_LEN
                                    209 	.globl _UEP4_CTRL
                                    210 	.globl _UEP0_T_LEN
                                    211 	.globl _UEP0_CTRL
                                    212 	.globl _USB_RX_LEN
                                    213 	.globl _USB_MIS_ST
                                    214 	.globl _USB_INT_ST
                                    215 	.globl _USB_INT_FG
                                    216 	.globl _UEP3_T_LEN
                                    217 	.globl _UEP3_CTRL
                                    218 	.globl _UEP2_T_LEN
                                    219 	.globl _UEP2_CTRL
                                    220 	.globl _UEP1_T_LEN
                                    221 	.globl _UEP1_CTRL
                                    222 	.globl _UDEV_CTRL
                                    223 	.globl _USB_C_CTRL
                                    224 	.globl _ADC_PIN
                                    225 	.globl _ADC_CHAN
                                    226 	.globl _ADC_DAT_H
                                    227 	.globl _ADC_DAT_L
                                    228 	.globl _ADC_DAT
                                    229 	.globl _ADC_CFG
                                    230 	.globl _ADC_CTRL
                                    231 	.globl _TKEY_CTRL
                                    232 	.globl _SIF3
                                    233 	.globl _SBAUD3
                                    234 	.globl _SBUF3
                                    235 	.globl _SCON3
                                    236 	.globl _SIF2
                                    237 	.globl _SBAUD2
                                    238 	.globl _SBUF2
                                    239 	.globl _SCON2
                                    240 	.globl _SIF1
                                    241 	.globl _SBAUD1
                                    242 	.globl _SBUF1
                                    243 	.globl _SCON1
                                    244 	.globl _SPI0_SETUP
                                    245 	.globl _SPI0_CK_SE
                                    246 	.globl _SPI0_CTRL
                                    247 	.globl _SPI0_DATA
                                    248 	.globl _SPI0_STAT
                                    249 	.globl _PWM_DATA7
                                    250 	.globl _PWM_DATA6
                                    251 	.globl _PWM_DATA5
                                    252 	.globl _PWM_DATA4
                                    253 	.globl _PWM_DATA3
                                    254 	.globl _PWM_CTRL2
                                    255 	.globl _PWM_CK_SE
                                    256 	.globl _PWM_CTRL
                                    257 	.globl _PWM_DATA0
                                    258 	.globl _PWM_DATA1
                                    259 	.globl _PWM_DATA2
                                    260 	.globl _T2CAP1H
                                    261 	.globl _T2CAP1L
                                    262 	.globl _T2CAP1
                                    263 	.globl _TH2
                                    264 	.globl _TL2
                                    265 	.globl _T2COUNT
                                    266 	.globl _RCAP2H
                                    267 	.globl _RCAP2L
                                    268 	.globl _RCAP2
                                    269 	.globl _T2MOD
                                    270 	.globl _T2CON
                                    271 	.globl _T2CAP0H
                                    272 	.globl _T2CAP0L
                                    273 	.globl _T2CAP0
                                    274 	.globl _T2CON2
                                    275 	.globl _SBUF
                                    276 	.globl _SCON
                                    277 	.globl _TH1
                                    278 	.globl _TH0
                                    279 	.globl _TL1
                                    280 	.globl _TL0
                                    281 	.globl _TMOD
                                    282 	.globl _TCON
                                    283 	.globl _XBUS_AUX
                                    284 	.globl _PIN_FUNC
                                    285 	.globl _P5
                                    286 	.globl _P4_DIR_PU
                                    287 	.globl _P4_MOD_OC
                                    288 	.globl _P4
                                    289 	.globl _P3_DIR_PU
                                    290 	.globl _P3_MOD_OC
                                    291 	.globl _P3
                                    292 	.globl _P2_DIR_PU
                                    293 	.globl _P2_MOD_OC
                                    294 	.globl _P2
                                    295 	.globl _P1_DIR_PU
                                    296 	.globl _P1_MOD_OC
                                    297 	.globl _P1
                                    298 	.globl _P0_DIR_PU
                                    299 	.globl _P0_MOD_OC
                                    300 	.globl _P0
                                    301 	.globl _ROM_CTRL
                                    302 	.globl _ROM_DATA_HH
                                    303 	.globl _ROM_DATA_HL
                                    304 	.globl _ROM_DATA_HI
                                    305 	.globl _ROM_ADDR_H
                                    306 	.globl _ROM_ADDR_L
                                    307 	.globl _ROM_ADDR
                                    308 	.globl _GPIO_IE
                                    309 	.globl _INTX
                                    310 	.globl _IP_EX
                                    311 	.globl _IE_EX
                                    312 	.globl _IP
                                    313 	.globl _IE
                                    314 	.globl _WDOG_COUNT
                                    315 	.globl _RESET_KEEP
                                    316 	.globl _WAKE_CTRL
                                    317 	.globl _CLOCK_CFG
                                    318 	.globl _POWER_CFG
                                    319 	.globl _PCON
                                    320 	.globl _GLOBAL_CFG
                                    321 	.globl _SAFE_MOD
                                    322 	.globl _DPH
                                    323 	.globl _DPL
                                    324 	.globl _SP
                                    325 	.globl _A_INV
                                    326 	.globl _B
                                    327 	.globl _ACC
                                    328 	.globl _PSW
                                    329 	.globl _GPIO_Init
                                    330 	.globl _GPIO_INT_Init
                                    331 ;--------------------------------------------------------
                                    332 ; special function registers
                                    333 ;--------------------------------------------------------
                                    334 	.area RSEG    (ABS,DATA)
      000000                        335 	.org 0x0000
                           0000D0   336 _PSW	=	0x00d0
                           0000E0   337 _ACC	=	0x00e0
                           0000F0   338 _B	=	0x00f0
                           0000FD   339 _A_INV	=	0x00fd
                           000081   340 _SP	=	0x0081
                           000082   341 _DPL	=	0x0082
                           000083   342 _DPH	=	0x0083
                           0000A1   343 _SAFE_MOD	=	0x00a1
                           0000B1   344 _GLOBAL_CFG	=	0x00b1
                           000087   345 _PCON	=	0x0087
                           0000BA   346 _POWER_CFG	=	0x00ba
                           0000B9   347 _CLOCK_CFG	=	0x00b9
                           0000A9   348 _WAKE_CTRL	=	0x00a9
                           0000FE   349 _RESET_KEEP	=	0x00fe
                           0000FF   350 _WDOG_COUNT	=	0x00ff
                           0000A8   351 _IE	=	0x00a8
                           0000B8   352 _IP	=	0x00b8
                           0000E8   353 _IE_EX	=	0x00e8
                           0000E9   354 _IP_EX	=	0x00e9
                           0000B3   355 _INTX	=	0x00b3
                           0000B2   356 _GPIO_IE	=	0x00b2
                           008584   357 _ROM_ADDR	=	0x8584
                           000084   358 _ROM_ADDR_L	=	0x0084
                           000085   359 _ROM_ADDR_H	=	0x0085
                           008F8E   360 _ROM_DATA_HI	=	0x8f8e
                           00008E   361 _ROM_DATA_HL	=	0x008e
                           00008F   362 _ROM_DATA_HH	=	0x008f
                           000086   363 _ROM_CTRL	=	0x0086
                           000080   364 _P0	=	0x0080
                           0000C4   365 _P0_MOD_OC	=	0x00c4
                           0000C5   366 _P0_DIR_PU	=	0x00c5
                           000090   367 _P1	=	0x0090
                           000092   368 _P1_MOD_OC	=	0x0092
                           000093   369 _P1_DIR_PU	=	0x0093
                           0000A0   370 _P2	=	0x00a0
                           000094   371 _P2_MOD_OC	=	0x0094
                           000095   372 _P2_DIR_PU	=	0x0095
                           0000B0   373 _P3	=	0x00b0
                           000096   374 _P3_MOD_OC	=	0x0096
                           000097   375 _P3_DIR_PU	=	0x0097
                           0000C0   376 _P4	=	0x00c0
                           0000C2   377 _P4_MOD_OC	=	0x00c2
                           0000C3   378 _P4_DIR_PU	=	0x00c3
                           0000AB   379 _P5	=	0x00ab
                           0000AA   380 _PIN_FUNC	=	0x00aa
                           0000A2   381 _XBUS_AUX	=	0x00a2
                           000088   382 _TCON	=	0x0088
                           000089   383 _TMOD	=	0x0089
                           00008A   384 _TL0	=	0x008a
                           00008B   385 _TL1	=	0x008b
                           00008C   386 _TH0	=	0x008c
                           00008D   387 _TH1	=	0x008d
                           000098   388 _SCON	=	0x0098
                           000099   389 _SBUF	=	0x0099
                           0000C1   390 _T2CON2	=	0x00c1
                           00C7C6   391 _T2CAP0	=	0xc7c6
                           0000C6   392 _T2CAP0L	=	0x00c6
                           0000C7   393 _T2CAP0H	=	0x00c7
                           0000C8   394 _T2CON	=	0x00c8
                           0000C9   395 _T2MOD	=	0x00c9
                           00CBCA   396 _RCAP2	=	0xcbca
                           0000CA   397 _RCAP2L	=	0x00ca
                           0000CB   398 _RCAP2H	=	0x00cb
                           00CDCC   399 _T2COUNT	=	0xcdcc
                           0000CC   400 _TL2	=	0x00cc
                           0000CD   401 _TH2	=	0x00cd
                           00CFCE   402 _T2CAP1	=	0xcfce
                           0000CE   403 _T2CAP1L	=	0x00ce
                           0000CF   404 _T2CAP1H	=	0x00cf
                           00009A   405 _PWM_DATA2	=	0x009a
                           00009B   406 _PWM_DATA1	=	0x009b
                           00009C   407 _PWM_DATA0	=	0x009c
                           00009D   408 _PWM_CTRL	=	0x009d
                           00009E   409 _PWM_CK_SE	=	0x009e
                           00009F   410 _PWM_CTRL2	=	0x009f
                           0000A3   411 _PWM_DATA3	=	0x00a3
                           0000A4   412 _PWM_DATA4	=	0x00a4
                           0000A5   413 _PWM_DATA5	=	0x00a5
                           0000A6   414 _PWM_DATA6	=	0x00a6
                           0000A7   415 _PWM_DATA7	=	0x00a7
                           0000F8   416 _SPI0_STAT	=	0x00f8
                           0000F9   417 _SPI0_DATA	=	0x00f9
                           0000FA   418 _SPI0_CTRL	=	0x00fa
                           0000FB   419 _SPI0_CK_SE	=	0x00fb
                           0000FC   420 _SPI0_SETUP	=	0x00fc
                           0000BC   421 _SCON1	=	0x00bc
                           0000BD   422 _SBUF1	=	0x00bd
                           0000BE   423 _SBAUD1	=	0x00be
                           0000BF   424 _SIF1	=	0x00bf
                           0000B4   425 _SCON2	=	0x00b4
                           0000B5   426 _SBUF2	=	0x00b5
                           0000B6   427 _SBAUD2	=	0x00b6
                           0000B7   428 _SIF2	=	0x00b7
                           0000AC   429 _SCON3	=	0x00ac
                           0000AD   430 _SBUF3	=	0x00ad
                           0000AE   431 _SBAUD3	=	0x00ae
                           0000AF   432 _SIF3	=	0x00af
                           0000F1   433 _TKEY_CTRL	=	0x00f1
                           0000F2   434 _ADC_CTRL	=	0x00f2
                           0000F3   435 _ADC_CFG	=	0x00f3
                           00F5F4   436 _ADC_DAT	=	0xf5f4
                           0000F4   437 _ADC_DAT_L	=	0x00f4
                           0000F5   438 _ADC_DAT_H	=	0x00f5
                           0000F6   439 _ADC_CHAN	=	0x00f6
                           0000F7   440 _ADC_PIN	=	0x00f7
                           000091   441 _USB_C_CTRL	=	0x0091
                           0000D1   442 _UDEV_CTRL	=	0x00d1
                           0000D2   443 _UEP1_CTRL	=	0x00d2
                           0000D3   444 _UEP1_T_LEN	=	0x00d3
                           0000D4   445 _UEP2_CTRL	=	0x00d4
                           0000D5   446 _UEP2_T_LEN	=	0x00d5
                           0000D6   447 _UEP3_CTRL	=	0x00d6
                           0000D7   448 _UEP3_T_LEN	=	0x00d7
                           0000D8   449 _USB_INT_FG	=	0x00d8
                           0000D9   450 _USB_INT_ST	=	0x00d9
                           0000DA   451 _USB_MIS_ST	=	0x00da
                           0000DB   452 _USB_RX_LEN	=	0x00db
                           0000DC   453 _UEP0_CTRL	=	0x00dc
                           0000DD   454 _UEP0_T_LEN	=	0x00dd
                           0000DE   455 _UEP4_CTRL	=	0x00de
                           0000DF   456 _UEP4_T_LEN	=	0x00df
                           0000E1   457 _USB_INT_EN	=	0x00e1
                           0000E2   458 _USB_CTRL	=	0x00e2
                           0000E3   459 _USB_DEV_AD	=	0x00e3
                           00E5E4   460 _UEP2_DMA	=	0xe5e4
                           0000E4   461 _UEP2_DMA_L	=	0x00e4
                           0000E5   462 _UEP2_DMA_H	=	0x00e5
                           00E7E6   463 _UEP3_DMA	=	0xe7e6
                           0000E6   464 _UEP3_DMA_L	=	0x00e6
                           0000E7   465 _UEP3_DMA_H	=	0x00e7
                           0000EA   466 _UEP4_1_MOD	=	0x00ea
                           0000EB   467 _UEP2_3_MOD	=	0x00eb
                           00EDEC   468 _UEP0_DMA	=	0xedec
                           0000EC   469 _UEP0_DMA_L	=	0x00ec
                           0000ED   470 _UEP0_DMA_H	=	0x00ed
                           00EFEE   471 _UEP1_DMA	=	0xefee
                           0000EE   472 _UEP1_DMA_L	=	0x00ee
                           0000EF   473 _UEP1_DMA_H	=	0x00ef
                                    474 ;--------------------------------------------------------
                                    475 ; special function bits
                                    476 ;--------------------------------------------------------
                                    477 	.area RSEG    (ABS,DATA)
      000000                        478 	.org 0x0000
                           0000D7   479 _CY	=	0x00d7
                           0000D6   480 _AC	=	0x00d6
                           0000D5   481 _F0	=	0x00d5
                           0000D4   482 _RS1	=	0x00d4
                           0000D3   483 _RS0	=	0x00d3
                           0000D2   484 _OV	=	0x00d2
                           0000D1   485 _F1	=	0x00d1
                           0000D0   486 _P	=	0x00d0
                           0000AF   487 _EA	=	0x00af
                           0000AE   488 _E_DIS	=	0x00ae
                           0000AD   489 _ET2	=	0x00ad
                           0000AC   490 _ES	=	0x00ac
                           0000AB   491 _ET1	=	0x00ab
                           0000AA   492 _EX1	=	0x00aa
                           0000A9   493 _ET0	=	0x00a9
                           0000A8   494 _EX0	=	0x00a8
                           0000BF   495 _PH_FLAG	=	0x00bf
                           0000BE   496 _PL_FLAG	=	0x00be
                           0000BD   497 _PT2	=	0x00bd
                           0000BC   498 _PS	=	0x00bc
                           0000BB   499 _PT1	=	0x00bb
                           0000BA   500 _PX1	=	0x00ba
                           0000B9   501 _PT0	=	0x00b9
                           0000B8   502 _PX0	=	0x00b8
                           0000EF   503 _IE_WDOG	=	0x00ef
                           0000EE   504 _IE_GPIO	=	0x00ee
                           0000ED   505 _IE_PWMX	=	0x00ed
                           0000ED   506 _IE_UART3	=	0x00ed
                           0000EC   507 _IE_UART1	=	0x00ec
                           0000EB   508 _IE_ADC	=	0x00eb
                           0000EB   509 _IE_UART2	=	0x00eb
                           0000EA   510 _IE_USB	=	0x00ea
                           0000E9   511 _IE_INT3	=	0x00e9
                           0000E8   512 _IE_SPI0	=	0x00e8
                           000087   513 _P0_7	=	0x0087
                           000086   514 _P0_6	=	0x0086
                           000085   515 _P0_5	=	0x0085
                           000084   516 _P0_4	=	0x0084
                           000083   517 _P0_3	=	0x0083
                           000082   518 _P0_2	=	0x0082
                           000081   519 _P0_1	=	0x0081
                           000080   520 _P0_0	=	0x0080
                           000087   521 _TXD3	=	0x0087
                           000087   522 _AIN15	=	0x0087
                           000086   523 _RXD3	=	0x0086
                           000086   524 _AIN14	=	0x0086
                           000085   525 _TXD2	=	0x0085
                           000085   526 _AIN13	=	0x0085
                           000084   527 _RXD2	=	0x0084
                           000084   528 _AIN12	=	0x0084
                           000083   529 _TXD_	=	0x0083
                           000083   530 _AIN11	=	0x0083
                           000082   531 _RXD_	=	0x0082
                           000082   532 _AIN10	=	0x0082
                           000081   533 _AIN9	=	0x0081
                           000080   534 _AIN8	=	0x0080
                           000097   535 _P1_7	=	0x0097
                           000096   536 _P1_6	=	0x0096
                           000095   537 _P1_5	=	0x0095
                           000094   538 _P1_4	=	0x0094
                           000093   539 _P1_3	=	0x0093
                           000092   540 _P1_2	=	0x0092
                           000091   541 _P1_1	=	0x0091
                           000090   542 _P1_0	=	0x0090
                           000097   543 _SCK	=	0x0097
                           000097   544 _TXD1_	=	0x0097
                           000097   545 _AIN7	=	0x0097
                           000096   546 _MISO	=	0x0096
                           000096   547 _RXD1_	=	0x0096
                           000096   548 _VBUS	=	0x0096
                           000096   549 _AIN6	=	0x0096
                           000095   550 _MOSI	=	0x0095
                           000095   551 _PWM0_	=	0x0095
                           000095   552 _UCC2	=	0x0095
                           000095   553 _AIN5	=	0x0095
                           000094   554 _SCS	=	0x0094
                           000094   555 _UCC1	=	0x0094
                           000094   556 _AIN4	=	0x0094
                           000093   557 _AIN3	=	0x0093
                           000092   558 _AIN2	=	0x0092
                           000091   559 _T2EX	=	0x0091
                           000091   560 _CAP2	=	0x0091
                           000091   561 _AIN1	=	0x0091
                           000090   562 _T2	=	0x0090
                           000090   563 _CAP1	=	0x0090
                           000090   564 _AIN0	=	0x0090
                           0000A7   565 _P2_7	=	0x00a7
                           0000A6   566 _P2_6	=	0x00a6
                           0000A5   567 _P2_5	=	0x00a5
                           0000A4   568 _P2_4	=	0x00a4
                           0000A3   569 _P2_3	=	0x00a3
                           0000A2   570 _P2_2	=	0x00a2
                           0000A1   571 _P2_1	=	0x00a1
                           0000A0   572 _P2_0	=	0x00a0
                           0000A7   573 _PWM7	=	0x00a7
                           0000A7   574 _TXD1	=	0x00a7
                           0000A6   575 _PWM6	=	0x00a6
                           0000A6   576 _RXD1	=	0x00a6
                           0000A5   577 _PWM0	=	0x00a5
                           0000A5   578 _T2EX_	=	0x00a5
                           0000A5   579 _CAP2_	=	0x00a5
                           0000A4   580 _PWM1	=	0x00a4
                           0000A4   581 _T2_	=	0x00a4
                           0000A4   582 _CAP1_	=	0x00a4
                           0000A3   583 _PWM2	=	0x00a3
                           0000A2   584 _PWM3	=	0x00a2
                           0000A2   585 _INT0_	=	0x00a2
                           0000A1   586 _PWM4	=	0x00a1
                           0000A0   587 _PWM5	=	0x00a0
                           0000B7   588 _P3_7	=	0x00b7
                           0000B6   589 _P3_6	=	0x00b6
                           0000B5   590 _P3_5	=	0x00b5
                           0000B4   591 _P3_4	=	0x00b4
                           0000B3   592 _P3_3	=	0x00b3
                           0000B2   593 _P3_2	=	0x00b2
                           0000B1   594 _P3_1	=	0x00b1
                           0000B0   595 _P3_0	=	0x00b0
                           0000B7   596 _INT3	=	0x00b7
                           0000B6   597 _CAP0	=	0x00b6
                           0000B5   598 _T1	=	0x00b5
                           0000B4   599 _T0	=	0x00b4
                           0000B3   600 _INT1	=	0x00b3
                           0000B2   601 _INT0	=	0x00b2
                           0000B1   602 _TXD	=	0x00b1
                           0000B0   603 _RXD	=	0x00b0
                           0000C6   604 _P4_6	=	0x00c6
                           0000C5   605 _P4_5	=	0x00c5
                           0000C4   606 _P4_4	=	0x00c4
                           0000C3   607 _P4_3	=	0x00c3
                           0000C2   608 _P4_2	=	0x00c2
                           0000C1   609 _P4_1	=	0x00c1
                           0000C0   610 _P4_0	=	0x00c0
                           0000C7   611 _XO	=	0x00c7
                           0000C6   612 _XI	=	0x00c6
                           00008F   613 _TF1	=	0x008f
                           00008E   614 _TR1	=	0x008e
                           00008D   615 _TF0	=	0x008d
                           00008C   616 _TR0	=	0x008c
                           00008B   617 _IE1	=	0x008b
                           00008A   618 _IT1	=	0x008a
                           000089   619 _IE0	=	0x0089
                           000088   620 _IT0	=	0x0088
                           00009F   621 _SM0	=	0x009f
                           00009E   622 _SM1	=	0x009e
                           00009D   623 _SM2	=	0x009d
                           00009C   624 _REN	=	0x009c
                           00009B   625 _TB8	=	0x009b
                           00009A   626 _RB8	=	0x009a
                           000099   627 _TI	=	0x0099
                           000098   628 _RI	=	0x0098
                           0000CF   629 _TF2	=	0x00cf
                           0000CF   630 _CAP1F	=	0x00cf
                           0000CE   631 _EXF2	=	0x00ce
                           0000CD   632 _RCLK	=	0x00cd
                           0000CC   633 _TCLK	=	0x00cc
                           0000CB   634 _EXEN2	=	0x00cb
                           0000CA   635 _TR2	=	0x00ca
                           0000C9   636 _C_T2	=	0x00c9
                           0000C8   637 _CP_RL2	=	0x00c8
                           0000FF   638 _S0_FST_ACT	=	0x00ff
                           0000FE   639 _S0_IF_OV	=	0x00fe
                           0000FD   640 _S0_IF_FIRST	=	0x00fd
                           0000FC   641 _S0_IF_BYTE	=	0x00fc
                           0000FB   642 _S0_FREE	=	0x00fb
                           0000FA   643 _S0_T_FIFO	=	0x00fa
                           0000F8   644 _S0_R_FIFO	=	0x00f8
                           0000DF   645 _U_IS_NAK	=	0x00df
                           0000DE   646 _U_TOG_OK	=	0x00de
                           0000DD   647 _U_SIE_FREE	=	0x00dd
                           0000DC   648 _UIF_FIFO_OV	=	0x00dc
                           0000DB   649 _UIF_HST_SOF	=	0x00db
                           0000DA   650 _UIF_SUSPEND	=	0x00da
                           0000D9   651 _UIF_TRANSFER	=	0x00d9
                           0000D8   652 _UIF_DETECT	=	0x00d8
                           0000D8   653 _UIF_BUS_RST	=	0x00d8
                                    654 ;--------------------------------------------------------
                                    655 ; overlayable register banks
                                    656 ;--------------------------------------------------------
                                    657 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        658 	.ds 8
                                    659 	.area REG_BANK_1	(REL,OVR,DATA)
      000008                        660 	.ds 8
                                    661 ;--------------------------------------------------------
                                    662 ; overlayable bit register bank
                                    663 ;--------------------------------------------------------
                                    664 	.area BIT_BANK	(REL,OVR,DATA)
      000021                        665 bits:
      000021                        666 	.ds 1
                           008000   667 	b0 = bits[0]
                           008100   668 	b1 = bits[1]
                           008200   669 	b2 = bits[2]
                           008300   670 	b3 = bits[3]
                           008400   671 	b4 = bits[4]
                           008500   672 	b5 = bits[5]
                           008600   673 	b6 = bits[6]
                           008700   674 	b7 = bits[7]
                                    675 ;--------------------------------------------------------
                                    676 ; internal ram data
                                    677 ;--------------------------------------------------------
                                    678 	.area DSEG    (DATA)
                                    679 ;--------------------------------------------------------
                                    680 ; overlayable items in internal ram 
                                    681 ;--------------------------------------------------------
                                    682 ;--------------------------------------------------------
                                    683 ; indirectly addressable internal ram data
                                    684 ;--------------------------------------------------------
                                    685 	.area ISEG    (DATA)
                                    686 ;--------------------------------------------------------
                                    687 ; absolute internal ram data
                                    688 ;--------------------------------------------------------
                                    689 	.area IABS    (ABS,DATA)
                                    690 	.area IABS    (ABS,DATA)
                                    691 ;--------------------------------------------------------
                                    692 ; bit data
                                    693 ;--------------------------------------------------------
                                    694 	.area BSEG    (BIT)
                                    695 ;--------------------------------------------------------
                                    696 ; paged external ram data
                                    697 ;--------------------------------------------------------
                                    698 	.area PSEG    (PAG,XDATA)
                                    699 ;--------------------------------------------------------
                                    700 ; external ram data
                                    701 ;--------------------------------------------------------
                                    702 	.area XSEG    (XDATA)
                                    703 ;--------------------------------------------------------
                                    704 ; absolute external ram data
                                    705 ;--------------------------------------------------------
                                    706 	.area XABS    (ABS,XDATA)
                                    707 ;--------------------------------------------------------
                                    708 ; external initialized ram data
                                    709 ;--------------------------------------------------------
                                    710 	.area HOME    (CODE)
                                    711 	.area GSINIT0 (CODE)
                                    712 	.area GSINIT1 (CODE)
                                    713 	.area GSINIT2 (CODE)
                                    714 	.area GSINIT3 (CODE)
                                    715 	.area GSINIT4 (CODE)
                                    716 	.area GSINIT5 (CODE)
                                    717 	.area GSINIT  (CODE)
                                    718 	.area GSFINAL (CODE)
                                    719 	.area CSEG    (CODE)
                                    720 ;--------------------------------------------------------
                                    721 ; global & static initialisations
                                    722 ;--------------------------------------------------------
                                    723 	.area HOME    (CODE)
                                    724 	.area GSINIT  (CODE)
                                    725 	.area GSFINAL (CODE)
                                    726 	.area GSINIT  (CODE)
                                    727 ;--------------------------------------------------------
                                    728 ; Home
                                    729 ;--------------------------------------------------------
                                    730 	.area HOME    (CODE)
                                    731 	.area HOME    (CODE)
                                    732 ;--------------------------------------------------------
                                    733 ; code
                                    734 ;--------------------------------------------------------
                                    735 	.area CSEG    (CODE)
                                    736 ;------------------------------------------------------------
                                    737 ;Allocation info for local variables in function 'GPIO_Init'
                                    738 ;------------------------------------------------------------
                                    739 ;PINx                      Allocated to stack - _bp -3
                                    740 ;MODEx                     Allocated to stack - _bp -4
                                    741 ;PORTx                     Allocated to registers r7 
                                    742 ;Px_DIR_PU                 Allocated to stack - _bp +2
                                    743 ;Px_MOD_OC                 Allocated to stack - _bp +1
                                    744 ;------------------------------------------------------------
                                    745 ;	source/CH549_GPIO.c:22: void GPIO_Init(UINT8 PORTx,UINT8 PINx,UINT8 MODEx)
                                    746 ;	-----------------------------------------
                                    747 ;	 function GPIO_Init
                                    748 ;	-----------------------------------------
      00137C                        749 _GPIO_Init:
                           000007   750 	ar7 = 0x07
                           000006   751 	ar6 = 0x06
                           000005   752 	ar5 = 0x05
                           000004   753 	ar4 = 0x04
                           000003   754 	ar3 = 0x03
                           000002   755 	ar2 = 0x02
                           000001   756 	ar1 = 0x01
                           000000   757 	ar0 = 0x00
      00137C C0 16            [24]  758 	push	_bp
      00137E 85 81 16         [24]  759 	mov	_bp,sp
      001381 05 81            [12]  760 	inc	sp
      001383 05 81            [12]  761 	inc	sp
      001385 AF 82            [24]  762 	mov	r7,dpl
                                    763 ;	source/CH549_GPIO.c:25: switch(PORTx)                                         //读出初始值
      001387 C3               [12]  764 	clr	c
      001388 74 04            [12]  765 	mov	a,#0x04
      00138A 9F               [12]  766 	subb	a,r7
      00138B E4               [12]  767 	clr	a
      00138C 33               [12]  768 	rlc	a
      00138D FE               [12]  769 	mov	r6,a
      00138E 70 2C            [24]  770 	jnz	00107$
      001390 EF               [12]  771 	mov	a,r7
      001391 2F               [12]  772 	add	a,r7
                                    773 ;	source/CH549_GPIO.c:27: case PORT0:
      001392 90 13 96         [24]  774 	mov	dptr,#00136$
      001395 73               [24]  775 	jmp	@a+dptr
      001396                        776 00136$:
      001396 80 08            [24]  777 	sjmp	00101$
      001398 80 0C            [24]  778 	sjmp	00102$
      00139A 80 10            [24]  779 	sjmp	00103$
      00139C 80 14            [24]  780 	sjmp	00104$
      00139E 80 18            [24]  781 	sjmp	00105$
      0013A0                        782 00101$:
                                    783 ;	source/CH549_GPIO.c:28: Px_MOD_OC = P0_MOD_OC;
      0013A0 AD C4            [24]  784 	mov	r5,_P0_MOD_OC
                                    785 ;	source/CH549_GPIO.c:29: Px_DIR_PU = P0_DIR_PU;
      0013A2 AC C5            [24]  786 	mov	r4,_P0_DIR_PU
                                    787 ;	source/CH549_GPIO.c:30: break;
                                    788 ;	source/CH549_GPIO.c:31: case PORT1:
      0013A4 80 16            [24]  789 	sjmp	00107$
      0013A6                        790 00102$:
                                    791 ;	source/CH549_GPIO.c:32: Px_MOD_OC = P1_MOD_OC;
      0013A6 AD 92            [24]  792 	mov	r5,_P1_MOD_OC
                                    793 ;	source/CH549_GPIO.c:33: Px_DIR_PU = P1_DIR_PU;
      0013A8 AC 93            [24]  794 	mov	r4,_P1_DIR_PU
                                    795 ;	source/CH549_GPIO.c:34: break;
                                    796 ;	source/CH549_GPIO.c:35: case PORT2:
      0013AA 80 10            [24]  797 	sjmp	00107$
      0013AC                        798 00103$:
                                    799 ;	source/CH549_GPIO.c:36: Px_MOD_OC = P2_MOD_OC;
      0013AC AD 94            [24]  800 	mov	r5,_P2_MOD_OC
                                    801 ;	source/CH549_GPIO.c:37: Px_DIR_PU = P2_DIR_PU;
      0013AE AC 95            [24]  802 	mov	r4,_P2_DIR_PU
                                    803 ;	source/CH549_GPIO.c:38: break;
                                    804 ;	source/CH549_GPIO.c:39: case PORT3:
      0013B0 80 0A            [24]  805 	sjmp	00107$
      0013B2                        806 00104$:
                                    807 ;	source/CH549_GPIO.c:40: Px_MOD_OC = P3_MOD_OC;
      0013B2 AD 96            [24]  808 	mov	r5,_P3_MOD_OC
                                    809 ;	source/CH549_GPIO.c:41: Px_DIR_PU = P3_DIR_PU;
      0013B4 AC 97            [24]  810 	mov	r4,_P3_DIR_PU
                                    811 ;	source/CH549_GPIO.c:42: break;
                                    812 ;	source/CH549_GPIO.c:43: case PORT4:
      0013B6 80 04            [24]  813 	sjmp	00107$
      0013B8                        814 00105$:
                                    815 ;	source/CH549_GPIO.c:44: Px_MOD_OC = P4_MOD_OC;
      0013B8 AD C2            [24]  816 	mov	r5,_P4_MOD_OC
                                    817 ;	source/CH549_GPIO.c:45: Px_DIR_PU = P4_DIR_PU;
      0013BA AC C3            [24]  818 	mov	r4,_P4_DIR_PU
                                    819 ;	source/CH549_GPIO.c:49: }
      0013BC                        820 00107$:
                                    821 ;	source/CH549_GPIO.c:50: switch(MODEx)
      0013BC E5 16            [12]  822 	mov	a,_bp
      0013BE 24 FC            [12]  823 	add	a,#0xfc
      0013C0 F8               [12]  824 	mov	r0,a
      0013C1 E6               [12]  825 	mov	a,@r0
      0013C2 24 FC            [12]  826 	add	a,#0xff - 0x03
      0013C4 40 5A            [24]  827 	jc	00113$
      0013C6 E5 16            [12]  828 	mov	a,_bp
      0013C8 24 FC            [12]  829 	add	a,#0xfc
      0013CA F8               [12]  830 	mov	r0,a
      0013CB E6               [12]  831 	mov	a,@r0
      0013CC 26               [12]  832 	add	a,@r0
                                    833 ;	source/CH549_GPIO.c:52: case MODE0:                                       //高阻输入模式，引脚没有上拉电阻
      0013CD 90 13 D1         [24]  834 	mov	dptr,#00138$
      0013D0 73               [24]  835 	jmp	@a+dptr
      0013D1                        836 00138$:
      0013D1 80 06            [24]  837 	sjmp	00108$
      0013D3 80 13            [24]  838 	sjmp	00109$
      0013D5 80 25            [24]  839 	sjmp	00110$
      0013D7 80 37            [24]  840 	sjmp	00111$
      0013D9                        841 00108$:
                                    842 ;	source/CH549_GPIO.c:53: Px_MOD_OC &= ~PINx;
      0013D9 E5 16            [12]  843 	mov	a,_bp
      0013DB 24 FD            [12]  844 	add	a,#0xfd
      0013DD F8               [12]  845 	mov	r0,a
      0013DE E6               [12]  846 	mov	a,@r0
      0013DF F4               [12]  847 	cpl	a
      0013E0 FB               [12]  848 	mov	r3,a
      0013E1 52 05            [12]  849 	anl	ar5,a
                                    850 ;	source/CH549_GPIO.c:54: Px_DIR_PU &= ~PINx;
      0013E3 EB               [12]  851 	mov	a,r3
      0013E4 52 04            [12]  852 	anl	ar4,a
                                    853 ;	source/CH549_GPIO.c:55: break;
                                    854 ;	source/CH549_GPIO.c:56: case MODE1:                                       //推挽输出模式，具有对称驱动能力，可以输出或者吸收较大电流
      0013E6 80 38            [24]  855 	sjmp	00113$
      0013E8                        856 00109$:
                                    857 ;	source/CH549_GPIO.c:57: Px_MOD_OC &= ~PINx;
      0013E8 E5 16            [12]  858 	mov	a,_bp
      0013EA 24 FD            [12]  859 	add	a,#0xfd
      0013EC F8               [12]  860 	mov	r0,a
      0013ED E6               [12]  861 	mov	a,@r0
      0013EE F4               [12]  862 	cpl	a
      0013EF FB               [12]  863 	mov	r3,a
      0013F0 52 05            [12]  864 	anl	ar5,a
                                    865 ;	source/CH549_GPIO.c:58: Px_DIR_PU |= PINx;
      0013F2 E5 16            [12]  866 	mov	a,_bp
      0013F4 24 FD            [12]  867 	add	a,#0xfd
      0013F6 F8               [12]  868 	mov	r0,a
      0013F7 E6               [12]  869 	mov	a,@r0
      0013F8 42 04            [12]  870 	orl	ar4,a
                                    871 ;	source/CH549_GPIO.c:59: break;
                                    872 ;	source/CH549_GPIO.c:60: case MODE2:                                       //开漏输出，支持高阻输入，引脚没有上拉电阻
      0013FA 80 24            [24]  873 	sjmp	00113$
      0013FC                        874 00110$:
                                    875 ;	source/CH549_GPIO.c:61: Px_MOD_OC |= PINx;
      0013FC E5 16            [12]  876 	mov	a,_bp
      0013FE 24 FD            [12]  877 	add	a,#0xfd
      001400 F8               [12]  878 	mov	r0,a
      001401 E6               [12]  879 	mov	a,@r0
      001402 42 05            [12]  880 	orl	ar5,a
                                    881 ;	source/CH549_GPIO.c:62: Px_DIR_PU &= ~PINx;
      001404 E5 16            [12]  882 	mov	a,_bp
      001406 24 FD            [12]  883 	add	a,#0xfd
      001408 F8               [12]  884 	mov	r0,a
      001409 E6               [12]  885 	mov	a,@r0
      00140A F4               [12]  886 	cpl	a
      00140B FB               [12]  887 	mov	r3,a
      00140C 52 04            [12]  888 	anl	ar4,a
                                    889 ;	source/CH549_GPIO.c:63: break;
                                    890 ;	source/CH549_GPIO.c:64: case MODE3:                                       //准双向模式(标准 8051)，开漏输出，支持输入，引脚有上拉电阻
      00140E 80 10            [24]  891 	sjmp	00113$
      001410                        892 00111$:
                                    893 ;	source/CH549_GPIO.c:65: Px_MOD_OC |= PINx;
      001410 E5 16            [12]  894 	mov	a,_bp
      001412 24 FD            [12]  895 	add	a,#0xfd
      001414 F8               [12]  896 	mov	r0,a
      001415 E6               [12]  897 	mov	a,@r0
      001416 42 05            [12]  898 	orl	ar5,a
                                    899 ;	source/CH549_GPIO.c:66: Px_DIR_PU |= PINx;
      001418 E5 16            [12]  900 	mov	a,_bp
      00141A 24 FD            [12]  901 	add	a,#0xfd
      00141C F8               [12]  902 	mov	r0,a
      00141D E6               [12]  903 	mov	a,@r0
      00141E 42 04            [12]  904 	orl	ar4,a
                                    905 ;	source/CH549_GPIO.c:70: }
      001420                        906 00113$:
                                    907 ;	source/CH549_GPIO.c:71: switch(PORTx)                                         //回写
      001420 EE               [12]  908 	mov	a,r6
      001421 70 2C            [24]  909 	jnz	00121$
      001423 EF               [12]  910 	mov	a,r7
      001424 2F               [12]  911 	add	a,r7
                                    912 ;	source/CH549_GPIO.c:73: case PORT0:
      001425 90 14 29         [24]  913 	mov	dptr,#00140$
      001428 73               [24]  914 	jmp	@a+dptr
      001429                        915 00140$:
      001429 80 08            [24]  916 	sjmp	00114$
      00142B 80 0C            [24]  917 	sjmp	00115$
      00142D 80 10            [24]  918 	sjmp	00116$
      00142F 80 14            [24]  919 	sjmp	00117$
      001431 80 18            [24]  920 	sjmp	00118$
      001433                        921 00114$:
                                    922 ;	source/CH549_GPIO.c:74: P0_MOD_OC = Px_MOD_OC;
      001433 8D C4            [24]  923 	mov	_P0_MOD_OC,r5
                                    924 ;	source/CH549_GPIO.c:75: P0_DIR_PU = Px_DIR_PU;
      001435 8C C5            [24]  925 	mov	_P0_DIR_PU,r4
                                    926 ;	source/CH549_GPIO.c:76: break;
                                    927 ;	source/CH549_GPIO.c:77: case PORT1:
      001437 80 16            [24]  928 	sjmp	00121$
      001439                        929 00115$:
                                    930 ;	source/CH549_GPIO.c:78: P1_MOD_OC = Px_MOD_OC;
      001439 8D 92            [24]  931 	mov	_P1_MOD_OC,r5
                                    932 ;	source/CH549_GPIO.c:79: P1_DIR_PU = Px_DIR_PU;
      00143B 8C 93            [24]  933 	mov	_P1_DIR_PU,r4
                                    934 ;	source/CH549_GPIO.c:80: break;
                                    935 ;	source/CH549_GPIO.c:81: case PORT2:
      00143D 80 10            [24]  936 	sjmp	00121$
      00143F                        937 00116$:
                                    938 ;	source/CH549_GPIO.c:82: P2_MOD_OC = Px_MOD_OC;
      00143F 8D 94            [24]  939 	mov	_P2_MOD_OC,r5
                                    940 ;	source/CH549_GPIO.c:83: P2_DIR_PU = Px_DIR_PU;
      001441 8C 95            [24]  941 	mov	_P2_DIR_PU,r4
                                    942 ;	source/CH549_GPIO.c:84: break;
                                    943 ;	source/CH549_GPIO.c:85: case PORT3:
      001443 80 0A            [24]  944 	sjmp	00121$
      001445                        945 00117$:
                                    946 ;	source/CH549_GPIO.c:86: P3_MOD_OC = Px_MOD_OC;
      001445 8D 96            [24]  947 	mov	_P3_MOD_OC,r5
                                    948 ;	source/CH549_GPIO.c:87: P3_DIR_PU = Px_DIR_PU;
      001447 8C 97            [24]  949 	mov	_P3_DIR_PU,r4
                                    950 ;	source/CH549_GPIO.c:88: break;
                                    951 ;	source/CH549_GPIO.c:89: case PORT4:
      001449 80 04            [24]  952 	sjmp	00121$
      00144B                        953 00118$:
                                    954 ;	source/CH549_GPIO.c:90: P4_MOD_OC = Px_MOD_OC;
      00144B 8D C2            [24]  955 	mov	_P4_MOD_OC,r5
                                    956 ;	source/CH549_GPIO.c:91: P4_DIR_PU = Px_DIR_PU;
      00144D 8C C3            [24]  957 	mov	_P4_DIR_PU,r4
                                    958 ;	source/CH549_GPIO.c:95: }
      00144F                        959 00121$:
                                    960 ;	source/CH549_GPIO.c:96: }
      00144F 85 16 81         [24]  961 	mov	sp,_bp
      001452 D0 16            [24]  962 	pop	_bp
      001454 22               [24]  963 	ret
                                    964 ;------------------------------------------------------------
                                    965 ;Allocation info for local variables in function 'GPIO_INT_Init'
                                    966 ;------------------------------------------------------------
                                    967 ;Mode                      Allocated to stack - _bp -3
                                    968 ;NewState                  Allocated to stack - _bp -4
                                    969 ;IntSrc                    Allocated to registers r6 r7 
                                    970 ;------------------------------------------------------------
                                    971 ;	source/CH549_GPIO.c:108: void GPIO_INT_Init( UINT16 IntSrc,UINT8 Mode,UINT8 NewState )
                                    972 ;	-----------------------------------------
                                    973 ;	 function GPIO_INT_Init
                                    974 ;	-----------------------------------------
      001455                        975 _GPIO_INT_Init:
      001455 C0 16            [24]  976 	push	_bp
      001457 85 81 16         [24]  977 	mov	_bp,sp
      00145A AE 82            [24]  978 	mov	r6,dpl
      00145C AF 83            [24]  979 	mov	r7,dph
                                    980 ;	source/CH549_GPIO.c:111: if(Mode == INT_EDGE)                                //边沿触发模式
      00145E E5 16            [12]  981 	mov	a,_bp
      001460 24 FD            [12]  982 	add	a,#0xfd
      001462 F8               [12]  983 	mov	r0,a
      001463 B6 01 16         [24]  984 	cjne	@r0,#0x01,00114$
                                    985 ;	source/CH549_GPIO.c:113: if(IntSrc & 0x7F)                               //存在扩展中断
      001466 EE               [12]  986 	mov	a,r6
      001467 54 7F            [12]  987 	anl	a,#0x7f
      001469 60 03            [24]  988 	jz	00102$
                                    989 ;	source/CH549_GPIO.c:115: GPIO_IE |= bIE_IO_EDGE;
      00146B 43 B2 80         [24]  990 	orl	_GPIO_IE,#0x80
      00146E                        991 00102$:
                                    992 ;	source/CH549_GPIO.c:117: if(IntSrc&INT_INT0_L)                           //存在外部中断0
      00146E EE               [12]  993 	mov	a,r6
      00146F 30 E7 02         [24]  994 	jnb	acc.7,00104$
                                    995 ;	source/CH549_GPIO.c:119: IT0 = 1;
                                    996 ;	assignBit
      001472 D2 88            [12]  997 	setb	_IT0
      001474                        998 00104$:
                                    999 ;	source/CH549_GPIO.c:121: if(IntSrc&INT_INT1_L)                           //存在外部中断1
      001474 EF               [12] 1000 	mov	a,r7
      001475 30 E0 18         [24] 1001 	jnb	acc.0,00115$
                                   1002 ;	source/CH549_GPIO.c:123: IT1 = 1;
                                   1003 ;	assignBit
      001478 D2 8A            [12] 1004 	setb	_IT1
      00147A 80 14            [24] 1005 	sjmp	00115$
      00147C                       1006 00114$:
                                   1007 ;	source/CH549_GPIO.c:128: if(IntSrc & 0x7F)                               //存在扩展中断
      00147C EE               [12] 1008 	mov	a,r6
      00147D 54 7F            [12] 1009 	anl	a,#0x7f
      00147F 60 03            [24] 1010 	jz	00108$
                                   1011 ;	source/CH549_GPIO.c:130: GPIO_IE &= ~bIE_IO_EDGE;
      001481 53 B2 7F         [24] 1012 	anl	_GPIO_IE,#0x7f
      001484                       1013 00108$:
                                   1014 ;	source/CH549_GPIO.c:132: if(IntSrc&INT_INT0_L)                           //存在外部中断0
      001484 EE               [12] 1015 	mov	a,r6
      001485 30 E7 02         [24] 1016 	jnb	acc.7,00110$
                                   1017 ;	source/CH549_GPIO.c:134: IT0 = 1;
                                   1018 ;	assignBit
      001488 D2 88            [12] 1019 	setb	_IT0
      00148A                       1020 00110$:
                                   1021 ;	source/CH549_GPIO.c:136: if(IntSrc&INT_INT1_L)                           //存在外部中断1
      00148A EF               [12] 1022 	mov	a,r7
      00148B 30 E0 02         [24] 1023 	jnb	acc.0,00115$
                                   1024 ;	source/CH549_GPIO.c:138: IT1 = 1;
                                   1025 ;	assignBit
      00148E D2 8A            [12] 1026 	setb	_IT1
      001490                       1027 00115$:
                                   1028 ;	source/CH549_GPIO.c:142: if(NewState == Enable)                              //开启外部中断
      001490 E5 16            [12] 1029 	mov	a,_bp
      001492 24 FC            [12] 1030 	add	a,#0xfc
      001494 F8               [12] 1031 	mov	r0,a
      001495 B6 01 20         [24] 1032 	cjne	@r0,#0x01,00129$
                                   1033 ;	source/CH549_GPIO.c:144: GPIO_IE |= ((UINT8)IntSrc&0x7F);
      001498 8E 05            [24] 1034 	mov	ar5,r6
      00149A 53 05 7F         [24] 1035 	anl	ar5,#0x7f
      00149D ED               [12] 1036 	mov	a,r5
      00149E 42 B2            [12] 1037 	orl	_GPIO_IE,a
                                   1038 ;	source/CH549_GPIO.c:145: if(IntSrc&INT_INT0_L)                           //存在外部中断0
      0014A0 EE               [12] 1039 	mov	a,r6
      0014A1 30 E7 02         [24] 1040 	jnb	acc.7,00117$
                                   1041 ;	source/CH549_GPIO.c:147: EX0 = 1;
                                   1042 ;	assignBit
      0014A4 D2 A8            [12] 1043 	setb	_EX0
      0014A6                       1044 00117$:
                                   1045 ;	source/CH549_GPIO.c:149: if(IntSrc&INT_INT1_L)                           //存在外部中断1
      0014A6 EF               [12] 1046 	mov	a,r7
      0014A7 30 E0 02         [24] 1047 	jnb	acc.0,00119$
                                   1048 ;	source/CH549_GPIO.c:151: EX1 = 1;
                                   1049 ;	assignBit
      0014AA D2 AA            [12] 1050 	setb	_EX1
      0014AC                       1051 00119$:
                                   1052 ;	source/CH549_GPIO.c:153: if(GPIO_IE&0x7F)
      0014AC E5 B2            [12] 1053 	mov	a,_GPIO_IE
      0014AE 54 7F            [12] 1054 	anl	a,#0x7f
      0014B0 60 02            [24] 1055 	jz	00121$
                                   1056 ;	source/CH549_GPIO.c:155: IE_GPIO = 1;                                //开启扩展GPIO中断
                                   1057 ;	assignBit
      0014B2 D2 EE            [12] 1058 	setb	_IE_GPIO
      0014B4                       1059 00121$:
                                   1060 ;	source/CH549_GPIO.c:157: EA = 1;                                         //开启总中断
                                   1061 ;	assignBit
      0014B4 D2 AF            [12] 1062 	setb	_EA
      0014B6 80 21            [24] 1063 	sjmp	00131$
      0014B8                       1064 00129$:
                                   1065 ;	source/CH549_GPIO.c:161: GPIO_IE &= ~((UINT8)IntSrc&0x7F);
      0014B8 8E 05            [24] 1066 	mov	ar5,r6
      0014BA 53 05 7F         [24] 1067 	anl	ar5,#0x7f
      0014BD ED               [12] 1068 	mov	a,r5
      0014BE F4               [12] 1069 	cpl	a
      0014BF FD               [12] 1070 	mov	r5,a
      0014C0 AC B2            [24] 1071 	mov	r4,_GPIO_IE
      0014C2 5C               [12] 1072 	anl	a,r4
      0014C3 F5 B2            [12] 1073 	mov	_GPIO_IE,a
                                   1074 ;	source/CH549_GPIO.c:162: if(IntSrc&INT_INT0_L)                           //存在外部中断0
      0014C5 EE               [12] 1075 	mov	a,r6
      0014C6 30 E7 02         [24] 1076 	jnb	acc.7,00123$
                                   1077 ;	source/CH549_GPIO.c:164: EX0 = 0;
                                   1078 ;	assignBit
      0014C9 C2 A8            [12] 1079 	clr	_EX0
      0014CB                       1080 00123$:
                                   1081 ;	source/CH549_GPIO.c:166: if(IntSrc&INT_INT1_L)                           //存在外部中断1
      0014CB EF               [12] 1082 	mov	a,r7
      0014CC 30 E0 02         [24] 1083 	jnb	acc.0,00125$
                                   1084 ;	source/CH549_GPIO.c:168: EX1 = 0;
                                   1085 ;	assignBit
      0014CF C2 AA            [12] 1086 	clr	_EX1
      0014D1                       1087 00125$:
                                   1088 ;	source/CH549_GPIO.c:170: if((GPIO_IE&0x7F)== 0)
      0014D1 E5 B2            [12] 1089 	mov	a,_GPIO_IE
      0014D3 54 7F            [12] 1090 	anl	a,#0x7f
      0014D5 70 02            [24] 1091 	jnz	00131$
                                   1092 ;	source/CH549_GPIO.c:172: IE_GPIO = 0;                                //关闭扩展GPIO中断
                                   1093 ;	assignBit
      0014D7 C2 EE            [12] 1094 	clr	_IE_GPIO
      0014D9                       1095 00131$:
                                   1096 ;	source/CH549_GPIO.c:175: }
      0014D9 D0 16            [24] 1097 	pop	_bp
      0014DB 22               [24] 1098 	ret
                                   1099 ;------------------------------------------------------------
                                   1100 ;Allocation info for local variables in function 'GPIO_EXT_ISR'
                                   1101 ;------------------------------------------------------------
                                   1102 ;	source/CH549_GPIO.c:183: void GPIO_EXT_ISR(void) __interrupt INT_NO_GPIO __using 1
                                   1103 ;	-----------------------------------------
                                   1104 ;	 function GPIO_EXT_ISR
                                   1105 ;	-----------------------------------------
      0014DC                       1106 _GPIO_EXT_ISR:
                           00000F  1107 	ar7 = 0x0f
                           00000E  1108 	ar6 = 0x0e
                           00000D  1109 	ar5 = 0x0d
                           00000C  1110 	ar4 = 0x0c
                           00000B  1111 	ar3 = 0x0b
                           00000A  1112 	ar2 = 0x0a
                           000009  1113 	ar1 = 0x09
                           000008  1114 	ar0 = 0x08
      0014DC C0 21            [24] 1115 	push	bits
      0014DE C0 E0            [24] 1116 	push	acc
      0014E0 C0 F0            [24] 1117 	push	b
      0014E2 C0 82            [24] 1118 	push	dpl
      0014E4 C0 83            [24] 1119 	push	dph
      0014E6 C0 07            [24] 1120 	push	(0+7)
      0014E8 C0 06            [24] 1121 	push	(0+6)
      0014EA C0 05            [24] 1122 	push	(0+5)
      0014EC C0 04            [24] 1123 	push	(0+4)
      0014EE C0 03            [24] 1124 	push	(0+3)
      0014F0 C0 02            [24] 1125 	push	(0+2)
      0014F2 C0 01            [24] 1126 	push	(0+1)
      0014F4 C0 00            [24] 1127 	push	(0+0)
      0014F6 C0 D0            [24] 1128 	push	psw
      0014F8 75 D0 08         [24] 1129 	mov	psw,#0x08
                                   1130 ;	source/CH549_GPIO.c:185: if(AIN11==0)
      0014FB 20 83 27         [24] 1131 	jb	_AIN11,00102$
                                   1132 ;	source/CH549_GPIO.c:187: mDelaymS(10);
      0014FE 90 00 0A         [24] 1133 	mov	dptr,#0x000a
      001501 75 D0 00         [24] 1134 	mov	psw,#0x00
      001504 12 0D 07         [24] 1135 	lcall	_mDelaymS
      001507 75 D0 08         [24] 1136 	mov	psw,#0x08
                                   1137 ;	source/CH549_GPIO.c:188: printf("P03 Falling\n");
      00150A 74 1B            [12] 1138 	mov	a,#___str_0
      00150C C0 E0            [24] 1139 	push	acc
      00150E 74 3D            [12] 1140 	mov	a,#(___str_0 >> 8)
      001510 C0 E0            [24] 1141 	push	acc
      001512 74 80            [12] 1142 	mov	a,#0x80
      001514 C0 E0            [24] 1143 	push	acc
      001516 75 D0 00         [24] 1144 	mov	psw,#0x00
      001519 12 1E 8A         [24] 1145 	lcall	_printf
      00151C 75 D0 08         [24] 1146 	mov	psw,#0x08
      00151F 15 81            [12] 1147 	dec	sp
      001521 15 81            [12] 1148 	dec	sp
      001523 15 81            [12] 1149 	dec	sp
      001525                       1150 00102$:
                                   1151 ;	source/CH549_GPIO.c:191: if(AIN5==0)
      001525 20 95 27         [24] 1152 	jb	_AIN5,00105$
                                   1153 ;	source/CH549_GPIO.c:193: mDelaymS(10);
      001528 90 00 0A         [24] 1154 	mov	dptr,#0x000a
      00152B 75 D0 00         [24] 1155 	mov	psw,#0x00
      00152E 12 0D 07         [24] 1156 	lcall	_mDelaymS
      001531 75 D0 08         [24] 1157 	mov	psw,#0x08
                                   1158 ;	source/CH549_GPIO.c:194: printf("P15 Falling\n");
      001534 74 28            [12] 1159 	mov	a,#___str_1
      001536 C0 E0            [24] 1160 	push	acc
      001538 74 3D            [12] 1161 	mov	a,#(___str_1 >> 8)
      00153A C0 E0            [24] 1162 	push	acc
      00153C 74 80            [12] 1163 	mov	a,#0x80
      00153E C0 E0            [24] 1164 	push	acc
      001540 75 D0 00         [24] 1165 	mov	psw,#0x00
      001543 12 1E 8A         [24] 1166 	lcall	_printf
      001546 75 D0 08         [24] 1167 	mov	psw,#0x08
      001549 15 81            [12] 1168 	dec	sp
      00154B 15 81            [12] 1169 	dec	sp
      00154D 15 81            [12] 1170 	dec	sp
      00154F                       1171 00105$:
                                   1172 ;	source/CH549_GPIO.c:198: }
      00154F D0 D0            [24] 1173 	pop	psw
      001551 D0 00            [24] 1174 	pop	(0+0)
      001553 D0 01            [24] 1175 	pop	(0+1)
      001555 D0 02            [24] 1176 	pop	(0+2)
      001557 D0 03            [24] 1177 	pop	(0+3)
      001559 D0 04            [24] 1178 	pop	(0+4)
      00155B D0 05            [24] 1179 	pop	(0+5)
      00155D D0 06            [24] 1180 	pop	(0+6)
      00155F D0 07            [24] 1181 	pop	(0+7)
      001561 D0 83            [24] 1182 	pop	dph
      001563 D0 82            [24] 1183 	pop	dpl
      001565 D0 F0            [24] 1184 	pop	b
      001567 D0 E0            [24] 1185 	pop	acc
      001569 D0 21            [24] 1186 	pop	bits
      00156B 32               [24] 1187 	reti
                                   1188 ;------------------------------------------------------------
                                   1189 ;Allocation info for local variables in function 'GPIO_STD0_ISR'
                                   1190 ;------------------------------------------------------------
                                   1191 ;	source/CH549_GPIO.c:206: void GPIO_STD0_ISR(void) __interrupt INT_NO_INT0 __using 1
                                   1192 ;	-----------------------------------------
                                   1193 ;	 function GPIO_STD0_ISR
                                   1194 ;	-----------------------------------------
      00156C                       1195 _GPIO_STD0_ISR:
      00156C C0 21            [24] 1196 	push	bits
      00156E C0 E0            [24] 1197 	push	acc
      001570 C0 F0            [24] 1198 	push	b
      001572 C0 82            [24] 1199 	push	dpl
      001574 C0 83            [24] 1200 	push	dph
      001576 C0 07            [24] 1201 	push	(0+7)
      001578 C0 06            [24] 1202 	push	(0+6)
      00157A C0 05            [24] 1203 	push	(0+5)
      00157C C0 04            [24] 1204 	push	(0+4)
      00157E C0 03            [24] 1205 	push	(0+3)
      001580 C0 02            [24] 1206 	push	(0+2)
      001582 C0 01            [24] 1207 	push	(0+1)
      001584 C0 00            [24] 1208 	push	(0+0)
      001586 C0 D0            [24] 1209 	push	psw
      001588 75 D0 08         [24] 1210 	mov	psw,#0x08
                                   1211 ;	source/CH549_GPIO.c:208: mDelaymS(10);
      00158B 90 00 0A         [24] 1212 	mov	dptr,#0x000a
      00158E 75 D0 00         [24] 1213 	mov	psw,#0x00
      001591 12 0D 07         [24] 1214 	lcall	_mDelaymS
      001594 75 D0 08         [24] 1215 	mov	psw,#0x08
                                   1216 ;	source/CH549_GPIO.c:209: printf("P32 Falling\n");
      001597 74 35            [12] 1217 	mov	a,#___str_2
      001599 C0 E0            [24] 1218 	push	acc
      00159B 74 3D            [12] 1219 	mov	a,#(___str_2 >> 8)
      00159D C0 E0            [24] 1220 	push	acc
      00159F 74 80            [12] 1221 	mov	a,#0x80
      0015A1 C0 E0            [24] 1222 	push	acc
      0015A3 75 D0 00         [24] 1223 	mov	psw,#0x00
      0015A6 12 1E 8A         [24] 1224 	lcall	_printf
      0015A9 75 D0 08         [24] 1225 	mov	psw,#0x08
      0015AC 15 81            [12] 1226 	dec	sp
      0015AE 15 81            [12] 1227 	dec	sp
      0015B0 15 81            [12] 1228 	dec	sp
                                   1229 ;	source/CH549_GPIO.c:211: }
      0015B2 D0 D0            [24] 1230 	pop	psw
      0015B4 D0 00            [24] 1231 	pop	(0+0)
      0015B6 D0 01            [24] 1232 	pop	(0+1)
      0015B8 D0 02            [24] 1233 	pop	(0+2)
      0015BA D0 03            [24] 1234 	pop	(0+3)
      0015BC D0 04            [24] 1235 	pop	(0+4)
      0015BE D0 05            [24] 1236 	pop	(0+5)
      0015C0 D0 06            [24] 1237 	pop	(0+6)
      0015C2 D0 07            [24] 1238 	pop	(0+7)
      0015C4 D0 83            [24] 1239 	pop	dph
      0015C6 D0 82            [24] 1240 	pop	dpl
      0015C8 D0 F0            [24] 1241 	pop	b
      0015CA D0 E0            [24] 1242 	pop	acc
      0015CC D0 21            [24] 1243 	pop	bits
      0015CE 32               [24] 1244 	reti
                                   1245 ;------------------------------------------------------------
                                   1246 ;Allocation info for local variables in function 'GPIO_STD1_ISR'
                                   1247 ;------------------------------------------------------------
                                   1248 ;	source/CH549_GPIO.c:219: void GPIO_STD1_ISR(void) __interrupt INT_NO_INT1 __using 1
                                   1249 ;	-----------------------------------------
                                   1250 ;	 function GPIO_STD1_ISR
                                   1251 ;	-----------------------------------------
      0015CF                       1252 _GPIO_STD1_ISR:
      0015CF C0 21            [24] 1253 	push	bits
      0015D1 C0 E0            [24] 1254 	push	acc
      0015D3 C0 F0            [24] 1255 	push	b
      0015D5 C0 82            [24] 1256 	push	dpl
      0015D7 C0 83            [24] 1257 	push	dph
      0015D9 C0 07            [24] 1258 	push	(0+7)
      0015DB C0 06            [24] 1259 	push	(0+6)
      0015DD C0 05            [24] 1260 	push	(0+5)
      0015DF C0 04            [24] 1261 	push	(0+4)
      0015E1 C0 03            [24] 1262 	push	(0+3)
      0015E3 C0 02            [24] 1263 	push	(0+2)
      0015E5 C0 01            [24] 1264 	push	(0+1)
      0015E7 C0 00            [24] 1265 	push	(0+0)
      0015E9 C0 D0            [24] 1266 	push	psw
      0015EB 75 D0 08         [24] 1267 	mov	psw,#0x08
                                   1268 ;	source/CH549_GPIO.c:221: mDelaymS(10);
      0015EE 90 00 0A         [24] 1269 	mov	dptr,#0x000a
      0015F1 75 D0 00         [24] 1270 	mov	psw,#0x00
      0015F4 12 0D 07         [24] 1271 	lcall	_mDelaymS
      0015F7 75 D0 08         [24] 1272 	mov	psw,#0x08
                                   1273 ;	source/CH549_GPIO.c:222: printf("P33 Falling\n");
      0015FA 74 42            [12] 1274 	mov	a,#___str_3
      0015FC C0 E0            [24] 1275 	push	acc
      0015FE 74 3D            [12] 1276 	mov	a,#(___str_3 >> 8)
      001600 C0 E0            [24] 1277 	push	acc
      001602 74 80            [12] 1278 	mov	a,#0x80
      001604 C0 E0            [24] 1279 	push	acc
      001606 75 D0 00         [24] 1280 	mov	psw,#0x00
      001609 12 1E 8A         [24] 1281 	lcall	_printf
      00160C 75 D0 08         [24] 1282 	mov	psw,#0x08
      00160F 15 81            [12] 1283 	dec	sp
      001611 15 81            [12] 1284 	dec	sp
      001613 15 81            [12] 1285 	dec	sp
                                   1286 ;	source/CH549_GPIO.c:224: }
      001615 D0 D0            [24] 1287 	pop	psw
      001617 D0 00            [24] 1288 	pop	(0+0)
      001619 D0 01            [24] 1289 	pop	(0+1)
      00161B D0 02            [24] 1290 	pop	(0+2)
      00161D D0 03            [24] 1291 	pop	(0+3)
      00161F D0 04            [24] 1292 	pop	(0+4)
      001621 D0 05            [24] 1293 	pop	(0+5)
      001623 D0 06            [24] 1294 	pop	(0+6)
      001625 D0 07            [24] 1295 	pop	(0+7)
      001627 D0 83            [24] 1296 	pop	dph
      001629 D0 82            [24] 1297 	pop	dpl
      00162B D0 F0            [24] 1298 	pop	b
      00162D D0 E0            [24] 1299 	pop	acc
      00162F D0 21            [24] 1300 	pop	bits
      001631 32               [24] 1301 	reti
                                   1302 	.area CSEG    (CODE)
                                   1303 	.area CONST   (CODE)
                                   1304 	.area CONST   (CODE)
      003D1B                       1305 ___str_0:
      003D1B 50 30 33 20 46 61 6C  1306 	.ascii "P03 Falling"
             6C 69 6E 67
      003D26 0A                    1307 	.db 0x0a
      003D27 00                    1308 	.db 0x00
                                   1309 	.area CSEG    (CODE)
                                   1310 	.area CONST   (CODE)
      003D28                       1311 ___str_1:
      003D28 50 31 35 20 46 61 6C  1312 	.ascii "P15 Falling"
             6C 69 6E 67
      003D33 0A                    1313 	.db 0x0a
      003D34 00                    1314 	.db 0x00
                                   1315 	.area CSEG    (CODE)
                                   1316 	.area CONST   (CODE)
      003D35                       1317 ___str_2:
      003D35 50 33 32 20 46 61 6C  1318 	.ascii "P32 Falling"
             6C 69 6E 67
      003D40 0A                    1319 	.db 0x0a
      003D41 00                    1320 	.db 0x00
                                   1321 	.area CSEG    (CODE)
                                   1322 	.area CONST   (CODE)
      003D42                       1323 ___str_3:
      003D42 50 33 33 20 46 61 6C  1324 	.ascii "P33 Falling"
             6C 69 6E 67
      003D4D 0A                    1325 	.db 0x0a
      003D4E 00                    1326 	.db 0x00
                                   1327 	.area CSEG    (CODE)
                                   1328 	.area CABS    (ABS,CODE)
