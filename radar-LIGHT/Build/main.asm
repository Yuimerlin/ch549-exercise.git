;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.0.0 #11528 (MINGW64)
;--------------------------------------------------------
	.module main
	.optsdcc -mmcs51 --model-large
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _main
	.globl _FaceTrack
	.globl _LightTurn
	.globl _decode_XenP201S
	.globl _UART1Init
	.globl _GPIO_Init
	.globl _SPIMasterModeSet
	.globl _CH549UART1RcvByte
	.globl _mDelaymS
	.globl _mDelayuS
	.globl _CfgFsys
	.globl _printf_fast_f
	.globl _setFontSize
	.globl _OLED_Clear
	.globl _OLED_Init
	.globl _OLED_ShowChar
	.globl _UIF_BUS_RST
	.globl _UIF_DETECT
	.globl _UIF_TRANSFER
	.globl _UIF_SUSPEND
	.globl _UIF_HST_SOF
	.globl _UIF_FIFO_OV
	.globl _U_SIE_FREE
	.globl _U_TOG_OK
	.globl _U_IS_NAK
	.globl _S0_R_FIFO
	.globl _S0_T_FIFO
	.globl _S0_FREE
	.globl _S0_IF_BYTE
	.globl _S0_IF_FIRST
	.globl _S0_IF_OV
	.globl _S0_FST_ACT
	.globl _CP_RL2
	.globl _C_T2
	.globl _TR2
	.globl _EXEN2
	.globl _TCLK
	.globl _RCLK
	.globl _EXF2
	.globl _CAP1F
	.globl _TF2
	.globl _RI
	.globl _TI
	.globl _RB8
	.globl _TB8
	.globl _REN
	.globl _SM2
	.globl _SM1
	.globl _SM0
	.globl _IT0
	.globl _IE0
	.globl _IT1
	.globl _IE1
	.globl _TR0
	.globl _TF0
	.globl _TR1
	.globl _TF1
	.globl _XI
	.globl _XO
	.globl _P4_0
	.globl _P4_1
	.globl _P4_2
	.globl _P4_3
	.globl _P4_4
	.globl _P4_5
	.globl _P4_6
	.globl _RXD
	.globl _TXD
	.globl _INT0
	.globl _INT1
	.globl _T0
	.globl _T1
	.globl _CAP0
	.globl _INT3
	.globl _P3_0
	.globl _P3_1
	.globl _P3_2
	.globl _P3_3
	.globl _P3_4
	.globl _P3_5
	.globl _P3_6
	.globl _P3_7
	.globl _PWM5
	.globl _PWM4
	.globl _INT0_
	.globl _PWM3
	.globl _PWM2
	.globl _CAP1_
	.globl _T2_
	.globl _PWM1
	.globl _CAP2_
	.globl _T2EX_
	.globl _PWM0
	.globl _RXD1
	.globl _PWM6
	.globl _TXD1
	.globl _PWM7
	.globl _P2_0
	.globl _P2_1
	.globl _P2_2
	.globl _P2_3
	.globl _P2_4
	.globl _P2_5
	.globl _P2_6
	.globl _P2_7
	.globl _AIN0
	.globl _CAP1
	.globl _T2
	.globl _AIN1
	.globl _CAP2
	.globl _T2EX
	.globl _AIN2
	.globl _AIN3
	.globl _AIN4
	.globl _UCC1
	.globl _SCS
	.globl _AIN5
	.globl _UCC2
	.globl _PWM0_
	.globl _MOSI
	.globl _AIN6
	.globl _VBUS
	.globl _RXD1_
	.globl _MISO
	.globl _AIN7
	.globl _TXD1_
	.globl _SCK
	.globl _P1_0
	.globl _P1_1
	.globl _P1_2
	.globl _P1_3
	.globl _P1_4
	.globl _P1_5
	.globl _P1_6
	.globl _P1_7
	.globl _AIN8
	.globl _AIN9
	.globl _AIN10
	.globl _RXD_
	.globl _AIN11
	.globl _TXD_
	.globl _AIN12
	.globl _RXD2
	.globl _AIN13
	.globl _TXD2
	.globl _AIN14
	.globl _RXD3
	.globl _AIN15
	.globl _TXD3
	.globl _P0_0
	.globl _P0_1
	.globl _P0_2
	.globl _P0_3
	.globl _P0_4
	.globl _P0_5
	.globl _P0_6
	.globl _P0_7
	.globl _IE_SPI0
	.globl _IE_INT3
	.globl _IE_USB
	.globl _IE_UART2
	.globl _IE_ADC
	.globl _IE_UART1
	.globl _IE_UART3
	.globl _IE_PWMX
	.globl _IE_GPIO
	.globl _IE_WDOG
	.globl _PX0
	.globl _PT0
	.globl _PX1
	.globl _PT1
	.globl _PS
	.globl _PT2
	.globl _PL_FLAG
	.globl _PH_FLAG
	.globl _EX0
	.globl _ET0
	.globl _EX1
	.globl _ET1
	.globl _ES
	.globl _ET2
	.globl _E_DIS
	.globl _EA
	.globl _P
	.globl _F1
	.globl _OV
	.globl _RS0
	.globl _RS1
	.globl _F0
	.globl _AC
	.globl _CY
	.globl _UEP1_DMA_H
	.globl _UEP1_DMA_L
	.globl _UEP1_DMA
	.globl _UEP0_DMA_H
	.globl _UEP0_DMA_L
	.globl _UEP0_DMA
	.globl _UEP2_3_MOD
	.globl _UEP4_1_MOD
	.globl _UEP3_DMA_H
	.globl _UEP3_DMA_L
	.globl _UEP3_DMA
	.globl _UEP2_DMA_H
	.globl _UEP2_DMA_L
	.globl _UEP2_DMA
	.globl _USB_DEV_AD
	.globl _USB_CTRL
	.globl _USB_INT_EN
	.globl _UEP4_T_LEN
	.globl _UEP4_CTRL
	.globl _UEP0_T_LEN
	.globl _UEP0_CTRL
	.globl _USB_RX_LEN
	.globl _USB_MIS_ST
	.globl _USB_INT_ST
	.globl _USB_INT_FG
	.globl _UEP3_T_LEN
	.globl _UEP3_CTRL
	.globl _UEP2_T_LEN
	.globl _UEP2_CTRL
	.globl _UEP1_T_LEN
	.globl _UEP1_CTRL
	.globl _UDEV_CTRL
	.globl _USB_C_CTRL
	.globl _ADC_PIN
	.globl _ADC_CHAN
	.globl _ADC_DAT_H
	.globl _ADC_DAT_L
	.globl _ADC_DAT
	.globl _ADC_CFG
	.globl _ADC_CTRL
	.globl _TKEY_CTRL
	.globl _SIF3
	.globl _SBAUD3
	.globl _SBUF3
	.globl _SCON3
	.globl _SIF2
	.globl _SBAUD2
	.globl _SBUF2
	.globl _SCON2
	.globl _SIF1
	.globl _SBAUD1
	.globl _SBUF1
	.globl _SCON1
	.globl _SPI0_SETUP
	.globl _SPI0_CK_SE
	.globl _SPI0_CTRL
	.globl _SPI0_DATA
	.globl _SPI0_STAT
	.globl _PWM_DATA7
	.globl _PWM_DATA6
	.globl _PWM_DATA5
	.globl _PWM_DATA4
	.globl _PWM_DATA3
	.globl _PWM_CTRL2
	.globl _PWM_CK_SE
	.globl _PWM_CTRL
	.globl _PWM_DATA0
	.globl _PWM_DATA1
	.globl _PWM_DATA2
	.globl _T2CAP1H
	.globl _T2CAP1L
	.globl _T2CAP1
	.globl _TH2
	.globl _TL2
	.globl _T2COUNT
	.globl _RCAP2H
	.globl _RCAP2L
	.globl _RCAP2
	.globl _T2MOD
	.globl _T2CON
	.globl _T2CAP0H
	.globl _T2CAP0L
	.globl _T2CAP0
	.globl _T2CON2
	.globl _SBUF
	.globl _SCON
	.globl _TH1
	.globl _TH0
	.globl _TL1
	.globl _TL0
	.globl _TMOD
	.globl _TCON
	.globl _XBUS_AUX
	.globl _PIN_FUNC
	.globl _P5
	.globl _P4_DIR_PU
	.globl _P4_MOD_OC
	.globl _P4
	.globl _P3_DIR_PU
	.globl _P3_MOD_OC
	.globl _P3
	.globl _P2_DIR_PU
	.globl _P2_MOD_OC
	.globl _P2
	.globl _P1_DIR_PU
	.globl _P1_MOD_OC
	.globl _P1
	.globl _P0_DIR_PU
	.globl _P0_MOD_OC
	.globl _P0
	.globl _ROM_CTRL
	.globl _ROM_DATA_HH
	.globl _ROM_DATA_HL
	.globl _ROM_DATA_HI
	.globl _ROM_ADDR_H
	.globl _ROM_ADDR_L
	.globl _ROM_ADDR
	.globl _GPIO_IE
	.globl _INTX
	.globl _IP_EX
	.globl _IE_EX
	.globl _IP
	.globl _IE
	.globl _WDOG_COUNT
	.globl _RESET_KEEP
	.globl _WAKE_CTRL
	.globl _CLOCK_CFG
	.globl _POWER_CFG
	.globl _PCON
	.globl _GLOBAL_CFG
	.globl _SAFE_MOD
	.globl _DPH
	.globl _DPL
	.globl _SP
	.globl _A_INV
	.globl _B
	.globl _ACC
	.globl _PSW
	.globl _yPos
	.globl _xPos
	.globl _fRecv
	.globl _pBuf
	.globl _recvBuf
	.globl _oled_row
	.globl _oled_colum
	.globl _putchar
	.globl _setCursor
	.globl _UART1Interrupt
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
	.area RSEG    (ABS,DATA)
	.org 0x0000
_PSW	=	0x00d0
_ACC	=	0x00e0
_B	=	0x00f0
_A_INV	=	0x00fd
_SP	=	0x0081
_DPL	=	0x0082
_DPH	=	0x0083
_SAFE_MOD	=	0x00a1
_GLOBAL_CFG	=	0x00b1
_PCON	=	0x0087
_POWER_CFG	=	0x00ba
_CLOCK_CFG	=	0x00b9
_WAKE_CTRL	=	0x00a9
_RESET_KEEP	=	0x00fe
_WDOG_COUNT	=	0x00ff
_IE	=	0x00a8
_IP	=	0x00b8
_IE_EX	=	0x00e8
_IP_EX	=	0x00e9
_INTX	=	0x00b3
_GPIO_IE	=	0x00b2
_ROM_ADDR	=	0x8584
_ROM_ADDR_L	=	0x0084
_ROM_ADDR_H	=	0x0085
_ROM_DATA_HI	=	0x8f8e
_ROM_DATA_HL	=	0x008e
_ROM_DATA_HH	=	0x008f
_ROM_CTRL	=	0x0086
_P0	=	0x0080
_P0_MOD_OC	=	0x00c4
_P0_DIR_PU	=	0x00c5
_P1	=	0x0090
_P1_MOD_OC	=	0x0092
_P1_DIR_PU	=	0x0093
_P2	=	0x00a0
_P2_MOD_OC	=	0x0094
_P2_DIR_PU	=	0x0095
_P3	=	0x00b0
_P3_MOD_OC	=	0x0096
_P3_DIR_PU	=	0x0097
_P4	=	0x00c0
_P4_MOD_OC	=	0x00c2
_P4_DIR_PU	=	0x00c3
_P5	=	0x00ab
_PIN_FUNC	=	0x00aa
_XBUS_AUX	=	0x00a2
_TCON	=	0x0088
_TMOD	=	0x0089
_TL0	=	0x008a
_TL1	=	0x008b
_TH0	=	0x008c
_TH1	=	0x008d
_SCON	=	0x0098
_SBUF	=	0x0099
_T2CON2	=	0x00c1
_T2CAP0	=	0xc7c6
_T2CAP0L	=	0x00c6
_T2CAP0H	=	0x00c7
_T2CON	=	0x00c8
_T2MOD	=	0x00c9
_RCAP2	=	0xcbca
_RCAP2L	=	0x00ca
_RCAP2H	=	0x00cb
_T2COUNT	=	0xcdcc
_TL2	=	0x00cc
_TH2	=	0x00cd
_T2CAP1	=	0xcfce
_T2CAP1L	=	0x00ce
_T2CAP1H	=	0x00cf
_PWM_DATA2	=	0x009a
_PWM_DATA1	=	0x009b
_PWM_DATA0	=	0x009c
_PWM_CTRL	=	0x009d
_PWM_CK_SE	=	0x009e
_PWM_CTRL2	=	0x009f
_PWM_DATA3	=	0x00a3
_PWM_DATA4	=	0x00a4
_PWM_DATA5	=	0x00a5
_PWM_DATA6	=	0x00a6
_PWM_DATA7	=	0x00a7
_SPI0_STAT	=	0x00f8
_SPI0_DATA	=	0x00f9
_SPI0_CTRL	=	0x00fa
_SPI0_CK_SE	=	0x00fb
_SPI0_SETUP	=	0x00fc
_SCON1	=	0x00bc
_SBUF1	=	0x00bd
_SBAUD1	=	0x00be
_SIF1	=	0x00bf
_SCON2	=	0x00b4
_SBUF2	=	0x00b5
_SBAUD2	=	0x00b6
_SIF2	=	0x00b7
_SCON3	=	0x00ac
_SBUF3	=	0x00ad
_SBAUD3	=	0x00ae
_SIF3	=	0x00af
_TKEY_CTRL	=	0x00f1
_ADC_CTRL	=	0x00f2
_ADC_CFG	=	0x00f3
_ADC_DAT	=	0xf5f4
_ADC_DAT_L	=	0x00f4
_ADC_DAT_H	=	0x00f5
_ADC_CHAN	=	0x00f6
_ADC_PIN	=	0x00f7
_USB_C_CTRL	=	0x0091
_UDEV_CTRL	=	0x00d1
_UEP1_CTRL	=	0x00d2
_UEP1_T_LEN	=	0x00d3
_UEP2_CTRL	=	0x00d4
_UEP2_T_LEN	=	0x00d5
_UEP3_CTRL	=	0x00d6
_UEP3_T_LEN	=	0x00d7
_USB_INT_FG	=	0x00d8
_USB_INT_ST	=	0x00d9
_USB_MIS_ST	=	0x00da
_USB_RX_LEN	=	0x00db
_UEP0_CTRL	=	0x00dc
_UEP0_T_LEN	=	0x00dd
_UEP4_CTRL	=	0x00de
_UEP4_T_LEN	=	0x00df
_USB_INT_EN	=	0x00e1
_USB_CTRL	=	0x00e2
_USB_DEV_AD	=	0x00e3
_UEP2_DMA	=	0xe5e4
_UEP2_DMA_L	=	0x00e4
_UEP2_DMA_H	=	0x00e5
_UEP3_DMA	=	0xe7e6
_UEP3_DMA_L	=	0x00e6
_UEP3_DMA_H	=	0x00e7
_UEP4_1_MOD	=	0x00ea
_UEP2_3_MOD	=	0x00eb
_UEP0_DMA	=	0xedec
_UEP0_DMA_L	=	0x00ec
_UEP0_DMA_H	=	0x00ed
_UEP1_DMA	=	0xefee
_UEP1_DMA_L	=	0x00ee
_UEP1_DMA_H	=	0x00ef
;--------------------------------------------------------
; special function bits
;--------------------------------------------------------
	.area RSEG    (ABS,DATA)
	.org 0x0000
_CY	=	0x00d7
_AC	=	0x00d6
_F0	=	0x00d5
_RS1	=	0x00d4
_RS0	=	0x00d3
_OV	=	0x00d2
_F1	=	0x00d1
_P	=	0x00d0
_EA	=	0x00af
_E_DIS	=	0x00ae
_ET2	=	0x00ad
_ES	=	0x00ac
_ET1	=	0x00ab
_EX1	=	0x00aa
_ET0	=	0x00a9
_EX0	=	0x00a8
_PH_FLAG	=	0x00bf
_PL_FLAG	=	0x00be
_PT2	=	0x00bd
_PS	=	0x00bc
_PT1	=	0x00bb
_PX1	=	0x00ba
_PT0	=	0x00b9
_PX0	=	0x00b8
_IE_WDOG	=	0x00ef
_IE_GPIO	=	0x00ee
_IE_PWMX	=	0x00ed
_IE_UART3	=	0x00ed
_IE_UART1	=	0x00ec
_IE_ADC	=	0x00eb
_IE_UART2	=	0x00eb
_IE_USB	=	0x00ea
_IE_INT3	=	0x00e9
_IE_SPI0	=	0x00e8
_P0_7	=	0x0087
_P0_6	=	0x0086
_P0_5	=	0x0085
_P0_4	=	0x0084
_P0_3	=	0x0083
_P0_2	=	0x0082
_P0_1	=	0x0081
_P0_0	=	0x0080
_TXD3	=	0x0087
_AIN15	=	0x0087
_RXD3	=	0x0086
_AIN14	=	0x0086
_TXD2	=	0x0085
_AIN13	=	0x0085
_RXD2	=	0x0084
_AIN12	=	0x0084
_TXD_	=	0x0083
_AIN11	=	0x0083
_RXD_	=	0x0082
_AIN10	=	0x0082
_AIN9	=	0x0081
_AIN8	=	0x0080
_P1_7	=	0x0097
_P1_6	=	0x0096
_P1_5	=	0x0095
_P1_4	=	0x0094
_P1_3	=	0x0093
_P1_2	=	0x0092
_P1_1	=	0x0091
_P1_0	=	0x0090
_SCK	=	0x0097
_TXD1_	=	0x0097
_AIN7	=	0x0097
_MISO	=	0x0096
_RXD1_	=	0x0096
_VBUS	=	0x0096
_AIN6	=	0x0096
_MOSI	=	0x0095
_PWM0_	=	0x0095
_UCC2	=	0x0095
_AIN5	=	0x0095
_SCS	=	0x0094
_UCC1	=	0x0094
_AIN4	=	0x0094
_AIN3	=	0x0093
_AIN2	=	0x0092
_T2EX	=	0x0091
_CAP2	=	0x0091
_AIN1	=	0x0091
_T2	=	0x0090
_CAP1	=	0x0090
_AIN0	=	0x0090
_P2_7	=	0x00a7
_P2_6	=	0x00a6
_P2_5	=	0x00a5
_P2_4	=	0x00a4
_P2_3	=	0x00a3
_P2_2	=	0x00a2
_P2_1	=	0x00a1
_P2_0	=	0x00a0
_PWM7	=	0x00a7
_TXD1	=	0x00a7
_PWM6	=	0x00a6
_RXD1	=	0x00a6
_PWM0	=	0x00a5
_T2EX_	=	0x00a5
_CAP2_	=	0x00a5
_PWM1	=	0x00a4
_T2_	=	0x00a4
_CAP1_	=	0x00a4
_PWM2	=	0x00a3
_PWM3	=	0x00a2
_INT0_	=	0x00a2
_PWM4	=	0x00a1
_PWM5	=	0x00a0
_P3_7	=	0x00b7
_P3_6	=	0x00b6
_P3_5	=	0x00b5
_P3_4	=	0x00b4
_P3_3	=	0x00b3
_P3_2	=	0x00b2
_P3_1	=	0x00b1
_P3_0	=	0x00b0
_INT3	=	0x00b7
_CAP0	=	0x00b6
_T1	=	0x00b5
_T0	=	0x00b4
_INT1	=	0x00b3
_INT0	=	0x00b2
_TXD	=	0x00b1
_RXD	=	0x00b0
_P4_6	=	0x00c6
_P4_5	=	0x00c5
_P4_4	=	0x00c4
_P4_3	=	0x00c3
_P4_2	=	0x00c2
_P4_1	=	0x00c1
_P4_0	=	0x00c0
_XO	=	0x00c7
_XI	=	0x00c6
_TF1	=	0x008f
_TR1	=	0x008e
_TF0	=	0x008d
_TR0	=	0x008c
_IE1	=	0x008b
_IT1	=	0x008a
_IE0	=	0x0089
_IT0	=	0x0088
_SM0	=	0x009f
_SM1	=	0x009e
_SM2	=	0x009d
_REN	=	0x009c
_TB8	=	0x009b
_RB8	=	0x009a
_TI	=	0x0099
_RI	=	0x0098
_TF2	=	0x00cf
_CAP1F	=	0x00cf
_EXF2	=	0x00ce
_RCLK	=	0x00cd
_TCLK	=	0x00cc
_EXEN2	=	0x00cb
_TR2	=	0x00ca
_C_T2	=	0x00c9
_CP_RL2	=	0x00c8
_S0_FST_ACT	=	0x00ff
_S0_IF_OV	=	0x00fe
_S0_IF_FIRST	=	0x00fd
_S0_IF_BYTE	=	0x00fc
_S0_FREE	=	0x00fb
_S0_T_FIFO	=	0x00fa
_S0_R_FIFO	=	0x00f8
_U_IS_NAK	=	0x00df
_U_TOG_OK	=	0x00de
_U_SIE_FREE	=	0x00dd
_UIF_FIFO_OV	=	0x00dc
_UIF_HST_SOF	=	0x00db
_UIF_SUSPEND	=	0x00da
_UIF_TRANSFER	=	0x00d9
_UIF_DETECT	=	0x00d8
_UIF_BUS_RST	=	0x00d8
;--------------------------------------------------------
; overlayable register banks
;--------------------------------------------------------
	.area REG_BANK_0	(REL,OVR,DATA)
	.ds 8
	.area REG_BANK_1	(REL,OVR,DATA)
	.ds 8
;--------------------------------------------------------
; overlayable bit register bank
;--------------------------------------------------------
	.area BIT_BANK	(REL,OVR,DATA)
bits:
	.ds 1
	b0 = bits[0]
	b1 = bits[1]
	b2 = bits[2]
	b3 = bits[3]
	b4 = bits[4]
	b5 = bits[5]
	b6 = bits[6]
	b7 = bits[7]
;--------------------------------------------------------
; internal ram data
;--------------------------------------------------------
	.area DSEG    (DATA)
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;--------------------------------------------------------
; Stack segment in internal ram 
;--------------------------------------------------------
	.area	SSEG
__start__stack:
	.ds	1

;--------------------------------------------------------
; indirectly addressable internal ram data
;--------------------------------------------------------
	.area ISEG    (DATA)
;--------------------------------------------------------
; absolute internal ram data
;--------------------------------------------------------
	.area IABS    (ABS,DATA)
	.area IABS    (ABS,DATA)
;--------------------------------------------------------
; bit data
;--------------------------------------------------------
	.area BSEG    (BIT)
;--------------------------------------------------------
; paged external ram data
;--------------------------------------------------------
	.area PSEG    (PAG,XDATA)
;--------------------------------------------------------
; external ram data
;--------------------------------------------------------
	.area XSEG    (XDATA)
_oled_colum::
	.ds 2
_oled_row::
	.ds 2
_recvBuf::
	.ds 30
_pBuf::
	.ds 1
_fRecv::
	.ds 1
_xPos::
	.ds 2
_yPos::
	.ds 2
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area XABS    (ABS,XDATA)
;--------------------------------------------------------
; external initialized ram data
;--------------------------------------------------------
	.area HOME    (CODE)
	.area GSINIT0 (CODE)
	.area GSINIT1 (CODE)
	.area GSINIT2 (CODE)
	.area GSINIT3 (CODE)
	.area GSINIT4 (CODE)
	.area GSINIT5 (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area CSEG    (CODE)
;--------------------------------------------------------
; interrupt vector 
;--------------------------------------------------------
	.area HOME    (CODE)
__interrupt_vect:
	ljmp	__sdcc_gsinit_startup
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	ljmp	_UART2Interrupt
	.ds	5
	ljmp	_UART1Interrupt
	.ds	5
	ljmp	_UART3Interrupt
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME    (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area GSINIT  (CODE)
	.globl __sdcc_gsinit_startup
	.globl __sdcc_program_startup
	.globl __start__stack
	.globl __mcs51_genRAMCLEAR
;	usr/main.c:131: volatile UINT8 pBuf = 0;
	mov	dptr,#_pBuf
	clr	a
	movx	@dptr,a
;	usr/main.c:132: volatile UINT8 fRecv = 0;
	mov	dptr,#_fRecv
	movx	@dptr,a
	.area GSFINAL (CODE)
	ljmp	__sdcc_program_startup
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME    (CODE)
	.area HOME    (CODE)
__sdcc_program_startup:
	ljmp	_main
;	return from main will return to caller
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CSEG    (CODE)
;------------------------------------------------------------
;Allocation info for local variables in function 'UART1Init'
;------------------------------------------------------------
;interrupt                 Allocated to stack - _bp -3
;baudrate                  Allocated to registers r4 r5 r6 r7 
;------------------------------------------------------------
;	usr/main.c:31: void UART1Init(UINT32 baudrate,UINT8 interrupt)
;	-----------------------------------------
;	 function UART1Init
;	-----------------------------------------
_UART1Init:
	ar7 = 0x07
	ar6 = 0x06
	ar5 = 0x05
	ar4 = 0x04
	ar3 = 0x03
	ar2 = 0x02
	ar1 = 0x01
	ar0 = 0x00
	push	_bp
	mov	_bp,sp
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
;	usr/main.c:33: SCON1 &= ~bU1SM0;                    //选择8位数据通讯
	anl	_SCON1,#0x7f
;	usr/main.c:34: SCON1 |= bU1SMOD;                    //快速模式
	orl	_SCON1,#0x20
;	usr/main.c:35: SCON1 |= bU1REN;                     //使能接收
	orl	_SCON1,#0x10
;	usr/main.c:36: SBAUD1 = 0 - FREQ_SYS/16/baudrate; //波特率配置
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	mov	dptr,#0xc6c0
	mov	b,#0x2d
	clr	a
	lcall	__divulong
	mov	r4,dpl
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	clr	c
	clr	a
	subb	a,r4
	mov	_SBAUD1,a
;	usr/main.c:37: SIF1 = bU1TI;                        //清空发送完成标志
	mov	_SIF1,#0x02
;	usr/main.c:38: if(interrupt){                   //开启中断使能
	mov	a,_bp
	add	a,#0xfd
	mov	r0,a
	mov	a,@r0
	jz	00103$
;	usr/main.c:39: IE_UART1 = 1;
;	assignBit
	setb	_IE_UART1
;	usr/main.c:40: EA = 1;
;	assignBit
	setb	_EA
00103$:
;	usr/main.c:42: }
	pop	_bp
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'decode_XenP201S'
;------------------------------------------------------------
;radar                     Allocated to stack - _bp -5
;buf                       Allocated to stack - _bp +1
;sloc0                     Allocated to stack - _bp +4
;sloc1                     Allocated to stack - _bp +6
;sloc2                     Allocated to stack - _bp +12
;sloc3                     Allocated to stack - _bp +9
;------------------------------------------------------------
;	usr/main.c:65: UINT8 decode_XenP201S( char* buf,RadarData* radar){
;	-----------------------------------------
;	 function decode_XenP201S
;	-----------------------------------------
_decode_XenP201S:
	push	_bp
	mov	_bp,sp
	push	dpl
	push	dph
	push	b
	mov	a,sp
	add	a,#0x0b
	mov	sp,a
;	usr/main.c:66: radar->x1Pos = (((int)buf[2]<<8)+buf[1]);
	mov	a,_bp
	add	a,#0xfb
	mov	r0,a
	mov	a,_bp
	add	a,#0x0c
	mov	r1,a
	mov	a,@r0
	mov	@r1,a
	inc	r0
	inc	r1
	mov	a,@r0
	mov	@r1,a
	inc	r0
	inc	r1
	mov	a,@r0
	mov	@r1,a
	mov	r0,_bp
	inc	r0
	mov	a,#0x02
	add	a,@r0
	mov	r5,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r6,a
	inc	r0
	mov	ar7,@r0
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	lcall	__gptrget
	mov	r3,a
	mov	r4,#0x00
	mov	r0,_bp
	inc	r0
	mov	a,#0x01
	add	a,@r0
	mov	r5,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r6,a
	inc	r0
	mov	ar7,@r0
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	lcall	__gptrget
	mov	r7,#0x00
	add	a,r4
	mov	r5,a
	mov	a,r7
	addc	a,r3
	mov	r7,a
	mov	a,_bp
	add	a,#0x0c
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	inc	r0
	mov	b,@r0
	mov	a,r5
	lcall	__gptrput
	inc	dptr
	mov	a,r7
	lcall	__gptrput
;	usr/main.c:67: radar->y1Pos = (((int)buf[4]<<8)+buf[3]);
	mov	a,_bp
	add	a,#0x0c
	mov	r0,a
	mov	a,#0x02
	add	a,@r0
	mov	r4,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r3,a
	inc	r0
	mov	ar2,@r0
	mov	r0,_bp
	inc	r0
	mov	a,#0x04
	add	a,@r0
	mov	r5,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r6,a
	inc	r0
	mov	ar7,@r0
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	lcall	__gptrget
	mov	r5,a
	mov	a,_bp
	add	a,#0x04
	mov	r0,a
	inc	r0
	mov	@r0,ar5
	dec	r0
	mov	@r0,#0x00
	mov	r0,_bp
	inc	r0
	mov	a,#0x03
	add	a,@r0
	mov	r5,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r6,a
	inc	r0
	mov	ar7,@r0
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	lcall	__gptrget
	mov	r5,a
	mov	r7,#0x00
	mov	a,_bp
	add	a,#0x04
	mov	r0,a
	mov	a,r5
	add	a,@r0
	mov	r5,a
	mov	a,r7
	inc	r0
	addc	a,@r0
	mov	r7,a
	mov	dpl,r4
	mov	dph,r3
	mov	b,r2
	mov	a,r5
	lcall	__gptrput
	inc	dptr
	mov	a,r7
	lcall	__gptrput
;	usr/main.c:68: radar->dist1 = (((int)buf[6]<<8)+buf[5]);
	mov	a,_bp
	add	a,#0x0c
	mov	r0,a
	mov	a,#0x04
	add	a,@r0
	mov	r4,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r3,a
	inc	r0
	mov	ar2,@r0
	mov	r0,_bp
	inc	r0
	mov	a,#0x06
	add	a,@r0
	mov	r5,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r6,a
	inc	r0
	mov	ar7,@r0
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	lcall	__gptrget
	mov	r5,a
	mov	a,_bp
	add	a,#0x04
	mov	r0,a
	inc	r0
	mov	@r0,ar5
	dec	r0
	mov	@r0,#0x00
	mov	r0,_bp
	inc	r0
	mov	a,#0x05
	add	a,@r0
	mov	r5,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r6,a
	inc	r0
	mov	ar7,@r0
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	lcall	__gptrget
	mov	r5,a
	mov	r7,#0x00
	mov	a,_bp
	add	a,#0x04
	mov	r0,a
	mov	a,r5
	add	a,@r0
	mov	r5,a
	mov	a,r7
	inc	r0
	addc	a,@r0
	mov	r7,a
	mov	dpl,r4
	mov	dph,r3
	mov	b,r2
	mov	a,r5
	lcall	__gptrput
	inc	dptr
	mov	a,r7
	lcall	__gptrput
;	usr/main.c:69: radar->ang1 = (((int)buf[8]<<8)+buf[7]);
	mov	a,_bp
	add	a,#0x0c
	mov	r0,a
	mov	a,_bp
	add	a,#0x06
	mov	r1,a
	mov	a,#0x06
	add	a,@r0
	mov	@r1,a
	clr	a
	inc	r0
	addc	a,@r0
	inc	r1
	mov	@r1,a
	inc	r0
	inc	r1
	mov	a,@r0
	mov	@r1,a
	mov	r0,_bp
	inc	r0
	mov	a,#0x08
	add	a,@r0
	mov	r5,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r6,a
	inc	r0
	mov	ar7,@r0
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	lcall	__gptrget
	mov	r3,a
	mov	r4,#0x00
	mov	r0,_bp
	inc	r0
	mov	a,#0x07
	add	a,@r0
	mov	r5,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r6,a
	inc	r0
	mov	ar7,@r0
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	lcall	__gptrget
	mov	r7,#0x00
	add	a,r4
	mov	r5,a
	mov	a,r7
	addc	a,r3
	mov	r7,a
	mov	a,_bp
	add	a,#0x06
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	inc	r0
	mov	b,@r0
	mov	a,r5
	lcall	__gptrput
	inc	dptr
	mov	a,r7
	lcall	__gptrput
;	usr/main.c:74: radar->ang1 = radar->ang1 / 10.0 - 90;
	mov	dpl,r5
	mov	dph,r7
	lcall	___sint2fs
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	clr	a
	push	acc
	push	acc
	mov	a,#0x20
	push	acc
	mov	a,#0x41
	push	acc
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	mov	a,r7
	lcall	___fsdiv
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	clr	a
	push	acc
	push	acc
	mov	a,#0xb4
	push	acc
	mov	a,#0x42
	push	acc
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	mov	a,r7
	lcall	___fssub
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	mov	a,r7
	lcall	___fs2sint
	mov	r7,dpl
	mov	r6,dph
	mov	a,_bp
	add	a,#0x06
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	inc	r0
	mov	b,@r0
	mov	a,r7
	lcall	__gptrput
	inc	dptr
	mov	a,r6
	lcall	__gptrput
;	usr/main.c:76: radar->x2Pos = (((int)buf[10]<<8)+buf[9]);
	mov	a,_bp
	add	a,#0x0c
	mov	r0,a
	mov	a,#0x08
	add	a,@r0
	mov	r5,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r6,a
	inc	r0
	mov	ar7,@r0
	mov	r0,_bp
	inc	r0
	mov	a,#0x0a
	add	a,@r0
	mov	r2,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r3,a
	inc	r0
	mov	ar4,@r0
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
	mov	r2,a
	mov	a,_bp
	add	a,#0x06
	mov	r0,a
	inc	r0
	mov	@r0,ar2
	dec	r0
	mov	@r0,#0x00
	mov	r0,_bp
	inc	r0
	mov	a,#0x09
	add	a,@r0
	mov	r2,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r3,a
	inc	r0
	mov	ar4,@r0
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
	mov	r2,a
	mov	r4,#0x00
	mov	a,_bp
	add	a,#0x06
	mov	r0,a
	mov	a,r2
	add	a,@r0
	mov	r2,a
	mov	a,r4
	inc	r0
	addc	a,@r0
	mov	r4,a
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	mov	a,r2
	lcall	__gptrput
	inc	dptr
	mov	a,r4
	lcall	__gptrput
;	usr/main.c:77: radar->y2Pos = (((int)buf[12]<<8)+buf[11]);
	mov	a,_bp
	add	a,#0x0c
	mov	r0,a
	mov	a,#0x0a
	add	a,@r0
	mov	r5,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r6,a
	inc	r0
	mov	ar7,@r0
	mov	r0,_bp
	inc	r0
	mov	a,#0x0c
	add	a,@r0
	mov	r2,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r3,a
	inc	r0
	mov	ar4,@r0
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
	mov	r2,a
	mov	a,_bp
	add	a,#0x06
	mov	r0,a
	inc	r0
	mov	@r0,ar2
	dec	r0
	mov	@r0,#0x00
	mov	r0,_bp
	inc	r0
	mov	a,#0x0b
	add	a,@r0
	mov	r2,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r3,a
	inc	r0
	mov	ar4,@r0
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
	mov	r2,a
	mov	r4,#0x00
	mov	a,_bp
	add	a,#0x06
	mov	r0,a
	mov	a,r2
	add	a,@r0
	mov	r2,a
	mov	a,r4
	inc	r0
	addc	a,@r0
	mov	r4,a
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	mov	a,r2
	lcall	__gptrput
	inc	dptr
	mov	a,r4
	lcall	__gptrput
;	usr/main.c:78: radar->dist2 = (((int)buf[14]<<8)+buf[13]);
	mov	a,_bp
	add	a,#0x0c
	mov	r0,a
	mov	a,#0x0c
	add	a,@r0
	mov	r5,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r6,a
	inc	r0
	mov	ar7,@r0
	mov	r0,_bp
	inc	r0
	mov	a,#0x0e
	add	a,@r0
	mov	r2,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r3,a
	inc	r0
	mov	ar4,@r0
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
	mov	r2,a
	mov	a,_bp
	add	a,#0x06
	mov	r0,a
	inc	r0
	mov	@r0,ar2
	dec	r0
	mov	@r0,#0x00
	mov	r0,_bp
	inc	r0
	mov	a,#0x0d
	add	a,@r0
	mov	r2,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r3,a
	inc	r0
	mov	ar4,@r0
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
	mov	r2,a
	mov	r4,#0x00
	mov	a,_bp
	add	a,#0x06
	mov	r0,a
	mov	a,r2
	add	a,@r0
	mov	r2,a
	mov	a,r4
	inc	r0
	addc	a,@r0
	mov	r4,a
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	mov	a,r2
	lcall	__gptrput
	inc	dptr
	mov	a,r4
	lcall	__gptrput
;	usr/main.c:79: radar->ang2 = (((int)buf[16]<<8)+buf[15]);
	mov	a,_bp
	add	a,#0x0c
	mov	r0,a
	mov	a,_bp
	add	a,#0x09
	mov	r1,a
	mov	a,#0x0e
	add	a,@r0
	mov	@r1,a
	clr	a
	inc	r0
	addc	a,@r0
	inc	r1
	mov	@r1,a
	inc	r0
	inc	r1
	mov	a,@r0
	mov	@r1,a
	mov	r0,_bp
	inc	r0
	mov	a,#0x10
	add	a,@r0
	mov	r2,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r3,a
	inc	r0
	mov	ar4,@r0
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
	mov	r6,a
	mov	r7,#0x00
	mov	r0,_bp
	inc	r0
	mov	a,#0x0f
	add	a,@r0
	mov	r2,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r3,a
	inc	r0
	mov	ar4,@r0
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
	mov	r4,#0x00
	add	a,r7
	mov	r2,a
	mov	a,r4
	addc	a,r6
	mov	r4,a
	mov	a,_bp
	add	a,#0x09
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	inc	r0
	mov	b,@r0
	mov	a,r2
	lcall	__gptrput
	inc	dptr
	mov	a,r4
	lcall	__gptrput
;	usr/main.c:84: radar->ang2 = radar->ang2 / 10.0 - 90;
	mov	dpl,r2
	mov	dph,r4
	lcall	___sint2fs
	mov	r2,dpl
	mov	r3,dph
	mov	r4,b
	mov	r7,a
	clr	a
	push	acc
	push	acc
	mov	a,#0x20
	push	acc
	mov	a,#0x41
	push	acc
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	mov	a,r7
	lcall	___fsdiv
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	clr	a
	push	acc
	push	acc
	mov	a,#0xb4
	push	acc
	mov	a,#0x42
	push	acc
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	mov	a,r7
	lcall	___fssub
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	mov	a,r7
	lcall	___fs2sint
	mov	r7,dpl
	mov	r6,dph
	mov	a,_bp
	add	a,#0x09
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	inc	r0
	mov	b,@r0
	mov	a,r7
	lcall	__gptrput
	inc	dptr
	mov	a,r6
	lcall	__gptrput
;	usr/main.c:86: radar->x3Pos = (((int)buf[18]<<8)+buf[17]);
	mov	a,_bp
	add	a,#0x0c
	mov	r0,a
	mov	a,#0x10
	add	a,@r0
	mov	r5,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r6,a
	inc	r0
	mov	ar7,@r0
	mov	r0,_bp
	inc	r0
	mov	a,#0x12
	add	a,@r0
	mov	r2,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r3,a
	inc	r0
	mov	ar4,@r0
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
	mov	r2,a
	mov	a,_bp
	add	a,#0x09
	mov	r0,a
	inc	r0
	mov	@r0,ar2
	dec	r0
	mov	@r0,#0x00
	mov	r0,_bp
	inc	r0
	mov	a,#0x11
	add	a,@r0
	mov	r2,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r3,a
	inc	r0
	mov	ar4,@r0
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
	mov	r2,a
	mov	r4,#0x00
	mov	a,_bp
	add	a,#0x09
	mov	r0,a
	mov	a,r2
	add	a,@r0
	mov	r2,a
	mov	a,r4
	inc	r0
	addc	a,@r0
	mov	r4,a
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	mov	a,r2
	lcall	__gptrput
	inc	dptr
	mov	a,r4
	lcall	__gptrput
;	usr/main.c:87: radar->y3Pos = (((int)buf[20]<<8)+buf[19]);
	mov	a,_bp
	add	a,#0x0c
	mov	r0,a
	mov	a,#0x12
	add	a,@r0
	mov	r5,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r6,a
	inc	r0
	mov	ar7,@r0
	mov	r0,_bp
	inc	r0
	mov	a,#0x14
	add	a,@r0
	mov	r2,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r3,a
	inc	r0
	mov	ar4,@r0
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
	mov	r2,a
	mov	a,_bp
	add	a,#0x09
	mov	r0,a
	inc	r0
	mov	@r0,ar2
	dec	r0
	mov	@r0,#0x00
	mov	r0,_bp
	inc	r0
	mov	a,#0x13
	add	a,@r0
	mov	r2,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r3,a
	inc	r0
	mov	ar4,@r0
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
	mov	r2,a
	mov	r4,#0x00
	mov	a,_bp
	add	a,#0x09
	mov	r0,a
	mov	a,r2
	add	a,@r0
	mov	r2,a
	mov	a,r4
	inc	r0
	addc	a,@r0
	mov	r4,a
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	mov	a,r2
	lcall	__gptrput
	inc	dptr
	mov	a,r4
	lcall	__gptrput
;	usr/main.c:88: radar->dist3 = (((int)buf[22]<<8)+buf[21]);
	mov	a,_bp
	add	a,#0x0c
	mov	r0,a
	mov	a,#0x14
	add	a,@r0
	mov	r5,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r6,a
	inc	r0
	mov	ar7,@r0
	mov	r0,_bp
	inc	r0
	mov	a,#0x16
	add	a,@r0
	mov	r2,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r3,a
	inc	r0
	mov	ar4,@r0
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
	mov	r2,a
	mov	a,_bp
	add	a,#0x09
	mov	r0,a
	inc	r0
	mov	@r0,ar2
	dec	r0
	mov	@r0,#0x00
	mov	r0,_bp
	inc	r0
	mov	a,#0x15
	add	a,@r0
	mov	r2,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r3,a
	inc	r0
	mov	ar4,@r0
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
	mov	r2,a
	mov	r4,#0x00
	mov	a,_bp
	add	a,#0x09
	mov	r0,a
	mov	a,r2
	add	a,@r0
	mov	r2,a
	mov	a,r4
	inc	r0
	addc	a,@r0
	mov	r4,a
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	mov	a,r2
	lcall	__gptrput
	inc	dptr
	mov	a,r4
	lcall	__gptrput
;	usr/main.c:89: radar->ang3 = (((int)buf[24]<<8)+buf[23]);
	mov	a,_bp
	add	a,#0x0c
	mov	r0,a
	mov	a,#0x16
	add	a,@r0
	mov	@r0,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	@r0,a
	mov	r0,_bp
	inc	r0
	mov	a,#0x18
	add	a,@r0
	mov	r2,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r3,a
	inc	r0
	mov	ar4,@r0
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
	mov	r6,a
	mov	r7,#0x00
	mov	r0,_bp
	inc	r0
	mov	a,#0x17
	add	a,@r0
	mov	r2,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r3,a
	inc	r0
	mov	ar4,@r0
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
	mov	r4,#0x00
	add	a,r7
	mov	r2,a
	mov	a,r4
	addc	a,r6
	mov	r4,a
	mov	a,_bp
	add	a,#0x0c
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	inc	r0
	mov	b,@r0
	mov	a,r2
	lcall	__gptrput
	inc	dptr
	mov	a,r4
	lcall	__gptrput
;	usr/main.c:94: radar->ang3 = radar->ang3 / 10.0 - 90;
	mov	dpl,r2
	mov	dph,r4
	lcall	___sint2fs
	mov	r2,dpl
	mov	r3,dph
	mov	r4,b
	mov	r7,a
	clr	a
	push	acc
	push	acc
	mov	a,#0x20
	push	acc
	mov	a,#0x41
	push	acc
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	mov	a,r7
	lcall	___fsdiv
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	clr	a
	push	acc
	push	acc
	mov	a,#0xb4
	push	acc
	mov	a,#0x42
	push	acc
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	mov	a,r7
	lcall	___fssub
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	mov	a,r7
	lcall	___fs2sint
	mov	r7,dpl
	mov	r6,dph
	mov	a,_bp
	add	a,#0x0c
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	inc	r0
	mov	b,@r0
	mov	a,r7
	lcall	__gptrput
	inc	dptr
	mov	a,r6
	lcall	__gptrput
;	usr/main.c:96: return 0;
	mov	dpl,#0x00
;	usr/main.c:97: }
	mov	sp,_bp
	pop	_bp
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'LightTurn'
;------------------------------------------------------------
;Deg                       Allocated to registers r6 r7 
;Dir                       Allocated to registers b0 
;STEP                      Allocated to registers r4 r5 
;i                         Allocated to stack - _bp +1
;------------------------------------------------------------
;	usr/main.c:99: void LightTurn(BOOL Dir, UINT16 Deg)
;	-----------------------------------------
;	 function LightTurn
;	-----------------------------------------
_LightTurn:
	push	_bp
	mov	_bp,sp
	inc	sp
	inc	sp
	mov	r6,dpl
	mov	r7,dph
;	usr/main.c:101: GPIO_Init(PORT0,PIN6,MODE1);
	push	ar7
	push	ar6
	push	bits
	mov	a,#0x01
	push	acc
	mov	a,#0x40
	push	acc
	mov	dpl,#0x00
	lcall	_GPIO_Init
	dec	sp
	dec	sp
	pop	bits
	pop	ar6
	pop	ar7
;	usr/main.c:103: STEP = Deg * 5 * 8 / 1.8;
	push	ar7
	push	ar6
	push	bits
	push	ar6
	push	ar7
	mov	dptr,#0x0028
	lcall	__mulint
	mov	r4,dpl
	mov	r5,dph
	dec	sp
	dec	sp
	pop	bits
	mov	dpl,r4
	mov	dph,r5
	push	bits
	lcall	___uint2fs
	mov	r2,dpl
	mov	r3,dph
	mov	r4,b
	mov	r5,a
	pop	bits
	push	bits
	mov	a,#0x66
	push	acc
	push	acc
	mov	a,#0xe6
	push	acc
	mov	a,#0x3f
	push	acc
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	mov	a,r5
	lcall	___fsdiv
	mov	r2,dpl
	mov	r3,dph
	mov	r4,b
	mov	r5,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	pop	bits
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	mov	a,r5
	push	bits
	lcall	___fs2uint
	mov	r4,dpl
	mov	r5,dph
	pop	bits
;	usr/main.c:104: Direction = Dir;
;	assignBit
	mov	c,b0
	mov	_P0_6,c
;	usr/main.c:105: setCursor(0,4);
	push	ar5
	push	ar4
	push	bits
	mov	a,#0x04
	push	acc
	clr	a
	push	acc
	mov	dptr,#0x0000
	lcall	_setCursor
	dec	sp
	dec	sp
	pop	bits
	pop	ar4
	pop	ar5
	pop	ar6
	pop	ar7
;	usr/main.c:106: printf_fast_f("D:%d S:%3d G:%2d",Dir,STEP,Deg);
	mov	c,b0
	clr	a
	rlc	a
	mov	r2,a
	mov	r3,#0x00
	push	ar5
	push	ar4
	push	ar6
	push	ar7
	push	ar4
	push	ar5
	push	ar2
	push	ar3
	mov	a,#___str_0
	push	acc
	mov	a,#(___str_0 >> 8)
	push	acc
	lcall	_printf_fast_f
	mov	a,sp
	add	a,#0xf8
	mov	sp,a
	pop	ar4
	pop	ar5
;	usr/main.c:107: for (UINT16 i = 0; i < STEP; i++)
	mov	r0,_bp
	inc	r0
	clr	a
	mov	@r0,a
	inc	r0
	mov	@r0,a
00103$:
	mov	r0,_bp
	inc	r0
	clr	c
	mov	a,@r0
	subb	a,r4
	inc	r0
	mov	a,@r0
	subb	a,r5
	jnc	00105$
;	usr/main.c:109: Steps = 1;
;	assignBit
	setb	_P0_7
;	usr/main.c:110: mDelayuS(1000000/STEP);
	mov	ar2,r4
	mov	ar3,r5
	mov	r6,#0x00
	mov	r7,#0x00
	push	ar5
	push	ar4
	push	ar2
	push	ar3
	push	ar6
	push	ar7
	mov	dptr,#0x4240
	mov	b,#0x0f
	clr	a
	lcall	__divslong
	mov	r2,dpl
	mov	r3,dph
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	mov	dpl,r2
	mov	dph,r3
	push	ar3
	push	ar2
	lcall	_mDelayuS
	pop	ar2
	pop	ar3
;	usr/main.c:111: Steps = 0;
;	assignBit
	clr	_P0_7
;	usr/main.c:112: mDelayuS(1000000/STEP);
	mov	dpl,r2
	mov	dph,r3
	lcall	_mDelayuS
	pop	ar4
	pop	ar5
;	usr/main.c:107: for (UINT16 i = 0; i < STEP; i++)
	mov	r0,_bp
	inc	r0
	inc	@r0
	cjne	@r0,#0x00,00117$
	inc	r0
	inc	@r0
00117$:
	sjmp	00103$
00105$:
;	usr/main.c:115: }
	mov	sp,_bp
	pop	_bp
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'FaceTrack'
;------------------------------------------------------------
;A                         Allocated to registers r6 r7 
;------------------------------------------------------------
;	usr/main.c:117: void FaceTrack( int A)
;	-----------------------------------------
;	 function FaceTrack
;	-----------------------------------------
_FaceTrack:
	mov	r6,dpl
	mov	r7,dph
;	usr/main.c:119: if ( A < 85)
	clr	c
	mov	a,r6
	subb	a,#0x55
	mov	a,r7
	xrl	a,#0x80
	subb	a,#0x80
	jnc	00102$
;	usr/main.c:121: LightTurn(Counterclockwise, 1+(90-A)*5/9);
	mov	a,#0x5a
	clr	c
	subb	a,r6
	mov	r4,a
	clr	a
	subb	a,r7
	mov	r5,a
	push	ar7
	push	ar6
	push	ar4
	push	ar5
	mov	dptr,#0x0005
	lcall	__mulint
	mov	r4,dpl
	mov	r5,dph
	dec	sp
	dec	sp
	mov	a,#0x09
	push	acc
	clr	a
	push	acc
	mov	dpl,r4
	mov	dph,r5
	lcall	__divsint
	mov	r4,dpl
	mov	r5,dph
	dec	sp
	dec	sp
	pop	ar6
	pop	ar7
	inc	r4
	cjne	r4,#0x00,00116$
	inc	r5
00116$:
	clr	b[0]
	push	ar7
	push	ar6
	mov	bits,b
	mov	dpl,r4
	mov	dph,r5
	lcall	_LightTurn
	pop	ar6
	pop	ar7
00102$:
;	usr/main.c:123: if ( A > 95)
	clr	c
	mov	a,#0x5f
	subb	a,r6
	mov	a,#(0x00 ^ 0x80)
	mov	b,r7
	xrl	b,#0x80
	subb	a,b
	jnc	00105$
;	usr/main.c:125: LightTurn(Clockwise, 1+(A-90)*5/9);
	mov	a,r6
	add	a,#0xa6
	mov	r6,a
	mov	a,r7
	addc	a,#0xff
	mov	r7,a
	push	ar6
	push	ar7
	mov	dptr,#0x0005
	lcall	__mulint
	mov	r6,dpl
	mov	r7,dph
	dec	sp
	dec	sp
	mov	a,#0x09
	push	acc
	clr	a
	push	acc
	mov	dpl,r6
	mov	dph,r7
	lcall	__divsint
	mov	r6,dpl
	mov	r7,dph
	dec	sp
	dec	sp
	inc	r6
	cjne	r6,#0x00,00118$
	inc	r7
00118$:
	setb	b[0]
	mov	bits,b
	mov	dpl,r6
	mov	dph,r7
;	usr/main.c:128: }
	ljmp	_LightTurn
00105$:
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'main'
;------------------------------------------------------------
;object                    Allocated to stack - _bp +1
;------------------------------------------------------------
;	usr/main.c:135: void main()
;	-----------------------------------------
;	 function main
;	-----------------------------------------
_main:
	push	_bp
	mov	a,sp
	mov	_bp,a
	add	a,#0x19
	mov	sp,a
;	usr/main.c:137: CfgFsys( );           //CH549时钟选择配置
	lcall	_CfgFsys
;	usr/main.c:138: mDelaymS(20);
	mov	dptr,#0x0014
	lcall	_mDelaymS
;	usr/main.c:139: UART1Init(115200,1);
	mov	a,#0x01
	push	acc
	mov	dptr,#0xc200
	mov	b,#0x01
	clr	a
	lcall	_UART1Init
	dec	sp
;	usr/main.c:142: SPIMasterModeSet(3);  //SPI主机模式设置，模式3
	mov	dpl,#0x03
	lcall	_SPIMasterModeSet
;	usr/main.c:143: SPI_CK_SET(4);       //设置SPI sclk 时钟信号分频
	mov	_SPI0_CK_SE,#0x04
;	usr/main.c:144: OLED_Init();		  //初始化OLED  
	lcall	_OLED_Init
;	usr/main.c:145: OLED_Clear();         //将OLED屏幕上内容清除
	lcall	_OLED_Clear
;	usr/main.c:146: setFontSize(8);      //设置文字大小
	mov	dpl,#0x08
	lcall	_setFontSize
;	usr/main.c:148: OLED_Clear();
	lcall	_OLED_Clear
;	usr/main.c:149: setCursor(0,0);
	clr	a
	push	acc
	push	acc
	mov	dptr,#0x0000
	lcall	_setCursor
	dec	sp
	dec	sp
;	usr/main.c:150: printf_fast_f("Radar");
	mov	a,#___str_1
	push	acc
	mov	a,#(___str_1 >> 8)
	push	acc
	lcall	_printf_fast_f
	dec	sp
	dec	sp
;	usr/main.c:152: while(1){
00114$:
;	usr/main.c:153: if(fRecv){
	mov	dptr,#_fRecv
	movx	a,@dptr
	jz	00114$
;	usr/main.c:154: fRecv = 0;		
	mov	dptr,#_fRecv
	clr	a
	movx	@dptr,a
;	usr/main.c:155: if (pBuf == 28){
	mov	dptr,#_pBuf
	movx	a,@dptr
	mov	r7,a
	cjne	r7,#0x1c,00147$
	sjmp	00148$
00147$:
	ljmp	00107$
00148$:
;	usr/main.c:156: pBuf = 0;
	mov	dptr,#_pBuf
	clr	a
	movx	@dptr,a
;	usr/main.c:157: IE_UART1 = 0;
;	assignBit
	clr	_IE_UART1
;	usr/main.c:158: setCursor(0,3);
	mov	a,#0x03
	push	acc
	clr	a
	push	acc
	mov	dptr,#0x0000
	lcall	_setCursor
	dec	sp
	dec	sp
;	usr/main.c:159: if(recvBuf[0]==0XAA && recvBuf[26]==0X55 && recvBuf[27]==0XCC){
	mov	dptr,#_recvBuf
	movx	a,@dptr
	mov	r7,a
	cjne	r7,#0xaa,00149$
	sjmp	00150$
00149$:
	ljmp	00102$
00150$:
	mov	dptr,#(_recvBuf + 0x001a)
	movx	a,@dptr
	mov	r7,a
	cjne	r7,#0x55,00151$
	sjmp	00152$
00151$:
	ljmp	00102$
00152$:
	mov	dptr,#(_recvBuf + 0x001b)
	movx	a,@dptr
	mov	r7,a
	cjne	r7,#0xcc,00153$
	sjmp	00154$
00153$:
	ljmp	00102$
00154$:
;	usr/main.c:161: decode_XenP201S(recvBuf,&object);										
	mov	r1,_bp
	inc	r1
	mov	ar5,r1
	mov	r6,#0x00
	mov	r7,#0x40
	push	ar1
	push	ar5
	push	ar6
	push	ar7
	mov	dptr,#_recvBuf
	mov	b,#0x00
	lcall	_decode_XenP201S
	dec	sp
	dec	sp
	dec	sp
;	usr/main.c:162: setCursor(0,0);
	clr	a
	push	acc
	push	acc
	mov	dptr,#0x0000
	lcall	_setCursor
	dec	sp
	dec	sp
	pop	ar1
;	usr/main.c:163: printf_fast_f("X :%5d%5d%5d",object.x1Pos,object.x2Pos,object.x3Pos);
	mov	a,#0x10
	add	a,r1
	mov	r0,a
	mov	ar6,@r0
	inc	r0
	mov	ar7,@r0
	dec	r0
	mov	a,#0x08
	add	a,r1
	mov	r0,a
	mov	ar4,@r0
	inc	r0
	mov	ar5,@r0
	dec	r0
	mov	ar2,@r1
	inc	r1
	mov	ar3,@r1
	dec	r1
	push	ar1
	push	ar6
	push	ar7
	push	ar4
	push	ar5
	push	ar2
	push	ar3
	mov	a,#___str_2
	push	acc
	mov	a,#(___str_2 >> 8)
	push	acc
	lcall	_printf_fast_f
	mov	a,sp
	add	a,#0xf8
	mov	sp,a
;	usr/main.c:164: setCursor(0,1);
	mov	a,#0x01
	push	acc
	clr	a
	push	acc
	mov	dptr,#0x0000
	lcall	_setCursor
	dec	sp
	dec	sp
	pop	ar1
;	usr/main.c:165: printf_fast_f("Y :%5d%5d%5d",object.y1Pos,object.y2Pos,object.y3Pos);
	mov	a,#0x12
	add	a,r1
	mov	r0,a
	mov	ar6,@r0
	inc	r0
	mov	ar7,@r0
	dec	r0
	mov	a,#0x0a
	add	a,r1
	mov	r0,a
	mov	ar4,@r0
	inc	r0
	mov	ar5,@r0
	dec	r0
	mov	a,#0x02
	add	a,r1
	mov	r0,a
	mov	ar2,@r0
	inc	r0
	mov	ar3,@r0
	dec	r0
	push	ar1
	push	ar6
	push	ar7
	push	ar4
	push	ar5
	push	ar2
	push	ar3
	mov	a,#___str_3
	push	acc
	mov	a,#(___str_3 >> 8)
	push	acc
	lcall	_printf_fast_f
	mov	a,sp
	add	a,#0xf8
	mov	sp,a
;	usr/main.c:166: setCursor(0,2);
	mov	a,#0x02
	push	acc
	clr	a
	push	acc
	mov	dptr,#0x0000
	lcall	_setCursor
	dec	sp
	dec	sp
	pop	ar1
;	usr/main.c:167: printf_fast_f("D :%5d%5d%5d",object.dist1,object.dist2,object.dist3);
	mov	a,#0x14
	add	a,r1
	mov	r0,a
	mov	ar6,@r0
	inc	r0
	mov	ar7,@r0
	dec	r0
	mov	a,#0x0c
	add	a,r1
	mov	r0,a
	mov	ar4,@r0
	inc	r0
	mov	ar5,@r0
	dec	r0
	mov	a,#0x04
	add	a,r1
	mov	r0,a
	mov	ar2,@r0
	inc	r0
	mov	ar3,@r0
	dec	r0
	push	ar1
	push	ar6
	push	ar7
	push	ar4
	push	ar5
	push	ar2
	push	ar3
	mov	a,#___str_4
	push	acc
	mov	a,#(___str_4 >> 8)
	push	acc
	lcall	_printf_fast_f
	mov	a,sp
	add	a,#0xf8
	mov	sp,a
;	usr/main.c:168: setCursor(0,3);
	mov	a,#0x03
	push	acc
	clr	a
	push	acc
	mov	dptr,#0x0000
	lcall	_setCursor
	dec	sp
	dec	sp
	pop	ar1
;	usr/main.c:169: printf_fast_f("A :%5d%5d%5d",object.ang1,object.ang2,object.ang3);
	mov	a,#0x16
	add	a,r1
	mov	r0,a
	mov	ar6,@r0
	inc	r0
	mov	ar7,@r0
	dec	r0
	mov	a,#0x0e
	add	a,r1
	mov	r0,a
	mov	ar4,@r0
	inc	r0
	mov	ar5,@r0
	dec	r0
	mov	a,#0x06
	add	a,r1
	mov	r1,a
	mov	ar2,@r1
	inc	r1
	mov	ar3,@r1
	dec	r1
	push	ar6
	push	ar7
	push	ar4
	push	ar5
	push	ar2
	push	ar3
	mov	a,#___str_5
	push	acc
	mov	a,#(___str_5 >> 8)
	push	acc
	lcall	_printf_fast_f
	mov	a,sp
	add	a,#0xf8
	mov	sp,a
	sjmp	00103$
00102$:
;	usr/main.c:173: setCursor(0,0);
	clr	a
	push	acc
	push	acc
	mov	dptr,#0x0000
	lcall	_setCursor
	dec	sp
	dec	sp
;	usr/main.c:174: printf_fast_f(" %x %x %x ",recvBuf[0],recvBuf[26],recvBuf[27]);
	mov	dptr,#(_recvBuf + 0x001b)
	movx	a,@dptr
	mov	r7,a
	mov	r6,#0x00
	mov	dptr,#(_recvBuf + 0x001a)
	movx	a,@dptr
	mov	r5,a
	mov	r4,#0x00
	mov	dptr,#_recvBuf
	movx	a,@dptr
	mov	r3,a
	mov	r2,#0x00
	push	ar7
	push	ar6
	push	ar5
	push	ar4
	push	ar3
	push	ar2
	mov	a,#___str_6
	push	acc
	mov	a,#(___str_6 >> 8)
	push	acc
	lcall	_printf_fast_f
	mov	a,sp
	add	a,#0xf8
	mov	sp,a
00103$:
;	usr/main.c:176: IE_UART1 = 1;
;	assignBit
	setb	_IE_UART1
;	usr/main.c:177: FaceTrack(object.ang1);
	mov	a,_bp
	inc	a
	add	a,#0x06
	mov	r1,a
	mov	ar6,@r1
	inc	r1
	mov	ar7,@r1
	dec	r1
	mov	dpl,r6
	mov	dph,r7
	lcall	_FaceTrack
00107$:
;	usr/main.c:179: if(recvBuf[0]!=0XAA){
	mov	dptr,#_recvBuf
	movx	a,@dptr
	mov	r7,a
	cjne	r7,#0xaa,00155$
	sjmp	00109$
00155$:
;	usr/main.c:180: P2_2 = 0;
;	assignBit
	clr	_P2_2
;	usr/main.c:181: pBuf=0;
	mov	dptr,#_pBuf
	clr	a
	movx	@dptr,a
	ljmp	00114$
00109$:
;	usr/main.c:183: P2_2 =1;
;	assignBit
	setb	_P2_2
	ljmp	00114$
;	usr/main.c:187: }
	mov	sp,_bp
	pop	_bp
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'putchar'
;------------------------------------------------------------
;a                         Allocated to registers r6 r7 
;------------------------------------------------------------
;	usr/main.c:196: int putchar( int a)
;	-----------------------------------------
;	 function putchar
;	-----------------------------------------
_putchar:
	mov	r6,dpl
	mov	r7,dph
;	usr/main.c:199: OLED_ShowChar(oled_colum,oled_row,a);
	mov	ar5,r6
	mov	dptr,#_oled_row
	movx	a,@dptr
	mov	r3,a
	inc	dptr
	movx	a,@dptr
	mov	dptr,#_oled_colum
	movx	a,@dptr
	mov	r2,a
	inc	dptr
	movx	a,@dptr
	push	ar7
	push	ar6
	push	ar5
	push	ar3
	mov	dpl,r2
	lcall	_OLED_ShowChar
	dec	sp
	dec	sp
	pop	ar6
	pop	ar7
;	usr/main.c:201: oled_colum+=6;
	mov	dptr,#_oled_colum
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	mov	dptr,#_oled_colum
	mov	a,#0x06
	add	a,r4
	movx	@dptr,a
	clr	a
	addc	a,r5
	inc	dptr
	movx	@dptr,a
;	usr/main.c:206: if (oled_colum>122){oled_colum=0;oled_row+=1;}
	mov	dptr,#_oled_colum
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	clr	c
	mov	a,#0x7a
	subb	a,r4
	mov	a,#(0x00 ^ 0x80)
	mov	b,r5
	xrl	b,#0x80
	subb	a,b
	jnc	00102$
	mov	dptr,#_oled_colum
	clr	a
	movx	@dptr,a
	inc	dptr
	movx	@dptr,a
	mov	dptr,#_oled_row
	movx	a,@dptr
	mov	r4,a
	inc	dptr
	movx	a,@dptr
	mov	r5,a
	mov	dptr,#_oled_row
	mov	a,#0x01
	add	a,r4
	movx	@dptr,a
	clr	a
	addc	a,r5
	inc	dptr
	movx	@dptr,a
00102$:
;	usr/main.c:207: return(a);
	mov	dpl,r6
	mov	dph,r7
;	usr/main.c:208: }
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'setCursor'
;------------------------------------------------------------
;row                       Allocated to stack - _bp -4
;column                    Allocated to registers 
;------------------------------------------------------------
;	usr/main.c:216: void setCursor(int column,int row)
;	-----------------------------------------
;	 function setCursor
;	-----------------------------------------
_setCursor:
	push	_bp
	mov	_bp,sp
	mov	r7,dph
	mov	a,dpl
	mov	dptr,#_oled_colum
	movx	@dptr,a
	mov	a,r7
	inc	dptr
	movx	@dptr,a
;	usr/main.c:219: oled_row = row;
	mov	a,_bp
	add	a,#0xfc
	mov	r0,a
	mov	dptr,#_oled_row
	mov	a,@r0
	movx	@dptr,a
	inc	r0
	mov	a,@r0
	inc	dptr
	movx	@dptr,a
;	usr/main.c:220: }
	pop	_bp
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'UART1Interrupt'
;------------------------------------------------------------
;	usr/main.c:222: void UART1Interrupt(void) __interrupt INT_NO_UART1 __using 1 {
;	-----------------------------------------
;	 function UART1Interrupt
;	-----------------------------------------
_UART1Interrupt:
	ar7 = 0x0f
	ar6 = 0x0e
	ar5 = 0x0d
	ar4 = 0x0c
	ar3 = 0x0b
	ar2 = 0x0a
	ar1 = 0x09
	ar0 = 0x08
	push	bits
	push	acc
	push	b
	push	dpl
	push	dph
	push	(0+7)
	push	(0+6)
	push	(0+5)
	push	(0+4)
	push	(0+3)
	push	(0+2)
	push	(0+1)
	push	(0+0)
	push	psw
	mov	psw,#0x08
;	usr/main.c:223: pBuf%=28;
	mov	dptr,#_pBuf
	movx	a,@dptr
	mov	r7,a
	mov	r6,#0x00
	mov	a,#0x1c
	push	acc
	clr	a
	push	acc
	mov	dpl,r7
	mov	dph,r6
	mov	psw,#0x00
	lcall	__modsint
	mov	psw,#0x08
	mov	r6,dpl
	dec	sp
	dec	sp
	mov	dptr,#_pBuf
	mov	a,r6
	movx	@dptr,a
;	usr/main.c:224: recvBuf[pBuf] = CH549UART1RcvByte();
	movx	a,@dptr
	add	a,#_recvBuf
	mov	r7,a
	clr	a
	addc	a,#(_recvBuf >> 8)
	mov	r6,a
	push	ar7
	push	ar6
	mov	psw,#0x00
	lcall	_CH549UART1RcvByte
	mov	psw,#0x08
	mov	r5,dpl
	pop	ar6
	pop	ar7
	mov	dpl,r7
	mov	dph,r6
	mov	a,r5
	movx	@dptr,a
;	usr/main.c:225: pBuf++;
	mov	dptr,#_pBuf
	movx	a,@dptr
	inc	a
	movx	@dptr,a
;	usr/main.c:226: fRecv = 1;
	mov	dptr,#_fRecv
	mov	a,#0x01
	movx	@dptr,a
;	usr/main.c:227: }
	pop	psw
	pop	(0+0)
	pop	(0+1)
	pop	(0+2)
	pop	(0+3)
	pop	(0+4)
	pop	(0+5)
	pop	(0+6)
	pop	(0+7)
	pop	dph
	pop	dpl
	pop	b
	pop	acc
	pop	bits
	reti
	.area CSEG    (CODE)
	.area CONST   (CODE)
	.area CONST   (CODE)
___str_0:
	.ascii "D:%d S:%3d G:%2d"
	.db 0x00
	.area CSEG    (CODE)
	.area CONST   (CODE)
___str_1:
	.ascii "Radar"
	.db 0x00
	.area CSEG    (CODE)
	.area CONST   (CODE)
___str_2:
	.ascii "X :%5d%5d%5d"
	.db 0x00
	.area CSEG    (CODE)
	.area CONST   (CODE)
___str_3:
	.ascii "Y :%5d%5d%5d"
	.db 0x00
	.area CSEG    (CODE)
	.area CONST   (CODE)
___str_4:
	.ascii "D :%5d%5d%5d"
	.db 0x00
	.area CSEG    (CODE)
	.area CONST   (CODE)
___str_5:
	.ascii "A :%5d%5d%5d"
	.db 0x00
	.area CSEG    (CODE)
	.area CONST   (CODE)
___str_6:
	.ascii " %x %x %x "
	.db 0x00
	.area CSEG    (CODE)
	.area CABS    (ABS,CODE)
