                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 4.0.0 #11528 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module CH549_ADC
                                      6 	.optsdcc -mmcs51 --model-large
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _UIF_BUS_RST
                                     12 	.globl _UIF_DETECT
                                     13 	.globl _UIF_TRANSFER
                                     14 	.globl _UIF_SUSPEND
                                     15 	.globl _UIF_HST_SOF
                                     16 	.globl _UIF_FIFO_OV
                                     17 	.globl _U_SIE_FREE
                                     18 	.globl _U_TOG_OK
                                     19 	.globl _U_IS_NAK
                                     20 	.globl _S0_R_FIFO
                                     21 	.globl _S0_T_FIFO
                                     22 	.globl _S0_FREE
                                     23 	.globl _S0_IF_BYTE
                                     24 	.globl _S0_IF_FIRST
                                     25 	.globl _S0_IF_OV
                                     26 	.globl _S0_FST_ACT
                                     27 	.globl _CP_RL2
                                     28 	.globl _C_T2
                                     29 	.globl _TR2
                                     30 	.globl _EXEN2
                                     31 	.globl _TCLK
                                     32 	.globl _RCLK
                                     33 	.globl _EXF2
                                     34 	.globl _CAP1F
                                     35 	.globl _TF2
                                     36 	.globl _RI
                                     37 	.globl _TI
                                     38 	.globl _RB8
                                     39 	.globl _TB8
                                     40 	.globl _REN
                                     41 	.globl _SM2
                                     42 	.globl _SM1
                                     43 	.globl _SM0
                                     44 	.globl _IT0
                                     45 	.globl _IE0
                                     46 	.globl _IT1
                                     47 	.globl _IE1
                                     48 	.globl _TR0
                                     49 	.globl _TF0
                                     50 	.globl _TR1
                                     51 	.globl _TF1
                                     52 	.globl _XI
                                     53 	.globl _XO
                                     54 	.globl _P4_0
                                     55 	.globl _P4_1
                                     56 	.globl _P4_2
                                     57 	.globl _P4_3
                                     58 	.globl _P4_4
                                     59 	.globl _P4_5
                                     60 	.globl _P4_6
                                     61 	.globl _RXD
                                     62 	.globl _TXD
                                     63 	.globl _INT0
                                     64 	.globl _INT1
                                     65 	.globl _T0
                                     66 	.globl _T1
                                     67 	.globl _CAP0
                                     68 	.globl _INT3
                                     69 	.globl _P3_0
                                     70 	.globl _P3_1
                                     71 	.globl _P3_2
                                     72 	.globl _P3_3
                                     73 	.globl _P3_4
                                     74 	.globl _P3_5
                                     75 	.globl _P3_6
                                     76 	.globl _P3_7
                                     77 	.globl _PWM5
                                     78 	.globl _PWM4
                                     79 	.globl _INT0_
                                     80 	.globl _PWM3
                                     81 	.globl _PWM2
                                     82 	.globl _CAP1_
                                     83 	.globl _T2_
                                     84 	.globl _PWM1
                                     85 	.globl _CAP2_
                                     86 	.globl _T2EX_
                                     87 	.globl _PWM0
                                     88 	.globl _RXD1
                                     89 	.globl _PWM6
                                     90 	.globl _TXD1
                                     91 	.globl _PWM7
                                     92 	.globl _P2_0
                                     93 	.globl _P2_1
                                     94 	.globl _P2_2
                                     95 	.globl _P2_3
                                     96 	.globl _P2_4
                                     97 	.globl _P2_5
                                     98 	.globl _P2_6
                                     99 	.globl _P2_7
                                    100 	.globl _AIN0
                                    101 	.globl _CAP1
                                    102 	.globl _T2
                                    103 	.globl _AIN1
                                    104 	.globl _CAP2
                                    105 	.globl _T2EX
                                    106 	.globl _AIN2
                                    107 	.globl _AIN3
                                    108 	.globl _AIN4
                                    109 	.globl _UCC1
                                    110 	.globl _SCS
                                    111 	.globl _AIN5
                                    112 	.globl _UCC2
                                    113 	.globl _PWM0_
                                    114 	.globl _MOSI
                                    115 	.globl _AIN6
                                    116 	.globl _VBUS
                                    117 	.globl _RXD1_
                                    118 	.globl _MISO
                                    119 	.globl _AIN7
                                    120 	.globl _TXD1_
                                    121 	.globl _SCK
                                    122 	.globl _P1_0
                                    123 	.globl _P1_1
                                    124 	.globl _P1_2
                                    125 	.globl _P1_3
                                    126 	.globl _P1_4
                                    127 	.globl _P1_5
                                    128 	.globl _P1_6
                                    129 	.globl _P1_7
                                    130 	.globl _AIN8
                                    131 	.globl _AIN9
                                    132 	.globl _AIN10
                                    133 	.globl _RXD_
                                    134 	.globl _AIN11
                                    135 	.globl _TXD_
                                    136 	.globl _AIN12
                                    137 	.globl _RXD2
                                    138 	.globl _AIN13
                                    139 	.globl _TXD2
                                    140 	.globl _AIN14
                                    141 	.globl _RXD3
                                    142 	.globl _AIN15
                                    143 	.globl _TXD3
                                    144 	.globl _P0_0
                                    145 	.globl _P0_1
                                    146 	.globl _P0_2
                                    147 	.globl _P0_3
                                    148 	.globl _P0_4
                                    149 	.globl _P0_5
                                    150 	.globl _P0_6
                                    151 	.globl _P0_7
                                    152 	.globl _IE_SPI0
                                    153 	.globl _IE_INT3
                                    154 	.globl _IE_USB
                                    155 	.globl _IE_UART2
                                    156 	.globl _IE_ADC
                                    157 	.globl _IE_UART1
                                    158 	.globl _IE_UART3
                                    159 	.globl _IE_PWMX
                                    160 	.globl _IE_GPIO
                                    161 	.globl _IE_WDOG
                                    162 	.globl _PX0
                                    163 	.globl _PT0
                                    164 	.globl _PX1
                                    165 	.globl _PT1
                                    166 	.globl _PS
                                    167 	.globl _PT2
                                    168 	.globl _PL_FLAG
                                    169 	.globl _PH_FLAG
                                    170 	.globl _EX0
                                    171 	.globl _ET0
                                    172 	.globl _EX1
                                    173 	.globl _ET1
                                    174 	.globl _ES
                                    175 	.globl _ET2
                                    176 	.globl _E_DIS
                                    177 	.globl _EA
                                    178 	.globl _P
                                    179 	.globl _F1
                                    180 	.globl _OV
                                    181 	.globl _RS0
                                    182 	.globl _RS1
                                    183 	.globl _F0
                                    184 	.globl _AC
                                    185 	.globl _CY
                                    186 	.globl _UEP1_DMA_H
                                    187 	.globl _UEP1_DMA_L
                                    188 	.globl _UEP1_DMA
                                    189 	.globl _UEP0_DMA_H
                                    190 	.globl _UEP0_DMA_L
                                    191 	.globl _UEP0_DMA
                                    192 	.globl _UEP2_3_MOD
                                    193 	.globl _UEP4_1_MOD
                                    194 	.globl _UEP3_DMA_H
                                    195 	.globl _UEP3_DMA_L
                                    196 	.globl _UEP3_DMA
                                    197 	.globl _UEP2_DMA_H
                                    198 	.globl _UEP2_DMA_L
                                    199 	.globl _UEP2_DMA
                                    200 	.globl _USB_DEV_AD
                                    201 	.globl _USB_CTRL
                                    202 	.globl _USB_INT_EN
                                    203 	.globl _UEP4_T_LEN
                                    204 	.globl _UEP4_CTRL
                                    205 	.globl _UEP0_T_LEN
                                    206 	.globl _UEP0_CTRL
                                    207 	.globl _USB_RX_LEN
                                    208 	.globl _USB_MIS_ST
                                    209 	.globl _USB_INT_ST
                                    210 	.globl _USB_INT_FG
                                    211 	.globl _UEP3_T_LEN
                                    212 	.globl _UEP3_CTRL
                                    213 	.globl _UEP2_T_LEN
                                    214 	.globl _UEP2_CTRL
                                    215 	.globl _UEP1_T_LEN
                                    216 	.globl _UEP1_CTRL
                                    217 	.globl _UDEV_CTRL
                                    218 	.globl _USB_C_CTRL
                                    219 	.globl _ADC_PIN
                                    220 	.globl _ADC_CHAN
                                    221 	.globl _ADC_DAT_H
                                    222 	.globl _ADC_DAT_L
                                    223 	.globl _ADC_DAT
                                    224 	.globl _ADC_CFG
                                    225 	.globl _ADC_CTRL
                                    226 	.globl _TKEY_CTRL
                                    227 	.globl _SIF3
                                    228 	.globl _SBAUD3
                                    229 	.globl _SBUF3
                                    230 	.globl _SCON3
                                    231 	.globl _SIF2
                                    232 	.globl _SBAUD2
                                    233 	.globl _SBUF2
                                    234 	.globl _SCON2
                                    235 	.globl _SIF1
                                    236 	.globl _SBAUD1
                                    237 	.globl _SBUF1
                                    238 	.globl _SCON1
                                    239 	.globl _SPI0_SETUP
                                    240 	.globl _SPI0_CK_SE
                                    241 	.globl _SPI0_CTRL
                                    242 	.globl _SPI0_DATA
                                    243 	.globl _SPI0_STAT
                                    244 	.globl _PWM_DATA7
                                    245 	.globl _PWM_DATA6
                                    246 	.globl _PWM_DATA5
                                    247 	.globl _PWM_DATA4
                                    248 	.globl _PWM_DATA3
                                    249 	.globl _PWM_CTRL2
                                    250 	.globl _PWM_CK_SE
                                    251 	.globl _PWM_CTRL
                                    252 	.globl _PWM_DATA0
                                    253 	.globl _PWM_DATA1
                                    254 	.globl _PWM_DATA2
                                    255 	.globl _T2CAP1H
                                    256 	.globl _T2CAP1L
                                    257 	.globl _T2CAP1
                                    258 	.globl _TH2
                                    259 	.globl _TL2
                                    260 	.globl _T2COUNT
                                    261 	.globl _RCAP2H
                                    262 	.globl _RCAP2L
                                    263 	.globl _RCAP2
                                    264 	.globl _T2MOD
                                    265 	.globl _T2CON
                                    266 	.globl _T2CAP0H
                                    267 	.globl _T2CAP0L
                                    268 	.globl _T2CAP0
                                    269 	.globl _T2CON2
                                    270 	.globl _SBUF
                                    271 	.globl _SCON
                                    272 	.globl _TH1
                                    273 	.globl _TH0
                                    274 	.globl _TL1
                                    275 	.globl _TL0
                                    276 	.globl _TMOD
                                    277 	.globl _TCON
                                    278 	.globl _XBUS_AUX
                                    279 	.globl _PIN_FUNC
                                    280 	.globl _P5
                                    281 	.globl _P4_DIR_PU
                                    282 	.globl _P4_MOD_OC
                                    283 	.globl _P4
                                    284 	.globl _P3_DIR_PU
                                    285 	.globl _P3_MOD_OC
                                    286 	.globl _P3
                                    287 	.globl _P2_DIR_PU
                                    288 	.globl _P2_MOD_OC
                                    289 	.globl _P2
                                    290 	.globl _P1_DIR_PU
                                    291 	.globl _P1_MOD_OC
                                    292 	.globl _P1
                                    293 	.globl _P0_DIR_PU
                                    294 	.globl _P0_MOD_OC
                                    295 	.globl _P0
                                    296 	.globl _ROM_CTRL
                                    297 	.globl _ROM_DATA_HH
                                    298 	.globl _ROM_DATA_HL
                                    299 	.globl _ROM_DATA_HI
                                    300 	.globl _ROM_ADDR_H
                                    301 	.globl _ROM_ADDR_L
                                    302 	.globl _ROM_ADDR
                                    303 	.globl _GPIO_IE
                                    304 	.globl _INTX
                                    305 	.globl _IP_EX
                                    306 	.globl _IE_EX
                                    307 	.globl _IP
                                    308 	.globl _IE
                                    309 	.globl _WDOG_COUNT
                                    310 	.globl _RESET_KEEP
                                    311 	.globl _WAKE_CTRL
                                    312 	.globl _CLOCK_CFG
                                    313 	.globl _POWER_CFG
                                    314 	.globl _PCON
                                    315 	.globl _GLOBAL_CFG
                                    316 	.globl _SAFE_MOD
                                    317 	.globl _DPH
                                    318 	.globl _DPL
                                    319 	.globl _SP
                                    320 	.globl _A_INV
                                    321 	.globl _B
                                    322 	.globl _ACC
                                    323 	.globl _PSW
                                    324 	.globl _ADC_ExInit
                                    325 	.globl _ADC_ChSelect
                                    326 	.globl _ADC_InTSInit
                                    327 ;--------------------------------------------------------
                                    328 ; special function registers
                                    329 ;--------------------------------------------------------
                                    330 	.area RSEG    (ABS,DATA)
      000000                        331 	.org 0x0000
                           0000D0   332 _PSW	=	0x00d0
                           0000E0   333 _ACC	=	0x00e0
                           0000F0   334 _B	=	0x00f0
                           0000FD   335 _A_INV	=	0x00fd
                           000081   336 _SP	=	0x0081
                           000082   337 _DPL	=	0x0082
                           000083   338 _DPH	=	0x0083
                           0000A1   339 _SAFE_MOD	=	0x00a1
                           0000B1   340 _GLOBAL_CFG	=	0x00b1
                           000087   341 _PCON	=	0x0087
                           0000BA   342 _POWER_CFG	=	0x00ba
                           0000B9   343 _CLOCK_CFG	=	0x00b9
                           0000A9   344 _WAKE_CTRL	=	0x00a9
                           0000FE   345 _RESET_KEEP	=	0x00fe
                           0000FF   346 _WDOG_COUNT	=	0x00ff
                           0000A8   347 _IE	=	0x00a8
                           0000B8   348 _IP	=	0x00b8
                           0000E8   349 _IE_EX	=	0x00e8
                           0000E9   350 _IP_EX	=	0x00e9
                           0000B3   351 _INTX	=	0x00b3
                           0000B2   352 _GPIO_IE	=	0x00b2
                           008584   353 _ROM_ADDR	=	0x8584
                           000084   354 _ROM_ADDR_L	=	0x0084
                           000085   355 _ROM_ADDR_H	=	0x0085
                           008F8E   356 _ROM_DATA_HI	=	0x8f8e
                           00008E   357 _ROM_DATA_HL	=	0x008e
                           00008F   358 _ROM_DATA_HH	=	0x008f
                           000086   359 _ROM_CTRL	=	0x0086
                           000080   360 _P0	=	0x0080
                           0000C4   361 _P0_MOD_OC	=	0x00c4
                           0000C5   362 _P0_DIR_PU	=	0x00c5
                           000090   363 _P1	=	0x0090
                           000092   364 _P1_MOD_OC	=	0x0092
                           000093   365 _P1_DIR_PU	=	0x0093
                           0000A0   366 _P2	=	0x00a0
                           000094   367 _P2_MOD_OC	=	0x0094
                           000095   368 _P2_DIR_PU	=	0x0095
                           0000B0   369 _P3	=	0x00b0
                           000096   370 _P3_MOD_OC	=	0x0096
                           000097   371 _P3_DIR_PU	=	0x0097
                           0000C0   372 _P4	=	0x00c0
                           0000C2   373 _P4_MOD_OC	=	0x00c2
                           0000C3   374 _P4_DIR_PU	=	0x00c3
                           0000AB   375 _P5	=	0x00ab
                           0000AA   376 _PIN_FUNC	=	0x00aa
                           0000A2   377 _XBUS_AUX	=	0x00a2
                           000088   378 _TCON	=	0x0088
                           000089   379 _TMOD	=	0x0089
                           00008A   380 _TL0	=	0x008a
                           00008B   381 _TL1	=	0x008b
                           00008C   382 _TH0	=	0x008c
                           00008D   383 _TH1	=	0x008d
                           000098   384 _SCON	=	0x0098
                           000099   385 _SBUF	=	0x0099
                           0000C1   386 _T2CON2	=	0x00c1
                           00C7C6   387 _T2CAP0	=	0xc7c6
                           0000C6   388 _T2CAP0L	=	0x00c6
                           0000C7   389 _T2CAP0H	=	0x00c7
                           0000C8   390 _T2CON	=	0x00c8
                           0000C9   391 _T2MOD	=	0x00c9
                           00CBCA   392 _RCAP2	=	0xcbca
                           0000CA   393 _RCAP2L	=	0x00ca
                           0000CB   394 _RCAP2H	=	0x00cb
                           00CDCC   395 _T2COUNT	=	0xcdcc
                           0000CC   396 _TL2	=	0x00cc
                           0000CD   397 _TH2	=	0x00cd
                           00CFCE   398 _T2CAP1	=	0xcfce
                           0000CE   399 _T2CAP1L	=	0x00ce
                           0000CF   400 _T2CAP1H	=	0x00cf
                           00009A   401 _PWM_DATA2	=	0x009a
                           00009B   402 _PWM_DATA1	=	0x009b
                           00009C   403 _PWM_DATA0	=	0x009c
                           00009D   404 _PWM_CTRL	=	0x009d
                           00009E   405 _PWM_CK_SE	=	0x009e
                           00009F   406 _PWM_CTRL2	=	0x009f
                           0000A3   407 _PWM_DATA3	=	0x00a3
                           0000A4   408 _PWM_DATA4	=	0x00a4
                           0000A5   409 _PWM_DATA5	=	0x00a5
                           0000A6   410 _PWM_DATA6	=	0x00a6
                           0000A7   411 _PWM_DATA7	=	0x00a7
                           0000F8   412 _SPI0_STAT	=	0x00f8
                           0000F9   413 _SPI0_DATA	=	0x00f9
                           0000FA   414 _SPI0_CTRL	=	0x00fa
                           0000FB   415 _SPI0_CK_SE	=	0x00fb
                           0000FC   416 _SPI0_SETUP	=	0x00fc
                           0000BC   417 _SCON1	=	0x00bc
                           0000BD   418 _SBUF1	=	0x00bd
                           0000BE   419 _SBAUD1	=	0x00be
                           0000BF   420 _SIF1	=	0x00bf
                           0000B4   421 _SCON2	=	0x00b4
                           0000B5   422 _SBUF2	=	0x00b5
                           0000B6   423 _SBAUD2	=	0x00b6
                           0000B7   424 _SIF2	=	0x00b7
                           0000AC   425 _SCON3	=	0x00ac
                           0000AD   426 _SBUF3	=	0x00ad
                           0000AE   427 _SBAUD3	=	0x00ae
                           0000AF   428 _SIF3	=	0x00af
                           0000F1   429 _TKEY_CTRL	=	0x00f1
                           0000F2   430 _ADC_CTRL	=	0x00f2
                           0000F3   431 _ADC_CFG	=	0x00f3
                           00F5F4   432 _ADC_DAT	=	0xf5f4
                           0000F4   433 _ADC_DAT_L	=	0x00f4
                           0000F5   434 _ADC_DAT_H	=	0x00f5
                           0000F6   435 _ADC_CHAN	=	0x00f6
                           0000F7   436 _ADC_PIN	=	0x00f7
                           000091   437 _USB_C_CTRL	=	0x0091
                           0000D1   438 _UDEV_CTRL	=	0x00d1
                           0000D2   439 _UEP1_CTRL	=	0x00d2
                           0000D3   440 _UEP1_T_LEN	=	0x00d3
                           0000D4   441 _UEP2_CTRL	=	0x00d4
                           0000D5   442 _UEP2_T_LEN	=	0x00d5
                           0000D6   443 _UEP3_CTRL	=	0x00d6
                           0000D7   444 _UEP3_T_LEN	=	0x00d7
                           0000D8   445 _USB_INT_FG	=	0x00d8
                           0000D9   446 _USB_INT_ST	=	0x00d9
                           0000DA   447 _USB_MIS_ST	=	0x00da
                           0000DB   448 _USB_RX_LEN	=	0x00db
                           0000DC   449 _UEP0_CTRL	=	0x00dc
                           0000DD   450 _UEP0_T_LEN	=	0x00dd
                           0000DE   451 _UEP4_CTRL	=	0x00de
                           0000DF   452 _UEP4_T_LEN	=	0x00df
                           0000E1   453 _USB_INT_EN	=	0x00e1
                           0000E2   454 _USB_CTRL	=	0x00e2
                           0000E3   455 _USB_DEV_AD	=	0x00e3
                           00E5E4   456 _UEP2_DMA	=	0xe5e4
                           0000E4   457 _UEP2_DMA_L	=	0x00e4
                           0000E5   458 _UEP2_DMA_H	=	0x00e5
                           00E7E6   459 _UEP3_DMA	=	0xe7e6
                           0000E6   460 _UEP3_DMA_L	=	0x00e6
                           0000E7   461 _UEP3_DMA_H	=	0x00e7
                           0000EA   462 _UEP4_1_MOD	=	0x00ea
                           0000EB   463 _UEP2_3_MOD	=	0x00eb
                           00EDEC   464 _UEP0_DMA	=	0xedec
                           0000EC   465 _UEP0_DMA_L	=	0x00ec
                           0000ED   466 _UEP0_DMA_H	=	0x00ed
                           00EFEE   467 _UEP1_DMA	=	0xefee
                           0000EE   468 _UEP1_DMA_L	=	0x00ee
                           0000EF   469 _UEP1_DMA_H	=	0x00ef
                                    470 ;--------------------------------------------------------
                                    471 ; special function bits
                                    472 ;--------------------------------------------------------
                                    473 	.area RSEG    (ABS,DATA)
      000000                        474 	.org 0x0000
                           0000D7   475 _CY	=	0x00d7
                           0000D6   476 _AC	=	0x00d6
                           0000D5   477 _F0	=	0x00d5
                           0000D4   478 _RS1	=	0x00d4
                           0000D3   479 _RS0	=	0x00d3
                           0000D2   480 _OV	=	0x00d2
                           0000D1   481 _F1	=	0x00d1
                           0000D0   482 _P	=	0x00d0
                           0000AF   483 _EA	=	0x00af
                           0000AE   484 _E_DIS	=	0x00ae
                           0000AD   485 _ET2	=	0x00ad
                           0000AC   486 _ES	=	0x00ac
                           0000AB   487 _ET1	=	0x00ab
                           0000AA   488 _EX1	=	0x00aa
                           0000A9   489 _ET0	=	0x00a9
                           0000A8   490 _EX0	=	0x00a8
                           0000BF   491 _PH_FLAG	=	0x00bf
                           0000BE   492 _PL_FLAG	=	0x00be
                           0000BD   493 _PT2	=	0x00bd
                           0000BC   494 _PS	=	0x00bc
                           0000BB   495 _PT1	=	0x00bb
                           0000BA   496 _PX1	=	0x00ba
                           0000B9   497 _PT0	=	0x00b9
                           0000B8   498 _PX0	=	0x00b8
                           0000EF   499 _IE_WDOG	=	0x00ef
                           0000EE   500 _IE_GPIO	=	0x00ee
                           0000ED   501 _IE_PWMX	=	0x00ed
                           0000ED   502 _IE_UART3	=	0x00ed
                           0000EC   503 _IE_UART1	=	0x00ec
                           0000EB   504 _IE_ADC	=	0x00eb
                           0000EB   505 _IE_UART2	=	0x00eb
                           0000EA   506 _IE_USB	=	0x00ea
                           0000E9   507 _IE_INT3	=	0x00e9
                           0000E8   508 _IE_SPI0	=	0x00e8
                           000087   509 _P0_7	=	0x0087
                           000086   510 _P0_6	=	0x0086
                           000085   511 _P0_5	=	0x0085
                           000084   512 _P0_4	=	0x0084
                           000083   513 _P0_3	=	0x0083
                           000082   514 _P0_2	=	0x0082
                           000081   515 _P0_1	=	0x0081
                           000080   516 _P0_0	=	0x0080
                           000087   517 _TXD3	=	0x0087
                           000087   518 _AIN15	=	0x0087
                           000086   519 _RXD3	=	0x0086
                           000086   520 _AIN14	=	0x0086
                           000085   521 _TXD2	=	0x0085
                           000085   522 _AIN13	=	0x0085
                           000084   523 _RXD2	=	0x0084
                           000084   524 _AIN12	=	0x0084
                           000083   525 _TXD_	=	0x0083
                           000083   526 _AIN11	=	0x0083
                           000082   527 _RXD_	=	0x0082
                           000082   528 _AIN10	=	0x0082
                           000081   529 _AIN9	=	0x0081
                           000080   530 _AIN8	=	0x0080
                           000097   531 _P1_7	=	0x0097
                           000096   532 _P1_6	=	0x0096
                           000095   533 _P1_5	=	0x0095
                           000094   534 _P1_4	=	0x0094
                           000093   535 _P1_3	=	0x0093
                           000092   536 _P1_2	=	0x0092
                           000091   537 _P1_1	=	0x0091
                           000090   538 _P1_0	=	0x0090
                           000097   539 _SCK	=	0x0097
                           000097   540 _TXD1_	=	0x0097
                           000097   541 _AIN7	=	0x0097
                           000096   542 _MISO	=	0x0096
                           000096   543 _RXD1_	=	0x0096
                           000096   544 _VBUS	=	0x0096
                           000096   545 _AIN6	=	0x0096
                           000095   546 _MOSI	=	0x0095
                           000095   547 _PWM0_	=	0x0095
                           000095   548 _UCC2	=	0x0095
                           000095   549 _AIN5	=	0x0095
                           000094   550 _SCS	=	0x0094
                           000094   551 _UCC1	=	0x0094
                           000094   552 _AIN4	=	0x0094
                           000093   553 _AIN3	=	0x0093
                           000092   554 _AIN2	=	0x0092
                           000091   555 _T2EX	=	0x0091
                           000091   556 _CAP2	=	0x0091
                           000091   557 _AIN1	=	0x0091
                           000090   558 _T2	=	0x0090
                           000090   559 _CAP1	=	0x0090
                           000090   560 _AIN0	=	0x0090
                           0000A7   561 _P2_7	=	0x00a7
                           0000A6   562 _P2_6	=	0x00a6
                           0000A5   563 _P2_5	=	0x00a5
                           0000A4   564 _P2_4	=	0x00a4
                           0000A3   565 _P2_3	=	0x00a3
                           0000A2   566 _P2_2	=	0x00a2
                           0000A1   567 _P2_1	=	0x00a1
                           0000A0   568 _P2_0	=	0x00a0
                           0000A7   569 _PWM7	=	0x00a7
                           0000A7   570 _TXD1	=	0x00a7
                           0000A6   571 _PWM6	=	0x00a6
                           0000A6   572 _RXD1	=	0x00a6
                           0000A5   573 _PWM0	=	0x00a5
                           0000A5   574 _T2EX_	=	0x00a5
                           0000A5   575 _CAP2_	=	0x00a5
                           0000A4   576 _PWM1	=	0x00a4
                           0000A4   577 _T2_	=	0x00a4
                           0000A4   578 _CAP1_	=	0x00a4
                           0000A3   579 _PWM2	=	0x00a3
                           0000A2   580 _PWM3	=	0x00a2
                           0000A2   581 _INT0_	=	0x00a2
                           0000A1   582 _PWM4	=	0x00a1
                           0000A0   583 _PWM5	=	0x00a0
                           0000B7   584 _P3_7	=	0x00b7
                           0000B6   585 _P3_6	=	0x00b6
                           0000B5   586 _P3_5	=	0x00b5
                           0000B4   587 _P3_4	=	0x00b4
                           0000B3   588 _P3_3	=	0x00b3
                           0000B2   589 _P3_2	=	0x00b2
                           0000B1   590 _P3_1	=	0x00b1
                           0000B0   591 _P3_0	=	0x00b0
                           0000B7   592 _INT3	=	0x00b7
                           0000B6   593 _CAP0	=	0x00b6
                           0000B5   594 _T1	=	0x00b5
                           0000B4   595 _T0	=	0x00b4
                           0000B3   596 _INT1	=	0x00b3
                           0000B2   597 _INT0	=	0x00b2
                           0000B1   598 _TXD	=	0x00b1
                           0000B0   599 _RXD	=	0x00b0
                           0000C6   600 _P4_6	=	0x00c6
                           0000C5   601 _P4_5	=	0x00c5
                           0000C4   602 _P4_4	=	0x00c4
                           0000C3   603 _P4_3	=	0x00c3
                           0000C2   604 _P4_2	=	0x00c2
                           0000C1   605 _P4_1	=	0x00c1
                           0000C0   606 _P4_0	=	0x00c0
                           0000C7   607 _XO	=	0x00c7
                           0000C6   608 _XI	=	0x00c6
                           00008F   609 _TF1	=	0x008f
                           00008E   610 _TR1	=	0x008e
                           00008D   611 _TF0	=	0x008d
                           00008C   612 _TR0	=	0x008c
                           00008B   613 _IE1	=	0x008b
                           00008A   614 _IT1	=	0x008a
                           000089   615 _IE0	=	0x0089
                           000088   616 _IT0	=	0x0088
                           00009F   617 _SM0	=	0x009f
                           00009E   618 _SM1	=	0x009e
                           00009D   619 _SM2	=	0x009d
                           00009C   620 _REN	=	0x009c
                           00009B   621 _TB8	=	0x009b
                           00009A   622 _RB8	=	0x009a
                           000099   623 _TI	=	0x0099
                           000098   624 _RI	=	0x0098
                           0000CF   625 _TF2	=	0x00cf
                           0000CF   626 _CAP1F	=	0x00cf
                           0000CE   627 _EXF2	=	0x00ce
                           0000CD   628 _RCLK	=	0x00cd
                           0000CC   629 _TCLK	=	0x00cc
                           0000CB   630 _EXEN2	=	0x00cb
                           0000CA   631 _TR2	=	0x00ca
                           0000C9   632 _C_T2	=	0x00c9
                           0000C8   633 _CP_RL2	=	0x00c8
                           0000FF   634 _S0_FST_ACT	=	0x00ff
                           0000FE   635 _S0_IF_OV	=	0x00fe
                           0000FD   636 _S0_IF_FIRST	=	0x00fd
                           0000FC   637 _S0_IF_BYTE	=	0x00fc
                           0000FB   638 _S0_FREE	=	0x00fb
                           0000FA   639 _S0_T_FIFO	=	0x00fa
                           0000F8   640 _S0_R_FIFO	=	0x00f8
                           0000DF   641 _U_IS_NAK	=	0x00df
                           0000DE   642 _U_TOG_OK	=	0x00de
                           0000DD   643 _U_SIE_FREE	=	0x00dd
                           0000DC   644 _UIF_FIFO_OV	=	0x00dc
                           0000DB   645 _UIF_HST_SOF	=	0x00db
                           0000DA   646 _UIF_SUSPEND	=	0x00da
                           0000D9   647 _UIF_TRANSFER	=	0x00d9
                           0000D8   648 _UIF_DETECT	=	0x00d8
                           0000D8   649 _UIF_BUS_RST	=	0x00d8
                                    650 ;--------------------------------------------------------
                                    651 ; overlayable register banks
                                    652 ;--------------------------------------------------------
                                    653 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        654 	.ds 8
                                    655 ;--------------------------------------------------------
                                    656 ; internal ram data
                                    657 ;--------------------------------------------------------
                                    658 	.area DSEG    (DATA)
                                    659 ;--------------------------------------------------------
                                    660 ; overlayable items in internal ram 
                                    661 ;--------------------------------------------------------
                                    662 ;--------------------------------------------------------
                                    663 ; indirectly addressable internal ram data
                                    664 ;--------------------------------------------------------
                                    665 	.area ISEG    (DATA)
                                    666 ;--------------------------------------------------------
                                    667 ; absolute internal ram data
                                    668 ;--------------------------------------------------------
                                    669 	.area IABS    (ABS,DATA)
                                    670 	.area IABS    (ABS,DATA)
                                    671 ;--------------------------------------------------------
                                    672 ; bit data
                                    673 ;--------------------------------------------------------
                                    674 	.area BSEG    (BIT)
                                    675 ;--------------------------------------------------------
                                    676 ; paged external ram data
                                    677 ;--------------------------------------------------------
                                    678 	.area PSEG    (PAG,XDATA)
                                    679 ;--------------------------------------------------------
                                    680 ; external ram data
                                    681 ;--------------------------------------------------------
                                    682 	.area XSEG    (XDATA)
                                    683 ;--------------------------------------------------------
                                    684 ; absolute external ram data
                                    685 ;--------------------------------------------------------
                                    686 	.area XABS    (ABS,XDATA)
                                    687 ;--------------------------------------------------------
                                    688 ; external initialized ram data
                                    689 ;--------------------------------------------------------
                                    690 	.area HOME    (CODE)
                                    691 	.area GSINIT0 (CODE)
                                    692 	.area GSINIT1 (CODE)
                                    693 	.area GSINIT2 (CODE)
                                    694 	.area GSINIT3 (CODE)
                                    695 	.area GSINIT4 (CODE)
                                    696 	.area GSINIT5 (CODE)
                                    697 	.area GSINIT  (CODE)
                                    698 	.area GSFINAL (CODE)
                                    699 	.area CSEG    (CODE)
                                    700 ;--------------------------------------------------------
                                    701 ; global & static initialisations
                                    702 ;--------------------------------------------------------
                                    703 	.area HOME    (CODE)
                                    704 	.area GSINIT  (CODE)
                                    705 	.area GSFINAL (CODE)
                                    706 	.area GSINIT  (CODE)
                                    707 ;--------------------------------------------------------
                                    708 ; Home
                                    709 ;--------------------------------------------------------
                                    710 	.area HOME    (CODE)
                                    711 	.area HOME    (CODE)
                                    712 ;--------------------------------------------------------
                                    713 ; code
                                    714 ;--------------------------------------------------------
                                    715 	.area CSEG    (CODE)
                                    716 ;------------------------------------------------------------
                                    717 ;Allocation info for local variables in function 'ADC_ExInit'
                                    718 ;------------------------------------------------------------
                                    719 ;AdcClk                    Allocated to registers r7 
                                    720 ;dat                       Allocated to registers 
                                    721 ;------------------------------------------------------------
                                    722 ;	source/CH549_ADC.c:23: void ADC_ExInit( UINT8 AdcClk )
                                    723 ;	-----------------------------------------
                                    724 ;	 function ADC_ExInit
                                    725 ;	-----------------------------------------
      000D79                        726 _ADC_ExInit:
                           000007   727 	ar7 = 0x07
                           000006   728 	ar6 = 0x06
                           000005   729 	ar5 = 0x05
                           000004   730 	ar4 = 0x04
                           000003   731 	ar3 = 0x03
                           000002   732 	ar2 = 0x02
                           000001   733 	ar1 = 0x01
                           000000   734 	ar0 = 0x00
      000D79 AF 82            [24]  735 	mov	r7,dpl
                                    736 ;	source/CH549_ADC.c:26: ADC_CFG |= bADC_EN;                              //开启ADC模块电源
      000D7B 43 F3 08         [24]  737 	orl	_ADC_CFG,#0x08
                                    738 ;	source/CH549_ADC.c:27: ADC_CFG = (ADC_CFG & ~(bADC_CLK0 | bADC_CLK1))|AdcClk;//选择ADC参考时钟
      000D7E 74 FC            [12]  739 	mov	a,#0xfc
      000D80 55 F3            [12]  740 	anl	a,_ADC_CFG
      000D82 4F               [12]  741 	orl	a,r7
      000D83 F5 F3            [12]  742 	mov	_ADC_CFG,a
                                    743 ;	source/CH549_ADC.c:28: ADC_CFG |= bADC_AIN_EN;                          //开启外部通道
      000D85 43 F3 20         [24]  744 	orl	_ADC_CFG,#0x20
                                    745 ;	source/CH549_ADC.c:29: dat = ADC_DAT;                                   //空读
      000D88 E5 F4            [12]  746 	mov	a,((_ADC_DAT >> 0) & 0xFF)
      000D8A E5 F5            [12]  747 	mov	a,((_ADC_DAT >> 8) & 0xFF)
                                    748 ;	source/CH549_ADC.c:30: ADC_CTRL = bADC_IF;                              //清除ADC转换完成标志，写1清零
      000D8C 75 F2 20         [24]  749 	mov	_ADC_CTRL,#0x20
                                    750 ;	source/CH549_ADC.c:36: }
      000D8F 22               [24]  751 	ret
                                    752 ;------------------------------------------------------------
                                    753 ;Allocation info for local variables in function 'ADC_ChSelect'
                                    754 ;------------------------------------------------------------
                                    755 ;ch                        Allocated to registers r7 
                                    756 ;------------------------------------------------------------
                                    757 ;	source/CH549_ADC.c:45: void ADC_ChSelect( UINT8 ch )
                                    758 ;	-----------------------------------------
                                    759 ;	 function ADC_ChSelect
                                    760 ;	-----------------------------------------
      000D90                        761 _ADC_ChSelect:
      000D90 AF 82            [24]  762 	mov	r7,dpl
                                    763 ;	source/CH549_ADC.c:47: ADC_CHAN = (ADC_CHAN & ~MASK_ADC_CHAN) | ch;
      000D92 74 F0            [12]  764 	mov	a,#0xf0
      000D94 55 F6            [12]  765 	anl	a,_ADC_CHAN
      000D96 4F               [12]  766 	orl	a,r7
      000D97 F5 F6            [12]  767 	mov	_ADC_CHAN,a
                                    768 ;	source/CH549_ADC.c:48: if(ch<=7)                                  //P10~P17引脚配置,不用每次都设置
      000D99 EF               [12]  769 	mov	a,r7
      000D9A 24 F8            [12]  770 	add	a,#0xff - 0x07
      000D9C 40 10            [24]  771 	jc	00102$
                                    772 ;	source/CH549_ADC.c:50: P1_MOD_OC &= ~(ch&0xFF);                       //高阻输入
      000D9E 8F 06            [24]  773 	mov	ar6,r7
      000DA0 EE               [12]  774 	mov	a,r6
      000DA1 F4               [12]  775 	cpl	a
      000DA2 FE               [12]  776 	mov	r6,a
      000DA3 AD 92            [24]  777 	mov	r5,_P1_MOD_OC
      000DA5 5D               [12]  778 	anl	a,r5
      000DA6 F5 92            [12]  779 	mov	_P1_MOD_OC,a
                                    780 ;	source/CH549_ADC.c:51: P1_DIR_PU &= ~(ch&0xFF);
      000DA8 AD 93            [24]  781 	mov	r5,_P1_DIR_PU
      000DAA EE               [12]  782 	mov	a,r6
      000DAB 5D               [12]  783 	anl	a,r5
      000DAC F5 93            [12]  784 	mov	_P1_DIR_PU,a
      000DAE                        785 00102$:
                                    786 ;	source/CH549_ADC.c:53: if(ch>7 && ch<=0x0f)                                  //P00~P07引脚配置，不用每次都设置
      000DAE EF               [12]  787 	mov	a,r7
      000DAF 24 F8            [12]  788 	add	a,#0xff - 0x07
      000DB1 50 15            [24]  789 	jnc	00106$
      000DB3 EF               [12]  790 	mov	a,r7
      000DB4 24 F0            [12]  791 	add	a,#0xff - 0x0f
      000DB6 40 10            [24]  792 	jc	00106$
                                    793 ;	source/CH549_ADC.c:55: P0_MOD_OC &= ~((ch-7)&0xFF);                  //高阻输入
      000DB8 EF               [12]  794 	mov	a,r7
      000DB9 24 F9            [12]  795 	add	a,#0xf9
      000DBB F4               [12]  796 	cpl	a
      000DBC FF               [12]  797 	mov	r7,a
      000DBD AE C4            [24]  798 	mov	r6,_P0_MOD_OC
      000DBF 5E               [12]  799 	anl	a,r6
      000DC0 F5 C4            [12]  800 	mov	_P0_MOD_OC,a
                                    801 ;	source/CH549_ADC.c:56: P0_DIR_PU &= ~((ch-7)&0xFF);
      000DC2 AE C5            [24]  802 	mov	r6,_P0_DIR_PU
      000DC4 EF               [12]  803 	mov	a,r7
      000DC5 5E               [12]  804 	anl	a,r6
      000DC6 F5 C5            [12]  805 	mov	_P0_DIR_PU,a
      000DC8                        806 00106$:
                                    807 ;	source/CH549_ADC.c:58: }
      000DC8 22               [24]  808 	ret
                                    809 ;------------------------------------------------------------
                                    810 ;Allocation info for local variables in function 'ADC_InTSInit'
                                    811 ;------------------------------------------------------------
                                    812 ;	source/CH549_ADC.c:67: void ADC_InTSInit(void)
                                    813 ;	-----------------------------------------
                                    814 ;	 function ADC_InTSInit
                                    815 ;	-----------------------------------------
      000DC9                        816 _ADC_InTSInit:
                                    817 ;	source/CH549_ADC.c:69: ADC_CFG |= bADC_EN;                              //开启ADC模块电源
      000DC9 43 F3 08         [24]  818 	orl	_ADC_CFG,#0x08
                                    819 ;	source/CH549_ADC.c:70: ADC_CFG = ADC_CFG & ~(bADC_CLK0 | bADC_CLK1);    //选择ADC参考时钟 750KHz
      000DCC 53 F3 FC         [24]  820 	anl	_ADC_CFG,#0xfc
                                    821 ;	source/CH549_ADC.c:71: ADC_CFG &= ~bADC_AIN_EN;                         //关闭外部通道
      000DCF 53 F3 DF         [24]  822 	anl	_ADC_CFG,#0xdf
                                    823 ;	source/CH549_ADC.c:72: ADC_CHAN = ADC_CHAN &~MASK_ADC_I_CH | (3<<4);    //内部通道3为温度检测通道
      000DD2 74 CF            [12]  824 	mov	a,#0xcf
      000DD4 55 F6            [12]  825 	anl	a,_ADC_CHAN
      000DD6 44 30            [12]  826 	orl	a,#0x30
      000DD8 F5 F6            [12]  827 	mov	_ADC_CHAN,a
                                    828 ;	source/CH549_ADC.c:73: ADC_CTRL |= bADC_IF;                             //清除ADC转换完成标志，写1清零
      000DDA 43 F2 20         [24]  829 	orl	_ADC_CTRL,#0x20
                                    830 ;	source/CH549_ADC.c:79: }
      000DDD 22               [24]  831 	ret
                                    832 	.area CSEG    (CODE)
                                    833 	.area CONST   (CODE)
                                    834 	.area CABS    (ABS,CODE)
