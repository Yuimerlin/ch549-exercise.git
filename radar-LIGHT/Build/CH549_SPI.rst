                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 4.0.0 #11528 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module CH549_SPI
                                      6 	.optsdcc -mmcs51 --model-large
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _UIF_BUS_RST
                                     12 	.globl _UIF_DETECT
                                     13 	.globl _UIF_TRANSFER
                                     14 	.globl _UIF_SUSPEND
                                     15 	.globl _UIF_HST_SOF
                                     16 	.globl _UIF_FIFO_OV
                                     17 	.globl _U_SIE_FREE
                                     18 	.globl _U_TOG_OK
                                     19 	.globl _U_IS_NAK
                                     20 	.globl _S0_R_FIFO
                                     21 	.globl _S0_T_FIFO
                                     22 	.globl _S0_FREE
                                     23 	.globl _S0_IF_BYTE
                                     24 	.globl _S0_IF_FIRST
                                     25 	.globl _S0_IF_OV
                                     26 	.globl _S0_FST_ACT
                                     27 	.globl _CP_RL2
                                     28 	.globl _C_T2
                                     29 	.globl _TR2
                                     30 	.globl _EXEN2
                                     31 	.globl _TCLK
                                     32 	.globl _RCLK
                                     33 	.globl _EXF2
                                     34 	.globl _CAP1F
                                     35 	.globl _TF2
                                     36 	.globl _RI
                                     37 	.globl _TI
                                     38 	.globl _RB8
                                     39 	.globl _TB8
                                     40 	.globl _REN
                                     41 	.globl _SM2
                                     42 	.globl _SM1
                                     43 	.globl _SM0
                                     44 	.globl _IT0
                                     45 	.globl _IE0
                                     46 	.globl _IT1
                                     47 	.globl _IE1
                                     48 	.globl _TR0
                                     49 	.globl _TF0
                                     50 	.globl _TR1
                                     51 	.globl _TF1
                                     52 	.globl _XI
                                     53 	.globl _XO
                                     54 	.globl _P4_0
                                     55 	.globl _P4_1
                                     56 	.globl _P4_2
                                     57 	.globl _P4_3
                                     58 	.globl _P4_4
                                     59 	.globl _P4_5
                                     60 	.globl _P4_6
                                     61 	.globl _RXD
                                     62 	.globl _TXD
                                     63 	.globl _INT0
                                     64 	.globl _INT1
                                     65 	.globl _T0
                                     66 	.globl _T1
                                     67 	.globl _CAP0
                                     68 	.globl _INT3
                                     69 	.globl _P3_0
                                     70 	.globl _P3_1
                                     71 	.globl _P3_2
                                     72 	.globl _P3_3
                                     73 	.globl _P3_4
                                     74 	.globl _P3_5
                                     75 	.globl _P3_6
                                     76 	.globl _P3_7
                                     77 	.globl _PWM5
                                     78 	.globl _PWM4
                                     79 	.globl _INT0_
                                     80 	.globl _PWM3
                                     81 	.globl _PWM2
                                     82 	.globl _CAP1_
                                     83 	.globl _T2_
                                     84 	.globl _PWM1
                                     85 	.globl _CAP2_
                                     86 	.globl _T2EX_
                                     87 	.globl _PWM0
                                     88 	.globl _RXD1
                                     89 	.globl _PWM6
                                     90 	.globl _TXD1
                                     91 	.globl _PWM7
                                     92 	.globl _P2_0
                                     93 	.globl _P2_1
                                     94 	.globl _P2_2
                                     95 	.globl _P2_3
                                     96 	.globl _P2_4
                                     97 	.globl _P2_5
                                     98 	.globl _P2_6
                                     99 	.globl _P2_7
                                    100 	.globl _AIN0
                                    101 	.globl _CAP1
                                    102 	.globl _T2
                                    103 	.globl _AIN1
                                    104 	.globl _CAP2
                                    105 	.globl _T2EX
                                    106 	.globl _AIN2
                                    107 	.globl _AIN3
                                    108 	.globl _AIN4
                                    109 	.globl _UCC1
                                    110 	.globl _SCS
                                    111 	.globl _AIN5
                                    112 	.globl _UCC2
                                    113 	.globl _PWM0_
                                    114 	.globl _MOSI
                                    115 	.globl _AIN6
                                    116 	.globl _VBUS
                                    117 	.globl _RXD1_
                                    118 	.globl _MISO
                                    119 	.globl _AIN7
                                    120 	.globl _TXD1_
                                    121 	.globl _SCK
                                    122 	.globl _P1_0
                                    123 	.globl _P1_1
                                    124 	.globl _P1_2
                                    125 	.globl _P1_3
                                    126 	.globl _P1_4
                                    127 	.globl _P1_5
                                    128 	.globl _P1_6
                                    129 	.globl _P1_7
                                    130 	.globl _AIN8
                                    131 	.globl _AIN9
                                    132 	.globl _AIN10
                                    133 	.globl _RXD_
                                    134 	.globl _AIN11
                                    135 	.globl _TXD_
                                    136 	.globl _AIN12
                                    137 	.globl _RXD2
                                    138 	.globl _AIN13
                                    139 	.globl _TXD2
                                    140 	.globl _AIN14
                                    141 	.globl _RXD3
                                    142 	.globl _AIN15
                                    143 	.globl _TXD3
                                    144 	.globl _P0_0
                                    145 	.globl _P0_1
                                    146 	.globl _P0_2
                                    147 	.globl _P0_3
                                    148 	.globl _P0_4
                                    149 	.globl _P0_5
                                    150 	.globl _P0_6
                                    151 	.globl _P0_7
                                    152 	.globl _IE_SPI0
                                    153 	.globl _IE_INT3
                                    154 	.globl _IE_USB
                                    155 	.globl _IE_UART2
                                    156 	.globl _IE_ADC
                                    157 	.globl _IE_UART1
                                    158 	.globl _IE_UART3
                                    159 	.globl _IE_PWMX
                                    160 	.globl _IE_GPIO
                                    161 	.globl _IE_WDOG
                                    162 	.globl _PX0
                                    163 	.globl _PT0
                                    164 	.globl _PX1
                                    165 	.globl _PT1
                                    166 	.globl _PS
                                    167 	.globl _PT2
                                    168 	.globl _PL_FLAG
                                    169 	.globl _PH_FLAG
                                    170 	.globl _EX0
                                    171 	.globl _ET0
                                    172 	.globl _EX1
                                    173 	.globl _ET1
                                    174 	.globl _ES
                                    175 	.globl _ET2
                                    176 	.globl _E_DIS
                                    177 	.globl _EA
                                    178 	.globl _P
                                    179 	.globl _F1
                                    180 	.globl _OV
                                    181 	.globl _RS0
                                    182 	.globl _RS1
                                    183 	.globl _F0
                                    184 	.globl _AC
                                    185 	.globl _CY
                                    186 	.globl _UEP1_DMA_H
                                    187 	.globl _UEP1_DMA_L
                                    188 	.globl _UEP1_DMA
                                    189 	.globl _UEP0_DMA_H
                                    190 	.globl _UEP0_DMA_L
                                    191 	.globl _UEP0_DMA
                                    192 	.globl _UEP2_3_MOD
                                    193 	.globl _UEP4_1_MOD
                                    194 	.globl _UEP3_DMA_H
                                    195 	.globl _UEP3_DMA_L
                                    196 	.globl _UEP3_DMA
                                    197 	.globl _UEP2_DMA_H
                                    198 	.globl _UEP2_DMA_L
                                    199 	.globl _UEP2_DMA
                                    200 	.globl _USB_DEV_AD
                                    201 	.globl _USB_CTRL
                                    202 	.globl _USB_INT_EN
                                    203 	.globl _UEP4_T_LEN
                                    204 	.globl _UEP4_CTRL
                                    205 	.globl _UEP0_T_LEN
                                    206 	.globl _UEP0_CTRL
                                    207 	.globl _USB_RX_LEN
                                    208 	.globl _USB_MIS_ST
                                    209 	.globl _USB_INT_ST
                                    210 	.globl _USB_INT_FG
                                    211 	.globl _UEP3_T_LEN
                                    212 	.globl _UEP3_CTRL
                                    213 	.globl _UEP2_T_LEN
                                    214 	.globl _UEP2_CTRL
                                    215 	.globl _UEP1_T_LEN
                                    216 	.globl _UEP1_CTRL
                                    217 	.globl _UDEV_CTRL
                                    218 	.globl _USB_C_CTRL
                                    219 	.globl _ADC_PIN
                                    220 	.globl _ADC_CHAN
                                    221 	.globl _ADC_DAT_H
                                    222 	.globl _ADC_DAT_L
                                    223 	.globl _ADC_DAT
                                    224 	.globl _ADC_CFG
                                    225 	.globl _ADC_CTRL
                                    226 	.globl _TKEY_CTRL
                                    227 	.globl _SIF3
                                    228 	.globl _SBAUD3
                                    229 	.globl _SBUF3
                                    230 	.globl _SCON3
                                    231 	.globl _SIF2
                                    232 	.globl _SBAUD2
                                    233 	.globl _SBUF2
                                    234 	.globl _SCON2
                                    235 	.globl _SIF1
                                    236 	.globl _SBAUD1
                                    237 	.globl _SBUF1
                                    238 	.globl _SCON1
                                    239 	.globl _SPI0_SETUP
                                    240 	.globl _SPI0_CK_SE
                                    241 	.globl _SPI0_CTRL
                                    242 	.globl _SPI0_DATA
                                    243 	.globl _SPI0_STAT
                                    244 	.globl _PWM_DATA7
                                    245 	.globl _PWM_DATA6
                                    246 	.globl _PWM_DATA5
                                    247 	.globl _PWM_DATA4
                                    248 	.globl _PWM_DATA3
                                    249 	.globl _PWM_CTRL2
                                    250 	.globl _PWM_CK_SE
                                    251 	.globl _PWM_CTRL
                                    252 	.globl _PWM_DATA0
                                    253 	.globl _PWM_DATA1
                                    254 	.globl _PWM_DATA2
                                    255 	.globl _T2CAP1H
                                    256 	.globl _T2CAP1L
                                    257 	.globl _T2CAP1
                                    258 	.globl _TH2
                                    259 	.globl _TL2
                                    260 	.globl _T2COUNT
                                    261 	.globl _RCAP2H
                                    262 	.globl _RCAP2L
                                    263 	.globl _RCAP2
                                    264 	.globl _T2MOD
                                    265 	.globl _T2CON
                                    266 	.globl _T2CAP0H
                                    267 	.globl _T2CAP0L
                                    268 	.globl _T2CAP0
                                    269 	.globl _T2CON2
                                    270 	.globl _SBUF
                                    271 	.globl _SCON
                                    272 	.globl _TH1
                                    273 	.globl _TH0
                                    274 	.globl _TL1
                                    275 	.globl _TL0
                                    276 	.globl _TMOD
                                    277 	.globl _TCON
                                    278 	.globl _XBUS_AUX
                                    279 	.globl _PIN_FUNC
                                    280 	.globl _P5
                                    281 	.globl _P4_DIR_PU
                                    282 	.globl _P4_MOD_OC
                                    283 	.globl _P4
                                    284 	.globl _P3_DIR_PU
                                    285 	.globl _P3_MOD_OC
                                    286 	.globl _P3
                                    287 	.globl _P2_DIR_PU
                                    288 	.globl _P2_MOD_OC
                                    289 	.globl _P2
                                    290 	.globl _P1_DIR_PU
                                    291 	.globl _P1_MOD_OC
                                    292 	.globl _P1
                                    293 	.globl _P0_DIR_PU
                                    294 	.globl _P0_MOD_OC
                                    295 	.globl _P0
                                    296 	.globl _ROM_CTRL
                                    297 	.globl _ROM_DATA_HH
                                    298 	.globl _ROM_DATA_HL
                                    299 	.globl _ROM_DATA_HI
                                    300 	.globl _ROM_ADDR_H
                                    301 	.globl _ROM_ADDR_L
                                    302 	.globl _ROM_ADDR
                                    303 	.globl _GPIO_IE
                                    304 	.globl _INTX
                                    305 	.globl _IP_EX
                                    306 	.globl _IE_EX
                                    307 	.globl _IP
                                    308 	.globl _IE
                                    309 	.globl _WDOG_COUNT
                                    310 	.globl _RESET_KEEP
                                    311 	.globl _WAKE_CTRL
                                    312 	.globl _CLOCK_CFG
                                    313 	.globl _POWER_CFG
                                    314 	.globl _PCON
                                    315 	.globl _GLOBAL_CFG
                                    316 	.globl _SAFE_MOD
                                    317 	.globl _DPH
                                    318 	.globl _DPL
                                    319 	.globl _SP
                                    320 	.globl _A_INV
                                    321 	.globl _B
                                    322 	.globl _ACC
                                    323 	.globl _PSW
                                    324 	.globl _SPIMasterModeSet
                                    325 	.globl _CH549SPIMasterWrite
                                    326 	.globl _CH549SPIMasterRead
                                    327 	.globl _SPISlvModeSet
                                    328 	.globl _CH549SPISlvWrite
                                    329 	.globl _CH549SPISlvRead
                                    330 ;--------------------------------------------------------
                                    331 ; special function registers
                                    332 ;--------------------------------------------------------
                                    333 	.area RSEG    (ABS,DATA)
      000000                        334 	.org 0x0000
                           0000D0   335 _PSW	=	0x00d0
                           0000E0   336 _ACC	=	0x00e0
                           0000F0   337 _B	=	0x00f0
                           0000FD   338 _A_INV	=	0x00fd
                           000081   339 _SP	=	0x0081
                           000082   340 _DPL	=	0x0082
                           000083   341 _DPH	=	0x0083
                           0000A1   342 _SAFE_MOD	=	0x00a1
                           0000B1   343 _GLOBAL_CFG	=	0x00b1
                           000087   344 _PCON	=	0x0087
                           0000BA   345 _POWER_CFG	=	0x00ba
                           0000B9   346 _CLOCK_CFG	=	0x00b9
                           0000A9   347 _WAKE_CTRL	=	0x00a9
                           0000FE   348 _RESET_KEEP	=	0x00fe
                           0000FF   349 _WDOG_COUNT	=	0x00ff
                           0000A8   350 _IE	=	0x00a8
                           0000B8   351 _IP	=	0x00b8
                           0000E8   352 _IE_EX	=	0x00e8
                           0000E9   353 _IP_EX	=	0x00e9
                           0000B3   354 _INTX	=	0x00b3
                           0000B2   355 _GPIO_IE	=	0x00b2
                           008584   356 _ROM_ADDR	=	0x8584
                           000084   357 _ROM_ADDR_L	=	0x0084
                           000085   358 _ROM_ADDR_H	=	0x0085
                           008F8E   359 _ROM_DATA_HI	=	0x8f8e
                           00008E   360 _ROM_DATA_HL	=	0x008e
                           00008F   361 _ROM_DATA_HH	=	0x008f
                           000086   362 _ROM_CTRL	=	0x0086
                           000080   363 _P0	=	0x0080
                           0000C4   364 _P0_MOD_OC	=	0x00c4
                           0000C5   365 _P0_DIR_PU	=	0x00c5
                           000090   366 _P1	=	0x0090
                           000092   367 _P1_MOD_OC	=	0x0092
                           000093   368 _P1_DIR_PU	=	0x0093
                           0000A0   369 _P2	=	0x00a0
                           000094   370 _P2_MOD_OC	=	0x0094
                           000095   371 _P2_DIR_PU	=	0x0095
                           0000B0   372 _P3	=	0x00b0
                           000096   373 _P3_MOD_OC	=	0x0096
                           000097   374 _P3_DIR_PU	=	0x0097
                           0000C0   375 _P4	=	0x00c0
                           0000C2   376 _P4_MOD_OC	=	0x00c2
                           0000C3   377 _P4_DIR_PU	=	0x00c3
                           0000AB   378 _P5	=	0x00ab
                           0000AA   379 _PIN_FUNC	=	0x00aa
                           0000A2   380 _XBUS_AUX	=	0x00a2
                           000088   381 _TCON	=	0x0088
                           000089   382 _TMOD	=	0x0089
                           00008A   383 _TL0	=	0x008a
                           00008B   384 _TL1	=	0x008b
                           00008C   385 _TH0	=	0x008c
                           00008D   386 _TH1	=	0x008d
                           000098   387 _SCON	=	0x0098
                           000099   388 _SBUF	=	0x0099
                           0000C1   389 _T2CON2	=	0x00c1
                           00C7C6   390 _T2CAP0	=	0xc7c6
                           0000C6   391 _T2CAP0L	=	0x00c6
                           0000C7   392 _T2CAP0H	=	0x00c7
                           0000C8   393 _T2CON	=	0x00c8
                           0000C9   394 _T2MOD	=	0x00c9
                           00CBCA   395 _RCAP2	=	0xcbca
                           0000CA   396 _RCAP2L	=	0x00ca
                           0000CB   397 _RCAP2H	=	0x00cb
                           00CDCC   398 _T2COUNT	=	0xcdcc
                           0000CC   399 _TL2	=	0x00cc
                           0000CD   400 _TH2	=	0x00cd
                           00CFCE   401 _T2CAP1	=	0xcfce
                           0000CE   402 _T2CAP1L	=	0x00ce
                           0000CF   403 _T2CAP1H	=	0x00cf
                           00009A   404 _PWM_DATA2	=	0x009a
                           00009B   405 _PWM_DATA1	=	0x009b
                           00009C   406 _PWM_DATA0	=	0x009c
                           00009D   407 _PWM_CTRL	=	0x009d
                           00009E   408 _PWM_CK_SE	=	0x009e
                           00009F   409 _PWM_CTRL2	=	0x009f
                           0000A3   410 _PWM_DATA3	=	0x00a3
                           0000A4   411 _PWM_DATA4	=	0x00a4
                           0000A5   412 _PWM_DATA5	=	0x00a5
                           0000A6   413 _PWM_DATA6	=	0x00a6
                           0000A7   414 _PWM_DATA7	=	0x00a7
                           0000F8   415 _SPI0_STAT	=	0x00f8
                           0000F9   416 _SPI0_DATA	=	0x00f9
                           0000FA   417 _SPI0_CTRL	=	0x00fa
                           0000FB   418 _SPI0_CK_SE	=	0x00fb
                           0000FC   419 _SPI0_SETUP	=	0x00fc
                           0000BC   420 _SCON1	=	0x00bc
                           0000BD   421 _SBUF1	=	0x00bd
                           0000BE   422 _SBAUD1	=	0x00be
                           0000BF   423 _SIF1	=	0x00bf
                           0000B4   424 _SCON2	=	0x00b4
                           0000B5   425 _SBUF2	=	0x00b5
                           0000B6   426 _SBAUD2	=	0x00b6
                           0000B7   427 _SIF2	=	0x00b7
                           0000AC   428 _SCON3	=	0x00ac
                           0000AD   429 _SBUF3	=	0x00ad
                           0000AE   430 _SBAUD3	=	0x00ae
                           0000AF   431 _SIF3	=	0x00af
                           0000F1   432 _TKEY_CTRL	=	0x00f1
                           0000F2   433 _ADC_CTRL	=	0x00f2
                           0000F3   434 _ADC_CFG	=	0x00f3
                           00F5F4   435 _ADC_DAT	=	0xf5f4
                           0000F4   436 _ADC_DAT_L	=	0x00f4
                           0000F5   437 _ADC_DAT_H	=	0x00f5
                           0000F6   438 _ADC_CHAN	=	0x00f6
                           0000F7   439 _ADC_PIN	=	0x00f7
                           000091   440 _USB_C_CTRL	=	0x0091
                           0000D1   441 _UDEV_CTRL	=	0x00d1
                           0000D2   442 _UEP1_CTRL	=	0x00d2
                           0000D3   443 _UEP1_T_LEN	=	0x00d3
                           0000D4   444 _UEP2_CTRL	=	0x00d4
                           0000D5   445 _UEP2_T_LEN	=	0x00d5
                           0000D6   446 _UEP3_CTRL	=	0x00d6
                           0000D7   447 _UEP3_T_LEN	=	0x00d7
                           0000D8   448 _USB_INT_FG	=	0x00d8
                           0000D9   449 _USB_INT_ST	=	0x00d9
                           0000DA   450 _USB_MIS_ST	=	0x00da
                           0000DB   451 _USB_RX_LEN	=	0x00db
                           0000DC   452 _UEP0_CTRL	=	0x00dc
                           0000DD   453 _UEP0_T_LEN	=	0x00dd
                           0000DE   454 _UEP4_CTRL	=	0x00de
                           0000DF   455 _UEP4_T_LEN	=	0x00df
                           0000E1   456 _USB_INT_EN	=	0x00e1
                           0000E2   457 _USB_CTRL	=	0x00e2
                           0000E3   458 _USB_DEV_AD	=	0x00e3
                           00E5E4   459 _UEP2_DMA	=	0xe5e4
                           0000E4   460 _UEP2_DMA_L	=	0x00e4
                           0000E5   461 _UEP2_DMA_H	=	0x00e5
                           00E7E6   462 _UEP3_DMA	=	0xe7e6
                           0000E6   463 _UEP3_DMA_L	=	0x00e6
                           0000E7   464 _UEP3_DMA_H	=	0x00e7
                           0000EA   465 _UEP4_1_MOD	=	0x00ea
                           0000EB   466 _UEP2_3_MOD	=	0x00eb
                           00EDEC   467 _UEP0_DMA	=	0xedec
                           0000EC   468 _UEP0_DMA_L	=	0x00ec
                           0000ED   469 _UEP0_DMA_H	=	0x00ed
                           00EFEE   470 _UEP1_DMA	=	0xefee
                           0000EE   471 _UEP1_DMA_L	=	0x00ee
                           0000EF   472 _UEP1_DMA_H	=	0x00ef
                                    473 ;--------------------------------------------------------
                                    474 ; special function bits
                                    475 ;--------------------------------------------------------
                                    476 	.area RSEG    (ABS,DATA)
      000000                        477 	.org 0x0000
                           0000D7   478 _CY	=	0x00d7
                           0000D6   479 _AC	=	0x00d6
                           0000D5   480 _F0	=	0x00d5
                           0000D4   481 _RS1	=	0x00d4
                           0000D3   482 _RS0	=	0x00d3
                           0000D2   483 _OV	=	0x00d2
                           0000D1   484 _F1	=	0x00d1
                           0000D0   485 _P	=	0x00d0
                           0000AF   486 _EA	=	0x00af
                           0000AE   487 _E_DIS	=	0x00ae
                           0000AD   488 _ET2	=	0x00ad
                           0000AC   489 _ES	=	0x00ac
                           0000AB   490 _ET1	=	0x00ab
                           0000AA   491 _EX1	=	0x00aa
                           0000A9   492 _ET0	=	0x00a9
                           0000A8   493 _EX0	=	0x00a8
                           0000BF   494 _PH_FLAG	=	0x00bf
                           0000BE   495 _PL_FLAG	=	0x00be
                           0000BD   496 _PT2	=	0x00bd
                           0000BC   497 _PS	=	0x00bc
                           0000BB   498 _PT1	=	0x00bb
                           0000BA   499 _PX1	=	0x00ba
                           0000B9   500 _PT0	=	0x00b9
                           0000B8   501 _PX0	=	0x00b8
                           0000EF   502 _IE_WDOG	=	0x00ef
                           0000EE   503 _IE_GPIO	=	0x00ee
                           0000ED   504 _IE_PWMX	=	0x00ed
                           0000ED   505 _IE_UART3	=	0x00ed
                           0000EC   506 _IE_UART1	=	0x00ec
                           0000EB   507 _IE_ADC	=	0x00eb
                           0000EB   508 _IE_UART2	=	0x00eb
                           0000EA   509 _IE_USB	=	0x00ea
                           0000E9   510 _IE_INT3	=	0x00e9
                           0000E8   511 _IE_SPI0	=	0x00e8
                           000087   512 _P0_7	=	0x0087
                           000086   513 _P0_6	=	0x0086
                           000085   514 _P0_5	=	0x0085
                           000084   515 _P0_4	=	0x0084
                           000083   516 _P0_3	=	0x0083
                           000082   517 _P0_2	=	0x0082
                           000081   518 _P0_1	=	0x0081
                           000080   519 _P0_0	=	0x0080
                           000087   520 _TXD3	=	0x0087
                           000087   521 _AIN15	=	0x0087
                           000086   522 _RXD3	=	0x0086
                           000086   523 _AIN14	=	0x0086
                           000085   524 _TXD2	=	0x0085
                           000085   525 _AIN13	=	0x0085
                           000084   526 _RXD2	=	0x0084
                           000084   527 _AIN12	=	0x0084
                           000083   528 _TXD_	=	0x0083
                           000083   529 _AIN11	=	0x0083
                           000082   530 _RXD_	=	0x0082
                           000082   531 _AIN10	=	0x0082
                           000081   532 _AIN9	=	0x0081
                           000080   533 _AIN8	=	0x0080
                           000097   534 _P1_7	=	0x0097
                           000096   535 _P1_6	=	0x0096
                           000095   536 _P1_5	=	0x0095
                           000094   537 _P1_4	=	0x0094
                           000093   538 _P1_3	=	0x0093
                           000092   539 _P1_2	=	0x0092
                           000091   540 _P1_1	=	0x0091
                           000090   541 _P1_0	=	0x0090
                           000097   542 _SCK	=	0x0097
                           000097   543 _TXD1_	=	0x0097
                           000097   544 _AIN7	=	0x0097
                           000096   545 _MISO	=	0x0096
                           000096   546 _RXD1_	=	0x0096
                           000096   547 _VBUS	=	0x0096
                           000096   548 _AIN6	=	0x0096
                           000095   549 _MOSI	=	0x0095
                           000095   550 _PWM0_	=	0x0095
                           000095   551 _UCC2	=	0x0095
                           000095   552 _AIN5	=	0x0095
                           000094   553 _SCS	=	0x0094
                           000094   554 _UCC1	=	0x0094
                           000094   555 _AIN4	=	0x0094
                           000093   556 _AIN3	=	0x0093
                           000092   557 _AIN2	=	0x0092
                           000091   558 _T2EX	=	0x0091
                           000091   559 _CAP2	=	0x0091
                           000091   560 _AIN1	=	0x0091
                           000090   561 _T2	=	0x0090
                           000090   562 _CAP1	=	0x0090
                           000090   563 _AIN0	=	0x0090
                           0000A7   564 _P2_7	=	0x00a7
                           0000A6   565 _P2_6	=	0x00a6
                           0000A5   566 _P2_5	=	0x00a5
                           0000A4   567 _P2_4	=	0x00a4
                           0000A3   568 _P2_3	=	0x00a3
                           0000A2   569 _P2_2	=	0x00a2
                           0000A1   570 _P2_1	=	0x00a1
                           0000A0   571 _P2_0	=	0x00a0
                           0000A7   572 _PWM7	=	0x00a7
                           0000A7   573 _TXD1	=	0x00a7
                           0000A6   574 _PWM6	=	0x00a6
                           0000A6   575 _RXD1	=	0x00a6
                           0000A5   576 _PWM0	=	0x00a5
                           0000A5   577 _T2EX_	=	0x00a5
                           0000A5   578 _CAP2_	=	0x00a5
                           0000A4   579 _PWM1	=	0x00a4
                           0000A4   580 _T2_	=	0x00a4
                           0000A4   581 _CAP1_	=	0x00a4
                           0000A3   582 _PWM2	=	0x00a3
                           0000A2   583 _PWM3	=	0x00a2
                           0000A2   584 _INT0_	=	0x00a2
                           0000A1   585 _PWM4	=	0x00a1
                           0000A0   586 _PWM5	=	0x00a0
                           0000B7   587 _P3_7	=	0x00b7
                           0000B6   588 _P3_6	=	0x00b6
                           0000B5   589 _P3_5	=	0x00b5
                           0000B4   590 _P3_4	=	0x00b4
                           0000B3   591 _P3_3	=	0x00b3
                           0000B2   592 _P3_2	=	0x00b2
                           0000B1   593 _P3_1	=	0x00b1
                           0000B0   594 _P3_0	=	0x00b0
                           0000B7   595 _INT3	=	0x00b7
                           0000B6   596 _CAP0	=	0x00b6
                           0000B5   597 _T1	=	0x00b5
                           0000B4   598 _T0	=	0x00b4
                           0000B3   599 _INT1	=	0x00b3
                           0000B2   600 _INT0	=	0x00b2
                           0000B1   601 _TXD	=	0x00b1
                           0000B0   602 _RXD	=	0x00b0
                           0000C6   603 _P4_6	=	0x00c6
                           0000C5   604 _P4_5	=	0x00c5
                           0000C4   605 _P4_4	=	0x00c4
                           0000C3   606 _P4_3	=	0x00c3
                           0000C2   607 _P4_2	=	0x00c2
                           0000C1   608 _P4_1	=	0x00c1
                           0000C0   609 _P4_0	=	0x00c0
                           0000C7   610 _XO	=	0x00c7
                           0000C6   611 _XI	=	0x00c6
                           00008F   612 _TF1	=	0x008f
                           00008E   613 _TR1	=	0x008e
                           00008D   614 _TF0	=	0x008d
                           00008C   615 _TR0	=	0x008c
                           00008B   616 _IE1	=	0x008b
                           00008A   617 _IT1	=	0x008a
                           000089   618 _IE0	=	0x0089
                           000088   619 _IT0	=	0x0088
                           00009F   620 _SM0	=	0x009f
                           00009E   621 _SM1	=	0x009e
                           00009D   622 _SM2	=	0x009d
                           00009C   623 _REN	=	0x009c
                           00009B   624 _TB8	=	0x009b
                           00009A   625 _RB8	=	0x009a
                           000099   626 _TI	=	0x0099
                           000098   627 _RI	=	0x0098
                           0000CF   628 _TF2	=	0x00cf
                           0000CF   629 _CAP1F	=	0x00cf
                           0000CE   630 _EXF2	=	0x00ce
                           0000CD   631 _RCLK	=	0x00cd
                           0000CC   632 _TCLK	=	0x00cc
                           0000CB   633 _EXEN2	=	0x00cb
                           0000CA   634 _TR2	=	0x00ca
                           0000C9   635 _C_T2	=	0x00c9
                           0000C8   636 _CP_RL2	=	0x00c8
                           0000FF   637 _S0_FST_ACT	=	0x00ff
                           0000FE   638 _S0_IF_OV	=	0x00fe
                           0000FD   639 _S0_IF_FIRST	=	0x00fd
                           0000FC   640 _S0_IF_BYTE	=	0x00fc
                           0000FB   641 _S0_FREE	=	0x00fb
                           0000FA   642 _S0_T_FIFO	=	0x00fa
                           0000F8   643 _S0_R_FIFO	=	0x00f8
                           0000DF   644 _U_IS_NAK	=	0x00df
                           0000DE   645 _U_TOG_OK	=	0x00de
                           0000DD   646 _U_SIE_FREE	=	0x00dd
                           0000DC   647 _UIF_FIFO_OV	=	0x00dc
                           0000DB   648 _UIF_HST_SOF	=	0x00db
                           0000DA   649 _UIF_SUSPEND	=	0x00da
                           0000D9   650 _UIF_TRANSFER	=	0x00d9
                           0000D8   651 _UIF_DETECT	=	0x00d8
                           0000D8   652 _UIF_BUS_RST	=	0x00d8
                                    653 ;--------------------------------------------------------
                                    654 ; overlayable register banks
                                    655 ;--------------------------------------------------------
                                    656 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        657 	.ds 8
                                    658 ;--------------------------------------------------------
                                    659 ; internal ram data
                                    660 ;--------------------------------------------------------
                                    661 	.area DSEG    (DATA)
                                    662 ;--------------------------------------------------------
                                    663 ; overlayable items in internal ram 
                                    664 ;--------------------------------------------------------
                                    665 ;--------------------------------------------------------
                                    666 ; indirectly addressable internal ram data
                                    667 ;--------------------------------------------------------
                                    668 	.area ISEG    (DATA)
                                    669 ;--------------------------------------------------------
                                    670 ; absolute internal ram data
                                    671 ;--------------------------------------------------------
                                    672 	.area IABS    (ABS,DATA)
                                    673 	.area IABS    (ABS,DATA)
                                    674 ;--------------------------------------------------------
                                    675 ; bit data
                                    676 ;--------------------------------------------------------
                                    677 	.area BSEG    (BIT)
                                    678 ;--------------------------------------------------------
                                    679 ; paged external ram data
                                    680 ;--------------------------------------------------------
                                    681 	.area PSEG    (PAG,XDATA)
                                    682 ;--------------------------------------------------------
                                    683 ; external ram data
                                    684 ;--------------------------------------------------------
                                    685 	.area XSEG    (XDATA)
                                    686 ;--------------------------------------------------------
                                    687 ; absolute external ram data
                                    688 ;--------------------------------------------------------
                                    689 	.area XABS    (ABS,XDATA)
                                    690 ;--------------------------------------------------------
                                    691 ; external initialized ram data
                                    692 ;--------------------------------------------------------
                                    693 	.area HOME    (CODE)
                                    694 	.area GSINIT0 (CODE)
                                    695 	.area GSINIT1 (CODE)
                                    696 	.area GSINIT2 (CODE)
                                    697 	.area GSINIT3 (CODE)
                                    698 	.area GSINIT4 (CODE)
                                    699 	.area GSINIT5 (CODE)
                                    700 	.area GSINIT  (CODE)
                                    701 	.area GSFINAL (CODE)
                                    702 	.area CSEG    (CODE)
                                    703 ;--------------------------------------------------------
                                    704 ; global & static initialisations
                                    705 ;--------------------------------------------------------
                                    706 	.area HOME    (CODE)
                                    707 	.area GSINIT  (CODE)
                                    708 	.area GSFINAL (CODE)
                                    709 	.area GSINIT  (CODE)
                                    710 ;--------------------------------------------------------
                                    711 ; Home
                                    712 ;--------------------------------------------------------
                                    713 	.area HOME    (CODE)
                                    714 	.area HOME    (CODE)
                                    715 ;--------------------------------------------------------
                                    716 ; code
                                    717 ;--------------------------------------------------------
                                    718 	.area CSEG    (CODE)
                                    719 ;------------------------------------------------------------
                                    720 ;Allocation info for local variables in function 'SPIMasterModeSet'
                                    721 ;------------------------------------------------------------
                                    722 ;mode                      Allocated to registers r7 
                                    723 ;------------------------------------------------------------
                                    724 ;	source/CH549_SPI.c:22: void SPIMasterModeSet(UINT8 mode)
                                    725 ;	-----------------------------------------
                                    726 ;	 function SPIMasterModeSet
                                    727 ;	-----------------------------------------
      000F65                        728 _SPIMasterModeSet:
                           000007   729 	ar7 = 0x07
                           000006   730 	ar6 = 0x06
                           000005   731 	ar5 = 0x05
                           000004   732 	ar4 = 0x04
                           000003   733 	ar3 = 0x03
                           000002   734 	ar2 = 0x02
                           000001   735 	ar1 = 0x01
                           000000   736 	ar0 = 0x00
      000F65 AF 82            [24]  737 	mov	r7,dpl
                                    738 ;	source/CH549_SPI.c:24: SCS = 1;
                                    739 ;	assignBit
      000F67 D2 94            [12]  740 	setb	_SCS
                                    741 ;	source/CH549_SPI.c:25: P1_MOD_OC &= ~(bSCS|bMOSI|bSCK);
      000F69 53 92 4F         [24]  742 	anl	_P1_MOD_OC,#0x4f
                                    743 ;	source/CH549_SPI.c:26: P1_DIR_PU |= (bSCS|bMOSI|bSCK);                                            //SCS,MOSI,SCK设推挽输出
      000F6C 43 93 B0         [24]  744 	orl	_P1_DIR_PU,#0xb0
                                    745 ;	source/CH549_SPI.c:27: P1_MOD_OC |= bMISO;                                                        //MISO 上拉输入
      000F6F 43 92 40         [24]  746 	orl	_P1_MOD_OC,#0x40
                                    747 ;	source/CH549_SPI.c:28: P1_DIR_PU |= bMISO;
      000F72 43 93 40         [24]  748 	orl	_P1_DIR_PU,#0x40
                                    749 ;	source/CH549_SPI.c:29: SPI0_SETUP = 0;                                                           //Master模式,高位在前
      000F75 75 FC 00         [24]  750 	mov	_SPI0_SETUP,#0x00
                                    751 ;	source/CH549_SPI.c:30: if(mode == 0)
      000F78 EF               [12]  752 	mov	a,r7
      000F79 70 04            [24]  753 	jnz	00104$
                                    754 ;	source/CH549_SPI.c:32: SPI0_CTRL = (bS0_MOSI_OE|bS0_SCK_OE);                                   //模式0
      000F7B 75 FA 60         [24]  755 	mov	_SPI0_CTRL,#0x60
      000F7E 22               [24]  756 	ret
      000F7F                        757 00104$:
                                    758 ;	source/CH549_SPI.c:34: else if(mode == 3)
      000F7F BF 03 03         [24]  759 	cjne	r7,#0x03,00106$
                                    760 ;	source/CH549_SPI.c:36: SPI0_CTRL = (bS0_MOSI_OE|bS0_SCK_OE|bS0_MST_CLK);                       //模式3
      000F82 75 FA 68         [24]  761 	mov	_SPI0_CTRL,#0x68
      000F85                        762 00106$:
                                    763 ;	source/CH549_SPI.c:38: }
      000F85 22               [24]  764 	ret
                                    765 ;------------------------------------------------------------
                                    766 ;Allocation info for local variables in function 'CH549SPIMasterWrite'
                                    767 ;------------------------------------------------------------
                                    768 ;dat                       Allocated to registers 
                                    769 ;------------------------------------------------------------
                                    770 ;	source/CH549_SPI.c:46: void CH549SPIMasterWrite(UINT8 dat)
                                    771 ;	-----------------------------------------
                                    772 ;	 function CH549SPIMasterWrite
                                    773 ;	-----------------------------------------
      000F86                        774 _CH549SPIMasterWrite:
      000F86 85 82 F9         [24]  775 	mov	_SPI0_DATA,dpl
                                    776 ;	source/CH549_SPI.c:49: while(S0_FREE == 0)
      000F89                        777 00101$:
      000F89 30 FB FD         [24]  778 	jnb	_S0_FREE,00101$
                                    779 ;	source/CH549_SPI.c:53: }
      000F8C 22               [24]  780 	ret
                                    781 ;------------------------------------------------------------
                                    782 ;Allocation info for local variables in function 'CH549SPIMasterRead'
                                    783 ;------------------------------------------------------------
                                    784 ;	source/CH549_SPI.c:61: UINT8 CH549SPIMasterRead()
                                    785 ;	-----------------------------------------
                                    786 ;	 function CH549SPIMasterRead
                                    787 ;	-----------------------------------------
      000F8D                        788 _CH549SPIMasterRead:
                                    789 ;	source/CH549_SPI.c:63: SPI0_DATA = 0xff;
      000F8D 75 F9 FF         [24]  790 	mov	_SPI0_DATA,#0xff
                                    791 ;	source/CH549_SPI.c:64: while(S0_FREE == 0)
      000F90                        792 00101$:
      000F90 30 FB FD         [24]  793 	jnb	_S0_FREE,00101$
                                    794 ;	source/CH549_SPI.c:68: return SPI0_DATA;
      000F93 85 F9 82         [24]  795 	mov	dpl,_SPI0_DATA
                                    796 ;	source/CH549_SPI.c:69: }
      000F96 22               [24]  797 	ret
                                    798 ;------------------------------------------------------------
                                    799 ;Allocation info for local variables in function 'SPISlvModeSet'
                                    800 ;------------------------------------------------------------
                                    801 ;	source/CH549_SPI.c:77: void SPISlvModeSet( )
                                    802 ;	-----------------------------------------
                                    803 ;	 function SPISlvModeSet
                                    804 ;	-----------------------------------------
      000F97                        805 _SPISlvModeSet:
                                    806 ;	source/CH549_SPI.c:79: P1_MOD_OC &= ~(bSCS|bMOSI|bSCK);                                          //SCS,MOSI,SCK 浮空输入
      000F97 53 92 4F         [24]  807 	anl	_P1_MOD_OC,#0x4f
                                    808 ;	source/CH549_SPI.c:80: P1_DIR_PU &= ~(bSCS|bMOSI|bSCK);
      000F9A 53 93 4F         [24]  809 	anl	_P1_DIR_PU,#0x4f
                                    810 ;	source/CH549_SPI.c:81: P1_MOD_OC &= ~bMISO;                                                      //MISO推挽输出
      000F9D 53 92 BF         [24]  811 	anl	_P1_MOD_OC,#0xbf
                                    812 ;	source/CH549_SPI.c:82: P1_DIR_PU |= bMISO;
      000FA0 43 93 40         [24]  813 	orl	_P1_DIR_PU,#0x40
                                    814 ;	source/CH549_SPI.c:83: SPI0_S_PRE = 0x66;                                                        //预置值,任意值
      000FA3 75 FB 66         [24]  815 	mov	_SPI0_CK_SE,#0x66
                                    816 ;	source/CH549_SPI.c:84: SPI0_SETUP = bS0_MODE_SLV;                                                //Slv模式,高位在前
      000FA6 75 FC 80         [24]  817 	mov	_SPI0_SETUP,#0x80
                                    818 ;	source/CH549_SPI.c:85: SPI0_CTRL = bS0_MISO_OE;                                                  //MISO 输出使能
      000FA9 75 FA 80         [24]  819 	mov	_SPI0_CTRL,#0x80
                                    820 ;	source/CH549_SPI.c:91: }
      000FAC 22               [24]  821 	ret
                                    822 ;------------------------------------------------------------
                                    823 ;Allocation info for local variables in function 'CH549SPISlvWrite'
                                    824 ;------------------------------------------------------------
                                    825 ;dat                       Allocated to registers 
                                    826 ;------------------------------------------------------------
                                    827 ;	source/CH549_SPI.c:99: void CH549SPISlvWrite(UINT8 dat)
                                    828 ;	-----------------------------------------
                                    829 ;	 function CH549SPISlvWrite
                                    830 ;	-----------------------------------------
      000FAD                        831 _CH549SPISlvWrite:
      000FAD 85 82 F9         [24]  832 	mov	_SPI0_DATA,dpl
                                    833 ;	source/CH549_SPI.c:102: while(S0_FREE==0)
      000FB0                        834 00101$:
      000FB0 30 FB FD         [24]  835 	jnb	_S0_FREE,00101$
                                    836 ;	source/CH549_SPI.c:106: }
      000FB3 22               [24]  837 	ret
                                    838 ;------------------------------------------------------------
                                    839 ;Allocation info for local variables in function 'CH549SPISlvRead'
                                    840 ;------------------------------------------------------------
                                    841 ;	source/CH549_SPI.c:114: UINT8 CH549SPISlvRead()
                                    842 ;	-----------------------------------------
                                    843 ;	 function CH549SPISlvRead
                                    844 ;	-----------------------------------------
      000FB4                        845 _CH549SPISlvRead:
                                    846 ;	source/CH549_SPI.c:116: while(S0_FREE == 0)
      000FB4                        847 00101$:
      000FB4 30 FB FD         [24]  848 	jnb	_S0_FREE,00101$
                                    849 ;	source/CH549_SPI.c:120: return SPI0_DATA;
      000FB7 85 F9 82         [24]  850 	mov	dpl,_SPI0_DATA
                                    851 ;	source/CH549_SPI.c:121: }
      000FBA 22               [24]  852 	ret
                                    853 	.area CSEG    (CODE)
                                    854 	.area CONST   (CODE)
                                    855 	.area CABS    (ABS,CODE)
