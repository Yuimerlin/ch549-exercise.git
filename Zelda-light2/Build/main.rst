                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 4.0.0 #11528 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module main
                                      6 	.optsdcc -mmcs51 --model-large
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _main
                                     12 	.globl _Send_ALL
                                     13 	.globl _Set_Color
                                     14 	.globl _genColor
                                     15 	.globl _Buf_Put0
                                     16 	.globl _RGB_Rst
                                     17 	.globl _Send_2811_24bits
                                     18 	.globl _rand
                                     19 	.globl _mDelaymS
                                     20 	.globl _mDelayuS
                                     21 	.globl _CfgFsys
                                     22 	.globl _UIF_BUS_RST
                                     23 	.globl _UIF_DETECT
                                     24 	.globl _UIF_TRANSFER
                                     25 	.globl _UIF_SUSPEND
                                     26 	.globl _UIF_HST_SOF
                                     27 	.globl _UIF_FIFO_OV
                                     28 	.globl _U_SIE_FREE
                                     29 	.globl _U_TOG_OK
                                     30 	.globl _U_IS_NAK
                                     31 	.globl _S0_R_FIFO
                                     32 	.globl _S0_T_FIFO
                                     33 	.globl _S0_FREE
                                     34 	.globl _S0_IF_BYTE
                                     35 	.globl _S0_IF_FIRST
                                     36 	.globl _S0_IF_OV
                                     37 	.globl _S0_FST_ACT
                                     38 	.globl _CP_RL2
                                     39 	.globl _C_T2
                                     40 	.globl _TR2
                                     41 	.globl _EXEN2
                                     42 	.globl _TCLK
                                     43 	.globl _RCLK
                                     44 	.globl _EXF2
                                     45 	.globl _CAP1F
                                     46 	.globl _TF2
                                     47 	.globl _RI
                                     48 	.globl _TI
                                     49 	.globl _RB8
                                     50 	.globl _TB8
                                     51 	.globl _REN
                                     52 	.globl _SM2
                                     53 	.globl _SM1
                                     54 	.globl _SM0
                                     55 	.globl _IT0
                                     56 	.globl _IE0
                                     57 	.globl _IT1
                                     58 	.globl _IE1
                                     59 	.globl _TR0
                                     60 	.globl _TF0
                                     61 	.globl _TR1
                                     62 	.globl _TF1
                                     63 	.globl _XI
                                     64 	.globl _XO
                                     65 	.globl _P4_0
                                     66 	.globl _P4_1
                                     67 	.globl _P4_2
                                     68 	.globl _P4_3
                                     69 	.globl _P4_4
                                     70 	.globl _P4_5
                                     71 	.globl _P4_6
                                     72 	.globl _RXD
                                     73 	.globl _TXD
                                     74 	.globl _INT0
                                     75 	.globl _INT1
                                     76 	.globl _T0
                                     77 	.globl _T1
                                     78 	.globl _CAP0
                                     79 	.globl _INT3
                                     80 	.globl _P3_0
                                     81 	.globl _P3_1
                                     82 	.globl _P3_2
                                     83 	.globl _P3_3
                                     84 	.globl _P3_4
                                     85 	.globl _P3_5
                                     86 	.globl _P3_6
                                     87 	.globl _P3_7
                                     88 	.globl _PWM5
                                     89 	.globl _PWM4
                                     90 	.globl _INT0_
                                     91 	.globl _PWM3
                                     92 	.globl _PWM2
                                     93 	.globl _CAP1_
                                     94 	.globl _T2_
                                     95 	.globl _PWM1
                                     96 	.globl _CAP2_
                                     97 	.globl _T2EX_
                                     98 	.globl _PWM0
                                     99 	.globl _RXD1
                                    100 	.globl _PWM6
                                    101 	.globl _TXD1
                                    102 	.globl _PWM7
                                    103 	.globl _P2_0
                                    104 	.globl _P2_1
                                    105 	.globl _P2_2
                                    106 	.globl _P2_3
                                    107 	.globl _P2_4
                                    108 	.globl _P2_5
                                    109 	.globl _P2_6
                                    110 	.globl _P2_7
                                    111 	.globl _AIN0
                                    112 	.globl _CAP1
                                    113 	.globl _T2
                                    114 	.globl _AIN1
                                    115 	.globl _CAP2
                                    116 	.globl _T2EX
                                    117 	.globl _AIN2
                                    118 	.globl _AIN3
                                    119 	.globl _AIN4
                                    120 	.globl _UCC1
                                    121 	.globl _SCS
                                    122 	.globl _AIN5
                                    123 	.globl _UCC2
                                    124 	.globl _PWM0_
                                    125 	.globl _MOSI
                                    126 	.globl _AIN6
                                    127 	.globl _VBUS
                                    128 	.globl _RXD1_
                                    129 	.globl _MISO
                                    130 	.globl _AIN7
                                    131 	.globl _TXD1_
                                    132 	.globl _SCK
                                    133 	.globl _P1_0
                                    134 	.globl _P1_1
                                    135 	.globl _P1_2
                                    136 	.globl _P1_3
                                    137 	.globl _P1_4
                                    138 	.globl _P1_5
                                    139 	.globl _P1_6
                                    140 	.globl _P1_7
                                    141 	.globl _AIN8
                                    142 	.globl _AIN9
                                    143 	.globl _AIN10
                                    144 	.globl _RXD_
                                    145 	.globl _AIN11
                                    146 	.globl _TXD_
                                    147 	.globl _AIN12
                                    148 	.globl _RXD2
                                    149 	.globl _AIN13
                                    150 	.globl _TXD2
                                    151 	.globl _AIN14
                                    152 	.globl _RXD3
                                    153 	.globl _AIN15
                                    154 	.globl _TXD3
                                    155 	.globl _P0_0
                                    156 	.globl _P0_1
                                    157 	.globl _P0_2
                                    158 	.globl _P0_3
                                    159 	.globl _P0_4
                                    160 	.globl _P0_5
                                    161 	.globl _P0_6
                                    162 	.globl _P0_7
                                    163 	.globl _IE_SPI0
                                    164 	.globl _IE_INT3
                                    165 	.globl _IE_USB
                                    166 	.globl _IE_UART2
                                    167 	.globl _IE_ADC
                                    168 	.globl _IE_UART1
                                    169 	.globl _IE_UART3
                                    170 	.globl _IE_PWMX
                                    171 	.globl _IE_GPIO
                                    172 	.globl _IE_WDOG
                                    173 	.globl _PX0
                                    174 	.globl _PT0
                                    175 	.globl _PX1
                                    176 	.globl _PT1
                                    177 	.globl _PS
                                    178 	.globl _PT2
                                    179 	.globl _PL_FLAG
                                    180 	.globl _PH_FLAG
                                    181 	.globl _EX0
                                    182 	.globl _ET0
                                    183 	.globl _EX1
                                    184 	.globl _ET1
                                    185 	.globl _ES
                                    186 	.globl _ET2
                                    187 	.globl _E_DIS
                                    188 	.globl _EA
                                    189 	.globl _P
                                    190 	.globl _F1
                                    191 	.globl _OV
                                    192 	.globl _RS0
                                    193 	.globl _RS1
                                    194 	.globl _F0
                                    195 	.globl _AC
                                    196 	.globl _CY
                                    197 	.globl _UEP1_DMA_H
                                    198 	.globl _UEP1_DMA_L
                                    199 	.globl _UEP1_DMA
                                    200 	.globl _UEP0_DMA_H
                                    201 	.globl _UEP0_DMA_L
                                    202 	.globl _UEP0_DMA
                                    203 	.globl _UEP2_3_MOD
                                    204 	.globl _UEP4_1_MOD
                                    205 	.globl _UEP3_DMA_H
                                    206 	.globl _UEP3_DMA_L
                                    207 	.globl _UEP3_DMA
                                    208 	.globl _UEP2_DMA_H
                                    209 	.globl _UEP2_DMA_L
                                    210 	.globl _UEP2_DMA
                                    211 	.globl _USB_DEV_AD
                                    212 	.globl _USB_CTRL
                                    213 	.globl _USB_INT_EN
                                    214 	.globl _UEP4_T_LEN
                                    215 	.globl _UEP4_CTRL
                                    216 	.globl _UEP0_T_LEN
                                    217 	.globl _UEP0_CTRL
                                    218 	.globl _USB_RX_LEN
                                    219 	.globl _USB_MIS_ST
                                    220 	.globl _USB_INT_ST
                                    221 	.globl _USB_INT_FG
                                    222 	.globl _UEP3_T_LEN
                                    223 	.globl _UEP3_CTRL
                                    224 	.globl _UEP2_T_LEN
                                    225 	.globl _UEP2_CTRL
                                    226 	.globl _UEP1_T_LEN
                                    227 	.globl _UEP1_CTRL
                                    228 	.globl _UDEV_CTRL
                                    229 	.globl _USB_C_CTRL
                                    230 	.globl _ADC_PIN
                                    231 	.globl _ADC_CHAN
                                    232 	.globl _ADC_DAT_H
                                    233 	.globl _ADC_DAT_L
                                    234 	.globl _ADC_DAT
                                    235 	.globl _ADC_CFG
                                    236 	.globl _ADC_CTRL
                                    237 	.globl _TKEY_CTRL
                                    238 	.globl _SIF3
                                    239 	.globl _SBAUD3
                                    240 	.globl _SBUF3
                                    241 	.globl _SCON3
                                    242 	.globl _SIF2
                                    243 	.globl _SBAUD2
                                    244 	.globl _SBUF2
                                    245 	.globl _SCON2
                                    246 	.globl _SIF1
                                    247 	.globl _SBAUD1
                                    248 	.globl _SBUF1
                                    249 	.globl _SCON1
                                    250 	.globl _SPI0_SETUP
                                    251 	.globl _SPI0_CK_SE
                                    252 	.globl _SPI0_CTRL
                                    253 	.globl _SPI0_DATA
                                    254 	.globl _SPI0_STAT
                                    255 	.globl _PWM_DATA7
                                    256 	.globl _PWM_DATA6
                                    257 	.globl _PWM_DATA5
                                    258 	.globl _PWM_DATA4
                                    259 	.globl _PWM_DATA3
                                    260 	.globl _PWM_CTRL2
                                    261 	.globl _PWM_CK_SE
                                    262 	.globl _PWM_CTRL
                                    263 	.globl _PWM_DATA0
                                    264 	.globl _PWM_DATA1
                                    265 	.globl _PWM_DATA2
                                    266 	.globl _T2CAP1H
                                    267 	.globl _T2CAP1L
                                    268 	.globl _T2CAP1
                                    269 	.globl _TH2
                                    270 	.globl _TL2
                                    271 	.globl _T2COUNT
                                    272 	.globl _RCAP2H
                                    273 	.globl _RCAP2L
                                    274 	.globl _RCAP2
                                    275 	.globl _T2MOD
                                    276 	.globl _T2CON
                                    277 	.globl _T2CAP0H
                                    278 	.globl _T2CAP0L
                                    279 	.globl _T2CAP0
                                    280 	.globl _T2CON2
                                    281 	.globl _SBUF
                                    282 	.globl _SCON
                                    283 	.globl _TH1
                                    284 	.globl _TH0
                                    285 	.globl _TL1
                                    286 	.globl _TL0
                                    287 	.globl _TMOD
                                    288 	.globl _TCON
                                    289 	.globl _XBUS_AUX
                                    290 	.globl _PIN_FUNC
                                    291 	.globl _P5
                                    292 	.globl _P4_DIR_PU
                                    293 	.globl _P4_MOD_OC
                                    294 	.globl _P4
                                    295 	.globl _P3_DIR_PU
                                    296 	.globl _P3_MOD_OC
                                    297 	.globl _P3
                                    298 	.globl _P2_DIR_PU
                                    299 	.globl _P2_MOD_OC
                                    300 	.globl _P2
                                    301 	.globl _P1_DIR_PU
                                    302 	.globl _P1_MOD_OC
                                    303 	.globl _P1
                                    304 	.globl _P0_DIR_PU
                                    305 	.globl _P0_MOD_OC
                                    306 	.globl _P0
                                    307 	.globl _ROM_CTRL
                                    308 	.globl _ROM_DATA_HH
                                    309 	.globl _ROM_DATA_HL
                                    310 	.globl _ROM_DATA_HI
                                    311 	.globl _ROM_ADDR_H
                                    312 	.globl _ROM_ADDR_L
                                    313 	.globl _ROM_ADDR
                                    314 	.globl _GPIO_IE
                                    315 	.globl _INTX
                                    316 	.globl _IP_EX
                                    317 	.globl _IE_EX
                                    318 	.globl _IP
                                    319 	.globl _IE
                                    320 	.globl _WDOG_COUNT
                                    321 	.globl _RESET_KEEP
                                    322 	.globl _WAKE_CTRL
                                    323 	.globl _CLOCK_CFG
                                    324 	.globl _POWER_CFG
                                    325 	.globl _PCON
                                    326 	.globl _GLOBAL_CFG
                                    327 	.globl _SAFE_MOD
                                    328 	.globl _DPH
                                    329 	.globl _DPL
                                    330 	.globl _SP
                                    331 	.globl _A_INV
                                    332 	.globl _B
                                    333 	.globl _ACC
                                    334 	.globl _PSW
                                    335 	.globl _RGB_B
                                    336 	.globl _RGB_G
                                    337 	.globl _RGB_R
                                    338 	.globl _Column
                                    339 	.globl _buf_B
                                    340 	.globl _buf_G
                                    341 	.globl _buf_R
                                    342 ;--------------------------------------------------------
                                    343 ; special function registers
                                    344 ;--------------------------------------------------------
                                    345 	.area RSEG    (ABS,DATA)
      000000                        346 	.org 0x0000
                           0000D0   347 _PSW	=	0x00d0
                           0000E0   348 _ACC	=	0x00e0
                           0000F0   349 _B	=	0x00f0
                           0000FD   350 _A_INV	=	0x00fd
                           000081   351 _SP	=	0x0081
                           000082   352 _DPL	=	0x0082
                           000083   353 _DPH	=	0x0083
                           0000A1   354 _SAFE_MOD	=	0x00a1
                           0000B1   355 _GLOBAL_CFG	=	0x00b1
                           000087   356 _PCON	=	0x0087
                           0000BA   357 _POWER_CFG	=	0x00ba
                           0000B9   358 _CLOCK_CFG	=	0x00b9
                           0000A9   359 _WAKE_CTRL	=	0x00a9
                           0000FE   360 _RESET_KEEP	=	0x00fe
                           0000FF   361 _WDOG_COUNT	=	0x00ff
                           0000A8   362 _IE	=	0x00a8
                           0000B8   363 _IP	=	0x00b8
                           0000E8   364 _IE_EX	=	0x00e8
                           0000E9   365 _IP_EX	=	0x00e9
                           0000B3   366 _INTX	=	0x00b3
                           0000B2   367 _GPIO_IE	=	0x00b2
                           008584   368 _ROM_ADDR	=	0x8584
                           000084   369 _ROM_ADDR_L	=	0x0084
                           000085   370 _ROM_ADDR_H	=	0x0085
                           008F8E   371 _ROM_DATA_HI	=	0x8f8e
                           00008E   372 _ROM_DATA_HL	=	0x008e
                           00008F   373 _ROM_DATA_HH	=	0x008f
                           000086   374 _ROM_CTRL	=	0x0086
                           000080   375 _P0	=	0x0080
                           0000C4   376 _P0_MOD_OC	=	0x00c4
                           0000C5   377 _P0_DIR_PU	=	0x00c5
                           000090   378 _P1	=	0x0090
                           000092   379 _P1_MOD_OC	=	0x0092
                           000093   380 _P1_DIR_PU	=	0x0093
                           0000A0   381 _P2	=	0x00a0
                           000094   382 _P2_MOD_OC	=	0x0094
                           000095   383 _P2_DIR_PU	=	0x0095
                           0000B0   384 _P3	=	0x00b0
                           000096   385 _P3_MOD_OC	=	0x0096
                           000097   386 _P3_DIR_PU	=	0x0097
                           0000C0   387 _P4	=	0x00c0
                           0000C2   388 _P4_MOD_OC	=	0x00c2
                           0000C3   389 _P4_DIR_PU	=	0x00c3
                           0000AB   390 _P5	=	0x00ab
                           0000AA   391 _PIN_FUNC	=	0x00aa
                           0000A2   392 _XBUS_AUX	=	0x00a2
                           000088   393 _TCON	=	0x0088
                           000089   394 _TMOD	=	0x0089
                           00008A   395 _TL0	=	0x008a
                           00008B   396 _TL1	=	0x008b
                           00008C   397 _TH0	=	0x008c
                           00008D   398 _TH1	=	0x008d
                           000098   399 _SCON	=	0x0098
                           000099   400 _SBUF	=	0x0099
                           0000C1   401 _T2CON2	=	0x00c1
                           00C7C6   402 _T2CAP0	=	0xc7c6
                           0000C6   403 _T2CAP0L	=	0x00c6
                           0000C7   404 _T2CAP0H	=	0x00c7
                           0000C8   405 _T2CON	=	0x00c8
                           0000C9   406 _T2MOD	=	0x00c9
                           00CBCA   407 _RCAP2	=	0xcbca
                           0000CA   408 _RCAP2L	=	0x00ca
                           0000CB   409 _RCAP2H	=	0x00cb
                           00CDCC   410 _T2COUNT	=	0xcdcc
                           0000CC   411 _TL2	=	0x00cc
                           0000CD   412 _TH2	=	0x00cd
                           00CFCE   413 _T2CAP1	=	0xcfce
                           0000CE   414 _T2CAP1L	=	0x00ce
                           0000CF   415 _T2CAP1H	=	0x00cf
                           00009A   416 _PWM_DATA2	=	0x009a
                           00009B   417 _PWM_DATA1	=	0x009b
                           00009C   418 _PWM_DATA0	=	0x009c
                           00009D   419 _PWM_CTRL	=	0x009d
                           00009E   420 _PWM_CK_SE	=	0x009e
                           00009F   421 _PWM_CTRL2	=	0x009f
                           0000A3   422 _PWM_DATA3	=	0x00a3
                           0000A4   423 _PWM_DATA4	=	0x00a4
                           0000A5   424 _PWM_DATA5	=	0x00a5
                           0000A6   425 _PWM_DATA6	=	0x00a6
                           0000A7   426 _PWM_DATA7	=	0x00a7
                           0000F8   427 _SPI0_STAT	=	0x00f8
                           0000F9   428 _SPI0_DATA	=	0x00f9
                           0000FA   429 _SPI0_CTRL	=	0x00fa
                           0000FB   430 _SPI0_CK_SE	=	0x00fb
                           0000FC   431 _SPI0_SETUP	=	0x00fc
                           0000BC   432 _SCON1	=	0x00bc
                           0000BD   433 _SBUF1	=	0x00bd
                           0000BE   434 _SBAUD1	=	0x00be
                           0000BF   435 _SIF1	=	0x00bf
                           0000B4   436 _SCON2	=	0x00b4
                           0000B5   437 _SBUF2	=	0x00b5
                           0000B6   438 _SBAUD2	=	0x00b6
                           0000B7   439 _SIF2	=	0x00b7
                           0000AC   440 _SCON3	=	0x00ac
                           0000AD   441 _SBUF3	=	0x00ad
                           0000AE   442 _SBAUD3	=	0x00ae
                           0000AF   443 _SIF3	=	0x00af
                           0000F1   444 _TKEY_CTRL	=	0x00f1
                           0000F2   445 _ADC_CTRL	=	0x00f2
                           0000F3   446 _ADC_CFG	=	0x00f3
                           00F5F4   447 _ADC_DAT	=	0xf5f4
                           0000F4   448 _ADC_DAT_L	=	0x00f4
                           0000F5   449 _ADC_DAT_H	=	0x00f5
                           0000F6   450 _ADC_CHAN	=	0x00f6
                           0000F7   451 _ADC_PIN	=	0x00f7
                           000091   452 _USB_C_CTRL	=	0x0091
                           0000D1   453 _UDEV_CTRL	=	0x00d1
                           0000D2   454 _UEP1_CTRL	=	0x00d2
                           0000D3   455 _UEP1_T_LEN	=	0x00d3
                           0000D4   456 _UEP2_CTRL	=	0x00d4
                           0000D5   457 _UEP2_T_LEN	=	0x00d5
                           0000D6   458 _UEP3_CTRL	=	0x00d6
                           0000D7   459 _UEP3_T_LEN	=	0x00d7
                           0000D8   460 _USB_INT_FG	=	0x00d8
                           0000D9   461 _USB_INT_ST	=	0x00d9
                           0000DA   462 _USB_MIS_ST	=	0x00da
                           0000DB   463 _USB_RX_LEN	=	0x00db
                           0000DC   464 _UEP0_CTRL	=	0x00dc
                           0000DD   465 _UEP0_T_LEN	=	0x00dd
                           0000DE   466 _UEP4_CTRL	=	0x00de
                           0000DF   467 _UEP4_T_LEN	=	0x00df
                           0000E1   468 _USB_INT_EN	=	0x00e1
                           0000E2   469 _USB_CTRL	=	0x00e2
                           0000E3   470 _USB_DEV_AD	=	0x00e3
                           00E5E4   471 _UEP2_DMA	=	0xe5e4
                           0000E4   472 _UEP2_DMA_L	=	0x00e4
                           0000E5   473 _UEP2_DMA_H	=	0x00e5
                           00E7E6   474 _UEP3_DMA	=	0xe7e6
                           0000E6   475 _UEP3_DMA_L	=	0x00e6
                           0000E7   476 _UEP3_DMA_H	=	0x00e7
                           0000EA   477 _UEP4_1_MOD	=	0x00ea
                           0000EB   478 _UEP2_3_MOD	=	0x00eb
                           00EDEC   479 _UEP0_DMA	=	0xedec
                           0000EC   480 _UEP0_DMA_L	=	0x00ec
                           0000ED   481 _UEP0_DMA_H	=	0x00ed
                           00EFEE   482 _UEP1_DMA	=	0xefee
                           0000EE   483 _UEP1_DMA_L	=	0x00ee
                           0000EF   484 _UEP1_DMA_H	=	0x00ef
                                    485 ;--------------------------------------------------------
                                    486 ; special function bits
                                    487 ;--------------------------------------------------------
                                    488 	.area RSEG    (ABS,DATA)
      000000                        489 	.org 0x0000
                           0000D7   490 _CY	=	0x00d7
                           0000D6   491 _AC	=	0x00d6
                           0000D5   492 _F0	=	0x00d5
                           0000D4   493 _RS1	=	0x00d4
                           0000D3   494 _RS0	=	0x00d3
                           0000D2   495 _OV	=	0x00d2
                           0000D1   496 _F1	=	0x00d1
                           0000D0   497 _P	=	0x00d0
                           0000AF   498 _EA	=	0x00af
                           0000AE   499 _E_DIS	=	0x00ae
                           0000AD   500 _ET2	=	0x00ad
                           0000AC   501 _ES	=	0x00ac
                           0000AB   502 _ET1	=	0x00ab
                           0000AA   503 _EX1	=	0x00aa
                           0000A9   504 _ET0	=	0x00a9
                           0000A8   505 _EX0	=	0x00a8
                           0000BF   506 _PH_FLAG	=	0x00bf
                           0000BE   507 _PL_FLAG	=	0x00be
                           0000BD   508 _PT2	=	0x00bd
                           0000BC   509 _PS	=	0x00bc
                           0000BB   510 _PT1	=	0x00bb
                           0000BA   511 _PX1	=	0x00ba
                           0000B9   512 _PT0	=	0x00b9
                           0000B8   513 _PX0	=	0x00b8
                           0000EF   514 _IE_WDOG	=	0x00ef
                           0000EE   515 _IE_GPIO	=	0x00ee
                           0000ED   516 _IE_PWMX	=	0x00ed
                           0000ED   517 _IE_UART3	=	0x00ed
                           0000EC   518 _IE_UART1	=	0x00ec
                           0000EB   519 _IE_ADC	=	0x00eb
                           0000EB   520 _IE_UART2	=	0x00eb
                           0000EA   521 _IE_USB	=	0x00ea
                           0000E9   522 _IE_INT3	=	0x00e9
                           0000E8   523 _IE_SPI0	=	0x00e8
                           000087   524 _P0_7	=	0x0087
                           000086   525 _P0_6	=	0x0086
                           000085   526 _P0_5	=	0x0085
                           000084   527 _P0_4	=	0x0084
                           000083   528 _P0_3	=	0x0083
                           000082   529 _P0_2	=	0x0082
                           000081   530 _P0_1	=	0x0081
                           000080   531 _P0_0	=	0x0080
                           000087   532 _TXD3	=	0x0087
                           000087   533 _AIN15	=	0x0087
                           000086   534 _RXD3	=	0x0086
                           000086   535 _AIN14	=	0x0086
                           000085   536 _TXD2	=	0x0085
                           000085   537 _AIN13	=	0x0085
                           000084   538 _RXD2	=	0x0084
                           000084   539 _AIN12	=	0x0084
                           000083   540 _TXD_	=	0x0083
                           000083   541 _AIN11	=	0x0083
                           000082   542 _RXD_	=	0x0082
                           000082   543 _AIN10	=	0x0082
                           000081   544 _AIN9	=	0x0081
                           000080   545 _AIN8	=	0x0080
                           000097   546 _P1_7	=	0x0097
                           000096   547 _P1_6	=	0x0096
                           000095   548 _P1_5	=	0x0095
                           000094   549 _P1_4	=	0x0094
                           000093   550 _P1_3	=	0x0093
                           000092   551 _P1_2	=	0x0092
                           000091   552 _P1_1	=	0x0091
                           000090   553 _P1_0	=	0x0090
                           000097   554 _SCK	=	0x0097
                           000097   555 _TXD1_	=	0x0097
                           000097   556 _AIN7	=	0x0097
                           000096   557 _MISO	=	0x0096
                           000096   558 _RXD1_	=	0x0096
                           000096   559 _VBUS	=	0x0096
                           000096   560 _AIN6	=	0x0096
                           000095   561 _MOSI	=	0x0095
                           000095   562 _PWM0_	=	0x0095
                           000095   563 _UCC2	=	0x0095
                           000095   564 _AIN5	=	0x0095
                           000094   565 _SCS	=	0x0094
                           000094   566 _UCC1	=	0x0094
                           000094   567 _AIN4	=	0x0094
                           000093   568 _AIN3	=	0x0093
                           000092   569 _AIN2	=	0x0092
                           000091   570 _T2EX	=	0x0091
                           000091   571 _CAP2	=	0x0091
                           000091   572 _AIN1	=	0x0091
                           000090   573 _T2	=	0x0090
                           000090   574 _CAP1	=	0x0090
                           000090   575 _AIN0	=	0x0090
                           0000A7   576 _P2_7	=	0x00a7
                           0000A6   577 _P2_6	=	0x00a6
                           0000A5   578 _P2_5	=	0x00a5
                           0000A4   579 _P2_4	=	0x00a4
                           0000A3   580 _P2_3	=	0x00a3
                           0000A2   581 _P2_2	=	0x00a2
                           0000A1   582 _P2_1	=	0x00a1
                           0000A0   583 _P2_0	=	0x00a0
                           0000A7   584 _PWM7	=	0x00a7
                           0000A7   585 _TXD1	=	0x00a7
                           0000A6   586 _PWM6	=	0x00a6
                           0000A6   587 _RXD1	=	0x00a6
                           0000A5   588 _PWM0	=	0x00a5
                           0000A5   589 _T2EX_	=	0x00a5
                           0000A5   590 _CAP2_	=	0x00a5
                           0000A4   591 _PWM1	=	0x00a4
                           0000A4   592 _T2_	=	0x00a4
                           0000A4   593 _CAP1_	=	0x00a4
                           0000A3   594 _PWM2	=	0x00a3
                           0000A2   595 _PWM3	=	0x00a2
                           0000A2   596 _INT0_	=	0x00a2
                           0000A1   597 _PWM4	=	0x00a1
                           0000A0   598 _PWM5	=	0x00a0
                           0000B7   599 _P3_7	=	0x00b7
                           0000B6   600 _P3_6	=	0x00b6
                           0000B5   601 _P3_5	=	0x00b5
                           0000B4   602 _P3_4	=	0x00b4
                           0000B3   603 _P3_3	=	0x00b3
                           0000B2   604 _P3_2	=	0x00b2
                           0000B1   605 _P3_1	=	0x00b1
                           0000B0   606 _P3_0	=	0x00b0
                           0000B7   607 _INT3	=	0x00b7
                           0000B6   608 _CAP0	=	0x00b6
                           0000B5   609 _T1	=	0x00b5
                           0000B4   610 _T0	=	0x00b4
                           0000B3   611 _INT1	=	0x00b3
                           0000B2   612 _INT0	=	0x00b2
                           0000B1   613 _TXD	=	0x00b1
                           0000B0   614 _RXD	=	0x00b0
                           0000C6   615 _P4_6	=	0x00c6
                           0000C5   616 _P4_5	=	0x00c5
                           0000C4   617 _P4_4	=	0x00c4
                           0000C3   618 _P4_3	=	0x00c3
                           0000C2   619 _P4_2	=	0x00c2
                           0000C1   620 _P4_1	=	0x00c1
                           0000C0   621 _P4_0	=	0x00c0
                           0000C7   622 _XO	=	0x00c7
                           0000C6   623 _XI	=	0x00c6
                           00008F   624 _TF1	=	0x008f
                           00008E   625 _TR1	=	0x008e
                           00008D   626 _TF0	=	0x008d
                           00008C   627 _TR0	=	0x008c
                           00008B   628 _IE1	=	0x008b
                           00008A   629 _IT1	=	0x008a
                           000089   630 _IE0	=	0x0089
                           000088   631 _IT0	=	0x0088
                           00009F   632 _SM0	=	0x009f
                           00009E   633 _SM1	=	0x009e
                           00009D   634 _SM2	=	0x009d
                           00009C   635 _REN	=	0x009c
                           00009B   636 _TB8	=	0x009b
                           00009A   637 _RB8	=	0x009a
                           000099   638 _TI	=	0x0099
                           000098   639 _RI	=	0x0098
                           0000CF   640 _TF2	=	0x00cf
                           0000CF   641 _CAP1F	=	0x00cf
                           0000CE   642 _EXF2	=	0x00ce
                           0000CD   643 _RCLK	=	0x00cd
                           0000CC   644 _TCLK	=	0x00cc
                           0000CB   645 _EXEN2	=	0x00cb
                           0000CA   646 _TR2	=	0x00ca
                           0000C9   647 _C_T2	=	0x00c9
                           0000C8   648 _CP_RL2	=	0x00c8
                           0000FF   649 _S0_FST_ACT	=	0x00ff
                           0000FE   650 _S0_IF_OV	=	0x00fe
                           0000FD   651 _S0_IF_FIRST	=	0x00fd
                           0000FC   652 _S0_IF_BYTE	=	0x00fc
                           0000FB   653 _S0_FREE	=	0x00fb
                           0000FA   654 _S0_T_FIFO	=	0x00fa
                           0000F8   655 _S0_R_FIFO	=	0x00f8
                           0000DF   656 _U_IS_NAK	=	0x00df
                           0000DE   657 _U_TOG_OK	=	0x00de
                           0000DD   658 _U_SIE_FREE	=	0x00dd
                           0000DC   659 _UIF_FIFO_OV	=	0x00dc
                           0000DB   660 _UIF_HST_SOF	=	0x00db
                           0000DA   661 _UIF_SUSPEND	=	0x00da
                           0000D9   662 _UIF_TRANSFER	=	0x00d9
                           0000D8   663 _UIF_DETECT	=	0x00d8
                           0000D8   664 _UIF_BUS_RST	=	0x00d8
                                    665 ;--------------------------------------------------------
                                    666 ; overlayable register banks
                                    667 ;--------------------------------------------------------
                                    668 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        669 	.ds 8
                                    670 ;--------------------------------------------------------
                                    671 ; internal ram data
                                    672 ;--------------------------------------------------------
                                    673 	.area DSEG    (DATA)
                                    674 ;--------------------------------------------------------
                                    675 ; overlayable items in internal ram 
                                    676 ;--------------------------------------------------------
                                    677 ;--------------------------------------------------------
                                    678 ; Stack segment in internal ram 
                                    679 ;--------------------------------------------------------
                                    680 	.area	SSEG
      000009                        681 __start__stack:
      000009                        682 	.ds	1
                                    683 
                                    684 ;--------------------------------------------------------
                                    685 ; indirectly addressable internal ram data
                                    686 ;--------------------------------------------------------
                                    687 	.area ISEG    (DATA)
                                    688 ;--------------------------------------------------------
                                    689 ; absolute internal ram data
                                    690 ;--------------------------------------------------------
                                    691 	.area IABS    (ABS,DATA)
                                    692 	.area IABS    (ABS,DATA)
                                    693 ;--------------------------------------------------------
                                    694 ; bit data
                                    695 ;--------------------------------------------------------
                                    696 	.area BSEG    (BIT)
                                    697 ;--------------------------------------------------------
                                    698 ; paged external ram data
                                    699 ;--------------------------------------------------------
                                    700 	.area PSEG    (PAG,XDATA)
                                    701 ;--------------------------------------------------------
                                    702 ; external ram data
                                    703 ;--------------------------------------------------------
                                    704 	.area XSEG    (XDATA)
      000001                        705 _buf_R::
      000001                        706 	.ds 455
      0001C8                        707 _buf_G::
      0001C8                        708 	.ds 455
      00038F                        709 _buf_B::
      00038F                        710 	.ds 455
      000556                        711 _Column::
      000556                        712 	.ds 13
      000563                        713 _RGB_R::
      000563                        714 	.ds 13
      000570                        715 _RGB_G::
      000570                        716 	.ds 13
      00057D                        717 _RGB_B::
      00057D                        718 	.ds 13
                                    719 ;--------------------------------------------------------
                                    720 ; absolute external ram data
                                    721 ;--------------------------------------------------------
                                    722 	.area XABS    (ABS,XDATA)
                                    723 ;--------------------------------------------------------
                                    724 ; external initialized ram data
                                    725 ;--------------------------------------------------------
                                    726 	.area HOME    (CODE)
                                    727 	.area GSINIT0 (CODE)
                                    728 	.area GSINIT1 (CODE)
                                    729 	.area GSINIT2 (CODE)
                                    730 	.area GSINIT3 (CODE)
                                    731 	.area GSINIT4 (CODE)
                                    732 	.area GSINIT5 (CODE)
                                    733 	.area GSINIT  (CODE)
                                    734 	.area GSFINAL (CODE)
                                    735 	.area CSEG    (CODE)
                                    736 ;--------------------------------------------------------
                                    737 ; interrupt vector 
                                    738 ;--------------------------------------------------------
                                    739 	.area HOME    (CODE)
      000000                        740 __interrupt_vect:
      000000 02 00 06         [24]  741 	ljmp	__sdcc_gsinit_startup
                                    742 ;--------------------------------------------------------
                                    743 ; global & static initialisations
                                    744 ;--------------------------------------------------------
                                    745 	.area HOME    (CODE)
                                    746 	.area GSINIT  (CODE)
                                    747 	.area GSFINAL (CODE)
                                    748 	.area GSINIT  (CODE)
                                    749 	.globl __sdcc_gsinit_startup
                                    750 	.globl __sdcc_program_startup
                                    751 	.globl __start__stack
                                    752 	.globl __mcs51_genRAMCLEAR
                                    753 ;	usr/main.c:22: unsigned char buf_R[COLUMN_LED][COW] = {{0}};//假定灯带长度数据条的输出颜色缓存
      000019 90 00 01         [24]  754 	mov	dptr,#_buf_R
      00001C E4               [12]  755 	clr	a
      00001D F0               [24]  756 	movx	@dptr,a
                                    757 ;	usr/main.c:23: unsigned char buf_G[COLUMN_LED][COW] = {{0}};
      00001E 90 01 C8         [24]  758 	mov	dptr,#_buf_G
      000021 F0               [24]  759 	movx	@dptr,a
                                    760 ;	usr/main.c:24: unsigned char buf_B[COLUMN_LED][COW] = {{0}};
      000022 90 03 8F         [24]  761 	mov	dptr,#_buf_B
      000025 F0               [24]  762 	movx	@dptr,a
                                    763 	.area GSFINAL (CODE)
      000026 02 00 03         [24]  764 	ljmp	__sdcc_program_startup
                                    765 ;--------------------------------------------------------
                                    766 ; Home
                                    767 ;--------------------------------------------------------
                                    768 	.area HOME    (CODE)
                                    769 	.area HOME    (CODE)
      000003                        770 __sdcc_program_startup:
      000003 02 0A D3         [24]  771 	ljmp	_main
                                    772 ;	return from main will return to caller
                                    773 ;--------------------------------------------------------
                                    774 ; code
                                    775 ;--------------------------------------------------------
                                    776 	.area CSEG    (CODE)
                                    777 ;------------------------------------------------------------
                                    778 ;Allocation info for local variables in function 'Send_2811_24bits'
                                    779 ;------------------------------------------------------------
                                    780 ;R8                        Allocated to stack - _bp -3
                                    781 ;B8                        Allocated to stack - _bp -4
                                    782 ;G8                        Allocated to registers r7 
                                    783 ;n                         Allocated to registers r6 
                                    784 ;------------------------------------------------------------
                                    785 ;	usr/main.c:58: void Send_2811_24bits(unsigned char G8,unsigned char R8,unsigned char B8)
                                    786 ;	-----------------------------------------
                                    787 ;	 function Send_2811_24bits
                                    788 ;	-----------------------------------------
      000029                        789 _Send_2811_24bits:
                           000007   790 	ar7 = 0x07
                           000006   791 	ar6 = 0x06
                           000005   792 	ar5 = 0x05
                           000004   793 	ar4 = 0x04
                           000003   794 	ar3 = 0x03
                           000002   795 	ar2 = 0x02
                           000001   796 	ar1 = 0x01
                           000000   797 	ar0 = 0x00
      000029 C0 08            [24]  798 	push	_bp
      00002B 85 81 08         [24]  799 	mov	_bp,sp
      00002E AF 82            [24]  800 	mov	r7,dpl
                                    801 ;	usr/main.c:63: for(n=0;n<8;n++)
      000030 7E 00            [12]  802 	mov	r6,#0x00
      000032                        803 00131$:
                                    804 ;	usr/main.c:66: if(G8&0x80)
      000032 EF               [12]  805 	mov	a,r7
      000033 30 E7 2E         [24]  806 	jnb	acc.7,00104$
                                    807 ;	usr/main.c:68: RGB_1();
                                    808 ;	assignBit
      000036 D2 A2            [12]  809 	setb	_P2_2
      000038 00               [12]  810 	NOP	
      000039 00               [12]  811 	NOP	
      00003A 00               [12]  812 	NOP	
      00003B 00               [12]  813 	NOP	
      00003C 00               [12]  814 	NOP	
      00003D 00               [12]  815 	NOP	
      00003E 00               [12]  816 	NOP	
      00003F 00               [12]  817 	NOP	
      000040 00               [12]  818 	NOP	
      000041 00               [12]  819 	NOP	
      000042 00               [12]  820 	NOP	
      000043 00               [12]  821 	NOP	
      000044 00               [12]  822 	NOP	
      000045 00               [12]  823 	NOP	
      000046 00               [12]  824 	NOP	
      000047 00               [12]  825 	NOP	
      000048 00               [12]  826 	NOP	
      000049 00               [12]  827 	NOP	
      00004A 00               [12]  828 	NOP	
      00004B 00               [12]  829 	NOP	
      00004C 00               [12]  830 	NOP	
      00004D 00               [12]  831 	NOP	
      00004E 00               [12]  832 	NOP	
      00004F 00               [12]  833 	NOP	
      000050 00               [12]  834 	NOP	
      000051 00               [12]  835 	NOP	
      000052 00               [12]  836 	NOP	
      000053 00               [12]  837 	NOP	
      000054 00               [12]  838 	NOP	
      000055 00               [12]  839 	NOP	
      000056 00               [12]  840 	NOP	
      000057 00               [12]  841 	NOP	
      000058 00               [12]  842 	NOP	
      000059 00               [12]  843 	NOP	
      00005A 00               [12]  844 	NOP	
      00005B 00               [12]  845 	NOP	
      00005C 00               [12]  846 	NOP	
      00005D 00               [12]  847 	NOP	
      00005E 00               [12]  848 	NOP	
      00005F 00               [12]  849 	NOP	
                                    850 ;	assignBit
      000060 C2 A2            [12]  851 	clr	_P2_2
                                    852 ;	usr/main.c:72: RGB_0();
      000062 80 22            [24]  853 	sjmp	00109$
      000064                        854 00104$:
                                    855 ;	assignBit
      000064 D2 A2            [12]  856 	setb	_P2_2
      000066 00               [12]  857 	NOP	
      000067 00               [12]  858 	NOP	
      000068 00               [12]  859 	NOP	
      000069 00               [12]  860 	NOP	
      00006A 00               [12]  861 	NOP	
      00006B 00               [12]  862 	NOP	
      00006C 00               [12]  863 	NOP	
      00006D 00               [12]  864 	NOP	
      00006E 00               [12]  865 	NOP	
      00006F 00               [12]  866 	NOP	
      000070 00               [12]  867 	NOP	
      000071 00               [12]  868 	NOP	
      000072 00               [12]  869 	NOP	
      000073 00               [12]  870 	NOP	
      000074 00               [12]  871 	NOP	
      000075 00               [12]  872 	NOP	
      000076 00               [12]  873 	NOP	
      000077 00               [12]  874 	NOP	
      000078 00               [12]  875 	NOP	
      000079 00               [12]  876 	NOP	
                                    877 ;	assignBit
      00007A C2 A2            [12]  878 	clr	_P2_2
      00007C 00               [12]  879 	NOP	
      00007D 00               [12]  880 	NOP	
      00007E 00               [12]  881 	NOP	
      00007F 00               [12]  882 	NOP	
      000080 00               [12]  883 	NOP	
      000081 00               [12]  884 	NOP	
      000082 00               [12]  885 	NOP	
      000083 00               [12]  886 	NOP	
      000084 00               [12]  887 	NOP	
      000085 00               [12]  888 	NOP	
      000086                        889 00109$:
                                    890 ;	usr/main.c:74: G8<<=1;
      000086 8F 05            [24]  891 	mov	ar5,r7
      000088 ED               [12]  892 	mov	a,r5
      000089 2D               [12]  893 	add	a,r5
      00008A FF               [12]  894 	mov	r7,a
                                    895 ;	usr/main.c:63: for(n=0;n<8;n++)
      00008B 0E               [12]  896 	inc	r6
      00008C BE 08 00         [24]  897 	cjne	r6,#0x08,00175$
      00008F                        898 00175$:
      00008F 40 A1            [24]  899 	jc	00131$
                                    900 ;	usr/main.c:77: for(n=0;n<8;n++)
      000091 7F 00            [12]  901 	mov	r7,#0x00
      000093                        902 00133$:
                                    903 ;	usr/main.c:80: if(R8&0x80)
      000093 E5 08            [12]  904 	mov	a,_bp
      000095 24 FD            [12]  905 	add	a,#0xfd
      000097 F8               [12]  906 	mov	r0,a
      000098 E6               [12]  907 	mov	a,@r0
      000099 30 E7 2E         [24]  908 	jnb	acc.7,00114$
                                    909 ;	usr/main.c:82: RGB_1();
                                    910 ;	assignBit
      00009C D2 A2            [12]  911 	setb	_P2_2
      00009E 00               [12]  912 	NOP	
      00009F 00               [12]  913 	NOP	
      0000A0 00               [12]  914 	NOP	
      0000A1 00               [12]  915 	NOP	
      0000A2 00               [12]  916 	NOP	
      0000A3 00               [12]  917 	NOP	
      0000A4 00               [12]  918 	NOP	
      0000A5 00               [12]  919 	NOP	
      0000A6 00               [12]  920 	NOP	
      0000A7 00               [12]  921 	NOP	
      0000A8 00               [12]  922 	NOP	
      0000A9 00               [12]  923 	NOP	
      0000AA 00               [12]  924 	NOP	
      0000AB 00               [12]  925 	NOP	
      0000AC 00               [12]  926 	NOP	
      0000AD 00               [12]  927 	NOP	
      0000AE 00               [12]  928 	NOP	
      0000AF 00               [12]  929 	NOP	
      0000B0 00               [12]  930 	NOP	
      0000B1 00               [12]  931 	NOP	
      0000B2 00               [12]  932 	NOP	
      0000B3 00               [12]  933 	NOP	
      0000B4 00               [12]  934 	NOP	
      0000B5 00               [12]  935 	NOP	
      0000B6 00               [12]  936 	NOP	
      0000B7 00               [12]  937 	NOP	
      0000B8 00               [12]  938 	NOP	
      0000B9 00               [12]  939 	NOP	
      0000BA 00               [12]  940 	NOP	
      0000BB 00               [12]  941 	NOP	
      0000BC 00               [12]  942 	NOP	
      0000BD 00               [12]  943 	NOP	
      0000BE 00               [12]  944 	NOP	
      0000BF 00               [12]  945 	NOP	
      0000C0 00               [12]  946 	NOP	
      0000C1 00               [12]  947 	NOP	
      0000C2 00               [12]  948 	NOP	
      0000C3 00               [12]  949 	NOP	
      0000C4 00               [12]  950 	NOP	
      0000C5 00               [12]  951 	NOP	
                                    952 ;	assignBit
      0000C6 C2 A2            [12]  953 	clr	_P2_2
                                    954 ;	usr/main.c:86: RGB_0();
      0000C8 80 22            [24]  955 	sjmp	00119$
      0000CA                        956 00114$:
                                    957 ;	assignBit
      0000CA D2 A2            [12]  958 	setb	_P2_2
      0000CC 00               [12]  959 	NOP	
      0000CD 00               [12]  960 	NOP	
      0000CE 00               [12]  961 	NOP	
      0000CF 00               [12]  962 	NOP	
      0000D0 00               [12]  963 	NOP	
      0000D1 00               [12]  964 	NOP	
      0000D2 00               [12]  965 	NOP	
      0000D3 00               [12]  966 	NOP	
      0000D4 00               [12]  967 	NOP	
      0000D5 00               [12]  968 	NOP	
      0000D6 00               [12]  969 	NOP	
      0000D7 00               [12]  970 	NOP	
      0000D8 00               [12]  971 	NOP	
      0000D9 00               [12]  972 	NOP	
      0000DA 00               [12]  973 	NOP	
      0000DB 00               [12]  974 	NOP	
      0000DC 00               [12]  975 	NOP	
      0000DD 00               [12]  976 	NOP	
      0000DE 00               [12]  977 	NOP	
      0000DF 00               [12]  978 	NOP	
                                    979 ;	assignBit
      0000E0 C2 A2            [12]  980 	clr	_P2_2
      0000E2 00               [12]  981 	NOP	
      0000E3 00               [12]  982 	NOP	
      0000E4 00               [12]  983 	NOP	
      0000E5 00               [12]  984 	NOP	
      0000E6 00               [12]  985 	NOP	
      0000E7 00               [12]  986 	NOP	
      0000E8 00               [12]  987 	NOP	
      0000E9 00               [12]  988 	NOP	
      0000EA 00               [12]  989 	NOP	
      0000EB 00               [12]  990 	NOP	
      0000EC                        991 00119$:
                                    992 ;	usr/main.c:88: R8<<=1;
      0000EC E5 08            [12]  993 	mov	a,_bp
      0000EE 24 FD            [12]  994 	add	a,#0xfd
      0000F0 F8               [12]  995 	mov	r0,a
      0000F1 86 06            [24]  996 	mov	ar6,@r0
      0000F3 E5 08            [12]  997 	mov	a,_bp
      0000F5 24 FD            [12]  998 	add	a,#0xfd
      0000F7 F8               [12]  999 	mov	r0,a
      0000F8 EE               [12] 1000 	mov	a,r6
      0000F9 2E               [12] 1001 	add	a,r6
      0000FA F6               [12] 1002 	mov	@r0,a
                                   1003 ;	usr/main.c:77: for(n=0;n<8;n++)
      0000FB 0F               [12] 1004 	inc	r7
      0000FC BF 08 00         [24] 1005 	cjne	r7,#0x08,00178$
      0000FF                       1006 00178$:
      0000FF 40 92            [24] 1007 	jc	00133$
                                   1008 ;	usr/main.c:91: for(n=0;n<8;n++)
      000101 7F 00            [12] 1009 	mov	r7,#0x00
      000103                       1010 00135$:
                                   1011 ;	usr/main.c:94: if(B8&0x80)
      000103 E5 08            [12] 1012 	mov	a,_bp
      000105 24 FC            [12] 1013 	add	a,#0xfc
      000107 F8               [12] 1014 	mov	r0,a
      000108 E6               [12] 1015 	mov	a,@r0
      000109 30 E7 2E         [24] 1016 	jnb	acc.7,00124$
                                   1017 ;	usr/main.c:96: RGB_1();
                                   1018 ;	assignBit
      00010C D2 A2            [12] 1019 	setb	_P2_2
      00010E 00               [12] 1020 	NOP	
      00010F 00               [12] 1021 	NOP	
      000110 00               [12] 1022 	NOP	
      000111 00               [12] 1023 	NOP	
      000112 00               [12] 1024 	NOP	
      000113 00               [12] 1025 	NOP	
      000114 00               [12] 1026 	NOP	
      000115 00               [12] 1027 	NOP	
      000116 00               [12] 1028 	NOP	
      000117 00               [12] 1029 	NOP	
      000118 00               [12] 1030 	NOP	
      000119 00               [12] 1031 	NOP	
      00011A 00               [12] 1032 	NOP	
      00011B 00               [12] 1033 	NOP	
      00011C 00               [12] 1034 	NOP	
      00011D 00               [12] 1035 	NOP	
      00011E 00               [12] 1036 	NOP	
      00011F 00               [12] 1037 	NOP	
      000120 00               [12] 1038 	NOP	
      000121 00               [12] 1039 	NOP	
      000122 00               [12] 1040 	NOP	
      000123 00               [12] 1041 	NOP	
      000124 00               [12] 1042 	NOP	
      000125 00               [12] 1043 	NOP	
      000126 00               [12] 1044 	NOP	
      000127 00               [12] 1045 	NOP	
      000128 00               [12] 1046 	NOP	
      000129 00               [12] 1047 	NOP	
      00012A 00               [12] 1048 	NOP	
      00012B 00               [12] 1049 	NOP	
      00012C 00               [12] 1050 	NOP	
      00012D 00               [12] 1051 	NOP	
      00012E 00               [12] 1052 	NOP	
      00012F 00               [12] 1053 	NOP	
      000130 00               [12] 1054 	NOP	
      000131 00               [12] 1055 	NOP	
      000132 00               [12] 1056 	NOP	
      000133 00               [12] 1057 	NOP	
      000134 00               [12] 1058 	NOP	
      000135 00               [12] 1059 	NOP	
                                   1060 ;	assignBit
      000136 C2 A2            [12] 1061 	clr	_P2_2
                                   1062 ;	usr/main.c:100: RGB_0();
      000138 80 22            [24] 1063 	sjmp	00129$
      00013A                       1064 00124$:
                                   1065 ;	assignBit
      00013A D2 A2            [12] 1066 	setb	_P2_2
      00013C 00               [12] 1067 	NOP	
      00013D 00               [12] 1068 	NOP	
      00013E 00               [12] 1069 	NOP	
      00013F 00               [12] 1070 	NOP	
      000140 00               [12] 1071 	NOP	
      000141 00               [12] 1072 	NOP	
      000142 00               [12] 1073 	NOP	
      000143 00               [12] 1074 	NOP	
      000144 00               [12] 1075 	NOP	
      000145 00               [12] 1076 	NOP	
      000146 00               [12] 1077 	NOP	
      000147 00               [12] 1078 	NOP	
      000148 00               [12] 1079 	NOP	
      000149 00               [12] 1080 	NOP	
      00014A 00               [12] 1081 	NOP	
      00014B 00               [12] 1082 	NOP	
      00014C 00               [12] 1083 	NOP	
      00014D 00               [12] 1084 	NOP	
      00014E 00               [12] 1085 	NOP	
      00014F 00               [12] 1086 	NOP	
                                   1087 ;	assignBit
      000150 C2 A2            [12] 1088 	clr	_P2_2
      000152 00               [12] 1089 	NOP	
      000153 00               [12] 1090 	NOP	
      000154 00               [12] 1091 	NOP	
      000155 00               [12] 1092 	NOP	
      000156 00               [12] 1093 	NOP	
      000157 00               [12] 1094 	NOP	
      000158 00               [12] 1095 	NOP	
      000159 00               [12] 1096 	NOP	
      00015A 00               [12] 1097 	NOP	
      00015B 00               [12] 1098 	NOP	
      00015C                       1099 00129$:
                                   1100 ;	usr/main.c:102: B8<<=1;
      00015C E5 08            [12] 1101 	mov	a,_bp
      00015E 24 FC            [12] 1102 	add	a,#0xfc
      000160 F8               [12] 1103 	mov	r0,a
      000161 86 06            [24] 1104 	mov	ar6,@r0
      000163 E5 08            [12] 1105 	mov	a,_bp
      000165 24 FC            [12] 1106 	add	a,#0xfc
      000167 F8               [12] 1107 	mov	r0,a
      000168 EE               [12] 1108 	mov	a,r6
      000169 2E               [12] 1109 	add	a,r6
      00016A F6               [12] 1110 	mov	@r0,a
                                   1111 ;	usr/main.c:91: for(n=0;n<8;n++)
      00016B 0F               [12] 1112 	inc	r7
      00016C BF 08 00         [24] 1113 	cjne	r7,#0x08,00181$
      00016F                       1114 00181$:
      00016F 40 92            [24] 1115 	jc	00135$
                                   1116 ;	usr/main.c:104: }
      000171 D0 08            [24] 1117 	pop	_bp
      000173 22               [24] 1118 	ret
                                   1119 ;------------------------------------------------------------
                                   1120 ;Allocation info for local variables in function 'RGB_Rst'
                                   1121 ;------------------------------------------------------------
                                   1122 ;	usr/main.c:107: void RGB_Rst()
                                   1123 ;	-----------------------------------------
                                   1124 ;	 function RGB_Rst
                                   1125 ;	-----------------------------------------
      000174                       1126 _RGB_Rst:
                                   1127 ;	usr/main.c:109: WS2812 = 0;
                                   1128 ;	assignBit
      000174 C2 A2            [12] 1129 	clr	_P2_2
                                   1130 ;	usr/main.c:110: mDelayuS( 50 );
      000176 90 00 32         [24] 1131 	mov	dptr,#0x0032
                                   1132 ;	usr/main.c:111: }
      000179 02 0B C4         [24] 1133 	ljmp	_mDelayuS
                                   1134 ;------------------------------------------------------------
                                   1135 ;Allocation info for local variables in function 'Buf_Put0'
                                   1136 ;------------------------------------------------------------
                                   1137 ;i                         Allocated to registers r0 
                                   1138 ;j                         Allocated to registers r7 
                                   1139 ;------------------------------------------------------------
                                   1140 ;	usr/main.c:114: void Buf_Put0()
                                   1141 ;	-----------------------------------------
                                   1142 ;	 function Buf_Put0
                                   1143 ;	-----------------------------------------
      00017C                       1144 _Buf_Put0:
                                   1145 ;	usr/main.c:117: for (j = 0; j < COLUMN_LED; j++)
      00017C 7F 00            [12] 1146 	mov	r7,#0x00
                                   1147 ;	usr/main.c:119: for(i=0;i<COW;i++)
      00017E                       1148 00109$:
      00017E EF               [12] 1149 	mov	a,r7
      00017F 75 F0 23         [24] 1150 	mov	b,#0x23
      000182 A4               [48] 1151 	mul	ab
      000183 FD               [12] 1152 	mov	r5,a
      000184 AE F0            [24] 1153 	mov	r6,b
      000186 24 01            [12] 1154 	add	a,#_buf_R
      000188 FB               [12] 1155 	mov	r3,a
      000189 EE               [12] 1156 	mov	a,r6
      00018A 34 00            [12] 1157 	addc	a,#(_buf_R >> 8)
      00018C FC               [12] 1158 	mov	r4,a
      00018D ED               [12] 1159 	mov	a,r5
      00018E 24 C8            [12] 1160 	add	a,#_buf_G
      000190 F9               [12] 1161 	mov	r1,a
      000191 EE               [12] 1162 	mov	a,r6
      000192 34 01            [12] 1163 	addc	a,#(_buf_G >> 8)
      000194 FA               [12] 1164 	mov	r2,a
      000195 ED               [12] 1165 	mov	a,r5
      000196 24 8F            [12] 1166 	add	a,#_buf_B
      000198 FD               [12] 1167 	mov	r5,a
      000199 EE               [12] 1168 	mov	a,r6
      00019A 34 03            [12] 1169 	addc	a,#(_buf_B >> 8)
      00019C FE               [12] 1170 	mov	r6,a
      00019D 78 00            [12] 1171 	mov	r0,#0x00
      00019F                       1172 00103$:
                                   1173 ;	usr/main.c:121: buf_R[j][i] = 0; 
      00019F E8               [12] 1174 	mov	a,r0
      0001A0 2B               [12] 1175 	add	a,r3
      0001A1 F5 82            [12] 1176 	mov	dpl,a
      0001A3 E4               [12] 1177 	clr	a
      0001A4 3C               [12] 1178 	addc	a,r4
      0001A5 F5 83            [12] 1179 	mov	dph,a
      0001A7 E4               [12] 1180 	clr	a
      0001A8 F0               [24] 1181 	movx	@dptr,a
                                   1182 ;	usr/main.c:122: buf_G[j][i] = 0;
      0001A9 E8               [12] 1183 	mov	a,r0
      0001AA 29               [12] 1184 	add	a,r1
      0001AB F5 82            [12] 1185 	mov	dpl,a
      0001AD E4               [12] 1186 	clr	a
      0001AE 3A               [12] 1187 	addc	a,r2
      0001AF F5 83            [12] 1188 	mov	dph,a
      0001B1 E4               [12] 1189 	clr	a
      0001B2 F0               [24] 1190 	movx	@dptr,a
                                   1191 ;	usr/main.c:123: buf_B[j][i] = 0;
      0001B3 E8               [12] 1192 	mov	a,r0
      0001B4 2D               [12] 1193 	add	a,r5
      0001B5 F5 82            [12] 1194 	mov	dpl,a
      0001B7 E4               [12] 1195 	clr	a
      0001B8 3E               [12] 1196 	addc	a,r6
      0001B9 F5 83            [12] 1197 	mov	dph,a
      0001BB E4               [12] 1198 	clr	a
      0001BC F0               [24] 1199 	movx	@dptr,a
                                   1200 ;	usr/main.c:119: for(i=0;i<COW;i++)
      0001BD 08               [12] 1201 	inc	r0
      0001BE B8 23 00         [24] 1202 	cjne	r0,#0x23,00123$
      0001C1                       1203 00123$:
      0001C1 40 DC            [24] 1204 	jc	00103$
                                   1205 ;	usr/main.c:117: for (j = 0; j < COLUMN_LED; j++)
      0001C3 0F               [12] 1206 	inc	r7
      0001C4 BF 0D 00         [24] 1207 	cjne	r7,#0x0d,00125$
      0001C7                       1208 00125$:
      0001C7 40 B5            [24] 1209 	jc	00109$
                                   1210 ;	usr/main.c:126: }
      0001C9 22               [24] 1211 	ret
                                   1212 ;------------------------------------------------------------
                                   1213 ;Allocation info for local variables in function 'genColor'
                                   1214 ;------------------------------------------------------------
                                   1215 ;j                         Allocated to stack - _bp +5
                                   1216 ;h                         Allocated to registers r4 r5 r6 r7 
                                   1217 ;s                         Allocated to registers 
                                   1218 ;v                         Allocated to stack - _bp +7
                                   1219 ;i                         Allocated to stack - _bp +11
                                   1220 ;C                         Allocated to stack - _bp +13
                                   1221 ;X                         Allocated to registers 
                                   1222 ;Y                         Allocated to stack - _bp +17
                                   1223 ;Z                         Allocated to stack - _bp +21
                                   1224 ;sloc0                     Allocated to stack - _bp +1
                                   1225 ;------------------------------------------------------------
                                   1226 ;	usr/main.c:129: void genColor()
                                   1227 ;	-----------------------------------------
                                   1228 ;	 function genColor
                                   1229 ;	-----------------------------------------
      0001CA                       1230 _genColor:
      0001CA C0 08            [24] 1231 	push	_bp
      0001CC E5 81            [12] 1232 	mov	a,sp
      0001CE F5 08            [12] 1233 	mov	_bp,a
      0001D0 24 18            [12] 1234 	add	a,#0x18
      0001D2 F5 81            [12] 1235 	mov	sp,a
                                   1236 ;	usr/main.c:132: float h , s = 1.0 , v = 1.0;                          //色相等分，明度和饱和度均设定为满
      0001D4 E5 08            [12] 1237 	mov	a,_bp
      0001D6 24 07            [12] 1238 	add	a,#0x07
      0001D8 F8               [12] 1239 	mov	r0,a
      0001D9 E4               [12] 1240 	clr	a
      0001DA F6               [12] 1241 	mov	@r0,a
      0001DB 08               [12] 1242 	inc	r0
      0001DC F6               [12] 1243 	mov	@r0,a
      0001DD 08               [12] 1244 	inc	r0
      0001DE 76 80            [12] 1245 	mov	@r0,#0x80
      0001E0 08               [12] 1246 	inc	r0
      0001E1 76 3F            [12] 1247 	mov	@r0,#0x3f
                                   1248 ;	usr/main.c:133: for (j = 0; j < COLUMN_LED; j++)
      0001E3 E5 08            [12] 1249 	mov	a,_bp
      0001E5 24 05            [12] 1250 	add	a,#0x05
      0001E7 F8               [12] 1251 	mov	r0,a
      0001E8 E4               [12] 1252 	clr	a
      0001E9 F6               [12] 1253 	mov	@r0,a
      0001EA 08               [12] 1254 	inc	r0
      0001EB F6               [12] 1255 	mov	@r0,a
      0001EC                       1256 00112$:
                                   1257 ;	usr/main.c:135: h = j*1.0/COLUMN_LED;
      0001EC E5 08            [12] 1258 	mov	a,_bp
      0001EE 24 05            [12] 1259 	add	a,#0x05
      0001F0 F8               [12] 1260 	mov	r0,a
      0001F1 86 82            [24] 1261 	mov	dpl,@r0
      0001F3 08               [12] 1262 	inc	r0
      0001F4 86 83            [24] 1263 	mov	dph,@r0
      0001F6 12 0E F2         [24] 1264 	lcall	___uint2fs
      0001F9 AC 82            [24] 1265 	mov	r4,dpl
      0001FB AD 83            [24] 1266 	mov	r5,dph
      0001FD AE F0            [24] 1267 	mov	r6,b
      0001FF FF               [12] 1268 	mov	r7,a
      000200 E4               [12] 1269 	clr	a
      000201 C0 E0            [24] 1270 	push	acc
      000203 C0 E0            [24] 1271 	push	acc
      000205 74 50            [12] 1272 	mov	a,#0x50
      000207 C0 E0            [24] 1273 	push	acc
      000209 74 41            [12] 1274 	mov	a,#0x41
      00020B C0 E0            [24] 1275 	push	acc
      00020D 8C 82            [24] 1276 	mov	dpl,r4
      00020F 8D 83            [24] 1277 	mov	dph,r5
      000211 8E F0            [24] 1278 	mov	b,r6
      000213 EF               [12] 1279 	mov	a,r7
      000214 12 0F 67         [24] 1280 	lcall	___fsdiv
      000217 AC 82            [24] 1281 	mov	r4,dpl
      000219 AD 83            [24] 1282 	mov	r5,dph
      00021B AE F0            [24] 1283 	mov	r6,b
      00021D FF               [12] 1284 	mov	r7,a
      00021E E5 81            [12] 1285 	mov	a,sp
      000220 24 FC            [12] 1286 	add	a,#0xfc
      000222 F5 81            [12] 1287 	mov	sp,a
                                   1288 ;	usr/main.c:136: h *= 360.0f;                                          //HSV转RGB
      000224 C0 04            [24] 1289 	push	ar4
      000226 C0 05            [24] 1290 	push	ar5
      000228 C0 06            [24] 1291 	push	ar6
      00022A C0 07            [24] 1292 	push	ar7
      00022C 90 00 00         [24] 1293 	mov	dptr,#0x0000
      00022F 75 F0 B4         [24] 1294 	mov	b,#0xb4
      000232 74 43            [12] 1295 	mov	a,#0x43
      000234 12 0D 54         [24] 1296 	lcall	___fsmul
      000237 AC 82            [24] 1297 	mov	r4,dpl
      000239 AD 83            [24] 1298 	mov	r5,dph
      00023B AE F0            [24] 1299 	mov	r6,b
      00023D FF               [12] 1300 	mov	r7,a
      00023E E5 81            [12] 1301 	mov	a,sp
      000240 24 FC            [12] 1302 	add	a,#0xfc
      000242 F5 81            [12] 1303 	mov	sp,a
                                   1304 ;	usr/main.c:141: h = h / 60.0;
      000244 E4               [12] 1305 	clr	a
      000245 C0 E0            [24] 1306 	push	acc
      000247 C0 E0            [24] 1307 	push	acc
      000249 74 70            [12] 1308 	mov	a,#0x70
      00024B C0 E0            [24] 1309 	push	acc
      00024D 74 42            [12] 1310 	mov	a,#0x42
      00024F C0 E0            [24] 1311 	push	acc
      000251 8C 82            [24] 1312 	mov	dpl,r4
      000253 8D 83            [24] 1313 	mov	dph,r5
      000255 8E F0            [24] 1314 	mov	b,r6
      000257 EF               [12] 1315 	mov	a,r7
      000258 12 0F 67         [24] 1316 	lcall	___fsdiv
      00025B A8 08            [24] 1317 	mov	r0,_bp
      00025D 08               [12] 1318 	inc	r0
      00025E A6 82            [24] 1319 	mov	@r0,dpl
      000260 08               [12] 1320 	inc	r0
      000261 A6 83            [24] 1321 	mov	@r0,dph
      000263 08               [12] 1322 	inc	r0
      000264 A6 F0            [24] 1323 	mov	@r0,b
      000266 08               [12] 1324 	inc	r0
      000267 F6               [12] 1325 	mov	@r0,a
      000268 E5 81            [12] 1326 	mov	a,sp
      00026A 24 FC            [12] 1327 	add	a,#0xfc
      00026C F5 81            [12] 1328 	mov	sp,a
                                   1329 ;	usr/main.c:142: int i = (int)h;
      00026E A8 08            [24] 1330 	mov	r0,_bp
      000270 08               [12] 1331 	inc	r0
      000271 86 82            [24] 1332 	mov	dpl,@r0
      000273 08               [12] 1333 	inc	r0
      000274 86 83            [24] 1334 	mov	dph,@r0
      000276 08               [12] 1335 	inc	r0
      000277 86 F0            [24] 1336 	mov	b,@r0
      000279 08               [12] 1337 	inc	r0
      00027A E6               [12] 1338 	mov	a,@r0
      00027B 12 0E B1         [24] 1339 	lcall	___fs2sint
      00027E AF 82            [24] 1340 	mov	r7,dpl
      000280 AE 83            [24] 1341 	mov	r6,dph
      000282 E5 08            [12] 1342 	mov	a,_bp
      000284 24 0B            [12] 1343 	add	a,#0x0b
      000286 F8               [12] 1344 	mov	r0,a
      000287 A6 07            [24] 1345 	mov	@r0,ar7
      000289 08               [12] 1346 	inc	r0
      00028A A6 06            [24] 1347 	mov	@r0,ar6
                                   1348 ;	usr/main.c:143: float C = h - i;
      00028C E5 08            [12] 1349 	mov	a,_bp
      00028E 24 0B            [12] 1350 	add	a,#0x0b
      000290 F8               [12] 1351 	mov	r0,a
      000291 86 82            [24] 1352 	mov	dpl,@r0
      000293 08               [12] 1353 	inc	r0
      000294 86 83            [24] 1354 	mov	dph,@r0
      000296 12 0E E5         [24] 1355 	lcall	___sint2fs
      000299 AA 82            [24] 1356 	mov	r2,dpl
      00029B AB 83            [24] 1357 	mov	r3,dph
      00029D AE F0            [24] 1358 	mov	r6,b
      00029F FF               [12] 1359 	mov	r7,a
      0002A0 C0 02            [24] 1360 	push	ar2
      0002A2 C0 03            [24] 1361 	push	ar3
      0002A4 C0 06            [24] 1362 	push	ar6
      0002A6 C0 07            [24] 1363 	push	ar7
      0002A8 A8 08            [24] 1364 	mov	r0,_bp
      0002AA 08               [12] 1365 	inc	r0
      0002AB 86 82            [24] 1366 	mov	dpl,@r0
      0002AD 08               [12] 1367 	inc	r0
      0002AE 86 83            [24] 1368 	mov	dph,@r0
      0002B0 08               [12] 1369 	inc	r0
      0002B1 86 F0            [24] 1370 	mov	b,@r0
      0002B3 08               [12] 1371 	inc	r0
      0002B4 E6               [12] 1372 	mov	a,@r0
      0002B5 12 0C 6C         [24] 1373 	lcall	___fssub
      0002B8 C8               [12] 1374 	xch	a,r0
      0002B9 E5 08            [12] 1375 	mov	a,_bp
      0002BB 24 0D            [12] 1376 	add	a,#0x0d
      0002BD C8               [12] 1377 	xch	a,r0
      0002BE A6 82            [24] 1378 	mov	@r0,dpl
      0002C0 08               [12] 1379 	inc	r0
      0002C1 A6 83            [24] 1380 	mov	@r0,dph
      0002C3 08               [12] 1381 	inc	r0
      0002C4 A6 F0            [24] 1382 	mov	@r0,b
      0002C6 08               [12] 1383 	inc	r0
      0002C7 F6               [12] 1384 	mov	@r0,a
      0002C8 E5 81            [12] 1385 	mov	a,sp
      0002CA 24 FC            [12] 1386 	add	a,#0xfc
      0002CC F5 81            [12] 1387 	mov	sp,a
                                   1388 ;	usr/main.c:144: v *=255.0;
      0002CE E5 08            [12] 1389 	mov	a,_bp
      0002D0 24 07            [12] 1390 	add	a,#0x07
      0002D2 F8               [12] 1391 	mov	r0,a
      0002D3 E6               [12] 1392 	mov	a,@r0
      0002D4 C0 E0            [24] 1393 	push	acc
      0002D6 08               [12] 1394 	inc	r0
      0002D7 E6               [12] 1395 	mov	a,@r0
      0002D8 C0 E0            [24] 1396 	push	acc
      0002DA 08               [12] 1397 	inc	r0
      0002DB E6               [12] 1398 	mov	a,@r0
      0002DC C0 E0            [24] 1399 	push	acc
      0002DE 08               [12] 1400 	inc	r0
      0002DF E6               [12] 1401 	mov	a,@r0
      0002E0 C0 E0            [24] 1402 	push	acc
      0002E2 90 00 00         [24] 1403 	mov	dptr,#0x0000
      0002E5 75 F0 7F         [24] 1404 	mov	b,#0x7f
      0002E8 74 43            [12] 1405 	mov	a,#0x43
      0002EA 12 0D 54         [24] 1406 	lcall	___fsmul
      0002ED AA 82            [24] 1407 	mov	r2,dpl
      0002EF AB 83            [24] 1408 	mov	r3,dph
      0002F1 AE F0            [24] 1409 	mov	r6,b
      0002F3 FF               [12] 1410 	mov	r7,a
      0002F4 E5 81            [12] 1411 	mov	a,sp
      0002F6 24 FC            [12] 1412 	add	a,#0xfc
      0002F8 F5 81            [12] 1413 	mov	sp,a
      0002FA E5 08            [12] 1414 	mov	a,_bp
      0002FC 24 07            [12] 1415 	add	a,#0x07
      0002FE F8               [12] 1416 	mov	r0,a
      0002FF A6 02            [24] 1417 	mov	@r0,ar2
      000301 08               [12] 1418 	inc	r0
      000302 A6 03            [24] 1419 	mov	@r0,ar3
      000304 08               [12] 1420 	inc	r0
      000305 A6 06            [24] 1421 	mov	@r0,ar6
      000307 08               [12] 1422 	inc	r0
      000308 A6 07            [24] 1423 	mov	@r0,ar7
                                   1424 ;	usr/main.c:146: float Y = v * (1 - s * C) * 255.0;
      00030A E5 08            [12] 1425 	mov	a,_bp
      00030C 24 0D            [12] 1426 	add	a,#0x0d
      00030E F8               [12] 1427 	mov	r0,a
      00030F E6               [12] 1428 	mov	a,@r0
      000310 C0 E0            [24] 1429 	push	acc
      000312 08               [12] 1430 	inc	r0
      000313 E6               [12] 1431 	mov	a,@r0
      000314 C0 E0            [24] 1432 	push	acc
      000316 08               [12] 1433 	inc	r0
      000317 E6               [12] 1434 	mov	a,@r0
      000318 C0 E0            [24] 1435 	push	acc
      00031A 08               [12] 1436 	inc	r0
      00031B E6               [12] 1437 	mov	a,@r0
      00031C C0 E0            [24] 1438 	push	acc
      00031E 90 00 00         [24] 1439 	mov	dptr,#0x0000
      000321 75 F0 80         [24] 1440 	mov	b,#0x80
      000324 74 3F            [12] 1441 	mov	a,#0x3f
      000326 12 0C 6C         [24] 1442 	lcall	___fssub
      000329 A8 08            [24] 1443 	mov	r0,_bp
      00032B 08               [12] 1444 	inc	r0
      00032C A6 82            [24] 1445 	mov	@r0,dpl
      00032E 08               [12] 1446 	inc	r0
      00032F A6 83            [24] 1447 	mov	@r0,dph
      000331 08               [12] 1448 	inc	r0
      000332 A6 F0            [24] 1449 	mov	@r0,b
      000334 08               [12] 1450 	inc	r0
      000335 F6               [12] 1451 	mov	@r0,a
      000336 E5 81            [12] 1452 	mov	a,sp
      000338 24 FC            [12] 1453 	add	a,#0xfc
      00033A F5 81            [12] 1454 	mov	sp,a
      00033C A8 08            [24] 1455 	mov	r0,_bp
      00033E 08               [12] 1456 	inc	r0
      00033F E6               [12] 1457 	mov	a,@r0
      000340 C0 E0            [24] 1458 	push	acc
      000342 08               [12] 1459 	inc	r0
      000343 E6               [12] 1460 	mov	a,@r0
      000344 C0 E0            [24] 1461 	push	acc
      000346 08               [12] 1462 	inc	r0
      000347 E6               [12] 1463 	mov	a,@r0
      000348 C0 E0            [24] 1464 	push	acc
      00034A 08               [12] 1465 	inc	r0
      00034B E6               [12] 1466 	mov	a,@r0
      00034C C0 E0            [24] 1467 	push	acc
      00034E E5 08            [12] 1468 	mov	a,_bp
      000350 24 07            [12] 1469 	add	a,#0x07
      000352 F8               [12] 1470 	mov	r0,a
      000353 86 82            [24] 1471 	mov	dpl,@r0
      000355 08               [12] 1472 	inc	r0
      000356 86 83            [24] 1473 	mov	dph,@r0
      000358 08               [12] 1474 	inc	r0
      000359 86 F0            [24] 1475 	mov	b,@r0
      00035B 08               [12] 1476 	inc	r0
      00035C E6               [12] 1477 	mov	a,@r0
      00035D 12 0D 54         [24] 1478 	lcall	___fsmul
      000360 AA 82            [24] 1479 	mov	r2,dpl
      000362 AB 83            [24] 1480 	mov	r3,dph
      000364 AE F0            [24] 1481 	mov	r6,b
      000366 FF               [12] 1482 	mov	r7,a
      000367 E5 81            [12] 1483 	mov	a,sp
      000369 24 FC            [12] 1484 	add	a,#0xfc
      00036B F5 81            [12] 1485 	mov	sp,a
      00036D C0 02            [24] 1486 	push	ar2
      00036F C0 03            [24] 1487 	push	ar3
      000371 C0 06            [24] 1488 	push	ar6
      000373 C0 07            [24] 1489 	push	ar7
      000375 90 00 00         [24] 1490 	mov	dptr,#0x0000
      000378 75 F0 7F         [24] 1491 	mov	b,#0x7f
      00037B 74 43            [12] 1492 	mov	a,#0x43
      00037D 12 0D 54         [24] 1493 	lcall	___fsmul
      000380 AC 82            [24] 1494 	mov	r4,dpl
      000382 AD 83            [24] 1495 	mov	r5,dph
      000384 AE F0            [24] 1496 	mov	r6,b
      000386 FF               [12] 1497 	mov	r7,a
      000387 E5 81            [12] 1498 	mov	a,sp
      000389 24 FC            [12] 1499 	add	a,#0xfc
      00038B F5 81            [12] 1500 	mov	sp,a
      00038D E5 08            [12] 1501 	mov	a,_bp
      00038F 24 11            [12] 1502 	add	a,#0x11
      000391 F8               [12] 1503 	mov	r0,a
      000392 A6 04            [24] 1504 	mov	@r0,ar4
      000394 08               [12] 1505 	inc	r0
      000395 A6 05            [24] 1506 	mov	@r0,ar5
      000397 08               [12] 1507 	inc	r0
      000398 A6 06            [24] 1508 	mov	@r0,ar6
      00039A 08               [12] 1509 	inc	r0
      00039B A6 07            [24] 1510 	mov	@r0,ar7
                                   1511 ;	usr/main.c:147: float Z = v * (1 - s * (1 - C)) * 255.0;
      00039D A8 08            [24] 1512 	mov	r0,_bp
      00039F 08               [12] 1513 	inc	r0
      0003A0 E6               [12] 1514 	mov	a,@r0
      0003A1 C0 E0            [24] 1515 	push	acc
      0003A3 08               [12] 1516 	inc	r0
      0003A4 E6               [12] 1517 	mov	a,@r0
      0003A5 C0 E0            [24] 1518 	push	acc
      0003A7 08               [12] 1519 	inc	r0
      0003A8 E6               [12] 1520 	mov	a,@r0
      0003A9 C0 E0            [24] 1521 	push	acc
      0003AB 08               [12] 1522 	inc	r0
      0003AC E6               [12] 1523 	mov	a,@r0
      0003AD C0 E0            [24] 1524 	push	acc
      0003AF 90 00 00         [24] 1525 	mov	dptr,#0x0000
      0003B2 75 F0 80         [24] 1526 	mov	b,#0x80
      0003B5 74 3F            [12] 1527 	mov	a,#0x3f
      0003B7 12 0C 6C         [24] 1528 	lcall	___fssub
      0003BA AA 82            [24] 1529 	mov	r2,dpl
      0003BC AB 83            [24] 1530 	mov	r3,dph
      0003BE AE F0            [24] 1531 	mov	r6,b
      0003C0 FF               [12] 1532 	mov	r7,a
      0003C1 E5 81            [12] 1533 	mov	a,sp
      0003C3 24 FC            [12] 1534 	add	a,#0xfc
      0003C5 F5 81            [12] 1535 	mov	sp,a
      0003C7 C0 02            [24] 1536 	push	ar2
      0003C9 C0 03            [24] 1537 	push	ar3
      0003CB C0 06            [24] 1538 	push	ar6
      0003CD C0 07            [24] 1539 	push	ar7
      0003CF E5 08            [12] 1540 	mov	a,_bp
      0003D1 24 07            [12] 1541 	add	a,#0x07
      0003D3 F8               [12] 1542 	mov	r0,a
      0003D4 86 82            [24] 1543 	mov	dpl,@r0
      0003D6 08               [12] 1544 	inc	r0
      0003D7 86 83            [24] 1545 	mov	dph,@r0
      0003D9 08               [12] 1546 	inc	r0
      0003DA 86 F0            [24] 1547 	mov	b,@r0
      0003DC 08               [12] 1548 	inc	r0
      0003DD E6               [12] 1549 	mov	a,@r0
      0003DE 12 0D 54         [24] 1550 	lcall	___fsmul
      0003E1 AC 82            [24] 1551 	mov	r4,dpl
      0003E3 AD 83            [24] 1552 	mov	r5,dph
      0003E5 AE F0            [24] 1553 	mov	r6,b
      0003E7 FF               [12] 1554 	mov	r7,a
      0003E8 E5 81            [12] 1555 	mov	a,sp
      0003EA 24 FC            [12] 1556 	add	a,#0xfc
      0003EC F5 81            [12] 1557 	mov	sp,a
      0003EE C0 04            [24] 1558 	push	ar4
      0003F0 C0 05            [24] 1559 	push	ar5
      0003F2 C0 06            [24] 1560 	push	ar6
      0003F4 C0 07            [24] 1561 	push	ar7
      0003F6 90 00 00         [24] 1562 	mov	dptr,#0x0000
      0003F9 75 F0 7F         [24] 1563 	mov	b,#0x7f
      0003FC 74 43            [12] 1564 	mov	a,#0x43
      0003FE 12 0D 54         [24] 1565 	lcall	___fsmul
      000401 AC 82            [24] 1566 	mov	r4,dpl
      000403 AD 83            [24] 1567 	mov	r5,dph
      000405 AE F0            [24] 1568 	mov	r6,b
      000407 FF               [12] 1569 	mov	r7,a
      000408 E5 81            [12] 1570 	mov	a,sp
      00040A 24 FC            [12] 1571 	add	a,#0xfc
      00040C F5 81            [12] 1572 	mov	sp,a
      00040E E5 08            [12] 1573 	mov	a,_bp
      000410 24 15            [12] 1574 	add	a,#0x15
      000412 F8               [12] 1575 	mov	r0,a
      000413 A6 04            [24] 1576 	mov	@r0,ar4
      000415 08               [12] 1577 	inc	r0
      000416 A6 05            [24] 1578 	mov	@r0,ar5
      000418 08               [12] 1579 	inc	r0
      000419 A6 06            [24] 1580 	mov	@r0,ar6
      00041B 08               [12] 1581 	inc	r0
      00041C A6 07            [24] 1582 	mov	@r0,ar7
                                   1583 ;	usr/main.c:148: switch (i)
      00041E E5 08            [12] 1584 	mov	a,_bp
      000420 24 0B            [12] 1585 	add	a,#0x0b
      000422 F8               [12] 1586 	mov	r0,a
      000423 08               [12] 1587 	inc	r0
      000424 E6               [12] 1588 	mov	a,@r0
      000425 30 E7 03         [24] 1589 	jnb	acc.7,00133$
      000428 02 07 1B         [24] 1590 	ljmp	00113$
      00042B                       1591 00133$:
      00042B E5 08            [12] 1592 	mov	a,_bp
      00042D 24 0B            [12] 1593 	add	a,#0x0b
      00042F F8               [12] 1594 	mov	r0,a
      000430 C3               [12] 1595 	clr	c
      000431 74 05            [12] 1596 	mov	a,#0x05
      000433 96               [12] 1597 	subb	a,@r0
      000434 74 80            [12] 1598 	mov	a,#(0x00 ^ 0x80)
      000436 08               [12] 1599 	inc	r0
      000437 86 F0            [24] 1600 	mov	b,@r0
      000439 63 F0 80         [24] 1601 	xrl	b,#0x80
      00043C 95 F0            [12] 1602 	subb	a,b
      00043E 50 03            [24] 1603 	jnc	00134$
      000440 02 07 1B         [24] 1604 	ljmp	00113$
      000443                       1605 00134$:
      000443 E5 08            [12] 1606 	mov	a,_bp
      000445 24 0B            [12] 1607 	add	a,#0x0b
      000447 F8               [12] 1608 	mov	r0,a
      000448 E6               [12] 1609 	mov	a,@r0
      000449 26               [12] 1610 	add	a,@r0
      00044A 26               [12] 1611 	add	a,@r0
      00044B 90 04 4F         [24] 1612 	mov	dptr,#00135$
      00044E 73               [24] 1613 	jmp	@a+dptr
      00044F                       1614 00135$:
      00044F 02 04 61         [24] 1615 	ljmp	00101$
      000452 02 04 D6         [24] 1616 	ljmp	00102$
      000455 02 05 4B         [24] 1617 	ljmp	00103$
      000458 02 05 C0         [24] 1618 	ljmp	00104$
      00045B 02 06 35         [24] 1619 	ljmp	00105$
      00045E 02 06 A9         [24] 1620 	ljmp	00106$
                                   1621 ;	usr/main.c:150: case 0: RGB_R[j]  = v; RGB_G[j]  = Z; RGB_B[j]  = X; break;
      000461                       1622 00101$:
      000461 E5 08            [12] 1623 	mov	a,_bp
      000463 24 05            [12] 1624 	add	a,#0x05
      000465 F8               [12] 1625 	mov	r0,a
      000466 E6               [12] 1626 	mov	a,@r0
      000467 24 63            [12] 1627 	add	a,#_RGB_R
      000469 FA               [12] 1628 	mov	r2,a
      00046A 08               [12] 1629 	inc	r0
      00046B E6               [12] 1630 	mov	a,@r0
      00046C 34 05            [12] 1631 	addc	a,#(_RGB_R >> 8)
      00046E FB               [12] 1632 	mov	r3,a
      00046F E5 08            [12] 1633 	mov	a,_bp
      000471 24 07            [12] 1634 	add	a,#0x07
      000473 F8               [12] 1635 	mov	r0,a
      000474 86 82            [24] 1636 	mov	dpl,@r0
      000476 08               [12] 1637 	inc	r0
      000477 86 83            [24] 1638 	mov	dph,@r0
      000479 08               [12] 1639 	inc	r0
      00047A 86 F0            [24] 1640 	mov	b,@r0
      00047C 08               [12] 1641 	inc	r0
      00047D E6               [12] 1642 	mov	a,@r0
      00047E C0 03            [24] 1643 	push	ar3
      000480 C0 02            [24] 1644 	push	ar2
      000482 12 10 2A         [24] 1645 	lcall	___fs2uchar
      000485 AF 82            [24] 1646 	mov	r7,dpl
      000487 D0 02            [24] 1647 	pop	ar2
      000489 D0 03            [24] 1648 	pop	ar3
      00048B 8A 82            [24] 1649 	mov	dpl,r2
      00048D 8B 83            [24] 1650 	mov	dph,r3
      00048F EF               [12] 1651 	mov	a,r7
      000490 F0               [24] 1652 	movx	@dptr,a
      000491 E5 08            [12] 1653 	mov	a,_bp
      000493 24 05            [12] 1654 	add	a,#0x05
      000495 F8               [12] 1655 	mov	r0,a
      000496 E6               [12] 1656 	mov	a,@r0
      000497 24 70            [12] 1657 	add	a,#_RGB_G
      000499 FE               [12] 1658 	mov	r6,a
      00049A 08               [12] 1659 	inc	r0
      00049B E6               [12] 1660 	mov	a,@r0
      00049C 34 05            [12] 1661 	addc	a,#(_RGB_G >> 8)
      00049E FF               [12] 1662 	mov	r7,a
      00049F E5 08            [12] 1663 	mov	a,_bp
      0004A1 24 15            [12] 1664 	add	a,#0x15
      0004A3 F8               [12] 1665 	mov	r0,a
      0004A4 86 82            [24] 1666 	mov	dpl,@r0
      0004A6 08               [12] 1667 	inc	r0
      0004A7 86 83            [24] 1668 	mov	dph,@r0
      0004A9 08               [12] 1669 	inc	r0
      0004AA 86 F0            [24] 1670 	mov	b,@r0
      0004AC 08               [12] 1671 	inc	r0
      0004AD E6               [12] 1672 	mov	a,@r0
      0004AE C0 07            [24] 1673 	push	ar7
      0004B0 C0 06            [24] 1674 	push	ar6
      0004B2 12 10 2A         [24] 1675 	lcall	___fs2uchar
      0004B5 AD 82            [24] 1676 	mov	r5,dpl
      0004B7 D0 06            [24] 1677 	pop	ar6
      0004B9 D0 07            [24] 1678 	pop	ar7
      0004BB 8E 82            [24] 1679 	mov	dpl,r6
      0004BD 8F 83            [24] 1680 	mov	dph,r7
      0004BF ED               [12] 1681 	mov	a,r5
      0004C0 F0               [24] 1682 	movx	@dptr,a
      0004C1 E5 08            [12] 1683 	mov	a,_bp
      0004C3 24 05            [12] 1684 	add	a,#0x05
      0004C5 F8               [12] 1685 	mov	r0,a
      0004C6 E6               [12] 1686 	mov	a,@r0
      0004C7 24 7D            [12] 1687 	add	a,#_RGB_B
      0004C9 F5 82            [12] 1688 	mov	dpl,a
      0004CB 08               [12] 1689 	inc	r0
      0004CC E6               [12] 1690 	mov	a,@r0
      0004CD 34 05            [12] 1691 	addc	a,#(_RGB_B >> 8)
      0004CF F5 83            [12] 1692 	mov	dph,a
      0004D1 E4               [12] 1693 	clr	a
      0004D2 F0               [24] 1694 	movx	@dptr,a
      0004D3 02 07 1B         [24] 1695 	ljmp	00113$
                                   1696 ;	usr/main.c:151: case 1: RGB_R[j]  = Y; RGB_G[j]  = v; RGB_B[j]  = X; break;
      0004D6                       1697 00102$:
      0004D6 E5 08            [12] 1698 	mov	a,_bp
      0004D8 24 05            [12] 1699 	add	a,#0x05
      0004DA F8               [12] 1700 	mov	r0,a
      0004DB E6               [12] 1701 	mov	a,@r0
      0004DC 24 63            [12] 1702 	add	a,#_RGB_R
      0004DE FE               [12] 1703 	mov	r6,a
      0004DF 08               [12] 1704 	inc	r0
      0004E0 E6               [12] 1705 	mov	a,@r0
      0004E1 34 05            [12] 1706 	addc	a,#(_RGB_R >> 8)
      0004E3 FF               [12] 1707 	mov	r7,a
      0004E4 E5 08            [12] 1708 	mov	a,_bp
      0004E6 24 11            [12] 1709 	add	a,#0x11
      0004E8 F8               [12] 1710 	mov	r0,a
      0004E9 86 82            [24] 1711 	mov	dpl,@r0
      0004EB 08               [12] 1712 	inc	r0
      0004EC 86 83            [24] 1713 	mov	dph,@r0
      0004EE 08               [12] 1714 	inc	r0
      0004EF 86 F0            [24] 1715 	mov	b,@r0
      0004F1 08               [12] 1716 	inc	r0
      0004F2 E6               [12] 1717 	mov	a,@r0
      0004F3 C0 07            [24] 1718 	push	ar7
      0004F5 C0 06            [24] 1719 	push	ar6
      0004F7 12 10 2A         [24] 1720 	lcall	___fs2uchar
      0004FA AD 82            [24] 1721 	mov	r5,dpl
      0004FC D0 06            [24] 1722 	pop	ar6
      0004FE D0 07            [24] 1723 	pop	ar7
      000500 8E 82            [24] 1724 	mov	dpl,r6
      000502 8F 83            [24] 1725 	mov	dph,r7
      000504 ED               [12] 1726 	mov	a,r5
      000505 F0               [24] 1727 	movx	@dptr,a
      000506 E5 08            [12] 1728 	mov	a,_bp
      000508 24 05            [12] 1729 	add	a,#0x05
      00050A F8               [12] 1730 	mov	r0,a
      00050B E6               [12] 1731 	mov	a,@r0
      00050C 24 70            [12] 1732 	add	a,#_RGB_G
      00050E FE               [12] 1733 	mov	r6,a
      00050F 08               [12] 1734 	inc	r0
      000510 E6               [12] 1735 	mov	a,@r0
      000511 34 05            [12] 1736 	addc	a,#(_RGB_G >> 8)
      000513 FF               [12] 1737 	mov	r7,a
      000514 E5 08            [12] 1738 	mov	a,_bp
      000516 24 07            [12] 1739 	add	a,#0x07
      000518 F8               [12] 1740 	mov	r0,a
      000519 86 82            [24] 1741 	mov	dpl,@r0
      00051B 08               [12] 1742 	inc	r0
      00051C 86 83            [24] 1743 	mov	dph,@r0
      00051E 08               [12] 1744 	inc	r0
      00051F 86 F0            [24] 1745 	mov	b,@r0
      000521 08               [12] 1746 	inc	r0
      000522 E6               [12] 1747 	mov	a,@r0
      000523 C0 07            [24] 1748 	push	ar7
      000525 C0 06            [24] 1749 	push	ar6
      000527 12 10 2A         [24] 1750 	lcall	___fs2uchar
      00052A AD 82            [24] 1751 	mov	r5,dpl
      00052C D0 06            [24] 1752 	pop	ar6
      00052E D0 07            [24] 1753 	pop	ar7
      000530 8E 82            [24] 1754 	mov	dpl,r6
      000532 8F 83            [24] 1755 	mov	dph,r7
      000534 ED               [12] 1756 	mov	a,r5
      000535 F0               [24] 1757 	movx	@dptr,a
      000536 E5 08            [12] 1758 	mov	a,_bp
      000538 24 05            [12] 1759 	add	a,#0x05
      00053A F8               [12] 1760 	mov	r0,a
      00053B E6               [12] 1761 	mov	a,@r0
      00053C 24 7D            [12] 1762 	add	a,#_RGB_B
      00053E F5 82            [12] 1763 	mov	dpl,a
      000540 08               [12] 1764 	inc	r0
      000541 E6               [12] 1765 	mov	a,@r0
      000542 34 05            [12] 1766 	addc	a,#(_RGB_B >> 8)
      000544 F5 83            [12] 1767 	mov	dph,a
      000546 E4               [12] 1768 	clr	a
      000547 F0               [24] 1769 	movx	@dptr,a
      000548 02 07 1B         [24] 1770 	ljmp	00113$
                                   1771 ;	usr/main.c:152: case 2: RGB_R[j]  = X; RGB_G[j]  = v; RGB_B[j]  = Z; break;
      00054B                       1772 00103$:
      00054B E5 08            [12] 1773 	mov	a,_bp
      00054D 24 05            [12] 1774 	add	a,#0x05
      00054F F8               [12] 1775 	mov	r0,a
      000550 E6               [12] 1776 	mov	a,@r0
      000551 24 63            [12] 1777 	add	a,#_RGB_R
      000553 F5 82            [12] 1778 	mov	dpl,a
      000555 08               [12] 1779 	inc	r0
      000556 E6               [12] 1780 	mov	a,@r0
      000557 34 05            [12] 1781 	addc	a,#(_RGB_R >> 8)
      000559 F5 83            [12] 1782 	mov	dph,a
      00055B E4               [12] 1783 	clr	a
      00055C F0               [24] 1784 	movx	@dptr,a
      00055D E5 08            [12] 1785 	mov	a,_bp
      00055F 24 05            [12] 1786 	add	a,#0x05
      000561 F8               [12] 1787 	mov	r0,a
      000562 E6               [12] 1788 	mov	a,@r0
      000563 24 70            [12] 1789 	add	a,#_RGB_G
      000565 FE               [12] 1790 	mov	r6,a
      000566 08               [12] 1791 	inc	r0
      000567 E6               [12] 1792 	mov	a,@r0
      000568 34 05            [12] 1793 	addc	a,#(_RGB_G >> 8)
      00056A FF               [12] 1794 	mov	r7,a
      00056B E5 08            [12] 1795 	mov	a,_bp
      00056D 24 07            [12] 1796 	add	a,#0x07
      00056F F8               [12] 1797 	mov	r0,a
      000570 86 82            [24] 1798 	mov	dpl,@r0
      000572 08               [12] 1799 	inc	r0
      000573 86 83            [24] 1800 	mov	dph,@r0
      000575 08               [12] 1801 	inc	r0
      000576 86 F0            [24] 1802 	mov	b,@r0
      000578 08               [12] 1803 	inc	r0
      000579 E6               [12] 1804 	mov	a,@r0
      00057A C0 07            [24] 1805 	push	ar7
      00057C C0 06            [24] 1806 	push	ar6
      00057E 12 10 2A         [24] 1807 	lcall	___fs2uchar
      000581 AD 82            [24] 1808 	mov	r5,dpl
      000583 D0 06            [24] 1809 	pop	ar6
      000585 D0 07            [24] 1810 	pop	ar7
      000587 8E 82            [24] 1811 	mov	dpl,r6
      000589 8F 83            [24] 1812 	mov	dph,r7
      00058B ED               [12] 1813 	mov	a,r5
      00058C F0               [24] 1814 	movx	@dptr,a
      00058D E5 08            [12] 1815 	mov	a,_bp
      00058F 24 05            [12] 1816 	add	a,#0x05
      000591 F8               [12] 1817 	mov	r0,a
      000592 E6               [12] 1818 	mov	a,@r0
      000593 24 7D            [12] 1819 	add	a,#_RGB_B
      000595 FE               [12] 1820 	mov	r6,a
      000596 08               [12] 1821 	inc	r0
      000597 E6               [12] 1822 	mov	a,@r0
      000598 34 05            [12] 1823 	addc	a,#(_RGB_B >> 8)
      00059A FF               [12] 1824 	mov	r7,a
      00059B E5 08            [12] 1825 	mov	a,_bp
      00059D 24 15            [12] 1826 	add	a,#0x15
      00059F F8               [12] 1827 	mov	r0,a
      0005A0 86 82            [24] 1828 	mov	dpl,@r0
      0005A2 08               [12] 1829 	inc	r0
      0005A3 86 83            [24] 1830 	mov	dph,@r0
      0005A5 08               [12] 1831 	inc	r0
      0005A6 86 F0            [24] 1832 	mov	b,@r0
      0005A8 08               [12] 1833 	inc	r0
      0005A9 E6               [12] 1834 	mov	a,@r0
      0005AA C0 07            [24] 1835 	push	ar7
      0005AC C0 06            [24] 1836 	push	ar6
      0005AE 12 10 2A         [24] 1837 	lcall	___fs2uchar
      0005B1 AD 82            [24] 1838 	mov	r5,dpl
      0005B3 D0 06            [24] 1839 	pop	ar6
      0005B5 D0 07            [24] 1840 	pop	ar7
      0005B7 8E 82            [24] 1841 	mov	dpl,r6
      0005B9 8F 83            [24] 1842 	mov	dph,r7
      0005BB ED               [12] 1843 	mov	a,r5
      0005BC F0               [24] 1844 	movx	@dptr,a
      0005BD 02 07 1B         [24] 1845 	ljmp	00113$
                                   1846 ;	usr/main.c:153: case 3: RGB_R[j]  = X; RGB_G[j]  = Y; RGB_B[j]  = v; break;
      0005C0                       1847 00104$:
      0005C0 E5 08            [12] 1848 	mov	a,_bp
      0005C2 24 05            [12] 1849 	add	a,#0x05
      0005C4 F8               [12] 1850 	mov	r0,a
      0005C5 E6               [12] 1851 	mov	a,@r0
      0005C6 24 63            [12] 1852 	add	a,#_RGB_R
      0005C8 F5 82            [12] 1853 	mov	dpl,a
      0005CA 08               [12] 1854 	inc	r0
      0005CB E6               [12] 1855 	mov	a,@r0
      0005CC 34 05            [12] 1856 	addc	a,#(_RGB_R >> 8)
      0005CE F5 83            [12] 1857 	mov	dph,a
      0005D0 E4               [12] 1858 	clr	a
      0005D1 F0               [24] 1859 	movx	@dptr,a
      0005D2 E5 08            [12] 1860 	mov	a,_bp
      0005D4 24 05            [12] 1861 	add	a,#0x05
      0005D6 F8               [12] 1862 	mov	r0,a
      0005D7 E6               [12] 1863 	mov	a,@r0
      0005D8 24 70            [12] 1864 	add	a,#_RGB_G
      0005DA FE               [12] 1865 	mov	r6,a
      0005DB 08               [12] 1866 	inc	r0
      0005DC E6               [12] 1867 	mov	a,@r0
      0005DD 34 05            [12] 1868 	addc	a,#(_RGB_G >> 8)
      0005DF FF               [12] 1869 	mov	r7,a
      0005E0 E5 08            [12] 1870 	mov	a,_bp
      0005E2 24 11            [12] 1871 	add	a,#0x11
      0005E4 F8               [12] 1872 	mov	r0,a
      0005E5 86 82            [24] 1873 	mov	dpl,@r0
      0005E7 08               [12] 1874 	inc	r0
      0005E8 86 83            [24] 1875 	mov	dph,@r0
      0005EA 08               [12] 1876 	inc	r0
      0005EB 86 F0            [24] 1877 	mov	b,@r0
      0005ED 08               [12] 1878 	inc	r0
      0005EE E6               [12] 1879 	mov	a,@r0
      0005EF C0 07            [24] 1880 	push	ar7
      0005F1 C0 06            [24] 1881 	push	ar6
      0005F3 12 10 2A         [24] 1882 	lcall	___fs2uchar
      0005F6 AD 82            [24] 1883 	mov	r5,dpl
      0005F8 D0 06            [24] 1884 	pop	ar6
      0005FA D0 07            [24] 1885 	pop	ar7
      0005FC 8E 82            [24] 1886 	mov	dpl,r6
      0005FE 8F 83            [24] 1887 	mov	dph,r7
      000600 ED               [12] 1888 	mov	a,r5
      000601 F0               [24] 1889 	movx	@dptr,a
      000602 E5 08            [12] 1890 	mov	a,_bp
      000604 24 05            [12] 1891 	add	a,#0x05
      000606 F8               [12] 1892 	mov	r0,a
      000607 E6               [12] 1893 	mov	a,@r0
      000608 24 7D            [12] 1894 	add	a,#_RGB_B
      00060A FE               [12] 1895 	mov	r6,a
      00060B 08               [12] 1896 	inc	r0
      00060C E6               [12] 1897 	mov	a,@r0
      00060D 34 05            [12] 1898 	addc	a,#(_RGB_B >> 8)
      00060F FF               [12] 1899 	mov	r7,a
      000610 E5 08            [12] 1900 	mov	a,_bp
      000612 24 07            [12] 1901 	add	a,#0x07
      000614 F8               [12] 1902 	mov	r0,a
      000615 86 82            [24] 1903 	mov	dpl,@r0
      000617 08               [12] 1904 	inc	r0
      000618 86 83            [24] 1905 	mov	dph,@r0
      00061A 08               [12] 1906 	inc	r0
      00061B 86 F0            [24] 1907 	mov	b,@r0
      00061D 08               [12] 1908 	inc	r0
      00061E E6               [12] 1909 	mov	a,@r0
      00061F C0 07            [24] 1910 	push	ar7
      000621 C0 06            [24] 1911 	push	ar6
      000623 12 10 2A         [24] 1912 	lcall	___fs2uchar
      000626 AD 82            [24] 1913 	mov	r5,dpl
      000628 D0 06            [24] 1914 	pop	ar6
      00062A D0 07            [24] 1915 	pop	ar7
      00062C 8E 82            [24] 1916 	mov	dpl,r6
      00062E 8F 83            [24] 1917 	mov	dph,r7
      000630 ED               [12] 1918 	mov	a,r5
      000631 F0               [24] 1919 	movx	@dptr,a
      000632 02 07 1B         [24] 1920 	ljmp	00113$
                                   1921 ;	usr/main.c:154: case 4: RGB_R[j]  = Z; RGB_G[j]  = X; RGB_B[j]  = v; break;
      000635                       1922 00105$:
      000635 E5 08            [12] 1923 	mov	a,_bp
      000637 24 05            [12] 1924 	add	a,#0x05
      000639 F8               [12] 1925 	mov	r0,a
      00063A E6               [12] 1926 	mov	a,@r0
      00063B 24 63            [12] 1927 	add	a,#_RGB_R
      00063D FE               [12] 1928 	mov	r6,a
      00063E 08               [12] 1929 	inc	r0
      00063F E6               [12] 1930 	mov	a,@r0
      000640 34 05            [12] 1931 	addc	a,#(_RGB_R >> 8)
      000642 FF               [12] 1932 	mov	r7,a
      000643 E5 08            [12] 1933 	mov	a,_bp
      000645 24 15            [12] 1934 	add	a,#0x15
      000647 F8               [12] 1935 	mov	r0,a
      000648 86 82            [24] 1936 	mov	dpl,@r0
      00064A 08               [12] 1937 	inc	r0
      00064B 86 83            [24] 1938 	mov	dph,@r0
      00064D 08               [12] 1939 	inc	r0
      00064E 86 F0            [24] 1940 	mov	b,@r0
      000650 08               [12] 1941 	inc	r0
      000651 E6               [12] 1942 	mov	a,@r0
      000652 C0 07            [24] 1943 	push	ar7
      000654 C0 06            [24] 1944 	push	ar6
      000656 12 10 2A         [24] 1945 	lcall	___fs2uchar
      000659 AD 82            [24] 1946 	mov	r5,dpl
      00065B D0 06            [24] 1947 	pop	ar6
      00065D D0 07            [24] 1948 	pop	ar7
      00065F 8E 82            [24] 1949 	mov	dpl,r6
      000661 8F 83            [24] 1950 	mov	dph,r7
      000663 ED               [12] 1951 	mov	a,r5
      000664 F0               [24] 1952 	movx	@dptr,a
      000665 E5 08            [12] 1953 	mov	a,_bp
      000667 24 05            [12] 1954 	add	a,#0x05
      000669 F8               [12] 1955 	mov	r0,a
      00066A E6               [12] 1956 	mov	a,@r0
      00066B 24 70            [12] 1957 	add	a,#_RGB_G
      00066D F5 82            [12] 1958 	mov	dpl,a
      00066F 08               [12] 1959 	inc	r0
      000670 E6               [12] 1960 	mov	a,@r0
      000671 34 05            [12] 1961 	addc	a,#(_RGB_G >> 8)
      000673 F5 83            [12] 1962 	mov	dph,a
      000675 E4               [12] 1963 	clr	a
      000676 F0               [24] 1964 	movx	@dptr,a
      000677 E5 08            [12] 1965 	mov	a,_bp
      000679 24 05            [12] 1966 	add	a,#0x05
      00067B F8               [12] 1967 	mov	r0,a
      00067C E6               [12] 1968 	mov	a,@r0
      00067D 24 7D            [12] 1969 	add	a,#_RGB_B
      00067F FE               [12] 1970 	mov	r6,a
      000680 08               [12] 1971 	inc	r0
      000681 E6               [12] 1972 	mov	a,@r0
      000682 34 05            [12] 1973 	addc	a,#(_RGB_B >> 8)
      000684 FF               [12] 1974 	mov	r7,a
      000685 E5 08            [12] 1975 	mov	a,_bp
      000687 24 07            [12] 1976 	add	a,#0x07
      000689 F8               [12] 1977 	mov	r0,a
      00068A 86 82            [24] 1978 	mov	dpl,@r0
      00068C 08               [12] 1979 	inc	r0
      00068D 86 83            [24] 1980 	mov	dph,@r0
      00068F 08               [12] 1981 	inc	r0
      000690 86 F0            [24] 1982 	mov	b,@r0
      000692 08               [12] 1983 	inc	r0
      000693 E6               [12] 1984 	mov	a,@r0
      000694 C0 07            [24] 1985 	push	ar7
      000696 C0 06            [24] 1986 	push	ar6
      000698 12 10 2A         [24] 1987 	lcall	___fs2uchar
      00069B AD 82            [24] 1988 	mov	r5,dpl
      00069D D0 06            [24] 1989 	pop	ar6
      00069F D0 07            [24] 1990 	pop	ar7
      0006A1 8E 82            [24] 1991 	mov	dpl,r6
      0006A3 8F 83            [24] 1992 	mov	dph,r7
      0006A5 ED               [12] 1993 	mov	a,r5
      0006A6 F0               [24] 1994 	movx	@dptr,a
                                   1995 ;	usr/main.c:155: case 5: RGB_R[j]  = v; RGB_G[j]  = X; RGB_B[j]  = Y; break;
      0006A7 80 72            [24] 1996 	sjmp	00113$
      0006A9                       1997 00106$:
      0006A9 E5 08            [12] 1998 	mov	a,_bp
      0006AB 24 05            [12] 1999 	add	a,#0x05
      0006AD F8               [12] 2000 	mov	r0,a
      0006AE E6               [12] 2001 	mov	a,@r0
      0006AF 24 63            [12] 2002 	add	a,#_RGB_R
      0006B1 FE               [12] 2003 	mov	r6,a
      0006B2 08               [12] 2004 	inc	r0
      0006B3 E6               [12] 2005 	mov	a,@r0
      0006B4 34 05            [12] 2006 	addc	a,#(_RGB_R >> 8)
      0006B6 FF               [12] 2007 	mov	r7,a
      0006B7 E5 08            [12] 2008 	mov	a,_bp
      0006B9 24 07            [12] 2009 	add	a,#0x07
      0006BB F8               [12] 2010 	mov	r0,a
      0006BC 86 82            [24] 2011 	mov	dpl,@r0
      0006BE 08               [12] 2012 	inc	r0
      0006BF 86 83            [24] 2013 	mov	dph,@r0
      0006C1 08               [12] 2014 	inc	r0
      0006C2 86 F0            [24] 2015 	mov	b,@r0
      0006C4 08               [12] 2016 	inc	r0
      0006C5 E6               [12] 2017 	mov	a,@r0
      0006C6 C0 07            [24] 2018 	push	ar7
      0006C8 C0 06            [24] 2019 	push	ar6
      0006CA 12 10 2A         [24] 2020 	lcall	___fs2uchar
      0006CD AD 82            [24] 2021 	mov	r5,dpl
      0006CF D0 06            [24] 2022 	pop	ar6
      0006D1 D0 07            [24] 2023 	pop	ar7
      0006D3 8E 82            [24] 2024 	mov	dpl,r6
      0006D5 8F 83            [24] 2025 	mov	dph,r7
      0006D7 ED               [12] 2026 	mov	a,r5
      0006D8 F0               [24] 2027 	movx	@dptr,a
      0006D9 E5 08            [12] 2028 	mov	a,_bp
      0006DB 24 05            [12] 2029 	add	a,#0x05
      0006DD F8               [12] 2030 	mov	r0,a
      0006DE E6               [12] 2031 	mov	a,@r0
      0006DF 24 70            [12] 2032 	add	a,#_RGB_G
      0006E1 F5 82            [12] 2033 	mov	dpl,a
      0006E3 08               [12] 2034 	inc	r0
      0006E4 E6               [12] 2035 	mov	a,@r0
      0006E5 34 05            [12] 2036 	addc	a,#(_RGB_G >> 8)
      0006E7 F5 83            [12] 2037 	mov	dph,a
      0006E9 E4               [12] 2038 	clr	a
      0006EA F0               [24] 2039 	movx	@dptr,a
      0006EB E5 08            [12] 2040 	mov	a,_bp
      0006ED 24 05            [12] 2041 	add	a,#0x05
      0006EF F8               [12] 2042 	mov	r0,a
      0006F0 E6               [12] 2043 	mov	a,@r0
      0006F1 24 7D            [12] 2044 	add	a,#_RGB_B
      0006F3 FE               [12] 2045 	mov	r6,a
      0006F4 08               [12] 2046 	inc	r0
      0006F5 E6               [12] 2047 	mov	a,@r0
      0006F6 34 05            [12] 2048 	addc	a,#(_RGB_B >> 8)
      0006F8 FF               [12] 2049 	mov	r7,a
      0006F9 E5 08            [12] 2050 	mov	a,_bp
      0006FB 24 11            [12] 2051 	add	a,#0x11
      0006FD F8               [12] 2052 	mov	r0,a
      0006FE 86 82            [24] 2053 	mov	dpl,@r0
      000700 08               [12] 2054 	inc	r0
      000701 86 83            [24] 2055 	mov	dph,@r0
      000703 08               [12] 2056 	inc	r0
      000704 86 F0            [24] 2057 	mov	b,@r0
      000706 08               [12] 2058 	inc	r0
      000707 E6               [12] 2059 	mov	a,@r0
      000708 C0 07            [24] 2060 	push	ar7
      00070A C0 06            [24] 2061 	push	ar6
      00070C 12 10 2A         [24] 2062 	lcall	___fs2uchar
      00070F AD 82            [24] 2063 	mov	r5,dpl
      000711 D0 06            [24] 2064 	pop	ar6
      000713 D0 07            [24] 2065 	pop	ar7
      000715 8E 82            [24] 2066 	mov	dpl,r6
      000717 8F 83            [24] 2067 	mov	dph,r7
      000719 ED               [12] 2068 	mov	a,r5
      00071A F0               [24] 2069 	movx	@dptr,a
                                   2070 ;	usr/main.c:156: }
      00071B                       2071 00113$:
                                   2072 ;	usr/main.c:133: for (j = 0; j < COLUMN_LED; j++)
      00071B E5 08            [12] 2073 	mov	a,_bp
      00071D 24 05            [12] 2074 	add	a,#0x05
      00071F F8               [12] 2075 	mov	r0,a
      000720 06               [12] 2076 	inc	@r0
      000721 B6 00 02         [24] 2077 	cjne	@r0,#0x00,00136$
      000724 08               [12] 2078 	inc	r0
      000725 06               [12] 2079 	inc	@r0
      000726                       2080 00136$:
      000726 E5 08            [12] 2081 	mov	a,_bp
      000728 24 05            [12] 2082 	add	a,#0x05
      00072A F8               [12] 2083 	mov	r0,a
      00072B C3               [12] 2084 	clr	c
      00072C E6               [12] 2085 	mov	a,@r0
      00072D 94 0D            [12] 2086 	subb	a,#0x0d
      00072F 08               [12] 2087 	inc	r0
      000730 E6               [12] 2088 	mov	a,@r0
      000731 94 00            [12] 2089 	subb	a,#0x00
      000733 50 03            [24] 2090 	jnc	00137$
      000735 02 01 EC         [24] 2091 	ljmp	00112$
      000738                       2092 00137$:
                                   2093 ;	usr/main.c:159: }
      000738 85 08 81         [24] 2094 	mov	sp,_bp
      00073B D0 08            [24] 2095 	pop	_bp
      00073D 22               [24] 2096 	ret
                                   2097 ;------------------------------------------------------------
                                   2098 ;Allocation info for local variables in function 'Set_Color'
                                   2099 ;------------------------------------------------------------
                                   2100 ;cow                       Allocated to stack - _bp -3
                                   2101 ;column                    Allocated to registers r7 
                                   2102 ;i                         Allocated to stack - _bp +16
                                   2103 ;sloc0                     Allocated to stack - _bp +12
                                   2104 ;sloc1                     Allocated to stack - _bp +4
                                   2105 ;sloc2                     Allocated to stack - _bp +8
                                   2106 ;sloc3                     Allocated to stack - _bp +1
                                   2107 ;sloc4                     Allocated to stack - _bp +14
                                   2108 ;sloc5                     Allocated to stack - _bp +10
                                   2109 ;sloc6                     Allocated to stack - _bp +15
                                   2110 ;sloc7                     Allocated to stack - _bp +3
                                   2111 ;sloc8                     Allocated to stack - _bp +6
                                   2112 ;------------------------------------------------------------
                                   2113 ;	usr/main.c:162: void Set_Color(unsigned char column,unsigned char cow)
                                   2114 ;	-----------------------------------------
                                   2115 ;	 function Set_Color
                                   2116 ;	-----------------------------------------
      00073E                       2117 _Set_Color:
      00073E C0 08            [24] 2118 	push	_bp
      000740 E5 81            [12] 2119 	mov	a,sp
      000742 F5 08            [12] 2120 	mov	_bp,a
      000744 24 10            [12] 2121 	add	a,#0x10
      000746 F5 81            [12] 2122 	mov	sp,a
                                   2123 ;	usr/main.c:165: for(i=0;i<PIXEL;i++)
      000748 E5 82            [12] 2124 	mov	a,dpl
      00074A FF               [12] 2125 	mov	r7,a
      00074B 75 F0 23         [24] 2126 	mov	b,#0x23
      00074E A4               [48] 2127 	mul	ab
      00074F FD               [12] 2128 	mov	r5,a
      000750 AE F0            [24] 2129 	mov	r6,b
      000752 E5 08            [12] 2130 	mov	a,_bp
      000754 24 0E            [12] 2131 	add	a,#0x0e
      000756 F8               [12] 2132 	mov	r0,a
      000757 ED               [12] 2133 	mov	a,r5
      000758 24 01            [12] 2134 	add	a,#_buf_R
      00075A F6               [12] 2135 	mov	@r0,a
      00075B EE               [12] 2136 	mov	a,r6
      00075C 34 00            [12] 2137 	addc	a,#(_buf_R >> 8)
      00075E 08               [12] 2138 	inc	r0
      00075F F6               [12] 2139 	mov	@r0,a
      000760 E5 08            [12] 2140 	mov	a,_bp
      000762 24 0C            [12] 2141 	add	a,#0x0c
      000764 F8               [12] 2142 	mov	r0,a
      000765 EF               [12] 2143 	mov	a,r7
      000766 24 63            [12] 2144 	add	a,#_RGB_R
      000768 F6               [12] 2145 	mov	@r0,a
      000769 E4               [12] 2146 	clr	a
      00076A 34 05            [12] 2147 	addc	a,#(_RGB_R >> 8)
      00076C 08               [12] 2148 	inc	r0
      00076D F6               [12] 2149 	mov	@r0,a
      00076E E5 08            [12] 2150 	mov	a,_bp
      000770 24 04            [12] 2151 	add	a,#0x04
      000772 F8               [12] 2152 	mov	r0,a
      000773 ED               [12] 2153 	mov	a,r5
      000774 24 C8            [12] 2154 	add	a,#_buf_G
      000776 F6               [12] 2155 	mov	@r0,a
      000777 EE               [12] 2156 	mov	a,r6
      000778 34 01            [12] 2157 	addc	a,#(_buf_G >> 8)
      00077A 08               [12] 2158 	inc	r0
      00077B F6               [12] 2159 	mov	@r0,a
      00077C E5 08            [12] 2160 	mov	a,_bp
      00077E 24 08            [12] 2161 	add	a,#0x08
      000780 F8               [12] 2162 	mov	r0,a
      000781 EF               [12] 2163 	mov	a,r7
      000782 24 70            [12] 2164 	add	a,#_RGB_G
      000784 F6               [12] 2165 	mov	@r0,a
      000785 E4               [12] 2166 	clr	a
      000786 34 05            [12] 2167 	addc	a,#(_RGB_G >> 8)
      000788 08               [12] 2168 	inc	r0
      000789 F6               [12] 2169 	mov	@r0,a
      00078A A8 08            [24] 2170 	mov	r0,_bp
      00078C 08               [12] 2171 	inc	r0
      00078D ED               [12] 2172 	mov	a,r5
      00078E 24 8F            [12] 2173 	add	a,#_buf_B
      000790 F6               [12] 2174 	mov	@r0,a
      000791 EE               [12] 2175 	mov	a,r6
      000792 34 03            [12] 2176 	addc	a,#(_buf_B >> 8)
      000794 08               [12] 2177 	inc	r0
      000795 F6               [12] 2178 	mov	@r0,a
      000796 E5 08            [12] 2179 	mov	a,_bp
      000798 24 06            [12] 2180 	add	a,#0x06
      00079A F8               [12] 2181 	mov	r0,a
      00079B EF               [12] 2182 	mov	a,r7
      00079C 24 7D            [12] 2183 	add	a,#_RGB_B
      00079E F6               [12] 2184 	mov	@r0,a
      00079F E4               [12] 2185 	clr	a
      0007A0 34 05            [12] 2186 	addc	a,#(_RGB_B >> 8)
      0007A2 08               [12] 2187 	inc	r0
      0007A3 F6               [12] 2188 	mov	@r0,a
      0007A4 E5 08            [12] 2189 	mov	a,_bp
      0007A6 24 10            [12] 2190 	add	a,#0x10
      0007A8 F8               [12] 2191 	mov	r0,a
      0007A9 76 00            [12] 2192 	mov	@r0,#0x00
      0007AB                       2193 00103$:
                                   2194 ;	usr/main.c:167: buf_R[column][cow+PIXEL-i] = RGB_R[column] / (PIXEL * i + 1);
      0007AB C0 05            [24] 2195 	push	ar5
      0007AD C0 06            [24] 2196 	push	ar6
      0007AF E5 08            [12] 2197 	mov	a,_bp
      0007B1 24 FD            [12] 2198 	add	a,#0xfd
      0007B3 F8               [12] 2199 	mov	r0,a
      0007B4 E5 08            [12] 2200 	mov	a,_bp
      0007B6 24 03            [12] 2201 	add	a,#0x03
      0007B8 F9               [12] 2202 	mov	r1,a
      0007B9 E6               [12] 2203 	mov	a,@r0
      0007BA F7               [12] 2204 	mov	@r1,a
      0007BB E5 08            [12] 2205 	mov	a,_bp
      0007BD 24 03            [12] 2206 	add	a,#0x03
      0007BF F8               [12] 2207 	mov	r0,a
      0007C0 74 04            [12] 2208 	mov	a,#0x04
      0007C2 26               [12] 2209 	add	a,@r0
      0007C3 FE               [12] 2210 	mov	r6,a
      0007C4 E5 08            [12] 2211 	mov	a,_bp
      0007C6 24 10            [12] 2212 	add	a,#0x10
      0007C8 F8               [12] 2213 	mov	r0,a
      0007C9 86 05            [24] 2214 	mov	ar5,@r0
      0007CB EE               [12] 2215 	mov	a,r6
      0007CC C3               [12] 2216 	clr	c
      0007CD 9D               [12] 2217 	subb	a,r5
      0007CE FE               [12] 2218 	mov	r6,a
      0007CF E5 08            [12] 2219 	mov	a,_bp
      0007D1 24 0E            [12] 2220 	add	a,#0x0e
      0007D3 F8               [12] 2221 	mov	r0,a
      0007D4 E5 08            [12] 2222 	mov	a,_bp
      0007D6 24 0A            [12] 2223 	add	a,#0x0a
      0007D8 F9               [12] 2224 	mov	r1,a
      0007D9 EE               [12] 2225 	mov	a,r6
      0007DA 26               [12] 2226 	add	a,@r0
      0007DB F7               [12] 2227 	mov	@r1,a
      0007DC E4               [12] 2228 	clr	a
      0007DD 08               [12] 2229 	inc	r0
      0007DE 36               [12] 2230 	addc	a,@r0
      0007DF 09               [12] 2231 	inc	r1
      0007E0 F7               [12] 2232 	mov	@r1,a
      0007E1 E5 08            [12] 2233 	mov	a,_bp
      0007E3 24 0C            [12] 2234 	add	a,#0x0c
      0007E5 F8               [12] 2235 	mov	r0,a
      0007E6 86 82            [24] 2236 	mov	dpl,@r0
      0007E8 08               [12] 2237 	inc	r0
      0007E9 86 83            [24] 2238 	mov	dph,@r0
      0007EB E0               [24] 2239 	movx	a,@dptr
      0007EC FF               [12] 2240 	mov	r7,a
      0007ED E5 08            [12] 2241 	mov	a,_bp
      0007EF 24 10            [12] 2242 	add	a,#0x10
      0007F1 F8               [12] 2243 	mov	r0,a
      0007F2 86 04            [24] 2244 	mov	ar4,@r0
      0007F4 7D 00            [12] 2245 	mov	r5,#0x00
      0007F6 EC               [12] 2246 	mov	a,r4
      0007F7 2C               [12] 2247 	add	a,r4
      0007F8 FC               [12] 2248 	mov	r4,a
      0007F9 ED               [12] 2249 	mov	a,r5
      0007FA 33               [12] 2250 	rlc	a
      0007FB FD               [12] 2251 	mov	r5,a
      0007FC EC               [12] 2252 	mov	a,r4
      0007FD 2C               [12] 2253 	add	a,r4
      0007FE FC               [12] 2254 	mov	r4,a
      0007FF ED               [12] 2255 	mov	a,r5
      000800 33               [12] 2256 	rlc	a
      000801 FD               [12] 2257 	mov	r5,a
      000802 0C               [12] 2258 	inc	r4
      000803 BC 00 01         [24] 2259 	cjne	r4,#0x00,00123$
      000806 0D               [12] 2260 	inc	r5
      000807                       2261 00123$:
      000807 8F 03            [24] 2262 	mov	ar3,r7
      000809 7F 00            [12] 2263 	mov	r7,#0x00
      00080B C0 06            [24] 2264 	push	ar6
      00080D C0 05            [24] 2265 	push	ar5
      00080F C0 04            [24] 2266 	push	ar4
      000811 C0 04            [24] 2267 	push	ar4
      000813 C0 05            [24] 2268 	push	ar5
      000815 8B 82            [24] 2269 	mov	dpl,r3
      000817 8F 83            [24] 2270 	mov	dph,r7
      000819 12 11 5F         [24] 2271 	lcall	__divsint
      00081C AB 82            [24] 2272 	mov	r3,dpl
      00081E 15 81            [12] 2273 	dec	sp
      000820 15 81            [12] 2274 	dec	sp
      000822 D0 04            [24] 2275 	pop	ar4
      000824 D0 05            [24] 2276 	pop	ar5
      000826 D0 06            [24] 2277 	pop	ar6
      000828 E5 08            [12] 2278 	mov	a,_bp
      00082A 24 0A            [12] 2279 	add	a,#0x0a
      00082C F8               [12] 2280 	mov	r0,a
      00082D 86 82            [24] 2281 	mov	dpl,@r0
      00082F 08               [12] 2282 	inc	r0
      000830 86 83            [24] 2283 	mov	dph,@r0
      000832 EB               [12] 2284 	mov	a,r3
      000833 F0               [24] 2285 	movx	@dptr,a
                                   2286 ;	usr/main.c:168: buf_G[column][cow+PIXEL-i] = RGB_G[column] / (PIXEL * i + 1);
      000834 E5 08            [12] 2287 	mov	a,_bp
      000836 24 04            [12] 2288 	add	a,#0x04
      000838 F8               [12] 2289 	mov	r0,a
      000839 E5 08            [12] 2290 	mov	a,_bp
      00083B 24 0A            [12] 2291 	add	a,#0x0a
      00083D F9               [12] 2292 	mov	r1,a
      00083E EE               [12] 2293 	mov	a,r6
      00083F 26               [12] 2294 	add	a,@r0
      000840 F7               [12] 2295 	mov	@r1,a
      000841 E4               [12] 2296 	clr	a
      000842 08               [12] 2297 	inc	r0
      000843 36               [12] 2298 	addc	a,@r0
      000844 09               [12] 2299 	inc	r1
      000845 F7               [12] 2300 	mov	@r1,a
      000846 E5 08            [12] 2301 	mov	a,_bp
      000848 24 08            [12] 2302 	add	a,#0x08
      00084A F8               [12] 2303 	mov	r0,a
      00084B 86 82            [24] 2304 	mov	dpl,@r0
      00084D 08               [12] 2305 	inc	r0
      00084E 86 83            [24] 2306 	mov	dph,@r0
      000850 E0               [24] 2307 	movx	a,@dptr
      000851 FA               [12] 2308 	mov	r2,a
      000852 7F 00            [12] 2309 	mov	r7,#0x00
      000854 C0 06            [24] 2310 	push	ar6
      000856 C0 05            [24] 2311 	push	ar5
      000858 C0 04            [24] 2312 	push	ar4
      00085A C0 04            [24] 2313 	push	ar4
      00085C C0 05            [24] 2314 	push	ar5
      00085E 8A 82            [24] 2315 	mov	dpl,r2
      000860 8F 83            [24] 2316 	mov	dph,r7
      000862 12 11 5F         [24] 2317 	lcall	__divsint
      000865 AB 82            [24] 2318 	mov	r3,dpl
      000867 15 81            [12] 2319 	dec	sp
      000869 15 81            [12] 2320 	dec	sp
      00086B D0 04            [24] 2321 	pop	ar4
      00086D D0 05            [24] 2322 	pop	ar5
      00086F D0 06            [24] 2323 	pop	ar6
      000871 E5 08            [12] 2324 	mov	a,_bp
      000873 24 0A            [12] 2325 	add	a,#0x0a
      000875 F8               [12] 2326 	mov	r0,a
      000876 86 82            [24] 2327 	mov	dpl,@r0
      000878 08               [12] 2328 	inc	r0
      000879 86 83            [24] 2329 	mov	dph,@r0
      00087B EB               [12] 2330 	mov	a,r3
      00087C F0               [24] 2331 	movx	@dptr,a
                                   2332 ;	usr/main.c:169: buf_B[column][cow+PIXEL-i] = RGB_B[column] / (PIXEL * i + 1);
      00087D A8 08            [24] 2333 	mov	r0,_bp
      00087F 08               [12] 2334 	inc	r0
      000880 EE               [12] 2335 	mov	a,r6
      000881 26               [12] 2336 	add	a,@r0
      000882 FE               [12] 2337 	mov	r6,a
      000883 E4               [12] 2338 	clr	a
      000884 08               [12] 2339 	inc	r0
      000885 36               [12] 2340 	addc	a,@r0
      000886 FF               [12] 2341 	mov	r7,a
      000887 E5 08            [12] 2342 	mov	a,_bp
      000889 24 06            [12] 2343 	add	a,#0x06
      00088B F8               [12] 2344 	mov	r0,a
      00088C 86 82            [24] 2345 	mov	dpl,@r0
      00088E 08               [12] 2346 	inc	r0
      00088F 86 83            [24] 2347 	mov	dph,@r0
      000891 E0               [24] 2348 	movx	a,@dptr
      000892 FB               [12] 2349 	mov	r3,a
      000893 7A 00            [12] 2350 	mov	r2,#0x00
      000895 C0 07            [24] 2351 	push	ar7
      000897 C0 06            [24] 2352 	push	ar6
      000899 C0 04            [24] 2353 	push	ar4
      00089B C0 05            [24] 2354 	push	ar5
      00089D 8B 82            [24] 2355 	mov	dpl,r3
      00089F 8A 83            [24] 2356 	mov	dph,r2
      0008A1 12 11 5F         [24] 2357 	lcall	__divsint
      0008A4 AC 82            [24] 2358 	mov	r4,dpl
      0008A6 15 81            [12] 2359 	dec	sp
      0008A8 15 81            [12] 2360 	dec	sp
      0008AA D0 06            [24] 2361 	pop	ar6
      0008AC D0 07            [24] 2362 	pop	ar7
      0008AE 8E 82            [24] 2363 	mov	dpl,r6
      0008B0 8F 83            [24] 2364 	mov	dph,r7
      0008B2 EC               [12] 2365 	mov	a,r4
      0008B3 F0               [24] 2366 	movx	@dptr,a
                                   2367 ;	usr/main.c:165: for(i=0;i<PIXEL;i++)
      0008B4 E5 08            [12] 2368 	mov	a,_bp
      0008B6 24 10            [12] 2369 	add	a,#0x10
      0008B8 F8               [12] 2370 	mov	r0,a
      0008B9 06               [12] 2371 	inc	@r0
      0008BA E5 08            [12] 2372 	mov	a,_bp
      0008BC 24 10            [12] 2373 	add	a,#0x10
      0008BE F8               [12] 2374 	mov	r0,a
      0008BF B6 04 00         [24] 2375 	cjne	@r0,#0x04,00124$
      0008C2                       2376 00124$:
      0008C2 D0 06            [24] 2377 	pop	ar6
      0008C4 D0 05            [24] 2378 	pop	ar5
      0008C6 50 03            [24] 2379 	jnc	00125$
      0008C8 02 07 AB         [24] 2380 	ljmp	00103$
      0008CB                       2381 00125$:
                                   2382 ;	usr/main.c:171: for(i=1;i<PIXEL;i++)
      0008CB E5 08            [12] 2383 	mov	a,_bp
      0008CD 24 04            [12] 2384 	add	a,#0x04
      0008CF F8               [12] 2385 	mov	r0,a
      0008D0 ED               [12] 2386 	mov	a,r5
      0008D1 24 01            [12] 2387 	add	a,#_buf_R
      0008D3 F6               [12] 2388 	mov	@r0,a
      0008D4 EE               [12] 2389 	mov	a,r6
      0008D5 34 00            [12] 2390 	addc	a,#(_buf_R >> 8)
      0008D7 08               [12] 2391 	inc	r0
      0008D8 F6               [12] 2392 	mov	@r0,a
      0008D9 E5 08            [12] 2393 	mov	a,_bp
      0008DB 24 03            [12] 2394 	add	a,#0x03
      0008DD F8               [12] 2395 	mov	r0,a
      0008DE 74 04            [12] 2396 	mov	a,#0x04
      0008E0 26               [12] 2397 	add	a,@r0
      0008E1 FB               [12] 2398 	mov	r3,a
      0008E2 E5 08            [12] 2399 	mov	a,_bp
      0008E4 24 0A            [12] 2400 	add	a,#0x0a
      0008E6 F8               [12] 2401 	mov	r0,a
      0008E7 ED               [12] 2402 	mov	a,r5
      0008E8 24 C8            [12] 2403 	add	a,#_buf_G
      0008EA F6               [12] 2404 	mov	@r0,a
      0008EB EE               [12] 2405 	mov	a,r6
      0008EC 34 01            [12] 2406 	addc	a,#(_buf_G >> 8)
      0008EE 08               [12] 2407 	inc	r0
      0008EF F6               [12] 2408 	mov	@r0,a
      0008F0 E5 08            [12] 2409 	mov	a,_bp
      0008F2 24 0E            [12] 2410 	add	a,#0x0e
      0008F4 F8               [12] 2411 	mov	r0,a
      0008F5 ED               [12] 2412 	mov	a,r5
      0008F6 24 8F            [12] 2413 	add	a,#_buf_B
      0008F8 F6               [12] 2414 	mov	@r0,a
      0008F9 EE               [12] 2415 	mov	a,r6
      0008FA 34 03            [12] 2416 	addc	a,#(_buf_B >> 8)
      0008FC 08               [12] 2417 	inc	r0
      0008FD F6               [12] 2418 	mov	@r0,a
      0008FE E5 08            [12] 2419 	mov	a,_bp
      000900 24 10            [12] 2420 	add	a,#0x10
      000902 F8               [12] 2421 	mov	r0,a
      000903 76 01            [12] 2422 	mov	@r0,#0x01
      000905                       2423 00105$:
                                   2424 ;	usr/main.c:173: buf_R[column][cow+PIXEL+i] = RGB_R[column] / (PIXEL * i + 1);
      000905 E5 08            [12] 2425 	mov	a,_bp
      000907 24 10            [12] 2426 	add	a,#0x10
      000909 F8               [12] 2427 	mov	r0,a
      00090A E6               [12] 2428 	mov	a,@r0
      00090B 2B               [12] 2429 	add	a,r3
      00090C FA               [12] 2430 	mov	r2,a
      00090D C0 03            [24] 2431 	push	ar3
      00090F E5 08            [12] 2432 	mov	a,_bp
      000911 24 04            [12] 2433 	add	a,#0x04
      000913 F8               [12] 2434 	mov	r0,a
      000914 A9 08            [24] 2435 	mov	r1,_bp
      000916 09               [12] 2436 	inc	r1
      000917 EA               [12] 2437 	mov	a,r2
      000918 26               [12] 2438 	add	a,@r0
      000919 F7               [12] 2439 	mov	@r1,a
      00091A E4               [12] 2440 	clr	a
      00091B 08               [12] 2441 	inc	r0
      00091C 36               [12] 2442 	addc	a,@r0
      00091D 09               [12] 2443 	inc	r1
      00091E F7               [12] 2444 	mov	@r1,a
      00091F E5 08            [12] 2445 	mov	a,_bp
      000921 24 0C            [12] 2446 	add	a,#0x0c
      000923 F8               [12] 2447 	mov	r0,a
      000924 86 82            [24] 2448 	mov	dpl,@r0
      000926 08               [12] 2449 	inc	r0
      000927 86 83            [24] 2450 	mov	dph,@r0
      000929 E0               [24] 2451 	movx	a,@dptr
      00092A FF               [12] 2452 	mov	r7,a
      00092B E5 08            [12] 2453 	mov	a,_bp
      00092D 24 10            [12] 2454 	add	a,#0x10
      00092F F8               [12] 2455 	mov	r0,a
      000930 86 05            [24] 2456 	mov	ar5,@r0
      000932 7E 00            [12] 2457 	mov	r6,#0x00
      000934 ED               [12] 2458 	mov	a,r5
      000935 2D               [12] 2459 	add	a,r5
      000936 FD               [12] 2460 	mov	r5,a
      000937 EE               [12] 2461 	mov	a,r6
      000938 33               [12] 2462 	rlc	a
      000939 FE               [12] 2463 	mov	r6,a
      00093A ED               [12] 2464 	mov	a,r5
      00093B 2D               [12] 2465 	add	a,r5
      00093C FD               [12] 2466 	mov	r5,a
      00093D EE               [12] 2467 	mov	a,r6
      00093E 33               [12] 2468 	rlc	a
      00093F FE               [12] 2469 	mov	r6,a
      000940 0D               [12] 2470 	inc	r5
      000941 BD 00 01         [24] 2471 	cjne	r5,#0x00,00126$
      000944 0E               [12] 2472 	inc	r6
      000945                       2473 00126$:
      000945 8F 03            [24] 2474 	mov	ar3,r7
      000947 7F 00            [12] 2475 	mov	r7,#0x00
      000949 C0 06            [24] 2476 	push	ar6
      00094B C0 05            [24] 2477 	push	ar5
      00094D C0 03            [24] 2478 	push	ar3
      00094F C0 02            [24] 2479 	push	ar2
      000951 C0 05            [24] 2480 	push	ar5
      000953 C0 06            [24] 2481 	push	ar6
      000955 8B 82            [24] 2482 	mov	dpl,r3
      000957 8F 83            [24] 2483 	mov	dph,r7
      000959 12 11 5F         [24] 2484 	lcall	__divsint
      00095C AC 82            [24] 2485 	mov	r4,dpl
      00095E 15 81            [12] 2486 	dec	sp
      000960 15 81            [12] 2487 	dec	sp
      000962 D0 02            [24] 2488 	pop	ar2
      000964 D0 03            [24] 2489 	pop	ar3
      000966 D0 05            [24] 2490 	pop	ar5
      000968 D0 06            [24] 2491 	pop	ar6
      00096A A8 08            [24] 2492 	mov	r0,_bp
      00096C 08               [12] 2493 	inc	r0
      00096D 86 82            [24] 2494 	mov	dpl,@r0
      00096F 08               [12] 2495 	inc	r0
      000970 86 83            [24] 2496 	mov	dph,@r0
      000972 EC               [12] 2497 	mov	a,r4
      000973 F0               [24] 2498 	movx	@dptr,a
                                   2499 ;	usr/main.c:174: buf_G[column][cow+PIXEL+i] = RGB_G[column] / (PIXEL * i + 1);
      000974 E5 08            [12] 2500 	mov	a,_bp
      000976 24 0A            [12] 2501 	add	a,#0x0a
      000978 F8               [12] 2502 	mov	r0,a
      000979 A9 08            [24] 2503 	mov	r1,_bp
      00097B 09               [12] 2504 	inc	r1
      00097C EA               [12] 2505 	mov	a,r2
      00097D 26               [12] 2506 	add	a,@r0
      00097E F7               [12] 2507 	mov	@r1,a
      00097F E4               [12] 2508 	clr	a
      000980 08               [12] 2509 	inc	r0
      000981 36               [12] 2510 	addc	a,@r0
      000982 09               [12] 2511 	inc	r1
      000983 F7               [12] 2512 	mov	@r1,a
      000984 E5 08            [12] 2513 	mov	a,_bp
      000986 24 08            [12] 2514 	add	a,#0x08
      000988 F8               [12] 2515 	mov	r0,a
      000989 86 82            [24] 2516 	mov	dpl,@r0
      00098B 08               [12] 2517 	inc	r0
      00098C 86 83            [24] 2518 	mov	dph,@r0
      00098E E0               [24] 2519 	movx	a,@dptr
      00098F FB               [12] 2520 	mov	r3,a
      000990 7F 00            [12] 2521 	mov	r7,#0x00
      000992 C0 06            [24] 2522 	push	ar6
      000994 C0 05            [24] 2523 	push	ar5
      000996 C0 03            [24] 2524 	push	ar3
      000998 C0 02            [24] 2525 	push	ar2
      00099A C0 05            [24] 2526 	push	ar5
      00099C C0 06            [24] 2527 	push	ar6
      00099E 8B 82            [24] 2528 	mov	dpl,r3
      0009A0 8F 83            [24] 2529 	mov	dph,r7
      0009A2 12 11 5F         [24] 2530 	lcall	__divsint
      0009A5 AC 82            [24] 2531 	mov	r4,dpl
      0009A7 15 81            [12] 2532 	dec	sp
      0009A9 15 81            [12] 2533 	dec	sp
      0009AB D0 02            [24] 2534 	pop	ar2
      0009AD D0 03            [24] 2535 	pop	ar3
      0009AF D0 05            [24] 2536 	pop	ar5
      0009B1 D0 06            [24] 2537 	pop	ar6
      0009B3 A8 08            [24] 2538 	mov	r0,_bp
      0009B5 08               [12] 2539 	inc	r0
      0009B6 86 82            [24] 2540 	mov	dpl,@r0
      0009B8 08               [12] 2541 	inc	r0
      0009B9 86 83            [24] 2542 	mov	dph,@r0
      0009BB EC               [12] 2543 	mov	a,r4
      0009BC F0               [24] 2544 	movx	@dptr,a
                                   2545 ;	usr/main.c:175: buf_B[column][cow+PIXEL+i] = RGB_B[column] / (PIXEL * i + 1);
      0009BD E5 08            [12] 2546 	mov	a,_bp
      0009BF 24 0E            [12] 2547 	add	a,#0x0e
      0009C1 F8               [12] 2548 	mov	r0,a
      0009C2 EA               [12] 2549 	mov	a,r2
      0009C3 26               [12] 2550 	add	a,@r0
      0009C4 FA               [12] 2551 	mov	r2,a
      0009C5 E4               [12] 2552 	clr	a
      0009C6 08               [12] 2553 	inc	r0
      0009C7 36               [12] 2554 	addc	a,@r0
      0009C8 FF               [12] 2555 	mov	r7,a
      0009C9 E5 08            [12] 2556 	mov	a,_bp
      0009CB 24 06            [12] 2557 	add	a,#0x06
      0009CD F8               [12] 2558 	mov	r0,a
      0009CE 86 82            [24] 2559 	mov	dpl,@r0
      0009D0 08               [12] 2560 	inc	r0
      0009D1 86 83            [24] 2561 	mov	dph,@r0
      0009D3 E0               [24] 2562 	movx	a,@dptr
      0009D4 FC               [12] 2563 	mov	r4,a
      0009D5 7B 00            [12] 2564 	mov	r3,#0x00
      0009D7 C0 07            [24] 2565 	push	ar7
      0009D9 C0 03            [24] 2566 	push	ar3
      0009DB C0 02            [24] 2567 	push	ar2
      0009DD C0 05            [24] 2568 	push	ar5
      0009DF C0 06            [24] 2569 	push	ar6
      0009E1 8C 82            [24] 2570 	mov	dpl,r4
      0009E3 8B 83            [24] 2571 	mov	dph,r3
      0009E5 12 11 5F         [24] 2572 	lcall	__divsint
      0009E8 AD 82            [24] 2573 	mov	r5,dpl
      0009EA AE 83            [24] 2574 	mov	r6,dph
      0009EC 15 81            [12] 2575 	dec	sp
      0009EE 15 81            [12] 2576 	dec	sp
      0009F0 D0 02            [24] 2577 	pop	ar2
      0009F2 D0 03            [24] 2578 	pop	ar3
      0009F4 D0 07            [24] 2579 	pop	ar7
      0009F6 8A 82            [24] 2580 	mov	dpl,r2
      0009F8 8F 83            [24] 2581 	mov	dph,r7
      0009FA ED               [12] 2582 	mov	a,r5
      0009FB F0               [24] 2583 	movx	@dptr,a
                                   2584 ;	usr/main.c:171: for(i=1;i<PIXEL;i++)
      0009FC E5 08            [12] 2585 	mov	a,_bp
      0009FE 24 10            [12] 2586 	add	a,#0x10
      000A00 F8               [12] 2587 	mov	r0,a
      000A01 06               [12] 2588 	inc	@r0
      000A02 E5 08            [12] 2589 	mov	a,_bp
      000A04 24 10            [12] 2590 	add	a,#0x10
      000A06 F8               [12] 2591 	mov	r0,a
      000A07 B6 04 00         [24] 2592 	cjne	@r0,#0x04,00127$
      000A0A                       2593 00127$:
      000A0A D0 03            [24] 2594 	pop	ar3
      000A0C 50 03            [24] 2595 	jnc	00128$
      000A0E 02 09 05         [24] 2596 	ljmp	00105$
      000A11                       2597 00128$:
                                   2598 ;	usr/main.c:177: }
      000A11 85 08 81         [24] 2599 	mov	sp,_bp
      000A14 D0 08            [24] 2600 	pop	_bp
      000A16 22               [24] 2601 	ret
                                   2602 ;------------------------------------------------------------
                                   2603 ;Allocation info for local variables in function 'Send_ALL'
                                   2604 ;------------------------------------------------------------
                                   2605 ;i                         Allocated to stack - _bp +4
                                   2606 ;j                         Allocated to registers r7 
                                   2607 ;sloc0                     Allocated to stack - _bp +1
                                   2608 ;sloc1                     Allocated to stack - _bp +3
                                   2609 ;------------------------------------------------------------
                                   2610 ;	usr/main.c:180: void Send_ALL()
                                   2611 ;	-----------------------------------------
                                   2612 ;	 function Send_ALL
                                   2613 ;	-----------------------------------------
      000A17                       2614 _Send_ALL:
      000A17 C0 08            [24] 2615 	push	_bp
      000A19 E5 81            [12] 2616 	mov	a,sp
      000A1B F5 08            [12] 2617 	mov	_bp,a
      000A1D 24 04            [12] 2618 	add	a,#0x04
      000A1F F5 81            [12] 2619 	mov	sp,a
                                   2620 ;	usr/main.c:183: for (j = 0; j < COLUMN_LED; j++)                          
      000A21 7F 00            [12] 2621 	mov	r7,#0x00
                                   2622 ;	usr/main.c:185: for(i=PIXEL*2-1;i<COW-PIXEL*2+1;i++)
      000A23                       2623 00109$:
      000A23 EF               [12] 2624 	mov	a,r7
      000A24 75 F0 23         [24] 2625 	mov	b,#0x23
      000A27 A4               [48] 2626 	mul	ab
      000A28 FD               [12] 2627 	mov	r5,a
      000A29 AE F0            [24] 2628 	mov	r6,b
      000A2B 24 8F            [12] 2629 	add	a,#_buf_B
      000A2D FB               [12] 2630 	mov	r3,a
      000A2E EE               [12] 2631 	mov	a,r6
      000A2F 34 03            [12] 2632 	addc	a,#(_buf_B >> 8)
      000A31 FC               [12] 2633 	mov	r4,a
      000A32 A8 08            [24] 2634 	mov	r0,_bp
      000A34 08               [12] 2635 	inc	r0
      000A35 ED               [12] 2636 	mov	a,r5
      000A36 24 01            [12] 2637 	add	a,#_buf_R
      000A38 F6               [12] 2638 	mov	@r0,a
      000A39 EE               [12] 2639 	mov	a,r6
      000A3A 34 00            [12] 2640 	addc	a,#(_buf_R >> 8)
      000A3C 08               [12] 2641 	inc	r0
      000A3D F6               [12] 2642 	mov	@r0,a
      000A3E ED               [12] 2643 	mov	a,r5
      000A3F 24 C8            [12] 2644 	add	a,#_buf_G
      000A41 FD               [12] 2645 	mov	r5,a
      000A42 EE               [12] 2646 	mov	a,r6
      000A43 34 01            [12] 2647 	addc	a,#(_buf_G >> 8)
      000A45 FE               [12] 2648 	mov	r6,a
      000A46 E5 08            [12] 2649 	mov	a,_bp
      000A48 24 04            [12] 2650 	add	a,#0x04
      000A4A F8               [12] 2651 	mov	r0,a
      000A4B 76 07            [12] 2652 	mov	@r0,#0x07
      000A4D                       2653 00103$:
                                   2654 ;	usr/main.c:187: Send_2811_24bits(buf_G[j][i],buf_R[j][i],buf_B[j][i]);
      000A4D C0 07            [24] 2655 	push	ar7
      000A4F E5 08            [12] 2656 	mov	a,_bp
      000A51 24 04            [12] 2657 	add	a,#0x04
      000A53 F8               [12] 2658 	mov	r0,a
      000A54 E6               [12] 2659 	mov	a,@r0
      000A55 2B               [12] 2660 	add	a,r3
      000A56 F5 82            [12] 2661 	mov	dpl,a
      000A58 E4               [12] 2662 	clr	a
      000A59 3C               [12] 2663 	addc	a,r4
      000A5A F5 83            [12] 2664 	mov	dph,a
      000A5C E0               [24] 2665 	movx	a,@dptr
      000A5D FF               [12] 2666 	mov	r7,a
      000A5E A8 08            [24] 2667 	mov	r0,_bp
      000A60 08               [12] 2668 	inc	r0
      000A61 E5 08            [12] 2669 	mov	a,_bp
      000A63 24 04            [12] 2670 	add	a,#0x04
      000A65 F9               [12] 2671 	mov	r1,a
      000A66 E7               [12] 2672 	mov	a,@r1
      000A67 26               [12] 2673 	add	a,@r0
      000A68 F5 82            [12] 2674 	mov	dpl,a
      000A6A E4               [12] 2675 	clr	a
      000A6B 08               [12] 2676 	inc	r0
      000A6C 36               [12] 2677 	addc	a,@r0
      000A6D F5 83            [12] 2678 	mov	dph,a
      000A6F E5 08            [12] 2679 	mov	a,_bp
      000A71 24 03            [12] 2680 	add	a,#0x03
      000A73 F8               [12] 2681 	mov	r0,a
      000A74 E0               [24] 2682 	movx	a,@dptr
      000A75 F6               [12] 2683 	mov	@r0,a
      000A76 E5 08            [12] 2684 	mov	a,_bp
      000A78 24 04            [12] 2685 	add	a,#0x04
      000A7A F8               [12] 2686 	mov	r0,a
      000A7B E6               [12] 2687 	mov	a,@r0
      000A7C 2D               [12] 2688 	add	a,r5
      000A7D F5 82            [12] 2689 	mov	dpl,a
      000A7F E4               [12] 2690 	clr	a
      000A80 3E               [12] 2691 	addc	a,r6
      000A81 F5 83            [12] 2692 	mov	dph,a
      000A83 E0               [24] 2693 	movx	a,@dptr
      000A84 FA               [12] 2694 	mov	r2,a
      000A85 C0 07            [24] 2695 	push	ar7
      000A87 C0 06            [24] 2696 	push	ar6
      000A89 C0 05            [24] 2697 	push	ar5
      000A8B C0 04            [24] 2698 	push	ar4
      000A8D C0 03            [24] 2699 	push	ar3
      000A8F C0 07            [24] 2700 	push	ar7
      000A91 E5 08            [12] 2701 	mov	a,_bp
      000A93 24 03            [12] 2702 	add	a,#0x03
      000A95 F8               [12] 2703 	mov	r0,a
      000A96 E6               [12] 2704 	mov	a,@r0
      000A97 C0 E0            [24] 2705 	push	acc
      000A99 8A 82            [24] 2706 	mov	dpl,r2
      000A9B 12 00 29         [24] 2707 	lcall	_Send_2811_24bits
      000A9E 15 81            [12] 2708 	dec	sp
      000AA0 15 81            [12] 2709 	dec	sp
      000AA2 D0 03            [24] 2710 	pop	ar3
      000AA4 D0 04            [24] 2711 	pop	ar4
      000AA6 D0 05            [24] 2712 	pop	ar5
      000AA8 D0 06            [24] 2713 	pop	ar6
      000AAA D0 07            [24] 2714 	pop	ar7
                                   2715 ;	usr/main.c:185: for(i=PIXEL*2-1;i<COW-PIXEL*2+1;i++)
      000AAC E5 08            [12] 2716 	mov	a,_bp
      000AAE 24 04            [12] 2717 	add	a,#0x04
      000AB0 F8               [12] 2718 	mov	r0,a
      000AB1 06               [12] 2719 	inc	@r0
      000AB2 E5 08            [12] 2720 	mov	a,_bp
      000AB4 24 04            [12] 2721 	add	a,#0x04
      000AB6 F8               [12] 2722 	mov	r0,a
      000AB7 B6 1C 00         [24] 2723 	cjne	@r0,#0x1c,00127$
      000ABA                       2724 00127$:
      000ABA D0 07            [24] 2725 	pop	ar7
      000ABC 40 8F            [24] 2726 	jc	00103$
                                   2727 ;	usr/main.c:183: for (j = 0; j < COLUMN_LED; j++)                          
      000ABE 0F               [12] 2728 	inc	r7
      000ABF BF 0D 00         [24] 2729 	cjne	r7,#0x0d,00129$
      000AC2                       2730 00129$:
      000AC2 50 03            [24] 2731 	jnc	00130$
      000AC4 02 0A 23         [24] 2732 	ljmp	00109$
      000AC7                       2733 00130$:
                                   2734 ;	usr/main.c:190: mDelaymS(50);
      000AC7 90 00 32         [24] 2735 	mov	dptr,#0x0032
      000ACA 12 0B FA         [24] 2736 	lcall	_mDelaymS
                                   2737 ;	usr/main.c:191: }
      000ACD 85 08 81         [24] 2738 	mov	sp,_bp
      000AD0 D0 08            [24] 2739 	pop	_bp
      000AD2 22               [24] 2740 	ret
                                   2741 ;------------------------------------------------------------
                                   2742 ;Allocation info for local variables in function 'main'
                                   2743 ;------------------------------------------------------------
                                   2744 ;m                         Allocated to registers 
                                   2745 ;i                         Allocated to registers r6 r7 
                                   2746 ;j                         Allocated to registers r6 r7 
                                   2747 ;------------------------------------------------------------
                                   2748 ;	usr/main.c:199: void main(void)
                                   2749 ;	-----------------------------------------
                                   2750 ;	 function main
                                   2751 ;	-----------------------------------------
      000AD3                       2752 _main:
                                   2753 ;	usr/main.c:202: CfgFsys();                        //CH549时钟选择配置
      000AD3 12 0B A6         [24] 2754 	lcall	_CfgFsys
                                   2755 ;	usr/main.c:203: mDelaymS(20);
      000AD6 90 00 14         [24] 2756 	mov	dptr,#0x0014
      000AD9 12 0B FA         [24] 2757 	lcall	_mDelaymS
                                   2758 ;	usr/main.c:204: genColor();                       //颜色预设
      000ADC 12 01 CA         [24] 2759 	lcall	_genColor
                                   2760 ;	usr/main.c:205: for ( j = 0; j < COLUMN_LED; j++)     //所有列的流水灯进入就绪状态
      000ADF 7E 00            [12] 2761 	mov	r6,#0x00
      000AE1 7F 00            [12] 2762 	mov	r7,#0x00
      000AE3                       2763 00113$:
                                   2764 ;	usr/main.c:207: Column[j] = COW_LED+PIXEL*2-1;
      000AE3 EE               [12] 2765 	mov	a,r6
      000AE4 24 56            [12] 2766 	add	a,#_Column
      000AE6 F5 82            [12] 2767 	mov	dpl,a
      000AE8 EF               [12] 2768 	mov	a,r7
      000AE9 34 05            [12] 2769 	addc	a,#(_Column >> 8)
      000AEB F5 83            [12] 2770 	mov	dph,a
      000AED 74 1C            [12] 2771 	mov	a,#0x1c
      000AEF F0               [24] 2772 	movx	@dptr,a
                                   2773 ;	usr/main.c:205: for ( j = 0; j < COLUMN_LED; j++)     //所有列的流水灯进入就绪状态
      000AF0 0E               [12] 2774 	inc	r6
      000AF1 BE 00 01         [24] 2775 	cjne	r6,#0x00,00149$
      000AF4 0F               [12] 2776 	inc	r7
      000AF5                       2777 00149$:
      000AF5 C3               [12] 2778 	clr	c
      000AF6 EE               [12] 2779 	mov	a,r6
      000AF7 94 0D            [12] 2780 	subb	a,#0x0d
      000AF9 EF               [12] 2781 	mov	a,r7
      000AFA 94 00            [12] 2782 	subb	a,#0x00
      000AFC 40 E5            [24] 2783 	jc	00113$
                                   2784 ;	usr/main.c:211: while (1)
      000AFE                       2785 00111$:
                                   2786 ;	usr/main.c:213: Buf_Put0();                    //数据条清0
      000AFE 12 01 7C         [24] 2787 	lcall	_Buf_Put0
                                   2788 ;	usr/main.c:214: m = rand() % COLUMN_LED;       //取随机数
      000B01 12 0C 77         [24] 2789 	lcall	_rand
      000B04 AE 82            [24] 2790 	mov	r6,dpl
      000B06 AF 83            [24] 2791 	mov	r7,dph
      000B08 74 0D            [12] 2792 	mov	a,#0x0d
      000B0A C0 E0            [24] 2793 	push	acc
      000B0C E4               [12] 2794 	clr	a
      000B0D C0 E0            [24] 2795 	push	acc
      000B0F 8E 82            [24] 2796 	mov	dpl,r6
      000B11 8F 83            [24] 2797 	mov	dph,r7
      000B13 12 10 32         [24] 2798 	lcall	__modsint
      000B16 AE 82            [24] 2799 	mov	r6,dpl
      000B18 AF 83            [24] 2800 	mov	r7,dph
      000B1A 15 81            [12] 2801 	dec	sp
      000B1C 15 81            [12] 2802 	dec	sp
                                   2803 ;	usr/main.c:215: if (Column[m] == COW_LED+PIXEL*2-1) {Column[m] = 0;}       //选中的列若以就绪开启流水灯
      000B1E EE               [12] 2804 	mov	a,r6
      000B1F 24 56            [12] 2805 	add	a,#_Column
      000B21 FE               [12] 2806 	mov	r6,a
      000B22 EF               [12] 2807 	mov	a,r7
      000B23 34 05            [12] 2808 	addc	a,#(_Column >> 8)
      000B25 FF               [12] 2809 	mov	r7,a
      000B26 8E 82            [24] 2810 	mov	dpl,r6
      000B28 8F 83            [24] 2811 	mov	dph,r7
      000B2A E0               [24] 2812 	movx	a,@dptr
      000B2B FD               [12] 2813 	mov	r5,a
      000B2C BD 1C 06         [24] 2814 	cjne	r5,#0x1c,00123$
      000B2F 8E 82            [24] 2815 	mov	dpl,r6
      000B31 8F 83            [24] 2816 	mov	dph,r7
      000B33 E4               [12] 2817 	clr	a
      000B34 F0               [24] 2818 	movx	@dptr,a
                                   2819 ;	usr/main.c:216: for ( i = 0; i < COLUMN_LED; i++)                          //检测每一列的状态
      000B35                       2820 00123$:
      000B35 7E 00            [12] 2821 	mov	r6,#0x00
      000B37 7F 00            [12] 2822 	mov	r7,#0x00
      000B39                       2823 00115$:
                                   2824 ;	usr/main.c:218: if (Column[i] < COW_LED+PIXEL*2-1)                     //若列数据未走完给该列赋值
      000B39 EE               [12] 2825 	mov	a,r6
      000B3A 24 56            [12] 2826 	add	a,#_Column
      000B3C F5 82            [12] 2827 	mov	dpl,a
      000B3E EF               [12] 2828 	mov	a,r7
      000B3F 34 05            [12] 2829 	addc	a,#(_Column >> 8)
      000B41 F5 83            [12] 2830 	mov	dph,a
      000B43 E0               [24] 2831 	movx	a,@dptr
      000B44 FD               [12] 2832 	mov	r5,a
      000B45 BD 1C 00         [24] 2833 	cjne	r5,#0x1c,00153$
      000B48                       2834 00153$:
      000B48 50 48            [24] 2835 	jnc	00116$
                                   2836 ;	usr/main.c:220: if (i % 2)                                         //判断单双列
      000B4A EE               [12] 2837 	mov	a,r6
      000B4B 30 E0 1C         [24] 2838 	jnb	acc.0,00105$
                                   2839 ;	usr/main.c:222: Set_Color(i,COW_LED+PIXEL*2-1-Column[i]);      //双列
      000B4E 8D 04            [24] 2840 	mov	ar4,r5
      000B50 74 1C            [12] 2841 	mov	a,#0x1c
      000B52 C3               [12] 2842 	clr	c
      000B53 9C               [12] 2843 	subb	a,r4
      000B54 FC               [12] 2844 	mov	r4,a
      000B55 8E 03            [24] 2845 	mov	ar3,r6
      000B57 C0 07            [24] 2846 	push	ar7
      000B59 C0 06            [24] 2847 	push	ar6
      000B5B C0 04            [24] 2848 	push	ar4
      000B5D 8B 82            [24] 2849 	mov	dpl,r3
      000B5F 12 07 3E         [24] 2850 	lcall	_Set_Color
      000B62 15 81            [12] 2851 	dec	sp
      000B64 D0 06            [24] 2852 	pop	ar6
      000B66 D0 07            [24] 2853 	pop	ar7
      000B68 80 13            [24] 2854 	sjmp	00106$
      000B6A                       2855 00105$:
                                   2856 ;	usr/main.c:226: Set_Color(i,Column[i]);                        //单列
      000B6A 8E 04            [24] 2857 	mov	ar4,r6
      000B6C C0 07            [24] 2858 	push	ar7
      000B6E C0 06            [24] 2859 	push	ar6
      000B70 C0 05            [24] 2860 	push	ar5
      000B72 8C 82            [24] 2861 	mov	dpl,r4
      000B74 12 07 3E         [24] 2862 	lcall	_Set_Color
      000B77 15 81            [12] 2863 	dec	sp
      000B79 D0 06            [24] 2864 	pop	ar6
      000B7B D0 07            [24] 2865 	pop	ar7
      000B7D                       2866 00106$:
                                   2867 ;	usr/main.c:228: Column[i]++;                                        //下次循环走下一个瞬间的数据
      000B7D EE               [12] 2868 	mov	a,r6
      000B7E 24 56            [12] 2869 	add	a,#_Column
      000B80 FC               [12] 2870 	mov	r4,a
      000B81 EF               [12] 2871 	mov	a,r7
      000B82 34 05            [12] 2872 	addc	a,#(_Column >> 8)
      000B84 FD               [12] 2873 	mov	r5,a
      000B85 8C 82            [24] 2874 	mov	dpl,r4
      000B87 8D 83            [24] 2875 	mov	dph,r5
      000B89 E0               [24] 2876 	movx	a,@dptr
      000B8A FB               [12] 2877 	mov	r3,a
      000B8B 0B               [12] 2878 	inc	r3
      000B8C 8C 82            [24] 2879 	mov	dpl,r4
      000B8E 8D 83            [24] 2880 	mov	dph,r5
      000B90 EB               [12] 2881 	mov	a,r3
      000B91 F0               [24] 2882 	movx	@dptr,a
      000B92                       2883 00116$:
                                   2884 ;	usr/main.c:216: for ( i = 0; i < COLUMN_LED; i++)                          //检测每一列的状态
      000B92 0E               [12] 2885 	inc	r6
      000B93 BE 00 01         [24] 2886 	cjne	r6,#0x00,00156$
      000B96 0F               [12] 2887 	inc	r7
      000B97                       2888 00156$:
      000B97 C3               [12] 2889 	clr	c
      000B98 EE               [12] 2890 	mov	a,r6
      000B99 94 0D            [12] 2891 	subb	a,#0x0d
      000B9B EF               [12] 2892 	mov	a,r7
      000B9C 94 00            [12] 2893 	subb	a,#0x00
      000B9E 40 99            [24] 2894 	jc	00115$
                                   2895 ;	usr/main.c:233: Send_ALL();                                                //将该时间数据发送显示
      000BA0 12 0A 17         [24] 2896 	lcall	_Send_ALL
                                   2897 ;	usr/main.c:235: }
      000BA3 02 0A FE         [24] 2898 	ljmp	00111$
                                   2899 	.area CSEG    (CODE)
                                   2900 	.area CONST   (CODE)
                                   2901 	.area CABS    (ABS,CODE)
