;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.0.0 #11528 (MINGW64)
;--------------------------------------------------------
	.module main
	.optsdcc -mmcs51 --model-large
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _main
	.globl _Send_ALL
	.globl _Set_Color
	.globl _genColor
	.globl _Buf_Put0
	.globl _RGB_Rst
	.globl _Send_2811_24bits
	.globl _rand
	.globl _mDelaymS
	.globl _mDelayuS
	.globl _CfgFsys
	.globl _UIF_BUS_RST
	.globl _UIF_DETECT
	.globl _UIF_TRANSFER
	.globl _UIF_SUSPEND
	.globl _UIF_HST_SOF
	.globl _UIF_FIFO_OV
	.globl _U_SIE_FREE
	.globl _U_TOG_OK
	.globl _U_IS_NAK
	.globl _S0_R_FIFO
	.globl _S0_T_FIFO
	.globl _S0_FREE
	.globl _S0_IF_BYTE
	.globl _S0_IF_FIRST
	.globl _S0_IF_OV
	.globl _S0_FST_ACT
	.globl _CP_RL2
	.globl _C_T2
	.globl _TR2
	.globl _EXEN2
	.globl _TCLK
	.globl _RCLK
	.globl _EXF2
	.globl _CAP1F
	.globl _TF2
	.globl _RI
	.globl _TI
	.globl _RB8
	.globl _TB8
	.globl _REN
	.globl _SM2
	.globl _SM1
	.globl _SM0
	.globl _IT0
	.globl _IE0
	.globl _IT1
	.globl _IE1
	.globl _TR0
	.globl _TF0
	.globl _TR1
	.globl _TF1
	.globl _XI
	.globl _XO
	.globl _P4_0
	.globl _P4_1
	.globl _P4_2
	.globl _P4_3
	.globl _P4_4
	.globl _P4_5
	.globl _P4_6
	.globl _RXD
	.globl _TXD
	.globl _INT0
	.globl _INT1
	.globl _T0
	.globl _T1
	.globl _CAP0
	.globl _INT3
	.globl _P3_0
	.globl _P3_1
	.globl _P3_2
	.globl _P3_3
	.globl _P3_4
	.globl _P3_5
	.globl _P3_6
	.globl _P3_7
	.globl _PWM5
	.globl _PWM4
	.globl _INT0_
	.globl _PWM3
	.globl _PWM2
	.globl _CAP1_
	.globl _T2_
	.globl _PWM1
	.globl _CAP2_
	.globl _T2EX_
	.globl _PWM0
	.globl _RXD1
	.globl _PWM6
	.globl _TXD1
	.globl _PWM7
	.globl _P2_0
	.globl _P2_1
	.globl _P2_2
	.globl _P2_3
	.globl _P2_4
	.globl _P2_5
	.globl _P2_6
	.globl _P2_7
	.globl _AIN0
	.globl _CAP1
	.globl _T2
	.globl _AIN1
	.globl _CAP2
	.globl _T2EX
	.globl _AIN2
	.globl _AIN3
	.globl _AIN4
	.globl _UCC1
	.globl _SCS
	.globl _AIN5
	.globl _UCC2
	.globl _PWM0_
	.globl _MOSI
	.globl _AIN6
	.globl _VBUS
	.globl _RXD1_
	.globl _MISO
	.globl _AIN7
	.globl _TXD1_
	.globl _SCK
	.globl _P1_0
	.globl _P1_1
	.globl _P1_2
	.globl _P1_3
	.globl _P1_4
	.globl _P1_5
	.globl _P1_6
	.globl _P1_7
	.globl _AIN8
	.globl _AIN9
	.globl _AIN10
	.globl _RXD_
	.globl _AIN11
	.globl _TXD_
	.globl _AIN12
	.globl _RXD2
	.globl _AIN13
	.globl _TXD2
	.globl _AIN14
	.globl _RXD3
	.globl _AIN15
	.globl _TXD3
	.globl _P0_0
	.globl _P0_1
	.globl _P0_2
	.globl _P0_3
	.globl _P0_4
	.globl _P0_5
	.globl _P0_6
	.globl _P0_7
	.globl _IE_SPI0
	.globl _IE_INT3
	.globl _IE_USB
	.globl _IE_UART2
	.globl _IE_ADC
	.globl _IE_UART1
	.globl _IE_UART3
	.globl _IE_PWMX
	.globl _IE_GPIO
	.globl _IE_WDOG
	.globl _PX0
	.globl _PT0
	.globl _PX1
	.globl _PT1
	.globl _PS
	.globl _PT2
	.globl _PL_FLAG
	.globl _PH_FLAG
	.globl _EX0
	.globl _ET0
	.globl _EX1
	.globl _ET1
	.globl _ES
	.globl _ET2
	.globl _E_DIS
	.globl _EA
	.globl _P
	.globl _F1
	.globl _OV
	.globl _RS0
	.globl _RS1
	.globl _F0
	.globl _AC
	.globl _CY
	.globl _UEP1_DMA_H
	.globl _UEP1_DMA_L
	.globl _UEP1_DMA
	.globl _UEP0_DMA_H
	.globl _UEP0_DMA_L
	.globl _UEP0_DMA
	.globl _UEP2_3_MOD
	.globl _UEP4_1_MOD
	.globl _UEP3_DMA_H
	.globl _UEP3_DMA_L
	.globl _UEP3_DMA
	.globl _UEP2_DMA_H
	.globl _UEP2_DMA_L
	.globl _UEP2_DMA
	.globl _USB_DEV_AD
	.globl _USB_CTRL
	.globl _USB_INT_EN
	.globl _UEP4_T_LEN
	.globl _UEP4_CTRL
	.globl _UEP0_T_LEN
	.globl _UEP0_CTRL
	.globl _USB_RX_LEN
	.globl _USB_MIS_ST
	.globl _USB_INT_ST
	.globl _USB_INT_FG
	.globl _UEP3_T_LEN
	.globl _UEP3_CTRL
	.globl _UEP2_T_LEN
	.globl _UEP2_CTRL
	.globl _UEP1_T_LEN
	.globl _UEP1_CTRL
	.globl _UDEV_CTRL
	.globl _USB_C_CTRL
	.globl _ADC_PIN
	.globl _ADC_CHAN
	.globl _ADC_DAT_H
	.globl _ADC_DAT_L
	.globl _ADC_DAT
	.globl _ADC_CFG
	.globl _ADC_CTRL
	.globl _TKEY_CTRL
	.globl _SIF3
	.globl _SBAUD3
	.globl _SBUF3
	.globl _SCON3
	.globl _SIF2
	.globl _SBAUD2
	.globl _SBUF2
	.globl _SCON2
	.globl _SIF1
	.globl _SBAUD1
	.globl _SBUF1
	.globl _SCON1
	.globl _SPI0_SETUP
	.globl _SPI0_CK_SE
	.globl _SPI0_CTRL
	.globl _SPI0_DATA
	.globl _SPI0_STAT
	.globl _PWM_DATA7
	.globl _PWM_DATA6
	.globl _PWM_DATA5
	.globl _PWM_DATA4
	.globl _PWM_DATA3
	.globl _PWM_CTRL2
	.globl _PWM_CK_SE
	.globl _PWM_CTRL
	.globl _PWM_DATA0
	.globl _PWM_DATA1
	.globl _PWM_DATA2
	.globl _T2CAP1H
	.globl _T2CAP1L
	.globl _T2CAP1
	.globl _TH2
	.globl _TL2
	.globl _T2COUNT
	.globl _RCAP2H
	.globl _RCAP2L
	.globl _RCAP2
	.globl _T2MOD
	.globl _T2CON
	.globl _T2CAP0H
	.globl _T2CAP0L
	.globl _T2CAP0
	.globl _T2CON2
	.globl _SBUF
	.globl _SCON
	.globl _TH1
	.globl _TH0
	.globl _TL1
	.globl _TL0
	.globl _TMOD
	.globl _TCON
	.globl _XBUS_AUX
	.globl _PIN_FUNC
	.globl _P5
	.globl _P4_DIR_PU
	.globl _P4_MOD_OC
	.globl _P4
	.globl _P3_DIR_PU
	.globl _P3_MOD_OC
	.globl _P3
	.globl _P2_DIR_PU
	.globl _P2_MOD_OC
	.globl _P2
	.globl _P1_DIR_PU
	.globl _P1_MOD_OC
	.globl _P1
	.globl _P0_DIR_PU
	.globl _P0_MOD_OC
	.globl _P0
	.globl _ROM_CTRL
	.globl _ROM_DATA_HH
	.globl _ROM_DATA_HL
	.globl _ROM_DATA_HI
	.globl _ROM_ADDR_H
	.globl _ROM_ADDR_L
	.globl _ROM_ADDR
	.globl _GPIO_IE
	.globl _INTX
	.globl _IP_EX
	.globl _IE_EX
	.globl _IP
	.globl _IE
	.globl _WDOG_COUNT
	.globl _RESET_KEEP
	.globl _WAKE_CTRL
	.globl _CLOCK_CFG
	.globl _POWER_CFG
	.globl _PCON
	.globl _GLOBAL_CFG
	.globl _SAFE_MOD
	.globl _DPH
	.globl _DPL
	.globl _SP
	.globl _A_INV
	.globl _B
	.globl _ACC
	.globl _PSW
	.globl _RGB_B
	.globl _RGB_G
	.globl _RGB_R
	.globl _Column
	.globl _buf_B
	.globl _buf_G
	.globl _buf_R
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
	.area RSEG    (ABS,DATA)
	.org 0x0000
_PSW	=	0x00d0
_ACC	=	0x00e0
_B	=	0x00f0
_A_INV	=	0x00fd
_SP	=	0x0081
_DPL	=	0x0082
_DPH	=	0x0083
_SAFE_MOD	=	0x00a1
_GLOBAL_CFG	=	0x00b1
_PCON	=	0x0087
_POWER_CFG	=	0x00ba
_CLOCK_CFG	=	0x00b9
_WAKE_CTRL	=	0x00a9
_RESET_KEEP	=	0x00fe
_WDOG_COUNT	=	0x00ff
_IE	=	0x00a8
_IP	=	0x00b8
_IE_EX	=	0x00e8
_IP_EX	=	0x00e9
_INTX	=	0x00b3
_GPIO_IE	=	0x00b2
_ROM_ADDR	=	0x8584
_ROM_ADDR_L	=	0x0084
_ROM_ADDR_H	=	0x0085
_ROM_DATA_HI	=	0x8f8e
_ROM_DATA_HL	=	0x008e
_ROM_DATA_HH	=	0x008f
_ROM_CTRL	=	0x0086
_P0	=	0x0080
_P0_MOD_OC	=	0x00c4
_P0_DIR_PU	=	0x00c5
_P1	=	0x0090
_P1_MOD_OC	=	0x0092
_P1_DIR_PU	=	0x0093
_P2	=	0x00a0
_P2_MOD_OC	=	0x0094
_P2_DIR_PU	=	0x0095
_P3	=	0x00b0
_P3_MOD_OC	=	0x0096
_P3_DIR_PU	=	0x0097
_P4	=	0x00c0
_P4_MOD_OC	=	0x00c2
_P4_DIR_PU	=	0x00c3
_P5	=	0x00ab
_PIN_FUNC	=	0x00aa
_XBUS_AUX	=	0x00a2
_TCON	=	0x0088
_TMOD	=	0x0089
_TL0	=	0x008a
_TL1	=	0x008b
_TH0	=	0x008c
_TH1	=	0x008d
_SCON	=	0x0098
_SBUF	=	0x0099
_T2CON2	=	0x00c1
_T2CAP0	=	0xc7c6
_T2CAP0L	=	0x00c6
_T2CAP0H	=	0x00c7
_T2CON	=	0x00c8
_T2MOD	=	0x00c9
_RCAP2	=	0xcbca
_RCAP2L	=	0x00ca
_RCAP2H	=	0x00cb
_T2COUNT	=	0xcdcc
_TL2	=	0x00cc
_TH2	=	0x00cd
_T2CAP1	=	0xcfce
_T2CAP1L	=	0x00ce
_T2CAP1H	=	0x00cf
_PWM_DATA2	=	0x009a
_PWM_DATA1	=	0x009b
_PWM_DATA0	=	0x009c
_PWM_CTRL	=	0x009d
_PWM_CK_SE	=	0x009e
_PWM_CTRL2	=	0x009f
_PWM_DATA3	=	0x00a3
_PWM_DATA4	=	0x00a4
_PWM_DATA5	=	0x00a5
_PWM_DATA6	=	0x00a6
_PWM_DATA7	=	0x00a7
_SPI0_STAT	=	0x00f8
_SPI0_DATA	=	0x00f9
_SPI0_CTRL	=	0x00fa
_SPI0_CK_SE	=	0x00fb
_SPI0_SETUP	=	0x00fc
_SCON1	=	0x00bc
_SBUF1	=	0x00bd
_SBAUD1	=	0x00be
_SIF1	=	0x00bf
_SCON2	=	0x00b4
_SBUF2	=	0x00b5
_SBAUD2	=	0x00b6
_SIF2	=	0x00b7
_SCON3	=	0x00ac
_SBUF3	=	0x00ad
_SBAUD3	=	0x00ae
_SIF3	=	0x00af
_TKEY_CTRL	=	0x00f1
_ADC_CTRL	=	0x00f2
_ADC_CFG	=	0x00f3
_ADC_DAT	=	0xf5f4
_ADC_DAT_L	=	0x00f4
_ADC_DAT_H	=	0x00f5
_ADC_CHAN	=	0x00f6
_ADC_PIN	=	0x00f7
_USB_C_CTRL	=	0x0091
_UDEV_CTRL	=	0x00d1
_UEP1_CTRL	=	0x00d2
_UEP1_T_LEN	=	0x00d3
_UEP2_CTRL	=	0x00d4
_UEP2_T_LEN	=	0x00d5
_UEP3_CTRL	=	0x00d6
_UEP3_T_LEN	=	0x00d7
_USB_INT_FG	=	0x00d8
_USB_INT_ST	=	0x00d9
_USB_MIS_ST	=	0x00da
_USB_RX_LEN	=	0x00db
_UEP0_CTRL	=	0x00dc
_UEP0_T_LEN	=	0x00dd
_UEP4_CTRL	=	0x00de
_UEP4_T_LEN	=	0x00df
_USB_INT_EN	=	0x00e1
_USB_CTRL	=	0x00e2
_USB_DEV_AD	=	0x00e3
_UEP2_DMA	=	0xe5e4
_UEP2_DMA_L	=	0x00e4
_UEP2_DMA_H	=	0x00e5
_UEP3_DMA	=	0xe7e6
_UEP3_DMA_L	=	0x00e6
_UEP3_DMA_H	=	0x00e7
_UEP4_1_MOD	=	0x00ea
_UEP2_3_MOD	=	0x00eb
_UEP0_DMA	=	0xedec
_UEP0_DMA_L	=	0x00ec
_UEP0_DMA_H	=	0x00ed
_UEP1_DMA	=	0xefee
_UEP1_DMA_L	=	0x00ee
_UEP1_DMA_H	=	0x00ef
;--------------------------------------------------------
; special function bits
;--------------------------------------------------------
	.area RSEG    (ABS,DATA)
	.org 0x0000
_CY	=	0x00d7
_AC	=	0x00d6
_F0	=	0x00d5
_RS1	=	0x00d4
_RS0	=	0x00d3
_OV	=	0x00d2
_F1	=	0x00d1
_P	=	0x00d0
_EA	=	0x00af
_E_DIS	=	0x00ae
_ET2	=	0x00ad
_ES	=	0x00ac
_ET1	=	0x00ab
_EX1	=	0x00aa
_ET0	=	0x00a9
_EX0	=	0x00a8
_PH_FLAG	=	0x00bf
_PL_FLAG	=	0x00be
_PT2	=	0x00bd
_PS	=	0x00bc
_PT1	=	0x00bb
_PX1	=	0x00ba
_PT0	=	0x00b9
_PX0	=	0x00b8
_IE_WDOG	=	0x00ef
_IE_GPIO	=	0x00ee
_IE_PWMX	=	0x00ed
_IE_UART3	=	0x00ed
_IE_UART1	=	0x00ec
_IE_ADC	=	0x00eb
_IE_UART2	=	0x00eb
_IE_USB	=	0x00ea
_IE_INT3	=	0x00e9
_IE_SPI0	=	0x00e8
_P0_7	=	0x0087
_P0_6	=	0x0086
_P0_5	=	0x0085
_P0_4	=	0x0084
_P0_3	=	0x0083
_P0_2	=	0x0082
_P0_1	=	0x0081
_P0_0	=	0x0080
_TXD3	=	0x0087
_AIN15	=	0x0087
_RXD3	=	0x0086
_AIN14	=	0x0086
_TXD2	=	0x0085
_AIN13	=	0x0085
_RXD2	=	0x0084
_AIN12	=	0x0084
_TXD_	=	0x0083
_AIN11	=	0x0083
_RXD_	=	0x0082
_AIN10	=	0x0082
_AIN9	=	0x0081
_AIN8	=	0x0080
_P1_7	=	0x0097
_P1_6	=	0x0096
_P1_5	=	0x0095
_P1_4	=	0x0094
_P1_3	=	0x0093
_P1_2	=	0x0092
_P1_1	=	0x0091
_P1_0	=	0x0090
_SCK	=	0x0097
_TXD1_	=	0x0097
_AIN7	=	0x0097
_MISO	=	0x0096
_RXD1_	=	0x0096
_VBUS	=	0x0096
_AIN6	=	0x0096
_MOSI	=	0x0095
_PWM0_	=	0x0095
_UCC2	=	0x0095
_AIN5	=	0x0095
_SCS	=	0x0094
_UCC1	=	0x0094
_AIN4	=	0x0094
_AIN3	=	0x0093
_AIN2	=	0x0092
_T2EX	=	0x0091
_CAP2	=	0x0091
_AIN1	=	0x0091
_T2	=	0x0090
_CAP1	=	0x0090
_AIN0	=	0x0090
_P2_7	=	0x00a7
_P2_6	=	0x00a6
_P2_5	=	0x00a5
_P2_4	=	0x00a4
_P2_3	=	0x00a3
_P2_2	=	0x00a2
_P2_1	=	0x00a1
_P2_0	=	0x00a0
_PWM7	=	0x00a7
_TXD1	=	0x00a7
_PWM6	=	0x00a6
_RXD1	=	0x00a6
_PWM0	=	0x00a5
_T2EX_	=	0x00a5
_CAP2_	=	0x00a5
_PWM1	=	0x00a4
_T2_	=	0x00a4
_CAP1_	=	0x00a4
_PWM2	=	0x00a3
_PWM3	=	0x00a2
_INT0_	=	0x00a2
_PWM4	=	0x00a1
_PWM5	=	0x00a0
_P3_7	=	0x00b7
_P3_6	=	0x00b6
_P3_5	=	0x00b5
_P3_4	=	0x00b4
_P3_3	=	0x00b3
_P3_2	=	0x00b2
_P3_1	=	0x00b1
_P3_0	=	0x00b0
_INT3	=	0x00b7
_CAP0	=	0x00b6
_T1	=	0x00b5
_T0	=	0x00b4
_INT1	=	0x00b3
_INT0	=	0x00b2
_TXD	=	0x00b1
_RXD	=	0x00b0
_P4_6	=	0x00c6
_P4_5	=	0x00c5
_P4_4	=	0x00c4
_P4_3	=	0x00c3
_P4_2	=	0x00c2
_P4_1	=	0x00c1
_P4_0	=	0x00c0
_XO	=	0x00c7
_XI	=	0x00c6
_TF1	=	0x008f
_TR1	=	0x008e
_TF0	=	0x008d
_TR0	=	0x008c
_IE1	=	0x008b
_IT1	=	0x008a
_IE0	=	0x0089
_IT0	=	0x0088
_SM0	=	0x009f
_SM1	=	0x009e
_SM2	=	0x009d
_REN	=	0x009c
_TB8	=	0x009b
_RB8	=	0x009a
_TI	=	0x0099
_RI	=	0x0098
_TF2	=	0x00cf
_CAP1F	=	0x00cf
_EXF2	=	0x00ce
_RCLK	=	0x00cd
_TCLK	=	0x00cc
_EXEN2	=	0x00cb
_TR2	=	0x00ca
_C_T2	=	0x00c9
_CP_RL2	=	0x00c8
_S0_FST_ACT	=	0x00ff
_S0_IF_OV	=	0x00fe
_S0_IF_FIRST	=	0x00fd
_S0_IF_BYTE	=	0x00fc
_S0_FREE	=	0x00fb
_S0_T_FIFO	=	0x00fa
_S0_R_FIFO	=	0x00f8
_U_IS_NAK	=	0x00df
_U_TOG_OK	=	0x00de
_U_SIE_FREE	=	0x00dd
_UIF_FIFO_OV	=	0x00dc
_UIF_HST_SOF	=	0x00db
_UIF_SUSPEND	=	0x00da
_UIF_TRANSFER	=	0x00d9
_UIF_DETECT	=	0x00d8
_UIF_BUS_RST	=	0x00d8
;--------------------------------------------------------
; overlayable register banks
;--------------------------------------------------------
	.area REG_BANK_0	(REL,OVR,DATA)
	.ds 8
;--------------------------------------------------------
; internal ram data
;--------------------------------------------------------
	.area DSEG    (DATA)
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;--------------------------------------------------------
; Stack segment in internal ram 
;--------------------------------------------------------
	.area	SSEG
__start__stack:
	.ds	1

;--------------------------------------------------------
; indirectly addressable internal ram data
;--------------------------------------------------------
	.area ISEG    (DATA)
;--------------------------------------------------------
; absolute internal ram data
;--------------------------------------------------------
	.area IABS    (ABS,DATA)
	.area IABS    (ABS,DATA)
;--------------------------------------------------------
; bit data
;--------------------------------------------------------
	.area BSEG    (BIT)
;--------------------------------------------------------
; paged external ram data
;--------------------------------------------------------
	.area PSEG    (PAG,XDATA)
;--------------------------------------------------------
; external ram data
;--------------------------------------------------------
	.area XSEG    (XDATA)
_buf_R::
	.ds 455
_buf_G::
	.ds 455
_buf_B::
	.ds 455
_Column::
	.ds 13
_RGB_R::
	.ds 13
_RGB_G::
	.ds 13
_RGB_B::
	.ds 13
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area XABS    (ABS,XDATA)
;--------------------------------------------------------
; external initialized ram data
;--------------------------------------------------------
	.area HOME    (CODE)
	.area GSINIT0 (CODE)
	.area GSINIT1 (CODE)
	.area GSINIT2 (CODE)
	.area GSINIT3 (CODE)
	.area GSINIT4 (CODE)
	.area GSINIT5 (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area CSEG    (CODE)
;--------------------------------------------------------
; interrupt vector 
;--------------------------------------------------------
	.area HOME    (CODE)
__interrupt_vect:
	ljmp	__sdcc_gsinit_startup
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME    (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area GSINIT  (CODE)
	.globl __sdcc_gsinit_startup
	.globl __sdcc_program_startup
	.globl __start__stack
	.globl __mcs51_genRAMCLEAR
;	usr/main.c:22: unsigned char buf_R[COLUMN_LED][COW] = {{0}};//假定灯带长度数据条的输出颜色缓存
	mov	dptr,#_buf_R
	clr	a
	movx	@dptr,a
;	usr/main.c:23: unsigned char buf_G[COLUMN_LED][COW] = {{0}};
	mov	dptr,#_buf_G
	movx	@dptr,a
;	usr/main.c:24: unsigned char buf_B[COLUMN_LED][COW] = {{0}};
	mov	dptr,#_buf_B
	movx	@dptr,a
	.area GSFINAL (CODE)
	ljmp	__sdcc_program_startup
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME    (CODE)
	.area HOME    (CODE)
__sdcc_program_startup:
	ljmp	_main
;	return from main will return to caller
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CSEG    (CODE)
;------------------------------------------------------------
;Allocation info for local variables in function 'Send_2811_24bits'
;------------------------------------------------------------
;R8                        Allocated to stack - _bp -3
;B8                        Allocated to stack - _bp -4
;G8                        Allocated to registers r7 
;n                         Allocated to registers r6 
;------------------------------------------------------------
;	usr/main.c:58: void Send_2811_24bits(unsigned char G8,unsigned char R8,unsigned char B8)
;	-----------------------------------------
;	 function Send_2811_24bits
;	-----------------------------------------
_Send_2811_24bits:
	ar7 = 0x07
	ar6 = 0x06
	ar5 = 0x05
	ar4 = 0x04
	ar3 = 0x03
	ar2 = 0x02
	ar1 = 0x01
	ar0 = 0x00
	push	_bp
	mov	_bp,sp
	mov	r7,dpl
;	usr/main.c:63: for(n=0;n<8;n++)
	mov	r6,#0x00
00131$:
;	usr/main.c:66: if(G8&0x80)
	mov	a,r7
	jnb	acc.7,00104$
;	usr/main.c:68: RGB_1();
;	assignBit
	setb	_P2_2
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
;	assignBit
	clr	_P2_2
;	usr/main.c:72: RGB_0();
	sjmp	00109$
00104$:
;	assignBit
	setb	_P2_2
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
;	assignBit
	clr	_P2_2
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
00109$:
;	usr/main.c:74: G8<<=1;
	mov	ar5,r7
	mov	a,r5
	add	a,r5
	mov	r7,a
;	usr/main.c:63: for(n=0;n<8;n++)
	inc	r6
	cjne	r6,#0x08,00175$
00175$:
	jc	00131$
;	usr/main.c:77: for(n=0;n<8;n++)
	mov	r7,#0x00
00133$:
;	usr/main.c:80: if(R8&0x80)
	mov	a,_bp
	add	a,#0xfd
	mov	r0,a
	mov	a,@r0
	jnb	acc.7,00114$
;	usr/main.c:82: RGB_1();
;	assignBit
	setb	_P2_2
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
;	assignBit
	clr	_P2_2
;	usr/main.c:86: RGB_0();
	sjmp	00119$
00114$:
;	assignBit
	setb	_P2_2
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
;	assignBit
	clr	_P2_2
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
00119$:
;	usr/main.c:88: R8<<=1;
	mov	a,_bp
	add	a,#0xfd
	mov	r0,a
	mov	ar6,@r0
	mov	a,_bp
	add	a,#0xfd
	mov	r0,a
	mov	a,r6
	add	a,r6
	mov	@r0,a
;	usr/main.c:77: for(n=0;n<8;n++)
	inc	r7
	cjne	r7,#0x08,00178$
00178$:
	jc	00133$
;	usr/main.c:91: for(n=0;n<8;n++)
	mov	r7,#0x00
00135$:
;	usr/main.c:94: if(B8&0x80)
	mov	a,_bp
	add	a,#0xfc
	mov	r0,a
	mov	a,@r0
	jnb	acc.7,00124$
;	usr/main.c:96: RGB_1();
;	assignBit
	setb	_P2_2
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
;	assignBit
	clr	_P2_2
;	usr/main.c:100: RGB_0();
	sjmp	00129$
00124$:
;	assignBit
	setb	_P2_2
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
;	assignBit
	clr	_P2_2
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
	NOP	
00129$:
;	usr/main.c:102: B8<<=1;
	mov	a,_bp
	add	a,#0xfc
	mov	r0,a
	mov	ar6,@r0
	mov	a,_bp
	add	a,#0xfc
	mov	r0,a
	mov	a,r6
	add	a,r6
	mov	@r0,a
;	usr/main.c:91: for(n=0;n<8;n++)
	inc	r7
	cjne	r7,#0x08,00181$
00181$:
	jc	00135$
;	usr/main.c:104: }
	pop	_bp
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'RGB_Rst'
;------------------------------------------------------------
;	usr/main.c:107: void RGB_Rst()
;	-----------------------------------------
;	 function RGB_Rst
;	-----------------------------------------
_RGB_Rst:
;	usr/main.c:109: WS2812 = 0;
;	assignBit
	clr	_P2_2
;	usr/main.c:110: mDelayuS( 50 );
	mov	dptr,#0x0032
;	usr/main.c:111: }
	ljmp	_mDelayuS
;------------------------------------------------------------
;Allocation info for local variables in function 'Buf_Put0'
;------------------------------------------------------------
;i                         Allocated to registers r0 
;j                         Allocated to registers r7 
;------------------------------------------------------------
;	usr/main.c:114: void Buf_Put0()
;	-----------------------------------------
;	 function Buf_Put0
;	-----------------------------------------
_Buf_Put0:
;	usr/main.c:117: for (j = 0; j < COLUMN_LED; j++)
	mov	r7,#0x00
;	usr/main.c:119: for(i=0;i<COW;i++)
00109$:
	mov	a,r7
	mov	b,#0x23
	mul	ab
	mov	r5,a
	mov	r6,b
	add	a,#_buf_R
	mov	r3,a
	mov	a,r6
	addc	a,#(_buf_R >> 8)
	mov	r4,a
	mov	a,r5
	add	a,#_buf_G
	mov	r1,a
	mov	a,r6
	addc	a,#(_buf_G >> 8)
	mov	r2,a
	mov	a,r5
	add	a,#_buf_B
	mov	r5,a
	mov	a,r6
	addc	a,#(_buf_B >> 8)
	mov	r6,a
	mov	r0,#0x00
00103$:
;	usr/main.c:121: buf_R[j][i] = 0; 
	mov	a,r0
	add	a,r3
	mov	dpl,a
	clr	a
	addc	a,r4
	mov	dph,a
	clr	a
	movx	@dptr,a
;	usr/main.c:122: buf_G[j][i] = 0;
	mov	a,r0
	add	a,r1
	mov	dpl,a
	clr	a
	addc	a,r2
	mov	dph,a
	clr	a
	movx	@dptr,a
;	usr/main.c:123: buf_B[j][i] = 0;
	mov	a,r0
	add	a,r5
	mov	dpl,a
	clr	a
	addc	a,r6
	mov	dph,a
	clr	a
	movx	@dptr,a
;	usr/main.c:119: for(i=0;i<COW;i++)
	inc	r0
	cjne	r0,#0x23,00123$
00123$:
	jc	00103$
;	usr/main.c:117: for (j = 0; j < COLUMN_LED; j++)
	inc	r7
	cjne	r7,#0x0d,00125$
00125$:
	jc	00109$
;	usr/main.c:126: }
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'genColor'
;------------------------------------------------------------
;j                         Allocated to stack - _bp +5
;h                         Allocated to registers r4 r5 r6 r7 
;s                         Allocated to registers 
;v                         Allocated to stack - _bp +7
;i                         Allocated to stack - _bp +11
;C                         Allocated to stack - _bp +13
;X                         Allocated to registers 
;Y                         Allocated to stack - _bp +17
;Z                         Allocated to stack - _bp +21
;sloc0                     Allocated to stack - _bp +1
;------------------------------------------------------------
;	usr/main.c:129: void genColor()
;	-----------------------------------------
;	 function genColor
;	-----------------------------------------
_genColor:
	push	_bp
	mov	a,sp
	mov	_bp,a
	add	a,#0x18
	mov	sp,a
;	usr/main.c:132: float h , s = 1.0 , v = 1.0;                          //色相等分，明度和饱和度均设定为满
	mov	a,_bp
	add	a,#0x07
	mov	r0,a
	clr	a
	mov	@r0,a
	inc	r0
	mov	@r0,a
	inc	r0
	mov	@r0,#0x80
	inc	r0
	mov	@r0,#0x3f
;	usr/main.c:133: for (j = 0; j < COLUMN_LED; j++)
	mov	a,_bp
	add	a,#0x05
	mov	r0,a
	clr	a
	mov	@r0,a
	inc	r0
	mov	@r0,a
00112$:
;	usr/main.c:135: h = j*1.0/COLUMN_LED;
	mov	a,_bp
	add	a,#0x05
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	lcall	___uint2fs
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	clr	a
	push	acc
	push	acc
	mov	a,#0x50
	push	acc
	mov	a,#0x41
	push	acc
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	mov	a,r7
	lcall	___fsdiv
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
;	usr/main.c:136: h *= 360.0f;                                          //HSV转RGB
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	mov	dptr,#0x0000
	mov	b,#0xb4
	mov	a,#0x43
	lcall	___fsmul
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
;	usr/main.c:141: h = h / 60.0;
	clr	a
	push	acc
	push	acc
	mov	a,#0x70
	push	acc
	mov	a,#0x42
	push	acc
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	mov	a,r7
	lcall	___fsdiv
	mov	r0,_bp
	inc	r0
	mov	@r0,dpl
	inc	r0
	mov	@r0,dph
	inc	r0
	mov	@r0,b
	inc	r0
	mov	@r0,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
;	usr/main.c:142: int i = (int)h;
	mov	r0,_bp
	inc	r0
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	inc	r0
	mov	b,@r0
	inc	r0
	mov	a,@r0
	lcall	___fs2sint
	mov	r7,dpl
	mov	r6,dph
	mov	a,_bp
	add	a,#0x0b
	mov	r0,a
	mov	@r0,ar7
	inc	r0
	mov	@r0,ar6
;	usr/main.c:143: float C = h - i;
	mov	a,_bp
	add	a,#0x0b
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	lcall	___sint2fs
	mov	r2,dpl
	mov	r3,dph
	mov	r6,b
	mov	r7,a
	push	ar2
	push	ar3
	push	ar6
	push	ar7
	mov	r0,_bp
	inc	r0
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	inc	r0
	mov	b,@r0
	inc	r0
	mov	a,@r0
	lcall	___fssub
	xch	a,r0
	mov	a,_bp
	add	a,#0x0d
	xch	a,r0
	mov	@r0,dpl
	inc	r0
	mov	@r0,dph
	inc	r0
	mov	@r0,b
	inc	r0
	mov	@r0,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
;	usr/main.c:144: v *=255.0;
	mov	a,_bp
	add	a,#0x07
	mov	r0,a
	mov	a,@r0
	push	acc
	inc	r0
	mov	a,@r0
	push	acc
	inc	r0
	mov	a,@r0
	push	acc
	inc	r0
	mov	a,@r0
	push	acc
	mov	dptr,#0x0000
	mov	b,#0x7f
	mov	a,#0x43
	lcall	___fsmul
	mov	r2,dpl
	mov	r3,dph
	mov	r6,b
	mov	r7,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	mov	a,_bp
	add	a,#0x07
	mov	r0,a
	mov	@r0,ar2
	inc	r0
	mov	@r0,ar3
	inc	r0
	mov	@r0,ar6
	inc	r0
	mov	@r0,ar7
;	usr/main.c:146: float Y = v * (1 - s * C) * 255.0;
	mov	a,_bp
	add	a,#0x0d
	mov	r0,a
	mov	a,@r0
	push	acc
	inc	r0
	mov	a,@r0
	push	acc
	inc	r0
	mov	a,@r0
	push	acc
	inc	r0
	mov	a,@r0
	push	acc
	mov	dptr,#0x0000
	mov	b,#0x80
	mov	a,#0x3f
	lcall	___fssub
	mov	r0,_bp
	inc	r0
	mov	@r0,dpl
	inc	r0
	mov	@r0,dph
	inc	r0
	mov	@r0,b
	inc	r0
	mov	@r0,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	mov	r0,_bp
	inc	r0
	mov	a,@r0
	push	acc
	inc	r0
	mov	a,@r0
	push	acc
	inc	r0
	mov	a,@r0
	push	acc
	inc	r0
	mov	a,@r0
	push	acc
	mov	a,_bp
	add	a,#0x07
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	inc	r0
	mov	b,@r0
	inc	r0
	mov	a,@r0
	lcall	___fsmul
	mov	r2,dpl
	mov	r3,dph
	mov	r6,b
	mov	r7,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	push	ar2
	push	ar3
	push	ar6
	push	ar7
	mov	dptr,#0x0000
	mov	b,#0x7f
	mov	a,#0x43
	lcall	___fsmul
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	mov	a,_bp
	add	a,#0x11
	mov	r0,a
	mov	@r0,ar4
	inc	r0
	mov	@r0,ar5
	inc	r0
	mov	@r0,ar6
	inc	r0
	mov	@r0,ar7
;	usr/main.c:147: float Z = v * (1 - s * (1 - C)) * 255.0;
	mov	r0,_bp
	inc	r0
	mov	a,@r0
	push	acc
	inc	r0
	mov	a,@r0
	push	acc
	inc	r0
	mov	a,@r0
	push	acc
	inc	r0
	mov	a,@r0
	push	acc
	mov	dptr,#0x0000
	mov	b,#0x80
	mov	a,#0x3f
	lcall	___fssub
	mov	r2,dpl
	mov	r3,dph
	mov	r6,b
	mov	r7,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	push	ar2
	push	ar3
	push	ar6
	push	ar7
	mov	a,_bp
	add	a,#0x07
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	inc	r0
	mov	b,@r0
	inc	r0
	mov	a,@r0
	lcall	___fsmul
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	push	ar4
	push	ar5
	push	ar6
	push	ar7
	mov	dptr,#0x0000
	mov	b,#0x7f
	mov	a,#0x43
	lcall	___fsmul
	mov	r4,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	mov	a,_bp
	add	a,#0x15
	mov	r0,a
	mov	@r0,ar4
	inc	r0
	mov	@r0,ar5
	inc	r0
	mov	@r0,ar6
	inc	r0
	mov	@r0,ar7
;	usr/main.c:148: switch (i)
	mov	a,_bp
	add	a,#0x0b
	mov	r0,a
	inc	r0
	mov	a,@r0
	jnb	acc.7,00133$
	ljmp	00113$
00133$:
	mov	a,_bp
	add	a,#0x0b
	mov	r0,a
	clr	c
	mov	a,#0x05
	subb	a,@r0
	mov	a,#(0x00 ^ 0x80)
	inc	r0
	mov	b,@r0
	xrl	b,#0x80
	subb	a,b
	jnc	00134$
	ljmp	00113$
00134$:
	mov	a,_bp
	add	a,#0x0b
	mov	r0,a
	mov	a,@r0
	add	a,@r0
	add	a,@r0
	mov	dptr,#00135$
	jmp	@a+dptr
00135$:
	ljmp	00101$
	ljmp	00102$
	ljmp	00103$
	ljmp	00104$
	ljmp	00105$
	ljmp	00106$
;	usr/main.c:150: case 0: RGB_R[j]  = v; RGB_G[j]  = Z; RGB_B[j]  = X; break;
00101$:
	mov	a,_bp
	add	a,#0x05
	mov	r0,a
	mov	a,@r0
	add	a,#_RGB_R
	mov	r2,a
	inc	r0
	mov	a,@r0
	addc	a,#(_RGB_R >> 8)
	mov	r3,a
	mov	a,_bp
	add	a,#0x07
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	inc	r0
	mov	b,@r0
	inc	r0
	mov	a,@r0
	push	ar3
	push	ar2
	lcall	___fs2uchar
	mov	r7,dpl
	pop	ar2
	pop	ar3
	mov	dpl,r2
	mov	dph,r3
	mov	a,r7
	movx	@dptr,a
	mov	a,_bp
	add	a,#0x05
	mov	r0,a
	mov	a,@r0
	add	a,#_RGB_G
	mov	r6,a
	inc	r0
	mov	a,@r0
	addc	a,#(_RGB_G >> 8)
	mov	r7,a
	mov	a,_bp
	add	a,#0x15
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	inc	r0
	mov	b,@r0
	inc	r0
	mov	a,@r0
	push	ar7
	push	ar6
	lcall	___fs2uchar
	mov	r5,dpl
	pop	ar6
	pop	ar7
	mov	dpl,r6
	mov	dph,r7
	mov	a,r5
	movx	@dptr,a
	mov	a,_bp
	add	a,#0x05
	mov	r0,a
	mov	a,@r0
	add	a,#_RGB_B
	mov	dpl,a
	inc	r0
	mov	a,@r0
	addc	a,#(_RGB_B >> 8)
	mov	dph,a
	clr	a
	movx	@dptr,a
	ljmp	00113$
;	usr/main.c:151: case 1: RGB_R[j]  = Y; RGB_G[j]  = v; RGB_B[j]  = X; break;
00102$:
	mov	a,_bp
	add	a,#0x05
	mov	r0,a
	mov	a,@r0
	add	a,#_RGB_R
	mov	r6,a
	inc	r0
	mov	a,@r0
	addc	a,#(_RGB_R >> 8)
	mov	r7,a
	mov	a,_bp
	add	a,#0x11
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	inc	r0
	mov	b,@r0
	inc	r0
	mov	a,@r0
	push	ar7
	push	ar6
	lcall	___fs2uchar
	mov	r5,dpl
	pop	ar6
	pop	ar7
	mov	dpl,r6
	mov	dph,r7
	mov	a,r5
	movx	@dptr,a
	mov	a,_bp
	add	a,#0x05
	mov	r0,a
	mov	a,@r0
	add	a,#_RGB_G
	mov	r6,a
	inc	r0
	mov	a,@r0
	addc	a,#(_RGB_G >> 8)
	mov	r7,a
	mov	a,_bp
	add	a,#0x07
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	inc	r0
	mov	b,@r0
	inc	r0
	mov	a,@r0
	push	ar7
	push	ar6
	lcall	___fs2uchar
	mov	r5,dpl
	pop	ar6
	pop	ar7
	mov	dpl,r6
	mov	dph,r7
	mov	a,r5
	movx	@dptr,a
	mov	a,_bp
	add	a,#0x05
	mov	r0,a
	mov	a,@r0
	add	a,#_RGB_B
	mov	dpl,a
	inc	r0
	mov	a,@r0
	addc	a,#(_RGB_B >> 8)
	mov	dph,a
	clr	a
	movx	@dptr,a
	ljmp	00113$
;	usr/main.c:152: case 2: RGB_R[j]  = X; RGB_G[j]  = v; RGB_B[j]  = Z; break;
00103$:
	mov	a,_bp
	add	a,#0x05
	mov	r0,a
	mov	a,@r0
	add	a,#_RGB_R
	mov	dpl,a
	inc	r0
	mov	a,@r0
	addc	a,#(_RGB_R >> 8)
	mov	dph,a
	clr	a
	movx	@dptr,a
	mov	a,_bp
	add	a,#0x05
	mov	r0,a
	mov	a,@r0
	add	a,#_RGB_G
	mov	r6,a
	inc	r0
	mov	a,@r0
	addc	a,#(_RGB_G >> 8)
	mov	r7,a
	mov	a,_bp
	add	a,#0x07
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	inc	r0
	mov	b,@r0
	inc	r0
	mov	a,@r0
	push	ar7
	push	ar6
	lcall	___fs2uchar
	mov	r5,dpl
	pop	ar6
	pop	ar7
	mov	dpl,r6
	mov	dph,r7
	mov	a,r5
	movx	@dptr,a
	mov	a,_bp
	add	a,#0x05
	mov	r0,a
	mov	a,@r0
	add	a,#_RGB_B
	mov	r6,a
	inc	r0
	mov	a,@r0
	addc	a,#(_RGB_B >> 8)
	mov	r7,a
	mov	a,_bp
	add	a,#0x15
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	inc	r0
	mov	b,@r0
	inc	r0
	mov	a,@r0
	push	ar7
	push	ar6
	lcall	___fs2uchar
	mov	r5,dpl
	pop	ar6
	pop	ar7
	mov	dpl,r6
	mov	dph,r7
	mov	a,r5
	movx	@dptr,a
	ljmp	00113$
;	usr/main.c:153: case 3: RGB_R[j]  = X; RGB_G[j]  = Y; RGB_B[j]  = v; break;
00104$:
	mov	a,_bp
	add	a,#0x05
	mov	r0,a
	mov	a,@r0
	add	a,#_RGB_R
	mov	dpl,a
	inc	r0
	mov	a,@r0
	addc	a,#(_RGB_R >> 8)
	mov	dph,a
	clr	a
	movx	@dptr,a
	mov	a,_bp
	add	a,#0x05
	mov	r0,a
	mov	a,@r0
	add	a,#_RGB_G
	mov	r6,a
	inc	r0
	mov	a,@r0
	addc	a,#(_RGB_G >> 8)
	mov	r7,a
	mov	a,_bp
	add	a,#0x11
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	inc	r0
	mov	b,@r0
	inc	r0
	mov	a,@r0
	push	ar7
	push	ar6
	lcall	___fs2uchar
	mov	r5,dpl
	pop	ar6
	pop	ar7
	mov	dpl,r6
	mov	dph,r7
	mov	a,r5
	movx	@dptr,a
	mov	a,_bp
	add	a,#0x05
	mov	r0,a
	mov	a,@r0
	add	a,#_RGB_B
	mov	r6,a
	inc	r0
	mov	a,@r0
	addc	a,#(_RGB_B >> 8)
	mov	r7,a
	mov	a,_bp
	add	a,#0x07
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	inc	r0
	mov	b,@r0
	inc	r0
	mov	a,@r0
	push	ar7
	push	ar6
	lcall	___fs2uchar
	mov	r5,dpl
	pop	ar6
	pop	ar7
	mov	dpl,r6
	mov	dph,r7
	mov	a,r5
	movx	@dptr,a
	ljmp	00113$
;	usr/main.c:154: case 4: RGB_R[j]  = Z; RGB_G[j]  = X; RGB_B[j]  = v; break;
00105$:
	mov	a,_bp
	add	a,#0x05
	mov	r0,a
	mov	a,@r0
	add	a,#_RGB_R
	mov	r6,a
	inc	r0
	mov	a,@r0
	addc	a,#(_RGB_R >> 8)
	mov	r7,a
	mov	a,_bp
	add	a,#0x15
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	inc	r0
	mov	b,@r0
	inc	r0
	mov	a,@r0
	push	ar7
	push	ar6
	lcall	___fs2uchar
	mov	r5,dpl
	pop	ar6
	pop	ar7
	mov	dpl,r6
	mov	dph,r7
	mov	a,r5
	movx	@dptr,a
	mov	a,_bp
	add	a,#0x05
	mov	r0,a
	mov	a,@r0
	add	a,#_RGB_G
	mov	dpl,a
	inc	r0
	mov	a,@r0
	addc	a,#(_RGB_G >> 8)
	mov	dph,a
	clr	a
	movx	@dptr,a
	mov	a,_bp
	add	a,#0x05
	mov	r0,a
	mov	a,@r0
	add	a,#_RGB_B
	mov	r6,a
	inc	r0
	mov	a,@r0
	addc	a,#(_RGB_B >> 8)
	mov	r7,a
	mov	a,_bp
	add	a,#0x07
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	inc	r0
	mov	b,@r0
	inc	r0
	mov	a,@r0
	push	ar7
	push	ar6
	lcall	___fs2uchar
	mov	r5,dpl
	pop	ar6
	pop	ar7
	mov	dpl,r6
	mov	dph,r7
	mov	a,r5
	movx	@dptr,a
;	usr/main.c:155: case 5: RGB_R[j]  = v; RGB_G[j]  = X; RGB_B[j]  = Y; break;
	sjmp	00113$
00106$:
	mov	a,_bp
	add	a,#0x05
	mov	r0,a
	mov	a,@r0
	add	a,#_RGB_R
	mov	r6,a
	inc	r0
	mov	a,@r0
	addc	a,#(_RGB_R >> 8)
	mov	r7,a
	mov	a,_bp
	add	a,#0x07
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	inc	r0
	mov	b,@r0
	inc	r0
	mov	a,@r0
	push	ar7
	push	ar6
	lcall	___fs2uchar
	mov	r5,dpl
	pop	ar6
	pop	ar7
	mov	dpl,r6
	mov	dph,r7
	mov	a,r5
	movx	@dptr,a
	mov	a,_bp
	add	a,#0x05
	mov	r0,a
	mov	a,@r0
	add	a,#_RGB_G
	mov	dpl,a
	inc	r0
	mov	a,@r0
	addc	a,#(_RGB_G >> 8)
	mov	dph,a
	clr	a
	movx	@dptr,a
	mov	a,_bp
	add	a,#0x05
	mov	r0,a
	mov	a,@r0
	add	a,#_RGB_B
	mov	r6,a
	inc	r0
	mov	a,@r0
	addc	a,#(_RGB_B >> 8)
	mov	r7,a
	mov	a,_bp
	add	a,#0x11
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	inc	r0
	mov	b,@r0
	inc	r0
	mov	a,@r0
	push	ar7
	push	ar6
	lcall	___fs2uchar
	mov	r5,dpl
	pop	ar6
	pop	ar7
	mov	dpl,r6
	mov	dph,r7
	mov	a,r5
	movx	@dptr,a
;	usr/main.c:156: }
00113$:
;	usr/main.c:133: for (j = 0; j < COLUMN_LED; j++)
	mov	a,_bp
	add	a,#0x05
	mov	r0,a
	inc	@r0
	cjne	@r0,#0x00,00136$
	inc	r0
	inc	@r0
00136$:
	mov	a,_bp
	add	a,#0x05
	mov	r0,a
	clr	c
	mov	a,@r0
	subb	a,#0x0d
	inc	r0
	mov	a,@r0
	subb	a,#0x00
	jnc	00137$
	ljmp	00112$
00137$:
;	usr/main.c:159: }
	mov	sp,_bp
	pop	_bp
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'Set_Color'
;------------------------------------------------------------
;cow                       Allocated to stack - _bp -3
;column                    Allocated to registers r7 
;i                         Allocated to stack - _bp +16
;sloc0                     Allocated to stack - _bp +12
;sloc1                     Allocated to stack - _bp +4
;sloc2                     Allocated to stack - _bp +8
;sloc3                     Allocated to stack - _bp +1
;sloc4                     Allocated to stack - _bp +14
;sloc5                     Allocated to stack - _bp +10
;sloc6                     Allocated to stack - _bp +15
;sloc7                     Allocated to stack - _bp +3
;sloc8                     Allocated to stack - _bp +6
;------------------------------------------------------------
;	usr/main.c:162: void Set_Color(unsigned char column,unsigned char cow)
;	-----------------------------------------
;	 function Set_Color
;	-----------------------------------------
_Set_Color:
	push	_bp
	mov	a,sp
	mov	_bp,a
	add	a,#0x10
	mov	sp,a
;	usr/main.c:165: for(i=0;i<PIXEL;i++)
	mov	a,dpl
	mov	r7,a
	mov	b,#0x23
	mul	ab
	mov	r5,a
	mov	r6,b
	mov	a,_bp
	add	a,#0x0e
	mov	r0,a
	mov	a,r5
	add	a,#_buf_R
	mov	@r0,a
	mov	a,r6
	addc	a,#(_buf_R >> 8)
	inc	r0
	mov	@r0,a
	mov	a,_bp
	add	a,#0x0c
	mov	r0,a
	mov	a,r7
	add	a,#_RGB_R
	mov	@r0,a
	clr	a
	addc	a,#(_RGB_R >> 8)
	inc	r0
	mov	@r0,a
	mov	a,_bp
	add	a,#0x04
	mov	r0,a
	mov	a,r5
	add	a,#_buf_G
	mov	@r0,a
	mov	a,r6
	addc	a,#(_buf_G >> 8)
	inc	r0
	mov	@r0,a
	mov	a,_bp
	add	a,#0x08
	mov	r0,a
	mov	a,r7
	add	a,#_RGB_G
	mov	@r0,a
	clr	a
	addc	a,#(_RGB_G >> 8)
	inc	r0
	mov	@r0,a
	mov	r0,_bp
	inc	r0
	mov	a,r5
	add	a,#_buf_B
	mov	@r0,a
	mov	a,r6
	addc	a,#(_buf_B >> 8)
	inc	r0
	mov	@r0,a
	mov	a,_bp
	add	a,#0x06
	mov	r0,a
	mov	a,r7
	add	a,#_RGB_B
	mov	@r0,a
	clr	a
	addc	a,#(_RGB_B >> 8)
	inc	r0
	mov	@r0,a
	mov	a,_bp
	add	a,#0x10
	mov	r0,a
	mov	@r0,#0x00
00103$:
;	usr/main.c:167: buf_R[column][cow+PIXEL-i] = RGB_R[column] / (PIXEL * i + 1);
	push	ar5
	push	ar6
	mov	a,_bp
	add	a,#0xfd
	mov	r0,a
	mov	a,_bp
	add	a,#0x03
	mov	r1,a
	mov	a,@r0
	mov	@r1,a
	mov	a,_bp
	add	a,#0x03
	mov	r0,a
	mov	a,#0x04
	add	a,@r0
	mov	r6,a
	mov	a,_bp
	add	a,#0x10
	mov	r0,a
	mov	ar5,@r0
	mov	a,r6
	clr	c
	subb	a,r5
	mov	r6,a
	mov	a,_bp
	add	a,#0x0e
	mov	r0,a
	mov	a,_bp
	add	a,#0x0a
	mov	r1,a
	mov	a,r6
	add	a,@r0
	mov	@r1,a
	clr	a
	inc	r0
	addc	a,@r0
	inc	r1
	mov	@r1,a
	mov	a,_bp
	add	a,#0x0c
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	movx	a,@dptr
	mov	r7,a
	mov	a,_bp
	add	a,#0x10
	mov	r0,a
	mov	ar4,@r0
	mov	r5,#0x00
	mov	a,r4
	add	a,r4
	mov	r4,a
	mov	a,r5
	rlc	a
	mov	r5,a
	mov	a,r4
	add	a,r4
	mov	r4,a
	mov	a,r5
	rlc	a
	mov	r5,a
	inc	r4
	cjne	r4,#0x00,00123$
	inc	r5
00123$:
	mov	ar3,r7
	mov	r7,#0x00
	push	ar6
	push	ar5
	push	ar4
	push	ar4
	push	ar5
	mov	dpl,r3
	mov	dph,r7
	lcall	__divsint
	mov	r3,dpl
	dec	sp
	dec	sp
	pop	ar4
	pop	ar5
	pop	ar6
	mov	a,_bp
	add	a,#0x0a
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	mov	a,r3
	movx	@dptr,a
;	usr/main.c:168: buf_G[column][cow+PIXEL-i] = RGB_G[column] / (PIXEL * i + 1);
	mov	a,_bp
	add	a,#0x04
	mov	r0,a
	mov	a,_bp
	add	a,#0x0a
	mov	r1,a
	mov	a,r6
	add	a,@r0
	mov	@r1,a
	clr	a
	inc	r0
	addc	a,@r0
	inc	r1
	mov	@r1,a
	mov	a,_bp
	add	a,#0x08
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	movx	a,@dptr
	mov	r2,a
	mov	r7,#0x00
	push	ar6
	push	ar5
	push	ar4
	push	ar4
	push	ar5
	mov	dpl,r2
	mov	dph,r7
	lcall	__divsint
	mov	r3,dpl
	dec	sp
	dec	sp
	pop	ar4
	pop	ar5
	pop	ar6
	mov	a,_bp
	add	a,#0x0a
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	mov	a,r3
	movx	@dptr,a
;	usr/main.c:169: buf_B[column][cow+PIXEL-i] = RGB_B[column] / (PIXEL * i + 1);
	mov	r0,_bp
	inc	r0
	mov	a,r6
	add	a,@r0
	mov	r6,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r7,a
	mov	a,_bp
	add	a,#0x06
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	movx	a,@dptr
	mov	r3,a
	mov	r2,#0x00
	push	ar7
	push	ar6
	push	ar4
	push	ar5
	mov	dpl,r3
	mov	dph,r2
	lcall	__divsint
	mov	r4,dpl
	dec	sp
	dec	sp
	pop	ar6
	pop	ar7
	mov	dpl,r6
	mov	dph,r7
	mov	a,r4
	movx	@dptr,a
;	usr/main.c:165: for(i=0;i<PIXEL;i++)
	mov	a,_bp
	add	a,#0x10
	mov	r0,a
	inc	@r0
	mov	a,_bp
	add	a,#0x10
	mov	r0,a
	cjne	@r0,#0x04,00124$
00124$:
	pop	ar6
	pop	ar5
	jnc	00125$
	ljmp	00103$
00125$:
;	usr/main.c:171: for(i=1;i<PIXEL;i++)
	mov	a,_bp
	add	a,#0x04
	mov	r0,a
	mov	a,r5
	add	a,#_buf_R
	mov	@r0,a
	mov	a,r6
	addc	a,#(_buf_R >> 8)
	inc	r0
	mov	@r0,a
	mov	a,_bp
	add	a,#0x03
	mov	r0,a
	mov	a,#0x04
	add	a,@r0
	mov	r3,a
	mov	a,_bp
	add	a,#0x0a
	mov	r0,a
	mov	a,r5
	add	a,#_buf_G
	mov	@r0,a
	mov	a,r6
	addc	a,#(_buf_G >> 8)
	inc	r0
	mov	@r0,a
	mov	a,_bp
	add	a,#0x0e
	mov	r0,a
	mov	a,r5
	add	a,#_buf_B
	mov	@r0,a
	mov	a,r6
	addc	a,#(_buf_B >> 8)
	inc	r0
	mov	@r0,a
	mov	a,_bp
	add	a,#0x10
	mov	r0,a
	mov	@r0,#0x01
00105$:
;	usr/main.c:173: buf_R[column][cow+PIXEL+i] = RGB_R[column] / (PIXEL * i + 1);
	mov	a,_bp
	add	a,#0x10
	mov	r0,a
	mov	a,@r0
	add	a,r3
	mov	r2,a
	push	ar3
	mov	a,_bp
	add	a,#0x04
	mov	r0,a
	mov	r1,_bp
	inc	r1
	mov	a,r2
	add	a,@r0
	mov	@r1,a
	clr	a
	inc	r0
	addc	a,@r0
	inc	r1
	mov	@r1,a
	mov	a,_bp
	add	a,#0x0c
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	movx	a,@dptr
	mov	r7,a
	mov	a,_bp
	add	a,#0x10
	mov	r0,a
	mov	ar5,@r0
	mov	r6,#0x00
	mov	a,r5
	add	a,r5
	mov	r5,a
	mov	a,r6
	rlc	a
	mov	r6,a
	mov	a,r5
	add	a,r5
	mov	r5,a
	mov	a,r6
	rlc	a
	mov	r6,a
	inc	r5
	cjne	r5,#0x00,00126$
	inc	r6
00126$:
	mov	ar3,r7
	mov	r7,#0x00
	push	ar6
	push	ar5
	push	ar3
	push	ar2
	push	ar5
	push	ar6
	mov	dpl,r3
	mov	dph,r7
	lcall	__divsint
	mov	r4,dpl
	dec	sp
	dec	sp
	pop	ar2
	pop	ar3
	pop	ar5
	pop	ar6
	mov	r0,_bp
	inc	r0
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	mov	a,r4
	movx	@dptr,a
;	usr/main.c:174: buf_G[column][cow+PIXEL+i] = RGB_G[column] / (PIXEL * i + 1);
	mov	a,_bp
	add	a,#0x0a
	mov	r0,a
	mov	r1,_bp
	inc	r1
	mov	a,r2
	add	a,@r0
	mov	@r1,a
	clr	a
	inc	r0
	addc	a,@r0
	inc	r1
	mov	@r1,a
	mov	a,_bp
	add	a,#0x08
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	movx	a,@dptr
	mov	r3,a
	mov	r7,#0x00
	push	ar6
	push	ar5
	push	ar3
	push	ar2
	push	ar5
	push	ar6
	mov	dpl,r3
	mov	dph,r7
	lcall	__divsint
	mov	r4,dpl
	dec	sp
	dec	sp
	pop	ar2
	pop	ar3
	pop	ar5
	pop	ar6
	mov	r0,_bp
	inc	r0
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	mov	a,r4
	movx	@dptr,a
;	usr/main.c:175: buf_B[column][cow+PIXEL+i] = RGB_B[column] / (PIXEL * i + 1);
	mov	a,_bp
	add	a,#0x0e
	mov	r0,a
	mov	a,r2
	add	a,@r0
	mov	r2,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r7,a
	mov	a,_bp
	add	a,#0x06
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	movx	a,@dptr
	mov	r4,a
	mov	r3,#0x00
	push	ar7
	push	ar3
	push	ar2
	push	ar5
	push	ar6
	mov	dpl,r4
	mov	dph,r3
	lcall	__divsint
	mov	r5,dpl
	mov	r6,dph
	dec	sp
	dec	sp
	pop	ar2
	pop	ar3
	pop	ar7
	mov	dpl,r2
	mov	dph,r7
	mov	a,r5
	movx	@dptr,a
;	usr/main.c:171: for(i=1;i<PIXEL;i++)
	mov	a,_bp
	add	a,#0x10
	mov	r0,a
	inc	@r0
	mov	a,_bp
	add	a,#0x10
	mov	r0,a
	cjne	@r0,#0x04,00127$
00127$:
	pop	ar3
	jnc	00128$
	ljmp	00105$
00128$:
;	usr/main.c:177: }
	mov	sp,_bp
	pop	_bp
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'Send_ALL'
;------------------------------------------------------------
;i                         Allocated to stack - _bp +4
;j                         Allocated to registers r7 
;sloc0                     Allocated to stack - _bp +1
;sloc1                     Allocated to stack - _bp +3
;------------------------------------------------------------
;	usr/main.c:180: void Send_ALL()
;	-----------------------------------------
;	 function Send_ALL
;	-----------------------------------------
_Send_ALL:
	push	_bp
	mov	a,sp
	mov	_bp,a
	add	a,#0x04
	mov	sp,a
;	usr/main.c:183: for (j = 0; j < COLUMN_LED; j++)                          
	mov	r7,#0x00
;	usr/main.c:185: for(i=PIXEL*2-1;i<COW-PIXEL*2+1;i++)
00109$:
	mov	a,r7
	mov	b,#0x23
	mul	ab
	mov	r5,a
	mov	r6,b
	add	a,#_buf_B
	mov	r3,a
	mov	a,r6
	addc	a,#(_buf_B >> 8)
	mov	r4,a
	mov	r0,_bp
	inc	r0
	mov	a,r5
	add	a,#_buf_R
	mov	@r0,a
	mov	a,r6
	addc	a,#(_buf_R >> 8)
	inc	r0
	mov	@r0,a
	mov	a,r5
	add	a,#_buf_G
	mov	r5,a
	mov	a,r6
	addc	a,#(_buf_G >> 8)
	mov	r6,a
	mov	a,_bp
	add	a,#0x04
	mov	r0,a
	mov	@r0,#0x07
00103$:
;	usr/main.c:187: Send_2811_24bits(buf_G[j][i],buf_R[j][i],buf_B[j][i]);
	push	ar7
	mov	a,_bp
	add	a,#0x04
	mov	r0,a
	mov	a,@r0
	add	a,r3
	mov	dpl,a
	clr	a
	addc	a,r4
	mov	dph,a
	movx	a,@dptr
	mov	r7,a
	mov	r0,_bp
	inc	r0
	mov	a,_bp
	add	a,#0x04
	mov	r1,a
	mov	a,@r1
	add	a,@r0
	mov	dpl,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	dph,a
	mov	a,_bp
	add	a,#0x03
	mov	r0,a
	movx	a,@dptr
	mov	@r0,a
	mov	a,_bp
	add	a,#0x04
	mov	r0,a
	mov	a,@r0
	add	a,r5
	mov	dpl,a
	clr	a
	addc	a,r6
	mov	dph,a
	movx	a,@dptr
	mov	r2,a
	push	ar7
	push	ar6
	push	ar5
	push	ar4
	push	ar3
	push	ar7
	mov	a,_bp
	add	a,#0x03
	mov	r0,a
	mov	a,@r0
	push	acc
	mov	dpl,r2
	lcall	_Send_2811_24bits
	dec	sp
	dec	sp
	pop	ar3
	pop	ar4
	pop	ar5
	pop	ar6
	pop	ar7
;	usr/main.c:185: for(i=PIXEL*2-1;i<COW-PIXEL*2+1;i++)
	mov	a,_bp
	add	a,#0x04
	mov	r0,a
	inc	@r0
	mov	a,_bp
	add	a,#0x04
	mov	r0,a
	cjne	@r0,#0x1c,00127$
00127$:
	pop	ar7
	jc	00103$
;	usr/main.c:183: for (j = 0; j < COLUMN_LED; j++)                          
	inc	r7
	cjne	r7,#0x0d,00129$
00129$:
	jnc	00130$
	ljmp	00109$
00130$:
;	usr/main.c:190: mDelaymS(50);
	mov	dptr,#0x0032
	lcall	_mDelaymS
;	usr/main.c:191: }
	mov	sp,_bp
	pop	_bp
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'main'
;------------------------------------------------------------
;m                         Allocated to registers 
;i                         Allocated to registers r6 r7 
;j                         Allocated to registers r6 r7 
;------------------------------------------------------------
;	usr/main.c:199: void main(void)
;	-----------------------------------------
;	 function main
;	-----------------------------------------
_main:
;	usr/main.c:202: CfgFsys();                        //CH549时钟选择配置
	lcall	_CfgFsys
;	usr/main.c:203: mDelaymS(20);
	mov	dptr,#0x0014
	lcall	_mDelaymS
;	usr/main.c:204: genColor();                       //颜色预设
	lcall	_genColor
;	usr/main.c:205: for ( j = 0; j < COLUMN_LED; j++)     //所有列的流水灯进入就绪状态
	mov	r6,#0x00
	mov	r7,#0x00
00113$:
;	usr/main.c:207: Column[j] = COW_LED+PIXEL*2-1;
	mov	a,r6
	add	a,#_Column
	mov	dpl,a
	mov	a,r7
	addc	a,#(_Column >> 8)
	mov	dph,a
	mov	a,#0x1c
	movx	@dptr,a
;	usr/main.c:205: for ( j = 0; j < COLUMN_LED; j++)     //所有列的流水灯进入就绪状态
	inc	r6
	cjne	r6,#0x00,00149$
	inc	r7
00149$:
	clr	c
	mov	a,r6
	subb	a,#0x0d
	mov	a,r7
	subb	a,#0x00
	jc	00113$
;	usr/main.c:211: while (1)
00111$:
;	usr/main.c:213: Buf_Put0();                    //数据条清0
	lcall	_Buf_Put0
;	usr/main.c:214: m = rand() % COLUMN_LED;       //取随机数
	lcall	_rand
	mov	r6,dpl
	mov	r7,dph
	mov	a,#0x0d
	push	acc
	clr	a
	push	acc
	mov	dpl,r6
	mov	dph,r7
	lcall	__modsint
	mov	r6,dpl
	mov	r7,dph
	dec	sp
	dec	sp
;	usr/main.c:215: if (Column[m] == COW_LED+PIXEL*2-1) {Column[m] = 0;}       //选中的列若以就绪开启流水灯
	mov	a,r6
	add	a,#_Column
	mov	r6,a
	mov	a,r7
	addc	a,#(_Column >> 8)
	mov	r7,a
	mov	dpl,r6
	mov	dph,r7
	movx	a,@dptr
	mov	r5,a
	cjne	r5,#0x1c,00123$
	mov	dpl,r6
	mov	dph,r7
	clr	a
	movx	@dptr,a
;	usr/main.c:216: for ( i = 0; i < COLUMN_LED; i++)                          //检测每一列的状态
00123$:
	mov	r6,#0x00
	mov	r7,#0x00
00115$:
;	usr/main.c:218: if (Column[i] < COW_LED+PIXEL*2-1)                     //若列数据未走完给该列赋值
	mov	a,r6
	add	a,#_Column
	mov	dpl,a
	mov	a,r7
	addc	a,#(_Column >> 8)
	mov	dph,a
	movx	a,@dptr
	mov	r5,a
	cjne	r5,#0x1c,00153$
00153$:
	jnc	00116$
;	usr/main.c:220: if (i % 2)                                         //判断单双列
	mov	a,r6
	jnb	acc.0,00105$
;	usr/main.c:222: Set_Color(i,COW_LED+PIXEL*2-1-Column[i]);      //双列
	mov	ar4,r5
	mov	a,#0x1c
	clr	c
	subb	a,r4
	mov	r4,a
	mov	ar3,r6
	push	ar7
	push	ar6
	push	ar4
	mov	dpl,r3
	lcall	_Set_Color
	dec	sp
	pop	ar6
	pop	ar7
	sjmp	00106$
00105$:
;	usr/main.c:226: Set_Color(i,Column[i]);                        //单列
	mov	ar4,r6
	push	ar7
	push	ar6
	push	ar5
	mov	dpl,r4
	lcall	_Set_Color
	dec	sp
	pop	ar6
	pop	ar7
00106$:
;	usr/main.c:228: Column[i]++;                                        //下次循环走下一个瞬间的数据
	mov	a,r6
	add	a,#_Column
	mov	r4,a
	mov	a,r7
	addc	a,#(_Column >> 8)
	mov	r5,a
	mov	dpl,r4
	mov	dph,r5
	movx	a,@dptr
	mov	r3,a
	inc	r3
	mov	dpl,r4
	mov	dph,r5
	mov	a,r3
	movx	@dptr,a
00116$:
;	usr/main.c:216: for ( i = 0; i < COLUMN_LED; i++)                          //检测每一列的状态
	inc	r6
	cjne	r6,#0x00,00156$
	inc	r7
00156$:
	clr	c
	mov	a,r6
	subb	a,#0x0d
	mov	a,r7
	subb	a,#0x00
	jc	00115$
;	usr/main.c:233: Send_ALL();                                                //将该时间数据发送显示
	lcall	_Send_ALL
;	usr/main.c:235: }
	ljmp	00111$
	.area CSEG    (CODE)
	.area CONST   (CODE)
	.area CABS    (ABS,CODE)
