/**
  ******************************************************************
  * @file    main.c
  * @author  Merlin	
  * @version V1.0
  * @date    2022-7-30
  * @brief   地铁板	
  ******************************************************************
  * @attention
  * verimake 用于ch549的进阶控制学习
  *
  ******************************************************************
  */
#include <CH549_sdcc.H>	   //ch549的头文件，其中定义了单片机的一些特殊功能寄存器
#include <CH549_DEBUG.h>   //CH549官方提供库的头文件，定义了一些关于主频，延时，串口设置，看门口，赋值设置等基础函数
#include <CH549_SubwayBoard.h>   //地铁板特供CH549头文件
// #include <stdlib.h>          //调用随机数函数

/********************************************************************
* 函 数 名       : main
* 函数功能		 : 主函数
* 输    入       : 无
* 输    出    	 : 无
*********************************************************************/
void main(void)
{
   UINT8 i;
	 LIGHT_Clean();

  /* 主循环 */
  while (1)
{
  LIGHT_Converge(40);
  LIGHT_Converge(40);
  LIGHT_Converge(40);
  mDelaymS(200);
  LIGHT_TLineOn();
	mDelaymS(200);
  LIGHT_Disperse(40);
  LIGHT_Disperse(40);
  LIGHT_Disperse(40);
  mDelaymS(200);
	LIGHT_FLineOn();
	mDelaymS(200);
  LIGHT_AllOn();
  mDelaymS(50);
  LIGHT_Clean();
  mDelaymS(50);
  LIGHT_AllOn();
  mDelaymS(50);
  LIGHT_Clean();
  mDelaymS(50);
  LIGHT_AllOn();
  mDelaymS(50);
  LIGHT_Clean();
  mDelaymS(50);
  LIGHT_Random(5);
  mDelaymS(200);
  for ( i = 0; i < 6; i++)
  {
    LIGHT_Converge(10 * (6 - i));

  }
  for ( i = 0; i < 6; i++)
  {
    LIGHT_Disperse(10 * (6 - i));

  }
  LIGHT_AllOn();
  mDelaymS(1000);
  
}
}
