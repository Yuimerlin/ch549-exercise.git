#ifndef __CH549_IIC_H__
#define __CH549_IIC_H__

#include "CH549_sdcc.h"       //ch549的头文件，其中定义了单片机的一些特殊功能寄存器
#include "CH549_DEBUG.h"      //CH549官方提供库的头文件，定义了一些关于主频，延时，串口设置，看门口，赋值设置等基础函数
#include "stdlib.h"           //调用随机数函数

/****************** 参数定义 ******************/
#define  LED_A1  P3_5
#define  LED_A2  P2_2
#define  LED_A3  P2_4
#define  LED_A4  P2_6
#define  LED_A5  P2_7
#define  LED_B1  P1_1
#define  LED_B2  P1_4
#define  LED_B3  P1_5
#define  LED_B4  P1_6
/****************** 外部调用子函数 ****************************/
extern UINT8 SubwayLine[6][7];
extern UINT8 SubwayCircle[4][9];
extern void LIGHT_ON(UINT8 LIGHT);
extern void LIGHT_OFF(UINT8 LIGHT);
extern void LIGHT_Flash(UINT8 LIGHT);
extern void LIGHT_Clean();
extern void LIGHT_AllOn();
extern void LIGHT_TLineOn();
extern void LIGHT_FLineOn();
extern void LIGHT_Converge(UINT16 FREQs);
extern void LIGHT_Disperse(UINT16 FREQs);
extern void LIGHT_Random(UINT8 FREQs);

#endif
