/********************************** (C) COPYRIGHT *******************************
* File Name          : CH549_SPARKROAD.C
* Author             : Yuimerlin
* Version            : V1.0
* Date               : 2022/7/20
* Description        : CH549 SPARKROAD相关函数
*******************************************************************************/
#include "CH549_SubwayBoard.h"

//地铁线路
UINT8 SubwayLine[6][7] = {
    { 10, 15, 14, 13, 12, 7 , 6 },
    { 5 , 4 , 3 , 8 , 13, 17, 16},
    { 20, 15, 9 , 8 , 2 , 7 , 11},
    { 19, 14, 9 , 5 , 0 , 0 , 0 },
    { 18, 17, 12, 13, 14, 15, 10},
    { 16, 7 , 1 , 0 , 0 , 0 , 0 },
    };

UINT8 SubwayCircle[4][9] = {
    { 1 , 5 , 6 , 10, 11, 16, 18, 19, 20},
    { 4 , 7 , 14, 15, 17, 0 , 0 , 0 , 0 },
    { 2 , 3 , 9 , 12, 13, 0 , 0 , 0 , 0 },
    { 8 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 },
    };

//亮灯
void LIGHT_ON(unsigned char LIGHT)
{
    switch (LIGHT)
    {
    case 1:LED_A1 = 1;LED_B1 = 0;break;
    case 2:LED_A2 = 1;LED_B1 = 0;break;
    case 3:LED_A3 = 1;LED_B1 = 0;break;
    case 4:LED_A4 = 1;LED_B1 = 0;break;
    case 5:LED_A5 = 1;LED_B1 = 0;break;
    case 6:LED_A1 = 1;LED_B2 = 0;break;
    case 7:LED_A2 = 1;LED_B2 = 0;break;
    case 8:LED_A3 = 1;LED_B2 = 0;break;
    case 9:LED_A4 = 1;LED_B2 = 0;break;
    case 10:LED_A5 = 1;LED_B2 = 0;break;
    case 11:LED_A1 = 1;LED_B3 = 0;break;
    case 12:LED_A2 = 1;LED_B3 = 0;break;
    case 13:LED_A3 = 1;LED_B3 = 0;break;
    case 14:LED_A4 = 1;LED_B3 = 0;break;
    case 15:LED_A5 = 1;LED_B3 = 0;break;
    case 16:LED_A1 = 1;LED_B4 = 0;break;
    case 17:LED_A2 = 1;LED_B4 = 0;break;
    case 18:LED_A3 = 1;LED_B4 = 0;break;
    case 19:LED_A4 = 1;LED_B4 = 0;break;
    case 20:LED_A5 = 1;LED_B4 = 0;break;
    }
}

//灭灯
void LIGHT_OFF(unsigned char LIGHT)
{
    switch (LIGHT)
    {
    case 1:LED_A1 = 0;LED_B1 = 1;break;
    case 2:LED_A2 = 0;LED_B1 = 1;break;
    case 3:LED_A3 = 0;LED_B1 = 1;break;
    case 4:LED_A4 = 0;LED_B1 = 1;break;
    case 5:LED_A5 = 0;LED_B1 = 1;break;
    case 6:LED_A1 = 0;LED_B2 = 1;break;
    case 7:LED_A2 = 0;LED_B2 = 1;break;
    case 8:LED_A3 = 0;LED_B2 = 1;break;
    case 9:LED_A4 = 0;LED_B2 = 1;break;
    case 10:LED_A5 = 0;LED_B2 = 1;break;
    case 11:LED_A1 = 0;LED_B3 = 1;break;
    case 12:LED_A2 = 0;LED_B3 = 1;break;
    case 13:LED_A3 = 0;LED_B3 = 1;break;
    case 14:LED_A4 = 0;LED_B3 = 1;break;
    case 15:LED_A5 = 0;LED_B3 = 1;break;
    case 16:LED_A1 = 0;LED_B4 = 1;break;
    case 17:LED_A2 = 0;LED_B4 = 1;break;
    case 18:LED_A3 = 0;LED_B4 = 1;break;
    case 19:LED_A4 = 0;LED_B4 = 1;break;
    case 20:LED_A5 = 0;LED_B4 = 1;break;
    }
}

//灯闪烁
void LIGHT_Flash(UINT8 LIGHT)
{
    LIGHT_ON(LIGHT);
    mDelayuS(100);
    LIGHT_OFF(LIGHT);

}

//清屏
void LIGHT_Clean()
{
    for (UINT8 LIGHT = 1; LIGHT < 21; LIGHT++)
    {
        LIGHT_OFF(LIGHT);
    }
    
}

//灯全开
void LIGHT_AllOn()
{
    for (UINT8 LIGHT = 1; LIGHT < 21; LIGHT++)
    {
        LIGHT_ON(LIGHT);
    }
    
}

//正方向按线路逐渐点亮再熄灭
void LIGHT_TLineOn()
{
    UINT8 DOT,LINE;
    for ( LINE = 0; LINE < 6; LINE++)
    {
        for ( DOT = 0; DOT < 7; DOT++)
        {
            if (SubwayLine[LINE][DOT])
            {
                LIGHT_ON(SubwayLine[LINE][DOT]);
                mDelaymS(50);
                LIGHT_OFF(SubwayLine[LINE][DOT]);

            }
            
        }
        
    }
    
}

//反方向按线路逐渐点亮再熄灭
void LIGHT_FLineOn()
{
    UINT8 DOT,LINE;
    for ( LINE = 0; LINE < 6; LINE++)
    {
        for ( DOT = 0; DOT < 7; DOT++)
        {
            if (SubwayLine[LINE][6-DOT])
            {
                LIGHT_ON(SubwayLine[LINE][6-DOT]);
                mDelaymS(50);
                LIGHT_OFF(SubwayLine[LINE][6-DOT]);

            }
            
        }
        
    }
    
}

//灯聚合
void LIGHT_Converge(UINT16 FREQs)
{
    float TIME[4] = {1 ,1.8, 1.8, 9};
    UINT8 DOT,CIRCLE;
    UINT16 FREQ;

    for ( CIRCLE = 0; CIRCLE < 4; CIRCLE++)
    {
      for ( FREQ = 0; FREQ < FREQs * TIME[CIRCLE]; FREQ++)
      {
          for ( DOT = 0; DOT < 9; DOT++)
          {
              if(SubwayCircle[CIRCLE][DOT])
              {
                  LIGHT_Flash(SubwayCircle[CIRCLE][DOT]);

              }

          }

      }

  }

}

//灯发散
void LIGHT_Disperse(UINT16 FREQs)
{
    float TIME[4] = {1 ,1.8, 1.8, 9};
    UINT8 DOT,CIRCLE;
    UINT16 FREQ;

    for ( CIRCLE = 0; CIRCLE < 4; CIRCLE++)
    {
      for ( FREQ = 0; FREQ < FREQs * TIME[3 - CIRCLE]; FREQ++)
      {
          for ( DOT = 0; DOT < 9; DOT++)
          {
              if(SubwayCircle[3 - CIRCLE][DOT])
              {
                  LIGHT_Flash(SubwayCircle[3 - CIRCLE][DOT]);

              }

          }

      }

  }

}

//随机跑灯
void LIGHT_Random(UINT8 FREQs)
{
    UINT8 i,m,DOT;
    for ( i = 0; i < FREQs; i++)
    {
        if (rand() % 2)
        {
            m = rand() % 6;
            for ( DOT = 0; DOT < 7; DOT++)
            {
                if (SubwayLine[m][DOT])
                {
                    LIGHT_ON(SubwayLine[m][DOT]);
                    mDelaymS(50);
                    LIGHT_OFF(SubwayLine[m][DOT]);

                }
            }
            
        }else
        {
            m = rand() % 6;
            for ( DOT = 0; DOT < 7; DOT++)
            {
                if (SubwayLine[m][6 - DOT])
                {
                    LIGHT_ON(SubwayLine[m][6 - DOT]);
                    mDelaymS(50);
                    LIGHT_OFF(SubwayLine[m][6 - DOT]);

                }

            }

        }
        mDelaymS(150);
        
    }
    
}
