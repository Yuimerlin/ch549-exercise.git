                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 4.0.0 #11528 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module CH549_SubwayBoard
                                      6 	.optsdcc -mmcs51 --model-large
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _rand
                                     12 	.globl _mDelaymS
                                     13 	.globl _mDelayuS
                                     14 	.globl _UIF_BUS_RST
                                     15 	.globl _UIF_DETECT
                                     16 	.globl _UIF_TRANSFER
                                     17 	.globl _UIF_SUSPEND
                                     18 	.globl _UIF_HST_SOF
                                     19 	.globl _UIF_FIFO_OV
                                     20 	.globl _U_SIE_FREE
                                     21 	.globl _U_TOG_OK
                                     22 	.globl _U_IS_NAK
                                     23 	.globl _S0_R_FIFO
                                     24 	.globl _S0_T_FIFO
                                     25 	.globl _S0_FREE
                                     26 	.globl _S0_IF_BYTE
                                     27 	.globl _S0_IF_FIRST
                                     28 	.globl _S0_IF_OV
                                     29 	.globl _S0_FST_ACT
                                     30 	.globl _CP_RL2
                                     31 	.globl _C_T2
                                     32 	.globl _TR2
                                     33 	.globl _EXEN2
                                     34 	.globl _TCLK
                                     35 	.globl _RCLK
                                     36 	.globl _EXF2
                                     37 	.globl _CAP1F
                                     38 	.globl _TF2
                                     39 	.globl _RI
                                     40 	.globl _TI
                                     41 	.globl _RB8
                                     42 	.globl _TB8
                                     43 	.globl _REN
                                     44 	.globl _SM2
                                     45 	.globl _SM1
                                     46 	.globl _SM0
                                     47 	.globl _IT0
                                     48 	.globl _IE0
                                     49 	.globl _IT1
                                     50 	.globl _IE1
                                     51 	.globl _TR0
                                     52 	.globl _TF0
                                     53 	.globl _TR1
                                     54 	.globl _TF1
                                     55 	.globl _XI
                                     56 	.globl _XO
                                     57 	.globl _P4_0
                                     58 	.globl _P4_1
                                     59 	.globl _P4_2
                                     60 	.globl _P4_3
                                     61 	.globl _P4_4
                                     62 	.globl _P4_5
                                     63 	.globl _P4_6
                                     64 	.globl _RXD
                                     65 	.globl _TXD
                                     66 	.globl _INT0
                                     67 	.globl _INT1
                                     68 	.globl _T0
                                     69 	.globl _T1
                                     70 	.globl _CAP0
                                     71 	.globl _INT3
                                     72 	.globl _P3_0
                                     73 	.globl _P3_1
                                     74 	.globl _P3_2
                                     75 	.globl _P3_3
                                     76 	.globl _P3_4
                                     77 	.globl _P3_5
                                     78 	.globl _P3_6
                                     79 	.globl _P3_7
                                     80 	.globl _PWM5
                                     81 	.globl _PWM4
                                     82 	.globl _INT0_
                                     83 	.globl _PWM3
                                     84 	.globl _PWM2
                                     85 	.globl _CAP1_
                                     86 	.globl _T2_
                                     87 	.globl _PWM1
                                     88 	.globl _CAP2_
                                     89 	.globl _T2EX_
                                     90 	.globl _PWM0
                                     91 	.globl _RXD1
                                     92 	.globl _PWM6
                                     93 	.globl _TXD1
                                     94 	.globl _PWM7
                                     95 	.globl _P2_0
                                     96 	.globl _P2_1
                                     97 	.globl _P2_2
                                     98 	.globl _P2_3
                                     99 	.globl _P2_4
                                    100 	.globl _P2_5
                                    101 	.globl _P2_6
                                    102 	.globl _P2_7
                                    103 	.globl _AIN0
                                    104 	.globl _CAP1
                                    105 	.globl _T2
                                    106 	.globl _AIN1
                                    107 	.globl _CAP2
                                    108 	.globl _T2EX
                                    109 	.globl _AIN2
                                    110 	.globl _AIN3
                                    111 	.globl _AIN4
                                    112 	.globl _UCC1
                                    113 	.globl _SCS
                                    114 	.globl _AIN5
                                    115 	.globl _UCC2
                                    116 	.globl _PWM0_
                                    117 	.globl _MOSI
                                    118 	.globl _AIN6
                                    119 	.globl _VBUS
                                    120 	.globl _RXD1_
                                    121 	.globl _MISO
                                    122 	.globl _AIN7
                                    123 	.globl _TXD1_
                                    124 	.globl _SCK
                                    125 	.globl _P1_0
                                    126 	.globl _P1_1
                                    127 	.globl _P1_2
                                    128 	.globl _P1_3
                                    129 	.globl _P1_4
                                    130 	.globl _P1_5
                                    131 	.globl _P1_6
                                    132 	.globl _P1_7
                                    133 	.globl _AIN8
                                    134 	.globl _AIN9
                                    135 	.globl _AIN10
                                    136 	.globl _RXD_
                                    137 	.globl _AIN11
                                    138 	.globl _TXD_
                                    139 	.globl _AIN12
                                    140 	.globl _RXD2
                                    141 	.globl _AIN13
                                    142 	.globl _TXD2
                                    143 	.globl _AIN14
                                    144 	.globl _RXD3
                                    145 	.globl _AIN15
                                    146 	.globl _TXD3
                                    147 	.globl _P0_0
                                    148 	.globl _P0_1
                                    149 	.globl _P0_2
                                    150 	.globl _P0_3
                                    151 	.globl _P0_4
                                    152 	.globl _P0_5
                                    153 	.globl _P0_6
                                    154 	.globl _P0_7
                                    155 	.globl _IE_SPI0
                                    156 	.globl _IE_INT3
                                    157 	.globl _IE_USB
                                    158 	.globl _IE_UART2
                                    159 	.globl _IE_ADC
                                    160 	.globl _IE_UART1
                                    161 	.globl _IE_UART3
                                    162 	.globl _IE_PWMX
                                    163 	.globl _IE_GPIO
                                    164 	.globl _IE_WDOG
                                    165 	.globl _PX0
                                    166 	.globl _PT0
                                    167 	.globl _PX1
                                    168 	.globl _PT1
                                    169 	.globl _PS
                                    170 	.globl _PT2
                                    171 	.globl _PL_FLAG
                                    172 	.globl _PH_FLAG
                                    173 	.globl _EX0
                                    174 	.globl _ET0
                                    175 	.globl _EX1
                                    176 	.globl _ET1
                                    177 	.globl _ES
                                    178 	.globl _ET2
                                    179 	.globl _E_DIS
                                    180 	.globl _EA
                                    181 	.globl _P
                                    182 	.globl _F1
                                    183 	.globl _OV
                                    184 	.globl _RS0
                                    185 	.globl _RS1
                                    186 	.globl _F0
                                    187 	.globl _AC
                                    188 	.globl _CY
                                    189 	.globl _UEP1_DMA_H
                                    190 	.globl _UEP1_DMA_L
                                    191 	.globl _UEP1_DMA
                                    192 	.globl _UEP0_DMA_H
                                    193 	.globl _UEP0_DMA_L
                                    194 	.globl _UEP0_DMA
                                    195 	.globl _UEP2_3_MOD
                                    196 	.globl _UEP4_1_MOD
                                    197 	.globl _UEP3_DMA_H
                                    198 	.globl _UEP3_DMA_L
                                    199 	.globl _UEP3_DMA
                                    200 	.globl _UEP2_DMA_H
                                    201 	.globl _UEP2_DMA_L
                                    202 	.globl _UEP2_DMA
                                    203 	.globl _USB_DEV_AD
                                    204 	.globl _USB_CTRL
                                    205 	.globl _USB_INT_EN
                                    206 	.globl _UEP4_T_LEN
                                    207 	.globl _UEP4_CTRL
                                    208 	.globl _UEP0_T_LEN
                                    209 	.globl _UEP0_CTRL
                                    210 	.globl _USB_RX_LEN
                                    211 	.globl _USB_MIS_ST
                                    212 	.globl _USB_INT_ST
                                    213 	.globl _USB_INT_FG
                                    214 	.globl _UEP3_T_LEN
                                    215 	.globl _UEP3_CTRL
                                    216 	.globl _UEP2_T_LEN
                                    217 	.globl _UEP2_CTRL
                                    218 	.globl _UEP1_T_LEN
                                    219 	.globl _UEP1_CTRL
                                    220 	.globl _UDEV_CTRL
                                    221 	.globl _USB_C_CTRL
                                    222 	.globl _ADC_PIN
                                    223 	.globl _ADC_CHAN
                                    224 	.globl _ADC_DAT_H
                                    225 	.globl _ADC_DAT_L
                                    226 	.globl _ADC_DAT
                                    227 	.globl _ADC_CFG
                                    228 	.globl _ADC_CTRL
                                    229 	.globl _TKEY_CTRL
                                    230 	.globl _SIF3
                                    231 	.globl _SBAUD3
                                    232 	.globl _SBUF3
                                    233 	.globl _SCON3
                                    234 	.globl _SIF2
                                    235 	.globl _SBAUD2
                                    236 	.globl _SBUF2
                                    237 	.globl _SCON2
                                    238 	.globl _SIF1
                                    239 	.globl _SBAUD1
                                    240 	.globl _SBUF1
                                    241 	.globl _SCON1
                                    242 	.globl _SPI0_SETUP
                                    243 	.globl _SPI0_CK_SE
                                    244 	.globl _SPI0_CTRL
                                    245 	.globl _SPI0_DATA
                                    246 	.globl _SPI0_STAT
                                    247 	.globl _PWM_DATA7
                                    248 	.globl _PWM_DATA6
                                    249 	.globl _PWM_DATA5
                                    250 	.globl _PWM_DATA4
                                    251 	.globl _PWM_DATA3
                                    252 	.globl _PWM_CTRL2
                                    253 	.globl _PWM_CK_SE
                                    254 	.globl _PWM_CTRL
                                    255 	.globl _PWM_DATA0
                                    256 	.globl _PWM_DATA1
                                    257 	.globl _PWM_DATA2
                                    258 	.globl _T2CAP1H
                                    259 	.globl _T2CAP1L
                                    260 	.globl _T2CAP1
                                    261 	.globl _TH2
                                    262 	.globl _TL2
                                    263 	.globl _T2COUNT
                                    264 	.globl _RCAP2H
                                    265 	.globl _RCAP2L
                                    266 	.globl _RCAP2
                                    267 	.globl _T2MOD
                                    268 	.globl _T2CON
                                    269 	.globl _T2CAP0H
                                    270 	.globl _T2CAP0L
                                    271 	.globl _T2CAP0
                                    272 	.globl _T2CON2
                                    273 	.globl _SBUF
                                    274 	.globl _SCON
                                    275 	.globl _TH1
                                    276 	.globl _TH0
                                    277 	.globl _TL1
                                    278 	.globl _TL0
                                    279 	.globl _TMOD
                                    280 	.globl _TCON
                                    281 	.globl _XBUS_AUX
                                    282 	.globl _PIN_FUNC
                                    283 	.globl _P5
                                    284 	.globl _P4_DIR_PU
                                    285 	.globl _P4_MOD_OC
                                    286 	.globl _P4
                                    287 	.globl _P3_DIR_PU
                                    288 	.globl _P3_MOD_OC
                                    289 	.globl _P3
                                    290 	.globl _P2_DIR_PU
                                    291 	.globl _P2_MOD_OC
                                    292 	.globl _P2
                                    293 	.globl _P1_DIR_PU
                                    294 	.globl _P1_MOD_OC
                                    295 	.globl _P1
                                    296 	.globl _P0_DIR_PU
                                    297 	.globl _P0_MOD_OC
                                    298 	.globl _P0
                                    299 	.globl _ROM_CTRL
                                    300 	.globl _ROM_DATA_HH
                                    301 	.globl _ROM_DATA_HL
                                    302 	.globl _ROM_DATA_HI
                                    303 	.globl _ROM_ADDR_H
                                    304 	.globl _ROM_ADDR_L
                                    305 	.globl _ROM_ADDR
                                    306 	.globl _GPIO_IE
                                    307 	.globl _INTX
                                    308 	.globl _IP_EX
                                    309 	.globl _IE_EX
                                    310 	.globl _IP
                                    311 	.globl _IE
                                    312 	.globl _WDOG_COUNT
                                    313 	.globl _RESET_KEEP
                                    314 	.globl _WAKE_CTRL
                                    315 	.globl _CLOCK_CFG
                                    316 	.globl _POWER_CFG
                                    317 	.globl _PCON
                                    318 	.globl _GLOBAL_CFG
                                    319 	.globl _SAFE_MOD
                                    320 	.globl _DPH
                                    321 	.globl _DPL
                                    322 	.globl _SP
                                    323 	.globl _A_INV
                                    324 	.globl _B
                                    325 	.globl _ACC
                                    326 	.globl _PSW
                                    327 	.globl _SubwayCircle
                                    328 	.globl _SubwayLine
                                    329 	.globl _LIGHT_ON
                                    330 	.globl _LIGHT_OFF
                                    331 	.globl _LIGHT_Flash
                                    332 	.globl _LIGHT_Clean
                                    333 	.globl _LIGHT_AllOn
                                    334 	.globl _LIGHT_TLineOn
                                    335 	.globl _LIGHT_FLineOn
                                    336 	.globl _LIGHT_Converge
                                    337 	.globl _LIGHT_Disperse
                                    338 	.globl _LIGHT_Random
                                    339 ;--------------------------------------------------------
                                    340 ; special function registers
                                    341 ;--------------------------------------------------------
                                    342 	.area RSEG    (ABS,DATA)
      000000                        343 	.org 0x0000
                           0000D0   344 _PSW	=	0x00d0
                           0000E0   345 _ACC	=	0x00e0
                           0000F0   346 _B	=	0x00f0
                           0000FD   347 _A_INV	=	0x00fd
                           000081   348 _SP	=	0x0081
                           000082   349 _DPL	=	0x0082
                           000083   350 _DPH	=	0x0083
                           0000A1   351 _SAFE_MOD	=	0x00a1
                           0000B1   352 _GLOBAL_CFG	=	0x00b1
                           000087   353 _PCON	=	0x0087
                           0000BA   354 _POWER_CFG	=	0x00ba
                           0000B9   355 _CLOCK_CFG	=	0x00b9
                           0000A9   356 _WAKE_CTRL	=	0x00a9
                           0000FE   357 _RESET_KEEP	=	0x00fe
                           0000FF   358 _WDOG_COUNT	=	0x00ff
                           0000A8   359 _IE	=	0x00a8
                           0000B8   360 _IP	=	0x00b8
                           0000E8   361 _IE_EX	=	0x00e8
                           0000E9   362 _IP_EX	=	0x00e9
                           0000B3   363 _INTX	=	0x00b3
                           0000B2   364 _GPIO_IE	=	0x00b2
                           008584   365 _ROM_ADDR	=	0x8584
                           000084   366 _ROM_ADDR_L	=	0x0084
                           000085   367 _ROM_ADDR_H	=	0x0085
                           008F8E   368 _ROM_DATA_HI	=	0x8f8e
                           00008E   369 _ROM_DATA_HL	=	0x008e
                           00008F   370 _ROM_DATA_HH	=	0x008f
                           000086   371 _ROM_CTRL	=	0x0086
                           000080   372 _P0	=	0x0080
                           0000C4   373 _P0_MOD_OC	=	0x00c4
                           0000C5   374 _P0_DIR_PU	=	0x00c5
                           000090   375 _P1	=	0x0090
                           000092   376 _P1_MOD_OC	=	0x0092
                           000093   377 _P1_DIR_PU	=	0x0093
                           0000A0   378 _P2	=	0x00a0
                           000094   379 _P2_MOD_OC	=	0x0094
                           000095   380 _P2_DIR_PU	=	0x0095
                           0000B0   381 _P3	=	0x00b0
                           000096   382 _P3_MOD_OC	=	0x0096
                           000097   383 _P3_DIR_PU	=	0x0097
                           0000C0   384 _P4	=	0x00c0
                           0000C2   385 _P4_MOD_OC	=	0x00c2
                           0000C3   386 _P4_DIR_PU	=	0x00c3
                           0000AB   387 _P5	=	0x00ab
                           0000AA   388 _PIN_FUNC	=	0x00aa
                           0000A2   389 _XBUS_AUX	=	0x00a2
                           000088   390 _TCON	=	0x0088
                           000089   391 _TMOD	=	0x0089
                           00008A   392 _TL0	=	0x008a
                           00008B   393 _TL1	=	0x008b
                           00008C   394 _TH0	=	0x008c
                           00008D   395 _TH1	=	0x008d
                           000098   396 _SCON	=	0x0098
                           000099   397 _SBUF	=	0x0099
                           0000C1   398 _T2CON2	=	0x00c1
                           00C7C6   399 _T2CAP0	=	0xc7c6
                           0000C6   400 _T2CAP0L	=	0x00c6
                           0000C7   401 _T2CAP0H	=	0x00c7
                           0000C8   402 _T2CON	=	0x00c8
                           0000C9   403 _T2MOD	=	0x00c9
                           00CBCA   404 _RCAP2	=	0xcbca
                           0000CA   405 _RCAP2L	=	0x00ca
                           0000CB   406 _RCAP2H	=	0x00cb
                           00CDCC   407 _T2COUNT	=	0xcdcc
                           0000CC   408 _TL2	=	0x00cc
                           0000CD   409 _TH2	=	0x00cd
                           00CFCE   410 _T2CAP1	=	0xcfce
                           0000CE   411 _T2CAP1L	=	0x00ce
                           0000CF   412 _T2CAP1H	=	0x00cf
                           00009A   413 _PWM_DATA2	=	0x009a
                           00009B   414 _PWM_DATA1	=	0x009b
                           00009C   415 _PWM_DATA0	=	0x009c
                           00009D   416 _PWM_CTRL	=	0x009d
                           00009E   417 _PWM_CK_SE	=	0x009e
                           00009F   418 _PWM_CTRL2	=	0x009f
                           0000A3   419 _PWM_DATA3	=	0x00a3
                           0000A4   420 _PWM_DATA4	=	0x00a4
                           0000A5   421 _PWM_DATA5	=	0x00a5
                           0000A6   422 _PWM_DATA6	=	0x00a6
                           0000A7   423 _PWM_DATA7	=	0x00a7
                           0000F8   424 _SPI0_STAT	=	0x00f8
                           0000F9   425 _SPI0_DATA	=	0x00f9
                           0000FA   426 _SPI0_CTRL	=	0x00fa
                           0000FB   427 _SPI0_CK_SE	=	0x00fb
                           0000FC   428 _SPI0_SETUP	=	0x00fc
                           0000BC   429 _SCON1	=	0x00bc
                           0000BD   430 _SBUF1	=	0x00bd
                           0000BE   431 _SBAUD1	=	0x00be
                           0000BF   432 _SIF1	=	0x00bf
                           0000B4   433 _SCON2	=	0x00b4
                           0000B5   434 _SBUF2	=	0x00b5
                           0000B6   435 _SBAUD2	=	0x00b6
                           0000B7   436 _SIF2	=	0x00b7
                           0000AC   437 _SCON3	=	0x00ac
                           0000AD   438 _SBUF3	=	0x00ad
                           0000AE   439 _SBAUD3	=	0x00ae
                           0000AF   440 _SIF3	=	0x00af
                           0000F1   441 _TKEY_CTRL	=	0x00f1
                           0000F2   442 _ADC_CTRL	=	0x00f2
                           0000F3   443 _ADC_CFG	=	0x00f3
                           00F5F4   444 _ADC_DAT	=	0xf5f4
                           0000F4   445 _ADC_DAT_L	=	0x00f4
                           0000F5   446 _ADC_DAT_H	=	0x00f5
                           0000F6   447 _ADC_CHAN	=	0x00f6
                           0000F7   448 _ADC_PIN	=	0x00f7
                           000091   449 _USB_C_CTRL	=	0x0091
                           0000D1   450 _UDEV_CTRL	=	0x00d1
                           0000D2   451 _UEP1_CTRL	=	0x00d2
                           0000D3   452 _UEP1_T_LEN	=	0x00d3
                           0000D4   453 _UEP2_CTRL	=	0x00d4
                           0000D5   454 _UEP2_T_LEN	=	0x00d5
                           0000D6   455 _UEP3_CTRL	=	0x00d6
                           0000D7   456 _UEP3_T_LEN	=	0x00d7
                           0000D8   457 _USB_INT_FG	=	0x00d8
                           0000D9   458 _USB_INT_ST	=	0x00d9
                           0000DA   459 _USB_MIS_ST	=	0x00da
                           0000DB   460 _USB_RX_LEN	=	0x00db
                           0000DC   461 _UEP0_CTRL	=	0x00dc
                           0000DD   462 _UEP0_T_LEN	=	0x00dd
                           0000DE   463 _UEP4_CTRL	=	0x00de
                           0000DF   464 _UEP4_T_LEN	=	0x00df
                           0000E1   465 _USB_INT_EN	=	0x00e1
                           0000E2   466 _USB_CTRL	=	0x00e2
                           0000E3   467 _USB_DEV_AD	=	0x00e3
                           00E5E4   468 _UEP2_DMA	=	0xe5e4
                           0000E4   469 _UEP2_DMA_L	=	0x00e4
                           0000E5   470 _UEP2_DMA_H	=	0x00e5
                           00E7E6   471 _UEP3_DMA	=	0xe7e6
                           0000E6   472 _UEP3_DMA_L	=	0x00e6
                           0000E7   473 _UEP3_DMA_H	=	0x00e7
                           0000EA   474 _UEP4_1_MOD	=	0x00ea
                           0000EB   475 _UEP2_3_MOD	=	0x00eb
                           00EDEC   476 _UEP0_DMA	=	0xedec
                           0000EC   477 _UEP0_DMA_L	=	0x00ec
                           0000ED   478 _UEP0_DMA_H	=	0x00ed
                           00EFEE   479 _UEP1_DMA	=	0xefee
                           0000EE   480 _UEP1_DMA_L	=	0x00ee
                           0000EF   481 _UEP1_DMA_H	=	0x00ef
                                    482 ;--------------------------------------------------------
                                    483 ; special function bits
                                    484 ;--------------------------------------------------------
                                    485 	.area RSEG    (ABS,DATA)
      000000                        486 	.org 0x0000
                           0000D7   487 _CY	=	0x00d7
                           0000D6   488 _AC	=	0x00d6
                           0000D5   489 _F0	=	0x00d5
                           0000D4   490 _RS1	=	0x00d4
                           0000D3   491 _RS0	=	0x00d3
                           0000D2   492 _OV	=	0x00d2
                           0000D1   493 _F1	=	0x00d1
                           0000D0   494 _P	=	0x00d0
                           0000AF   495 _EA	=	0x00af
                           0000AE   496 _E_DIS	=	0x00ae
                           0000AD   497 _ET2	=	0x00ad
                           0000AC   498 _ES	=	0x00ac
                           0000AB   499 _ET1	=	0x00ab
                           0000AA   500 _EX1	=	0x00aa
                           0000A9   501 _ET0	=	0x00a9
                           0000A8   502 _EX0	=	0x00a8
                           0000BF   503 _PH_FLAG	=	0x00bf
                           0000BE   504 _PL_FLAG	=	0x00be
                           0000BD   505 _PT2	=	0x00bd
                           0000BC   506 _PS	=	0x00bc
                           0000BB   507 _PT1	=	0x00bb
                           0000BA   508 _PX1	=	0x00ba
                           0000B9   509 _PT0	=	0x00b9
                           0000B8   510 _PX0	=	0x00b8
                           0000EF   511 _IE_WDOG	=	0x00ef
                           0000EE   512 _IE_GPIO	=	0x00ee
                           0000ED   513 _IE_PWMX	=	0x00ed
                           0000ED   514 _IE_UART3	=	0x00ed
                           0000EC   515 _IE_UART1	=	0x00ec
                           0000EB   516 _IE_ADC	=	0x00eb
                           0000EB   517 _IE_UART2	=	0x00eb
                           0000EA   518 _IE_USB	=	0x00ea
                           0000E9   519 _IE_INT3	=	0x00e9
                           0000E8   520 _IE_SPI0	=	0x00e8
                           000087   521 _P0_7	=	0x0087
                           000086   522 _P0_6	=	0x0086
                           000085   523 _P0_5	=	0x0085
                           000084   524 _P0_4	=	0x0084
                           000083   525 _P0_3	=	0x0083
                           000082   526 _P0_2	=	0x0082
                           000081   527 _P0_1	=	0x0081
                           000080   528 _P0_0	=	0x0080
                           000087   529 _TXD3	=	0x0087
                           000087   530 _AIN15	=	0x0087
                           000086   531 _RXD3	=	0x0086
                           000086   532 _AIN14	=	0x0086
                           000085   533 _TXD2	=	0x0085
                           000085   534 _AIN13	=	0x0085
                           000084   535 _RXD2	=	0x0084
                           000084   536 _AIN12	=	0x0084
                           000083   537 _TXD_	=	0x0083
                           000083   538 _AIN11	=	0x0083
                           000082   539 _RXD_	=	0x0082
                           000082   540 _AIN10	=	0x0082
                           000081   541 _AIN9	=	0x0081
                           000080   542 _AIN8	=	0x0080
                           000097   543 _P1_7	=	0x0097
                           000096   544 _P1_6	=	0x0096
                           000095   545 _P1_5	=	0x0095
                           000094   546 _P1_4	=	0x0094
                           000093   547 _P1_3	=	0x0093
                           000092   548 _P1_2	=	0x0092
                           000091   549 _P1_1	=	0x0091
                           000090   550 _P1_0	=	0x0090
                           000097   551 _SCK	=	0x0097
                           000097   552 _TXD1_	=	0x0097
                           000097   553 _AIN7	=	0x0097
                           000096   554 _MISO	=	0x0096
                           000096   555 _RXD1_	=	0x0096
                           000096   556 _VBUS	=	0x0096
                           000096   557 _AIN6	=	0x0096
                           000095   558 _MOSI	=	0x0095
                           000095   559 _PWM0_	=	0x0095
                           000095   560 _UCC2	=	0x0095
                           000095   561 _AIN5	=	0x0095
                           000094   562 _SCS	=	0x0094
                           000094   563 _UCC1	=	0x0094
                           000094   564 _AIN4	=	0x0094
                           000093   565 _AIN3	=	0x0093
                           000092   566 _AIN2	=	0x0092
                           000091   567 _T2EX	=	0x0091
                           000091   568 _CAP2	=	0x0091
                           000091   569 _AIN1	=	0x0091
                           000090   570 _T2	=	0x0090
                           000090   571 _CAP1	=	0x0090
                           000090   572 _AIN0	=	0x0090
                           0000A7   573 _P2_7	=	0x00a7
                           0000A6   574 _P2_6	=	0x00a6
                           0000A5   575 _P2_5	=	0x00a5
                           0000A4   576 _P2_4	=	0x00a4
                           0000A3   577 _P2_3	=	0x00a3
                           0000A2   578 _P2_2	=	0x00a2
                           0000A1   579 _P2_1	=	0x00a1
                           0000A0   580 _P2_0	=	0x00a0
                           0000A7   581 _PWM7	=	0x00a7
                           0000A7   582 _TXD1	=	0x00a7
                           0000A6   583 _PWM6	=	0x00a6
                           0000A6   584 _RXD1	=	0x00a6
                           0000A5   585 _PWM0	=	0x00a5
                           0000A5   586 _T2EX_	=	0x00a5
                           0000A5   587 _CAP2_	=	0x00a5
                           0000A4   588 _PWM1	=	0x00a4
                           0000A4   589 _T2_	=	0x00a4
                           0000A4   590 _CAP1_	=	0x00a4
                           0000A3   591 _PWM2	=	0x00a3
                           0000A2   592 _PWM3	=	0x00a2
                           0000A2   593 _INT0_	=	0x00a2
                           0000A1   594 _PWM4	=	0x00a1
                           0000A0   595 _PWM5	=	0x00a0
                           0000B7   596 _P3_7	=	0x00b7
                           0000B6   597 _P3_6	=	0x00b6
                           0000B5   598 _P3_5	=	0x00b5
                           0000B4   599 _P3_4	=	0x00b4
                           0000B3   600 _P3_3	=	0x00b3
                           0000B2   601 _P3_2	=	0x00b2
                           0000B1   602 _P3_1	=	0x00b1
                           0000B0   603 _P3_0	=	0x00b0
                           0000B7   604 _INT3	=	0x00b7
                           0000B6   605 _CAP0	=	0x00b6
                           0000B5   606 _T1	=	0x00b5
                           0000B4   607 _T0	=	0x00b4
                           0000B3   608 _INT1	=	0x00b3
                           0000B2   609 _INT0	=	0x00b2
                           0000B1   610 _TXD	=	0x00b1
                           0000B0   611 _RXD	=	0x00b0
                           0000C6   612 _P4_6	=	0x00c6
                           0000C5   613 _P4_5	=	0x00c5
                           0000C4   614 _P4_4	=	0x00c4
                           0000C3   615 _P4_3	=	0x00c3
                           0000C2   616 _P4_2	=	0x00c2
                           0000C1   617 _P4_1	=	0x00c1
                           0000C0   618 _P4_0	=	0x00c0
                           0000C7   619 _XO	=	0x00c7
                           0000C6   620 _XI	=	0x00c6
                           00008F   621 _TF1	=	0x008f
                           00008E   622 _TR1	=	0x008e
                           00008D   623 _TF0	=	0x008d
                           00008C   624 _TR0	=	0x008c
                           00008B   625 _IE1	=	0x008b
                           00008A   626 _IT1	=	0x008a
                           000089   627 _IE0	=	0x0089
                           000088   628 _IT0	=	0x0088
                           00009F   629 _SM0	=	0x009f
                           00009E   630 _SM1	=	0x009e
                           00009D   631 _SM2	=	0x009d
                           00009C   632 _REN	=	0x009c
                           00009B   633 _TB8	=	0x009b
                           00009A   634 _RB8	=	0x009a
                           000099   635 _TI	=	0x0099
                           000098   636 _RI	=	0x0098
                           0000CF   637 _TF2	=	0x00cf
                           0000CF   638 _CAP1F	=	0x00cf
                           0000CE   639 _EXF2	=	0x00ce
                           0000CD   640 _RCLK	=	0x00cd
                           0000CC   641 _TCLK	=	0x00cc
                           0000CB   642 _EXEN2	=	0x00cb
                           0000CA   643 _TR2	=	0x00ca
                           0000C9   644 _C_T2	=	0x00c9
                           0000C8   645 _CP_RL2	=	0x00c8
                           0000FF   646 _S0_FST_ACT	=	0x00ff
                           0000FE   647 _S0_IF_OV	=	0x00fe
                           0000FD   648 _S0_IF_FIRST	=	0x00fd
                           0000FC   649 _S0_IF_BYTE	=	0x00fc
                           0000FB   650 _S0_FREE	=	0x00fb
                           0000FA   651 _S0_T_FIFO	=	0x00fa
                           0000F8   652 _S0_R_FIFO	=	0x00f8
                           0000DF   653 _U_IS_NAK	=	0x00df
                           0000DE   654 _U_TOG_OK	=	0x00de
                           0000DD   655 _U_SIE_FREE	=	0x00dd
                           0000DC   656 _UIF_FIFO_OV	=	0x00dc
                           0000DB   657 _UIF_HST_SOF	=	0x00db
                           0000DA   658 _UIF_SUSPEND	=	0x00da
                           0000D9   659 _UIF_TRANSFER	=	0x00d9
                           0000D8   660 _UIF_DETECT	=	0x00d8
                           0000D8   661 _UIF_BUS_RST	=	0x00d8
                                    662 ;--------------------------------------------------------
                                    663 ; overlayable register banks
                                    664 ;--------------------------------------------------------
                                    665 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        666 	.ds 8
                                    667 ;--------------------------------------------------------
                                    668 ; internal ram data
                                    669 ;--------------------------------------------------------
                                    670 	.area DSEG    (DATA)
                                    671 ;--------------------------------------------------------
                                    672 ; overlayable items in internal ram 
                                    673 ;--------------------------------------------------------
                                    674 ;--------------------------------------------------------
                                    675 ; indirectly addressable internal ram data
                                    676 ;--------------------------------------------------------
                                    677 	.area ISEG    (DATA)
                                    678 ;--------------------------------------------------------
                                    679 ; absolute internal ram data
                                    680 ;--------------------------------------------------------
                                    681 	.area IABS    (ABS,DATA)
                                    682 	.area IABS    (ABS,DATA)
                                    683 ;--------------------------------------------------------
                                    684 ; bit data
                                    685 ;--------------------------------------------------------
                                    686 	.area BSEG    (BIT)
                                    687 ;--------------------------------------------------------
                                    688 ; paged external ram data
                                    689 ;--------------------------------------------------------
                                    690 	.area PSEG    (PAG,XDATA)
                                    691 ;--------------------------------------------------------
                                    692 ; external ram data
                                    693 ;--------------------------------------------------------
                                    694 	.area XSEG    (XDATA)
      000001                        695 _SubwayLine::
      000001                        696 	.ds 42
      00002B                        697 _SubwayCircle::
      00002B                        698 	.ds 36
                                    699 ;--------------------------------------------------------
                                    700 ; absolute external ram data
                                    701 ;--------------------------------------------------------
                                    702 	.area XABS    (ABS,XDATA)
                                    703 ;--------------------------------------------------------
                                    704 ; external initialized ram data
                                    705 ;--------------------------------------------------------
                                    706 	.area HOME    (CODE)
                                    707 	.area GSINIT0 (CODE)
                                    708 	.area GSINIT1 (CODE)
                                    709 	.area GSINIT2 (CODE)
                                    710 	.area GSINIT3 (CODE)
                                    711 	.area GSINIT4 (CODE)
                                    712 	.area GSINIT5 (CODE)
                                    713 	.area GSINIT  (CODE)
                                    714 	.area GSFINAL (CODE)
                                    715 	.area CSEG    (CODE)
                                    716 ;--------------------------------------------------------
                                    717 ; global & static initialisations
                                    718 ;--------------------------------------------------------
                                    719 	.area HOME    (CODE)
                                    720 	.area GSINIT  (CODE)
                                    721 	.area GSFINAL (CODE)
                                    722 	.area GSINIT  (CODE)
                                    723 ;	source/CH549_SubwayBoard.c:11: UINT8 SubwayLine[6][7] = {
      000019 90 00 01         [24]  724 	mov	dptr,#_SubwayLine
      00001C 74 0A            [12]  725 	mov	a,#0x0a
      00001E F0               [24]  726 	movx	@dptr,a
      00001F 90 00 02         [24]  727 	mov	dptr,#(_SubwayLine + 0x0001)
      000022 74 0F            [12]  728 	mov	a,#0x0f
      000024 F0               [24]  729 	movx	@dptr,a
      000025 90 00 03         [24]  730 	mov	dptr,#(_SubwayLine + 0x0002)
      000028 14               [12]  731 	dec	a
      000029 F0               [24]  732 	movx	@dptr,a
      00002A 90 00 04         [24]  733 	mov	dptr,#(_SubwayLine + 0x0003)
      00002D 14               [12]  734 	dec	a
      00002E F0               [24]  735 	movx	@dptr,a
      00002F 90 00 05         [24]  736 	mov	dptr,#(_SubwayLine + 0x0004)
      000032 14               [12]  737 	dec	a
      000033 F0               [24]  738 	movx	@dptr,a
      000034 90 00 06         [24]  739 	mov	dptr,#(_SubwayLine + 0x0005)
      000037 74 07            [12]  740 	mov	a,#0x07
      000039 F0               [24]  741 	movx	@dptr,a
      00003A 90 00 07         [24]  742 	mov	dptr,#(_SubwayLine + 0x0006)
      00003D 14               [12]  743 	dec	a
      00003E F0               [24]  744 	movx	@dptr,a
      00003F 90 00 08         [24]  745 	mov	dptr,#(_SubwayLine + 0x0007)
      000042 14               [12]  746 	dec	a
      000043 F0               [24]  747 	movx	@dptr,a
      000044 90 00 09         [24]  748 	mov	dptr,#(_SubwayLine + 0x0008)
      000047 14               [12]  749 	dec	a
      000048 F0               [24]  750 	movx	@dptr,a
      000049 90 00 0A         [24]  751 	mov	dptr,#(_SubwayLine + 0x0009)
      00004C 14               [12]  752 	dec	a
      00004D F0               [24]  753 	movx	@dptr,a
      00004E 90 00 0B         [24]  754 	mov	dptr,#(_SubwayLine + 0x000a)
      000051 74 08            [12]  755 	mov	a,#0x08
      000053 F0               [24]  756 	movx	@dptr,a
      000054 90 00 0C         [24]  757 	mov	dptr,#(_SubwayLine + 0x000b)
      000057 74 0D            [12]  758 	mov	a,#0x0d
      000059 F0               [24]  759 	movx	@dptr,a
      00005A 90 00 0D         [24]  760 	mov	dptr,#(_SubwayLine + 0x000c)
      00005D 74 11            [12]  761 	mov	a,#0x11
      00005F F0               [24]  762 	movx	@dptr,a
      000060 90 00 0E         [24]  763 	mov	dptr,#(_SubwayLine + 0x000d)
      000063 14               [12]  764 	dec	a
      000064 F0               [24]  765 	movx	@dptr,a
      000065 90 00 0F         [24]  766 	mov	dptr,#(_SubwayLine + 0x000e)
      000068 74 14            [12]  767 	mov	a,#0x14
      00006A F0               [24]  768 	movx	@dptr,a
      00006B 90 00 10         [24]  769 	mov	dptr,#(_SubwayLine + 0x000f)
      00006E 74 0F            [12]  770 	mov	a,#0x0f
      000070 F0               [24]  771 	movx	@dptr,a
      000071 90 00 11         [24]  772 	mov	dptr,#(_SubwayLine + 0x0010)
      000074 74 09            [12]  773 	mov	a,#0x09
      000076 F0               [24]  774 	movx	@dptr,a
      000077 90 00 12         [24]  775 	mov	dptr,#(_SubwayLine + 0x0011)
      00007A 14               [12]  776 	dec	a
      00007B F0               [24]  777 	movx	@dptr,a
      00007C 90 00 13         [24]  778 	mov	dptr,#(_SubwayLine + 0x0012)
      00007F 74 02            [12]  779 	mov	a,#0x02
      000081 F0               [24]  780 	movx	@dptr,a
      000082 90 00 14         [24]  781 	mov	dptr,#(_SubwayLine + 0x0013)
      000085 74 07            [12]  782 	mov	a,#0x07
      000087 F0               [24]  783 	movx	@dptr,a
      000088 90 00 15         [24]  784 	mov	dptr,#(_SubwayLine + 0x0014)
      00008B 74 0B            [12]  785 	mov	a,#0x0b
      00008D F0               [24]  786 	movx	@dptr,a
      00008E 90 00 16         [24]  787 	mov	dptr,#(_SubwayLine + 0x0015)
      000091 74 13            [12]  788 	mov	a,#0x13
      000093 F0               [24]  789 	movx	@dptr,a
      000094 90 00 17         [24]  790 	mov	dptr,#(_SubwayLine + 0x0016)
      000097 74 0E            [12]  791 	mov	a,#0x0e
      000099 F0               [24]  792 	movx	@dptr,a
      00009A 90 00 18         [24]  793 	mov	dptr,#(_SubwayLine + 0x0017)
      00009D 74 09            [12]  794 	mov	a,#0x09
      00009F F0               [24]  795 	movx	@dptr,a
      0000A0 90 00 19         [24]  796 	mov	dptr,#(_SubwayLine + 0x0018)
      0000A3 74 05            [12]  797 	mov	a,#0x05
      0000A5 F0               [24]  798 	movx	@dptr,a
      0000A6 90 00 1A         [24]  799 	mov	dptr,#(_SubwayLine + 0x0019)
      0000A9 E4               [12]  800 	clr	a
      0000AA F0               [24]  801 	movx	@dptr,a
      0000AB 90 00 1B         [24]  802 	mov	dptr,#(_SubwayLine + 0x001a)
      0000AE F0               [24]  803 	movx	@dptr,a
      0000AF 90 00 1C         [24]  804 	mov	dptr,#(_SubwayLine + 0x001b)
      0000B2 F0               [24]  805 	movx	@dptr,a
      0000B3 90 00 1D         [24]  806 	mov	dptr,#(_SubwayLine + 0x001c)
      0000B6 74 12            [12]  807 	mov	a,#0x12
      0000B8 F0               [24]  808 	movx	@dptr,a
      0000B9 90 00 1E         [24]  809 	mov	dptr,#(_SubwayLine + 0x001d)
      0000BC 14               [12]  810 	dec	a
      0000BD F0               [24]  811 	movx	@dptr,a
      0000BE 90 00 1F         [24]  812 	mov	dptr,#(_SubwayLine + 0x001e)
      0000C1 74 0C            [12]  813 	mov	a,#0x0c
      0000C3 F0               [24]  814 	movx	@dptr,a
      0000C4 90 00 20         [24]  815 	mov	dptr,#(_SubwayLine + 0x001f)
      0000C7 04               [12]  816 	inc	a
      0000C8 F0               [24]  817 	movx	@dptr,a
      0000C9 90 00 21         [24]  818 	mov	dptr,#(_SubwayLine + 0x0020)
      0000CC 04               [12]  819 	inc	a
      0000CD F0               [24]  820 	movx	@dptr,a
      0000CE 90 00 22         [24]  821 	mov	dptr,#(_SubwayLine + 0x0021)
      0000D1 04               [12]  822 	inc	a
      0000D2 F0               [24]  823 	movx	@dptr,a
      0000D3 90 00 23         [24]  824 	mov	dptr,#(_SubwayLine + 0x0022)
      0000D6 74 0A            [12]  825 	mov	a,#0x0a
      0000D8 F0               [24]  826 	movx	@dptr,a
      0000D9 90 00 24         [24]  827 	mov	dptr,#(_SubwayLine + 0x0023)
      0000DC 74 10            [12]  828 	mov	a,#0x10
      0000DE F0               [24]  829 	movx	@dptr,a
      0000DF 90 00 25         [24]  830 	mov	dptr,#(_SubwayLine + 0x0024)
      0000E2 74 07            [12]  831 	mov	a,#0x07
      0000E4 F0               [24]  832 	movx	@dptr,a
      0000E5 90 00 26         [24]  833 	mov	dptr,#(_SubwayLine + 0x0025)
      0000E8 74 01            [12]  834 	mov	a,#0x01
      0000EA F0               [24]  835 	movx	@dptr,a
      0000EB 90 00 27         [24]  836 	mov	dptr,#(_SubwayLine + 0x0026)
      0000EE E4               [12]  837 	clr	a
      0000EF F0               [24]  838 	movx	@dptr,a
      0000F0 90 00 28         [24]  839 	mov	dptr,#(_SubwayLine + 0x0027)
      0000F3 F0               [24]  840 	movx	@dptr,a
      0000F4 90 00 29         [24]  841 	mov	dptr,#(_SubwayLine + 0x0028)
      0000F7 F0               [24]  842 	movx	@dptr,a
      0000F8 90 00 2A         [24]  843 	mov	dptr,#(_SubwayLine + 0x0029)
      0000FB F0               [24]  844 	movx	@dptr,a
                                    845 ;	source/CH549_SubwayBoard.c:20: UINT8 SubwayCircle[4][9] = {
      0000FC 90 00 2B         [24]  846 	mov	dptr,#_SubwayCircle
      0000FF 04               [12]  847 	inc	a
      000100 F0               [24]  848 	movx	@dptr,a
      000101 90 00 2C         [24]  849 	mov	dptr,#(_SubwayCircle + 0x0001)
      000104 74 05            [12]  850 	mov	a,#0x05
      000106 F0               [24]  851 	movx	@dptr,a
      000107 90 00 2D         [24]  852 	mov	dptr,#(_SubwayCircle + 0x0002)
      00010A 04               [12]  853 	inc	a
      00010B F0               [24]  854 	movx	@dptr,a
      00010C 90 00 2E         [24]  855 	mov	dptr,#(_SubwayCircle + 0x0003)
      00010F 74 0A            [12]  856 	mov	a,#0x0a
      000111 F0               [24]  857 	movx	@dptr,a
      000112 90 00 2F         [24]  858 	mov	dptr,#(_SubwayCircle + 0x0004)
      000115 04               [12]  859 	inc	a
      000116 F0               [24]  860 	movx	@dptr,a
      000117 90 00 30         [24]  861 	mov	dptr,#(_SubwayCircle + 0x0005)
      00011A 74 10            [12]  862 	mov	a,#0x10
      00011C F0               [24]  863 	movx	@dptr,a
      00011D 90 00 31         [24]  864 	mov	dptr,#(_SubwayCircle + 0x0006)
      000120 74 12            [12]  865 	mov	a,#0x12
      000122 F0               [24]  866 	movx	@dptr,a
      000123 90 00 32         [24]  867 	mov	dptr,#(_SubwayCircle + 0x0007)
      000126 04               [12]  868 	inc	a
      000127 F0               [24]  869 	movx	@dptr,a
      000128 90 00 33         [24]  870 	mov	dptr,#(_SubwayCircle + 0x0008)
      00012B 04               [12]  871 	inc	a
      00012C F0               [24]  872 	movx	@dptr,a
      00012D 90 00 34         [24]  873 	mov	dptr,#(_SubwayCircle + 0x0009)
      000130 74 04            [12]  874 	mov	a,#0x04
      000132 F0               [24]  875 	movx	@dptr,a
      000133 90 00 35         [24]  876 	mov	dptr,#(_SubwayCircle + 0x000a)
      000136 74 07            [12]  877 	mov	a,#0x07
      000138 F0               [24]  878 	movx	@dptr,a
      000139 90 00 36         [24]  879 	mov	dptr,#(_SubwayCircle + 0x000b)
      00013C 23               [12]  880 	rl	a
      00013D F0               [24]  881 	movx	@dptr,a
      00013E 90 00 37         [24]  882 	mov	dptr,#(_SubwayCircle + 0x000c)
      000141 04               [12]  883 	inc	a
      000142 F0               [24]  884 	movx	@dptr,a
      000143 90 00 38         [24]  885 	mov	dptr,#(_SubwayCircle + 0x000d)
      000146 74 11            [12]  886 	mov	a,#0x11
      000148 F0               [24]  887 	movx	@dptr,a
      000149 90 00 39         [24]  888 	mov	dptr,#(_SubwayCircle + 0x000e)
      00014C E4               [12]  889 	clr	a
      00014D F0               [24]  890 	movx	@dptr,a
      00014E 90 00 3A         [24]  891 	mov	dptr,#(_SubwayCircle + 0x000f)
      000151 F0               [24]  892 	movx	@dptr,a
      000152 90 00 3B         [24]  893 	mov	dptr,#(_SubwayCircle + 0x0010)
      000155 F0               [24]  894 	movx	@dptr,a
      000156 90 00 3C         [24]  895 	mov	dptr,#(_SubwayCircle + 0x0011)
      000159 F0               [24]  896 	movx	@dptr,a
      00015A 90 00 3D         [24]  897 	mov	dptr,#(_SubwayCircle + 0x0012)
      00015D 74 02            [12]  898 	mov	a,#0x02
      00015F F0               [24]  899 	movx	@dptr,a
      000160 90 00 3E         [24]  900 	mov	dptr,#(_SubwayCircle + 0x0013)
      000163 04               [12]  901 	inc	a
      000164 F0               [24]  902 	movx	@dptr,a
      000165 90 00 3F         [24]  903 	mov	dptr,#(_SubwayCircle + 0x0014)
      000168 74 09            [12]  904 	mov	a,#0x09
      00016A F0               [24]  905 	movx	@dptr,a
      00016B 90 00 40         [24]  906 	mov	dptr,#(_SubwayCircle + 0x0015)
      00016E 74 0C            [12]  907 	mov	a,#0x0c
      000170 F0               [24]  908 	movx	@dptr,a
      000171 90 00 41         [24]  909 	mov	dptr,#(_SubwayCircle + 0x0016)
      000174 04               [12]  910 	inc	a
      000175 F0               [24]  911 	movx	@dptr,a
      000176 90 00 42         [24]  912 	mov	dptr,#(_SubwayCircle + 0x0017)
      000179 E4               [12]  913 	clr	a
      00017A F0               [24]  914 	movx	@dptr,a
      00017B 90 00 43         [24]  915 	mov	dptr,#(_SubwayCircle + 0x0018)
      00017E F0               [24]  916 	movx	@dptr,a
      00017F 90 00 44         [24]  917 	mov	dptr,#(_SubwayCircle + 0x0019)
      000182 F0               [24]  918 	movx	@dptr,a
      000183 90 00 45         [24]  919 	mov	dptr,#(_SubwayCircle + 0x001a)
      000186 F0               [24]  920 	movx	@dptr,a
      000187 90 00 46         [24]  921 	mov	dptr,#(_SubwayCircle + 0x001b)
      00018A 74 08            [12]  922 	mov	a,#0x08
      00018C F0               [24]  923 	movx	@dptr,a
      00018D 90 00 47         [24]  924 	mov	dptr,#(_SubwayCircle + 0x001c)
      000190 E4               [12]  925 	clr	a
      000191 F0               [24]  926 	movx	@dptr,a
      000192 90 00 48         [24]  927 	mov	dptr,#(_SubwayCircle + 0x001d)
      000195 F0               [24]  928 	movx	@dptr,a
      000196 90 00 49         [24]  929 	mov	dptr,#(_SubwayCircle + 0x001e)
      000199 F0               [24]  930 	movx	@dptr,a
      00019A 90 00 4A         [24]  931 	mov	dptr,#(_SubwayCircle + 0x001f)
      00019D F0               [24]  932 	movx	@dptr,a
      00019E 90 00 4B         [24]  933 	mov	dptr,#(_SubwayCircle + 0x0020)
      0001A1 F0               [24]  934 	movx	@dptr,a
      0001A2 90 00 4C         [24]  935 	mov	dptr,#(_SubwayCircle + 0x0021)
      0001A5 F0               [24]  936 	movx	@dptr,a
      0001A6 90 00 4D         [24]  937 	mov	dptr,#(_SubwayCircle + 0x0022)
      0001A9 F0               [24]  938 	movx	@dptr,a
      0001AA 90 00 4E         [24]  939 	mov	dptr,#(_SubwayCircle + 0x0023)
      0001AD F0               [24]  940 	movx	@dptr,a
                                    941 ;--------------------------------------------------------
                                    942 ; Home
                                    943 ;--------------------------------------------------------
                                    944 	.area HOME    (CODE)
                                    945 	.area HOME    (CODE)
                                    946 ;--------------------------------------------------------
                                    947 ; code
                                    948 ;--------------------------------------------------------
                                    949 	.area CSEG    (CODE)
                                    950 ;------------------------------------------------------------
                                    951 ;Allocation info for local variables in function 'LIGHT_ON'
                                    952 ;------------------------------------------------------------
                                    953 ;LIGHT                     Allocated to registers r7 
                                    954 ;------------------------------------------------------------
                                    955 ;	source/CH549_SubwayBoard.c:28: void LIGHT_ON(unsigned char LIGHT)
                                    956 ;	-----------------------------------------
                                    957 ;	 function LIGHT_ON
                                    958 ;	-----------------------------------------
      00036C                        959 _LIGHT_ON:
                           000007   960 	ar7 = 0x07
                           000006   961 	ar6 = 0x06
                           000005   962 	ar5 = 0x05
                           000004   963 	ar4 = 0x04
                           000003   964 	ar3 = 0x03
                           000002   965 	ar2 = 0x02
                           000001   966 	ar1 = 0x01
                           000000   967 	ar0 = 0x00
                                    968 ;	source/CH549_SubwayBoard.c:30: switch (LIGHT)
      00036C E5 82            [12]  969 	mov	a,dpl
      00036E FF               [12]  970 	mov	r7,a
      00036F 24 EB            [12]  971 	add	a,#0xff - 0x14
      000371 50 01            [24]  972 	jnc	00128$
      000373 22               [24]  973 	ret
      000374                        974 00128$:
      000374 EF               [12]  975 	mov	a,r7
      000375 24 0A            [12]  976 	add	a,#(00129$-3-.)
      000377 83               [24]  977 	movc	a,@a+pc
      000378 F5 82            [12]  978 	mov	dpl,a
      00037A EF               [12]  979 	mov	a,r7
      00037B 24 19            [12]  980 	add	a,#(00130$-3-.)
      00037D 83               [24]  981 	movc	a,@a+pc
      00037E F5 83            [12]  982 	mov	dph,a
      000380 E4               [12]  983 	clr	a
      000381 73               [24]  984 	jmp	@a+dptr
      000382                        985 00129$:
      000382 0F                     986 	.db	00122$
      000383 AC                     987 	.db	00101$
      000384 B1                     988 	.db	00102$
      000385 B6                     989 	.db	00103$
      000386 BB                     990 	.db	00104$
      000387 C0                     991 	.db	00105$
      000388 C5                     992 	.db	00106$
      000389 CA                     993 	.db	00107$
      00038A CF                     994 	.db	00108$
      00038B D4                     995 	.db	00109$
      00038C D9                     996 	.db	00110$
      00038D DE                     997 	.db	00111$
      00038E E3                     998 	.db	00112$
      00038F E8                     999 	.db	00113$
      000390 ED                    1000 	.db	00114$
      000391 F2                    1001 	.db	00115$
      000392 F7                    1002 	.db	00116$
      000393 FC                    1003 	.db	00117$
      000394 01                    1004 	.db	00118$
      000395 06                    1005 	.db	00119$
      000396 0B                    1006 	.db	00120$
      000397                       1007 00130$:
      000397 04                    1008 	.db	00122$>>8
      000398 03                    1009 	.db	00101$>>8
      000399 03                    1010 	.db	00102$>>8
      00039A 03                    1011 	.db	00103$>>8
      00039B 03                    1012 	.db	00104$>>8
      00039C 03                    1013 	.db	00105$>>8
      00039D 03                    1014 	.db	00106$>>8
      00039E 03                    1015 	.db	00107$>>8
      00039F 03                    1016 	.db	00108$>>8
      0003A0 03                    1017 	.db	00109$>>8
      0003A1 03                    1018 	.db	00110$>>8
      0003A2 03                    1019 	.db	00111$>>8
      0003A3 03                    1020 	.db	00112$>>8
      0003A4 03                    1021 	.db	00113$>>8
      0003A5 03                    1022 	.db	00114$>>8
      0003A6 03                    1023 	.db	00115$>>8
      0003A7 03                    1024 	.db	00116$>>8
      0003A8 03                    1025 	.db	00117$>>8
      0003A9 04                    1026 	.db	00118$>>8
      0003AA 04                    1027 	.db	00119$>>8
      0003AB 04                    1028 	.db	00120$>>8
                                   1029 ;	source/CH549_SubwayBoard.c:32: case 1:LED_A1 = 1;LED_B1 = 0;break;
      0003AC                       1030 00101$:
                                   1031 ;	assignBit
      0003AC D2 B5            [12] 1032 	setb	_P3_5
                                   1033 ;	assignBit
      0003AE C2 91            [12] 1034 	clr	_P1_1
      0003B0 22               [24] 1035 	ret
                                   1036 ;	source/CH549_SubwayBoard.c:33: case 2:LED_A2 = 1;LED_B1 = 0;break;
      0003B1                       1037 00102$:
                                   1038 ;	assignBit
      0003B1 D2 A2            [12] 1039 	setb	_P2_2
                                   1040 ;	assignBit
      0003B3 C2 91            [12] 1041 	clr	_P1_1
                                   1042 ;	source/CH549_SubwayBoard.c:34: case 3:LED_A3 = 1;LED_B1 = 0;break;
      0003B5 22               [24] 1043 	ret
      0003B6                       1044 00103$:
                                   1045 ;	assignBit
      0003B6 D2 A4            [12] 1046 	setb	_P2_4
                                   1047 ;	assignBit
      0003B8 C2 91            [12] 1048 	clr	_P1_1
                                   1049 ;	source/CH549_SubwayBoard.c:35: case 4:LED_A4 = 1;LED_B1 = 0;break;
      0003BA 22               [24] 1050 	ret
      0003BB                       1051 00104$:
                                   1052 ;	assignBit
      0003BB D2 A6            [12] 1053 	setb	_P2_6
                                   1054 ;	assignBit
      0003BD C2 91            [12] 1055 	clr	_P1_1
                                   1056 ;	source/CH549_SubwayBoard.c:36: case 5:LED_A5 = 1;LED_B1 = 0;break;
      0003BF 22               [24] 1057 	ret
      0003C0                       1058 00105$:
                                   1059 ;	assignBit
      0003C0 D2 A7            [12] 1060 	setb	_P2_7
                                   1061 ;	assignBit
      0003C2 C2 91            [12] 1062 	clr	_P1_1
                                   1063 ;	source/CH549_SubwayBoard.c:37: case 6:LED_A1 = 1;LED_B2 = 0;break;
      0003C4 22               [24] 1064 	ret
      0003C5                       1065 00106$:
                                   1066 ;	assignBit
      0003C5 D2 B5            [12] 1067 	setb	_P3_5
                                   1068 ;	assignBit
      0003C7 C2 94            [12] 1069 	clr	_P1_4
                                   1070 ;	source/CH549_SubwayBoard.c:38: case 7:LED_A2 = 1;LED_B2 = 0;break;
      0003C9 22               [24] 1071 	ret
      0003CA                       1072 00107$:
                                   1073 ;	assignBit
      0003CA D2 A2            [12] 1074 	setb	_P2_2
                                   1075 ;	assignBit
      0003CC C2 94            [12] 1076 	clr	_P1_4
                                   1077 ;	source/CH549_SubwayBoard.c:39: case 8:LED_A3 = 1;LED_B2 = 0;break;
      0003CE 22               [24] 1078 	ret
      0003CF                       1079 00108$:
                                   1080 ;	assignBit
      0003CF D2 A4            [12] 1081 	setb	_P2_4
                                   1082 ;	assignBit
      0003D1 C2 94            [12] 1083 	clr	_P1_4
                                   1084 ;	source/CH549_SubwayBoard.c:40: case 9:LED_A4 = 1;LED_B2 = 0;break;
      0003D3 22               [24] 1085 	ret
      0003D4                       1086 00109$:
                                   1087 ;	assignBit
      0003D4 D2 A6            [12] 1088 	setb	_P2_6
                                   1089 ;	assignBit
      0003D6 C2 94            [12] 1090 	clr	_P1_4
                                   1091 ;	source/CH549_SubwayBoard.c:41: case 10:LED_A5 = 1;LED_B2 = 0;break;
      0003D8 22               [24] 1092 	ret
      0003D9                       1093 00110$:
                                   1094 ;	assignBit
      0003D9 D2 A7            [12] 1095 	setb	_P2_7
                                   1096 ;	assignBit
      0003DB C2 94            [12] 1097 	clr	_P1_4
                                   1098 ;	source/CH549_SubwayBoard.c:42: case 11:LED_A1 = 1;LED_B3 = 0;break;
      0003DD 22               [24] 1099 	ret
      0003DE                       1100 00111$:
                                   1101 ;	assignBit
      0003DE D2 B5            [12] 1102 	setb	_P3_5
                                   1103 ;	assignBit
      0003E0 C2 95            [12] 1104 	clr	_P1_5
                                   1105 ;	source/CH549_SubwayBoard.c:43: case 12:LED_A2 = 1;LED_B3 = 0;break;
      0003E2 22               [24] 1106 	ret
      0003E3                       1107 00112$:
                                   1108 ;	assignBit
      0003E3 D2 A2            [12] 1109 	setb	_P2_2
                                   1110 ;	assignBit
      0003E5 C2 95            [12] 1111 	clr	_P1_5
                                   1112 ;	source/CH549_SubwayBoard.c:44: case 13:LED_A3 = 1;LED_B3 = 0;break;
      0003E7 22               [24] 1113 	ret
      0003E8                       1114 00113$:
                                   1115 ;	assignBit
      0003E8 D2 A4            [12] 1116 	setb	_P2_4
                                   1117 ;	assignBit
      0003EA C2 95            [12] 1118 	clr	_P1_5
                                   1119 ;	source/CH549_SubwayBoard.c:45: case 14:LED_A4 = 1;LED_B3 = 0;break;
      0003EC 22               [24] 1120 	ret
      0003ED                       1121 00114$:
                                   1122 ;	assignBit
      0003ED D2 A6            [12] 1123 	setb	_P2_6
                                   1124 ;	assignBit
      0003EF C2 95            [12] 1125 	clr	_P1_5
                                   1126 ;	source/CH549_SubwayBoard.c:46: case 15:LED_A5 = 1;LED_B3 = 0;break;
      0003F1 22               [24] 1127 	ret
      0003F2                       1128 00115$:
                                   1129 ;	assignBit
      0003F2 D2 A7            [12] 1130 	setb	_P2_7
                                   1131 ;	assignBit
      0003F4 C2 95            [12] 1132 	clr	_P1_5
                                   1133 ;	source/CH549_SubwayBoard.c:47: case 16:LED_A1 = 1;LED_B4 = 0;break;
      0003F6 22               [24] 1134 	ret
      0003F7                       1135 00116$:
                                   1136 ;	assignBit
      0003F7 D2 B5            [12] 1137 	setb	_P3_5
                                   1138 ;	assignBit
      0003F9 C2 96            [12] 1139 	clr	_P1_6
                                   1140 ;	source/CH549_SubwayBoard.c:48: case 17:LED_A2 = 1;LED_B4 = 0;break;
      0003FB 22               [24] 1141 	ret
      0003FC                       1142 00117$:
                                   1143 ;	assignBit
      0003FC D2 A2            [12] 1144 	setb	_P2_2
                                   1145 ;	assignBit
      0003FE C2 96            [12] 1146 	clr	_P1_6
                                   1147 ;	source/CH549_SubwayBoard.c:49: case 18:LED_A3 = 1;LED_B4 = 0;break;
      000400 22               [24] 1148 	ret
      000401                       1149 00118$:
                                   1150 ;	assignBit
      000401 D2 A4            [12] 1151 	setb	_P2_4
                                   1152 ;	assignBit
      000403 C2 96            [12] 1153 	clr	_P1_6
                                   1154 ;	source/CH549_SubwayBoard.c:50: case 19:LED_A4 = 1;LED_B4 = 0;break;
      000405 22               [24] 1155 	ret
      000406                       1156 00119$:
                                   1157 ;	assignBit
      000406 D2 A6            [12] 1158 	setb	_P2_6
                                   1159 ;	assignBit
      000408 C2 96            [12] 1160 	clr	_P1_6
                                   1161 ;	source/CH549_SubwayBoard.c:51: case 20:LED_A5 = 1;LED_B4 = 0;break;
      00040A 22               [24] 1162 	ret
      00040B                       1163 00120$:
                                   1164 ;	assignBit
      00040B D2 A7            [12] 1165 	setb	_P2_7
                                   1166 ;	assignBit
      00040D C2 96            [12] 1167 	clr	_P1_6
                                   1168 ;	source/CH549_SubwayBoard.c:52: }
      00040F                       1169 00122$:
                                   1170 ;	source/CH549_SubwayBoard.c:53: }
      00040F 22               [24] 1171 	ret
                                   1172 ;------------------------------------------------------------
                                   1173 ;Allocation info for local variables in function 'LIGHT_OFF'
                                   1174 ;------------------------------------------------------------
                                   1175 ;LIGHT                     Allocated to registers r7 
                                   1176 ;------------------------------------------------------------
                                   1177 ;	source/CH549_SubwayBoard.c:56: void LIGHT_OFF(unsigned char LIGHT)
                                   1178 ;	-----------------------------------------
                                   1179 ;	 function LIGHT_OFF
                                   1180 ;	-----------------------------------------
      000410                       1181 _LIGHT_OFF:
                                   1182 ;	source/CH549_SubwayBoard.c:58: switch (LIGHT)
      000410 E5 82            [12] 1183 	mov	a,dpl
      000412 FF               [12] 1184 	mov	r7,a
      000413 24 EB            [12] 1185 	add	a,#0xff - 0x14
      000415 50 01            [24] 1186 	jnc	00128$
      000417 22               [24] 1187 	ret
      000418                       1188 00128$:
      000418 EF               [12] 1189 	mov	a,r7
      000419 24 0A            [12] 1190 	add	a,#(00129$-3-.)
      00041B 83               [24] 1191 	movc	a,@a+pc
      00041C F5 82            [12] 1192 	mov	dpl,a
      00041E EF               [12] 1193 	mov	a,r7
      00041F 24 19            [12] 1194 	add	a,#(00130$-3-.)
      000421 83               [24] 1195 	movc	a,@a+pc
      000422 F5 83            [12] 1196 	mov	dph,a
      000424 E4               [12] 1197 	clr	a
      000425 73               [24] 1198 	jmp	@a+dptr
      000426                       1199 00129$:
      000426 B3                    1200 	.db	00122$
      000427 50                    1201 	.db	00101$
      000428 55                    1202 	.db	00102$
      000429 5A                    1203 	.db	00103$
      00042A 5F                    1204 	.db	00104$
      00042B 64                    1205 	.db	00105$
      00042C 69                    1206 	.db	00106$
      00042D 6E                    1207 	.db	00107$
      00042E 73                    1208 	.db	00108$
      00042F 78                    1209 	.db	00109$
      000430 7D                    1210 	.db	00110$
      000431 82                    1211 	.db	00111$
      000432 87                    1212 	.db	00112$
      000433 8C                    1213 	.db	00113$
      000434 91                    1214 	.db	00114$
      000435 96                    1215 	.db	00115$
      000436 9B                    1216 	.db	00116$
      000437 A0                    1217 	.db	00117$
      000438 A5                    1218 	.db	00118$
      000439 AA                    1219 	.db	00119$
      00043A AF                    1220 	.db	00120$
      00043B                       1221 00130$:
      00043B 04                    1222 	.db	00122$>>8
      00043C 04                    1223 	.db	00101$>>8
      00043D 04                    1224 	.db	00102$>>8
      00043E 04                    1225 	.db	00103$>>8
      00043F 04                    1226 	.db	00104$>>8
      000440 04                    1227 	.db	00105$>>8
      000441 04                    1228 	.db	00106$>>8
      000442 04                    1229 	.db	00107$>>8
      000443 04                    1230 	.db	00108$>>8
      000444 04                    1231 	.db	00109$>>8
      000445 04                    1232 	.db	00110$>>8
      000446 04                    1233 	.db	00111$>>8
      000447 04                    1234 	.db	00112$>>8
      000448 04                    1235 	.db	00113$>>8
      000449 04                    1236 	.db	00114$>>8
      00044A 04                    1237 	.db	00115$>>8
      00044B 04                    1238 	.db	00116$>>8
      00044C 04                    1239 	.db	00117$>>8
      00044D 04                    1240 	.db	00118$>>8
      00044E 04                    1241 	.db	00119$>>8
      00044F 04                    1242 	.db	00120$>>8
                                   1243 ;	source/CH549_SubwayBoard.c:60: case 1:LED_A1 = 0;LED_B1 = 1;break;
      000450                       1244 00101$:
                                   1245 ;	assignBit
      000450 C2 B5            [12] 1246 	clr	_P3_5
                                   1247 ;	assignBit
      000452 D2 91            [12] 1248 	setb	_P1_1
      000454 22               [24] 1249 	ret
                                   1250 ;	source/CH549_SubwayBoard.c:61: case 2:LED_A2 = 0;LED_B1 = 1;break;
      000455                       1251 00102$:
                                   1252 ;	assignBit
      000455 C2 A2            [12] 1253 	clr	_P2_2
                                   1254 ;	assignBit
      000457 D2 91            [12] 1255 	setb	_P1_1
                                   1256 ;	source/CH549_SubwayBoard.c:62: case 3:LED_A3 = 0;LED_B1 = 1;break;
      000459 22               [24] 1257 	ret
      00045A                       1258 00103$:
                                   1259 ;	assignBit
      00045A C2 A4            [12] 1260 	clr	_P2_4
                                   1261 ;	assignBit
      00045C D2 91            [12] 1262 	setb	_P1_1
                                   1263 ;	source/CH549_SubwayBoard.c:63: case 4:LED_A4 = 0;LED_B1 = 1;break;
      00045E 22               [24] 1264 	ret
      00045F                       1265 00104$:
                                   1266 ;	assignBit
      00045F C2 A6            [12] 1267 	clr	_P2_6
                                   1268 ;	assignBit
      000461 D2 91            [12] 1269 	setb	_P1_1
                                   1270 ;	source/CH549_SubwayBoard.c:64: case 5:LED_A5 = 0;LED_B1 = 1;break;
      000463 22               [24] 1271 	ret
      000464                       1272 00105$:
                                   1273 ;	assignBit
      000464 C2 A7            [12] 1274 	clr	_P2_7
                                   1275 ;	assignBit
      000466 D2 91            [12] 1276 	setb	_P1_1
                                   1277 ;	source/CH549_SubwayBoard.c:65: case 6:LED_A1 = 0;LED_B2 = 1;break;
      000468 22               [24] 1278 	ret
      000469                       1279 00106$:
                                   1280 ;	assignBit
      000469 C2 B5            [12] 1281 	clr	_P3_5
                                   1282 ;	assignBit
      00046B D2 94            [12] 1283 	setb	_P1_4
                                   1284 ;	source/CH549_SubwayBoard.c:66: case 7:LED_A2 = 0;LED_B2 = 1;break;
      00046D 22               [24] 1285 	ret
      00046E                       1286 00107$:
                                   1287 ;	assignBit
      00046E C2 A2            [12] 1288 	clr	_P2_2
                                   1289 ;	assignBit
      000470 D2 94            [12] 1290 	setb	_P1_4
                                   1291 ;	source/CH549_SubwayBoard.c:67: case 8:LED_A3 = 0;LED_B2 = 1;break;
      000472 22               [24] 1292 	ret
      000473                       1293 00108$:
                                   1294 ;	assignBit
      000473 C2 A4            [12] 1295 	clr	_P2_4
                                   1296 ;	assignBit
      000475 D2 94            [12] 1297 	setb	_P1_4
                                   1298 ;	source/CH549_SubwayBoard.c:68: case 9:LED_A4 = 0;LED_B2 = 1;break;
      000477 22               [24] 1299 	ret
      000478                       1300 00109$:
                                   1301 ;	assignBit
      000478 C2 A6            [12] 1302 	clr	_P2_6
                                   1303 ;	assignBit
      00047A D2 94            [12] 1304 	setb	_P1_4
                                   1305 ;	source/CH549_SubwayBoard.c:69: case 10:LED_A5 = 0;LED_B2 = 1;break;
      00047C 22               [24] 1306 	ret
      00047D                       1307 00110$:
                                   1308 ;	assignBit
      00047D C2 A7            [12] 1309 	clr	_P2_7
                                   1310 ;	assignBit
      00047F D2 94            [12] 1311 	setb	_P1_4
                                   1312 ;	source/CH549_SubwayBoard.c:70: case 11:LED_A1 = 0;LED_B3 = 1;break;
      000481 22               [24] 1313 	ret
      000482                       1314 00111$:
                                   1315 ;	assignBit
      000482 C2 B5            [12] 1316 	clr	_P3_5
                                   1317 ;	assignBit
      000484 D2 95            [12] 1318 	setb	_P1_5
                                   1319 ;	source/CH549_SubwayBoard.c:71: case 12:LED_A2 = 0;LED_B3 = 1;break;
      000486 22               [24] 1320 	ret
      000487                       1321 00112$:
                                   1322 ;	assignBit
      000487 C2 A2            [12] 1323 	clr	_P2_2
                                   1324 ;	assignBit
      000489 D2 95            [12] 1325 	setb	_P1_5
                                   1326 ;	source/CH549_SubwayBoard.c:72: case 13:LED_A3 = 0;LED_B3 = 1;break;
      00048B 22               [24] 1327 	ret
      00048C                       1328 00113$:
                                   1329 ;	assignBit
      00048C C2 A4            [12] 1330 	clr	_P2_4
                                   1331 ;	assignBit
      00048E D2 95            [12] 1332 	setb	_P1_5
                                   1333 ;	source/CH549_SubwayBoard.c:73: case 14:LED_A4 = 0;LED_B3 = 1;break;
      000490 22               [24] 1334 	ret
      000491                       1335 00114$:
                                   1336 ;	assignBit
      000491 C2 A6            [12] 1337 	clr	_P2_6
                                   1338 ;	assignBit
      000493 D2 95            [12] 1339 	setb	_P1_5
                                   1340 ;	source/CH549_SubwayBoard.c:74: case 15:LED_A5 = 0;LED_B3 = 1;break;
      000495 22               [24] 1341 	ret
      000496                       1342 00115$:
                                   1343 ;	assignBit
      000496 C2 A7            [12] 1344 	clr	_P2_7
                                   1345 ;	assignBit
      000498 D2 95            [12] 1346 	setb	_P1_5
                                   1347 ;	source/CH549_SubwayBoard.c:75: case 16:LED_A1 = 0;LED_B4 = 1;break;
      00049A 22               [24] 1348 	ret
      00049B                       1349 00116$:
                                   1350 ;	assignBit
      00049B C2 B5            [12] 1351 	clr	_P3_5
                                   1352 ;	assignBit
      00049D D2 96            [12] 1353 	setb	_P1_6
                                   1354 ;	source/CH549_SubwayBoard.c:76: case 17:LED_A2 = 0;LED_B4 = 1;break;
      00049F 22               [24] 1355 	ret
      0004A0                       1356 00117$:
                                   1357 ;	assignBit
      0004A0 C2 A2            [12] 1358 	clr	_P2_2
                                   1359 ;	assignBit
      0004A2 D2 96            [12] 1360 	setb	_P1_6
                                   1361 ;	source/CH549_SubwayBoard.c:77: case 18:LED_A3 = 0;LED_B4 = 1;break;
      0004A4 22               [24] 1362 	ret
      0004A5                       1363 00118$:
                                   1364 ;	assignBit
      0004A5 C2 A4            [12] 1365 	clr	_P2_4
                                   1366 ;	assignBit
      0004A7 D2 96            [12] 1367 	setb	_P1_6
                                   1368 ;	source/CH549_SubwayBoard.c:78: case 19:LED_A4 = 0;LED_B4 = 1;break;
      0004A9 22               [24] 1369 	ret
      0004AA                       1370 00119$:
                                   1371 ;	assignBit
      0004AA C2 A6            [12] 1372 	clr	_P2_6
                                   1373 ;	assignBit
      0004AC D2 96            [12] 1374 	setb	_P1_6
                                   1375 ;	source/CH549_SubwayBoard.c:79: case 20:LED_A5 = 0;LED_B4 = 1;break;
      0004AE 22               [24] 1376 	ret
      0004AF                       1377 00120$:
                                   1378 ;	assignBit
      0004AF C2 A7            [12] 1379 	clr	_P2_7
                                   1380 ;	assignBit
      0004B1 D2 96            [12] 1381 	setb	_P1_6
                                   1382 ;	source/CH549_SubwayBoard.c:80: }
      0004B3                       1383 00122$:
                                   1384 ;	source/CH549_SubwayBoard.c:81: }
      0004B3 22               [24] 1385 	ret
                                   1386 ;------------------------------------------------------------
                                   1387 ;Allocation info for local variables in function 'LIGHT_Flash'
                                   1388 ;------------------------------------------------------------
                                   1389 ;LIGHT                     Allocated to registers r7 
                                   1390 ;------------------------------------------------------------
                                   1391 ;	source/CH549_SubwayBoard.c:84: void LIGHT_Flash(UINT8 LIGHT)
                                   1392 ;	-----------------------------------------
                                   1393 ;	 function LIGHT_Flash
                                   1394 ;	-----------------------------------------
      0004B4                       1395 _LIGHT_Flash:
                                   1396 ;	source/CH549_SubwayBoard.c:86: LIGHT_ON(LIGHT);
      0004B4 AF 82            [24] 1397 	mov  r7,dpl
      0004B6 C0 07            [24] 1398 	push	ar7
      0004B8 12 03 6C         [24] 1399 	lcall	_LIGHT_ON
                                   1400 ;	source/CH549_SubwayBoard.c:87: mDelayuS(100);
      0004BB 90 00 64         [24] 1401 	mov	dptr,#0x0064
      0004BE 12 02 C4         [24] 1402 	lcall	_mDelayuS
      0004C1 D0 07            [24] 1403 	pop	ar7
                                   1404 ;	source/CH549_SubwayBoard.c:88: LIGHT_OFF(LIGHT);
      0004C3 8F 82            [24] 1405 	mov	dpl,r7
                                   1406 ;	source/CH549_SubwayBoard.c:90: }
      0004C5 02 04 10         [24] 1407 	ljmp	_LIGHT_OFF
                                   1408 ;------------------------------------------------------------
                                   1409 ;Allocation info for local variables in function 'LIGHT_Clean'
                                   1410 ;------------------------------------------------------------
                                   1411 ;LIGHT                     Allocated to registers r7 
                                   1412 ;------------------------------------------------------------
                                   1413 ;	source/CH549_SubwayBoard.c:93: void LIGHT_Clean()
                                   1414 ;	-----------------------------------------
                                   1415 ;	 function LIGHT_Clean
                                   1416 ;	-----------------------------------------
      0004C8                       1417 _LIGHT_Clean:
                                   1418 ;	source/CH549_SubwayBoard.c:95: for (UINT8 LIGHT = 1; LIGHT < 21; LIGHT++)
      0004C8 7F 01            [12] 1419 	mov	r7,#0x01
      0004CA                       1420 00103$:
      0004CA BF 15 00         [24] 1421 	cjne	r7,#0x15,00116$
      0004CD                       1422 00116$:
      0004CD 50 0C            [24] 1423 	jnc	00105$
                                   1424 ;	source/CH549_SubwayBoard.c:97: LIGHT_OFF(LIGHT);
      0004CF 8F 82            [24] 1425 	mov	dpl,r7
      0004D1 C0 07            [24] 1426 	push	ar7
      0004D3 12 04 10         [24] 1427 	lcall	_LIGHT_OFF
      0004D6 D0 07            [24] 1428 	pop	ar7
                                   1429 ;	source/CH549_SubwayBoard.c:95: for (UINT8 LIGHT = 1; LIGHT < 21; LIGHT++)
      0004D8 0F               [12] 1430 	inc	r7
      0004D9 80 EF            [24] 1431 	sjmp	00103$
      0004DB                       1432 00105$:
                                   1433 ;	source/CH549_SubwayBoard.c:100: }
      0004DB 22               [24] 1434 	ret
                                   1435 ;------------------------------------------------------------
                                   1436 ;Allocation info for local variables in function 'LIGHT_AllOn'
                                   1437 ;------------------------------------------------------------
                                   1438 ;LIGHT                     Allocated to registers r7 
                                   1439 ;------------------------------------------------------------
                                   1440 ;	source/CH549_SubwayBoard.c:103: void LIGHT_AllOn()
                                   1441 ;	-----------------------------------------
                                   1442 ;	 function LIGHT_AllOn
                                   1443 ;	-----------------------------------------
      0004DC                       1444 _LIGHT_AllOn:
                                   1445 ;	source/CH549_SubwayBoard.c:105: for (UINT8 LIGHT = 1; LIGHT < 21; LIGHT++)
      0004DC 7F 01            [12] 1446 	mov	r7,#0x01
      0004DE                       1447 00103$:
      0004DE BF 15 00         [24] 1448 	cjne	r7,#0x15,00116$
      0004E1                       1449 00116$:
      0004E1 50 0C            [24] 1450 	jnc	00105$
                                   1451 ;	source/CH549_SubwayBoard.c:107: LIGHT_ON(LIGHT);
      0004E3 8F 82            [24] 1452 	mov	dpl,r7
      0004E5 C0 07            [24] 1453 	push	ar7
      0004E7 12 03 6C         [24] 1454 	lcall	_LIGHT_ON
      0004EA D0 07            [24] 1455 	pop	ar7
                                   1456 ;	source/CH549_SubwayBoard.c:105: for (UINT8 LIGHT = 1; LIGHT < 21; LIGHT++)
      0004EC 0F               [12] 1457 	inc	r7
      0004ED 80 EF            [24] 1458 	sjmp	00103$
      0004EF                       1459 00105$:
                                   1460 ;	source/CH549_SubwayBoard.c:110: }
      0004EF 22               [24] 1461 	ret
                                   1462 ;------------------------------------------------------------
                                   1463 ;Allocation info for local variables in function 'LIGHT_TLineOn'
                                   1464 ;------------------------------------------------------------
                                   1465 ;DOT                       Allocated to registers r2 
                                   1466 ;LINE                      Allocated to registers r7 
                                   1467 ;------------------------------------------------------------
                                   1468 ;	source/CH549_SubwayBoard.c:113: void LIGHT_TLineOn()
                                   1469 ;	-----------------------------------------
                                   1470 ;	 function LIGHT_TLineOn
                                   1471 ;	-----------------------------------------
      0004F0                       1472 _LIGHT_TLineOn:
                                   1473 ;	source/CH549_SubwayBoard.c:116: for ( LINE = 0; LINE < 6; LINE++)
      0004F0 7F 00            [12] 1474 	mov	r7,#0x00
                                   1475 ;	source/CH549_SubwayBoard.c:118: for ( DOT = 0; DOT < 7; DOT++)
      0004F2                       1476 00112$:
      0004F2 EF               [12] 1477 	mov	a,r7
      0004F3 75 F0 07         [24] 1478 	mov	b,#0x07
      0004F6 A4               [48] 1479 	mul	ab
      0004F7 FD               [12] 1480 	mov	r5,a
      0004F8 AE F0            [24] 1481 	mov	r6,b
      0004FA 24 01            [12] 1482 	add	a,#_SubwayLine
      0004FC FB               [12] 1483 	mov	r3,a
      0004FD EE               [12] 1484 	mov	a,r6
      0004FE 34 00            [12] 1485 	addc	a,#(_SubwayLine >> 8)
      000500 FC               [12] 1486 	mov	r4,a
      000501 7A 00            [12] 1487 	mov	r2,#0x00
      000503                       1488 00105$:
                                   1489 ;	source/CH549_SubwayBoard.c:120: if (SubwayLine[LINE][DOT])
      000503 EA               [12] 1490 	mov	a,r2
      000504 2B               [12] 1491 	add	a,r3
      000505 F8               [12] 1492 	mov	r0,a
      000506 E4               [12] 1493 	clr	a
      000507 3C               [12] 1494 	addc	a,r4
      000508 F9               [12] 1495 	mov	r1,a
      000509 88 82            [24] 1496 	mov	dpl,r0
      00050B 89 83            [24] 1497 	mov	dph,r1
      00050D E0               [24] 1498 	movx	a,@dptr
      00050E 60 51            [24] 1499 	jz	00106$
                                   1500 ;	source/CH549_SubwayBoard.c:122: LIGHT_ON(SubwayLine[LINE][DOT]);
      000510 C0 03            [24] 1501 	push	ar3
      000512 C0 04            [24] 1502 	push	ar4
      000514 ED               [12] 1503 	mov	a,r5
      000515 24 01            [12] 1504 	add	a,#_SubwayLine
      000517 F8               [12] 1505 	mov	r0,a
      000518 EE               [12] 1506 	mov	a,r6
      000519 34 00            [12] 1507 	addc	a,#(_SubwayLine >> 8)
      00051B F9               [12] 1508 	mov	r1,a
      00051C EA               [12] 1509 	mov	a,r2
      00051D 28               [12] 1510 	add	a,r0
      00051E F8               [12] 1511 	mov	r0,a
      00051F E4               [12] 1512 	clr	a
      000520 39               [12] 1513 	addc	a,r1
      000521 F9               [12] 1514 	mov	r1,a
      000522 88 82            [24] 1515 	mov	dpl,r0
      000524 89 83            [24] 1516 	mov	dph,r1
      000526 E0               [24] 1517 	movx	a,@dptr
      000527 FC               [12] 1518 	mov	r4,a
      000528 F5 82            [12] 1519 	mov	dpl,a
      00052A C0 07            [24] 1520 	push	ar7
      00052C C0 06            [24] 1521 	push	ar6
      00052E C0 05            [24] 1522 	push	ar5
      000530 C0 04            [24] 1523 	push	ar4
      000532 C0 03            [24] 1524 	push	ar3
      000534 C0 02            [24] 1525 	push	ar2
      000536 C0 01            [24] 1526 	push	ar1
      000538 C0 00            [24] 1527 	push	ar0
      00053A 12 03 6C         [24] 1528 	lcall	_LIGHT_ON
                                   1529 ;	source/CH549_SubwayBoard.c:123: mDelaymS(50);
      00053D 90 00 32         [24] 1530 	mov	dptr,#0x0032
      000540 12 02 FA         [24] 1531 	lcall	_mDelaymS
      000543 D0 00            [24] 1532 	pop	ar0
      000545 D0 01            [24] 1533 	pop	ar1
                                   1534 ;	source/CH549_SubwayBoard.c:124: LIGHT_OFF(SubwayLine[LINE][DOT]);
      000547 88 82            [24] 1535 	mov	dpl,r0
      000549 89 83            [24] 1536 	mov	dph,r1
      00054B E0               [24] 1537 	movx	a,@dptr
      00054C F5 82            [12] 1538 	mov	dpl,a
      00054E 12 04 10         [24] 1539 	lcall	_LIGHT_OFF
      000551 D0 02            [24] 1540 	pop	ar2
      000553 D0 03            [24] 1541 	pop	ar3
      000555 D0 04            [24] 1542 	pop	ar4
      000557 D0 05            [24] 1543 	pop	ar5
      000559 D0 06            [24] 1544 	pop	ar6
      00055B D0 07            [24] 1545 	pop	ar7
                                   1546 ;	source/CH549_SubwayBoard.c:116: for ( LINE = 0; LINE < 6; LINE++)
      00055D D0 04            [24] 1547 	pop	ar4
      00055F D0 03            [24] 1548 	pop	ar3
                                   1549 ;	source/CH549_SubwayBoard.c:124: LIGHT_OFF(SubwayLine[LINE][DOT]);
      000561                       1550 00106$:
                                   1551 ;	source/CH549_SubwayBoard.c:118: for ( DOT = 0; DOT < 7; DOT++)
      000561 0A               [12] 1552 	inc	r2
      000562 BA 07 00         [24] 1553 	cjne	r2,#0x07,00130$
      000565                       1554 00130$:
      000565 40 9C            [24] 1555 	jc	00105$
                                   1556 ;	source/CH549_SubwayBoard.c:116: for ( LINE = 0; LINE < 6; LINE++)
      000567 0F               [12] 1557 	inc	r7
      000568 BF 06 00         [24] 1558 	cjne	r7,#0x06,00132$
      00056B                       1559 00132$:
      00056B 40 85            [24] 1560 	jc	00112$
                                   1561 ;	source/CH549_SubwayBoard.c:132: }
      00056D 22               [24] 1562 	ret
                                   1563 ;------------------------------------------------------------
                                   1564 ;Allocation info for local variables in function 'LIGHT_FLineOn'
                                   1565 ;------------------------------------------------------------
                                   1566 ;DOT                       Allocated to registers r2 
                                   1567 ;LINE                      Allocated to registers r7 
                                   1568 ;------------------------------------------------------------
                                   1569 ;	source/CH549_SubwayBoard.c:135: void LIGHT_FLineOn()
                                   1570 ;	-----------------------------------------
                                   1571 ;	 function LIGHT_FLineOn
                                   1572 ;	-----------------------------------------
      00056E                       1573 _LIGHT_FLineOn:
                                   1574 ;	source/CH549_SubwayBoard.c:138: for ( LINE = 0; LINE < 6; LINE++)
      00056E 7F 00            [12] 1575 	mov	r7,#0x00
                                   1576 ;	source/CH549_SubwayBoard.c:140: for ( DOT = 0; DOT < 7; DOT++)
      000570                       1577 00112$:
      000570 EF               [12] 1578 	mov	a,r7
      000571 75 F0 07         [24] 1579 	mov	b,#0x07
      000574 A4               [48] 1580 	mul	ab
      000575 FD               [12] 1581 	mov	r5,a
      000576 AE F0            [24] 1582 	mov	r6,b
      000578 24 01            [12] 1583 	add	a,#_SubwayLine
      00057A FB               [12] 1584 	mov	r3,a
      00057B EE               [12] 1585 	mov	a,r6
      00057C 34 00            [12] 1586 	addc	a,#(_SubwayLine >> 8)
      00057E FC               [12] 1587 	mov	r4,a
      00057F 7A 00            [12] 1588 	mov	r2,#0x00
      000581                       1589 00105$:
                                   1590 ;	source/CH549_SubwayBoard.c:142: if (SubwayLine[LINE][6-DOT])
      000581 C0 05            [24] 1591 	push	ar5
      000583 C0 06            [24] 1592 	push	ar6
      000585 8A 01            [24] 1593 	mov	ar1,r2
      000587 74 06            [12] 1594 	mov	a,#0x06
      000589 C3               [12] 1595 	clr	c
      00058A 99               [12] 1596 	subb	a,r1
      00058B 2B               [12] 1597 	add	a,r3
      00058C F8               [12] 1598 	mov	r0,a
      00058D E4               [12] 1599 	clr	a
      00058E 3C               [12] 1600 	addc	a,r4
      00058F FE               [12] 1601 	mov	r6,a
      000590 88 82            [24] 1602 	mov	dpl,r0
      000592 8E 83            [24] 1603 	mov	dph,r6
      000594 E0               [24] 1604 	movx	a,@dptr
      000595 D0 06            [24] 1605 	pop	ar6
      000597 D0 05            [24] 1606 	pop	ar5
      000599 60 5C            [24] 1607 	jz	00106$
                                   1608 ;	source/CH549_SubwayBoard.c:144: LIGHT_ON(SubwayLine[LINE][6-DOT]);
      00059B C0 03            [24] 1609 	push	ar3
      00059D C0 04            [24] 1610 	push	ar4
      00059F ED               [12] 1611 	mov	a,r5
      0005A0 24 01            [12] 1612 	add	a,#_SubwayLine
      0005A2 F8               [12] 1613 	mov	r0,a
      0005A3 EE               [12] 1614 	mov	a,r6
      0005A4 34 00            [12] 1615 	addc	a,#(_SubwayLine >> 8)
      0005A6 FC               [12] 1616 	mov	r4,a
      0005A7 74 06            [12] 1617 	mov	a,#0x06
      0005A9 C3               [12] 1618 	clr	c
      0005AA 99               [12] 1619 	subb	a,r1
      0005AB 28               [12] 1620 	add	a,r0
      0005AC F8               [12] 1621 	mov	r0,a
      0005AD E4               [12] 1622 	clr	a
      0005AE 3C               [12] 1623 	addc	a,r4
      0005AF FC               [12] 1624 	mov	r4,a
      0005B0 88 82            [24] 1625 	mov	dpl,r0
      0005B2 8C 83            [24] 1626 	mov	dph,r4
      0005B4 E0               [24] 1627 	movx	a,@dptr
      0005B5 FB               [12] 1628 	mov	r3,a
      0005B6 F5 82            [12] 1629 	mov	dpl,a
      0005B8 C0 07            [24] 1630 	push	ar7
      0005BA C0 06            [24] 1631 	push	ar6
      0005BC C0 05            [24] 1632 	push	ar5
      0005BE C0 04            [24] 1633 	push	ar4
      0005C0 C0 03            [24] 1634 	push	ar3
      0005C2 C0 02            [24] 1635 	push	ar2
      0005C4 C0 00            [24] 1636 	push	ar0
      0005C6 12 03 6C         [24] 1637 	lcall	_LIGHT_ON
                                   1638 ;	source/CH549_SubwayBoard.c:145: mDelaymS(50);
      0005C9 90 00 32         [24] 1639 	mov	dptr,#0x0032
      0005CC 12 02 FA         [24] 1640 	lcall	_mDelaymS
      0005CF D0 00            [24] 1641 	pop	ar0
      0005D1 D0 02            [24] 1642 	pop	ar2
      0005D3 D0 03            [24] 1643 	pop	ar3
      0005D5 D0 04            [24] 1644 	pop	ar4
                                   1645 ;	source/CH549_SubwayBoard.c:146: LIGHT_OFF(SubwayLine[LINE][6-DOT]);
      0005D7 88 82            [24] 1646 	mov	dpl,r0
      0005D9 8C 83            [24] 1647 	mov	dph,r4
      0005DB E0               [24] 1648 	movx	a,@dptr
      0005DC F5 82            [12] 1649 	mov	dpl,a
      0005DE C0 04            [24] 1650 	push	ar4
      0005E0 C0 03            [24] 1651 	push	ar3
      0005E2 C0 02            [24] 1652 	push	ar2
      0005E4 12 04 10         [24] 1653 	lcall	_LIGHT_OFF
      0005E7 D0 02            [24] 1654 	pop	ar2
      0005E9 D0 03            [24] 1655 	pop	ar3
      0005EB D0 04            [24] 1656 	pop	ar4
      0005ED D0 05            [24] 1657 	pop	ar5
      0005EF D0 06            [24] 1658 	pop	ar6
      0005F1 D0 07            [24] 1659 	pop	ar7
                                   1660 ;	source/CH549_SubwayBoard.c:138: for ( LINE = 0; LINE < 6; LINE++)
      0005F3 D0 04            [24] 1661 	pop	ar4
      0005F5 D0 03            [24] 1662 	pop	ar3
                                   1663 ;	source/CH549_SubwayBoard.c:146: LIGHT_OFF(SubwayLine[LINE][6-DOT]);
      0005F7                       1664 00106$:
                                   1665 ;	source/CH549_SubwayBoard.c:140: for ( DOT = 0; DOT < 7; DOT++)
      0005F7 0A               [12] 1666 	inc	r2
      0005F8 BA 07 00         [24] 1667 	cjne	r2,#0x07,00130$
      0005FB                       1668 00130$:
      0005FB 40 84            [24] 1669 	jc	00105$
                                   1670 ;	source/CH549_SubwayBoard.c:138: for ( LINE = 0; LINE < 6; LINE++)
      0005FD 0F               [12] 1671 	inc	r7
      0005FE BF 06 00         [24] 1672 	cjne	r7,#0x06,00132$
      000601                       1673 00132$:
      000601 50 03            [24] 1674 	jnc	00133$
      000603 02 05 70         [24] 1675 	ljmp	00112$
      000606                       1676 00133$:
                                   1677 ;	source/CH549_SubwayBoard.c:154: }
      000606 22               [24] 1678 	ret
                                   1679 ;------------------------------------------------------------
                                   1680 ;Allocation info for local variables in function 'LIGHT_Converge'
                                   1681 ;------------------------------------------------------------
                                   1682 ;FREQs                     Allocated to stack - _bp +7
                                   1683 ;TIME                      Allocated to stack - _bp +12
                                   1684 ;DOT                       Allocated to registers r7 
                                   1685 ;CIRCLE                    Allocated to stack - _bp +9
                                   1686 ;FREQ                      Allocated to stack - _bp +10
                                   1687 ;sloc0                     Allocated to stack - _bp +1
                                   1688 ;sloc1                     Allocated to stack - _bp +25
                                   1689 ;sloc2                     Allocated to stack - _bp +3
                                   1690 ;------------------------------------------------------------
                                   1691 ;	source/CH549_SubwayBoard.c:157: void LIGHT_Converge(UINT16 FREQs)
                                   1692 ;	-----------------------------------------
                                   1693 ;	 function LIGHT_Converge
                                   1694 ;	-----------------------------------------
      000607                       1695 _LIGHT_Converge:
      000607 C0 08            [24] 1696 	push	_bp
      000609 E5 81            [12] 1697 	mov	a,sp
      00060B F5 08            [12] 1698 	mov	_bp,a
      00060D 24 1B            [12] 1699 	add	a,#0x1b
      00060F F5 81            [12] 1700 	mov	sp,a
      000611 C8               [12] 1701 	xch	a,r0
      000612 E5 08            [12] 1702 	mov	a,_bp
      000614 24 07            [12] 1703 	add	a,#0x07
      000616 C8               [12] 1704 	xch	a,r0
      000617 A6 82            [24] 1705 	mov	@r0,dpl
      000619 08               [12] 1706 	inc	r0
      00061A A6 83            [24] 1707 	mov	@r0,dph
                                   1708 ;	source/CH549_SubwayBoard.c:159: float TIME[4] = {1 ,1.8, 1.8, 9};
      00061C E5 08            [12] 1709 	mov	a,_bp
      00061E 24 0C            [12] 1710 	add	a,#0x0c
      000620 F9               [12] 1711 	mov	r1,a
      000621 77 00            [12] 1712 	mov	@r1,#0x00
      000623 09               [12] 1713 	inc	r1
      000624 77 00            [12] 1714 	mov	@r1,#0x00
      000626 09               [12] 1715 	inc	r1
      000627 77 80            [12] 1716 	mov	@r1,#0x80
      000629 09               [12] 1717 	inc	r1
      00062A 77 3F            [12] 1718 	mov	@r1,#0x3f
      00062C 19               [12] 1719 	dec	r1
      00062D 19               [12] 1720 	dec	r1
      00062E 19               [12] 1721 	dec	r1
      00062F 74 04            [12] 1722 	mov	a,#0x04
      000631 29               [12] 1723 	add	a,r1
      000632 F8               [12] 1724 	mov	r0,a
      000633 76 66            [12] 1725 	mov	@r0,#0x66
      000635 08               [12] 1726 	inc	r0
      000636 76 66            [12] 1727 	mov	@r0,#0x66
      000638 08               [12] 1728 	inc	r0
      000639 76 E6            [12] 1729 	mov	@r0,#0xe6
      00063B 08               [12] 1730 	inc	r0
      00063C 76 3F            [12] 1731 	mov	@r0,#0x3f
      00063E 74 08            [12] 1732 	mov	a,#0x08
      000640 29               [12] 1733 	add	a,r1
      000641 F8               [12] 1734 	mov	r0,a
      000642 76 66            [12] 1735 	mov	@r0,#0x66
      000644 08               [12] 1736 	inc	r0
      000645 76 66            [12] 1737 	mov	@r0,#0x66
      000647 08               [12] 1738 	inc	r0
      000648 76 E6            [12] 1739 	mov	@r0,#0xe6
      00064A 08               [12] 1740 	inc	r0
      00064B 76 3F            [12] 1741 	mov	@r0,#0x3f
      00064D 74 0C            [12] 1742 	mov	a,#0x0c
      00064F 29               [12] 1743 	add	a,r1
      000650 F8               [12] 1744 	mov	r0,a
      000651 76 00            [12] 1745 	mov	@r0,#0x00
      000653 08               [12] 1746 	inc	r0
      000654 76 00            [12] 1747 	mov	@r0,#0x00
      000656 08               [12] 1748 	inc	r0
      000657 76 10            [12] 1749 	mov	@r0,#0x10
      000659 08               [12] 1750 	inc	r0
      00065A 76 41            [12] 1751 	mov	@r0,#0x41
                                   1752 ;	source/CH549_SubwayBoard.c:163: for ( CIRCLE = 0; CIRCLE < 4; CIRCLE++)
      00065C E5 08            [12] 1753 	mov	a,_bp
      00065E 24 09            [12] 1754 	add	a,#0x09
      000660 F8               [12] 1755 	mov	r0,a
      000661 76 00            [12] 1756 	mov	@r0,#0x00
                                   1757 ;	source/CH549_SubwayBoard.c:165: for ( FREQ = 0; FREQ < FREQs * TIME[CIRCLE]; FREQ++)
      000663                       1758 00119$:
      000663 E5 08            [12] 1759 	mov	a,_bp
      000665 24 09            [12] 1760 	add	a,#0x09
      000667 F8               [12] 1761 	mov	r0,a
      000668 E6               [12] 1762 	mov	a,@r0
      000669 75 F0 09         [24] 1763 	mov	b,#0x09
      00066C A4               [48] 1764 	mul	ab
      00066D 24 2B            [12] 1765 	add	a,#_SubwayCircle
      00066F FB               [12] 1766 	mov	r3,a
      000670 74 00            [12] 1767 	mov	a,#(_SubwayCircle >> 8)
      000672 35 F0            [12] 1768 	addc	a,b
      000674 FC               [12] 1769 	mov	r4,a
      000675 A8 08            [24] 1770 	mov	r0,_bp
      000677 08               [12] 1771 	inc	r0
      000678 A6 03            [24] 1772 	mov	@r0,ar3
      00067A 08               [12] 1773 	inc	r0
      00067B A6 04            [24] 1774 	mov	@r0,ar4
      00067D E5 08            [12] 1775 	mov	a,_bp
      00067F 24 09            [12] 1776 	add	a,#0x09
      000681 F8               [12] 1777 	mov	r0,a
      000682 E6               [12] 1778 	mov	a,@r0
      000683 26               [12] 1779 	add	a,@r0
      000684 25 E0            [12] 1780 	add	a,acc
      000686 FA               [12] 1781 	mov	r2,a
      000687 29               [12] 1782 	add	a,r1
      000688 F8               [12] 1783 	mov	r0,a
      000689 C0 00            [24] 1784 	push	ar0
      00068B E5 08            [12] 1785 	mov	a,_bp
      00068D 24 0A            [12] 1786 	add	a,#0x0a
      00068F F8               [12] 1787 	mov	r0,a
      000690 E4               [12] 1788 	clr	a
      000691 F6               [12] 1789 	mov	@r0,a
      000692 08               [12] 1790 	inc	r0
      000693 F6               [12] 1791 	mov	@r0,a
      000694 D0 00            [24] 1792 	pop	ar0
      000696                       1793 00109$:
      000696 C0 01            [24] 1794 	push	ar1
      000698 C0 01            [24] 1795 	push	ar1
      00069A E5 08            [12] 1796 	mov	a,_bp
      00069C 24 03            [12] 1797 	add	a,#0x03
      00069E F9               [12] 1798 	mov	r1,a
      00069F E6               [12] 1799 	mov	a,@r0
      0006A0 F7               [12] 1800 	mov	@r1,a
      0006A1 08               [12] 1801 	inc	r0
      0006A2 E6               [12] 1802 	mov	a,@r0
      0006A3 09               [12] 1803 	inc	r1
      0006A4 F7               [12] 1804 	mov	@r1,a
      0006A5 08               [12] 1805 	inc	r0
      0006A6 E6               [12] 1806 	mov	a,@r0
      0006A7 09               [12] 1807 	inc	r1
      0006A8 F7               [12] 1808 	mov	@r1,a
      0006A9 08               [12] 1809 	inc	r0
      0006AA E6               [12] 1810 	mov	a,@r0
      0006AB 09               [12] 1811 	inc	r1
      0006AC F7               [12] 1812 	mov	@r1,a
      0006AD 18               [12] 1813 	dec	r0
      0006AE 18               [12] 1814 	dec	r0
      0006AF 18               [12] 1815 	dec	r0
      0006B0 D0 01            [24] 1816 	pop	ar1
      0006B2 C0 00            [24] 1817 	push	ar0
      0006B4 E5 08            [12] 1818 	mov	a,_bp
      0006B6 24 07            [12] 1819 	add	a,#0x07
      0006B8 F8               [12] 1820 	mov	r0,a
      0006B9 86 82            [24] 1821 	mov	dpl,@r0
      0006BB 08               [12] 1822 	inc	r0
      0006BC 86 83            [24] 1823 	mov	dph,@r0
      0006BE D0 00            [24] 1824 	pop	ar0
      0006C0 C0 04            [24] 1825 	push	ar4
      0006C2 C0 03            [24] 1826 	push	ar3
      0006C4 C0 01            [24] 1827 	push	ar1
      0006C6 C0 00            [24] 1828 	push	ar0
      0006C8 12 0E 38         [24] 1829 	lcall	___uint2fs
      0006CB AA 82            [24] 1830 	mov	r2,dpl
      0006CD AD 83            [24] 1831 	mov	r5,dph
      0006CF AE F0            [24] 1832 	mov	r6,b
      0006D1 FF               [12] 1833 	mov	r7,a
      0006D2 D0 00            [24] 1834 	pop	ar0
      0006D4 C0 00            [24] 1835 	push	ar0
      0006D6 85 00 F0         [24] 1836 	mov	b,ar0
      0006D9 E5 08            [12] 1837 	mov	a,_bp
      0006DB 24 03            [12] 1838 	add	a,#0x03
      0006DD F8               [12] 1839 	mov	r0,a
      0006DE E6               [12] 1840 	mov	a,@r0
      0006DF C0 E0            [24] 1841 	push	acc
      0006E1 08               [12] 1842 	inc	r0
      0006E2 E6               [12] 1843 	mov	a,@r0
      0006E3 C0 E0            [24] 1844 	push	acc
      0006E5 08               [12] 1845 	inc	r0
      0006E6 E6               [12] 1846 	mov	a,@r0
      0006E7 C0 E0            [24] 1847 	push	acc
      0006E9 08               [12] 1848 	inc	r0
      0006EA E6               [12] 1849 	mov	a,@r0
      0006EB C0 E0            [24] 1850 	push	acc
      0006ED 8A 82            [24] 1851 	mov	dpl,r2
      0006EF 8D 83            [24] 1852 	mov	dph,r5
      0006F1 8E F0            [24] 1853 	mov	b,r6
      0006F3 EF               [12] 1854 	mov	a,r7
      0006F4 12 0C 1D         [24] 1855 	lcall	___fsmul
      0006F7 C0 00            [24] 1856 	push	ar0
      0006F9 A8 08            [24] 1857 	mov	r0,_bp
      0006FB 08               [12] 1858 	inc	r0
      0006FC 08               [12] 1859 	inc	r0
      0006FD 08               [12] 1860 	inc	r0
      0006FE A6 82            [24] 1861 	mov	@r0,dpl
      000700 08               [12] 1862 	inc	r0
      000701 A6 83            [24] 1863 	mov	@r0,dph
      000703 08               [12] 1864 	inc	r0
      000704 A6 F0            [24] 1865 	mov	@r0,b
      000706 08               [12] 1866 	inc	r0
      000707 F6               [12] 1867 	mov	@r0,a
      000708 D0 00            [24] 1868 	pop	ar0
      00070A E5 81            [12] 1869 	mov	a,sp
      00070C 24 FC            [12] 1870 	add	a,#0xfc
      00070E F5 81            [12] 1871 	mov	sp,a
      000710 D0 00            [24] 1872 	pop	ar0
      000712 D0 01            [24] 1873 	pop	ar1
      000714 D0 03            [24] 1874 	pop	ar3
      000716 D0 04            [24] 1875 	pop	ar4
      000718 C0 00            [24] 1876 	push	ar0
      00071A E5 08            [12] 1877 	mov	a,_bp
      00071C 24 0A            [12] 1878 	add	a,#0x0a
      00071E F8               [12] 1879 	mov	r0,a
      00071F 86 82            [24] 1880 	mov	dpl,@r0
      000721 08               [12] 1881 	inc	r0
      000722 86 83            [24] 1882 	mov	dph,@r0
      000724 D0 00            [24] 1883 	pop	ar0
      000726 C0 04            [24] 1884 	push	ar4
      000728 C0 03            [24] 1885 	push	ar3
      00072A C0 01            [24] 1886 	push	ar1
      00072C C0 00            [24] 1887 	push	ar0
      00072E 12 0E 38         [24] 1888 	lcall	___uint2fs
      000731 AA 82            [24] 1889 	mov	r2,dpl
      000733 AD 83            [24] 1890 	mov	r5,dph
      000735 AE F0            [24] 1891 	mov	r6,b
      000737 FF               [12] 1892 	mov	r7,a
      000738 D0 00            [24] 1893 	pop	ar0
      00073A C0 00            [24] 1894 	push	ar0
      00073C 85 00 F0         [24] 1895 	mov	b,ar0
      00073F E5 08            [12] 1896 	mov	a,_bp
      000741 24 03            [12] 1897 	add	a,#0x03
      000743 F8               [12] 1898 	mov	r0,a
      000744 E6               [12] 1899 	mov	a,@r0
      000745 C0 E0            [24] 1900 	push	acc
      000747 08               [12] 1901 	inc	r0
      000748 E6               [12] 1902 	mov	a,@r0
      000749 C0 E0            [24] 1903 	push	acc
      00074B 08               [12] 1904 	inc	r0
      00074C E6               [12] 1905 	mov	a,@r0
      00074D C0 E0            [24] 1906 	push	acc
      00074F 08               [12] 1907 	inc	r0
      000750 E6               [12] 1908 	mov	a,@r0
      000751 C0 E0            [24] 1909 	push	acc
      000753 8A 82            [24] 1910 	mov	dpl,r2
      000755 8D 83            [24] 1911 	mov	dph,r5
      000757 8E F0            [24] 1912 	mov	b,r6
      000759 EF               [12] 1913 	mov	a,r7
      00075A 12 0D 21         [24] 1914 	lcall	___fslt
      00075D AF 82            [24] 1915 	mov	r7,dpl
      00075F E5 81            [12] 1916 	mov	a,sp
      000761 24 FC            [12] 1917 	add	a,#0xfc
      000763 F5 81            [12] 1918 	mov	sp,a
      000765 D0 00            [24] 1919 	pop	ar0
      000767 D0 01            [24] 1920 	pop	ar1
      000769 D0 03            [24] 1921 	pop	ar3
      00076B D0 04            [24] 1922 	pop	ar4
      00076D D0 01            [24] 1923 	pop	ar1
      00076F EF               [12] 1924 	mov	a,r7
      000770 60 51            [24] 1925 	jz	00112$
                                   1926 ;	source/CH549_SubwayBoard.c:167: for ( DOT = 0; DOT < 9; DOT++)
      000772 7F 00            [12] 1927 	mov	r7,#0x00
      000774                       1928 00106$:
                                   1929 ;	source/CH549_SubwayBoard.c:169: if(SubwayCircle[CIRCLE][DOT])
      000774 EF               [12] 1930 	mov	a,r7
      000775 2B               [12] 1931 	add	a,r3
      000776 FD               [12] 1932 	mov	r5,a
      000777 E4               [12] 1933 	clr	a
      000778 3C               [12] 1934 	addc	a,r4
      000779 FE               [12] 1935 	mov	r6,a
      00077A 8D 82            [24] 1936 	mov	dpl,r5
      00077C 8E 83            [24] 1937 	mov	dph,r6
      00077E E0               [24] 1938 	movx	a,@dptr
      00077F 60 2A            [24] 1939 	jz	00107$
                                   1940 ;	source/CH549_SubwayBoard.c:171: LIGHT_Flash(SubwayCircle[CIRCLE][DOT]);
      000781 C0 00            [24] 1941 	push	ar0
      000783 A8 08            [24] 1942 	mov	r0,_bp
      000785 08               [12] 1943 	inc	r0
      000786 EF               [12] 1944 	mov	a,r7
      000787 26               [12] 1945 	add	a,@r0
      000788 F5 82            [12] 1946 	mov	dpl,a
      00078A E4               [12] 1947 	clr	a
      00078B 08               [12] 1948 	inc	r0
      00078C 36               [12] 1949 	addc	a,@r0
      00078D F5 83            [12] 1950 	mov	dph,a
      00078F D0 00            [24] 1951 	pop	ar0
      000791 E0               [24] 1952 	movx	a,@dptr
      000792 F5 82            [12] 1953 	mov	dpl,a
      000794 C0 07            [24] 1954 	push	ar7
      000796 C0 04            [24] 1955 	push	ar4
      000798 C0 03            [24] 1956 	push	ar3
      00079A C0 01            [24] 1957 	push	ar1
      00079C C0 00            [24] 1958 	push	ar0
      00079E 12 04 B4         [24] 1959 	lcall	_LIGHT_Flash
      0007A1 D0 00            [24] 1960 	pop	ar0
      0007A3 D0 01            [24] 1961 	pop	ar1
      0007A5 D0 03            [24] 1962 	pop	ar3
      0007A7 D0 04            [24] 1963 	pop	ar4
      0007A9 D0 07            [24] 1964 	pop	ar7
      0007AB                       1965 00107$:
                                   1966 ;	source/CH549_SubwayBoard.c:167: for ( DOT = 0; DOT < 9; DOT++)
      0007AB 0F               [12] 1967 	inc	r7
      0007AC BF 09 00         [24] 1968 	cjne	r7,#0x09,00144$
      0007AF                       1969 00144$:
      0007AF 40 C3            [24] 1970 	jc	00106$
                                   1971 ;	source/CH549_SubwayBoard.c:165: for ( FREQ = 0; FREQ < FREQs * TIME[CIRCLE]; FREQ++)
      0007B1 C0 00            [24] 1972 	push	ar0
      0007B3 E5 08            [12] 1973 	mov	a,_bp
      0007B5 24 0A            [12] 1974 	add	a,#0x0a
      0007B7 F8               [12] 1975 	mov	r0,a
      0007B8 06               [12] 1976 	inc	@r0
      0007B9 B6 00 02         [24] 1977 	cjne	@r0,#0x00,00146$
      0007BC 08               [12] 1978 	inc	r0
      0007BD 06               [12] 1979 	inc	@r0
      0007BE                       1980 00146$:
      0007BE D0 00            [24] 1981 	pop	ar0
      0007C0 02 06 96         [24] 1982 	ljmp	00109$
      0007C3                       1983 00112$:
                                   1984 ;	source/CH549_SubwayBoard.c:163: for ( CIRCLE = 0; CIRCLE < 4; CIRCLE++)
      0007C3 E5 08            [12] 1985 	mov	a,_bp
      0007C5 24 09            [12] 1986 	add	a,#0x09
      0007C7 F8               [12] 1987 	mov	r0,a
      0007C8 06               [12] 1988 	inc	@r0
      0007C9 E5 08            [12] 1989 	mov	a,_bp
      0007CB 24 09            [12] 1990 	add	a,#0x09
      0007CD F8               [12] 1991 	mov	r0,a
      0007CE B6 04 00         [24] 1992 	cjne	@r0,#0x04,00147$
      0007D1                       1993 00147$:
      0007D1 50 03            [24] 1994 	jnc	00148$
      0007D3 02 06 63         [24] 1995 	ljmp	00119$
      0007D6                       1996 00148$:
                                   1997 ;	source/CH549_SubwayBoard.c:181: }
      0007D6 85 08 81         [24] 1998 	mov	sp,_bp
      0007D9 D0 08            [24] 1999 	pop	_bp
      0007DB 22               [24] 2000 	ret
                                   2001 ;------------------------------------------------------------
                                   2002 ;Allocation info for local variables in function 'LIGHT_Disperse'
                                   2003 ;------------------------------------------------------------
                                   2004 ;FREQs                     Allocated to stack - _bp +1
                                   2005 ;TIME                      Allocated to stack - _bp +9
                                   2006 ;DOT                       Allocated to registers r7 
                                   2007 ;CIRCLE                    Allocated to stack - _bp +25
                                   2008 ;FREQ                      Allocated to stack - _bp +7
                                   2009 ;sloc0                     Allocated to stack - _bp +3
                                   2010 ;------------------------------------------------------------
                                   2011 ;	source/CH549_SubwayBoard.c:184: void LIGHT_Disperse(UINT16 FREQs)
                                   2012 ;	-----------------------------------------
                                   2013 ;	 function LIGHT_Disperse
                                   2014 ;	-----------------------------------------
      0007DC                       2015 _LIGHT_Disperse:
      0007DC C0 08            [24] 2016 	push	_bp
      0007DE 85 81 08         [24] 2017 	mov	_bp,sp
      0007E1 C0 82            [24] 2018 	push	dpl
      0007E3 C0 83            [24] 2019 	push	dph
      0007E5 E5 81            [12] 2020 	mov	a,sp
      0007E7 24 17            [12] 2021 	add	a,#0x17
      0007E9 F5 81            [12] 2022 	mov	sp,a
                                   2023 ;	source/CH549_SubwayBoard.c:186: float TIME[4] = {1 ,1.8, 1.8, 9};
      0007EB E5 08            [12] 2024 	mov	a,_bp
      0007ED 24 09            [12] 2025 	add	a,#0x09
      0007EF F9               [12] 2026 	mov	r1,a
      0007F0 77 00            [12] 2027 	mov	@r1,#0x00
      0007F2 09               [12] 2028 	inc	r1
      0007F3 77 00            [12] 2029 	mov	@r1,#0x00
      0007F5 09               [12] 2030 	inc	r1
      0007F6 77 80            [12] 2031 	mov	@r1,#0x80
      0007F8 09               [12] 2032 	inc	r1
      0007F9 77 3F            [12] 2033 	mov	@r1,#0x3f
      0007FB 19               [12] 2034 	dec	r1
      0007FC 19               [12] 2035 	dec	r1
      0007FD 19               [12] 2036 	dec	r1
      0007FE 74 04            [12] 2037 	mov	a,#0x04
      000800 29               [12] 2038 	add	a,r1
      000801 F8               [12] 2039 	mov	r0,a
      000802 76 66            [12] 2040 	mov	@r0,#0x66
      000804 08               [12] 2041 	inc	r0
      000805 76 66            [12] 2042 	mov	@r0,#0x66
      000807 08               [12] 2043 	inc	r0
      000808 76 E6            [12] 2044 	mov	@r0,#0xe6
      00080A 08               [12] 2045 	inc	r0
      00080B 76 3F            [12] 2046 	mov	@r0,#0x3f
      00080D 74 08            [12] 2047 	mov	a,#0x08
      00080F 29               [12] 2048 	add	a,r1
      000810 F8               [12] 2049 	mov	r0,a
      000811 76 66            [12] 2050 	mov	@r0,#0x66
      000813 08               [12] 2051 	inc	r0
      000814 76 66            [12] 2052 	mov	@r0,#0x66
      000816 08               [12] 2053 	inc	r0
      000817 76 E6            [12] 2054 	mov	@r0,#0xe6
      000819 08               [12] 2055 	inc	r0
      00081A 76 3F            [12] 2056 	mov	@r0,#0x3f
      00081C 74 0C            [12] 2057 	mov	a,#0x0c
      00081E 29               [12] 2058 	add	a,r1
      00081F F8               [12] 2059 	mov	r0,a
      000820 76 00            [12] 2060 	mov	@r0,#0x00
      000822 08               [12] 2061 	inc	r0
      000823 76 00            [12] 2062 	mov	@r0,#0x00
      000825 08               [12] 2063 	inc	r0
      000826 76 10            [12] 2064 	mov	@r0,#0x10
      000828 08               [12] 2065 	inc	r0
      000829 76 41            [12] 2066 	mov	@r0,#0x41
                                   2067 ;	source/CH549_SubwayBoard.c:190: for ( CIRCLE = 0; CIRCLE < 4; CIRCLE++)
      00082B E5 08            [12] 2068 	mov	a,_bp
      00082D 24 19            [12] 2069 	add	a,#0x19
      00082F F8               [12] 2070 	mov	r0,a
      000830 76 00            [12] 2071 	mov	@r0,#0x00
                                   2072 ;	source/CH549_SubwayBoard.c:192: for ( FREQ = 0; FREQ < FREQs * TIME[3 - CIRCLE]; FREQ++)
      000832                       2073 00119$:
      000832 E5 08            [12] 2074 	mov	a,_bp
      000834 24 07            [12] 2075 	add	a,#0x07
      000836 F8               [12] 2076 	mov	r0,a
      000837 E4               [12] 2077 	clr	a
      000838 F6               [12] 2078 	mov	@r0,a
      000839 08               [12] 2079 	inc	r0
      00083A F6               [12] 2080 	mov	@r0,a
      00083B                       2081 00109$:
      00083B E5 08            [12] 2082 	mov	a,_bp
      00083D 24 19            [12] 2083 	add	a,#0x19
      00083F F8               [12] 2084 	mov	r0,a
      000840 86 02            [24] 2085 	mov	ar2,@r0
      000842 74 03            [12] 2086 	mov	a,#0x03
      000844 C3               [12] 2087 	clr	c
      000845 9A               [12] 2088 	subb	a,r2
      000846 25 E0            [12] 2089 	add	a,acc
      000848 25 E0            [12] 2090 	add	a,acc
      00084A 29               [12] 2091 	add	a,r1
      00084B F8               [12] 2092 	mov	r0,a
      00084C C0 01            [24] 2093 	push	ar1
      00084E C0 01            [24] 2094 	push	ar1
      000850 E5 08            [12] 2095 	mov	a,_bp
      000852 24 03            [12] 2096 	add	a,#0x03
      000854 F9               [12] 2097 	mov	r1,a
      000855 E6               [12] 2098 	mov	a,@r0
      000856 F7               [12] 2099 	mov	@r1,a
      000857 08               [12] 2100 	inc	r0
      000858 E6               [12] 2101 	mov	a,@r0
      000859 09               [12] 2102 	inc	r1
      00085A F7               [12] 2103 	mov	@r1,a
      00085B 08               [12] 2104 	inc	r0
      00085C E6               [12] 2105 	mov	a,@r0
      00085D 09               [12] 2106 	inc	r1
      00085E F7               [12] 2107 	mov	@r1,a
      00085F 08               [12] 2108 	inc	r0
      000860 E6               [12] 2109 	mov	a,@r0
      000861 09               [12] 2110 	inc	r1
      000862 F7               [12] 2111 	mov	@r1,a
      000863 18               [12] 2112 	dec	r0
      000864 18               [12] 2113 	dec	r0
      000865 18               [12] 2114 	dec	r0
      000866 A8 08            [24] 2115 	mov	r0,_bp
      000868 08               [12] 2116 	inc	r0
      000869 86 82            [24] 2117 	mov	dpl,@r0
      00086B 08               [12] 2118 	inc	r0
      00086C 86 83            [24] 2119 	mov	dph,@r0
      00086E 12 0E 38         [24] 2120 	lcall	___uint2fs
      000871 AB 82            [24] 2121 	mov	r3,dpl
      000873 AD 83            [24] 2122 	mov	r5,dph
      000875 AE F0            [24] 2123 	mov	r6,b
      000877 FF               [12] 2124 	mov	r7,a
      000878 E5 08            [12] 2125 	mov	a,_bp
      00087A 24 03            [12] 2126 	add	a,#0x03
      00087C F8               [12] 2127 	mov	r0,a
      00087D E6               [12] 2128 	mov	a,@r0
      00087E C0 E0            [24] 2129 	push	acc
      000880 08               [12] 2130 	inc	r0
      000881 E6               [12] 2131 	mov	a,@r0
      000882 C0 E0            [24] 2132 	push	acc
      000884 08               [12] 2133 	inc	r0
      000885 E6               [12] 2134 	mov	a,@r0
      000886 C0 E0            [24] 2135 	push	acc
      000888 08               [12] 2136 	inc	r0
      000889 E6               [12] 2137 	mov	a,@r0
      00088A C0 E0            [24] 2138 	push	acc
      00088C 8B 82            [24] 2139 	mov	dpl,r3
      00088E 8D 83            [24] 2140 	mov	dph,r5
      000890 8E F0            [24] 2141 	mov	b,r6
      000892 EF               [12] 2142 	mov	a,r7
      000893 12 0C 1D         [24] 2143 	lcall	___fsmul
      000896 A8 08            [24] 2144 	mov	r0,_bp
      000898 08               [12] 2145 	inc	r0
      000899 08               [12] 2146 	inc	r0
      00089A 08               [12] 2147 	inc	r0
      00089B A6 82            [24] 2148 	mov	@r0,dpl
      00089D 08               [12] 2149 	inc	r0
      00089E A6 83            [24] 2150 	mov	@r0,dph
      0008A0 08               [12] 2151 	inc	r0
      0008A1 A6 F0            [24] 2152 	mov	@r0,b
      0008A3 08               [12] 2153 	inc	r0
      0008A4 F6               [12] 2154 	mov	@r0,a
      0008A5 E5 81            [12] 2155 	mov	a,sp
      0008A7 24 FC            [12] 2156 	add	a,#0xfc
      0008A9 F5 81            [12] 2157 	mov	sp,a
      0008AB E5 08            [12] 2158 	mov	a,_bp
      0008AD 24 07            [12] 2159 	add	a,#0x07
      0008AF F8               [12] 2160 	mov	r0,a
      0008B0 86 82            [24] 2161 	mov	dpl,@r0
      0008B2 08               [12] 2162 	inc	r0
      0008B3 86 83            [24] 2163 	mov	dph,@r0
      0008B5 12 0E 38         [24] 2164 	lcall	___uint2fs
      0008B8 AA 82            [24] 2165 	mov	r2,dpl
      0008BA AB 83            [24] 2166 	mov	r3,dph
      0008BC AE F0            [24] 2167 	mov	r6,b
      0008BE FF               [12] 2168 	mov	r7,a
      0008BF E5 08            [12] 2169 	mov	a,_bp
      0008C1 24 03            [12] 2170 	add	a,#0x03
      0008C3 F8               [12] 2171 	mov	r0,a
      0008C4 E6               [12] 2172 	mov	a,@r0
      0008C5 C0 E0            [24] 2173 	push	acc
      0008C7 08               [12] 2174 	inc	r0
      0008C8 E6               [12] 2175 	mov	a,@r0
      0008C9 C0 E0            [24] 2176 	push	acc
      0008CB 08               [12] 2177 	inc	r0
      0008CC E6               [12] 2178 	mov	a,@r0
      0008CD C0 E0            [24] 2179 	push	acc
      0008CF 08               [12] 2180 	inc	r0
      0008D0 E6               [12] 2181 	mov	a,@r0
      0008D1 C0 E0            [24] 2182 	push	acc
      0008D3 8A 82            [24] 2183 	mov	dpl,r2
      0008D5 8B 83            [24] 2184 	mov	dph,r3
      0008D7 8E F0            [24] 2185 	mov	b,r6
      0008D9 EF               [12] 2186 	mov	a,r7
      0008DA 12 0D 21         [24] 2187 	lcall	___fslt
      0008DD AF 82            [24] 2188 	mov	r7,dpl
      0008DF E5 81            [12] 2189 	mov	a,sp
      0008E1 24 FC            [12] 2190 	add	a,#0xfc
      0008E3 F5 81            [12] 2191 	mov	sp,a
      0008E5 D0 01            [24] 2192 	pop	ar1
      0008E7 D0 01            [24] 2193 	pop	ar1
      0008E9 EF               [12] 2194 	mov	a,r7
      0008EA 70 03            [24] 2195 	jnz	00142$
      0008EC 02 09 82         [24] 2196 	ljmp	00112$
      0008EF                       2197 00142$:
                                   2198 ;	source/CH549_SubwayBoard.c:194: for ( DOT = 0; DOT < 9; DOT++)
      0008EF 7F 00            [12] 2199 	mov	r7,#0x00
      0008F1                       2200 00106$:
                                   2201 ;	source/CH549_SubwayBoard.c:196: if(SubwayCircle[3 - CIRCLE][DOT])
      0008F1 E5 08            [12] 2202 	mov	a,_bp
      0008F3 24 19            [12] 2203 	add	a,#0x19
      0008F5 F8               [12] 2204 	mov	r0,a
      0008F6 86 05            [24] 2205 	mov	ar5,@r0
      0008F8 74 03            [12] 2206 	mov	a,#0x03
      0008FA C3               [12] 2207 	clr	c
      0008FB 9D               [12] 2208 	subb	a,r5
      0008FC FE               [12] 2209 	mov	r6,a
      0008FD C2 D5            [12] 2210 	clr	F0
      0008FF 75 F0 09         [24] 2211 	mov	b,#0x09
      000902 EE               [12] 2212 	mov	a,r6
      000903 30 E7 04         [24] 2213 	jnb	acc.7,00143$
      000906 B2 D5            [12] 2214 	cpl	F0
      000908 F4               [12] 2215 	cpl	a
      000909 04               [12] 2216 	inc	a
      00090A                       2217 00143$:
      00090A A4               [48] 2218 	mul	ab
      00090B 30 D5 0A         [24] 2219 	jnb	F0,00144$
      00090E F4               [12] 2220 	cpl	a
      00090F 24 01            [12] 2221 	add	a,#0x01
      000911 C5 F0            [12] 2222 	xch	a,b
      000913 F4               [12] 2223 	cpl	a
      000914 34 00            [12] 2224 	addc	a,#0x00
      000916 C5 F0            [12] 2225 	xch	a,b
      000918                       2226 00144$:
      000918 24 2B            [12] 2227 	add	a,#_SubwayCircle
      00091A FC               [12] 2228 	mov	r4,a
      00091B 74 00            [12] 2229 	mov	a,#(_SubwayCircle >> 8)
      00091D 35 F0            [12] 2230 	addc	a,b
      00091F FE               [12] 2231 	mov	r6,a
      000920 EF               [12] 2232 	mov	a,r7
      000921 2C               [12] 2233 	add	a,r4
      000922 FC               [12] 2234 	mov	r4,a
      000923 E4               [12] 2235 	clr	a
      000924 3E               [12] 2236 	addc	a,r6
      000925 FE               [12] 2237 	mov	r6,a
      000926 8C 82            [24] 2238 	mov	dpl,r4
      000928 8E 83            [24] 2239 	mov	dph,r6
      00092A E0               [24] 2240 	movx	a,@dptr
      00092B 60 3E            [24] 2241 	jz	00107$
                                   2242 ;	source/CH549_SubwayBoard.c:198: LIGHT_Flash(SubwayCircle[3 - CIRCLE][DOT]);
      00092D 74 03            [12] 2243 	mov	a,#0x03
      00092F C3               [12] 2244 	clr	c
      000930 9D               [12] 2245 	subb	a,r5
      000931 FD               [12] 2246 	mov	r5,a
      000932 C2 D5            [12] 2247 	clr	F0
      000934 75 F0 09         [24] 2248 	mov	b,#0x09
      000937 ED               [12] 2249 	mov	a,r5
      000938 30 E7 04         [24] 2250 	jnb	acc.7,00146$
      00093B B2 D5            [12] 2251 	cpl	F0
      00093D F4               [12] 2252 	cpl	a
      00093E 04               [12] 2253 	inc	a
      00093F                       2254 00146$:
      00093F A4               [48] 2255 	mul	ab
      000940 30 D5 0A         [24] 2256 	jnb	F0,00147$
      000943 F4               [12] 2257 	cpl	a
      000944 24 01            [12] 2258 	add	a,#0x01
      000946 C5 F0            [12] 2259 	xch	a,b
      000948 F4               [12] 2260 	cpl	a
      000949 34 00            [12] 2261 	addc	a,#0x00
      00094B C5 F0            [12] 2262 	xch	a,b
      00094D                       2263 00147$:
      00094D 24 2B            [12] 2264 	add	a,#_SubwayCircle
      00094F FD               [12] 2265 	mov	r5,a
      000950 74 00            [12] 2266 	mov	a,#(_SubwayCircle >> 8)
      000952 35 F0            [12] 2267 	addc	a,b
      000954 FE               [12] 2268 	mov	r6,a
      000955 EF               [12] 2269 	mov	a,r7
      000956 2D               [12] 2270 	add	a,r5
      000957 F5 82            [12] 2271 	mov	dpl,a
      000959 E4               [12] 2272 	clr	a
      00095A 3E               [12] 2273 	addc	a,r6
      00095B F5 83            [12] 2274 	mov	dph,a
      00095D E0               [24] 2275 	movx	a,@dptr
      00095E F5 82            [12] 2276 	mov	dpl,a
      000960 C0 07            [24] 2277 	push	ar7
      000962 C0 01            [24] 2278 	push	ar1
      000964 12 04 B4         [24] 2279 	lcall	_LIGHT_Flash
      000967 D0 01            [24] 2280 	pop	ar1
      000969 D0 07            [24] 2281 	pop	ar7
      00096B                       2282 00107$:
                                   2283 ;	source/CH549_SubwayBoard.c:194: for ( DOT = 0; DOT < 9; DOT++)
      00096B 0F               [12] 2284 	inc	r7
      00096C BF 09 00         [24] 2285 	cjne	r7,#0x09,00148$
      00096F                       2286 00148$:
      00096F 50 03            [24] 2287 	jnc	00149$
      000971 02 08 F1         [24] 2288 	ljmp	00106$
      000974                       2289 00149$:
                                   2290 ;	source/CH549_SubwayBoard.c:192: for ( FREQ = 0; FREQ < FREQs * TIME[3 - CIRCLE]; FREQ++)
      000974 E5 08            [12] 2291 	mov	a,_bp
      000976 24 07            [12] 2292 	add	a,#0x07
      000978 F8               [12] 2293 	mov	r0,a
      000979 06               [12] 2294 	inc	@r0
      00097A B6 00 02         [24] 2295 	cjne	@r0,#0x00,00150$
      00097D 08               [12] 2296 	inc	r0
      00097E 06               [12] 2297 	inc	@r0
      00097F                       2298 00150$:
      00097F 02 08 3B         [24] 2299 	ljmp	00109$
      000982                       2300 00112$:
                                   2301 ;	source/CH549_SubwayBoard.c:190: for ( CIRCLE = 0; CIRCLE < 4; CIRCLE++)
      000982 E5 08            [12] 2302 	mov	a,_bp
      000984 24 19            [12] 2303 	add	a,#0x19
      000986 F8               [12] 2304 	mov	r0,a
      000987 06               [12] 2305 	inc	@r0
      000988 E5 08            [12] 2306 	mov	a,_bp
      00098A 24 19            [12] 2307 	add	a,#0x19
      00098C F8               [12] 2308 	mov	r0,a
      00098D B6 04 00         [24] 2309 	cjne	@r0,#0x04,00151$
      000990                       2310 00151$:
      000990 50 03            [24] 2311 	jnc	00152$
      000992 02 08 32         [24] 2312 	ljmp	00119$
      000995                       2313 00152$:
                                   2314 ;	source/CH549_SubwayBoard.c:208: }
      000995 85 08 81         [24] 2315 	mov	sp,_bp
      000998 D0 08            [24] 2316 	pop	_bp
      00099A 22               [24] 2317 	ret
                                   2318 ;------------------------------------------------------------
                                   2319 ;Allocation info for local variables in function 'LIGHT_Random'
                                   2320 ;------------------------------------------------------------
                                   2321 ;FREQs                     Allocated to stack - _bp +1
                                   2322 ;i                         Allocated to registers r6 
                                   2323 ;m                         Allocated to registers r5 
                                   2324 ;DOT                       Allocated to registers r2 
                                   2325 ;sloc0                     Allocated to stack - _bp +2
                                   2326 ;sloc1                     Allocated to stack - _bp +4
                                   2327 ;------------------------------------------------------------
                                   2328 ;	source/CH549_SubwayBoard.c:211: void LIGHT_Random(UINT8 FREQs)
                                   2329 ;	-----------------------------------------
                                   2330 ;	 function LIGHT_Random
                                   2331 ;	-----------------------------------------
      00099B                       2332 _LIGHT_Random:
      00099B C0 08            [24] 2333 	push	_bp
      00099D 85 81 08         [24] 2334 	mov	_bp,sp
      0009A0 C0 82            [24] 2335 	push	dpl
      0009A2 E5 81            [12] 2336 	mov	a,sp
      0009A4 24 04            [12] 2337 	add	a,#0x04
      0009A6 F5 81            [12] 2338 	mov	sp,a
                                   2339 ;	source/CH549_SubwayBoard.c:214: for ( i = 0; i < FREQs; i++)
      0009A8 7E 00            [12] 2340 	mov	r6,#0x00
      0009AA                       2341 00116$:
      0009AA A8 08            [24] 2342 	mov	r0,_bp
      0009AC 08               [12] 2343 	inc	r0
      0009AD C3               [12] 2344 	clr	c
      0009AE EE               [12] 2345 	mov	a,r6
      0009AF 96               [12] 2346 	subb	a,@r0
      0009B0 40 03            [24] 2347 	jc	00163$
      0009B2 02 0B 3A         [24] 2348 	ljmp	00118$
      0009B5                       2349 00163$:
                                   2350 ;	source/CH549_SubwayBoard.c:216: if (rand() % 2)
      0009B5 C0 06            [24] 2351 	push	ar6
      0009B7 12 0B 40         [24] 2352 	lcall	_rand
      0009BA AC 82            [24] 2353 	mov	r4,dpl
      0009BC AD 83            [24] 2354 	mov	r5,dph
      0009BE 74 02            [12] 2355 	mov	a,#0x02
      0009C0 C0 E0            [24] 2356 	push	acc
      0009C2 E4               [12] 2357 	clr	a
      0009C3 C0 E0            [24] 2358 	push	acc
      0009C5 8C 82            [24] 2359 	mov	dpl,r4
      0009C7 8D 83            [24] 2360 	mov	dph,r5
      0009C9 12 0E AF         [24] 2361 	lcall	__modsint
      0009CC AC 82            [24] 2362 	mov	r4,dpl
      0009CE AD 83            [24] 2363 	mov	r5,dph
      0009D0 15 81            [12] 2364 	dec	sp
      0009D2 15 81            [12] 2365 	dec	sp
      0009D4 D0 06            [24] 2366 	pop	ar6
      0009D6 EC               [12] 2367 	mov	a,r4
      0009D7 4D               [12] 2368 	orl	a,r5
      0009D8 70 03            [24] 2369 	jnz	00164$
      0009DA 02 0A 83         [24] 2370 	ljmp	00108$
      0009DD                       2371 00164$:
                                   2372 ;	source/CH549_SubwayBoard.c:218: m = rand() % 6;
      0009DD C0 06            [24] 2373 	push	ar6
      0009DF 12 0B 40         [24] 2374 	lcall	_rand
      0009E2 AC 82            [24] 2375 	mov	r4,dpl
      0009E4 AD 83            [24] 2376 	mov	r5,dph
      0009E6 74 06            [12] 2377 	mov	a,#0x06
      0009E8 C0 E0            [24] 2378 	push	acc
      0009EA E4               [12] 2379 	clr	a
      0009EB C0 E0            [24] 2380 	push	acc
      0009ED 8C 82            [24] 2381 	mov	dpl,r4
      0009EF 8D 83            [24] 2382 	mov	dph,r5
      0009F1 12 0E AF         [24] 2383 	lcall	__modsint
      0009F4 AC 82            [24] 2384 	mov	r4,dpl
      0009F6 15 81            [12] 2385 	dec	sp
      0009F8 15 81            [12] 2386 	dec	sp
      0009FA D0 06            [24] 2387 	pop	ar6
                                   2388 ;	source/CH549_SubwayBoard.c:219: for ( DOT = 0; DOT < 7; DOT++)
      0009FC EC               [12] 2389 	mov	a,r4
      0009FD 75 F0 07         [24] 2390 	mov	b,#0x07
      000A00 A4               [48] 2391 	mul	ab
      000A01 FC               [12] 2392 	mov	r4,a
      000A02 AD F0            [24] 2393 	mov	r5,b
      000A04 A8 08            [24] 2394 	mov	r0,_bp
      000A06 08               [12] 2395 	inc	r0
      000A07 08               [12] 2396 	inc	r0
      000A08 EC               [12] 2397 	mov	a,r4
      000A09 24 01            [12] 2398 	add	a,#_SubwayLine
      000A0B F6               [12] 2399 	mov	@r0,a
      000A0C ED               [12] 2400 	mov	a,r5
      000A0D 34 00            [12] 2401 	addc	a,#(_SubwayLine >> 8)
      000A0F 08               [12] 2402 	inc	r0
      000A10 F6               [12] 2403 	mov	@r0,a
      000A11 7A 00            [12] 2404 	mov	r2,#0x00
      000A13                       2405 00111$:
                                   2406 ;	source/CH549_SubwayBoard.c:221: if (SubwayLine[m][DOT])
      000A13 C0 06            [24] 2407 	push	ar6
      000A15 A8 08            [24] 2408 	mov	r0,_bp
      000A17 08               [12] 2409 	inc	r0
      000A18 08               [12] 2410 	inc	r0
      000A19 EA               [12] 2411 	mov	a,r2
      000A1A 26               [12] 2412 	add	a,@r0
      000A1B FE               [12] 2413 	mov	r6,a
      000A1C E4               [12] 2414 	clr	a
      000A1D 08               [12] 2415 	inc	r0
      000A1E 36               [12] 2416 	addc	a,@r0
      000A1F FF               [12] 2417 	mov	r7,a
      000A20 8E 82            [24] 2418 	mov	dpl,r6
      000A22 8F 83            [24] 2419 	mov	dph,r7
      000A24 E0               [24] 2420 	movx	a,@dptr
      000A25 D0 06            [24] 2421 	pop	ar6
      000A27 60 51            [24] 2422 	jz	00112$
                                   2423 ;	source/CH549_SubwayBoard.c:223: LIGHT_ON(SubwayLine[m][DOT]);
      000A29 C0 06            [24] 2424 	push	ar6
      000A2B EC               [12] 2425 	mov	a,r4
      000A2C 24 01            [12] 2426 	add	a,#_SubwayLine
      000A2E FE               [12] 2427 	mov	r6,a
      000A2F ED               [12] 2428 	mov	a,r5
      000A30 34 00            [12] 2429 	addc	a,#(_SubwayLine >> 8)
      000A32 FF               [12] 2430 	mov	r7,a
      000A33 EA               [12] 2431 	mov	a,r2
      000A34 2E               [12] 2432 	add	a,r6
      000A35 FE               [12] 2433 	mov	r6,a
      000A36 E4               [12] 2434 	clr	a
      000A37 3F               [12] 2435 	addc	a,r7
      000A38 FF               [12] 2436 	mov	r7,a
      000A39 8E 82            [24] 2437 	mov	dpl,r6
      000A3B 8F 83            [24] 2438 	mov	dph,r7
      000A3D E0               [24] 2439 	movx	a,@dptr
      000A3E F5 82            [12] 2440 	mov	dpl,a
      000A40 C0 07            [24] 2441 	push	ar7
      000A42 C0 06            [24] 2442 	push	ar6
      000A44 C0 05            [24] 2443 	push	ar5
      000A46 C0 04            [24] 2444 	push	ar4
      000A48 C0 02            [24] 2445 	push	ar2
      000A4A 12 03 6C         [24] 2446 	lcall	_LIGHT_ON
                                   2447 ;	source/CH549_SubwayBoard.c:224: mDelaymS(50);
      000A4D 90 00 32         [24] 2448 	mov	dptr,#0x0032
      000A50 12 02 FA         [24] 2449 	lcall	_mDelaymS
      000A53 D0 02            [24] 2450 	pop	ar2
      000A55 D0 04            [24] 2451 	pop	ar4
      000A57 D0 05            [24] 2452 	pop	ar5
      000A59 D0 06            [24] 2453 	pop	ar6
      000A5B D0 07            [24] 2454 	pop	ar7
                                   2455 ;	source/CH549_SubwayBoard.c:225: LIGHT_OFF(SubwayLine[m][DOT]);
      000A5D 8E 82            [24] 2456 	mov	dpl,r6
      000A5F 8F 83            [24] 2457 	mov	dph,r7
      000A61 E0               [24] 2458 	movx	a,@dptr
      000A62 FE               [12] 2459 	mov	r6,a
      000A63 F5 82            [12] 2460 	mov	dpl,a
      000A65 C0 06            [24] 2461 	push	ar6
      000A67 C0 05            [24] 2462 	push	ar5
      000A69 C0 04            [24] 2463 	push	ar4
      000A6B C0 02            [24] 2464 	push	ar2
      000A6D 12 04 10         [24] 2465 	lcall	_LIGHT_OFF
      000A70 D0 02            [24] 2466 	pop	ar2
      000A72 D0 04            [24] 2467 	pop	ar4
      000A74 D0 05            [24] 2468 	pop	ar5
      000A76 D0 06            [24] 2469 	pop	ar6
                                   2470 ;	source/CH549_SubwayBoard.c:214: for ( i = 0; i < FREQs; i++)
      000A78 D0 06            [24] 2471 	pop	ar6
                                   2472 ;	source/CH549_SubwayBoard.c:225: LIGHT_OFF(SubwayLine[m][DOT]);
      000A7A                       2473 00112$:
                                   2474 ;	source/CH549_SubwayBoard.c:219: for ( DOT = 0; DOT < 7; DOT++)
      000A7A 0A               [12] 2475 	inc	r2
      000A7B BA 07 00         [24] 2476 	cjne	r2,#0x07,00166$
      000A7E                       2477 00166$:
      000A7E 40 93            [24] 2478 	jc	00111$
      000A80 02 0B 2C         [24] 2479 	ljmp	00109$
      000A83                       2480 00108$:
                                   2481 ;	source/CH549_SubwayBoard.c:232: m = rand() % 6;
      000A83 C0 06            [24] 2482 	push	ar6
      000A85 12 0B 40         [24] 2483 	lcall	_rand
      000A88 AD 82            [24] 2484 	mov	r5,dpl
      000A8A AF 83            [24] 2485 	mov	r7,dph
      000A8C 74 06            [12] 2486 	mov	a,#0x06
      000A8E C0 E0            [24] 2487 	push	acc
      000A90 E4               [12] 2488 	clr	a
      000A91 C0 E0            [24] 2489 	push	acc
      000A93 8D 82            [24] 2490 	mov	dpl,r5
      000A95 8F 83            [24] 2491 	mov	dph,r7
      000A97 12 0E AF         [24] 2492 	lcall	__modsint
      000A9A AD 82            [24] 2493 	mov	r5,dpl
      000A9C 15 81            [12] 2494 	dec	sp
      000A9E 15 81            [12] 2495 	dec	sp
      000AA0 D0 06            [24] 2496 	pop	ar6
                                   2497 ;	source/CH549_SubwayBoard.c:233: for ( DOT = 0; DOT < 7; DOT++)
      000AA2 ED               [12] 2498 	mov	a,r5
      000AA3 75 F0 07         [24] 2499 	mov	b,#0x07
      000AA6 A4               [48] 2500 	mul	ab
      000AA7 FD               [12] 2501 	mov	r5,a
      000AA8 AF F0            [24] 2502 	mov	r7,b
      000AAA E5 08            [12] 2503 	mov	a,_bp
      000AAC 24 04            [12] 2504 	add	a,#0x04
      000AAE F8               [12] 2505 	mov	r0,a
      000AAF ED               [12] 2506 	mov	a,r5
      000AB0 24 01            [12] 2507 	add	a,#_SubwayLine
      000AB2 F6               [12] 2508 	mov	@r0,a
      000AB3 EF               [12] 2509 	mov	a,r7
      000AB4 34 00            [12] 2510 	addc	a,#(_SubwayLine >> 8)
      000AB6 08               [12] 2511 	inc	r0
      000AB7 F6               [12] 2512 	mov	@r0,a
      000AB8 7B 00            [12] 2513 	mov	r3,#0x00
      000ABA                       2514 00113$:
                                   2515 ;	source/CH549_SubwayBoard.c:235: if (SubwayLine[m][6 - DOT])
      000ABA C0 06            [24] 2516 	push	ar6
      000ABC 8B 04            [24] 2517 	mov	ar4,r3
      000ABE 74 06            [12] 2518 	mov	a,#0x06
      000AC0 C3               [12] 2519 	clr	c
      000AC1 9C               [12] 2520 	subb	a,r4
      000AC2 C8               [12] 2521 	xch	a,r0
      000AC3 E5 08            [12] 2522 	mov	a,_bp
      000AC5 24 04            [12] 2523 	add	a,#0x04
      000AC7 C8               [12] 2524 	xch	a,r0
      000AC8 26               [12] 2525 	add	a,@r0
      000AC9 FA               [12] 2526 	mov	r2,a
      000ACA 08               [12] 2527 	inc	r0
      000ACB E4               [12] 2528 	clr	a
      000ACC 36               [12] 2529 	addc	a,@r0
      000ACD FE               [12] 2530 	mov	r6,a
      000ACE 8A 82            [24] 2531 	mov	dpl,r2
      000AD0 8E 83            [24] 2532 	mov	dph,r6
      000AD2 E0               [24] 2533 	movx	a,@dptr
      000AD3 D0 06            [24] 2534 	pop	ar6
      000AD5 60 4F            [24] 2535 	jz	00114$
                                   2536 ;	source/CH549_SubwayBoard.c:237: LIGHT_ON(SubwayLine[m][6 - DOT]);
      000AD7 C0 06            [24] 2537 	push	ar6
      000AD9 ED               [12] 2538 	mov	a,r5
      000ADA 24 01            [12] 2539 	add	a,#_SubwayLine
      000ADC FA               [12] 2540 	mov	r2,a
      000ADD EF               [12] 2541 	mov	a,r7
      000ADE 34 00            [12] 2542 	addc	a,#(_SubwayLine >> 8)
      000AE0 FE               [12] 2543 	mov	r6,a
      000AE1 74 06            [12] 2544 	mov	a,#0x06
      000AE3 C3               [12] 2545 	clr	c
      000AE4 9C               [12] 2546 	subb	a,r4
      000AE5 2A               [12] 2547 	add	a,r2
      000AE6 FA               [12] 2548 	mov	r2,a
      000AE7 E4               [12] 2549 	clr	a
      000AE8 3E               [12] 2550 	addc	a,r6
      000AE9 FE               [12] 2551 	mov	r6,a
      000AEA 8A 82            [24] 2552 	mov	dpl,r2
      000AEC 8E 83            [24] 2553 	mov	dph,r6
      000AEE E0               [24] 2554 	movx	a,@dptr
      000AEF F5 82            [12] 2555 	mov	dpl,a
      000AF1 C0 07            [24] 2556 	push	ar7
      000AF3 C0 06            [24] 2557 	push	ar6
      000AF5 C0 05            [24] 2558 	push	ar5
      000AF7 C0 03            [24] 2559 	push	ar3
      000AF9 C0 02            [24] 2560 	push	ar2
      000AFB 12 03 6C         [24] 2561 	lcall	_LIGHT_ON
                                   2562 ;	source/CH549_SubwayBoard.c:238: mDelaymS(50);
      000AFE 90 00 32         [24] 2563 	mov	dptr,#0x0032
      000B01 12 02 FA         [24] 2564 	lcall	_mDelaymS
      000B04 D0 02            [24] 2565 	pop	ar2
      000B06 D0 03            [24] 2566 	pop	ar3
      000B08 D0 05            [24] 2567 	pop	ar5
      000B0A D0 06            [24] 2568 	pop	ar6
                                   2569 ;	source/CH549_SubwayBoard.c:239: LIGHT_OFF(SubwayLine[m][6 - DOT]);
      000B0C 8A 82            [24] 2570 	mov	dpl,r2
      000B0E 8E 83            [24] 2571 	mov	dph,r6
      000B10 E0               [24] 2572 	movx	a,@dptr
      000B11 F5 82            [12] 2573 	mov	dpl,a
      000B13 C0 06            [24] 2574 	push	ar6
      000B15 C0 05            [24] 2575 	push	ar5
      000B17 C0 03            [24] 2576 	push	ar3
      000B19 12 04 10         [24] 2577 	lcall	_LIGHT_OFF
      000B1C D0 03            [24] 2578 	pop	ar3
      000B1E D0 05            [24] 2579 	pop	ar5
      000B20 D0 06            [24] 2580 	pop	ar6
      000B22 D0 07            [24] 2581 	pop	ar7
                                   2582 ;	source/CH549_SubwayBoard.c:214: for ( i = 0; i < FREQs; i++)
      000B24 D0 06            [24] 2583 	pop	ar6
                                   2584 ;	source/CH549_SubwayBoard.c:239: LIGHT_OFF(SubwayLine[m][6 - DOT]);
      000B26                       2585 00114$:
                                   2586 ;	source/CH549_SubwayBoard.c:233: for ( DOT = 0; DOT < 7; DOT++)
      000B26 0B               [12] 2587 	inc	r3
      000B27 BB 07 00         [24] 2588 	cjne	r3,#0x07,00169$
      000B2A                       2589 00169$:
      000B2A 40 8E            [24] 2590 	jc	00113$
      000B2C                       2591 00109$:
                                   2592 ;	source/CH549_SubwayBoard.c:246: mDelaymS(150);
      000B2C 90 00 96         [24] 2593 	mov	dptr,#0x0096
      000B2F C0 06            [24] 2594 	push	ar6
      000B31 12 02 FA         [24] 2595 	lcall	_mDelaymS
      000B34 D0 06            [24] 2596 	pop	ar6
                                   2597 ;	source/CH549_SubwayBoard.c:214: for ( i = 0; i < FREQs; i++)
      000B36 0E               [12] 2598 	inc	r6
      000B37 02 09 AA         [24] 2599 	ljmp	00116$
      000B3A                       2600 00118$:
                                   2601 ;	source/CH549_SubwayBoard.c:250: }
      000B3A 85 08 81         [24] 2602 	mov	sp,_bp
      000B3D D0 08            [24] 2603 	pop	_bp
      000B3F 22               [24] 2604 	ret
                                   2605 	.area CSEG    (CODE)
                                   2606 	.area CONST   (CODE)
                                   2607 	.area CABS    (ABS,CODE)
