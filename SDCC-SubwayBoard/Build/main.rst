                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 4.0.0 #11528 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module main
                                      6 	.optsdcc -mmcs51 --model-large
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _main
                                     12 	.globl _LIGHT_Random
                                     13 	.globl _LIGHT_Disperse
                                     14 	.globl _LIGHT_Converge
                                     15 	.globl _LIGHT_FLineOn
                                     16 	.globl _LIGHT_TLineOn
                                     17 	.globl _LIGHT_AllOn
                                     18 	.globl _LIGHT_Clean
                                     19 	.globl _mDelaymS
                                     20 	.globl _UIF_BUS_RST
                                     21 	.globl _UIF_DETECT
                                     22 	.globl _UIF_TRANSFER
                                     23 	.globl _UIF_SUSPEND
                                     24 	.globl _UIF_HST_SOF
                                     25 	.globl _UIF_FIFO_OV
                                     26 	.globl _U_SIE_FREE
                                     27 	.globl _U_TOG_OK
                                     28 	.globl _U_IS_NAK
                                     29 	.globl _S0_R_FIFO
                                     30 	.globl _S0_T_FIFO
                                     31 	.globl _S0_FREE
                                     32 	.globl _S0_IF_BYTE
                                     33 	.globl _S0_IF_FIRST
                                     34 	.globl _S0_IF_OV
                                     35 	.globl _S0_FST_ACT
                                     36 	.globl _CP_RL2
                                     37 	.globl _C_T2
                                     38 	.globl _TR2
                                     39 	.globl _EXEN2
                                     40 	.globl _TCLK
                                     41 	.globl _RCLK
                                     42 	.globl _EXF2
                                     43 	.globl _CAP1F
                                     44 	.globl _TF2
                                     45 	.globl _RI
                                     46 	.globl _TI
                                     47 	.globl _RB8
                                     48 	.globl _TB8
                                     49 	.globl _REN
                                     50 	.globl _SM2
                                     51 	.globl _SM1
                                     52 	.globl _SM0
                                     53 	.globl _IT0
                                     54 	.globl _IE0
                                     55 	.globl _IT1
                                     56 	.globl _IE1
                                     57 	.globl _TR0
                                     58 	.globl _TF0
                                     59 	.globl _TR1
                                     60 	.globl _TF1
                                     61 	.globl _XI
                                     62 	.globl _XO
                                     63 	.globl _P4_0
                                     64 	.globl _P4_1
                                     65 	.globl _P4_2
                                     66 	.globl _P4_3
                                     67 	.globl _P4_4
                                     68 	.globl _P4_5
                                     69 	.globl _P4_6
                                     70 	.globl _RXD
                                     71 	.globl _TXD
                                     72 	.globl _INT0
                                     73 	.globl _INT1
                                     74 	.globl _T0
                                     75 	.globl _T1
                                     76 	.globl _CAP0
                                     77 	.globl _INT3
                                     78 	.globl _P3_0
                                     79 	.globl _P3_1
                                     80 	.globl _P3_2
                                     81 	.globl _P3_3
                                     82 	.globl _P3_4
                                     83 	.globl _P3_5
                                     84 	.globl _P3_6
                                     85 	.globl _P3_7
                                     86 	.globl _PWM5
                                     87 	.globl _PWM4
                                     88 	.globl _INT0_
                                     89 	.globl _PWM3
                                     90 	.globl _PWM2
                                     91 	.globl _CAP1_
                                     92 	.globl _T2_
                                     93 	.globl _PWM1
                                     94 	.globl _CAP2_
                                     95 	.globl _T2EX_
                                     96 	.globl _PWM0
                                     97 	.globl _RXD1
                                     98 	.globl _PWM6
                                     99 	.globl _TXD1
                                    100 	.globl _PWM7
                                    101 	.globl _P2_0
                                    102 	.globl _P2_1
                                    103 	.globl _P2_2
                                    104 	.globl _P2_3
                                    105 	.globl _P2_4
                                    106 	.globl _P2_5
                                    107 	.globl _P2_6
                                    108 	.globl _P2_7
                                    109 	.globl _AIN0
                                    110 	.globl _CAP1
                                    111 	.globl _T2
                                    112 	.globl _AIN1
                                    113 	.globl _CAP2
                                    114 	.globl _T2EX
                                    115 	.globl _AIN2
                                    116 	.globl _AIN3
                                    117 	.globl _AIN4
                                    118 	.globl _UCC1
                                    119 	.globl _SCS
                                    120 	.globl _AIN5
                                    121 	.globl _UCC2
                                    122 	.globl _PWM0_
                                    123 	.globl _MOSI
                                    124 	.globl _AIN6
                                    125 	.globl _VBUS
                                    126 	.globl _RXD1_
                                    127 	.globl _MISO
                                    128 	.globl _AIN7
                                    129 	.globl _TXD1_
                                    130 	.globl _SCK
                                    131 	.globl _P1_0
                                    132 	.globl _P1_1
                                    133 	.globl _P1_2
                                    134 	.globl _P1_3
                                    135 	.globl _P1_4
                                    136 	.globl _P1_5
                                    137 	.globl _P1_6
                                    138 	.globl _P1_7
                                    139 	.globl _AIN8
                                    140 	.globl _AIN9
                                    141 	.globl _AIN10
                                    142 	.globl _RXD_
                                    143 	.globl _AIN11
                                    144 	.globl _TXD_
                                    145 	.globl _AIN12
                                    146 	.globl _RXD2
                                    147 	.globl _AIN13
                                    148 	.globl _TXD2
                                    149 	.globl _AIN14
                                    150 	.globl _RXD3
                                    151 	.globl _AIN15
                                    152 	.globl _TXD3
                                    153 	.globl _P0_0
                                    154 	.globl _P0_1
                                    155 	.globl _P0_2
                                    156 	.globl _P0_3
                                    157 	.globl _P0_4
                                    158 	.globl _P0_5
                                    159 	.globl _P0_6
                                    160 	.globl _P0_7
                                    161 	.globl _IE_SPI0
                                    162 	.globl _IE_INT3
                                    163 	.globl _IE_USB
                                    164 	.globl _IE_UART2
                                    165 	.globl _IE_ADC
                                    166 	.globl _IE_UART1
                                    167 	.globl _IE_UART3
                                    168 	.globl _IE_PWMX
                                    169 	.globl _IE_GPIO
                                    170 	.globl _IE_WDOG
                                    171 	.globl _PX0
                                    172 	.globl _PT0
                                    173 	.globl _PX1
                                    174 	.globl _PT1
                                    175 	.globl _PS
                                    176 	.globl _PT2
                                    177 	.globl _PL_FLAG
                                    178 	.globl _PH_FLAG
                                    179 	.globl _EX0
                                    180 	.globl _ET0
                                    181 	.globl _EX1
                                    182 	.globl _ET1
                                    183 	.globl _ES
                                    184 	.globl _ET2
                                    185 	.globl _E_DIS
                                    186 	.globl _EA
                                    187 	.globl _P
                                    188 	.globl _F1
                                    189 	.globl _OV
                                    190 	.globl _RS0
                                    191 	.globl _RS1
                                    192 	.globl _F0
                                    193 	.globl _AC
                                    194 	.globl _CY
                                    195 	.globl _UEP1_DMA_H
                                    196 	.globl _UEP1_DMA_L
                                    197 	.globl _UEP1_DMA
                                    198 	.globl _UEP0_DMA_H
                                    199 	.globl _UEP0_DMA_L
                                    200 	.globl _UEP0_DMA
                                    201 	.globl _UEP2_3_MOD
                                    202 	.globl _UEP4_1_MOD
                                    203 	.globl _UEP3_DMA_H
                                    204 	.globl _UEP3_DMA_L
                                    205 	.globl _UEP3_DMA
                                    206 	.globl _UEP2_DMA_H
                                    207 	.globl _UEP2_DMA_L
                                    208 	.globl _UEP2_DMA
                                    209 	.globl _USB_DEV_AD
                                    210 	.globl _USB_CTRL
                                    211 	.globl _USB_INT_EN
                                    212 	.globl _UEP4_T_LEN
                                    213 	.globl _UEP4_CTRL
                                    214 	.globl _UEP0_T_LEN
                                    215 	.globl _UEP0_CTRL
                                    216 	.globl _USB_RX_LEN
                                    217 	.globl _USB_MIS_ST
                                    218 	.globl _USB_INT_ST
                                    219 	.globl _USB_INT_FG
                                    220 	.globl _UEP3_T_LEN
                                    221 	.globl _UEP3_CTRL
                                    222 	.globl _UEP2_T_LEN
                                    223 	.globl _UEP2_CTRL
                                    224 	.globl _UEP1_T_LEN
                                    225 	.globl _UEP1_CTRL
                                    226 	.globl _UDEV_CTRL
                                    227 	.globl _USB_C_CTRL
                                    228 	.globl _ADC_PIN
                                    229 	.globl _ADC_CHAN
                                    230 	.globl _ADC_DAT_H
                                    231 	.globl _ADC_DAT_L
                                    232 	.globl _ADC_DAT
                                    233 	.globl _ADC_CFG
                                    234 	.globl _ADC_CTRL
                                    235 	.globl _TKEY_CTRL
                                    236 	.globl _SIF3
                                    237 	.globl _SBAUD3
                                    238 	.globl _SBUF3
                                    239 	.globl _SCON3
                                    240 	.globl _SIF2
                                    241 	.globl _SBAUD2
                                    242 	.globl _SBUF2
                                    243 	.globl _SCON2
                                    244 	.globl _SIF1
                                    245 	.globl _SBAUD1
                                    246 	.globl _SBUF1
                                    247 	.globl _SCON1
                                    248 	.globl _SPI0_SETUP
                                    249 	.globl _SPI0_CK_SE
                                    250 	.globl _SPI0_CTRL
                                    251 	.globl _SPI0_DATA
                                    252 	.globl _SPI0_STAT
                                    253 	.globl _PWM_DATA7
                                    254 	.globl _PWM_DATA6
                                    255 	.globl _PWM_DATA5
                                    256 	.globl _PWM_DATA4
                                    257 	.globl _PWM_DATA3
                                    258 	.globl _PWM_CTRL2
                                    259 	.globl _PWM_CK_SE
                                    260 	.globl _PWM_CTRL
                                    261 	.globl _PWM_DATA0
                                    262 	.globl _PWM_DATA1
                                    263 	.globl _PWM_DATA2
                                    264 	.globl _T2CAP1H
                                    265 	.globl _T2CAP1L
                                    266 	.globl _T2CAP1
                                    267 	.globl _TH2
                                    268 	.globl _TL2
                                    269 	.globl _T2COUNT
                                    270 	.globl _RCAP2H
                                    271 	.globl _RCAP2L
                                    272 	.globl _RCAP2
                                    273 	.globl _T2MOD
                                    274 	.globl _T2CON
                                    275 	.globl _T2CAP0H
                                    276 	.globl _T2CAP0L
                                    277 	.globl _T2CAP0
                                    278 	.globl _T2CON2
                                    279 	.globl _SBUF
                                    280 	.globl _SCON
                                    281 	.globl _TH1
                                    282 	.globl _TH0
                                    283 	.globl _TL1
                                    284 	.globl _TL0
                                    285 	.globl _TMOD
                                    286 	.globl _TCON
                                    287 	.globl _XBUS_AUX
                                    288 	.globl _PIN_FUNC
                                    289 	.globl _P5
                                    290 	.globl _P4_DIR_PU
                                    291 	.globl _P4_MOD_OC
                                    292 	.globl _P4
                                    293 	.globl _P3_DIR_PU
                                    294 	.globl _P3_MOD_OC
                                    295 	.globl _P3
                                    296 	.globl _P2_DIR_PU
                                    297 	.globl _P2_MOD_OC
                                    298 	.globl _P2
                                    299 	.globl _P1_DIR_PU
                                    300 	.globl _P1_MOD_OC
                                    301 	.globl _P1
                                    302 	.globl _P0_DIR_PU
                                    303 	.globl _P0_MOD_OC
                                    304 	.globl _P0
                                    305 	.globl _ROM_CTRL
                                    306 	.globl _ROM_DATA_HH
                                    307 	.globl _ROM_DATA_HL
                                    308 	.globl _ROM_DATA_HI
                                    309 	.globl _ROM_ADDR_H
                                    310 	.globl _ROM_ADDR_L
                                    311 	.globl _ROM_ADDR
                                    312 	.globl _GPIO_IE
                                    313 	.globl _INTX
                                    314 	.globl _IP_EX
                                    315 	.globl _IE_EX
                                    316 	.globl _IP
                                    317 	.globl _IE
                                    318 	.globl _WDOG_COUNT
                                    319 	.globl _RESET_KEEP
                                    320 	.globl _WAKE_CTRL
                                    321 	.globl _CLOCK_CFG
                                    322 	.globl _POWER_CFG
                                    323 	.globl _PCON
                                    324 	.globl _GLOBAL_CFG
                                    325 	.globl _SAFE_MOD
                                    326 	.globl _DPH
                                    327 	.globl _DPL
                                    328 	.globl _SP
                                    329 	.globl _A_INV
                                    330 	.globl _B
                                    331 	.globl _ACC
                                    332 	.globl _PSW
                                    333 ;--------------------------------------------------------
                                    334 ; special function registers
                                    335 ;--------------------------------------------------------
                                    336 	.area RSEG    (ABS,DATA)
      000000                        337 	.org 0x0000
                           0000D0   338 _PSW	=	0x00d0
                           0000E0   339 _ACC	=	0x00e0
                           0000F0   340 _B	=	0x00f0
                           0000FD   341 _A_INV	=	0x00fd
                           000081   342 _SP	=	0x0081
                           000082   343 _DPL	=	0x0082
                           000083   344 _DPH	=	0x0083
                           0000A1   345 _SAFE_MOD	=	0x00a1
                           0000B1   346 _GLOBAL_CFG	=	0x00b1
                           000087   347 _PCON	=	0x0087
                           0000BA   348 _POWER_CFG	=	0x00ba
                           0000B9   349 _CLOCK_CFG	=	0x00b9
                           0000A9   350 _WAKE_CTRL	=	0x00a9
                           0000FE   351 _RESET_KEEP	=	0x00fe
                           0000FF   352 _WDOG_COUNT	=	0x00ff
                           0000A8   353 _IE	=	0x00a8
                           0000B8   354 _IP	=	0x00b8
                           0000E8   355 _IE_EX	=	0x00e8
                           0000E9   356 _IP_EX	=	0x00e9
                           0000B3   357 _INTX	=	0x00b3
                           0000B2   358 _GPIO_IE	=	0x00b2
                           008584   359 _ROM_ADDR	=	0x8584
                           000084   360 _ROM_ADDR_L	=	0x0084
                           000085   361 _ROM_ADDR_H	=	0x0085
                           008F8E   362 _ROM_DATA_HI	=	0x8f8e
                           00008E   363 _ROM_DATA_HL	=	0x008e
                           00008F   364 _ROM_DATA_HH	=	0x008f
                           000086   365 _ROM_CTRL	=	0x0086
                           000080   366 _P0	=	0x0080
                           0000C4   367 _P0_MOD_OC	=	0x00c4
                           0000C5   368 _P0_DIR_PU	=	0x00c5
                           000090   369 _P1	=	0x0090
                           000092   370 _P1_MOD_OC	=	0x0092
                           000093   371 _P1_DIR_PU	=	0x0093
                           0000A0   372 _P2	=	0x00a0
                           000094   373 _P2_MOD_OC	=	0x0094
                           000095   374 _P2_DIR_PU	=	0x0095
                           0000B0   375 _P3	=	0x00b0
                           000096   376 _P3_MOD_OC	=	0x0096
                           000097   377 _P3_DIR_PU	=	0x0097
                           0000C0   378 _P4	=	0x00c0
                           0000C2   379 _P4_MOD_OC	=	0x00c2
                           0000C3   380 _P4_DIR_PU	=	0x00c3
                           0000AB   381 _P5	=	0x00ab
                           0000AA   382 _PIN_FUNC	=	0x00aa
                           0000A2   383 _XBUS_AUX	=	0x00a2
                           000088   384 _TCON	=	0x0088
                           000089   385 _TMOD	=	0x0089
                           00008A   386 _TL0	=	0x008a
                           00008B   387 _TL1	=	0x008b
                           00008C   388 _TH0	=	0x008c
                           00008D   389 _TH1	=	0x008d
                           000098   390 _SCON	=	0x0098
                           000099   391 _SBUF	=	0x0099
                           0000C1   392 _T2CON2	=	0x00c1
                           00C7C6   393 _T2CAP0	=	0xc7c6
                           0000C6   394 _T2CAP0L	=	0x00c6
                           0000C7   395 _T2CAP0H	=	0x00c7
                           0000C8   396 _T2CON	=	0x00c8
                           0000C9   397 _T2MOD	=	0x00c9
                           00CBCA   398 _RCAP2	=	0xcbca
                           0000CA   399 _RCAP2L	=	0x00ca
                           0000CB   400 _RCAP2H	=	0x00cb
                           00CDCC   401 _T2COUNT	=	0xcdcc
                           0000CC   402 _TL2	=	0x00cc
                           0000CD   403 _TH2	=	0x00cd
                           00CFCE   404 _T2CAP1	=	0xcfce
                           0000CE   405 _T2CAP1L	=	0x00ce
                           0000CF   406 _T2CAP1H	=	0x00cf
                           00009A   407 _PWM_DATA2	=	0x009a
                           00009B   408 _PWM_DATA1	=	0x009b
                           00009C   409 _PWM_DATA0	=	0x009c
                           00009D   410 _PWM_CTRL	=	0x009d
                           00009E   411 _PWM_CK_SE	=	0x009e
                           00009F   412 _PWM_CTRL2	=	0x009f
                           0000A3   413 _PWM_DATA3	=	0x00a3
                           0000A4   414 _PWM_DATA4	=	0x00a4
                           0000A5   415 _PWM_DATA5	=	0x00a5
                           0000A6   416 _PWM_DATA6	=	0x00a6
                           0000A7   417 _PWM_DATA7	=	0x00a7
                           0000F8   418 _SPI0_STAT	=	0x00f8
                           0000F9   419 _SPI0_DATA	=	0x00f9
                           0000FA   420 _SPI0_CTRL	=	0x00fa
                           0000FB   421 _SPI0_CK_SE	=	0x00fb
                           0000FC   422 _SPI0_SETUP	=	0x00fc
                           0000BC   423 _SCON1	=	0x00bc
                           0000BD   424 _SBUF1	=	0x00bd
                           0000BE   425 _SBAUD1	=	0x00be
                           0000BF   426 _SIF1	=	0x00bf
                           0000B4   427 _SCON2	=	0x00b4
                           0000B5   428 _SBUF2	=	0x00b5
                           0000B6   429 _SBAUD2	=	0x00b6
                           0000B7   430 _SIF2	=	0x00b7
                           0000AC   431 _SCON3	=	0x00ac
                           0000AD   432 _SBUF3	=	0x00ad
                           0000AE   433 _SBAUD3	=	0x00ae
                           0000AF   434 _SIF3	=	0x00af
                           0000F1   435 _TKEY_CTRL	=	0x00f1
                           0000F2   436 _ADC_CTRL	=	0x00f2
                           0000F3   437 _ADC_CFG	=	0x00f3
                           00F5F4   438 _ADC_DAT	=	0xf5f4
                           0000F4   439 _ADC_DAT_L	=	0x00f4
                           0000F5   440 _ADC_DAT_H	=	0x00f5
                           0000F6   441 _ADC_CHAN	=	0x00f6
                           0000F7   442 _ADC_PIN	=	0x00f7
                           000091   443 _USB_C_CTRL	=	0x0091
                           0000D1   444 _UDEV_CTRL	=	0x00d1
                           0000D2   445 _UEP1_CTRL	=	0x00d2
                           0000D3   446 _UEP1_T_LEN	=	0x00d3
                           0000D4   447 _UEP2_CTRL	=	0x00d4
                           0000D5   448 _UEP2_T_LEN	=	0x00d5
                           0000D6   449 _UEP3_CTRL	=	0x00d6
                           0000D7   450 _UEP3_T_LEN	=	0x00d7
                           0000D8   451 _USB_INT_FG	=	0x00d8
                           0000D9   452 _USB_INT_ST	=	0x00d9
                           0000DA   453 _USB_MIS_ST	=	0x00da
                           0000DB   454 _USB_RX_LEN	=	0x00db
                           0000DC   455 _UEP0_CTRL	=	0x00dc
                           0000DD   456 _UEP0_T_LEN	=	0x00dd
                           0000DE   457 _UEP4_CTRL	=	0x00de
                           0000DF   458 _UEP4_T_LEN	=	0x00df
                           0000E1   459 _USB_INT_EN	=	0x00e1
                           0000E2   460 _USB_CTRL	=	0x00e2
                           0000E3   461 _USB_DEV_AD	=	0x00e3
                           00E5E4   462 _UEP2_DMA	=	0xe5e4
                           0000E4   463 _UEP2_DMA_L	=	0x00e4
                           0000E5   464 _UEP2_DMA_H	=	0x00e5
                           00E7E6   465 _UEP3_DMA	=	0xe7e6
                           0000E6   466 _UEP3_DMA_L	=	0x00e6
                           0000E7   467 _UEP3_DMA_H	=	0x00e7
                           0000EA   468 _UEP4_1_MOD	=	0x00ea
                           0000EB   469 _UEP2_3_MOD	=	0x00eb
                           00EDEC   470 _UEP0_DMA	=	0xedec
                           0000EC   471 _UEP0_DMA_L	=	0x00ec
                           0000ED   472 _UEP0_DMA_H	=	0x00ed
                           00EFEE   473 _UEP1_DMA	=	0xefee
                           0000EE   474 _UEP1_DMA_L	=	0x00ee
                           0000EF   475 _UEP1_DMA_H	=	0x00ef
                                    476 ;--------------------------------------------------------
                                    477 ; special function bits
                                    478 ;--------------------------------------------------------
                                    479 	.area RSEG    (ABS,DATA)
      000000                        480 	.org 0x0000
                           0000D7   481 _CY	=	0x00d7
                           0000D6   482 _AC	=	0x00d6
                           0000D5   483 _F0	=	0x00d5
                           0000D4   484 _RS1	=	0x00d4
                           0000D3   485 _RS0	=	0x00d3
                           0000D2   486 _OV	=	0x00d2
                           0000D1   487 _F1	=	0x00d1
                           0000D0   488 _P	=	0x00d0
                           0000AF   489 _EA	=	0x00af
                           0000AE   490 _E_DIS	=	0x00ae
                           0000AD   491 _ET2	=	0x00ad
                           0000AC   492 _ES	=	0x00ac
                           0000AB   493 _ET1	=	0x00ab
                           0000AA   494 _EX1	=	0x00aa
                           0000A9   495 _ET0	=	0x00a9
                           0000A8   496 _EX0	=	0x00a8
                           0000BF   497 _PH_FLAG	=	0x00bf
                           0000BE   498 _PL_FLAG	=	0x00be
                           0000BD   499 _PT2	=	0x00bd
                           0000BC   500 _PS	=	0x00bc
                           0000BB   501 _PT1	=	0x00bb
                           0000BA   502 _PX1	=	0x00ba
                           0000B9   503 _PT0	=	0x00b9
                           0000B8   504 _PX0	=	0x00b8
                           0000EF   505 _IE_WDOG	=	0x00ef
                           0000EE   506 _IE_GPIO	=	0x00ee
                           0000ED   507 _IE_PWMX	=	0x00ed
                           0000ED   508 _IE_UART3	=	0x00ed
                           0000EC   509 _IE_UART1	=	0x00ec
                           0000EB   510 _IE_ADC	=	0x00eb
                           0000EB   511 _IE_UART2	=	0x00eb
                           0000EA   512 _IE_USB	=	0x00ea
                           0000E9   513 _IE_INT3	=	0x00e9
                           0000E8   514 _IE_SPI0	=	0x00e8
                           000087   515 _P0_7	=	0x0087
                           000086   516 _P0_6	=	0x0086
                           000085   517 _P0_5	=	0x0085
                           000084   518 _P0_4	=	0x0084
                           000083   519 _P0_3	=	0x0083
                           000082   520 _P0_2	=	0x0082
                           000081   521 _P0_1	=	0x0081
                           000080   522 _P0_0	=	0x0080
                           000087   523 _TXD3	=	0x0087
                           000087   524 _AIN15	=	0x0087
                           000086   525 _RXD3	=	0x0086
                           000086   526 _AIN14	=	0x0086
                           000085   527 _TXD2	=	0x0085
                           000085   528 _AIN13	=	0x0085
                           000084   529 _RXD2	=	0x0084
                           000084   530 _AIN12	=	0x0084
                           000083   531 _TXD_	=	0x0083
                           000083   532 _AIN11	=	0x0083
                           000082   533 _RXD_	=	0x0082
                           000082   534 _AIN10	=	0x0082
                           000081   535 _AIN9	=	0x0081
                           000080   536 _AIN8	=	0x0080
                           000097   537 _P1_7	=	0x0097
                           000096   538 _P1_6	=	0x0096
                           000095   539 _P1_5	=	0x0095
                           000094   540 _P1_4	=	0x0094
                           000093   541 _P1_3	=	0x0093
                           000092   542 _P1_2	=	0x0092
                           000091   543 _P1_1	=	0x0091
                           000090   544 _P1_0	=	0x0090
                           000097   545 _SCK	=	0x0097
                           000097   546 _TXD1_	=	0x0097
                           000097   547 _AIN7	=	0x0097
                           000096   548 _MISO	=	0x0096
                           000096   549 _RXD1_	=	0x0096
                           000096   550 _VBUS	=	0x0096
                           000096   551 _AIN6	=	0x0096
                           000095   552 _MOSI	=	0x0095
                           000095   553 _PWM0_	=	0x0095
                           000095   554 _UCC2	=	0x0095
                           000095   555 _AIN5	=	0x0095
                           000094   556 _SCS	=	0x0094
                           000094   557 _UCC1	=	0x0094
                           000094   558 _AIN4	=	0x0094
                           000093   559 _AIN3	=	0x0093
                           000092   560 _AIN2	=	0x0092
                           000091   561 _T2EX	=	0x0091
                           000091   562 _CAP2	=	0x0091
                           000091   563 _AIN1	=	0x0091
                           000090   564 _T2	=	0x0090
                           000090   565 _CAP1	=	0x0090
                           000090   566 _AIN0	=	0x0090
                           0000A7   567 _P2_7	=	0x00a7
                           0000A6   568 _P2_6	=	0x00a6
                           0000A5   569 _P2_5	=	0x00a5
                           0000A4   570 _P2_4	=	0x00a4
                           0000A3   571 _P2_3	=	0x00a3
                           0000A2   572 _P2_2	=	0x00a2
                           0000A1   573 _P2_1	=	0x00a1
                           0000A0   574 _P2_0	=	0x00a0
                           0000A7   575 _PWM7	=	0x00a7
                           0000A7   576 _TXD1	=	0x00a7
                           0000A6   577 _PWM6	=	0x00a6
                           0000A6   578 _RXD1	=	0x00a6
                           0000A5   579 _PWM0	=	0x00a5
                           0000A5   580 _T2EX_	=	0x00a5
                           0000A5   581 _CAP2_	=	0x00a5
                           0000A4   582 _PWM1	=	0x00a4
                           0000A4   583 _T2_	=	0x00a4
                           0000A4   584 _CAP1_	=	0x00a4
                           0000A3   585 _PWM2	=	0x00a3
                           0000A2   586 _PWM3	=	0x00a2
                           0000A2   587 _INT0_	=	0x00a2
                           0000A1   588 _PWM4	=	0x00a1
                           0000A0   589 _PWM5	=	0x00a0
                           0000B7   590 _P3_7	=	0x00b7
                           0000B6   591 _P3_6	=	0x00b6
                           0000B5   592 _P3_5	=	0x00b5
                           0000B4   593 _P3_4	=	0x00b4
                           0000B3   594 _P3_3	=	0x00b3
                           0000B2   595 _P3_2	=	0x00b2
                           0000B1   596 _P3_1	=	0x00b1
                           0000B0   597 _P3_0	=	0x00b0
                           0000B7   598 _INT3	=	0x00b7
                           0000B6   599 _CAP0	=	0x00b6
                           0000B5   600 _T1	=	0x00b5
                           0000B4   601 _T0	=	0x00b4
                           0000B3   602 _INT1	=	0x00b3
                           0000B2   603 _INT0	=	0x00b2
                           0000B1   604 _TXD	=	0x00b1
                           0000B0   605 _RXD	=	0x00b0
                           0000C6   606 _P4_6	=	0x00c6
                           0000C5   607 _P4_5	=	0x00c5
                           0000C4   608 _P4_4	=	0x00c4
                           0000C3   609 _P4_3	=	0x00c3
                           0000C2   610 _P4_2	=	0x00c2
                           0000C1   611 _P4_1	=	0x00c1
                           0000C0   612 _P4_0	=	0x00c0
                           0000C7   613 _XO	=	0x00c7
                           0000C6   614 _XI	=	0x00c6
                           00008F   615 _TF1	=	0x008f
                           00008E   616 _TR1	=	0x008e
                           00008D   617 _TF0	=	0x008d
                           00008C   618 _TR0	=	0x008c
                           00008B   619 _IE1	=	0x008b
                           00008A   620 _IT1	=	0x008a
                           000089   621 _IE0	=	0x0089
                           000088   622 _IT0	=	0x0088
                           00009F   623 _SM0	=	0x009f
                           00009E   624 _SM1	=	0x009e
                           00009D   625 _SM2	=	0x009d
                           00009C   626 _REN	=	0x009c
                           00009B   627 _TB8	=	0x009b
                           00009A   628 _RB8	=	0x009a
                           000099   629 _TI	=	0x0099
                           000098   630 _RI	=	0x0098
                           0000CF   631 _TF2	=	0x00cf
                           0000CF   632 _CAP1F	=	0x00cf
                           0000CE   633 _EXF2	=	0x00ce
                           0000CD   634 _RCLK	=	0x00cd
                           0000CC   635 _TCLK	=	0x00cc
                           0000CB   636 _EXEN2	=	0x00cb
                           0000CA   637 _TR2	=	0x00ca
                           0000C9   638 _C_T2	=	0x00c9
                           0000C8   639 _CP_RL2	=	0x00c8
                           0000FF   640 _S0_FST_ACT	=	0x00ff
                           0000FE   641 _S0_IF_OV	=	0x00fe
                           0000FD   642 _S0_IF_FIRST	=	0x00fd
                           0000FC   643 _S0_IF_BYTE	=	0x00fc
                           0000FB   644 _S0_FREE	=	0x00fb
                           0000FA   645 _S0_T_FIFO	=	0x00fa
                           0000F8   646 _S0_R_FIFO	=	0x00f8
                           0000DF   647 _U_IS_NAK	=	0x00df
                           0000DE   648 _U_TOG_OK	=	0x00de
                           0000DD   649 _U_SIE_FREE	=	0x00dd
                           0000DC   650 _UIF_FIFO_OV	=	0x00dc
                           0000DB   651 _UIF_HST_SOF	=	0x00db
                           0000DA   652 _UIF_SUSPEND	=	0x00da
                           0000D9   653 _UIF_TRANSFER	=	0x00d9
                           0000D8   654 _UIF_DETECT	=	0x00d8
                           0000D8   655 _UIF_BUS_RST	=	0x00d8
                                    656 ;--------------------------------------------------------
                                    657 ; overlayable register banks
                                    658 ;--------------------------------------------------------
                                    659 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        660 	.ds 8
                                    661 ;--------------------------------------------------------
                                    662 ; internal ram data
                                    663 ;--------------------------------------------------------
                                    664 	.area DSEG    (DATA)
                                    665 ;--------------------------------------------------------
                                    666 ; overlayable items in internal ram 
                                    667 ;--------------------------------------------------------
                                    668 ;--------------------------------------------------------
                                    669 ; Stack segment in internal ram 
                                    670 ;--------------------------------------------------------
                                    671 	.area	SSEG
      000009                        672 __start__stack:
      000009                        673 	.ds	1
                                    674 
                                    675 ;--------------------------------------------------------
                                    676 ; indirectly addressable internal ram data
                                    677 ;--------------------------------------------------------
                                    678 	.area ISEG    (DATA)
                                    679 ;--------------------------------------------------------
                                    680 ; absolute internal ram data
                                    681 ;--------------------------------------------------------
                                    682 	.area IABS    (ABS,DATA)
                                    683 	.area IABS    (ABS,DATA)
                                    684 ;--------------------------------------------------------
                                    685 ; bit data
                                    686 ;--------------------------------------------------------
                                    687 	.area BSEG    (BIT)
                                    688 ;--------------------------------------------------------
                                    689 ; paged external ram data
                                    690 ;--------------------------------------------------------
                                    691 	.area PSEG    (PAG,XDATA)
                                    692 ;--------------------------------------------------------
                                    693 ; external ram data
                                    694 ;--------------------------------------------------------
                                    695 	.area XSEG    (XDATA)
                                    696 ;--------------------------------------------------------
                                    697 ; absolute external ram data
                                    698 ;--------------------------------------------------------
                                    699 	.area XABS    (ABS,XDATA)
                                    700 ;--------------------------------------------------------
                                    701 ; external initialized ram data
                                    702 ;--------------------------------------------------------
                                    703 	.area HOME    (CODE)
                                    704 	.area GSINIT0 (CODE)
                                    705 	.area GSINIT1 (CODE)
                                    706 	.area GSINIT2 (CODE)
                                    707 	.area GSINIT3 (CODE)
                                    708 	.area GSINIT4 (CODE)
                                    709 	.area GSINIT5 (CODE)
                                    710 	.area GSINIT  (CODE)
                                    711 	.area GSFINAL (CODE)
                                    712 	.area CSEG    (CODE)
                                    713 ;--------------------------------------------------------
                                    714 ; interrupt vector 
                                    715 ;--------------------------------------------------------
                                    716 	.area HOME    (CODE)
      000000                        717 __interrupt_vect:
      000000 02 00 06         [24]  718 	ljmp	__sdcc_gsinit_startup
                                    719 ;--------------------------------------------------------
                                    720 ; global & static initialisations
                                    721 ;--------------------------------------------------------
                                    722 	.area HOME    (CODE)
                                    723 	.area GSINIT  (CODE)
                                    724 	.area GSFINAL (CODE)
                                    725 	.area GSINIT  (CODE)
                                    726 	.globl __sdcc_gsinit_startup
                                    727 	.globl __sdcc_program_startup
                                    728 	.globl __start__stack
                                    729 	.globl __mcs51_genRAMCLEAR
                                    730 	.area GSFINAL (CODE)
      0001AE 02 00 03         [24]  731 	ljmp	__sdcc_program_startup
                                    732 ;--------------------------------------------------------
                                    733 ; Home
                                    734 ;--------------------------------------------------------
                                    735 	.area HOME    (CODE)
                                    736 	.area HOME    (CODE)
      000003                        737 __sdcc_program_startup:
      000003 02 01 B1         [24]  738 	ljmp	_main
                                    739 ;	return from main will return to caller
                                    740 ;--------------------------------------------------------
                                    741 ; code
                                    742 ;--------------------------------------------------------
                                    743 	.area CSEG    (CODE)
                                    744 ;------------------------------------------------------------
                                    745 ;Allocation info for local variables in function 'main'
                                    746 ;------------------------------------------------------------
                                    747 ;i                         Allocated to registers r7 
                                    748 ;------------------------------------------------------------
                                    749 ;	usr/main.c:25: void main(void)
                                    750 ;	-----------------------------------------
                                    751 ;	 function main
                                    752 ;	-----------------------------------------
      0001B1                        753 _main:
                           000007   754 	ar7 = 0x07
                           000006   755 	ar6 = 0x06
                           000005   756 	ar5 = 0x05
                           000004   757 	ar4 = 0x04
                           000003   758 	ar3 = 0x03
                           000002   759 	ar2 = 0x02
                           000001   760 	ar1 = 0x01
                           000000   761 	ar0 = 0x00
                                    762 ;	usr/main.c:28: LIGHT_Clean();
      0001B1 12 04 C8         [24]  763 	lcall	_LIGHT_Clean
                                    764 ;	usr/main.c:31: while (1)
      0001B4                        765 00104$:
                                    766 ;	usr/main.c:33: LIGHT_Converge(40);
      0001B4 90 00 28         [24]  767 	mov	dptr,#0x0028
      0001B7 12 06 07         [24]  768 	lcall	_LIGHT_Converge
                                    769 ;	usr/main.c:34: LIGHT_Converge(40);
      0001BA 90 00 28         [24]  770 	mov	dptr,#0x0028
      0001BD 12 06 07         [24]  771 	lcall	_LIGHT_Converge
                                    772 ;	usr/main.c:35: LIGHT_Converge(40);
      0001C0 90 00 28         [24]  773 	mov	dptr,#0x0028
      0001C3 12 06 07         [24]  774 	lcall	_LIGHT_Converge
                                    775 ;	usr/main.c:36: mDelaymS(200);
      0001C6 90 00 C8         [24]  776 	mov	dptr,#0x00c8
      0001C9 12 02 FA         [24]  777 	lcall	_mDelaymS
                                    778 ;	usr/main.c:37: LIGHT_TLineOn();
      0001CC 12 04 F0         [24]  779 	lcall	_LIGHT_TLineOn
                                    780 ;	usr/main.c:38: mDelaymS(200);
      0001CF 90 00 C8         [24]  781 	mov	dptr,#0x00c8
      0001D2 12 02 FA         [24]  782 	lcall	_mDelaymS
                                    783 ;	usr/main.c:39: LIGHT_Disperse(40);
      0001D5 90 00 28         [24]  784 	mov	dptr,#0x0028
      0001D8 12 07 DC         [24]  785 	lcall	_LIGHT_Disperse
                                    786 ;	usr/main.c:40: LIGHT_Disperse(40);
      0001DB 90 00 28         [24]  787 	mov	dptr,#0x0028
      0001DE 12 07 DC         [24]  788 	lcall	_LIGHT_Disperse
                                    789 ;	usr/main.c:41: LIGHT_Disperse(40);
      0001E1 90 00 28         [24]  790 	mov	dptr,#0x0028
      0001E4 12 07 DC         [24]  791 	lcall	_LIGHT_Disperse
                                    792 ;	usr/main.c:42: mDelaymS(200);
      0001E7 90 00 C8         [24]  793 	mov	dptr,#0x00c8
      0001EA 12 02 FA         [24]  794 	lcall	_mDelaymS
                                    795 ;	usr/main.c:43: LIGHT_FLineOn();
      0001ED 12 05 6E         [24]  796 	lcall	_LIGHT_FLineOn
                                    797 ;	usr/main.c:44: mDelaymS(200);
      0001F0 90 00 C8         [24]  798 	mov	dptr,#0x00c8
      0001F3 12 02 FA         [24]  799 	lcall	_mDelaymS
                                    800 ;	usr/main.c:45: LIGHT_AllOn();
      0001F6 12 04 DC         [24]  801 	lcall	_LIGHT_AllOn
                                    802 ;	usr/main.c:46: mDelaymS(50);
      0001F9 90 00 32         [24]  803 	mov	dptr,#0x0032
      0001FC 12 02 FA         [24]  804 	lcall	_mDelaymS
                                    805 ;	usr/main.c:47: LIGHT_Clean();
      0001FF 12 04 C8         [24]  806 	lcall	_LIGHT_Clean
                                    807 ;	usr/main.c:48: mDelaymS(50);
      000202 90 00 32         [24]  808 	mov	dptr,#0x0032
      000205 12 02 FA         [24]  809 	lcall	_mDelaymS
                                    810 ;	usr/main.c:49: LIGHT_AllOn();
      000208 12 04 DC         [24]  811 	lcall	_LIGHT_AllOn
                                    812 ;	usr/main.c:50: mDelaymS(50);
      00020B 90 00 32         [24]  813 	mov	dptr,#0x0032
      00020E 12 02 FA         [24]  814 	lcall	_mDelaymS
                                    815 ;	usr/main.c:51: LIGHT_Clean();
      000211 12 04 C8         [24]  816 	lcall	_LIGHT_Clean
                                    817 ;	usr/main.c:52: mDelaymS(50);
      000214 90 00 32         [24]  818 	mov	dptr,#0x0032
      000217 12 02 FA         [24]  819 	lcall	_mDelaymS
                                    820 ;	usr/main.c:53: LIGHT_AllOn();
      00021A 12 04 DC         [24]  821 	lcall	_LIGHT_AllOn
                                    822 ;	usr/main.c:54: mDelaymS(50);
      00021D 90 00 32         [24]  823 	mov	dptr,#0x0032
      000220 12 02 FA         [24]  824 	lcall	_mDelaymS
                                    825 ;	usr/main.c:55: LIGHT_Clean();
      000223 12 04 C8         [24]  826 	lcall	_LIGHT_Clean
                                    827 ;	usr/main.c:56: mDelaymS(50);
      000226 90 00 32         [24]  828 	mov	dptr,#0x0032
      000229 12 02 FA         [24]  829 	lcall	_mDelaymS
                                    830 ;	usr/main.c:57: LIGHT_Random(5);
      00022C 75 82 05         [24]  831 	mov	dpl,#0x05
      00022F 12 09 9B         [24]  832 	lcall	_LIGHT_Random
                                    833 ;	usr/main.c:58: mDelaymS(200);
      000232 90 00 C8         [24]  834 	mov	dptr,#0x00c8
      000235 12 02 FA         [24]  835 	lcall	_mDelaymS
                                    836 ;	usr/main.c:59: for ( i = 0; i < 6; i++)
      000238 7F 00            [12]  837 	mov	r7,#0x00
      00023A                        838 00106$:
                                    839 ;	usr/main.c:61: LIGHT_Converge(10 * (6 - i));
      00023A 8F 05            [24]  840 	mov	ar5,r7
      00023C 7E 00            [12]  841 	mov	r6,#0x00
      00023E 74 06            [12]  842 	mov	a,#0x06
      000240 C3               [12]  843 	clr	c
      000241 9D               [12]  844 	subb	a,r5
      000242 FD               [12]  845 	mov	r5,a
      000243 E4               [12]  846 	clr	a
      000244 9E               [12]  847 	subb	a,r6
      000245 FE               [12]  848 	mov	r6,a
      000246 C0 07            [24]  849 	push	ar7
      000248 C0 05            [24]  850 	push	ar5
      00024A C0 06            [24]  851 	push	ar6
      00024C 90 00 0A         [24]  852 	mov	dptr,#0x000a
      00024F 12 0D 51         [24]  853 	lcall	__mulint
      000252 AD 82            [24]  854 	mov	r5,dpl
      000254 AE 83            [24]  855 	mov	r6,dph
      000256 15 81            [12]  856 	dec	sp
      000258 15 81            [12]  857 	dec	sp
      00025A 8D 82            [24]  858 	mov	dpl,r5
      00025C 8E 83            [24]  859 	mov	dph,r6
      00025E 12 06 07         [24]  860 	lcall	_LIGHT_Converge
      000261 D0 07            [24]  861 	pop	ar7
                                    862 ;	usr/main.c:59: for ( i = 0; i < 6; i++)
      000263 0F               [12]  863 	inc	r7
      000264 BF 06 00         [24]  864 	cjne	r7,#0x06,00134$
      000267                        865 00134$:
      000267 40 D1            [24]  866 	jc	00106$
                                    867 ;	usr/main.c:64: for ( i = 0; i < 6; i++)
      000269 7F 00            [12]  868 	mov	r7,#0x00
      00026B                        869 00108$:
                                    870 ;	usr/main.c:66: LIGHT_Disperse(10 * (6 - i));
      00026B 8F 05            [24]  871 	mov	ar5,r7
      00026D 7E 00            [12]  872 	mov	r6,#0x00
      00026F 74 06            [12]  873 	mov	a,#0x06
      000271 C3               [12]  874 	clr	c
      000272 9D               [12]  875 	subb	a,r5
      000273 FD               [12]  876 	mov	r5,a
      000274 E4               [12]  877 	clr	a
      000275 9E               [12]  878 	subb	a,r6
      000276 FE               [12]  879 	mov	r6,a
      000277 C0 07            [24]  880 	push	ar7
      000279 C0 05            [24]  881 	push	ar5
      00027B C0 06            [24]  882 	push	ar6
      00027D 90 00 0A         [24]  883 	mov	dptr,#0x000a
      000280 12 0D 51         [24]  884 	lcall	__mulint
      000283 AD 82            [24]  885 	mov	r5,dpl
      000285 AE 83            [24]  886 	mov	r6,dph
      000287 15 81            [12]  887 	dec	sp
      000289 15 81            [12]  888 	dec	sp
      00028B 8D 82            [24]  889 	mov	dpl,r5
      00028D 8E 83            [24]  890 	mov	dph,r6
      00028F 12 07 DC         [24]  891 	lcall	_LIGHT_Disperse
      000292 D0 07            [24]  892 	pop	ar7
                                    893 ;	usr/main.c:64: for ( i = 0; i < 6; i++)
      000294 0F               [12]  894 	inc	r7
      000295 BF 06 00         [24]  895 	cjne	r7,#0x06,00136$
      000298                        896 00136$:
      000298 40 D1            [24]  897 	jc	00108$
                                    898 ;	usr/main.c:69: LIGHT_AllOn();
      00029A 12 04 DC         [24]  899 	lcall	_LIGHT_AllOn
                                    900 ;	usr/main.c:70: mDelaymS(1000);
      00029D 90 03 E8         [24]  901 	mov	dptr,#0x03e8
      0002A0 12 02 FA         [24]  902 	lcall	_mDelaymS
                                    903 ;	usr/main.c:73: }
      0002A3 02 01 B4         [24]  904 	ljmp	00104$
                                    905 	.area CSEG    (CODE)
                                    906 	.area CONST   (CODE)
                                    907 	.area CABS    (ABS,CODE)
