;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.0.0 #11528 (MINGW64)
;--------------------------------------------------------
	.module CH549_SubwayBoard
	.optsdcc -mmcs51 --model-large
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _rand
	.globl _mDelaymS
	.globl _mDelayuS
	.globl _UIF_BUS_RST
	.globl _UIF_DETECT
	.globl _UIF_TRANSFER
	.globl _UIF_SUSPEND
	.globl _UIF_HST_SOF
	.globl _UIF_FIFO_OV
	.globl _U_SIE_FREE
	.globl _U_TOG_OK
	.globl _U_IS_NAK
	.globl _S0_R_FIFO
	.globl _S0_T_FIFO
	.globl _S0_FREE
	.globl _S0_IF_BYTE
	.globl _S0_IF_FIRST
	.globl _S0_IF_OV
	.globl _S0_FST_ACT
	.globl _CP_RL2
	.globl _C_T2
	.globl _TR2
	.globl _EXEN2
	.globl _TCLK
	.globl _RCLK
	.globl _EXF2
	.globl _CAP1F
	.globl _TF2
	.globl _RI
	.globl _TI
	.globl _RB8
	.globl _TB8
	.globl _REN
	.globl _SM2
	.globl _SM1
	.globl _SM0
	.globl _IT0
	.globl _IE0
	.globl _IT1
	.globl _IE1
	.globl _TR0
	.globl _TF0
	.globl _TR1
	.globl _TF1
	.globl _XI
	.globl _XO
	.globl _P4_0
	.globl _P4_1
	.globl _P4_2
	.globl _P4_3
	.globl _P4_4
	.globl _P4_5
	.globl _P4_6
	.globl _RXD
	.globl _TXD
	.globl _INT0
	.globl _INT1
	.globl _T0
	.globl _T1
	.globl _CAP0
	.globl _INT3
	.globl _P3_0
	.globl _P3_1
	.globl _P3_2
	.globl _P3_3
	.globl _P3_4
	.globl _P3_5
	.globl _P3_6
	.globl _P3_7
	.globl _PWM5
	.globl _PWM4
	.globl _INT0_
	.globl _PWM3
	.globl _PWM2
	.globl _CAP1_
	.globl _T2_
	.globl _PWM1
	.globl _CAP2_
	.globl _T2EX_
	.globl _PWM0
	.globl _RXD1
	.globl _PWM6
	.globl _TXD1
	.globl _PWM7
	.globl _P2_0
	.globl _P2_1
	.globl _P2_2
	.globl _P2_3
	.globl _P2_4
	.globl _P2_5
	.globl _P2_6
	.globl _P2_7
	.globl _AIN0
	.globl _CAP1
	.globl _T2
	.globl _AIN1
	.globl _CAP2
	.globl _T2EX
	.globl _AIN2
	.globl _AIN3
	.globl _AIN4
	.globl _UCC1
	.globl _SCS
	.globl _AIN5
	.globl _UCC2
	.globl _PWM0_
	.globl _MOSI
	.globl _AIN6
	.globl _VBUS
	.globl _RXD1_
	.globl _MISO
	.globl _AIN7
	.globl _TXD1_
	.globl _SCK
	.globl _P1_0
	.globl _P1_1
	.globl _P1_2
	.globl _P1_3
	.globl _P1_4
	.globl _P1_5
	.globl _P1_6
	.globl _P1_7
	.globl _AIN8
	.globl _AIN9
	.globl _AIN10
	.globl _RXD_
	.globl _AIN11
	.globl _TXD_
	.globl _AIN12
	.globl _RXD2
	.globl _AIN13
	.globl _TXD2
	.globl _AIN14
	.globl _RXD3
	.globl _AIN15
	.globl _TXD3
	.globl _P0_0
	.globl _P0_1
	.globl _P0_2
	.globl _P0_3
	.globl _P0_4
	.globl _P0_5
	.globl _P0_6
	.globl _P0_7
	.globl _IE_SPI0
	.globl _IE_INT3
	.globl _IE_USB
	.globl _IE_UART2
	.globl _IE_ADC
	.globl _IE_UART1
	.globl _IE_UART3
	.globl _IE_PWMX
	.globl _IE_GPIO
	.globl _IE_WDOG
	.globl _PX0
	.globl _PT0
	.globl _PX1
	.globl _PT1
	.globl _PS
	.globl _PT2
	.globl _PL_FLAG
	.globl _PH_FLAG
	.globl _EX0
	.globl _ET0
	.globl _EX1
	.globl _ET1
	.globl _ES
	.globl _ET2
	.globl _E_DIS
	.globl _EA
	.globl _P
	.globl _F1
	.globl _OV
	.globl _RS0
	.globl _RS1
	.globl _F0
	.globl _AC
	.globl _CY
	.globl _UEP1_DMA_H
	.globl _UEP1_DMA_L
	.globl _UEP1_DMA
	.globl _UEP0_DMA_H
	.globl _UEP0_DMA_L
	.globl _UEP0_DMA
	.globl _UEP2_3_MOD
	.globl _UEP4_1_MOD
	.globl _UEP3_DMA_H
	.globl _UEP3_DMA_L
	.globl _UEP3_DMA
	.globl _UEP2_DMA_H
	.globl _UEP2_DMA_L
	.globl _UEP2_DMA
	.globl _USB_DEV_AD
	.globl _USB_CTRL
	.globl _USB_INT_EN
	.globl _UEP4_T_LEN
	.globl _UEP4_CTRL
	.globl _UEP0_T_LEN
	.globl _UEP0_CTRL
	.globl _USB_RX_LEN
	.globl _USB_MIS_ST
	.globl _USB_INT_ST
	.globl _USB_INT_FG
	.globl _UEP3_T_LEN
	.globl _UEP3_CTRL
	.globl _UEP2_T_LEN
	.globl _UEP2_CTRL
	.globl _UEP1_T_LEN
	.globl _UEP1_CTRL
	.globl _UDEV_CTRL
	.globl _USB_C_CTRL
	.globl _ADC_PIN
	.globl _ADC_CHAN
	.globl _ADC_DAT_H
	.globl _ADC_DAT_L
	.globl _ADC_DAT
	.globl _ADC_CFG
	.globl _ADC_CTRL
	.globl _TKEY_CTRL
	.globl _SIF3
	.globl _SBAUD3
	.globl _SBUF3
	.globl _SCON3
	.globl _SIF2
	.globl _SBAUD2
	.globl _SBUF2
	.globl _SCON2
	.globl _SIF1
	.globl _SBAUD1
	.globl _SBUF1
	.globl _SCON1
	.globl _SPI0_SETUP
	.globl _SPI0_CK_SE
	.globl _SPI0_CTRL
	.globl _SPI0_DATA
	.globl _SPI0_STAT
	.globl _PWM_DATA7
	.globl _PWM_DATA6
	.globl _PWM_DATA5
	.globl _PWM_DATA4
	.globl _PWM_DATA3
	.globl _PWM_CTRL2
	.globl _PWM_CK_SE
	.globl _PWM_CTRL
	.globl _PWM_DATA0
	.globl _PWM_DATA1
	.globl _PWM_DATA2
	.globl _T2CAP1H
	.globl _T2CAP1L
	.globl _T2CAP1
	.globl _TH2
	.globl _TL2
	.globl _T2COUNT
	.globl _RCAP2H
	.globl _RCAP2L
	.globl _RCAP2
	.globl _T2MOD
	.globl _T2CON
	.globl _T2CAP0H
	.globl _T2CAP0L
	.globl _T2CAP0
	.globl _T2CON2
	.globl _SBUF
	.globl _SCON
	.globl _TH1
	.globl _TH0
	.globl _TL1
	.globl _TL0
	.globl _TMOD
	.globl _TCON
	.globl _XBUS_AUX
	.globl _PIN_FUNC
	.globl _P5
	.globl _P4_DIR_PU
	.globl _P4_MOD_OC
	.globl _P4
	.globl _P3_DIR_PU
	.globl _P3_MOD_OC
	.globl _P3
	.globl _P2_DIR_PU
	.globl _P2_MOD_OC
	.globl _P2
	.globl _P1_DIR_PU
	.globl _P1_MOD_OC
	.globl _P1
	.globl _P0_DIR_PU
	.globl _P0_MOD_OC
	.globl _P0
	.globl _ROM_CTRL
	.globl _ROM_DATA_HH
	.globl _ROM_DATA_HL
	.globl _ROM_DATA_HI
	.globl _ROM_ADDR_H
	.globl _ROM_ADDR_L
	.globl _ROM_ADDR
	.globl _GPIO_IE
	.globl _INTX
	.globl _IP_EX
	.globl _IE_EX
	.globl _IP
	.globl _IE
	.globl _WDOG_COUNT
	.globl _RESET_KEEP
	.globl _WAKE_CTRL
	.globl _CLOCK_CFG
	.globl _POWER_CFG
	.globl _PCON
	.globl _GLOBAL_CFG
	.globl _SAFE_MOD
	.globl _DPH
	.globl _DPL
	.globl _SP
	.globl _A_INV
	.globl _B
	.globl _ACC
	.globl _PSW
	.globl _SubwayCircle
	.globl _SubwayLine
	.globl _LIGHT_ON
	.globl _LIGHT_OFF
	.globl _LIGHT_Flash
	.globl _LIGHT_Clean
	.globl _LIGHT_AllOn
	.globl _LIGHT_TLineOn
	.globl _LIGHT_FLineOn
	.globl _LIGHT_Converge
	.globl _LIGHT_Disperse
	.globl _LIGHT_Random
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
	.area RSEG    (ABS,DATA)
	.org 0x0000
_PSW	=	0x00d0
_ACC	=	0x00e0
_B	=	0x00f0
_A_INV	=	0x00fd
_SP	=	0x0081
_DPL	=	0x0082
_DPH	=	0x0083
_SAFE_MOD	=	0x00a1
_GLOBAL_CFG	=	0x00b1
_PCON	=	0x0087
_POWER_CFG	=	0x00ba
_CLOCK_CFG	=	0x00b9
_WAKE_CTRL	=	0x00a9
_RESET_KEEP	=	0x00fe
_WDOG_COUNT	=	0x00ff
_IE	=	0x00a8
_IP	=	0x00b8
_IE_EX	=	0x00e8
_IP_EX	=	0x00e9
_INTX	=	0x00b3
_GPIO_IE	=	0x00b2
_ROM_ADDR	=	0x8584
_ROM_ADDR_L	=	0x0084
_ROM_ADDR_H	=	0x0085
_ROM_DATA_HI	=	0x8f8e
_ROM_DATA_HL	=	0x008e
_ROM_DATA_HH	=	0x008f
_ROM_CTRL	=	0x0086
_P0	=	0x0080
_P0_MOD_OC	=	0x00c4
_P0_DIR_PU	=	0x00c5
_P1	=	0x0090
_P1_MOD_OC	=	0x0092
_P1_DIR_PU	=	0x0093
_P2	=	0x00a0
_P2_MOD_OC	=	0x0094
_P2_DIR_PU	=	0x0095
_P3	=	0x00b0
_P3_MOD_OC	=	0x0096
_P3_DIR_PU	=	0x0097
_P4	=	0x00c0
_P4_MOD_OC	=	0x00c2
_P4_DIR_PU	=	0x00c3
_P5	=	0x00ab
_PIN_FUNC	=	0x00aa
_XBUS_AUX	=	0x00a2
_TCON	=	0x0088
_TMOD	=	0x0089
_TL0	=	0x008a
_TL1	=	0x008b
_TH0	=	0x008c
_TH1	=	0x008d
_SCON	=	0x0098
_SBUF	=	0x0099
_T2CON2	=	0x00c1
_T2CAP0	=	0xc7c6
_T2CAP0L	=	0x00c6
_T2CAP0H	=	0x00c7
_T2CON	=	0x00c8
_T2MOD	=	0x00c9
_RCAP2	=	0xcbca
_RCAP2L	=	0x00ca
_RCAP2H	=	0x00cb
_T2COUNT	=	0xcdcc
_TL2	=	0x00cc
_TH2	=	0x00cd
_T2CAP1	=	0xcfce
_T2CAP1L	=	0x00ce
_T2CAP1H	=	0x00cf
_PWM_DATA2	=	0x009a
_PWM_DATA1	=	0x009b
_PWM_DATA0	=	0x009c
_PWM_CTRL	=	0x009d
_PWM_CK_SE	=	0x009e
_PWM_CTRL2	=	0x009f
_PWM_DATA3	=	0x00a3
_PWM_DATA4	=	0x00a4
_PWM_DATA5	=	0x00a5
_PWM_DATA6	=	0x00a6
_PWM_DATA7	=	0x00a7
_SPI0_STAT	=	0x00f8
_SPI0_DATA	=	0x00f9
_SPI0_CTRL	=	0x00fa
_SPI0_CK_SE	=	0x00fb
_SPI0_SETUP	=	0x00fc
_SCON1	=	0x00bc
_SBUF1	=	0x00bd
_SBAUD1	=	0x00be
_SIF1	=	0x00bf
_SCON2	=	0x00b4
_SBUF2	=	0x00b5
_SBAUD2	=	0x00b6
_SIF2	=	0x00b7
_SCON3	=	0x00ac
_SBUF3	=	0x00ad
_SBAUD3	=	0x00ae
_SIF3	=	0x00af
_TKEY_CTRL	=	0x00f1
_ADC_CTRL	=	0x00f2
_ADC_CFG	=	0x00f3
_ADC_DAT	=	0xf5f4
_ADC_DAT_L	=	0x00f4
_ADC_DAT_H	=	0x00f5
_ADC_CHAN	=	0x00f6
_ADC_PIN	=	0x00f7
_USB_C_CTRL	=	0x0091
_UDEV_CTRL	=	0x00d1
_UEP1_CTRL	=	0x00d2
_UEP1_T_LEN	=	0x00d3
_UEP2_CTRL	=	0x00d4
_UEP2_T_LEN	=	0x00d5
_UEP3_CTRL	=	0x00d6
_UEP3_T_LEN	=	0x00d7
_USB_INT_FG	=	0x00d8
_USB_INT_ST	=	0x00d9
_USB_MIS_ST	=	0x00da
_USB_RX_LEN	=	0x00db
_UEP0_CTRL	=	0x00dc
_UEP0_T_LEN	=	0x00dd
_UEP4_CTRL	=	0x00de
_UEP4_T_LEN	=	0x00df
_USB_INT_EN	=	0x00e1
_USB_CTRL	=	0x00e2
_USB_DEV_AD	=	0x00e3
_UEP2_DMA	=	0xe5e4
_UEP2_DMA_L	=	0x00e4
_UEP2_DMA_H	=	0x00e5
_UEP3_DMA	=	0xe7e6
_UEP3_DMA_L	=	0x00e6
_UEP3_DMA_H	=	0x00e7
_UEP4_1_MOD	=	0x00ea
_UEP2_3_MOD	=	0x00eb
_UEP0_DMA	=	0xedec
_UEP0_DMA_L	=	0x00ec
_UEP0_DMA_H	=	0x00ed
_UEP1_DMA	=	0xefee
_UEP1_DMA_L	=	0x00ee
_UEP1_DMA_H	=	0x00ef
;--------------------------------------------------------
; special function bits
;--------------------------------------------------------
	.area RSEG    (ABS,DATA)
	.org 0x0000
_CY	=	0x00d7
_AC	=	0x00d6
_F0	=	0x00d5
_RS1	=	0x00d4
_RS0	=	0x00d3
_OV	=	0x00d2
_F1	=	0x00d1
_P	=	0x00d0
_EA	=	0x00af
_E_DIS	=	0x00ae
_ET2	=	0x00ad
_ES	=	0x00ac
_ET1	=	0x00ab
_EX1	=	0x00aa
_ET0	=	0x00a9
_EX0	=	0x00a8
_PH_FLAG	=	0x00bf
_PL_FLAG	=	0x00be
_PT2	=	0x00bd
_PS	=	0x00bc
_PT1	=	0x00bb
_PX1	=	0x00ba
_PT0	=	0x00b9
_PX0	=	0x00b8
_IE_WDOG	=	0x00ef
_IE_GPIO	=	0x00ee
_IE_PWMX	=	0x00ed
_IE_UART3	=	0x00ed
_IE_UART1	=	0x00ec
_IE_ADC	=	0x00eb
_IE_UART2	=	0x00eb
_IE_USB	=	0x00ea
_IE_INT3	=	0x00e9
_IE_SPI0	=	0x00e8
_P0_7	=	0x0087
_P0_6	=	0x0086
_P0_5	=	0x0085
_P0_4	=	0x0084
_P0_3	=	0x0083
_P0_2	=	0x0082
_P0_1	=	0x0081
_P0_0	=	0x0080
_TXD3	=	0x0087
_AIN15	=	0x0087
_RXD3	=	0x0086
_AIN14	=	0x0086
_TXD2	=	0x0085
_AIN13	=	0x0085
_RXD2	=	0x0084
_AIN12	=	0x0084
_TXD_	=	0x0083
_AIN11	=	0x0083
_RXD_	=	0x0082
_AIN10	=	0x0082
_AIN9	=	0x0081
_AIN8	=	0x0080
_P1_7	=	0x0097
_P1_6	=	0x0096
_P1_5	=	0x0095
_P1_4	=	0x0094
_P1_3	=	0x0093
_P1_2	=	0x0092
_P1_1	=	0x0091
_P1_0	=	0x0090
_SCK	=	0x0097
_TXD1_	=	0x0097
_AIN7	=	0x0097
_MISO	=	0x0096
_RXD1_	=	0x0096
_VBUS	=	0x0096
_AIN6	=	0x0096
_MOSI	=	0x0095
_PWM0_	=	0x0095
_UCC2	=	0x0095
_AIN5	=	0x0095
_SCS	=	0x0094
_UCC1	=	0x0094
_AIN4	=	0x0094
_AIN3	=	0x0093
_AIN2	=	0x0092
_T2EX	=	0x0091
_CAP2	=	0x0091
_AIN1	=	0x0091
_T2	=	0x0090
_CAP1	=	0x0090
_AIN0	=	0x0090
_P2_7	=	0x00a7
_P2_6	=	0x00a6
_P2_5	=	0x00a5
_P2_4	=	0x00a4
_P2_3	=	0x00a3
_P2_2	=	0x00a2
_P2_1	=	0x00a1
_P2_0	=	0x00a0
_PWM7	=	0x00a7
_TXD1	=	0x00a7
_PWM6	=	0x00a6
_RXD1	=	0x00a6
_PWM0	=	0x00a5
_T2EX_	=	0x00a5
_CAP2_	=	0x00a5
_PWM1	=	0x00a4
_T2_	=	0x00a4
_CAP1_	=	0x00a4
_PWM2	=	0x00a3
_PWM3	=	0x00a2
_INT0_	=	0x00a2
_PWM4	=	0x00a1
_PWM5	=	0x00a0
_P3_7	=	0x00b7
_P3_6	=	0x00b6
_P3_5	=	0x00b5
_P3_4	=	0x00b4
_P3_3	=	0x00b3
_P3_2	=	0x00b2
_P3_1	=	0x00b1
_P3_0	=	0x00b0
_INT3	=	0x00b7
_CAP0	=	0x00b6
_T1	=	0x00b5
_T0	=	0x00b4
_INT1	=	0x00b3
_INT0	=	0x00b2
_TXD	=	0x00b1
_RXD	=	0x00b0
_P4_6	=	0x00c6
_P4_5	=	0x00c5
_P4_4	=	0x00c4
_P4_3	=	0x00c3
_P4_2	=	0x00c2
_P4_1	=	0x00c1
_P4_0	=	0x00c0
_XO	=	0x00c7
_XI	=	0x00c6
_TF1	=	0x008f
_TR1	=	0x008e
_TF0	=	0x008d
_TR0	=	0x008c
_IE1	=	0x008b
_IT1	=	0x008a
_IE0	=	0x0089
_IT0	=	0x0088
_SM0	=	0x009f
_SM1	=	0x009e
_SM2	=	0x009d
_REN	=	0x009c
_TB8	=	0x009b
_RB8	=	0x009a
_TI	=	0x0099
_RI	=	0x0098
_TF2	=	0x00cf
_CAP1F	=	0x00cf
_EXF2	=	0x00ce
_RCLK	=	0x00cd
_TCLK	=	0x00cc
_EXEN2	=	0x00cb
_TR2	=	0x00ca
_C_T2	=	0x00c9
_CP_RL2	=	0x00c8
_S0_FST_ACT	=	0x00ff
_S0_IF_OV	=	0x00fe
_S0_IF_FIRST	=	0x00fd
_S0_IF_BYTE	=	0x00fc
_S0_FREE	=	0x00fb
_S0_T_FIFO	=	0x00fa
_S0_R_FIFO	=	0x00f8
_U_IS_NAK	=	0x00df
_U_TOG_OK	=	0x00de
_U_SIE_FREE	=	0x00dd
_UIF_FIFO_OV	=	0x00dc
_UIF_HST_SOF	=	0x00db
_UIF_SUSPEND	=	0x00da
_UIF_TRANSFER	=	0x00d9
_UIF_DETECT	=	0x00d8
_UIF_BUS_RST	=	0x00d8
;--------------------------------------------------------
; overlayable register banks
;--------------------------------------------------------
	.area REG_BANK_0	(REL,OVR,DATA)
	.ds 8
;--------------------------------------------------------
; internal ram data
;--------------------------------------------------------
	.area DSEG    (DATA)
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;--------------------------------------------------------
; indirectly addressable internal ram data
;--------------------------------------------------------
	.area ISEG    (DATA)
;--------------------------------------------------------
; absolute internal ram data
;--------------------------------------------------------
	.area IABS    (ABS,DATA)
	.area IABS    (ABS,DATA)
;--------------------------------------------------------
; bit data
;--------------------------------------------------------
	.area BSEG    (BIT)
;--------------------------------------------------------
; paged external ram data
;--------------------------------------------------------
	.area PSEG    (PAG,XDATA)
;--------------------------------------------------------
; external ram data
;--------------------------------------------------------
	.area XSEG    (XDATA)
_SubwayLine::
	.ds 42
_SubwayCircle::
	.ds 36
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area XABS    (ABS,XDATA)
;--------------------------------------------------------
; external initialized ram data
;--------------------------------------------------------
	.area HOME    (CODE)
	.area GSINIT0 (CODE)
	.area GSINIT1 (CODE)
	.area GSINIT2 (CODE)
	.area GSINIT3 (CODE)
	.area GSINIT4 (CODE)
	.area GSINIT5 (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area CSEG    (CODE)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME    (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area GSINIT  (CODE)
;	source/CH549_SubwayBoard.c:11: UINT8 SubwayLine[6][7] = {
	mov	dptr,#_SubwayLine
	mov	a,#0x0a
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x0001)
	mov	a,#0x0f
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x0002)
	dec	a
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x0003)
	dec	a
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x0004)
	dec	a
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x0005)
	mov	a,#0x07
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x0006)
	dec	a
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x0007)
	dec	a
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x0008)
	dec	a
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x0009)
	dec	a
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x000a)
	mov	a,#0x08
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x000b)
	mov	a,#0x0d
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x000c)
	mov	a,#0x11
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x000d)
	dec	a
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x000e)
	mov	a,#0x14
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x000f)
	mov	a,#0x0f
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x0010)
	mov	a,#0x09
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x0011)
	dec	a
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x0012)
	mov	a,#0x02
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x0013)
	mov	a,#0x07
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x0014)
	mov	a,#0x0b
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x0015)
	mov	a,#0x13
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x0016)
	mov	a,#0x0e
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x0017)
	mov	a,#0x09
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x0018)
	mov	a,#0x05
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x0019)
	clr	a
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x001a)
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x001b)
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x001c)
	mov	a,#0x12
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x001d)
	dec	a
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x001e)
	mov	a,#0x0c
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x001f)
	inc	a
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x0020)
	inc	a
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x0021)
	inc	a
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x0022)
	mov	a,#0x0a
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x0023)
	mov	a,#0x10
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x0024)
	mov	a,#0x07
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x0025)
	mov	a,#0x01
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x0026)
	clr	a
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x0027)
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x0028)
	movx	@dptr,a
	mov	dptr,#(_SubwayLine + 0x0029)
	movx	@dptr,a
;	source/CH549_SubwayBoard.c:20: UINT8 SubwayCircle[4][9] = {
	mov	dptr,#_SubwayCircle
	inc	a
	movx	@dptr,a
	mov	dptr,#(_SubwayCircle + 0x0001)
	mov	a,#0x05
	movx	@dptr,a
	mov	dptr,#(_SubwayCircle + 0x0002)
	inc	a
	movx	@dptr,a
	mov	dptr,#(_SubwayCircle + 0x0003)
	mov	a,#0x0a
	movx	@dptr,a
	mov	dptr,#(_SubwayCircle + 0x0004)
	inc	a
	movx	@dptr,a
	mov	dptr,#(_SubwayCircle + 0x0005)
	mov	a,#0x10
	movx	@dptr,a
	mov	dptr,#(_SubwayCircle + 0x0006)
	mov	a,#0x12
	movx	@dptr,a
	mov	dptr,#(_SubwayCircle + 0x0007)
	inc	a
	movx	@dptr,a
	mov	dptr,#(_SubwayCircle + 0x0008)
	inc	a
	movx	@dptr,a
	mov	dptr,#(_SubwayCircle + 0x0009)
	mov	a,#0x04
	movx	@dptr,a
	mov	dptr,#(_SubwayCircle + 0x000a)
	mov	a,#0x07
	movx	@dptr,a
	mov	dptr,#(_SubwayCircle + 0x000b)
	rl	a
	movx	@dptr,a
	mov	dptr,#(_SubwayCircle + 0x000c)
	inc	a
	movx	@dptr,a
	mov	dptr,#(_SubwayCircle + 0x000d)
	mov	a,#0x11
	movx	@dptr,a
	mov	dptr,#(_SubwayCircle + 0x000e)
	clr	a
	movx	@dptr,a
	mov	dptr,#(_SubwayCircle + 0x000f)
	movx	@dptr,a
	mov	dptr,#(_SubwayCircle + 0x0010)
	movx	@dptr,a
	mov	dptr,#(_SubwayCircle + 0x0011)
	movx	@dptr,a
	mov	dptr,#(_SubwayCircle + 0x0012)
	mov	a,#0x02
	movx	@dptr,a
	mov	dptr,#(_SubwayCircle + 0x0013)
	inc	a
	movx	@dptr,a
	mov	dptr,#(_SubwayCircle + 0x0014)
	mov	a,#0x09
	movx	@dptr,a
	mov	dptr,#(_SubwayCircle + 0x0015)
	mov	a,#0x0c
	movx	@dptr,a
	mov	dptr,#(_SubwayCircle + 0x0016)
	inc	a
	movx	@dptr,a
	mov	dptr,#(_SubwayCircle + 0x0017)
	clr	a
	movx	@dptr,a
	mov	dptr,#(_SubwayCircle + 0x0018)
	movx	@dptr,a
	mov	dptr,#(_SubwayCircle + 0x0019)
	movx	@dptr,a
	mov	dptr,#(_SubwayCircle + 0x001a)
	movx	@dptr,a
	mov	dptr,#(_SubwayCircle + 0x001b)
	mov	a,#0x08
	movx	@dptr,a
	mov	dptr,#(_SubwayCircle + 0x001c)
	clr	a
	movx	@dptr,a
	mov	dptr,#(_SubwayCircle + 0x001d)
	movx	@dptr,a
	mov	dptr,#(_SubwayCircle + 0x001e)
	movx	@dptr,a
	mov	dptr,#(_SubwayCircle + 0x001f)
	movx	@dptr,a
	mov	dptr,#(_SubwayCircle + 0x0020)
	movx	@dptr,a
	mov	dptr,#(_SubwayCircle + 0x0021)
	movx	@dptr,a
	mov	dptr,#(_SubwayCircle + 0x0022)
	movx	@dptr,a
	mov	dptr,#(_SubwayCircle + 0x0023)
	movx	@dptr,a
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME    (CODE)
	.area HOME    (CODE)
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CSEG    (CODE)
;------------------------------------------------------------
;Allocation info for local variables in function 'LIGHT_ON'
;------------------------------------------------------------
;LIGHT                     Allocated to registers r7 
;------------------------------------------------------------
;	source/CH549_SubwayBoard.c:28: void LIGHT_ON(unsigned char LIGHT)
;	-----------------------------------------
;	 function LIGHT_ON
;	-----------------------------------------
_LIGHT_ON:
	ar7 = 0x07
	ar6 = 0x06
	ar5 = 0x05
	ar4 = 0x04
	ar3 = 0x03
	ar2 = 0x02
	ar1 = 0x01
	ar0 = 0x00
;	source/CH549_SubwayBoard.c:30: switch (LIGHT)
	mov	a,dpl
	mov	r7,a
	add	a,#0xff - 0x14
	jnc	00128$
	ret
00128$:
	mov	a,r7
	add	a,#(00129$-3-.)
	movc	a,@a+pc
	mov	dpl,a
	mov	a,r7
	add	a,#(00130$-3-.)
	movc	a,@a+pc
	mov	dph,a
	clr	a
	jmp	@a+dptr
00129$:
	.db	00122$
	.db	00101$
	.db	00102$
	.db	00103$
	.db	00104$
	.db	00105$
	.db	00106$
	.db	00107$
	.db	00108$
	.db	00109$
	.db	00110$
	.db	00111$
	.db	00112$
	.db	00113$
	.db	00114$
	.db	00115$
	.db	00116$
	.db	00117$
	.db	00118$
	.db	00119$
	.db	00120$
00130$:
	.db	00122$>>8
	.db	00101$>>8
	.db	00102$>>8
	.db	00103$>>8
	.db	00104$>>8
	.db	00105$>>8
	.db	00106$>>8
	.db	00107$>>8
	.db	00108$>>8
	.db	00109$>>8
	.db	00110$>>8
	.db	00111$>>8
	.db	00112$>>8
	.db	00113$>>8
	.db	00114$>>8
	.db	00115$>>8
	.db	00116$>>8
	.db	00117$>>8
	.db	00118$>>8
	.db	00119$>>8
	.db	00120$>>8
;	source/CH549_SubwayBoard.c:32: case 1:LED_A1 = 1;LED_B1 = 0;break;
00101$:
;	assignBit
	setb	_P3_5
;	assignBit
	clr	_P1_1
	ret
;	source/CH549_SubwayBoard.c:33: case 2:LED_A2 = 1;LED_B1 = 0;break;
00102$:
;	assignBit
	setb	_P2_2
;	assignBit
	clr	_P1_1
;	source/CH549_SubwayBoard.c:34: case 3:LED_A3 = 1;LED_B1 = 0;break;
	ret
00103$:
;	assignBit
	setb	_P2_4
;	assignBit
	clr	_P1_1
;	source/CH549_SubwayBoard.c:35: case 4:LED_A4 = 1;LED_B1 = 0;break;
	ret
00104$:
;	assignBit
	setb	_P2_6
;	assignBit
	clr	_P1_1
;	source/CH549_SubwayBoard.c:36: case 5:LED_A5 = 1;LED_B1 = 0;break;
	ret
00105$:
;	assignBit
	setb	_P2_7
;	assignBit
	clr	_P1_1
;	source/CH549_SubwayBoard.c:37: case 6:LED_A1 = 1;LED_B2 = 0;break;
	ret
00106$:
;	assignBit
	setb	_P3_5
;	assignBit
	clr	_P1_4
;	source/CH549_SubwayBoard.c:38: case 7:LED_A2 = 1;LED_B2 = 0;break;
	ret
00107$:
;	assignBit
	setb	_P2_2
;	assignBit
	clr	_P1_4
;	source/CH549_SubwayBoard.c:39: case 8:LED_A3 = 1;LED_B2 = 0;break;
	ret
00108$:
;	assignBit
	setb	_P2_4
;	assignBit
	clr	_P1_4
;	source/CH549_SubwayBoard.c:40: case 9:LED_A4 = 1;LED_B2 = 0;break;
	ret
00109$:
;	assignBit
	setb	_P2_6
;	assignBit
	clr	_P1_4
;	source/CH549_SubwayBoard.c:41: case 10:LED_A5 = 1;LED_B2 = 0;break;
	ret
00110$:
;	assignBit
	setb	_P2_7
;	assignBit
	clr	_P1_4
;	source/CH549_SubwayBoard.c:42: case 11:LED_A1 = 1;LED_B3 = 0;break;
	ret
00111$:
;	assignBit
	setb	_P3_5
;	assignBit
	clr	_P1_5
;	source/CH549_SubwayBoard.c:43: case 12:LED_A2 = 1;LED_B3 = 0;break;
	ret
00112$:
;	assignBit
	setb	_P2_2
;	assignBit
	clr	_P1_5
;	source/CH549_SubwayBoard.c:44: case 13:LED_A3 = 1;LED_B3 = 0;break;
	ret
00113$:
;	assignBit
	setb	_P2_4
;	assignBit
	clr	_P1_5
;	source/CH549_SubwayBoard.c:45: case 14:LED_A4 = 1;LED_B3 = 0;break;
	ret
00114$:
;	assignBit
	setb	_P2_6
;	assignBit
	clr	_P1_5
;	source/CH549_SubwayBoard.c:46: case 15:LED_A5 = 1;LED_B3 = 0;break;
	ret
00115$:
;	assignBit
	setb	_P2_7
;	assignBit
	clr	_P1_5
;	source/CH549_SubwayBoard.c:47: case 16:LED_A1 = 1;LED_B4 = 0;break;
	ret
00116$:
;	assignBit
	setb	_P3_5
;	assignBit
	clr	_P1_6
;	source/CH549_SubwayBoard.c:48: case 17:LED_A2 = 1;LED_B4 = 0;break;
	ret
00117$:
;	assignBit
	setb	_P2_2
;	assignBit
	clr	_P1_6
;	source/CH549_SubwayBoard.c:49: case 18:LED_A3 = 1;LED_B4 = 0;break;
	ret
00118$:
;	assignBit
	setb	_P2_4
;	assignBit
	clr	_P1_6
;	source/CH549_SubwayBoard.c:50: case 19:LED_A4 = 1;LED_B4 = 0;break;
	ret
00119$:
;	assignBit
	setb	_P2_6
;	assignBit
	clr	_P1_6
;	source/CH549_SubwayBoard.c:51: case 20:LED_A5 = 1;LED_B4 = 0;break;
	ret
00120$:
;	assignBit
	setb	_P2_7
;	assignBit
	clr	_P1_6
;	source/CH549_SubwayBoard.c:52: }
00122$:
;	source/CH549_SubwayBoard.c:53: }
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'LIGHT_OFF'
;------------------------------------------------------------
;LIGHT                     Allocated to registers r7 
;------------------------------------------------------------
;	source/CH549_SubwayBoard.c:56: void LIGHT_OFF(unsigned char LIGHT)
;	-----------------------------------------
;	 function LIGHT_OFF
;	-----------------------------------------
_LIGHT_OFF:
;	source/CH549_SubwayBoard.c:58: switch (LIGHT)
	mov	a,dpl
	mov	r7,a
	add	a,#0xff - 0x14
	jnc	00128$
	ret
00128$:
	mov	a,r7
	add	a,#(00129$-3-.)
	movc	a,@a+pc
	mov	dpl,a
	mov	a,r7
	add	a,#(00130$-3-.)
	movc	a,@a+pc
	mov	dph,a
	clr	a
	jmp	@a+dptr
00129$:
	.db	00122$
	.db	00101$
	.db	00102$
	.db	00103$
	.db	00104$
	.db	00105$
	.db	00106$
	.db	00107$
	.db	00108$
	.db	00109$
	.db	00110$
	.db	00111$
	.db	00112$
	.db	00113$
	.db	00114$
	.db	00115$
	.db	00116$
	.db	00117$
	.db	00118$
	.db	00119$
	.db	00120$
00130$:
	.db	00122$>>8
	.db	00101$>>8
	.db	00102$>>8
	.db	00103$>>8
	.db	00104$>>8
	.db	00105$>>8
	.db	00106$>>8
	.db	00107$>>8
	.db	00108$>>8
	.db	00109$>>8
	.db	00110$>>8
	.db	00111$>>8
	.db	00112$>>8
	.db	00113$>>8
	.db	00114$>>8
	.db	00115$>>8
	.db	00116$>>8
	.db	00117$>>8
	.db	00118$>>8
	.db	00119$>>8
	.db	00120$>>8
;	source/CH549_SubwayBoard.c:60: case 1:LED_A1 = 0;LED_B1 = 1;break;
00101$:
;	assignBit
	clr	_P3_5
;	assignBit
	setb	_P1_1
	ret
;	source/CH549_SubwayBoard.c:61: case 2:LED_A2 = 0;LED_B1 = 1;break;
00102$:
;	assignBit
	clr	_P2_2
;	assignBit
	setb	_P1_1
;	source/CH549_SubwayBoard.c:62: case 3:LED_A3 = 0;LED_B1 = 1;break;
	ret
00103$:
;	assignBit
	clr	_P2_4
;	assignBit
	setb	_P1_1
;	source/CH549_SubwayBoard.c:63: case 4:LED_A4 = 0;LED_B1 = 1;break;
	ret
00104$:
;	assignBit
	clr	_P2_6
;	assignBit
	setb	_P1_1
;	source/CH549_SubwayBoard.c:64: case 5:LED_A5 = 0;LED_B1 = 1;break;
	ret
00105$:
;	assignBit
	clr	_P2_7
;	assignBit
	setb	_P1_1
;	source/CH549_SubwayBoard.c:65: case 6:LED_A1 = 0;LED_B2 = 1;break;
	ret
00106$:
;	assignBit
	clr	_P3_5
;	assignBit
	setb	_P1_4
;	source/CH549_SubwayBoard.c:66: case 7:LED_A2 = 0;LED_B2 = 1;break;
	ret
00107$:
;	assignBit
	clr	_P2_2
;	assignBit
	setb	_P1_4
;	source/CH549_SubwayBoard.c:67: case 8:LED_A3 = 0;LED_B2 = 1;break;
	ret
00108$:
;	assignBit
	clr	_P2_4
;	assignBit
	setb	_P1_4
;	source/CH549_SubwayBoard.c:68: case 9:LED_A4 = 0;LED_B2 = 1;break;
	ret
00109$:
;	assignBit
	clr	_P2_6
;	assignBit
	setb	_P1_4
;	source/CH549_SubwayBoard.c:69: case 10:LED_A5 = 0;LED_B2 = 1;break;
	ret
00110$:
;	assignBit
	clr	_P2_7
;	assignBit
	setb	_P1_4
;	source/CH549_SubwayBoard.c:70: case 11:LED_A1 = 0;LED_B3 = 1;break;
	ret
00111$:
;	assignBit
	clr	_P3_5
;	assignBit
	setb	_P1_5
;	source/CH549_SubwayBoard.c:71: case 12:LED_A2 = 0;LED_B3 = 1;break;
	ret
00112$:
;	assignBit
	clr	_P2_2
;	assignBit
	setb	_P1_5
;	source/CH549_SubwayBoard.c:72: case 13:LED_A3 = 0;LED_B3 = 1;break;
	ret
00113$:
;	assignBit
	clr	_P2_4
;	assignBit
	setb	_P1_5
;	source/CH549_SubwayBoard.c:73: case 14:LED_A4 = 0;LED_B3 = 1;break;
	ret
00114$:
;	assignBit
	clr	_P2_6
;	assignBit
	setb	_P1_5
;	source/CH549_SubwayBoard.c:74: case 15:LED_A5 = 0;LED_B3 = 1;break;
	ret
00115$:
;	assignBit
	clr	_P2_7
;	assignBit
	setb	_P1_5
;	source/CH549_SubwayBoard.c:75: case 16:LED_A1 = 0;LED_B4 = 1;break;
	ret
00116$:
;	assignBit
	clr	_P3_5
;	assignBit
	setb	_P1_6
;	source/CH549_SubwayBoard.c:76: case 17:LED_A2 = 0;LED_B4 = 1;break;
	ret
00117$:
;	assignBit
	clr	_P2_2
;	assignBit
	setb	_P1_6
;	source/CH549_SubwayBoard.c:77: case 18:LED_A3 = 0;LED_B4 = 1;break;
	ret
00118$:
;	assignBit
	clr	_P2_4
;	assignBit
	setb	_P1_6
;	source/CH549_SubwayBoard.c:78: case 19:LED_A4 = 0;LED_B4 = 1;break;
	ret
00119$:
;	assignBit
	clr	_P2_6
;	assignBit
	setb	_P1_6
;	source/CH549_SubwayBoard.c:79: case 20:LED_A5 = 0;LED_B4 = 1;break;
	ret
00120$:
;	assignBit
	clr	_P2_7
;	assignBit
	setb	_P1_6
;	source/CH549_SubwayBoard.c:80: }
00122$:
;	source/CH549_SubwayBoard.c:81: }
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'LIGHT_Flash'
;------------------------------------------------------------
;LIGHT                     Allocated to registers r7 
;------------------------------------------------------------
;	source/CH549_SubwayBoard.c:84: void LIGHT_Flash(UINT8 LIGHT)
;	-----------------------------------------
;	 function LIGHT_Flash
;	-----------------------------------------
_LIGHT_Flash:
;	source/CH549_SubwayBoard.c:86: LIGHT_ON(LIGHT);
	mov  r7,dpl
	push	ar7
	lcall	_LIGHT_ON
;	source/CH549_SubwayBoard.c:87: mDelayuS(100);
	mov	dptr,#0x0064
	lcall	_mDelayuS
	pop	ar7
;	source/CH549_SubwayBoard.c:88: LIGHT_OFF(LIGHT);
	mov	dpl,r7
;	source/CH549_SubwayBoard.c:90: }
	ljmp	_LIGHT_OFF
;------------------------------------------------------------
;Allocation info for local variables in function 'LIGHT_Clean'
;------------------------------------------------------------
;LIGHT                     Allocated to registers r7 
;------------------------------------------------------------
;	source/CH549_SubwayBoard.c:93: void LIGHT_Clean()
;	-----------------------------------------
;	 function LIGHT_Clean
;	-----------------------------------------
_LIGHT_Clean:
;	source/CH549_SubwayBoard.c:95: for (UINT8 LIGHT = 1; LIGHT < 21; LIGHT++)
	mov	r7,#0x01
00103$:
	cjne	r7,#0x15,00116$
00116$:
	jnc	00105$
;	source/CH549_SubwayBoard.c:97: LIGHT_OFF(LIGHT);
	mov	dpl,r7
	push	ar7
	lcall	_LIGHT_OFF
	pop	ar7
;	source/CH549_SubwayBoard.c:95: for (UINT8 LIGHT = 1; LIGHT < 21; LIGHT++)
	inc	r7
	sjmp	00103$
00105$:
;	source/CH549_SubwayBoard.c:100: }
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'LIGHT_AllOn'
;------------------------------------------------------------
;LIGHT                     Allocated to registers r7 
;------------------------------------------------------------
;	source/CH549_SubwayBoard.c:103: void LIGHT_AllOn()
;	-----------------------------------------
;	 function LIGHT_AllOn
;	-----------------------------------------
_LIGHT_AllOn:
;	source/CH549_SubwayBoard.c:105: for (UINT8 LIGHT = 1; LIGHT < 21; LIGHT++)
	mov	r7,#0x01
00103$:
	cjne	r7,#0x15,00116$
00116$:
	jnc	00105$
;	source/CH549_SubwayBoard.c:107: LIGHT_ON(LIGHT);
	mov	dpl,r7
	push	ar7
	lcall	_LIGHT_ON
	pop	ar7
;	source/CH549_SubwayBoard.c:105: for (UINT8 LIGHT = 1; LIGHT < 21; LIGHT++)
	inc	r7
	sjmp	00103$
00105$:
;	source/CH549_SubwayBoard.c:110: }
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'LIGHT_TLineOn'
;------------------------------------------------------------
;DOT                       Allocated to registers r2 
;LINE                      Allocated to registers r7 
;------------------------------------------------------------
;	source/CH549_SubwayBoard.c:113: void LIGHT_TLineOn()
;	-----------------------------------------
;	 function LIGHT_TLineOn
;	-----------------------------------------
_LIGHT_TLineOn:
;	source/CH549_SubwayBoard.c:116: for ( LINE = 0; LINE < 6; LINE++)
	mov	r7,#0x00
;	source/CH549_SubwayBoard.c:118: for ( DOT = 0; DOT < 7; DOT++)
00112$:
	mov	a,r7
	mov	b,#0x07
	mul	ab
	mov	r5,a
	mov	r6,b
	add	a,#_SubwayLine
	mov	r3,a
	mov	a,r6
	addc	a,#(_SubwayLine >> 8)
	mov	r4,a
	mov	r2,#0x00
00105$:
;	source/CH549_SubwayBoard.c:120: if (SubwayLine[LINE][DOT])
	mov	a,r2
	add	a,r3
	mov	r0,a
	clr	a
	addc	a,r4
	mov	r1,a
	mov	dpl,r0
	mov	dph,r1
	movx	a,@dptr
	jz	00106$
;	source/CH549_SubwayBoard.c:122: LIGHT_ON(SubwayLine[LINE][DOT]);
	push	ar3
	push	ar4
	mov	a,r5
	add	a,#_SubwayLine
	mov	r0,a
	mov	a,r6
	addc	a,#(_SubwayLine >> 8)
	mov	r1,a
	mov	a,r2
	add	a,r0
	mov	r0,a
	clr	a
	addc	a,r1
	mov	r1,a
	mov	dpl,r0
	mov	dph,r1
	movx	a,@dptr
	mov	r4,a
	mov	dpl,a
	push	ar7
	push	ar6
	push	ar5
	push	ar4
	push	ar3
	push	ar2
	push	ar1
	push	ar0
	lcall	_LIGHT_ON
;	source/CH549_SubwayBoard.c:123: mDelaymS(50);
	mov	dptr,#0x0032
	lcall	_mDelaymS
	pop	ar0
	pop	ar1
;	source/CH549_SubwayBoard.c:124: LIGHT_OFF(SubwayLine[LINE][DOT]);
	mov	dpl,r0
	mov	dph,r1
	movx	a,@dptr
	mov	dpl,a
	lcall	_LIGHT_OFF
	pop	ar2
	pop	ar3
	pop	ar4
	pop	ar5
	pop	ar6
	pop	ar7
;	source/CH549_SubwayBoard.c:116: for ( LINE = 0; LINE < 6; LINE++)
	pop	ar4
	pop	ar3
;	source/CH549_SubwayBoard.c:124: LIGHT_OFF(SubwayLine[LINE][DOT]);
00106$:
;	source/CH549_SubwayBoard.c:118: for ( DOT = 0; DOT < 7; DOT++)
	inc	r2
	cjne	r2,#0x07,00130$
00130$:
	jc	00105$
;	source/CH549_SubwayBoard.c:116: for ( LINE = 0; LINE < 6; LINE++)
	inc	r7
	cjne	r7,#0x06,00132$
00132$:
	jc	00112$
;	source/CH549_SubwayBoard.c:132: }
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'LIGHT_FLineOn'
;------------------------------------------------------------
;DOT                       Allocated to registers r2 
;LINE                      Allocated to registers r7 
;------------------------------------------------------------
;	source/CH549_SubwayBoard.c:135: void LIGHT_FLineOn()
;	-----------------------------------------
;	 function LIGHT_FLineOn
;	-----------------------------------------
_LIGHT_FLineOn:
;	source/CH549_SubwayBoard.c:138: for ( LINE = 0; LINE < 6; LINE++)
	mov	r7,#0x00
;	source/CH549_SubwayBoard.c:140: for ( DOT = 0; DOT < 7; DOT++)
00112$:
	mov	a,r7
	mov	b,#0x07
	mul	ab
	mov	r5,a
	mov	r6,b
	add	a,#_SubwayLine
	mov	r3,a
	mov	a,r6
	addc	a,#(_SubwayLine >> 8)
	mov	r4,a
	mov	r2,#0x00
00105$:
;	source/CH549_SubwayBoard.c:142: if (SubwayLine[LINE][6-DOT])
	push	ar5
	push	ar6
	mov	ar1,r2
	mov	a,#0x06
	clr	c
	subb	a,r1
	add	a,r3
	mov	r0,a
	clr	a
	addc	a,r4
	mov	r6,a
	mov	dpl,r0
	mov	dph,r6
	movx	a,@dptr
	pop	ar6
	pop	ar5
	jz	00106$
;	source/CH549_SubwayBoard.c:144: LIGHT_ON(SubwayLine[LINE][6-DOT]);
	push	ar3
	push	ar4
	mov	a,r5
	add	a,#_SubwayLine
	mov	r0,a
	mov	a,r6
	addc	a,#(_SubwayLine >> 8)
	mov	r4,a
	mov	a,#0x06
	clr	c
	subb	a,r1
	add	a,r0
	mov	r0,a
	clr	a
	addc	a,r4
	mov	r4,a
	mov	dpl,r0
	mov	dph,r4
	movx	a,@dptr
	mov	r3,a
	mov	dpl,a
	push	ar7
	push	ar6
	push	ar5
	push	ar4
	push	ar3
	push	ar2
	push	ar0
	lcall	_LIGHT_ON
;	source/CH549_SubwayBoard.c:145: mDelaymS(50);
	mov	dptr,#0x0032
	lcall	_mDelaymS
	pop	ar0
	pop	ar2
	pop	ar3
	pop	ar4
;	source/CH549_SubwayBoard.c:146: LIGHT_OFF(SubwayLine[LINE][6-DOT]);
	mov	dpl,r0
	mov	dph,r4
	movx	a,@dptr
	mov	dpl,a
	push	ar4
	push	ar3
	push	ar2
	lcall	_LIGHT_OFF
	pop	ar2
	pop	ar3
	pop	ar4
	pop	ar5
	pop	ar6
	pop	ar7
;	source/CH549_SubwayBoard.c:138: for ( LINE = 0; LINE < 6; LINE++)
	pop	ar4
	pop	ar3
;	source/CH549_SubwayBoard.c:146: LIGHT_OFF(SubwayLine[LINE][6-DOT]);
00106$:
;	source/CH549_SubwayBoard.c:140: for ( DOT = 0; DOT < 7; DOT++)
	inc	r2
	cjne	r2,#0x07,00130$
00130$:
	jc	00105$
;	source/CH549_SubwayBoard.c:138: for ( LINE = 0; LINE < 6; LINE++)
	inc	r7
	cjne	r7,#0x06,00132$
00132$:
	jnc	00133$
	ljmp	00112$
00133$:
;	source/CH549_SubwayBoard.c:154: }
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'LIGHT_Converge'
;------------------------------------------------------------
;FREQs                     Allocated to stack - _bp +7
;TIME                      Allocated to stack - _bp +12
;DOT                       Allocated to registers r7 
;CIRCLE                    Allocated to stack - _bp +9
;FREQ                      Allocated to stack - _bp +10
;sloc0                     Allocated to stack - _bp +1
;sloc1                     Allocated to stack - _bp +25
;sloc2                     Allocated to stack - _bp +3
;------------------------------------------------------------
;	source/CH549_SubwayBoard.c:157: void LIGHT_Converge(UINT16 FREQs)
;	-----------------------------------------
;	 function LIGHT_Converge
;	-----------------------------------------
_LIGHT_Converge:
	push	_bp
	mov	a,sp
	mov	_bp,a
	add	a,#0x1b
	mov	sp,a
	xch	a,r0
	mov	a,_bp
	add	a,#0x07
	xch	a,r0
	mov	@r0,dpl
	inc	r0
	mov	@r0,dph
;	source/CH549_SubwayBoard.c:159: float TIME[4] = {1 ,1.8, 1.8, 9};
	mov	a,_bp
	add	a,#0x0c
	mov	r1,a
	mov	@r1,#0x00
	inc	r1
	mov	@r1,#0x00
	inc	r1
	mov	@r1,#0x80
	inc	r1
	mov	@r1,#0x3f
	dec	r1
	dec	r1
	dec	r1
	mov	a,#0x04
	add	a,r1
	mov	r0,a
	mov	@r0,#0x66
	inc	r0
	mov	@r0,#0x66
	inc	r0
	mov	@r0,#0xe6
	inc	r0
	mov	@r0,#0x3f
	mov	a,#0x08
	add	a,r1
	mov	r0,a
	mov	@r0,#0x66
	inc	r0
	mov	@r0,#0x66
	inc	r0
	mov	@r0,#0xe6
	inc	r0
	mov	@r0,#0x3f
	mov	a,#0x0c
	add	a,r1
	mov	r0,a
	mov	@r0,#0x00
	inc	r0
	mov	@r0,#0x00
	inc	r0
	mov	@r0,#0x10
	inc	r0
	mov	@r0,#0x41
;	source/CH549_SubwayBoard.c:163: for ( CIRCLE = 0; CIRCLE < 4; CIRCLE++)
	mov	a,_bp
	add	a,#0x09
	mov	r0,a
	mov	@r0,#0x00
;	source/CH549_SubwayBoard.c:165: for ( FREQ = 0; FREQ < FREQs * TIME[CIRCLE]; FREQ++)
00119$:
	mov	a,_bp
	add	a,#0x09
	mov	r0,a
	mov	a,@r0
	mov	b,#0x09
	mul	ab
	add	a,#_SubwayCircle
	mov	r3,a
	mov	a,#(_SubwayCircle >> 8)
	addc	a,b
	mov	r4,a
	mov	r0,_bp
	inc	r0
	mov	@r0,ar3
	inc	r0
	mov	@r0,ar4
	mov	a,_bp
	add	a,#0x09
	mov	r0,a
	mov	a,@r0
	add	a,@r0
	add	a,acc
	mov	r2,a
	add	a,r1
	mov	r0,a
	push	ar0
	mov	a,_bp
	add	a,#0x0a
	mov	r0,a
	clr	a
	mov	@r0,a
	inc	r0
	mov	@r0,a
	pop	ar0
00109$:
	push	ar1
	push	ar1
	mov	a,_bp
	add	a,#0x03
	mov	r1,a
	mov	a,@r0
	mov	@r1,a
	inc	r0
	mov	a,@r0
	inc	r1
	mov	@r1,a
	inc	r0
	mov	a,@r0
	inc	r1
	mov	@r1,a
	inc	r0
	mov	a,@r0
	inc	r1
	mov	@r1,a
	dec	r0
	dec	r0
	dec	r0
	pop	ar1
	push	ar0
	mov	a,_bp
	add	a,#0x07
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	pop	ar0
	push	ar4
	push	ar3
	push	ar1
	push	ar0
	lcall	___uint2fs
	mov	r2,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	pop	ar0
	push	ar0
	mov	b,ar0
	mov	a,_bp
	add	a,#0x03
	mov	r0,a
	mov	a,@r0
	push	acc
	inc	r0
	mov	a,@r0
	push	acc
	inc	r0
	mov	a,@r0
	push	acc
	inc	r0
	mov	a,@r0
	push	acc
	mov	dpl,r2
	mov	dph,r5
	mov	b,r6
	mov	a,r7
	lcall	___fsmul
	push	ar0
	mov	r0,_bp
	inc	r0
	inc	r0
	inc	r0
	mov	@r0,dpl
	inc	r0
	mov	@r0,dph
	inc	r0
	mov	@r0,b
	inc	r0
	mov	@r0,a
	pop	ar0
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	pop	ar0
	pop	ar1
	pop	ar3
	pop	ar4
	push	ar0
	mov	a,_bp
	add	a,#0x0a
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	pop	ar0
	push	ar4
	push	ar3
	push	ar1
	push	ar0
	lcall	___uint2fs
	mov	r2,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	pop	ar0
	push	ar0
	mov	b,ar0
	mov	a,_bp
	add	a,#0x03
	mov	r0,a
	mov	a,@r0
	push	acc
	inc	r0
	mov	a,@r0
	push	acc
	inc	r0
	mov	a,@r0
	push	acc
	inc	r0
	mov	a,@r0
	push	acc
	mov	dpl,r2
	mov	dph,r5
	mov	b,r6
	mov	a,r7
	lcall	___fslt
	mov	r7,dpl
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	pop	ar0
	pop	ar1
	pop	ar3
	pop	ar4
	pop	ar1
	mov	a,r7
	jz	00112$
;	source/CH549_SubwayBoard.c:167: for ( DOT = 0; DOT < 9; DOT++)
	mov	r7,#0x00
00106$:
;	source/CH549_SubwayBoard.c:169: if(SubwayCircle[CIRCLE][DOT])
	mov	a,r7
	add	a,r3
	mov	r5,a
	clr	a
	addc	a,r4
	mov	r6,a
	mov	dpl,r5
	mov	dph,r6
	movx	a,@dptr
	jz	00107$
;	source/CH549_SubwayBoard.c:171: LIGHT_Flash(SubwayCircle[CIRCLE][DOT]);
	push	ar0
	mov	r0,_bp
	inc	r0
	mov	a,r7
	add	a,@r0
	mov	dpl,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	dph,a
	pop	ar0
	movx	a,@dptr
	mov	dpl,a
	push	ar7
	push	ar4
	push	ar3
	push	ar1
	push	ar0
	lcall	_LIGHT_Flash
	pop	ar0
	pop	ar1
	pop	ar3
	pop	ar4
	pop	ar7
00107$:
;	source/CH549_SubwayBoard.c:167: for ( DOT = 0; DOT < 9; DOT++)
	inc	r7
	cjne	r7,#0x09,00144$
00144$:
	jc	00106$
;	source/CH549_SubwayBoard.c:165: for ( FREQ = 0; FREQ < FREQs * TIME[CIRCLE]; FREQ++)
	push	ar0
	mov	a,_bp
	add	a,#0x0a
	mov	r0,a
	inc	@r0
	cjne	@r0,#0x00,00146$
	inc	r0
	inc	@r0
00146$:
	pop	ar0
	ljmp	00109$
00112$:
;	source/CH549_SubwayBoard.c:163: for ( CIRCLE = 0; CIRCLE < 4; CIRCLE++)
	mov	a,_bp
	add	a,#0x09
	mov	r0,a
	inc	@r0
	mov	a,_bp
	add	a,#0x09
	mov	r0,a
	cjne	@r0,#0x04,00147$
00147$:
	jnc	00148$
	ljmp	00119$
00148$:
;	source/CH549_SubwayBoard.c:181: }
	mov	sp,_bp
	pop	_bp
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'LIGHT_Disperse'
;------------------------------------------------------------
;FREQs                     Allocated to stack - _bp +1
;TIME                      Allocated to stack - _bp +9
;DOT                       Allocated to registers r7 
;CIRCLE                    Allocated to stack - _bp +25
;FREQ                      Allocated to stack - _bp +7
;sloc0                     Allocated to stack - _bp +3
;------------------------------------------------------------
;	source/CH549_SubwayBoard.c:184: void LIGHT_Disperse(UINT16 FREQs)
;	-----------------------------------------
;	 function LIGHT_Disperse
;	-----------------------------------------
_LIGHT_Disperse:
	push	_bp
	mov	_bp,sp
	push	dpl
	push	dph
	mov	a,sp
	add	a,#0x17
	mov	sp,a
;	source/CH549_SubwayBoard.c:186: float TIME[4] = {1 ,1.8, 1.8, 9};
	mov	a,_bp
	add	a,#0x09
	mov	r1,a
	mov	@r1,#0x00
	inc	r1
	mov	@r1,#0x00
	inc	r1
	mov	@r1,#0x80
	inc	r1
	mov	@r1,#0x3f
	dec	r1
	dec	r1
	dec	r1
	mov	a,#0x04
	add	a,r1
	mov	r0,a
	mov	@r0,#0x66
	inc	r0
	mov	@r0,#0x66
	inc	r0
	mov	@r0,#0xe6
	inc	r0
	mov	@r0,#0x3f
	mov	a,#0x08
	add	a,r1
	mov	r0,a
	mov	@r0,#0x66
	inc	r0
	mov	@r0,#0x66
	inc	r0
	mov	@r0,#0xe6
	inc	r0
	mov	@r0,#0x3f
	mov	a,#0x0c
	add	a,r1
	mov	r0,a
	mov	@r0,#0x00
	inc	r0
	mov	@r0,#0x00
	inc	r0
	mov	@r0,#0x10
	inc	r0
	mov	@r0,#0x41
;	source/CH549_SubwayBoard.c:190: for ( CIRCLE = 0; CIRCLE < 4; CIRCLE++)
	mov	a,_bp
	add	a,#0x19
	mov	r0,a
	mov	@r0,#0x00
;	source/CH549_SubwayBoard.c:192: for ( FREQ = 0; FREQ < FREQs * TIME[3 - CIRCLE]; FREQ++)
00119$:
	mov	a,_bp
	add	a,#0x07
	mov	r0,a
	clr	a
	mov	@r0,a
	inc	r0
	mov	@r0,a
00109$:
	mov	a,_bp
	add	a,#0x19
	mov	r0,a
	mov	ar2,@r0
	mov	a,#0x03
	clr	c
	subb	a,r2
	add	a,acc
	add	a,acc
	add	a,r1
	mov	r0,a
	push	ar1
	push	ar1
	mov	a,_bp
	add	a,#0x03
	mov	r1,a
	mov	a,@r0
	mov	@r1,a
	inc	r0
	mov	a,@r0
	inc	r1
	mov	@r1,a
	inc	r0
	mov	a,@r0
	inc	r1
	mov	@r1,a
	inc	r0
	mov	a,@r0
	inc	r1
	mov	@r1,a
	dec	r0
	dec	r0
	dec	r0
	mov	r0,_bp
	inc	r0
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	lcall	___uint2fs
	mov	r3,dpl
	mov	r5,dph
	mov	r6,b
	mov	r7,a
	mov	a,_bp
	add	a,#0x03
	mov	r0,a
	mov	a,@r0
	push	acc
	inc	r0
	mov	a,@r0
	push	acc
	inc	r0
	mov	a,@r0
	push	acc
	inc	r0
	mov	a,@r0
	push	acc
	mov	dpl,r3
	mov	dph,r5
	mov	b,r6
	mov	a,r7
	lcall	___fsmul
	mov	r0,_bp
	inc	r0
	inc	r0
	inc	r0
	mov	@r0,dpl
	inc	r0
	mov	@r0,dph
	inc	r0
	mov	@r0,b
	inc	r0
	mov	@r0,a
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	mov	a,_bp
	add	a,#0x07
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	lcall	___uint2fs
	mov	r2,dpl
	mov	r3,dph
	mov	r6,b
	mov	r7,a
	mov	a,_bp
	add	a,#0x03
	mov	r0,a
	mov	a,@r0
	push	acc
	inc	r0
	mov	a,@r0
	push	acc
	inc	r0
	mov	a,@r0
	push	acc
	inc	r0
	mov	a,@r0
	push	acc
	mov	dpl,r2
	mov	dph,r3
	mov	b,r6
	mov	a,r7
	lcall	___fslt
	mov	r7,dpl
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	pop	ar1
	pop	ar1
	mov	a,r7
	jnz	00142$
	ljmp	00112$
00142$:
;	source/CH549_SubwayBoard.c:194: for ( DOT = 0; DOT < 9; DOT++)
	mov	r7,#0x00
00106$:
;	source/CH549_SubwayBoard.c:196: if(SubwayCircle[3 - CIRCLE][DOT])
	mov	a,_bp
	add	a,#0x19
	mov	r0,a
	mov	ar5,@r0
	mov	a,#0x03
	clr	c
	subb	a,r5
	mov	r6,a
	clr	F0
	mov	b,#0x09
	mov	a,r6
	jnb	acc.7,00143$
	cpl	F0
	cpl	a
	inc	a
00143$:
	mul	ab
	jnb	F0,00144$
	cpl	a
	add	a,#0x01
	xch	a,b
	cpl	a
	addc	a,#0x00
	xch	a,b
00144$:
	add	a,#_SubwayCircle
	mov	r4,a
	mov	a,#(_SubwayCircle >> 8)
	addc	a,b
	mov	r6,a
	mov	a,r7
	add	a,r4
	mov	r4,a
	clr	a
	addc	a,r6
	mov	r6,a
	mov	dpl,r4
	mov	dph,r6
	movx	a,@dptr
	jz	00107$
;	source/CH549_SubwayBoard.c:198: LIGHT_Flash(SubwayCircle[3 - CIRCLE][DOT]);
	mov	a,#0x03
	clr	c
	subb	a,r5
	mov	r5,a
	clr	F0
	mov	b,#0x09
	mov	a,r5
	jnb	acc.7,00146$
	cpl	F0
	cpl	a
	inc	a
00146$:
	mul	ab
	jnb	F0,00147$
	cpl	a
	add	a,#0x01
	xch	a,b
	cpl	a
	addc	a,#0x00
	xch	a,b
00147$:
	add	a,#_SubwayCircle
	mov	r5,a
	mov	a,#(_SubwayCircle >> 8)
	addc	a,b
	mov	r6,a
	mov	a,r7
	add	a,r5
	mov	dpl,a
	clr	a
	addc	a,r6
	mov	dph,a
	movx	a,@dptr
	mov	dpl,a
	push	ar7
	push	ar1
	lcall	_LIGHT_Flash
	pop	ar1
	pop	ar7
00107$:
;	source/CH549_SubwayBoard.c:194: for ( DOT = 0; DOT < 9; DOT++)
	inc	r7
	cjne	r7,#0x09,00148$
00148$:
	jnc	00149$
	ljmp	00106$
00149$:
;	source/CH549_SubwayBoard.c:192: for ( FREQ = 0; FREQ < FREQs * TIME[3 - CIRCLE]; FREQ++)
	mov	a,_bp
	add	a,#0x07
	mov	r0,a
	inc	@r0
	cjne	@r0,#0x00,00150$
	inc	r0
	inc	@r0
00150$:
	ljmp	00109$
00112$:
;	source/CH549_SubwayBoard.c:190: for ( CIRCLE = 0; CIRCLE < 4; CIRCLE++)
	mov	a,_bp
	add	a,#0x19
	mov	r0,a
	inc	@r0
	mov	a,_bp
	add	a,#0x19
	mov	r0,a
	cjne	@r0,#0x04,00151$
00151$:
	jnc	00152$
	ljmp	00119$
00152$:
;	source/CH549_SubwayBoard.c:208: }
	mov	sp,_bp
	pop	_bp
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'LIGHT_Random'
;------------------------------------------------------------
;FREQs                     Allocated to stack - _bp +1
;i                         Allocated to registers r6 
;m                         Allocated to registers r5 
;DOT                       Allocated to registers r2 
;sloc0                     Allocated to stack - _bp +2
;sloc1                     Allocated to stack - _bp +4
;------------------------------------------------------------
;	source/CH549_SubwayBoard.c:211: void LIGHT_Random(UINT8 FREQs)
;	-----------------------------------------
;	 function LIGHT_Random
;	-----------------------------------------
_LIGHT_Random:
	push	_bp
	mov	_bp,sp
	push	dpl
	mov	a,sp
	add	a,#0x04
	mov	sp,a
;	source/CH549_SubwayBoard.c:214: for ( i = 0; i < FREQs; i++)
	mov	r6,#0x00
00116$:
	mov	r0,_bp
	inc	r0
	clr	c
	mov	a,r6
	subb	a,@r0
	jc	00163$
	ljmp	00118$
00163$:
;	source/CH549_SubwayBoard.c:216: if (rand() % 2)
	push	ar6
	lcall	_rand
	mov	r4,dpl
	mov	r5,dph
	mov	a,#0x02
	push	acc
	clr	a
	push	acc
	mov	dpl,r4
	mov	dph,r5
	lcall	__modsint
	mov	r4,dpl
	mov	r5,dph
	dec	sp
	dec	sp
	pop	ar6
	mov	a,r4
	orl	a,r5
	jnz	00164$
	ljmp	00108$
00164$:
;	source/CH549_SubwayBoard.c:218: m = rand() % 6;
	push	ar6
	lcall	_rand
	mov	r4,dpl
	mov	r5,dph
	mov	a,#0x06
	push	acc
	clr	a
	push	acc
	mov	dpl,r4
	mov	dph,r5
	lcall	__modsint
	mov	r4,dpl
	dec	sp
	dec	sp
	pop	ar6
;	source/CH549_SubwayBoard.c:219: for ( DOT = 0; DOT < 7; DOT++)
	mov	a,r4
	mov	b,#0x07
	mul	ab
	mov	r4,a
	mov	r5,b
	mov	r0,_bp
	inc	r0
	inc	r0
	mov	a,r4
	add	a,#_SubwayLine
	mov	@r0,a
	mov	a,r5
	addc	a,#(_SubwayLine >> 8)
	inc	r0
	mov	@r0,a
	mov	r2,#0x00
00111$:
;	source/CH549_SubwayBoard.c:221: if (SubwayLine[m][DOT])
	push	ar6
	mov	r0,_bp
	inc	r0
	inc	r0
	mov	a,r2
	add	a,@r0
	mov	r6,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r7,a
	mov	dpl,r6
	mov	dph,r7
	movx	a,@dptr
	pop	ar6
	jz	00112$
;	source/CH549_SubwayBoard.c:223: LIGHT_ON(SubwayLine[m][DOT]);
	push	ar6
	mov	a,r4
	add	a,#_SubwayLine
	mov	r6,a
	mov	a,r5
	addc	a,#(_SubwayLine >> 8)
	mov	r7,a
	mov	a,r2
	add	a,r6
	mov	r6,a
	clr	a
	addc	a,r7
	mov	r7,a
	mov	dpl,r6
	mov	dph,r7
	movx	a,@dptr
	mov	dpl,a
	push	ar7
	push	ar6
	push	ar5
	push	ar4
	push	ar2
	lcall	_LIGHT_ON
;	source/CH549_SubwayBoard.c:224: mDelaymS(50);
	mov	dptr,#0x0032
	lcall	_mDelaymS
	pop	ar2
	pop	ar4
	pop	ar5
	pop	ar6
	pop	ar7
;	source/CH549_SubwayBoard.c:225: LIGHT_OFF(SubwayLine[m][DOT]);
	mov	dpl,r6
	mov	dph,r7
	movx	a,@dptr
	mov	r6,a
	mov	dpl,a
	push	ar6
	push	ar5
	push	ar4
	push	ar2
	lcall	_LIGHT_OFF
	pop	ar2
	pop	ar4
	pop	ar5
	pop	ar6
;	source/CH549_SubwayBoard.c:214: for ( i = 0; i < FREQs; i++)
	pop	ar6
;	source/CH549_SubwayBoard.c:225: LIGHT_OFF(SubwayLine[m][DOT]);
00112$:
;	source/CH549_SubwayBoard.c:219: for ( DOT = 0; DOT < 7; DOT++)
	inc	r2
	cjne	r2,#0x07,00166$
00166$:
	jc	00111$
	ljmp	00109$
00108$:
;	source/CH549_SubwayBoard.c:232: m = rand() % 6;
	push	ar6
	lcall	_rand
	mov	r5,dpl
	mov	r7,dph
	mov	a,#0x06
	push	acc
	clr	a
	push	acc
	mov	dpl,r5
	mov	dph,r7
	lcall	__modsint
	mov	r5,dpl
	dec	sp
	dec	sp
	pop	ar6
;	source/CH549_SubwayBoard.c:233: for ( DOT = 0; DOT < 7; DOT++)
	mov	a,r5
	mov	b,#0x07
	mul	ab
	mov	r5,a
	mov	r7,b
	mov	a,_bp
	add	a,#0x04
	mov	r0,a
	mov	a,r5
	add	a,#_SubwayLine
	mov	@r0,a
	mov	a,r7
	addc	a,#(_SubwayLine >> 8)
	inc	r0
	mov	@r0,a
	mov	r3,#0x00
00113$:
;	source/CH549_SubwayBoard.c:235: if (SubwayLine[m][6 - DOT])
	push	ar6
	mov	ar4,r3
	mov	a,#0x06
	clr	c
	subb	a,r4
	xch	a,r0
	mov	a,_bp
	add	a,#0x04
	xch	a,r0
	add	a,@r0
	mov	r2,a
	inc	r0
	clr	a
	addc	a,@r0
	mov	r6,a
	mov	dpl,r2
	mov	dph,r6
	movx	a,@dptr
	pop	ar6
	jz	00114$
;	source/CH549_SubwayBoard.c:237: LIGHT_ON(SubwayLine[m][6 - DOT]);
	push	ar6
	mov	a,r5
	add	a,#_SubwayLine
	mov	r2,a
	mov	a,r7
	addc	a,#(_SubwayLine >> 8)
	mov	r6,a
	mov	a,#0x06
	clr	c
	subb	a,r4
	add	a,r2
	mov	r2,a
	clr	a
	addc	a,r6
	mov	r6,a
	mov	dpl,r2
	mov	dph,r6
	movx	a,@dptr
	mov	dpl,a
	push	ar7
	push	ar6
	push	ar5
	push	ar3
	push	ar2
	lcall	_LIGHT_ON
;	source/CH549_SubwayBoard.c:238: mDelaymS(50);
	mov	dptr,#0x0032
	lcall	_mDelaymS
	pop	ar2
	pop	ar3
	pop	ar5
	pop	ar6
;	source/CH549_SubwayBoard.c:239: LIGHT_OFF(SubwayLine[m][6 - DOT]);
	mov	dpl,r2
	mov	dph,r6
	movx	a,@dptr
	mov	dpl,a
	push	ar6
	push	ar5
	push	ar3
	lcall	_LIGHT_OFF
	pop	ar3
	pop	ar5
	pop	ar6
	pop	ar7
;	source/CH549_SubwayBoard.c:214: for ( i = 0; i < FREQs; i++)
	pop	ar6
;	source/CH549_SubwayBoard.c:239: LIGHT_OFF(SubwayLine[m][6 - DOT]);
00114$:
;	source/CH549_SubwayBoard.c:233: for ( DOT = 0; DOT < 7; DOT++)
	inc	r3
	cjne	r3,#0x07,00169$
00169$:
	jc	00113$
00109$:
;	source/CH549_SubwayBoard.c:246: mDelaymS(150);
	mov	dptr,#0x0096
	push	ar6
	lcall	_mDelaymS
	pop	ar6
;	source/CH549_SubwayBoard.c:214: for ( i = 0; i < FREQs; i++)
	inc	r6
	ljmp	00116$
00118$:
;	source/CH549_SubwayBoard.c:250: }
	mov	sp,_bp
	pop	_bp
	ret
	.area CSEG    (CODE)
	.area CONST   (CODE)
	.area CABS    (ABS,CODE)
