                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 4.0.0 #11528 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module main
                                      6 	.optsdcc -mmcs51 --model-small
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _Send_2811_24bits_PARM_3
                                     12 	.globl _Send_2811_24bits_PARM_2
                                     13 	.globl _main
                                     14 	.globl _Set_Light
                                     15 	.globl _Set_Colour
                                     16 	.globl _RGB_Rst
                                     17 	.globl _Send_2811_24bits
                                     18 	.globl _ADC_ChSelect
                                     19 	.globl _ADC_ExInit
                                     20 	.globl _mDelaymS
                                     21 	.globl _mDelayuS
                                     22 	.globl _CfgFsys
                                     23 	.globl _UIF_BUS_RST
                                     24 	.globl _UIF_DETECT
                                     25 	.globl _UIF_TRANSFER
                                     26 	.globl _UIF_SUSPEND
                                     27 	.globl _UIF_HST_SOF
                                     28 	.globl _UIF_FIFO_OV
                                     29 	.globl _U_SIE_FREE
                                     30 	.globl _U_TOG_OK
                                     31 	.globl _U_IS_NAK
                                     32 	.globl _S0_R_FIFO
                                     33 	.globl _S0_T_FIFO
                                     34 	.globl _S0_FREE
                                     35 	.globl _S0_IF_BYTE
                                     36 	.globl _S0_IF_FIRST
                                     37 	.globl _S0_IF_OV
                                     38 	.globl _S0_FST_ACT
                                     39 	.globl _CP_RL2
                                     40 	.globl _C_T2
                                     41 	.globl _TR2
                                     42 	.globl _EXEN2
                                     43 	.globl _TCLK
                                     44 	.globl _RCLK
                                     45 	.globl _EXF2
                                     46 	.globl _CAP1F
                                     47 	.globl _TF2
                                     48 	.globl _RI
                                     49 	.globl _TI
                                     50 	.globl _RB8
                                     51 	.globl _TB8
                                     52 	.globl _REN
                                     53 	.globl _SM2
                                     54 	.globl _SM1
                                     55 	.globl _SM0
                                     56 	.globl _IT0
                                     57 	.globl _IE0
                                     58 	.globl _IT1
                                     59 	.globl _IE1
                                     60 	.globl _TR0
                                     61 	.globl _TF0
                                     62 	.globl _TR1
                                     63 	.globl _TF1
                                     64 	.globl _XI
                                     65 	.globl _XO
                                     66 	.globl _P4_0
                                     67 	.globl _P4_1
                                     68 	.globl _P4_2
                                     69 	.globl _P4_3
                                     70 	.globl _P4_4
                                     71 	.globl _P4_5
                                     72 	.globl _P4_6
                                     73 	.globl _RXD
                                     74 	.globl _TXD
                                     75 	.globl _INT0
                                     76 	.globl _INT1
                                     77 	.globl _T0
                                     78 	.globl _T1
                                     79 	.globl _CAP0
                                     80 	.globl _INT3
                                     81 	.globl _P3_0
                                     82 	.globl _P3_1
                                     83 	.globl _P3_2
                                     84 	.globl _P3_3
                                     85 	.globl _P3_4
                                     86 	.globl _P3_5
                                     87 	.globl _P3_6
                                     88 	.globl _P3_7
                                     89 	.globl _PWM5
                                     90 	.globl _PWM4
                                     91 	.globl _INT0_
                                     92 	.globl _PWM3
                                     93 	.globl _PWM2
                                     94 	.globl _CAP1_
                                     95 	.globl _T2_
                                     96 	.globl _PWM1
                                     97 	.globl _CAP2_
                                     98 	.globl _T2EX_
                                     99 	.globl _PWM0
                                    100 	.globl _RXD1
                                    101 	.globl _PWM6
                                    102 	.globl _TXD1
                                    103 	.globl _PWM7
                                    104 	.globl _P2_0
                                    105 	.globl _P2_1
                                    106 	.globl _P2_2
                                    107 	.globl _P2_3
                                    108 	.globl _P2_4
                                    109 	.globl _P2_5
                                    110 	.globl _P2_6
                                    111 	.globl _P2_7
                                    112 	.globl _AIN0
                                    113 	.globl _CAP1
                                    114 	.globl _T2
                                    115 	.globl _AIN1
                                    116 	.globl _CAP2
                                    117 	.globl _T2EX
                                    118 	.globl _AIN2
                                    119 	.globl _AIN3
                                    120 	.globl _AIN4
                                    121 	.globl _UCC1
                                    122 	.globl _SCS
                                    123 	.globl _AIN5
                                    124 	.globl _UCC2
                                    125 	.globl _PWM0_
                                    126 	.globl _MOSI
                                    127 	.globl _AIN6
                                    128 	.globl _VBUS
                                    129 	.globl _RXD1_
                                    130 	.globl _MISO
                                    131 	.globl _AIN7
                                    132 	.globl _TXD1_
                                    133 	.globl _SCK
                                    134 	.globl _P1_0
                                    135 	.globl _P1_1
                                    136 	.globl _P1_2
                                    137 	.globl _P1_3
                                    138 	.globl _P1_4
                                    139 	.globl _P1_5
                                    140 	.globl _P1_6
                                    141 	.globl _P1_7
                                    142 	.globl _AIN8
                                    143 	.globl _AIN9
                                    144 	.globl _AIN10
                                    145 	.globl _RXD_
                                    146 	.globl _AIN11
                                    147 	.globl _TXD_
                                    148 	.globl _AIN12
                                    149 	.globl _RXD2
                                    150 	.globl _AIN13
                                    151 	.globl _TXD2
                                    152 	.globl _AIN14
                                    153 	.globl _RXD3
                                    154 	.globl _AIN15
                                    155 	.globl _TXD3
                                    156 	.globl _P0_0
                                    157 	.globl _P0_1
                                    158 	.globl _P0_2
                                    159 	.globl _P0_3
                                    160 	.globl _P0_4
                                    161 	.globl _P0_5
                                    162 	.globl _P0_6
                                    163 	.globl _P0_7
                                    164 	.globl _IE_SPI0
                                    165 	.globl _IE_INT3
                                    166 	.globl _IE_USB
                                    167 	.globl _IE_UART2
                                    168 	.globl _IE_ADC
                                    169 	.globl _IE_UART1
                                    170 	.globl _IE_UART3
                                    171 	.globl _IE_PWMX
                                    172 	.globl _IE_GPIO
                                    173 	.globl _IE_WDOG
                                    174 	.globl _PX0
                                    175 	.globl _PT0
                                    176 	.globl _PX1
                                    177 	.globl _PT1
                                    178 	.globl _PS
                                    179 	.globl _PT2
                                    180 	.globl _PL_FLAG
                                    181 	.globl _PH_FLAG
                                    182 	.globl _EX0
                                    183 	.globl _ET0
                                    184 	.globl _EX1
                                    185 	.globl _ET1
                                    186 	.globl _ES
                                    187 	.globl _ET2
                                    188 	.globl _E_DIS
                                    189 	.globl _EA
                                    190 	.globl _P
                                    191 	.globl _F1
                                    192 	.globl _OV
                                    193 	.globl _RS0
                                    194 	.globl _RS1
                                    195 	.globl _F0
                                    196 	.globl _AC
                                    197 	.globl _CY
                                    198 	.globl _UEP1_DMA_H
                                    199 	.globl _UEP1_DMA_L
                                    200 	.globl _UEP1_DMA
                                    201 	.globl _UEP0_DMA_H
                                    202 	.globl _UEP0_DMA_L
                                    203 	.globl _UEP0_DMA
                                    204 	.globl _UEP2_3_MOD
                                    205 	.globl _UEP4_1_MOD
                                    206 	.globl _UEP3_DMA_H
                                    207 	.globl _UEP3_DMA_L
                                    208 	.globl _UEP3_DMA
                                    209 	.globl _UEP2_DMA_H
                                    210 	.globl _UEP2_DMA_L
                                    211 	.globl _UEP2_DMA
                                    212 	.globl _USB_DEV_AD
                                    213 	.globl _USB_CTRL
                                    214 	.globl _USB_INT_EN
                                    215 	.globl _UEP4_T_LEN
                                    216 	.globl _UEP4_CTRL
                                    217 	.globl _UEP0_T_LEN
                                    218 	.globl _UEP0_CTRL
                                    219 	.globl _USB_RX_LEN
                                    220 	.globl _USB_MIS_ST
                                    221 	.globl _USB_INT_ST
                                    222 	.globl _USB_INT_FG
                                    223 	.globl _UEP3_T_LEN
                                    224 	.globl _UEP3_CTRL
                                    225 	.globl _UEP2_T_LEN
                                    226 	.globl _UEP2_CTRL
                                    227 	.globl _UEP1_T_LEN
                                    228 	.globl _UEP1_CTRL
                                    229 	.globl _UDEV_CTRL
                                    230 	.globl _USB_C_CTRL
                                    231 	.globl _ADC_PIN
                                    232 	.globl _ADC_CHAN
                                    233 	.globl _ADC_DAT_H
                                    234 	.globl _ADC_DAT_L
                                    235 	.globl _ADC_DAT
                                    236 	.globl _ADC_CFG
                                    237 	.globl _ADC_CTRL
                                    238 	.globl _TKEY_CTRL
                                    239 	.globl _SIF3
                                    240 	.globl _SBAUD3
                                    241 	.globl _SBUF3
                                    242 	.globl _SCON3
                                    243 	.globl _SIF2
                                    244 	.globl _SBAUD2
                                    245 	.globl _SBUF2
                                    246 	.globl _SCON2
                                    247 	.globl _SIF1
                                    248 	.globl _SBAUD1
                                    249 	.globl _SBUF1
                                    250 	.globl _SCON1
                                    251 	.globl _SPI0_SETUP
                                    252 	.globl _SPI0_CK_SE
                                    253 	.globl _SPI0_CTRL
                                    254 	.globl _SPI0_DATA
                                    255 	.globl _SPI0_STAT
                                    256 	.globl _PWM_DATA7
                                    257 	.globl _PWM_DATA6
                                    258 	.globl _PWM_DATA5
                                    259 	.globl _PWM_DATA4
                                    260 	.globl _PWM_DATA3
                                    261 	.globl _PWM_CTRL2
                                    262 	.globl _PWM_CK_SE
                                    263 	.globl _PWM_CTRL
                                    264 	.globl _PWM_DATA0
                                    265 	.globl _PWM_DATA1
                                    266 	.globl _PWM_DATA2
                                    267 	.globl _T2CAP1H
                                    268 	.globl _T2CAP1L
                                    269 	.globl _T2CAP1
                                    270 	.globl _TH2
                                    271 	.globl _TL2
                                    272 	.globl _T2COUNT
                                    273 	.globl _RCAP2H
                                    274 	.globl _RCAP2L
                                    275 	.globl _RCAP2
                                    276 	.globl _T2MOD
                                    277 	.globl _T2CON
                                    278 	.globl _T2CAP0H
                                    279 	.globl _T2CAP0L
                                    280 	.globl _T2CAP0
                                    281 	.globl _T2CON2
                                    282 	.globl _SBUF
                                    283 	.globl _SCON
                                    284 	.globl _TH1
                                    285 	.globl _TH0
                                    286 	.globl _TL1
                                    287 	.globl _TL0
                                    288 	.globl _TMOD
                                    289 	.globl _TCON
                                    290 	.globl _XBUS_AUX
                                    291 	.globl _PIN_FUNC
                                    292 	.globl _P5
                                    293 	.globl _P4_DIR_PU
                                    294 	.globl _P4_MOD_OC
                                    295 	.globl _P4
                                    296 	.globl _P3_DIR_PU
                                    297 	.globl _P3_MOD_OC
                                    298 	.globl _P3
                                    299 	.globl _P2_DIR_PU
                                    300 	.globl _P2_MOD_OC
                                    301 	.globl _P2
                                    302 	.globl _P1_DIR_PU
                                    303 	.globl _P1_MOD_OC
                                    304 	.globl _P1
                                    305 	.globl _P0_DIR_PU
                                    306 	.globl _P0_MOD_OC
                                    307 	.globl _P0
                                    308 	.globl _ROM_CTRL
                                    309 	.globl _ROM_DATA_HH
                                    310 	.globl _ROM_DATA_HL
                                    311 	.globl _ROM_DATA_HI
                                    312 	.globl _ROM_ADDR_H
                                    313 	.globl _ROM_ADDR_L
                                    314 	.globl _ROM_ADDR
                                    315 	.globl _GPIO_IE
                                    316 	.globl _INTX
                                    317 	.globl _IP_EX
                                    318 	.globl _IE_EX
                                    319 	.globl _IP
                                    320 	.globl _IE
                                    321 	.globl _WDOG_COUNT
                                    322 	.globl _RESET_KEEP
                                    323 	.globl _WAKE_CTRL
                                    324 	.globl _CLOCK_CFG
                                    325 	.globl _POWER_CFG
                                    326 	.globl _PCON
                                    327 	.globl _GLOBAL_CFG
                                    328 	.globl _SAFE_MOD
                                    329 	.globl _DPH
                                    330 	.globl _DPL
                                    331 	.globl _SP
                                    332 	.globl _A_INV
                                    333 	.globl _B
                                    334 	.globl _ACC
                                    335 	.globl _PSW
                                    336 	.globl _Set_Light_PARM_3
                                    337 	.globl _Set_Light_PARM_2
                                    338 	.globl _Set_Colour_PARM_3
                                    339 	.globl _Set_Colour_PARM_2
                                    340 	.globl _buf_B
                                    341 	.globl _buf_G
                                    342 	.globl _buf_R
                                    343 ;--------------------------------------------------------
                                    344 ; special function registers
                                    345 ;--------------------------------------------------------
                                    346 	.area RSEG    (ABS,DATA)
      000000                        347 	.org 0x0000
                           0000D0   348 _PSW	=	0x00d0
                           0000E0   349 _ACC	=	0x00e0
                           0000F0   350 _B	=	0x00f0
                           0000FD   351 _A_INV	=	0x00fd
                           000081   352 _SP	=	0x0081
                           000082   353 _DPL	=	0x0082
                           000083   354 _DPH	=	0x0083
                           0000A1   355 _SAFE_MOD	=	0x00a1
                           0000B1   356 _GLOBAL_CFG	=	0x00b1
                           000087   357 _PCON	=	0x0087
                           0000BA   358 _POWER_CFG	=	0x00ba
                           0000B9   359 _CLOCK_CFG	=	0x00b9
                           0000A9   360 _WAKE_CTRL	=	0x00a9
                           0000FE   361 _RESET_KEEP	=	0x00fe
                           0000FF   362 _WDOG_COUNT	=	0x00ff
                           0000A8   363 _IE	=	0x00a8
                           0000B8   364 _IP	=	0x00b8
                           0000E8   365 _IE_EX	=	0x00e8
                           0000E9   366 _IP_EX	=	0x00e9
                           0000B3   367 _INTX	=	0x00b3
                           0000B2   368 _GPIO_IE	=	0x00b2
                           008584   369 _ROM_ADDR	=	0x8584
                           000084   370 _ROM_ADDR_L	=	0x0084
                           000085   371 _ROM_ADDR_H	=	0x0085
                           008F8E   372 _ROM_DATA_HI	=	0x8f8e
                           00008E   373 _ROM_DATA_HL	=	0x008e
                           00008F   374 _ROM_DATA_HH	=	0x008f
                           000086   375 _ROM_CTRL	=	0x0086
                           000080   376 _P0	=	0x0080
                           0000C4   377 _P0_MOD_OC	=	0x00c4
                           0000C5   378 _P0_DIR_PU	=	0x00c5
                           000090   379 _P1	=	0x0090
                           000092   380 _P1_MOD_OC	=	0x0092
                           000093   381 _P1_DIR_PU	=	0x0093
                           0000A0   382 _P2	=	0x00a0
                           000094   383 _P2_MOD_OC	=	0x0094
                           000095   384 _P2_DIR_PU	=	0x0095
                           0000B0   385 _P3	=	0x00b0
                           000096   386 _P3_MOD_OC	=	0x0096
                           000097   387 _P3_DIR_PU	=	0x0097
                           0000C0   388 _P4	=	0x00c0
                           0000C2   389 _P4_MOD_OC	=	0x00c2
                           0000C3   390 _P4_DIR_PU	=	0x00c3
                           0000AB   391 _P5	=	0x00ab
                           0000AA   392 _PIN_FUNC	=	0x00aa
                           0000A2   393 _XBUS_AUX	=	0x00a2
                           000088   394 _TCON	=	0x0088
                           000089   395 _TMOD	=	0x0089
                           00008A   396 _TL0	=	0x008a
                           00008B   397 _TL1	=	0x008b
                           00008C   398 _TH0	=	0x008c
                           00008D   399 _TH1	=	0x008d
                           000098   400 _SCON	=	0x0098
                           000099   401 _SBUF	=	0x0099
                           0000C1   402 _T2CON2	=	0x00c1
                           00C7C6   403 _T2CAP0	=	0xc7c6
                           0000C6   404 _T2CAP0L	=	0x00c6
                           0000C7   405 _T2CAP0H	=	0x00c7
                           0000C8   406 _T2CON	=	0x00c8
                           0000C9   407 _T2MOD	=	0x00c9
                           00CBCA   408 _RCAP2	=	0xcbca
                           0000CA   409 _RCAP2L	=	0x00ca
                           0000CB   410 _RCAP2H	=	0x00cb
                           00CDCC   411 _T2COUNT	=	0xcdcc
                           0000CC   412 _TL2	=	0x00cc
                           0000CD   413 _TH2	=	0x00cd
                           00CFCE   414 _T2CAP1	=	0xcfce
                           0000CE   415 _T2CAP1L	=	0x00ce
                           0000CF   416 _T2CAP1H	=	0x00cf
                           00009A   417 _PWM_DATA2	=	0x009a
                           00009B   418 _PWM_DATA1	=	0x009b
                           00009C   419 _PWM_DATA0	=	0x009c
                           00009D   420 _PWM_CTRL	=	0x009d
                           00009E   421 _PWM_CK_SE	=	0x009e
                           00009F   422 _PWM_CTRL2	=	0x009f
                           0000A3   423 _PWM_DATA3	=	0x00a3
                           0000A4   424 _PWM_DATA4	=	0x00a4
                           0000A5   425 _PWM_DATA5	=	0x00a5
                           0000A6   426 _PWM_DATA6	=	0x00a6
                           0000A7   427 _PWM_DATA7	=	0x00a7
                           0000F8   428 _SPI0_STAT	=	0x00f8
                           0000F9   429 _SPI0_DATA	=	0x00f9
                           0000FA   430 _SPI0_CTRL	=	0x00fa
                           0000FB   431 _SPI0_CK_SE	=	0x00fb
                           0000FC   432 _SPI0_SETUP	=	0x00fc
                           0000BC   433 _SCON1	=	0x00bc
                           0000BD   434 _SBUF1	=	0x00bd
                           0000BE   435 _SBAUD1	=	0x00be
                           0000BF   436 _SIF1	=	0x00bf
                           0000B4   437 _SCON2	=	0x00b4
                           0000B5   438 _SBUF2	=	0x00b5
                           0000B6   439 _SBAUD2	=	0x00b6
                           0000B7   440 _SIF2	=	0x00b7
                           0000AC   441 _SCON3	=	0x00ac
                           0000AD   442 _SBUF3	=	0x00ad
                           0000AE   443 _SBAUD3	=	0x00ae
                           0000AF   444 _SIF3	=	0x00af
                           0000F1   445 _TKEY_CTRL	=	0x00f1
                           0000F2   446 _ADC_CTRL	=	0x00f2
                           0000F3   447 _ADC_CFG	=	0x00f3
                           00F5F4   448 _ADC_DAT	=	0xf5f4
                           0000F4   449 _ADC_DAT_L	=	0x00f4
                           0000F5   450 _ADC_DAT_H	=	0x00f5
                           0000F6   451 _ADC_CHAN	=	0x00f6
                           0000F7   452 _ADC_PIN	=	0x00f7
                           000091   453 _USB_C_CTRL	=	0x0091
                           0000D1   454 _UDEV_CTRL	=	0x00d1
                           0000D2   455 _UEP1_CTRL	=	0x00d2
                           0000D3   456 _UEP1_T_LEN	=	0x00d3
                           0000D4   457 _UEP2_CTRL	=	0x00d4
                           0000D5   458 _UEP2_T_LEN	=	0x00d5
                           0000D6   459 _UEP3_CTRL	=	0x00d6
                           0000D7   460 _UEP3_T_LEN	=	0x00d7
                           0000D8   461 _USB_INT_FG	=	0x00d8
                           0000D9   462 _USB_INT_ST	=	0x00d9
                           0000DA   463 _USB_MIS_ST	=	0x00da
                           0000DB   464 _USB_RX_LEN	=	0x00db
                           0000DC   465 _UEP0_CTRL	=	0x00dc
                           0000DD   466 _UEP0_T_LEN	=	0x00dd
                           0000DE   467 _UEP4_CTRL	=	0x00de
                           0000DF   468 _UEP4_T_LEN	=	0x00df
                           0000E1   469 _USB_INT_EN	=	0x00e1
                           0000E2   470 _USB_CTRL	=	0x00e2
                           0000E3   471 _USB_DEV_AD	=	0x00e3
                           00E5E4   472 _UEP2_DMA	=	0xe5e4
                           0000E4   473 _UEP2_DMA_L	=	0x00e4
                           0000E5   474 _UEP2_DMA_H	=	0x00e5
                           00E7E6   475 _UEP3_DMA	=	0xe7e6
                           0000E6   476 _UEP3_DMA_L	=	0x00e6
                           0000E7   477 _UEP3_DMA_H	=	0x00e7
                           0000EA   478 _UEP4_1_MOD	=	0x00ea
                           0000EB   479 _UEP2_3_MOD	=	0x00eb
                           00EDEC   480 _UEP0_DMA	=	0xedec
                           0000EC   481 _UEP0_DMA_L	=	0x00ec
                           0000ED   482 _UEP0_DMA_H	=	0x00ed
                           00EFEE   483 _UEP1_DMA	=	0xefee
                           0000EE   484 _UEP1_DMA_L	=	0x00ee
                           0000EF   485 _UEP1_DMA_H	=	0x00ef
                                    486 ;--------------------------------------------------------
                                    487 ; special function bits
                                    488 ;--------------------------------------------------------
                                    489 	.area RSEG    (ABS,DATA)
      000000                        490 	.org 0x0000
                           0000D7   491 _CY	=	0x00d7
                           0000D6   492 _AC	=	0x00d6
                           0000D5   493 _F0	=	0x00d5
                           0000D4   494 _RS1	=	0x00d4
                           0000D3   495 _RS0	=	0x00d3
                           0000D2   496 _OV	=	0x00d2
                           0000D1   497 _F1	=	0x00d1
                           0000D0   498 _P	=	0x00d0
                           0000AF   499 _EA	=	0x00af
                           0000AE   500 _E_DIS	=	0x00ae
                           0000AD   501 _ET2	=	0x00ad
                           0000AC   502 _ES	=	0x00ac
                           0000AB   503 _ET1	=	0x00ab
                           0000AA   504 _EX1	=	0x00aa
                           0000A9   505 _ET0	=	0x00a9
                           0000A8   506 _EX0	=	0x00a8
                           0000BF   507 _PH_FLAG	=	0x00bf
                           0000BE   508 _PL_FLAG	=	0x00be
                           0000BD   509 _PT2	=	0x00bd
                           0000BC   510 _PS	=	0x00bc
                           0000BB   511 _PT1	=	0x00bb
                           0000BA   512 _PX1	=	0x00ba
                           0000B9   513 _PT0	=	0x00b9
                           0000B8   514 _PX0	=	0x00b8
                           0000EF   515 _IE_WDOG	=	0x00ef
                           0000EE   516 _IE_GPIO	=	0x00ee
                           0000ED   517 _IE_PWMX	=	0x00ed
                           0000ED   518 _IE_UART3	=	0x00ed
                           0000EC   519 _IE_UART1	=	0x00ec
                           0000EB   520 _IE_ADC	=	0x00eb
                           0000EB   521 _IE_UART2	=	0x00eb
                           0000EA   522 _IE_USB	=	0x00ea
                           0000E9   523 _IE_INT3	=	0x00e9
                           0000E8   524 _IE_SPI0	=	0x00e8
                           000087   525 _P0_7	=	0x0087
                           000086   526 _P0_6	=	0x0086
                           000085   527 _P0_5	=	0x0085
                           000084   528 _P0_4	=	0x0084
                           000083   529 _P0_3	=	0x0083
                           000082   530 _P0_2	=	0x0082
                           000081   531 _P0_1	=	0x0081
                           000080   532 _P0_0	=	0x0080
                           000087   533 _TXD3	=	0x0087
                           000087   534 _AIN15	=	0x0087
                           000086   535 _RXD3	=	0x0086
                           000086   536 _AIN14	=	0x0086
                           000085   537 _TXD2	=	0x0085
                           000085   538 _AIN13	=	0x0085
                           000084   539 _RXD2	=	0x0084
                           000084   540 _AIN12	=	0x0084
                           000083   541 _TXD_	=	0x0083
                           000083   542 _AIN11	=	0x0083
                           000082   543 _RXD_	=	0x0082
                           000082   544 _AIN10	=	0x0082
                           000081   545 _AIN9	=	0x0081
                           000080   546 _AIN8	=	0x0080
                           000097   547 _P1_7	=	0x0097
                           000096   548 _P1_6	=	0x0096
                           000095   549 _P1_5	=	0x0095
                           000094   550 _P1_4	=	0x0094
                           000093   551 _P1_3	=	0x0093
                           000092   552 _P1_2	=	0x0092
                           000091   553 _P1_1	=	0x0091
                           000090   554 _P1_0	=	0x0090
                           000097   555 _SCK	=	0x0097
                           000097   556 _TXD1_	=	0x0097
                           000097   557 _AIN7	=	0x0097
                           000096   558 _MISO	=	0x0096
                           000096   559 _RXD1_	=	0x0096
                           000096   560 _VBUS	=	0x0096
                           000096   561 _AIN6	=	0x0096
                           000095   562 _MOSI	=	0x0095
                           000095   563 _PWM0_	=	0x0095
                           000095   564 _UCC2	=	0x0095
                           000095   565 _AIN5	=	0x0095
                           000094   566 _SCS	=	0x0094
                           000094   567 _UCC1	=	0x0094
                           000094   568 _AIN4	=	0x0094
                           000093   569 _AIN3	=	0x0093
                           000092   570 _AIN2	=	0x0092
                           000091   571 _T2EX	=	0x0091
                           000091   572 _CAP2	=	0x0091
                           000091   573 _AIN1	=	0x0091
                           000090   574 _T2	=	0x0090
                           000090   575 _CAP1	=	0x0090
                           000090   576 _AIN0	=	0x0090
                           0000A7   577 _P2_7	=	0x00a7
                           0000A6   578 _P2_6	=	0x00a6
                           0000A5   579 _P2_5	=	0x00a5
                           0000A4   580 _P2_4	=	0x00a4
                           0000A3   581 _P2_3	=	0x00a3
                           0000A2   582 _P2_2	=	0x00a2
                           0000A1   583 _P2_1	=	0x00a1
                           0000A0   584 _P2_0	=	0x00a0
                           0000A7   585 _PWM7	=	0x00a7
                           0000A7   586 _TXD1	=	0x00a7
                           0000A6   587 _PWM6	=	0x00a6
                           0000A6   588 _RXD1	=	0x00a6
                           0000A5   589 _PWM0	=	0x00a5
                           0000A5   590 _T2EX_	=	0x00a5
                           0000A5   591 _CAP2_	=	0x00a5
                           0000A4   592 _PWM1	=	0x00a4
                           0000A4   593 _T2_	=	0x00a4
                           0000A4   594 _CAP1_	=	0x00a4
                           0000A3   595 _PWM2	=	0x00a3
                           0000A2   596 _PWM3	=	0x00a2
                           0000A2   597 _INT0_	=	0x00a2
                           0000A1   598 _PWM4	=	0x00a1
                           0000A0   599 _PWM5	=	0x00a0
                           0000B7   600 _P3_7	=	0x00b7
                           0000B6   601 _P3_6	=	0x00b6
                           0000B5   602 _P3_5	=	0x00b5
                           0000B4   603 _P3_4	=	0x00b4
                           0000B3   604 _P3_3	=	0x00b3
                           0000B2   605 _P3_2	=	0x00b2
                           0000B1   606 _P3_1	=	0x00b1
                           0000B0   607 _P3_0	=	0x00b0
                           0000B7   608 _INT3	=	0x00b7
                           0000B6   609 _CAP0	=	0x00b6
                           0000B5   610 _T1	=	0x00b5
                           0000B4   611 _T0	=	0x00b4
                           0000B3   612 _INT1	=	0x00b3
                           0000B2   613 _INT0	=	0x00b2
                           0000B1   614 _TXD	=	0x00b1
                           0000B0   615 _RXD	=	0x00b0
                           0000C6   616 _P4_6	=	0x00c6
                           0000C5   617 _P4_5	=	0x00c5
                           0000C4   618 _P4_4	=	0x00c4
                           0000C3   619 _P4_3	=	0x00c3
                           0000C2   620 _P4_2	=	0x00c2
                           0000C1   621 _P4_1	=	0x00c1
                           0000C0   622 _P4_0	=	0x00c0
                           0000C7   623 _XO	=	0x00c7
                           0000C6   624 _XI	=	0x00c6
                           00008F   625 _TF1	=	0x008f
                           00008E   626 _TR1	=	0x008e
                           00008D   627 _TF0	=	0x008d
                           00008C   628 _TR0	=	0x008c
                           00008B   629 _IE1	=	0x008b
                           00008A   630 _IT1	=	0x008a
                           000089   631 _IE0	=	0x0089
                           000088   632 _IT0	=	0x0088
                           00009F   633 _SM0	=	0x009f
                           00009E   634 _SM1	=	0x009e
                           00009D   635 _SM2	=	0x009d
                           00009C   636 _REN	=	0x009c
                           00009B   637 _TB8	=	0x009b
                           00009A   638 _RB8	=	0x009a
                           000099   639 _TI	=	0x0099
                           000098   640 _RI	=	0x0098
                           0000CF   641 _TF2	=	0x00cf
                           0000CF   642 _CAP1F	=	0x00cf
                           0000CE   643 _EXF2	=	0x00ce
                           0000CD   644 _RCLK	=	0x00cd
                           0000CC   645 _TCLK	=	0x00cc
                           0000CB   646 _EXEN2	=	0x00cb
                           0000CA   647 _TR2	=	0x00ca
                           0000C9   648 _C_T2	=	0x00c9
                           0000C8   649 _CP_RL2	=	0x00c8
                           0000FF   650 _S0_FST_ACT	=	0x00ff
                           0000FE   651 _S0_IF_OV	=	0x00fe
                           0000FD   652 _S0_IF_FIRST	=	0x00fd
                           0000FC   653 _S0_IF_BYTE	=	0x00fc
                           0000FB   654 _S0_FREE	=	0x00fb
                           0000FA   655 _S0_T_FIFO	=	0x00fa
                           0000F8   656 _S0_R_FIFO	=	0x00f8
                           0000DF   657 _U_IS_NAK	=	0x00df
                           0000DE   658 _U_TOG_OK	=	0x00de
                           0000DD   659 _U_SIE_FREE	=	0x00dd
                           0000DC   660 _UIF_FIFO_OV	=	0x00dc
                           0000DB   661 _UIF_HST_SOF	=	0x00db
                           0000DA   662 _UIF_SUSPEND	=	0x00da
                           0000D9   663 _UIF_TRANSFER	=	0x00d9
                           0000D8   664 _UIF_DETECT	=	0x00d8
                           0000D8   665 _UIF_BUS_RST	=	0x00d8
                                    666 ;--------------------------------------------------------
                                    667 ; overlayable register banks
                                    668 ;--------------------------------------------------------
                                    669 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        670 	.ds 8
                                    671 ;--------------------------------------------------------
                                    672 ; internal ram data
                                    673 ;--------------------------------------------------------
                                    674 	.area DSEG    (DATA)
      000008                        675 _buf_R::
      000008                        676 	.ds 14
      000016                        677 _buf_G::
      000016                        678 	.ds 14
      000024                        679 _buf_B::
      000024                        680 	.ds 14
      000032                        681 _Set_Colour_PARM_2:
      000032                        682 	.ds 1
      000033                        683 _Set_Colour_PARM_3:
      000033                        684 	.ds 1
      000034                        685 _Set_Light_PARM_2:
      000034                        686 	.ds 1
      000035                        687 _Set_Light_PARM_3:
      000035                        688 	.ds 1
                                    689 ;--------------------------------------------------------
                                    690 ; overlayable items in internal ram 
                                    691 ;--------------------------------------------------------
                                    692 	.area	OSEG    (OVR,DATA)
      000036                        693 _Send_2811_24bits_PARM_2:
      000036                        694 	.ds 1
      000037                        695 _Send_2811_24bits_PARM_3:
      000037                        696 	.ds 1
                                    697 ;--------------------------------------------------------
                                    698 ; Stack segment in internal ram 
                                    699 ;--------------------------------------------------------
                                    700 	.area	SSEG
      000038                        701 __start__stack:
      000038                        702 	.ds	1
                                    703 
                                    704 ;--------------------------------------------------------
                                    705 ; indirectly addressable internal ram data
                                    706 ;--------------------------------------------------------
                                    707 	.area ISEG    (DATA)
                                    708 ;--------------------------------------------------------
                                    709 ; absolute internal ram data
                                    710 ;--------------------------------------------------------
                                    711 	.area IABS    (ABS,DATA)
                                    712 	.area IABS    (ABS,DATA)
                                    713 ;--------------------------------------------------------
                                    714 ; bit data
                                    715 ;--------------------------------------------------------
                                    716 	.area BSEG    (BIT)
                                    717 ;--------------------------------------------------------
                                    718 ; paged external ram data
                                    719 ;--------------------------------------------------------
                                    720 	.area PSEG    (PAG,XDATA)
                                    721 ;--------------------------------------------------------
                                    722 ; external ram data
                                    723 ;--------------------------------------------------------
                                    724 	.area XSEG    (XDATA)
                                    725 ;--------------------------------------------------------
                                    726 ; absolute external ram data
                                    727 ;--------------------------------------------------------
                                    728 	.area XABS    (ABS,XDATA)
                                    729 ;--------------------------------------------------------
                                    730 ; external initialized ram data
                                    731 ;--------------------------------------------------------
                                    732 	.area XISEG   (XDATA)
                                    733 	.area HOME    (CODE)
                                    734 	.area GSINIT0 (CODE)
                                    735 	.area GSINIT1 (CODE)
                                    736 	.area GSINIT2 (CODE)
                                    737 	.area GSINIT3 (CODE)
                                    738 	.area GSINIT4 (CODE)
                                    739 	.area GSINIT5 (CODE)
                                    740 	.area GSINIT  (CODE)
                                    741 	.area GSFINAL (CODE)
                                    742 	.area CSEG    (CODE)
                                    743 ;--------------------------------------------------------
                                    744 ; interrupt vector 
                                    745 ;--------------------------------------------------------
                                    746 	.area HOME    (CODE)
      000000                        747 __interrupt_vect:
      000000 02 00 06         [24]  748 	ljmp	__sdcc_gsinit_startup
                                    749 ;--------------------------------------------------------
                                    750 ; global & static initialisations
                                    751 ;--------------------------------------------------------
                                    752 	.area HOME    (CODE)
                                    753 	.area GSINIT  (CODE)
                                    754 	.area GSFINAL (CODE)
                                    755 	.area GSINIT  (CODE)
                                    756 	.globl __sdcc_gsinit_startup
                                    757 	.globl __sdcc_program_startup
                                    758 	.globl __start__stack
                                    759 	.globl __mcs51_genXINIT
                                    760 	.globl __mcs51_genXRAMCLEAR
                                    761 	.globl __mcs51_genRAMCLEAR
                                    762 ;	usr/main.c:56: UINT8 buf_R[numLEDs] = {0};//颜色缓存
      00005F 75 08 00         [24]  763 	mov	_buf_R,#0x00
                                    764 ;	usr/main.c:57: UINT8 buf_G[numLEDs] = {0};
      000062 75 16 00         [24]  765 	mov	_buf_G,#0x00
                                    766 ;	usr/main.c:58: UINT8 buf_B[numLEDs] = {0};
      000065 75 24 00         [24]  767 	mov	_buf_B,#0x00
                                    768 	.area GSFINAL (CODE)
      000068 02 00 03         [24]  769 	ljmp	__sdcc_program_startup
                                    770 ;--------------------------------------------------------
                                    771 ; Home
                                    772 ;--------------------------------------------------------
                                    773 	.area HOME    (CODE)
                                    774 	.area HOME    (CODE)
      000003                        775 __sdcc_program_startup:
      000003 02 02 0A         [24]  776 	ljmp	_main
                                    777 ;	return from main will return to caller
                                    778 ;--------------------------------------------------------
                                    779 ; code
                                    780 ;--------------------------------------------------------
                                    781 	.area CSEG    (CODE)
                                    782 ;------------------------------------------------------------
                                    783 ;Allocation info for local variables in function 'Send_2811_24bits'
                                    784 ;------------------------------------------------------------
                                    785 ;R8                        Allocated with name '_Send_2811_24bits_PARM_2'
                                    786 ;B8                        Allocated with name '_Send_2811_24bits_PARM_3'
                                    787 ;G8                        Allocated to registers r7 
                                    788 ;n                         Allocated to registers r6 
                                    789 ;------------------------------------------------------------
                                    790 ;	usr/main.c:84: void Send_2811_24bits(unsigned char G8,unsigned char R8,unsigned char B8)
                                    791 ;	-----------------------------------------
                                    792 ;	 function Send_2811_24bits
                                    793 ;	-----------------------------------------
      00006B                        794 _Send_2811_24bits:
                           000007   795 	ar7 = 0x07
                           000006   796 	ar6 = 0x06
                           000005   797 	ar5 = 0x05
                           000004   798 	ar4 = 0x04
                           000003   799 	ar3 = 0x03
                           000002   800 	ar2 = 0x02
                           000001   801 	ar1 = 0x01
                           000000   802 	ar0 = 0x00
      00006B AF 82            [24]  803 	mov	r7,dpl
                                    804 ;	usr/main.c:89: for(n=0;n<8;n++)
      00006D 7E 00            [12]  805 	mov	r6,#0x00
      00006F                        806 00131$:
                                    807 ;	usr/main.c:92: if(G8&0x80)
      00006F EF               [12]  808 	mov	a,r7
      000070 30 E7 2E         [24]  809 	jnb	acc.7,00104$
                                    810 ;	usr/main.c:94: RGB_1();
                                    811 ;	assignBit
      000073 D2 A2            [12]  812 	setb	_P2_2
      000075 00               [12]  813 	NOP	
      000076 00               [12]  814 	NOP	
      000077 00               [12]  815 	NOP	
      000078 00               [12]  816 	NOP	
      000079 00               [12]  817 	NOP	
      00007A 00               [12]  818 	NOP	
      00007B 00               [12]  819 	NOP	
      00007C 00               [12]  820 	NOP	
      00007D 00               [12]  821 	NOP	
      00007E 00               [12]  822 	NOP	
      00007F 00               [12]  823 	NOP	
      000080 00               [12]  824 	NOP	
      000081 00               [12]  825 	NOP	
      000082 00               [12]  826 	NOP	
      000083 00               [12]  827 	NOP	
      000084 00               [12]  828 	NOP	
      000085 00               [12]  829 	NOP	
      000086 00               [12]  830 	NOP	
      000087 00               [12]  831 	NOP	
      000088 00               [12]  832 	NOP	
      000089 00               [12]  833 	NOP	
      00008A 00               [12]  834 	NOP	
      00008B 00               [12]  835 	NOP	
      00008C 00               [12]  836 	NOP	
      00008D 00               [12]  837 	NOP	
      00008E 00               [12]  838 	NOP	
      00008F 00               [12]  839 	NOP	
      000090 00               [12]  840 	NOP	
      000091 00               [12]  841 	NOP	
      000092 00               [12]  842 	NOP	
      000093 00               [12]  843 	NOP	
      000094 00               [12]  844 	NOP	
      000095 00               [12]  845 	NOP	
      000096 00               [12]  846 	NOP	
      000097 00               [12]  847 	NOP	
      000098 00               [12]  848 	NOP	
      000099 00               [12]  849 	NOP	
      00009A 00               [12]  850 	NOP	
      00009B 00               [12]  851 	NOP	
      00009C 00               [12]  852 	NOP	
                                    853 ;	assignBit
      00009D C2 A2            [12]  854 	clr	_P2_2
                                    855 ;	usr/main.c:98: RGB_0();
      00009F 80 27            [24]  856 	sjmp	00109$
      0000A1                        857 00104$:
                                    858 ;	assignBit
      0000A1 D2 A2            [12]  859 	setb	_P2_2
      0000A3 00               [12]  860 	NOP	
      0000A4 00               [12]  861 	NOP	
      0000A5 00               [12]  862 	NOP	
      0000A6 00               [12]  863 	NOP	
      0000A7 00               [12]  864 	NOP	
      0000A8 00               [12]  865 	NOP	
      0000A9 00               [12]  866 	NOP	
      0000AA 00               [12]  867 	NOP	
      0000AB 00               [12]  868 	NOP	
      0000AC 00               [12]  869 	NOP	
      0000AD 00               [12]  870 	NOP	
      0000AE 00               [12]  871 	NOP	
      0000AF 00               [12]  872 	NOP	
      0000B0 00               [12]  873 	NOP	
      0000B1 00               [12]  874 	NOP	
      0000B2 00               [12]  875 	NOP	
      0000B3 00               [12]  876 	NOP	
      0000B4 00               [12]  877 	NOP	
      0000B5 00               [12]  878 	NOP	
      0000B6 00               [12]  879 	NOP	
                                    880 ;	assignBit
      0000B7 C2 A2            [12]  881 	clr	_P2_2
      0000B9 00               [12]  882 	NOP	
      0000BA 00               [12]  883 	NOP	
      0000BB 00               [12]  884 	NOP	
      0000BC 00               [12]  885 	NOP	
      0000BD 00               [12]  886 	NOP	
      0000BE 00               [12]  887 	NOP	
      0000BF 00               [12]  888 	NOP	
      0000C0 00               [12]  889 	NOP	
      0000C1 00               [12]  890 	NOP	
      0000C2 00               [12]  891 	NOP	
      0000C3 00               [12]  892 	NOP	
      0000C4 00               [12]  893 	NOP	
      0000C5 00               [12]  894 	NOP	
      0000C6 00               [12]  895 	NOP	
      0000C7 00               [12]  896 	NOP	
      0000C8                        897 00109$:
                                    898 ;	usr/main.c:100: G8<<=1;
      0000C8 8F 05            [24]  899 	mov	ar5,r7
      0000CA ED               [12]  900 	mov	a,r5
      0000CB 2D               [12]  901 	add	a,r5
      0000CC FF               [12]  902 	mov	r7,a
                                    903 ;	usr/main.c:89: for(n=0;n<8;n++)
      0000CD 0E               [12]  904 	inc	r6
      0000CE BE 08 00         [24]  905 	cjne	r6,#0x08,00175$
      0000D1                        906 00175$:
      0000D1 40 9C            [24]  907 	jc	00131$
                                    908 ;	usr/main.c:103: for(n=0;n<8;n++)
      0000D3 7F 00            [12]  909 	mov	r7,#0x00
      0000D5                        910 00133$:
                                    911 ;	usr/main.c:106: if(R8&0x80)
      0000D5 E5 36            [12]  912 	mov	a,_Send_2811_24bits_PARM_2
      0000D7 30 E7 2E         [24]  913 	jnb	acc.7,00114$
                                    914 ;	usr/main.c:108: RGB_1();
                                    915 ;	assignBit
      0000DA D2 A2            [12]  916 	setb	_P2_2
      0000DC 00               [12]  917 	NOP	
      0000DD 00               [12]  918 	NOP	
      0000DE 00               [12]  919 	NOP	
      0000DF 00               [12]  920 	NOP	
      0000E0 00               [12]  921 	NOP	
      0000E1 00               [12]  922 	NOP	
      0000E2 00               [12]  923 	NOP	
      0000E3 00               [12]  924 	NOP	
      0000E4 00               [12]  925 	NOP	
      0000E5 00               [12]  926 	NOP	
      0000E6 00               [12]  927 	NOP	
      0000E7 00               [12]  928 	NOP	
      0000E8 00               [12]  929 	NOP	
      0000E9 00               [12]  930 	NOP	
      0000EA 00               [12]  931 	NOP	
      0000EB 00               [12]  932 	NOP	
      0000EC 00               [12]  933 	NOP	
      0000ED 00               [12]  934 	NOP	
      0000EE 00               [12]  935 	NOP	
      0000EF 00               [12]  936 	NOP	
      0000F0 00               [12]  937 	NOP	
      0000F1 00               [12]  938 	NOP	
      0000F2 00               [12]  939 	NOP	
      0000F3 00               [12]  940 	NOP	
      0000F4 00               [12]  941 	NOP	
      0000F5 00               [12]  942 	NOP	
      0000F6 00               [12]  943 	NOP	
      0000F7 00               [12]  944 	NOP	
      0000F8 00               [12]  945 	NOP	
      0000F9 00               [12]  946 	NOP	
      0000FA 00               [12]  947 	NOP	
      0000FB 00               [12]  948 	NOP	
      0000FC 00               [12]  949 	NOP	
      0000FD 00               [12]  950 	NOP	
      0000FE 00               [12]  951 	NOP	
      0000FF 00               [12]  952 	NOP	
      000100 00               [12]  953 	NOP	
      000101 00               [12]  954 	NOP	
      000102 00               [12]  955 	NOP	
      000103 00               [12]  956 	NOP	
                                    957 ;	assignBit
      000104 C2 A2            [12]  958 	clr	_P2_2
                                    959 ;	usr/main.c:112: RGB_0();
      000106 80 27            [24]  960 	sjmp	00119$
      000108                        961 00114$:
                                    962 ;	assignBit
      000108 D2 A2            [12]  963 	setb	_P2_2
      00010A 00               [12]  964 	NOP	
      00010B 00               [12]  965 	NOP	
      00010C 00               [12]  966 	NOP	
      00010D 00               [12]  967 	NOP	
      00010E 00               [12]  968 	NOP	
      00010F 00               [12]  969 	NOP	
      000110 00               [12]  970 	NOP	
      000111 00               [12]  971 	NOP	
      000112 00               [12]  972 	NOP	
      000113 00               [12]  973 	NOP	
      000114 00               [12]  974 	NOP	
      000115 00               [12]  975 	NOP	
      000116 00               [12]  976 	NOP	
      000117 00               [12]  977 	NOP	
      000118 00               [12]  978 	NOP	
      000119 00               [12]  979 	NOP	
      00011A 00               [12]  980 	NOP	
      00011B 00               [12]  981 	NOP	
      00011C 00               [12]  982 	NOP	
      00011D 00               [12]  983 	NOP	
                                    984 ;	assignBit
      00011E C2 A2            [12]  985 	clr	_P2_2
      000120 00               [12]  986 	NOP	
      000121 00               [12]  987 	NOP	
      000122 00               [12]  988 	NOP	
      000123 00               [12]  989 	NOP	
      000124 00               [12]  990 	NOP	
      000125 00               [12]  991 	NOP	
      000126 00               [12]  992 	NOP	
      000127 00               [12]  993 	NOP	
      000128 00               [12]  994 	NOP	
      000129 00               [12]  995 	NOP	
      00012A 00               [12]  996 	NOP	
      00012B 00               [12]  997 	NOP	
      00012C 00               [12]  998 	NOP	
      00012D 00               [12]  999 	NOP	
      00012E 00               [12] 1000 	NOP	
      00012F                       1001 00119$:
                                   1002 ;	usr/main.c:114: R8<<=1;
      00012F E5 36            [12] 1003 	mov	a,_Send_2811_24bits_PARM_2
      000131 FE               [12] 1004 	mov	r6,a
      000132 25 E0            [12] 1005 	add	a,acc
      000134 F5 36            [12] 1006 	mov	_Send_2811_24bits_PARM_2,a
                                   1007 ;	usr/main.c:103: for(n=0;n<8;n++)
      000136 0F               [12] 1008 	inc	r7
      000137 BF 08 00         [24] 1009 	cjne	r7,#0x08,00178$
      00013A                       1010 00178$:
      00013A 40 99            [24] 1011 	jc	00133$
                                   1012 ;	usr/main.c:117: for(n=0;n<8;n++)
      00013C 7F 00            [12] 1013 	mov	r7,#0x00
      00013E                       1014 00135$:
                                   1015 ;	usr/main.c:120: if(B8&0x80)
      00013E E5 37            [12] 1016 	mov	a,_Send_2811_24bits_PARM_3
      000140 30 E7 2E         [24] 1017 	jnb	acc.7,00124$
                                   1018 ;	usr/main.c:122: RGB_1();
                                   1019 ;	assignBit
      000143 D2 A2            [12] 1020 	setb	_P2_2
      000145 00               [12] 1021 	NOP	
      000146 00               [12] 1022 	NOP	
      000147 00               [12] 1023 	NOP	
      000148 00               [12] 1024 	NOP	
      000149 00               [12] 1025 	NOP	
      00014A 00               [12] 1026 	NOP	
      00014B 00               [12] 1027 	NOP	
      00014C 00               [12] 1028 	NOP	
      00014D 00               [12] 1029 	NOP	
      00014E 00               [12] 1030 	NOP	
      00014F 00               [12] 1031 	NOP	
      000150 00               [12] 1032 	NOP	
      000151 00               [12] 1033 	NOP	
      000152 00               [12] 1034 	NOP	
      000153 00               [12] 1035 	NOP	
      000154 00               [12] 1036 	NOP	
      000155 00               [12] 1037 	NOP	
      000156 00               [12] 1038 	NOP	
      000157 00               [12] 1039 	NOP	
      000158 00               [12] 1040 	NOP	
      000159 00               [12] 1041 	NOP	
      00015A 00               [12] 1042 	NOP	
      00015B 00               [12] 1043 	NOP	
      00015C 00               [12] 1044 	NOP	
      00015D 00               [12] 1045 	NOP	
      00015E 00               [12] 1046 	NOP	
      00015F 00               [12] 1047 	NOP	
      000160 00               [12] 1048 	NOP	
      000161 00               [12] 1049 	NOP	
      000162 00               [12] 1050 	NOP	
      000163 00               [12] 1051 	NOP	
      000164 00               [12] 1052 	NOP	
      000165 00               [12] 1053 	NOP	
      000166 00               [12] 1054 	NOP	
      000167 00               [12] 1055 	NOP	
      000168 00               [12] 1056 	NOP	
      000169 00               [12] 1057 	NOP	
      00016A 00               [12] 1058 	NOP	
      00016B 00               [12] 1059 	NOP	
      00016C 00               [12] 1060 	NOP	
                                   1061 ;	assignBit
      00016D C2 A2            [12] 1062 	clr	_P2_2
                                   1063 ;	usr/main.c:126: RGB_0();
      00016F 80 27            [24] 1064 	sjmp	00129$
      000171                       1065 00124$:
                                   1066 ;	assignBit
      000171 D2 A2            [12] 1067 	setb	_P2_2
      000173 00               [12] 1068 	NOP	
      000174 00               [12] 1069 	NOP	
      000175 00               [12] 1070 	NOP	
      000176 00               [12] 1071 	NOP	
      000177 00               [12] 1072 	NOP	
      000178 00               [12] 1073 	NOP	
      000179 00               [12] 1074 	NOP	
      00017A 00               [12] 1075 	NOP	
      00017B 00               [12] 1076 	NOP	
      00017C 00               [12] 1077 	NOP	
      00017D 00               [12] 1078 	NOP	
      00017E 00               [12] 1079 	NOP	
      00017F 00               [12] 1080 	NOP	
      000180 00               [12] 1081 	NOP	
      000181 00               [12] 1082 	NOP	
      000182 00               [12] 1083 	NOP	
      000183 00               [12] 1084 	NOP	
      000184 00               [12] 1085 	NOP	
      000185 00               [12] 1086 	NOP	
      000186 00               [12] 1087 	NOP	
                                   1088 ;	assignBit
      000187 C2 A2            [12] 1089 	clr	_P2_2
      000189 00               [12] 1090 	NOP	
      00018A 00               [12] 1091 	NOP	
      00018B 00               [12] 1092 	NOP	
      00018C 00               [12] 1093 	NOP	
      00018D 00               [12] 1094 	NOP	
      00018E 00               [12] 1095 	NOP	
      00018F 00               [12] 1096 	NOP	
      000190 00               [12] 1097 	NOP	
      000191 00               [12] 1098 	NOP	
      000192 00               [12] 1099 	NOP	
      000193 00               [12] 1100 	NOP	
      000194 00               [12] 1101 	NOP	
      000195 00               [12] 1102 	NOP	
      000196 00               [12] 1103 	NOP	
      000197 00               [12] 1104 	NOP	
      000198                       1105 00129$:
                                   1106 ;	usr/main.c:128: B8<<=1;
      000198 E5 37            [12] 1107 	mov	a,_Send_2811_24bits_PARM_3
      00019A FE               [12] 1108 	mov	r6,a
      00019B 25 E0            [12] 1109 	add	a,acc
      00019D F5 37            [12] 1110 	mov	_Send_2811_24bits_PARM_3,a
                                   1111 ;	usr/main.c:117: for(n=0;n<8;n++)
      00019F 0F               [12] 1112 	inc	r7
      0001A0 BF 08 00         [24] 1113 	cjne	r7,#0x08,00181$
      0001A3                       1114 00181$:
      0001A3 40 99            [24] 1115 	jc	00135$
                                   1116 ;	usr/main.c:130: }
      0001A5 22               [24] 1117 	ret
                                   1118 ;------------------------------------------------------------
                                   1119 ;Allocation info for local variables in function 'RGB_Rst'
                                   1120 ;------------------------------------------------------------
                                   1121 ;	usr/main.c:133: void RGB_Rst()
                                   1122 ;	-----------------------------------------
                                   1123 ;	 function RGB_Rst
                                   1124 ;	-----------------------------------------
      0001A6                       1125 _RGB_Rst:
                                   1126 ;	usr/main.c:135: WS2812 = 0;
                                   1127 ;	assignBit
      0001A6 C2 A2            [12] 1128 	clr	_P2_2
                                   1129 ;	usr/main.c:136: mDelayuS( 50 );
      0001A8 90 00 32         [24] 1130 	mov	dptr,#0x0032
                                   1131 ;	usr/main.c:137: }
      0001AB 02 02 DF         [24] 1132 	ljmp	_mDelayuS
                                   1133 ;------------------------------------------------------------
                                   1134 ;Allocation info for local variables in function 'Set_Colour'
                                   1135 ;------------------------------------------------------------
                                   1136 ;g                         Allocated with name '_Set_Colour_PARM_2'
                                   1137 ;b                         Allocated with name '_Set_Colour_PARM_3'
                                   1138 ;r                         Allocated to registers r7 
                                   1139 ;i                         Allocated to registers r6 
                                   1140 ;------------------------------------------------------------
                                   1141 ;	usr/main.c:140: void Set_Colour(unsigned char r,unsigned char g,unsigned char b)
                                   1142 ;	-----------------------------------------
                                   1143 ;	 function Set_Colour
                                   1144 ;	-----------------------------------------
      0001AE                       1145 _Set_Colour:
      0001AE AF 82            [24] 1146 	mov	r7,dpl
                                   1147 ;	usr/main.c:143: for(i=0;i<numLEDs;i++)
      0001B0 7E 00            [12] 1148 	mov	r6,#0x00
      0001B2                       1149 00103$:
                                   1150 ;	usr/main.c:145: buf_R[i] = r; //缓冲
      0001B2 EE               [12] 1151 	mov	a,r6
      0001B3 24 08            [12] 1152 	add	a,#_buf_R
      0001B5 F8               [12] 1153 	mov	r0,a
      0001B6 A6 07            [24] 1154 	mov	@r0,ar7
                                   1155 ;	usr/main.c:146: buf_G[i] = g;
      0001B8 EE               [12] 1156 	mov	a,r6
      0001B9 24 16            [12] 1157 	add	a,#_buf_G
      0001BB F8               [12] 1158 	mov	r0,a
      0001BC A6 32            [24] 1159 	mov	@r0,_Set_Colour_PARM_2
                                   1160 ;	usr/main.c:147: buf_B[i] = b;
      0001BE EE               [12] 1161 	mov	a,r6
      0001BF 24 24            [12] 1162 	add	a,#_buf_B
      0001C1 F8               [12] 1163 	mov	r0,a
      0001C2 A6 33            [24] 1164 	mov	@r0,_Set_Colour_PARM_3
                                   1165 ;	usr/main.c:143: for(i=0;i<numLEDs;i++)
      0001C4 0E               [12] 1166 	inc	r6
      0001C5 BE 0E 00         [24] 1167 	cjne	r6,#0x0e,00123$
      0001C8                       1168 00123$:
      0001C8 40 E8            [24] 1169 	jc	00103$
                                   1170 ;	usr/main.c:149: for(i=0;i<numLEDs;i++)
      0001CA 7F 00            [12] 1171 	mov	r7,#0x00
      0001CC                       1172 00105$:
                                   1173 ;	usr/main.c:151: Send_2811_24bits(buf_G[i],buf_R[i],buf_B[i]);//发送显示
      0001CC EF               [12] 1174 	mov	a,r7
      0001CD 24 16            [12] 1175 	add	a,#_buf_G
      0001CF F9               [12] 1176 	mov	r1,a
      0001D0 87 82            [24] 1177 	mov	dpl,@r1
      0001D2 EF               [12] 1178 	mov	a,r7
      0001D3 24 08            [12] 1179 	add	a,#_buf_R
      0001D5 F9               [12] 1180 	mov	r1,a
      0001D6 87 36            [24] 1181 	mov	_Send_2811_24bits_PARM_2,@r1
      0001D8 EF               [12] 1182 	mov	a,r7
      0001D9 24 24            [12] 1183 	add	a,#_buf_B
      0001DB F9               [12] 1184 	mov	r1,a
      0001DC 87 37            [24] 1185 	mov	_Send_2811_24bits_PARM_3,@r1
      0001DE C0 07            [24] 1186 	push	ar7
      0001E0 12 00 6B         [24] 1187 	lcall	_Send_2811_24bits
      0001E3 D0 07            [24] 1188 	pop	ar7
                                   1189 ;	usr/main.c:149: for(i=0;i<numLEDs;i++)
      0001E5 0F               [12] 1190 	inc	r7
      0001E6 BF 0E 00         [24] 1191 	cjne	r7,#0x0e,00125$
      0001E9                       1192 00125$:
      0001E9 40 E1            [24] 1193 	jc	00105$
                                   1194 ;	usr/main.c:153: }
      0001EB 22               [24] 1195 	ret
                                   1196 ;------------------------------------------------------------
                                   1197 ;Allocation info for local variables in function 'Set_Light'
                                   1198 ;------------------------------------------------------------
                                   1199 ;y                         Allocated with name '_Set_Light_PARM_2'
                                   1200 ;z                         Allocated with name '_Set_Light_PARM_3'
                                   1201 ;x                         Allocated to registers r7 
                                   1202 ;i                         Allocated to registers r6 
                                   1203 ;------------------------------------------------------------
                                   1204 ;	usr/main.c:156: void Set_Light(unsigned char x,unsigned char y,unsigned char z)
                                   1205 ;	-----------------------------------------
                                   1206 ;	 function Set_Light
                                   1207 ;	-----------------------------------------
      0001EC                       1208 _Set_Light:
      0001EC AF 82            [24] 1209 	mov	r7,dpl
                                   1210 ;	usr/main.c:159: for ( i = 0; i < numLEDs; i++)
      0001EE 7E 00            [12] 1211 	mov	r6,#0x00
      0001F0                       1212 00102$:
                                   1213 ;	usr/main.c:161: Send_2811_24bits( x , y , z );//发送显示
      0001F0 85 34 36         [24] 1214 	mov	_Send_2811_24bits_PARM_2,_Set_Light_PARM_2
      0001F3 85 35 37         [24] 1215 	mov	_Send_2811_24bits_PARM_3,_Set_Light_PARM_3
      0001F6 8F 82            [24] 1216 	mov	dpl,r7
      0001F8 C0 07            [24] 1217 	push	ar7
      0001FA C0 06            [24] 1218 	push	ar6
      0001FC 12 00 6B         [24] 1219 	lcall	_Send_2811_24bits
      0001FF D0 06            [24] 1220 	pop	ar6
      000201 D0 07            [24] 1221 	pop	ar7
                                   1222 ;	usr/main.c:159: for ( i = 0; i < numLEDs; i++)
      000203 0E               [12] 1223 	inc	r6
      000204 BE 0E 00         [24] 1224 	cjne	r6,#0x0e,00111$
      000207                       1225 00111$:
      000207 40 E7            [24] 1226 	jc	00102$
                                   1227 ;	usr/main.c:165: }
      000209 22               [24] 1228 	ret
                                   1229 ;------------------------------------------------------------
                                   1230 ;Allocation info for local variables in function 'main'
                                   1231 ;------------------------------------------------------------
                                   1232 ;ch                        Allocated to registers 
                                   1233 ;Voltage                   Allocated to registers r6 r7 
                                   1234 ;------------------------------------------------------------
                                   1235 ;	usr/main.c:172: void main(void)
                                   1236 ;	-----------------------------------------
                                   1237 ;	 function main
                                   1238 ;	-----------------------------------------
      00020A                       1239 _main:
                                   1240 ;	usr/main.c:176: CfgFsys(); //CH549时钟选择配置
      00020A 12 02 C1         [24] 1241 	lcall	_CfgFsys
                                   1242 ;	usr/main.c:177: mDelaymS(20);
      00020D 90 00 14         [24] 1243 	mov	dptr,#0x0014
      000210 12 03 15         [24] 1244 	lcall	_mDelaymS
                                   1245 ;	usr/main.c:178: ADC_ExInit(3); //ADC初始化,选择采样时钟
      000213 75 82 03         [24] 1246 	mov	dpl,#0x03
      000216 12 03 87         [24] 1247 	lcall	_ADC_ExInit
                                   1248 ;	usr/main.c:180: while (1)
      000219                       1249 00108$:
                                   1250 ;	usr/main.c:183: ADC_ChSelect(ch);  //选择通道
      000219 75 82 07         [24] 1251 	mov	dpl,#0x07
      00021C 12 03 9E         [24] 1252 	lcall	_ADC_ChSelect
                                   1253 ;	usr/main.c:184: ADC_StartSample(); //启动采样
      00021F 75 F2 10         [24] 1254 	mov	_ADC_CTRL,#0x10
                                   1255 ;	usr/main.c:186: while ((ADC_CTRL & bADC_IF) == 0)
      000222                       1256 00101$:
      000222 E5 F2            [12] 1257 	mov	a,_ADC_CTRL
      000224 30 E5 FB         [24] 1258 	jnb	acc.5,00101$
                                   1259 ;	usr/main.c:190: ADC_CTRL = bADC_IF; //清标志2
      000227 75 F2 20         [24] 1260 	mov	_ADC_CTRL,#0x20
                                   1261 ;	usr/main.c:191: Voltage = ADC_DAT * 255.0 / 4095.0;                      //由于voltage数据类型的原因，先乘后除可以避免出现除法之后只有0和1再乘会出现灯带闪烁的情况
      00022A 85 F4 82         [24] 1262 	mov	dpl,((_ADC_DAT >> 0) & 0xFF)
      00022D 85 F5 83         [24] 1263 	mov	dph,((_ADC_DAT >> 8) & 0xFF)
      000230 12 04 F0         [24] 1264 	lcall	___uint2fs
      000233 AC 82            [24] 1265 	mov	r4,dpl
      000235 AD 83            [24] 1266 	mov	r5,dph
      000237 AE F0            [24] 1267 	mov	r6,b
      000239 FF               [12] 1268 	mov	r7,a
      00023A C0 04            [24] 1269 	push	ar4
      00023C C0 05            [24] 1270 	push	ar5
      00023E C0 06            [24] 1271 	push	ar6
      000240 C0 07            [24] 1272 	push	ar7
      000242 90 00 00         [24] 1273 	mov	dptr,#0x0000
      000245 75 F0 7F         [24] 1274 	mov	b,#0x7f
      000248 74 43            [12] 1275 	mov	a,#0x43
      00024A 12 03 EC         [24] 1276 	lcall	___fsmul
      00024D AC 82            [24] 1277 	mov	r4,dpl
      00024F AD 83            [24] 1278 	mov	r5,dph
      000251 AE F0            [24] 1279 	mov	r6,b
      000253 FF               [12] 1280 	mov	r7,a
      000254 E5 81            [12] 1281 	mov	a,sp
      000256 24 FC            [12] 1282 	add	a,#0xfc
      000258 F5 81            [12] 1283 	mov	sp,a
      00025A E4               [12] 1284 	clr	a
      00025B C0 E0            [24] 1285 	push	acc
      00025D 74 F0            [12] 1286 	mov	a,#0xf0
      00025F C0 E0            [24] 1287 	push	acc
      000261 74 7F            [12] 1288 	mov	a,#0x7f
      000263 C0 E0            [24] 1289 	push	acc
      000265 74 45            [12] 1290 	mov	a,#0x45
      000267 C0 E0            [24] 1291 	push	acc
      000269 8C 82            [24] 1292 	mov	dpl,r4
      00026B 8D 83            [24] 1293 	mov	dph,r5
      00026D 8E F0            [24] 1294 	mov	b,r6
      00026F EF               [12] 1295 	mov	a,r7
      000270 12 05 56         [24] 1296 	lcall	___fsdiv
      000273 AC 82            [24] 1297 	mov	r4,dpl
      000275 AD 83            [24] 1298 	mov	r5,dph
      000277 AE F0            [24] 1299 	mov	r6,b
      000279 FF               [12] 1300 	mov	r7,a
      00027A E5 81            [12] 1301 	mov	a,sp
      00027C 24 FC            [12] 1302 	add	a,#0xfc
      00027E F5 81            [12] 1303 	mov	sp,a
      000280 8C 82            [24] 1304 	mov	dpl,r4
      000282 8D 83            [24] 1305 	mov	dph,r5
      000284 8E F0            [24] 1306 	mov	b,r6
      000286 EF               [12] 1307 	mov	a,r7
      000287 12 04 FC         [24] 1308 	lcall	___fs2uint
      00028A AE 82            [24] 1309 	mov	r6,dpl
      00028C AF 83            [24] 1310 	mov	r7,dph
                                   1311 ;	usr/main.c:197: if (Voltage > 32 )                                  //voltage值大于32时打开灯
      00028E 8E 04            [24] 1312 	mov	ar4,r6
      000290 8F 05            [24] 1313 	mov	ar5,r7
      000292 C3               [12] 1314 	clr	c
      000293 74 20            [12] 1315 	mov	a,#0x20
      000295 9C               [12] 1316 	subb	a,r4
      000296 E4               [12] 1317 	clr	a
      000297 9D               [12] 1318 	subb	a,r5
      000298 50 12            [24] 1319 	jnc	00105$
                                   1320 ;	usr/main.c:199: Set_Light(Voltage,Voltage,Voltage );                //根据voltage值赋予灯亮度
      00029A 8E 34            [24] 1321 	mov	_Set_Light_PARM_2,r6
      00029C 8E 35            [24] 1322 	mov	_Set_Light_PARM_3,r6
      00029E 8E 82            [24] 1323 	mov	dpl,r6
      0002A0 12 01 EC         [24] 1324 	lcall	_Set_Light
                                   1325 ;	usr/main.c:200: mDelaymS(30);                                       //WS2812B需要一小段延迟将数据写入
      0002A3 90 00 1E         [24] 1326 	mov	dptr,#0x001e
      0002A6 12 03 15         [24] 1327 	lcall	_mDelaymS
      0002A9 02 02 19         [24] 1328 	ljmp	00108$
      0002AC                       1329 00105$:
                                   1330 ;	usr/main.c:204: Set_Light(0x00,0x00,0x00 );
      0002AC 75 34 00         [24] 1331 	mov	_Set_Light_PARM_2,#0x00
      0002AF 75 35 00         [24] 1332 	mov	_Set_Light_PARM_3,#0x00
      0002B2 75 82 00         [24] 1333 	mov	dpl,#0x00
      0002B5 12 01 EC         [24] 1334 	lcall	_Set_Light
                                   1335 ;	usr/main.c:205: mDelaymS(30);
      0002B8 90 00 1E         [24] 1336 	mov	dptr,#0x001e
      0002BB 12 03 15         [24] 1337 	lcall	_mDelaymS
                                   1338 ;	usr/main.c:211: }
      0002BE 02 02 19         [24] 1339 	ljmp	00108$
                                   1340 	.area CSEG    (CODE)
                                   1341 	.area CONST   (CODE)
                                   1342 	.area XINIT   (CODE)
                                   1343 	.area CABS    (ABS,CODE)
