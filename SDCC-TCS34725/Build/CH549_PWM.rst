                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 4.0.0 #11528 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module CH549_PWM
                                      6 	.optsdcc -mmcs51 --model-large
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _UIF_BUS_RST
                                     12 	.globl _UIF_DETECT
                                     13 	.globl _UIF_TRANSFER
                                     14 	.globl _UIF_SUSPEND
                                     15 	.globl _UIF_HST_SOF
                                     16 	.globl _UIF_FIFO_OV
                                     17 	.globl _U_SIE_FREE
                                     18 	.globl _U_TOG_OK
                                     19 	.globl _U_IS_NAK
                                     20 	.globl _S0_R_FIFO
                                     21 	.globl _S0_T_FIFO
                                     22 	.globl _S0_FREE
                                     23 	.globl _S0_IF_BYTE
                                     24 	.globl _S0_IF_FIRST
                                     25 	.globl _S0_IF_OV
                                     26 	.globl _S0_FST_ACT
                                     27 	.globl _CP_RL2
                                     28 	.globl _C_T2
                                     29 	.globl _TR2
                                     30 	.globl _EXEN2
                                     31 	.globl _TCLK
                                     32 	.globl _RCLK
                                     33 	.globl _EXF2
                                     34 	.globl _CAP1F
                                     35 	.globl _TF2
                                     36 	.globl _RI
                                     37 	.globl _TI
                                     38 	.globl _RB8
                                     39 	.globl _TB8
                                     40 	.globl _REN
                                     41 	.globl _SM2
                                     42 	.globl _SM1
                                     43 	.globl _SM0
                                     44 	.globl _IT0
                                     45 	.globl _IE0
                                     46 	.globl _IT1
                                     47 	.globl _IE1
                                     48 	.globl _TR0
                                     49 	.globl _TF0
                                     50 	.globl _TR1
                                     51 	.globl _TF1
                                     52 	.globl _XI
                                     53 	.globl _XO
                                     54 	.globl _P4_0
                                     55 	.globl _P4_1
                                     56 	.globl _P4_2
                                     57 	.globl _P4_3
                                     58 	.globl _P4_4
                                     59 	.globl _P4_5
                                     60 	.globl _P4_6
                                     61 	.globl _RXD
                                     62 	.globl _TXD
                                     63 	.globl _INT0
                                     64 	.globl _INT1
                                     65 	.globl _T0
                                     66 	.globl _T1
                                     67 	.globl _CAP0
                                     68 	.globl _INT3
                                     69 	.globl _P3_0
                                     70 	.globl _P3_1
                                     71 	.globl _P3_2
                                     72 	.globl _P3_3
                                     73 	.globl _P3_4
                                     74 	.globl _P3_5
                                     75 	.globl _P3_6
                                     76 	.globl _P3_7
                                     77 	.globl _PWM5
                                     78 	.globl _PWM4
                                     79 	.globl _INT0_
                                     80 	.globl _PWM3
                                     81 	.globl _PWM2
                                     82 	.globl _CAP1_
                                     83 	.globl _T2_
                                     84 	.globl _PWM1
                                     85 	.globl _CAP2_
                                     86 	.globl _T2EX_
                                     87 	.globl _PWM0
                                     88 	.globl _RXD1
                                     89 	.globl _PWM6
                                     90 	.globl _TXD1
                                     91 	.globl _PWM7
                                     92 	.globl _P2_0
                                     93 	.globl _P2_1
                                     94 	.globl _P2_2
                                     95 	.globl _P2_3
                                     96 	.globl _P2_4
                                     97 	.globl _P2_5
                                     98 	.globl _P2_6
                                     99 	.globl _P2_7
                                    100 	.globl _AIN0
                                    101 	.globl _CAP1
                                    102 	.globl _T2
                                    103 	.globl _AIN1
                                    104 	.globl _CAP2
                                    105 	.globl _T2EX
                                    106 	.globl _AIN2
                                    107 	.globl _AIN3
                                    108 	.globl _AIN4
                                    109 	.globl _UCC1
                                    110 	.globl _SCS
                                    111 	.globl _AIN5
                                    112 	.globl _UCC2
                                    113 	.globl _PWM0_
                                    114 	.globl _MOSI
                                    115 	.globl _AIN6
                                    116 	.globl _VBUS
                                    117 	.globl _RXD1_
                                    118 	.globl _MISO
                                    119 	.globl _AIN7
                                    120 	.globl _TXD1_
                                    121 	.globl _SCK
                                    122 	.globl _P1_0
                                    123 	.globl _P1_1
                                    124 	.globl _P1_2
                                    125 	.globl _P1_3
                                    126 	.globl _P1_4
                                    127 	.globl _P1_5
                                    128 	.globl _P1_6
                                    129 	.globl _P1_7
                                    130 	.globl _AIN8
                                    131 	.globl _AIN9
                                    132 	.globl _AIN10
                                    133 	.globl _RXD_
                                    134 	.globl _AIN11
                                    135 	.globl _TXD_
                                    136 	.globl _AIN12
                                    137 	.globl _RXD2
                                    138 	.globl _AIN13
                                    139 	.globl _TXD2
                                    140 	.globl _AIN14
                                    141 	.globl _RXD3
                                    142 	.globl _AIN15
                                    143 	.globl _TXD3
                                    144 	.globl _P0_0
                                    145 	.globl _P0_1
                                    146 	.globl _P0_2
                                    147 	.globl _P0_3
                                    148 	.globl _P0_4
                                    149 	.globl _P0_5
                                    150 	.globl _P0_6
                                    151 	.globl _P0_7
                                    152 	.globl _IE_SPI0
                                    153 	.globl _IE_INT3
                                    154 	.globl _IE_USB
                                    155 	.globl _IE_UART2
                                    156 	.globl _IE_ADC
                                    157 	.globl _IE_UART1
                                    158 	.globl _IE_UART3
                                    159 	.globl _IE_PWMX
                                    160 	.globl _IE_GPIO
                                    161 	.globl _IE_WDOG
                                    162 	.globl _PX0
                                    163 	.globl _PT0
                                    164 	.globl _PX1
                                    165 	.globl _PT1
                                    166 	.globl _PS
                                    167 	.globl _PT2
                                    168 	.globl _PL_FLAG
                                    169 	.globl _PH_FLAG
                                    170 	.globl _EX0
                                    171 	.globl _ET0
                                    172 	.globl _EX1
                                    173 	.globl _ET1
                                    174 	.globl _ES
                                    175 	.globl _ET2
                                    176 	.globl _E_DIS
                                    177 	.globl _EA
                                    178 	.globl _P
                                    179 	.globl _F1
                                    180 	.globl _OV
                                    181 	.globl _RS0
                                    182 	.globl _RS1
                                    183 	.globl _F0
                                    184 	.globl _AC
                                    185 	.globl _CY
                                    186 	.globl _UEP1_DMA_H
                                    187 	.globl _UEP1_DMA_L
                                    188 	.globl _UEP1_DMA
                                    189 	.globl _UEP0_DMA_H
                                    190 	.globl _UEP0_DMA_L
                                    191 	.globl _UEP0_DMA
                                    192 	.globl _UEP2_3_MOD
                                    193 	.globl _UEP4_1_MOD
                                    194 	.globl _UEP3_DMA_H
                                    195 	.globl _UEP3_DMA_L
                                    196 	.globl _UEP3_DMA
                                    197 	.globl _UEP2_DMA_H
                                    198 	.globl _UEP2_DMA_L
                                    199 	.globl _UEP2_DMA
                                    200 	.globl _USB_DEV_AD
                                    201 	.globl _USB_CTRL
                                    202 	.globl _USB_INT_EN
                                    203 	.globl _UEP4_T_LEN
                                    204 	.globl _UEP4_CTRL
                                    205 	.globl _UEP0_T_LEN
                                    206 	.globl _UEP0_CTRL
                                    207 	.globl _USB_RX_LEN
                                    208 	.globl _USB_MIS_ST
                                    209 	.globl _USB_INT_ST
                                    210 	.globl _USB_INT_FG
                                    211 	.globl _UEP3_T_LEN
                                    212 	.globl _UEP3_CTRL
                                    213 	.globl _UEP2_T_LEN
                                    214 	.globl _UEP2_CTRL
                                    215 	.globl _UEP1_T_LEN
                                    216 	.globl _UEP1_CTRL
                                    217 	.globl _UDEV_CTRL
                                    218 	.globl _USB_C_CTRL
                                    219 	.globl _ADC_PIN
                                    220 	.globl _ADC_CHAN
                                    221 	.globl _ADC_DAT_H
                                    222 	.globl _ADC_DAT_L
                                    223 	.globl _ADC_DAT
                                    224 	.globl _ADC_CFG
                                    225 	.globl _ADC_CTRL
                                    226 	.globl _TKEY_CTRL
                                    227 	.globl _SIF3
                                    228 	.globl _SBAUD3
                                    229 	.globl _SBUF3
                                    230 	.globl _SCON3
                                    231 	.globl _SIF2
                                    232 	.globl _SBAUD2
                                    233 	.globl _SBUF2
                                    234 	.globl _SCON2
                                    235 	.globl _SIF1
                                    236 	.globl _SBAUD1
                                    237 	.globl _SBUF1
                                    238 	.globl _SCON1
                                    239 	.globl _SPI0_SETUP
                                    240 	.globl _SPI0_CK_SE
                                    241 	.globl _SPI0_CTRL
                                    242 	.globl _SPI0_DATA
                                    243 	.globl _SPI0_STAT
                                    244 	.globl _PWM_DATA7
                                    245 	.globl _PWM_DATA6
                                    246 	.globl _PWM_DATA5
                                    247 	.globl _PWM_DATA4
                                    248 	.globl _PWM_DATA3
                                    249 	.globl _PWM_CTRL2
                                    250 	.globl _PWM_CK_SE
                                    251 	.globl _PWM_CTRL
                                    252 	.globl _PWM_DATA0
                                    253 	.globl _PWM_DATA1
                                    254 	.globl _PWM_DATA2
                                    255 	.globl _T2CAP1H
                                    256 	.globl _T2CAP1L
                                    257 	.globl _T2CAP1
                                    258 	.globl _TH2
                                    259 	.globl _TL2
                                    260 	.globl _T2COUNT
                                    261 	.globl _RCAP2H
                                    262 	.globl _RCAP2L
                                    263 	.globl _RCAP2
                                    264 	.globl _T2MOD
                                    265 	.globl _T2CON
                                    266 	.globl _T2CAP0H
                                    267 	.globl _T2CAP0L
                                    268 	.globl _T2CAP0
                                    269 	.globl _T2CON2
                                    270 	.globl _SBUF
                                    271 	.globl _SCON
                                    272 	.globl _TH1
                                    273 	.globl _TH0
                                    274 	.globl _TL1
                                    275 	.globl _TL0
                                    276 	.globl _TMOD
                                    277 	.globl _TCON
                                    278 	.globl _XBUS_AUX
                                    279 	.globl _PIN_FUNC
                                    280 	.globl _P5
                                    281 	.globl _P4_DIR_PU
                                    282 	.globl _P4_MOD_OC
                                    283 	.globl _P4
                                    284 	.globl _P3_DIR_PU
                                    285 	.globl _P3_MOD_OC
                                    286 	.globl _P3
                                    287 	.globl _P2_DIR_PU
                                    288 	.globl _P2_MOD_OC
                                    289 	.globl _P2
                                    290 	.globl _P1_DIR_PU
                                    291 	.globl _P1_MOD_OC
                                    292 	.globl _P1
                                    293 	.globl _P0_DIR_PU
                                    294 	.globl _P0_MOD_OC
                                    295 	.globl _P0
                                    296 	.globl _ROM_CTRL
                                    297 	.globl _ROM_DATA_HH
                                    298 	.globl _ROM_DATA_HL
                                    299 	.globl _ROM_DATA_HI
                                    300 	.globl _ROM_ADDR_H
                                    301 	.globl _ROM_ADDR_L
                                    302 	.globl _ROM_ADDR
                                    303 	.globl _GPIO_IE
                                    304 	.globl _INTX
                                    305 	.globl _IP_EX
                                    306 	.globl _IE_EX
                                    307 	.globl _IP
                                    308 	.globl _IE
                                    309 	.globl _WDOG_COUNT
                                    310 	.globl _RESET_KEEP
                                    311 	.globl _WAKE_CTRL
                                    312 	.globl _CLOCK_CFG
                                    313 	.globl _POWER_CFG
                                    314 	.globl _PCON
                                    315 	.globl _GLOBAL_CFG
                                    316 	.globl _SAFE_MOD
                                    317 	.globl _DPH
                                    318 	.globl _DPL
                                    319 	.globl _SP
                                    320 	.globl _A_INV
                                    321 	.globl _B
                                    322 	.globl _ACC
                                    323 	.globl _PSW
                                    324 	.globl _PWM_SEL_CHANNEL
                                    325 ;--------------------------------------------------------
                                    326 ; special function registers
                                    327 ;--------------------------------------------------------
                                    328 	.area RSEG    (ABS,DATA)
      000000                        329 	.org 0x0000
                           0000D0   330 _PSW	=	0x00d0
                           0000E0   331 _ACC	=	0x00e0
                           0000F0   332 _B	=	0x00f0
                           0000FD   333 _A_INV	=	0x00fd
                           000081   334 _SP	=	0x0081
                           000082   335 _DPL	=	0x0082
                           000083   336 _DPH	=	0x0083
                           0000A1   337 _SAFE_MOD	=	0x00a1
                           0000B1   338 _GLOBAL_CFG	=	0x00b1
                           000087   339 _PCON	=	0x0087
                           0000BA   340 _POWER_CFG	=	0x00ba
                           0000B9   341 _CLOCK_CFG	=	0x00b9
                           0000A9   342 _WAKE_CTRL	=	0x00a9
                           0000FE   343 _RESET_KEEP	=	0x00fe
                           0000FF   344 _WDOG_COUNT	=	0x00ff
                           0000A8   345 _IE	=	0x00a8
                           0000B8   346 _IP	=	0x00b8
                           0000E8   347 _IE_EX	=	0x00e8
                           0000E9   348 _IP_EX	=	0x00e9
                           0000B3   349 _INTX	=	0x00b3
                           0000B2   350 _GPIO_IE	=	0x00b2
                           008584   351 _ROM_ADDR	=	0x8584
                           000084   352 _ROM_ADDR_L	=	0x0084
                           000085   353 _ROM_ADDR_H	=	0x0085
                           008F8E   354 _ROM_DATA_HI	=	0x8f8e
                           00008E   355 _ROM_DATA_HL	=	0x008e
                           00008F   356 _ROM_DATA_HH	=	0x008f
                           000086   357 _ROM_CTRL	=	0x0086
                           000080   358 _P0	=	0x0080
                           0000C4   359 _P0_MOD_OC	=	0x00c4
                           0000C5   360 _P0_DIR_PU	=	0x00c5
                           000090   361 _P1	=	0x0090
                           000092   362 _P1_MOD_OC	=	0x0092
                           000093   363 _P1_DIR_PU	=	0x0093
                           0000A0   364 _P2	=	0x00a0
                           000094   365 _P2_MOD_OC	=	0x0094
                           000095   366 _P2_DIR_PU	=	0x0095
                           0000B0   367 _P3	=	0x00b0
                           000096   368 _P3_MOD_OC	=	0x0096
                           000097   369 _P3_DIR_PU	=	0x0097
                           0000C0   370 _P4	=	0x00c0
                           0000C2   371 _P4_MOD_OC	=	0x00c2
                           0000C3   372 _P4_DIR_PU	=	0x00c3
                           0000AB   373 _P5	=	0x00ab
                           0000AA   374 _PIN_FUNC	=	0x00aa
                           0000A2   375 _XBUS_AUX	=	0x00a2
                           000088   376 _TCON	=	0x0088
                           000089   377 _TMOD	=	0x0089
                           00008A   378 _TL0	=	0x008a
                           00008B   379 _TL1	=	0x008b
                           00008C   380 _TH0	=	0x008c
                           00008D   381 _TH1	=	0x008d
                           000098   382 _SCON	=	0x0098
                           000099   383 _SBUF	=	0x0099
                           0000C1   384 _T2CON2	=	0x00c1
                           00C7C6   385 _T2CAP0	=	0xc7c6
                           0000C6   386 _T2CAP0L	=	0x00c6
                           0000C7   387 _T2CAP0H	=	0x00c7
                           0000C8   388 _T2CON	=	0x00c8
                           0000C9   389 _T2MOD	=	0x00c9
                           00CBCA   390 _RCAP2	=	0xcbca
                           0000CA   391 _RCAP2L	=	0x00ca
                           0000CB   392 _RCAP2H	=	0x00cb
                           00CDCC   393 _T2COUNT	=	0xcdcc
                           0000CC   394 _TL2	=	0x00cc
                           0000CD   395 _TH2	=	0x00cd
                           00CFCE   396 _T2CAP1	=	0xcfce
                           0000CE   397 _T2CAP1L	=	0x00ce
                           0000CF   398 _T2CAP1H	=	0x00cf
                           00009A   399 _PWM_DATA2	=	0x009a
                           00009B   400 _PWM_DATA1	=	0x009b
                           00009C   401 _PWM_DATA0	=	0x009c
                           00009D   402 _PWM_CTRL	=	0x009d
                           00009E   403 _PWM_CK_SE	=	0x009e
                           00009F   404 _PWM_CTRL2	=	0x009f
                           0000A3   405 _PWM_DATA3	=	0x00a3
                           0000A4   406 _PWM_DATA4	=	0x00a4
                           0000A5   407 _PWM_DATA5	=	0x00a5
                           0000A6   408 _PWM_DATA6	=	0x00a6
                           0000A7   409 _PWM_DATA7	=	0x00a7
                           0000F8   410 _SPI0_STAT	=	0x00f8
                           0000F9   411 _SPI0_DATA	=	0x00f9
                           0000FA   412 _SPI0_CTRL	=	0x00fa
                           0000FB   413 _SPI0_CK_SE	=	0x00fb
                           0000FC   414 _SPI0_SETUP	=	0x00fc
                           0000BC   415 _SCON1	=	0x00bc
                           0000BD   416 _SBUF1	=	0x00bd
                           0000BE   417 _SBAUD1	=	0x00be
                           0000BF   418 _SIF1	=	0x00bf
                           0000B4   419 _SCON2	=	0x00b4
                           0000B5   420 _SBUF2	=	0x00b5
                           0000B6   421 _SBAUD2	=	0x00b6
                           0000B7   422 _SIF2	=	0x00b7
                           0000AC   423 _SCON3	=	0x00ac
                           0000AD   424 _SBUF3	=	0x00ad
                           0000AE   425 _SBAUD3	=	0x00ae
                           0000AF   426 _SIF3	=	0x00af
                           0000F1   427 _TKEY_CTRL	=	0x00f1
                           0000F2   428 _ADC_CTRL	=	0x00f2
                           0000F3   429 _ADC_CFG	=	0x00f3
                           00F5F4   430 _ADC_DAT	=	0xf5f4
                           0000F4   431 _ADC_DAT_L	=	0x00f4
                           0000F5   432 _ADC_DAT_H	=	0x00f5
                           0000F6   433 _ADC_CHAN	=	0x00f6
                           0000F7   434 _ADC_PIN	=	0x00f7
                           000091   435 _USB_C_CTRL	=	0x0091
                           0000D1   436 _UDEV_CTRL	=	0x00d1
                           0000D2   437 _UEP1_CTRL	=	0x00d2
                           0000D3   438 _UEP1_T_LEN	=	0x00d3
                           0000D4   439 _UEP2_CTRL	=	0x00d4
                           0000D5   440 _UEP2_T_LEN	=	0x00d5
                           0000D6   441 _UEP3_CTRL	=	0x00d6
                           0000D7   442 _UEP3_T_LEN	=	0x00d7
                           0000D8   443 _USB_INT_FG	=	0x00d8
                           0000D9   444 _USB_INT_ST	=	0x00d9
                           0000DA   445 _USB_MIS_ST	=	0x00da
                           0000DB   446 _USB_RX_LEN	=	0x00db
                           0000DC   447 _UEP0_CTRL	=	0x00dc
                           0000DD   448 _UEP0_T_LEN	=	0x00dd
                           0000DE   449 _UEP4_CTRL	=	0x00de
                           0000DF   450 _UEP4_T_LEN	=	0x00df
                           0000E1   451 _USB_INT_EN	=	0x00e1
                           0000E2   452 _USB_CTRL	=	0x00e2
                           0000E3   453 _USB_DEV_AD	=	0x00e3
                           00E5E4   454 _UEP2_DMA	=	0xe5e4
                           0000E4   455 _UEP2_DMA_L	=	0x00e4
                           0000E5   456 _UEP2_DMA_H	=	0x00e5
                           00E7E6   457 _UEP3_DMA	=	0xe7e6
                           0000E6   458 _UEP3_DMA_L	=	0x00e6
                           0000E7   459 _UEP3_DMA_H	=	0x00e7
                           0000EA   460 _UEP4_1_MOD	=	0x00ea
                           0000EB   461 _UEP2_3_MOD	=	0x00eb
                           00EDEC   462 _UEP0_DMA	=	0xedec
                           0000EC   463 _UEP0_DMA_L	=	0x00ec
                           0000ED   464 _UEP0_DMA_H	=	0x00ed
                           00EFEE   465 _UEP1_DMA	=	0xefee
                           0000EE   466 _UEP1_DMA_L	=	0x00ee
                           0000EF   467 _UEP1_DMA_H	=	0x00ef
                                    468 ;--------------------------------------------------------
                                    469 ; special function bits
                                    470 ;--------------------------------------------------------
                                    471 	.area RSEG    (ABS,DATA)
      000000                        472 	.org 0x0000
                           0000D7   473 _CY	=	0x00d7
                           0000D6   474 _AC	=	0x00d6
                           0000D5   475 _F0	=	0x00d5
                           0000D4   476 _RS1	=	0x00d4
                           0000D3   477 _RS0	=	0x00d3
                           0000D2   478 _OV	=	0x00d2
                           0000D1   479 _F1	=	0x00d1
                           0000D0   480 _P	=	0x00d0
                           0000AF   481 _EA	=	0x00af
                           0000AE   482 _E_DIS	=	0x00ae
                           0000AD   483 _ET2	=	0x00ad
                           0000AC   484 _ES	=	0x00ac
                           0000AB   485 _ET1	=	0x00ab
                           0000AA   486 _EX1	=	0x00aa
                           0000A9   487 _ET0	=	0x00a9
                           0000A8   488 _EX0	=	0x00a8
                           0000BF   489 _PH_FLAG	=	0x00bf
                           0000BE   490 _PL_FLAG	=	0x00be
                           0000BD   491 _PT2	=	0x00bd
                           0000BC   492 _PS	=	0x00bc
                           0000BB   493 _PT1	=	0x00bb
                           0000BA   494 _PX1	=	0x00ba
                           0000B9   495 _PT0	=	0x00b9
                           0000B8   496 _PX0	=	0x00b8
                           0000EF   497 _IE_WDOG	=	0x00ef
                           0000EE   498 _IE_GPIO	=	0x00ee
                           0000ED   499 _IE_PWMX	=	0x00ed
                           0000ED   500 _IE_UART3	=	0x00ed
                           0000EC   501 _IE_UART1	=	0x00ec
                           0000EB   502 _IE_ADC	=	0x00eb
                           0000EB   503 _IE_UART2	=	0x00eb
                           0000EA   504 _IE_USB	=	0x00ea
                           0000E9   505 _IE_INT3	=	0x00e9
                           0000E8   506 _IE_SPI0	=	0x00e8
                           000087   507 _P0_7	=	0x0087
                           000086   508 _P0_6	=	0x0086
                           000085   509 _P0_5	=	0x0085
                           000084   510 _P0_4	=	0x0084
                           000083   511 _P0_3	=	0x0083
                           000082   512 _P0_2	=	0x0082
                           000081   513 _P0_1	=	0x0081
                           000080   514 _P0_0	=	0x0080
                           000087   515 _TXD3	=	0x0087
                           000087   516 _AIN15	=	0x0087
                           000086   517 _RXD3	=	0x0086
                           000086   518 _AIN14	=	0x0086
                           000085   519 _TXD2	=	0x0085
                           000085   520 _AIN13	=	0x0085
                           000084   521 _RXD2	=	0x0084
                           000084   522 _AIN12	=	0x0084
                           000083   523 _TXD_	=	0x0083
                           000083   524 _AIN11	=	0x0083
                           000082   525 _RXD_	=	0x0082
                           000082   526 _AIN10	=	0x0082
                           000081   527 _AIN9	=	0x0081
                           000080   528 _AIN8	=	0x0080
                           000097   529 _P1_7	=	0x0097
                           000096   530 _P1_6	=	0x0096
                           000095   531 _P1_5	=	0x0095
                           000094   532 _P1_4	=	0x0094
                           000093   533 _P1_3	=	0x0093
                           000092   534 _P1_2	=	0x0092
                           000091   535 _P1_1	=	0x0091
                           000090   536 _P1_0	=	0x0090
                           000097   537 _SCK	=	0x0097
                           000097   538 _TXD1_	=	0x0097
                           000097   539 _AIN7	=	0x0097
                           000096   540 _MISO	=	0x0096
                           000096   541 _RXD1_	=	0x0096
                           000096   542 _VBUS	=	0x0096
                           000096   543 _AIN6	=	0x0096
                           000095   544 _MOSI	=	0x0095
                           000095   545 _PWM0_	=	0x0095
                           000095   546 _UCC2	=	0x0095
                           000095   547 _AIN5	=	0x0095
                           000094   548 _SCS	=	0x0094
                           000094   549 _UCC1	=	0x0094
                           000094   550 _AIN4	=	0x0094
                           000093   551 _AIN3	=	0x0093
                           000092   552 _AIN2	=	0x0092
                           000091   553 _T2EX	=	0x0091
                           000091   554 _CAP2	=	0x0091
                           000091   555 _AIN1	=	0x0091
                           000090   556 _T2	=	0x0090
                           000090   557 _CAP1	=	0x0090
                           000090   558 _AIN0	=	0x0090
                           0000A7   559 _P2_7	=	0x00a7
                           0000A6   560 _P2_6	=	0x00a6
                           0000A5   561 _P2_5	=	0x00a5
                           0000A4   562 _P2_4	=	0x00a4
                           0000A3   563 _P2_3	=	0x00a3
                           0000A2   564 _P2_2	=	0x00a2
                           0000A1   565 _P2_1	=	0x00a1
                           0000A0   566 _P2_0	=	0x00a0
                           0000A7   567 _PWM7	=	0x00a7
                           0000A7   568 _TXD1	=	0x00a7
                           0000A6   569 _PWM6	=	0x00a6
                           0000A6   570 _RXD1	=	0x00a6
                           0000A5   571 _PWM0	=	0x00a5
                           0000A5   572 _T2EX_	=	0x00a5
                           0000A5   573 _CAP2_	=	0x00a5
                           0000A4   574 _PWM1	=	0x00a4
                           0000A4   575 _T2_	=	0x00a4
                           0000A4   576 _CAP1_	=	0x00a4
                           0000A3   577 _PWM2	=	0x00a3
                           0000A2   578 _PWM3	=	0x00a2
                           0000A2   579 _INT0_	=	0x00a2
                           0000A1   580 _PWM4	=	0x00a1
                           0000A0   581 _PWM5	=	0x00a0
                           0000B7   582 _P3_7	=	0x00b7
                           0000B6   583 _P3_6	=	0x00b6
                           0000B5   584 _P3_5	=	0x00b5
                           0000B4   585 _P3_4	=	0x00b4
                           0000B3   586 _P3_3	=	0x00b3
                           0000B2   587 _P3_2	=	0x00b2
                           0000B1   588 _P3_1	=	0x00b1
                           0000B0   589 _P3_0	=	0x00b0
                           0000B7   590 _INT3	=	0x00b7
                           0000B6   591 _CAP0	=	0x00b6
                           0000B5   592 _T1	=	0x00b5
                           0000B4   593 _T0	=	0x00b4
                           0000B3   594 _INT1	=	0x00b3
                           0000B2   595 _INT0	=	0x00b2
                           0000B1   596 _TXD	=	0x00b1
                           0000B0   597 _RXD	=	0x00b0
                           0000C6   598 _P4_6	=	0x00c6
                           0000C5   599 _P4_5	=	0x00c5
                           0000C4   600 _P4_4	=	0x00c4
                           0000C3   601 _P4_3	=	0x00c3
                           0000C2   602 _P4_2	=	0x00c2
                           0000C1   603 _P4_1	=	0x00c1
                           0000C0   604 _P4_0	=	0x00c0
                           0000C7   605 _XO	=	0x00c7
                           0000C6   606 _XI	=	0x00c6
                           00008F   607 _TF1	=	0x008f
                           00008E   608 _TR1	=	0x008e
                           00008D   609 _TF0	=	0x008d
                           00008C   610 _TR0	=	0x008c
                           00008B   611 _IE1	=	0x008b
                           00008A   612 _IT1	=	0x008a
                           000089   613 _IE0	=	0x0089
                           000088   614 _IT0	=	0x0088
                           00009F   615 _SM0	=	0x009f
                           00009E   616 _SM1	=	0x009e
                           00009D   617 _SM2	=	0x009d
                           00009C   618 _REN	=	0x009c
                           00009B   619 _TB8	=	0x009b
                           00009A   620 _RB8	=	0x009a
                           000099   621 _TI	=	0x0099
                           000098   622 _RI	=	0x0098
                           0000CF   623 _TF2	=	0x00cf
                           0000CF   624 _CAP1F	=	0x00cf
                           0000CE   625 _EXF2	=	0x00ce
                           0000CD   626 _RCLK	=	0x00cd
                           0000CC   627 _TCLK	=	0x00cc
                           0000CB   628 _EXEN2	=	0x00cb
                           0000CA   629 _TR2	=	0x00ca
                           0000C9   630 _C_T2	=	0x00c9
                           0000C8   631 _CP_RL2	=	0x00c8
                           0000FF   632 _S0_FST_ACT	=	0x00ff
                           0000FE   633 _S0_IF_OV	=	0x00fe
                           0000FD   634 _S0_IF_FIRST	=	0x00fd
                           0000FC   635 _S0_IF_BYTE	=	0x00fc
                           0000FB   636 _S0_FREE	=	0x00fb
                           0000FA   637 _S0_T_FIFO	=	0x00fa
                           0000F8   638 _S0_R_FIFO	=	0x00f8
                           0000DF   639 _U_IS_NAK	=	0x00df
                           0000DE   640 _U_TOG_OK	=	0x00de
                           0000DD   641 _U_SIE_FREE	=	0x00dd
                           0000DC   642 _UIF_FIFO_OV	=	0x00dc
                           0000DB   643 _UIF_HST_SOF	=	0x00db
                           0000DA   644 _UIF_SUSPEND	=	0x00da
                           0000D9   645 _UIF_TRANSFER	=	0x00d9
                           0000D8   646 _UIF_DETECT	=	0x00d8
                           0000D8   647 _UIF_BUS_RST	=	0x00d8
                                    648 ;--------------------------------------------------------
                                    649 ; overlayable register banks
                                    650 ;--------------------------------------------------------
                                    651 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        652 	.ds 8
                                    653 ;--------------------------------------------------------
                                    654 ; internal ram data
                                    655 ;--------------------------------------------------------
                                    656 	.area DSEG    (DATA)
                                    657 ;--------------------------------------------------------
                                    658 ; overlayable items in internal ram 
                                    659 ;--------------------------------------------------------
                                    660 ;--------------------------------------------------------
                                    661 ; indirectly addressable internal ram data
                                    662 ;--------------------------------------------------------
                                    663 	.area ISEG    (DATA)
                                    664 ;--------------------------------------------------------
                                    665 ; absolute internal ram data
                                    666 ;--------------------------------------------------------
                                    667 	.area IABS    (ABS,DATA)
                                    668 	.area IABS    (ABS,DATA)
                                    669 ;--------------------------------------------------------
                                    670 ; bit data
                                    671 ;--------------------------------------------------------
                                    672 	.area BSEG    (BIT)
                                    673 ;--------------------------------------------------------
                                    674 ; paged external ram data
                                    675 ;--------------------------------------------------------
                                    676 	.area PSEG    (PAG,XDATA)
                                    677 ;--------------------------------------------------------
                                    678 ; external ram data
                                    679 ;--------------------------------------------------------
                                    680 	.area XSEG    (XDATA)
                                    681 ;--------------------------------------------------------
                                    682 ; absolute external ram data
                                    683 ;--------------------------------------------------------
                                    684 	.area XABS    (ABS,XDATA)
                                    685 ;--------------------------------------------------------
                                    686 ; external initialized ram data
                                    687 ;--------------------------------------------------------
                                    688 	.area HOME    (CODE)
                                    689 	.area GSINIT0 (CODE)
                                    690 	.area GSINIT1 (CODE)
                                    691 	.area GSINIT2 (CODE)
                                    692 	.area GSINIT3 (CODE)
                                    693 	.area GSINIT4 (CODE)
                                    694 	.area GSINIT5 (CODE)
                                    695 	.area GSINIT  (CODE)
                                    696 	.area GSFINAL (CODE)
                                    697 	.area CSEG    (CODE)
                                    698 ;--------------------------------------------------------
                                    699 ; global & static initialisations
                                    700 ;--------------------------------------------------------
                                    701 	.area HOME    (CODE)
                                    702 	.area GSINIT  (CODE)
                                    703 	.area GSFINAL (CODE)
                                    704 	.area GSINIT  (CODE)
                                    705 ;--------------------------------------------------------
                                    706 ; Home
                                    707 ;--------------------------------------------------------
                                    708 	.area HOME    (CODE)
                                    709 	.area HOME    (CODE)
                                    710 ;--------------------------------------------------------
                                    711 ; code
                                    712 ;--------------------------------------------------------
                                    713 	.area CSEG    (CODE)
                                    714 ;------------------------------------------------------------
                                    715 ;Allocation info for local variables in function 'PWM_SEL_CHANNEL'
                                    716 ;------------------------------------------------------------
                                    717 ;NewState                  Allocated to stack - _bp -3
                                    718 ;Channel                   Allocated to registers r7 
                                    719 ;i                         Allocated to registers r6 
                                    720 ;------------------------------------------------------------
                                    721 ;	source/CH549_PWM.c:27: void PWM_SEL_CHANNEL(UINT8 Channel,UINT8 NewState)
                                    722 ;	-----------------------------------------
                                    723 ;	 function PWM_SEL_CHANNEL
                                    724 ;	-----------------------------------------
      0014DD                        725 _PWM_SEL_CHANNEL:
                           000007   726 	ar7 = 0x07
                           000006   727 	ar6 = 0x06
                           000005   728 	ar5 = 0x05
                           000004   729 	ar4 = 0x04
                           000003   730 	ar3 = 0x03
                           000002   731 	ar2 = 0x02
                           000001   732 	ar1 = 0x01
                           000000   733 	ar0 = 0x00
      0014DD C0 16            [24]  734 	push	_bp
      0014DF 85 81 16         [24]  735 	mov	_bp,sp
      0014E2 AF 82            [24]  736 	mov	r7,dpl
                                    737 ;	source/CH549_PWM.c:31: if(NewState == Enable)                    //输出开启
      0014E4 E5 16            [12]  738 	mov	a,_bp
      0014E6 24 FD            [12]  739 	add	a,#0xfd
      0014E8 F8               [12]  740 	mov	r0,a
      0014E9 B6 01 75         [24]  741 	cjne	@r0,#0x01,00117$
                                    742 ;	source/CH549_PWM.c:33: PWM_CTRL &= ~bPWM_CLR_ALL;
      0014EC 53 9D FD         [24]  743 	anl	_PWM_CTRL,#0xfd
                                    744 ;	source/CH549_PWM.c:34: if(Channel&PWM_CH0)
      0014EF EF               [12]  745 	mov	a,r7
      0014F0 30 E0 03         [24]  746 	jnb	acc.0,00102$
                                    747 ;	source/CH549_PWM.c:36: PWM_CTRL |= bPWM0_OUT_EN;
      0014F3 43 9D 04         [24]  748 	orl	_PWM_CTRL,#0x04
      0014F6                        749 00102$:
                                    750 ;	source/CH549_PWM.c:38: if(Channel&PWM_CH1)
      0014F6 EF               [12]  751 	mov	a,r7
      0014F7 30 E1 03         [24]  752 	jnb	acc.1,00104$
                                    753 ;	source/CH549_PWM.c:40: PWM_CTRL |= bPWM1_OUT_EN;
      0014FA 43 9D 08         [24]  754 	orl	_PWM_CTRL,#0x08
      0014FD                        755 00104$:
                                    756 ;	source/CH549_PWM.c:42: PWM_CTRL2 = (Channel>>2);
      0014FD EF               [12]  757 	mov	a,r7
      0014FE 03               [12]  758 	rr	a
      0014FF 03               [12]  759 	rr	a
      001500 54 3F            [12]  760 	anl	a,#0x3f
      001502 F5 9F            [12]  761 	mov	_PWM_CTRL2,a
                                    762 ;	source/CH549_PWM.c:44: for(i=0; i!=6; i++)
      001504 7E 00            [12]  763 	mov	r6,#0x00
      001506                        764 00119$:
                                    765 ;	source/CH549_PWM.c:46: if(Channel & (1<<i))
      001506 8E F0            [24]  766 	mov	b,r6
      001508 05 F0            [12]  767 	inc	b
      00150A 7C 01            [12]  768 	mov	r4,#0x01
      00150C 7D 00            [12]  769 	mov	r5,#0x00
      00150E 80 06            [24]  770 	sjmp	00169$
      001510                        771 00168$:
      001510 EC               [12]  772 	mov	a,r4
      001511 2C               [12]  773 	add	a,r4
      001512 FC               [12]  774 	mov	r4,a
      001513 ED               [12]  775 	mov	a,r5
      001514 33               [12]  776 	rlc	a
      001515 FD               [12]  777 	mov	r5,a
      001516                        778 00169$:
      001516 D5 F0 F7         [24]  779 	djnz	b,00168$
      001519 8F 02            [24]  780 	mov	ar2,r7
      00151B 7B 00            [12]  781 	mov	r3,#0x00
      00151D EA               [12]  782 	mov	a,r2
      00151E 52 04            [12]  783 	anl	ar4,a
      001520 EB               [12]  784 	mov	a,r3
      001521 52 05            [12]  785 	anl	ar5,a
      001523 EC               [12]  786 	mov	a,r4
      001524 4D               [12]  787 	orl	a,r5
      001525 60 20            [24]  788 	jz	00120$
                                    789 ;	source/CH549_PWM.c:48: P2_MOD_OC &= ~(1<<(5-i));
      001527 8E 05            [24]  790 	mov	ar5,r6
      001529 74 05            [12]  791 	mov	a,#0x05
      00152B C3               [12]  792 	clr	c
      00152C 9D               [12]  793 	subb	a,r5
      00152D F5 F0            [12]  794 	mov	b,a
      00152F 05 F0            [12]  795 	inc	b
      001531 74 01            [12]  796 	mov	a,#0x01
      001533 80 02            [24]  797 	sjmp	00173$
      001535                        798 00171$:
      001535 25 E0            [12]  799 	add	a,acc
      001537                        800 00173$:
      001537 D5 F0 FB         [24]  801 	djnz	b,00171$
      00153A FD               [12]  802 	mov	r5,a
      00153B F4               [12]  803 	cpl	a
      00153C AB 94            [24]  804 	mov	r3,_P2_MOD_OC
      00153E 5B               [12]  805 	anl	a,r3
      00153F F5 94            [12]  806 	mov	_P2_MOD_OC,a
                                    807 ;	source/CH549_PWM.c:49: P2_DIR_PU |= (1<<(5-i));
      001541 AC 95            [24]  808 	mov	r4,_P2_DIR_PU
      001543 ED               [12]  809 	mov	a,r5
      001544 4C               [12]  810 	orl	a,r4
      001545 F5 95            [12]  811 	mov	_P2_DIR_PU,a
      001547                        812 00120$:
                                    813 ;	source/CH549_PWM.c:44: for(i=0; i!=6; i++)
      001547 0E               [12]  814 	inc	r6
      001548 BE 06 BB         [24]  815 	cjne	r6,#0x06,00119$
                                    816 ;	source/CH549_PWM.c:52: if(Channel&PWM_CH6)
      00154B EF               [12]  817 	mov	a,r7
      00154C 30 E6 06         [24]  818 	jnb	acc.6,00109$
                                    819 ;	source/CH549_PWM.c:54: P2_MOD_OC &= ~PWM_CH6;
      00154F 53 94 BF         [24]  820 	anl	_P2_MOD_OC,#0xbf
                                    821 ;	source/CH549_PWM.c:55: P2_DIR_PU |= PWM_CH6;
      001552 43 95 40         [24]  822 	orl	_P2_DIR_PU,#0x40
      001555                        823 00109$:
                                    824 ;	source/CH549_PWM.c:57: if(Channel&PWM_CH7)
      001555 EF               [12]  825 	mov	a,r7
      001556 30 E7 1F         [24]  826 	jnb	acc.7,00121$
                                    827 ;	source/CH549_PWM.c:59: P2_MOD_OC &= ~PWM_CH7;
      001559 53 94 7F         [24]  828 	anl	_P2_MOD_OC,#0x7f
                                    829 ;	source/CH549_PWM.c:60: P2_DIR_PU |= PWM_CH7;
      00155C 43 95 80         [24]  830 	orl	_P2_DIR_PU,#0x80
      00155F 80 17            [24]  831 	sjmp	00121$
      001561                        832 00117$:
                                    833 ;	source/CH549_PWM.c:65: if(Channel&PWM_CH0)
      001561 EF               [12]  834 	mov	a,r7
      001562 30 E0 03         [24]  835 	jnb	acc.0,00113$
                                    836 ;	source/CH549_PWM.c:67: PWM_CTRL &= ~bPWM0_OUT_EN;
      001565 53 9D FB         [24]  837 	anl	_PWM_CTRL,#0xfb
      001568                        838 00113$:
                                    839 ;	source/CH549_PWM.c:69: if(Channel&PWM_CH1)
      001568 EF               [12]  840 	mov	a,r7
      001569 30 E1 03         [24]  841 	jnb	acc.1,00115$
                                    842 ;	source/CH549_PWM.c:71: PWM_CTRL &= ~bPWM1_OUT_EN;
      00156C 53 9D F7         [24]  843 	anl	_PWM_CTRL,#0xf7
      00156F                        844 00115$:
                                    845 ;	source/CH549_PWM.c:73: PWM_CTRL2 &= ~(Channel>>2);
      00156F EF               [12]  846 	mov	a,r7
      001570 03               [12]  847 	rr	a
      001571 03               [12]  848 	rr	a
      001572 54 3F            [12]  849 	anl	a,#0x3f
      001574 F4               [12]  850 	cpl	a
      001575 FF               [12]  851 	mov	r7,a
      001576 52 9F            [12]  852 	anl	_PWM_CTRL2,a
      001578                        853 00121$:
                                    854 ;	source/CH549_PWM.c:75: }
      001578 D0 16            [24]  855 	pop	_bp
      00157A 22               [24]  856 	ret
                                    857 	.area CSEG    (CODE)
                                    858 	.area CONST   (CODE)
                                    859 	.area CABS    (ABS,CODE)
