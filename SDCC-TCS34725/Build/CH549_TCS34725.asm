;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.0.0 #11528 (MINGW64)
;--------------------------------------------------------
	.module CH549_TCS34725
	.optsdcc -mmcs51 --model-large
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _TCS34725_Write
	.globl _IIC_Read_Byte
	.globl _IIC_Send_Byte
	.globl _IIC_Wait_Ack
	.globl _IIC_Stop
	.globl _IIC_Start
	.globl _IIC_Init
	.globl _UIF_BUS_RST
	.globl _UIF_DETECT
	.globl _UIF_TRANSFER
	.globl _UIF_SUSPEND
	.globl _UIF_HST_SOF
	.globl _UIF_FIFO_OV
	.globl _U_SIE_FREE
	.globl _U_TOG_OK
	.globl _U_IS_NAK
	.globl _S0_R_FIFO
	.globl _S0_T_FIFO
	.globl _S0_FREE
	.globl _S0_IF_BYTE
	.globl _S0_IF_FIRST
	.globl _S0_IF_OV
	.globl _S0_FST_ACT
	.globl _CP_RL2
	.globl _C_T2
	.globl _TR2
	.globl _EXEN2
	.globl _TCLK
	.globl _RCLK
	.globl _EXF2
	.globl _CAP1F
	.globl _TF2
	.globl _RI
	.globl _TI
	.globl _RB8
	.globl _TB8
	.globl _REN
	.globl _SM2
	.globl _SM1
	.globl _SM0
	.globl _IT0
	.globl _IE0
	.globl _IT1
	.globl _IE1
	.globl _TR0
	.globl _TF0
	.globl _TR1
	.globl _TF1
	.globl _XI
	.globl _XO
	.globl _P4_0
	.globl _P4_1
	.globl _P4_2
	.globl _P4_3
	.globl _P4_4
	.globl _P4_5
	.globl _P4_6
	.globl _RXD
	.globl _TXD
	.globl _INT0
	.globl _INT1
	.globl _T0
	.globl _T1
	.globl _CAP0
	.globl _INT3
	.globl _P3_0
	.globl _P3_1
	.globl _P3_2
	.globl _P3_3
	.globl _P3_4
	.globl _P3_5
	.globl _P3_6
	.globl _P3_7
	.globl _PWM5
	.globl _PWM4
	.globl _INT0_
	.globl _PWM3
	.globl _PWM2
	.globl _CAP1_
	.globl _T2_
	.globl _PWM1
	.globl _CAP2_
	.globl _T2EX_
	.globl _PWM0
	.globl _RXD1
	.globl _PWM6
	.globl _TXD1
	.globl _PWM7
	.globl _P2_0
	.globl _P2_1
	.globl _P2_2
	.globl _P2_3
	.globl _P2_4
	.globl _P2_5
	.globl _P2_6
	.globl _P2_7
	.globl _AIN0
	.globl _CAP1
	.globl _T2
	.globl _AIN1
	.globl _CAP2
	.globl _T2EX
	.globl _AIN2
	.globl _AIN3
	.globl _AIN4
	.globl _UCC1
	.globl _SCS
	.globl _AIN5
	.globl _UCC2
	.globl _PWM0_
	.globl _MOSI
	.globl _AIN6
	.globl _VBUS
	.globl _RXD1_
	.globl _MISO
	.globl _AIN7
	.globl _TXD1_
	.globl _SCK
	.globl _P1_0
	.globl _P1_1
	.globl _P1_2
	.globl _P1_3
	.globl _P1_4
	.globl _P1_5
	.globl _P1_6
	.globl _P1_7
	.globl _AIN8
	.globl _AIN9
	.globl _AIN10
	.globl _RXD_
	.globl _AIN11
	.globl _TXD_
	.globl _AIN12
	.globl _RXD2
	.globl _AIN13
	.globl _TXD2
	.globl _AIN14
	.globl _RXD3
	.globl _AIN15
	.globl _TXD3
	.globl _P0_0
	.globl _P0_1
	.globl _P0_2
	.globl _P0_3
	.globl _P0_4
	.globl _P0_5
	.globl _P0_6
	.globl _P0_7
	.globl _IE_SPI0
	.globl _IE_INT3
	.globl _IE_USB
	.globl _IE_UART2
	.globl _IE_ADC
	.globl _IE_UART1
	.globl _IE_UART3
	.globl _IE_PWMX
	.globl _IE_GPIO
	.globl _IE_WDOG
	.globl _PX0
	.globl _PT0
	.globl _PX1
	.globl _PT1
	.globl _PS
	.globl _PT2
	.globl _PL_FLAG
	.globl _PH_FLAG
	.globl _EX0
	.globl _ET0
	.globl _EX1
	.globl _ET1
	.globl _ES
	.globl _ET2
	.globl _E_DIS
	.globl _EA
	.globl _P
	.globl _F1
	.globl _OV
	.globl _RS0
	.globl _RS1
	.globl _F0
	.globl _AC
	.globl _CY
	.globl _UEP1_DMA_H
	.globl _UEP1_DMA_L
	.globl _UEP1_DMA
	.globl _UEP0_DMA_H
	.globl _UEP0_DMA_L
	.globl _UEP0_DMA
	.globl _UEP2_3_MOD
	.globl _UEP4_1_MOD
	.globl _UEP3_DMA_H
	.globl _UEP3_DMA_L
	.globl _UEP3_DMA
	.globl _UEP2_DMA_H
	.globl _UEP2_DMA_L
	.globl _UEP2_DMA
	.globl _USB_DEV_AD
	.globl _USB_CTRL
	.globl _USB_INT_EN
	.globl _UEP4_T_LEN
	.globl _UEP4_CTRL
	.globl _UEP0_T_LEN
	.globl _UEP0_CTRL
	.globl _USB_RX_LEN
	.globl _USB_MIS_ST
	.globl _USB_INT_ST
	.globl _USB_INT_FG
	.globl _UEP3_T_LEN
	.globl _UEP3_CTRL
	.globl _UEP2_T_LEN
	.globl _UEP2_CTRL
	.globl _UEP1_T_LEN
	.globl _UEP1_CTRL
	.globl _UDEV_CTRL
	.globl _USB_C_CTRL
	.globl _ADC_PIN
	.globl _ADC_CHAN
	.globl _ADC_DAT_H
	.globl _ADC_DAT_L
	.globl _ADC_DAT
	.globl _ADC_CFG
	.globl _ADC_CTRL
	.globl _TKEY_CTRL
	.globl _SIF3
	.globl _SBAUD3
	.globl _SBUF3
	.globl _SCON3
	.globl _SIF2
	.globl _SBAUD2
	.globl _SBUF2
	.globl _SCON2
	.globl _SIF1
	.globl _SBAUD1
	.globl _SBUF1
	.globl _SCON1
	.globl _SPI0_SETUP
	.globl _SPI0_CK_SE
	.globl _SPI0_CTRL
	.globl _SPI0_DATA
	.globl _SPI0_STAT
	.globl _PWM_DATA7
	.globl _PWM_DATA6
	.globl _PWM_DATA5
	.globl _PWM_DATA4
	.globl _PWM_DATA3
	.globl _PWM_CTRL2
	.globl _PWM_CK_SE
	.globl _PWM_CTRL
	.globl _PWM_DATA0
	.globl _PWM_DATA1
	.globl _PWM_DATA2
	.globl _T2CAP1H
	.globl _T2CAP1L
	.globl _T2CAP1
	.globl _TH2
	.globl _TL2
	.globl _T2COUNT
	.globl _RCAP2H
	.globl _RCAP2L
	.globl _RCAP2
	.globl _T2MOD
	.globl _T2CON
	.globl _T2CAP0H
	.globl _T2CAP0L
	.globl _T2CAP0
	.globl _T2CON2
	.globl _SBUF
	.globl _SCON
	.globl _TH1
	.globl _TH0
	.globl _TL1
	.globl _TL0
	.globl _TMOD
	.globl _TCON
	.globl _XBUS_AUX
	.globl _PIN_FUNC
	.globl _P5
	.globl _P4_DIR_PU
	.globl _P4_MOD_OC
	.globl _P4
	.globl _P3_DIR_PU
	.globl _P3_MOD_OC
	.globl _P3
	.globl _P2_DIR_PU
	.globl _P2_MOD_OC
	.globl _P2
	.globl _P1_DIR_PU
	.globl _P1_MOD_OC
	.globl _P1
	.globl _P0_DIR_PU
	.globl _P0_MOD_OC
	.globl _P0
	.globl _ROM_CTRL
	.globl _ROM_DATA_HH
	.globl _ROM_DATA_HL
	.globl _ROM_DATA_HI
	.globl _ROM_ADDR_H
	.globl _ROM_ADDR_L
	.globl _ROM_ADDR
	.globl _GPIO_IE
	.globl _INTX
	.globl _IP_EX
	.globl _IE_EX
	.globl _IP
	.globl _IE
	.globl _WDOG_COUNT
	.globl _RESET_KEEP
	.globl _WAKE_CTRL
	.globl _CLOCK_CFG
	.globl _POWER_CFG
	.globl _PCON
	.globl _GLOBAL_CFG
	.globl _SAFE_MOD
	.globl _DPH
	.globl _DPL
	.globl _SP
	.globl _A_INV
	.globl _B
	.globl _ACC
	.globl _PSW
	.globl _TCS34725_I2C_Write
	.globl _TCS34725_I2C_Read
	.globl _TCS34725_Read
	.globl _TCS34725_SetIntegrationTime
	.globl _TCS34725_SetGain
	.globl _TCS34725_Enable
	.globl _TCS34725_Disable
	.globl _TCS34725_Init
	.globl _TCS34725_GetChannelData
	.globl _TCS34725_GetRawData
	.globl _RGBtoHSL
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
	.area RSEG    (ABS,DATA)
	.org 0x0000
_PSW	=	0x00d0
_ACC	=	0x00e0
_B	=	0x00f0
_A_INV	=	0x00fd
_SP	=	0x0081
_DPL	=	0x0082
_DPH	=	0x0083
_SAFE_MOD	=	0x00a1
_GLOBAL_CFG	=	0x00b1
_PCON	=	0x0087
_POWER_CFG	=	0x00ba
_CLOCK_CFG	=	0x00b9
_WAKE_CTRL	=	0x00a9
_RESET_KEEP	=	0x00fe
_WDOG_COUNT	=	0x00ff
_IE	=	0x00a8
_IP	=	0x00b8
_IE_EX	=	0x00e8
_IP_EX	=	0x00e9
_INTX	=	0x00b3
_GPIO_IE	=	0x00b2
_ROM_ADDR	=	0x8584
_ROM_ADDR_L	=	0x0084
_ROM_ADDR_H	=	0x0085
_ROM_DATA_HI	=	0x8f8e
_ROM_DATA_HL	=	0x008e
_ROM_DATA_HH	=	0x008f
_ROM_CTRL	=	0x0086
_P0	=	0x0080
_P0_MOD_OC	=	0x00c4
_P0_DIR_PU	=	0x00c5
_P1	=	0x0090
_P1_MOD_OC	=	0x0092
_P1_DIR_PU	=	0x0093
_P2	=	0x00a0
_P2_MOD_OC	=	0x0094
_P2_DIR_PU	=	0x0095
_P3	=	0x00b0
_P3_MOD_OC	=	0x0096
_P3_DIR_PU	=	0x0097
_P4	=	0x00c0
_P4_MOD_OC	=	0x00c2
_P4_DIR_PU	=	0x00c3
_P5	=	0x00ab
_PIN_FUNC	=	0x00aa
_XBUS_AUX	=	0x00a2
_TCON	=	0x0088
_TMOD	=	0x0089
_TL0	=	0x008a
_TL1	=	0x008b
_TH0	=	0x008c
_TH1	=	0x008d
_SCON	=	0x0098
_SBUF	=	0x0099
_T2CON2	=	0x00c1
_T2CAP0	=	0xc7c6
_T2CAP0L	=	0x00c6
_T2CAP0H	=	0x00c7
_T2CON	=	0x00c8
_T2MOD	=	0x00c9
_RCAP2	=	0xcbca
_RCAP2L	=	0x00ca
_RCAP2H	=	0x00cb
_T2COUNT	=	0xcdcc
_TL2	=	0x00cc
_TH2	=	0x00cd
_T2CAP1	=	0xcfce
_T2CAP1L	=	0x00ce
_T2CAP1H	=	0x00cf
_PWM_DATA2	=	0x009a
_PWM_DATA1	=	0x009b
_PWM_DATA0	=	0x009c
_PWM_CTRL	=	0x009d
_PWM_CK_SE	=	0x009e
_PWM_CTRL2	=	0x009f
_PWM_DATA3	=	0x00a3
_PWM_DATA4	=	0x00a4
_PWM_DATA5	=	0x00a5
_PWM_DATA6	=	0x00a6
_PWM_DATA7	=	0x00a7
_SPI0_STAT	=	0x00f8
_SPI0_DATA	=	0x00f9
_SPI0_CTRL	=	0x00fa
_SPI0_CK_SE	=	0x00fb
_SPI0_SETUP	=	0x00fc
_SCON1	=	0x00bc
_SBUF1	=	0x00bd
_SBAUD1	=	0x00be
_SIF1	=	0x00bf
_SCON2	=	0x00b4
_SBUF2	=	0x00b5
_SBAUD2	=	0x00b6
_SIF2	=	0x00b7
_SCON3	=	0x00ac
_SBUF3	=	0x00ad
_SBAUD3	=	0x00ae
_SIF3	=	0x00af
_TKEY_CTRL	=	0x00f1
_ADC_CTRL	=	0x00f2
_ADC_CFG	=	0x00f3
_ADC_DAT	=	0xf5f4
_ADC_DAT_L	=	0x00f4
_ADC_DAT_H	=	0x00f5
_ADC_CHAN	=	0x00f6
_ADC_PIN	=	0x00f7
_USB_C_CTRL	=	0x0091
_UDEV_CTRL	=	0x00d1
_UEP1_CTRL	=	0x00d2
_UEP1_T_LEN	=	0x00d3
_UEP2_CTRL	=	0x00d4
_UEP2_T_LEN	=	0x00d5
_UEP3_CTRL	=	0x00d6
_UEP3_T_LEN	=	0x00d7
_USB_INT_FG	=	0x00d8
_USB_INT_ST	=	0x00d9
_USB_MIS_ST	=	0x00da
_USB_RX_LEN	=	0x00db
_UEP0_CTRL	=	0x00dc
_UEP0_T_LEN	=	0x00dd
_UEP4_CTRL	=	0x00de
_UEP4_T_LEN	=	0x00df
_USB_INT_EN	=	0x00e1
_USB_CTRL	=	0x00e2
_USB_DEV_AD	=	0x00e3
_UEP2_DMA	=	0xe5e4
_UEP2_DMA_L	=	0x00e4
_UEP2_DMA_H	=	0x00e5
_UEP3_DMA	=	0xe7e6
_UEP3_DMA_L	=	0x00e6
_UEP3_DMA_H	=	0x00e7
_UEP4_1_MOD	=	0x00ea
_UEP2_3_MOD	=	0x00eb
_UEP0_DMA	=	0xedec
_UEP0_DMA_L	=	0x00ec
_UEP0_DMA_H	=	0x00ed
_UEP1_DMA	=	0xefee
_UEP1_DMA_L	=	0x00ee
_UEP1_DMA_H	=	0x00ef
;--------------------------------------------------------
; special function bits
;--------------------------------------------------------
	.area RSEG    (ABS,DATA)
	.org 0x0000
_CY	=	0x00d7
_AC	=	0x00d6
_F0	=	0x00d5
_RS1	=	0x00d4
_RS0	=	0x00d3
_OV	=	0x00d2
_F1	=	0x00d1
_P	=	0x00d0
_EA	=	0x00af
_E_DIS	=	0x00ae
_ET2	=	0x00ad
_ES	=	0x00ac
_ET1	=	0x00ab
_EX1	=	0x00aa
_ET0	=	0x00a9
_EX0	=	0x00a8
_PH_FLAG	=	0x00bf
_PL_FLAG	=	0x00be
_PT2	=	0x00bd
_PS	=	0x00bc
_PT1	=	0x00bb
_PX1	=	0x00ba
_PT0	=	0x00b9
_PX0	=	0x00b8
_IE_WDOG	=	0x00ef
_IE_GPIO	=	0x00ee
_IE_PWMX	=	0x00ed
_IE_UART3	=	0x00ed
_IE_UART1	=	0x00ec
_IE_ADC	=	0x00eb
_IE_UART2	=	0x00eb
_IE_USB	=	0x00ea
_IE_INT3	=	0x00e9
_IE_SPI0	=	0x00e8
_P0_7	=	0x0087
_P0_6	=	0x0086
_P0_5	=	0x0085
_P0_4	=	0x0084
_P0_3	=	0x0083
_P0_2	=	0x0082
_P0_1	=	0x0081
_P0_0	=	0x0080
_TXD3	=	0x0087
_AIN15	=	0x0087
_RXD3	=	0x0086
_AIN14	=	0x0086
_TXD2	=	0x0085
_AIN13	=	0x0085
_RXD2	=	0x0084
_AIN12	=	0x0084
_TXD_	=	0x0083
_AIN11	=	0x0083
_RXD_	=	0x0082
_AIN10	=	0x0082
_AIN9	=	0x0081
_AIN8	=	0x0080
_P1_7	=	0x0097
_P1_6	=	0x0096
_P1_5	=	0x0095
_P1_4	=	0x0094
_P1_3	=	0x0093
_P1_2	=	0x0092
_P1_1	=	0x0091
_P1_0	=	0x0090
_SCK	=	0x0097
_TXD1_	=	0x0097
_AIN7	=	0x0097
_MISO	=	0x0096
_RXD1_	=	0x0096
_VBUS	=	0x0096
_AIN6	=	0x0096
_MOSI	=	0x0095
_PWM0_	=	0x0095
_UCC2	=	0x0095
_AIN5	=	0x0095
_SCS	=	0x0094
_UCC1	=	0x0094
_AIN4	=	0x0094
_AIN3	=	0x0093
_AIN2	=	0x0092
_T2EX	=	0x0091
_CAP2	=	0x0091
_AIN1	=	0x0091
_T2	=	0x0090
_CAP1	=	0x0090
_AIN0	=	0x0090
_P2_7	=	0x00a7
_P2_6	=	0x00a6
_P2_5	=	0x00a5
_P2_4	=	0x00a4
_P2_3	=	0x00a3
_P2_2	=	0x00a2
_P2_1	=	0x00a1
_P2_0	=	0x00a0
_PWM7	=	0x00a7
_TXD1	=	0x00a7
_PWM6	=	0x00a6
_RXD1	=	0x00a6
_PWM0	=	0x00a5
_T2EX_	=	0x00a5
_CAP2_	=	0x00a5
_PWM1	=	0x00a4
_T2_	=	0x00a4
_CAP1_	=	0x00a4
_PWM2	=	0x00a3
_PWM3	=	0x00a2
_INT0_	=	0x00a2
_PWM4	=	0x00a1
_PWM5	=	0x00a0
_P3_7	=	0x00b7
_P3_6	=	0x00b6
_P3_5	=	0x00b5
_P3_4	=	0x00b4
_P3_3	=	0x00b3
_P3_2	=	0x00b2
_P3_1	=	0x00b1
_P3_0	=	0x00b0
_INT3	=	0x00b7
_CAP0	=	0x00b6
_T1	=	0x00b5
_T0	=	0x00b4
_INT1	=	0x00b3
_INT0	=	0x00b2
_TXD	=	0x00b1
_RXD	=	0x00b0
_P4_6	=	0x00c6
_P4_5	=	0x00c5
_P4_4	=	0x00c4
_P4_3	=	0x00c3
_P4_2	=	0x00c2
_P4_1	=	0x00c1
_P4_0	=	0x00c0
_XO	=	0x00c7
_XI	=	0x00c6
_TF1	=	0x008f
_TR1	=	0x008e
_TF0	=	0x008d
_TR0	=	0x008c
_IE1	=	0x008b
_IT1	=	0x008a
_IE0	=	0x0089
_IT0	=	0x0088
_SM0	=	0x009f
_SM1	=	0x009e
_SM2	=	0x009d
_REN	=	0x009c
_TB8	=	0x009b
_RB8	=	0x009a
_TI	=	0x0099
_RI	=	0x0098
_TF2	=	0x00cf
_CAP1F	=	0x00cf
_EXF2	=	0x00ce
_RCLK	=	0x00cd
_TCLK	=	0x00cc
_EXEN2	=	0x00cb
_TR2	=	0x00ca
_C_T2	=	0x00c9
_CP_RL2	=	0x00c8
_S0_FST_ACT	=	0x00ff
_S0_IF_OV	=	0x00fe
_S0_IF_FIRST	=	0x00fd
_S0_IF_BYTE	=	0x00fc
_S0_FREE	=	0x00fb
_S0_T_FIFO	=	0x00fa
_S0_R_FIFO	=	0x00f8
_U_IS_NAK	=	0x00df
_U_TOG_OK	=	0x00de
_U_SIE_FREE	=	0x00dd
_UIF_FIFO_OV	=	0x00dc
_UIF_HST_SOF	=	0x00db
_UIF_SUSPEND	=	0x00da
_UIF_TRANSFER	=	0x00d9
_UIF_DETECT	=	0x00d8
_UIF_BUS_RST	=	0x00d8
;--------------------------------------------------------
; overlayable register banks
;--------------------------------------------------------
	.area REG_BANK_0	(REL,OVR,DATA)
	.ds 8
;--------------------------------------------------------
; internal ram data
;--------------------------------------------------------
	.area DSEG    (DATA)
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
;--------------------------------------------------------
; indirectly addressable internal ram data
;--------------------------------------------------------
	.area ISEG    (DATA)
;--------------------------------------------------------
; absolute internal ram data
;--------------------------------------------------------
	.area IABS    (ABS,DATA)
	.area IABS    (ABS,DATA)
;--------------------------------------------------------
; bit data
;--------------------------------------------------------
	.area BSEG    (BIT)
;--------------------------------------------------------
; paged external ram data
;--------------------------------------------------------
	.area PSEG    (PAG,XDATA)
;--------------------------------------------------------
; external ram data
;--------------------------------------------------------
	.area XSEG    (XDATA)
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area XABS    (ABS,XDATA)
;--------------------------------------------------------
; external initialized ram data
;--------------------------------------------------------
	.area HOME    (CODE)
	.area GSINIT0 (CODE)
	.area GSINIT1 (CODE)
	.area GSINIT2 (CODE)
	.area GSINIT3 (CODE)
	.area GSINIT4 (CODE)
	.area GSINIT5 (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area CSEG    (CODE)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME    (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area GSINIT  (CODE)
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME    (CODE)
	.area HOME    (CODE)
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CSEG    (CODE)
;------------------------------------------------------------
;Allocation info for local variables in function 'TCS34725_I2C_Write'
;------------------------------------------------------------
;dataBuffer                Allocated to stack - _bp -5
;bytesNumber               Allocated to stack - _bp -6
;stopBit                   Allocated to stack - _bp -7
;slaveAddress              Allocated to registers r7 
;i                         Allocated to registers r7 
;------------------------------------------------------------
;	source/CH549_TCS34725.c:22: void TCS34725_I2C_Write(UINT8  slaveAddress, UINT8 * dataBuffer,UINT8  bytesNumber, UINT8  stopBit)
;	-----------------------------------------
;	 function TCS34725_I2C_Write
;	-----------------------------------------
_TCS34725_I2C_Write:
	ar7 = 0x07
	ar6 = 0x06
	ar5 = 0x05
	ar4 = 0x04
	ar3 = 0x03
	ar2 = 0x02
	ar1 = 0x01
	ar0 = 0x00
	push	_bp
	mov	_bp,sp
	mov	r7,dpl
;	source/CH549_TCS34725.c:26: IIC_Start();
	push	ar7
	lcall	_IIC_Start
	pop	ar7
;	source/CH549_TCS34725.c:27: IIC_Send_Byte((slaveAddress << 1) | 0x00);	   //发送从机地址写命令
	mov	a,r7
	add	a,r7
	mov	dpl,a
	lcall	_IIC_Send_Byte
;	source/CH549_TCS34725.c:28: IIC_Wait_Ack();
	lcall	_IIC_Wait_Ack
;	source/CH549_TCS34725.c:29: for(i = 0; i < bytesNumber; i++)
	mov	r7,#0x00
00105$:
	mov	a,_bp
	add	a,#0xfa
	mov	r0,a
	clr	c
	mov	a,r7
	subb	a,@r0
	jnc	00101$
;	source/CH549_TCS34725.c:31: IIC_Send_Byte(*(dataBuffer + i));
	mov	a,_bp
	add	a,#0xfb
	mov	r0,a
	mov	a,r7
	add	a,@r0
	mov	r4,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r5,a
	inc	r0
	mov	ar6,@r0
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	lcall	__gptrget
	mov	dpl,a
	push	ar7
	lcall	_IIC_Send_Byte
;	source/CH549_TCS34725.c:32: IIC_Wait_Ack();
	lcall	_IIC_Wait_Ack
	pop	ar7
;	source/CH549_TCS34725.c:29: for(i = 0; i < bytesNumber; i++)
	inc	r7
	sjmp	00105$
00101$:
;	source/CH549_TCS34725.c:34: if(stopBit == 1) IIC_Stop();
	mov	a,_bp
	add	a,#0xf9
	mov	r0,a
	cjne	@r0,#0x01,00107$
	lcall	_IIC_Stop
00107$:
;	source/CH549_TCS34725.c:35: }
	pop	_bp
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'TCS34725_I2C_Read'
;------------------------------------------------------------
;dataBuffer                Allocated to stack - _bp -5
;bytesNumber               Allocated to stack - _bp -6
;stopBit                   Allocated to stack - _bp -7
;slaveAddress              Allocated to registers r7 
;i                         Allocated to registers r7 
;------------------------------------------------------------
;	source/CH549_TCS34725.c:46: void TCS34725_I2C_Read(UINT8  slaveAddress, UINT8 * dataBuffer, UINT8  bytesNumber, UINT8  stopBit)
;	-----------------------------------------
;	 function TCS34725_I2C_Read
;	-----------------------------------------
_TCS34725_I2C_Read:
	push	_bp
	mov	_bp,sp
	mov	r7,dpl
;	source/CH549_TCS34725.c:50: IIC_Start();
	push	ar7
	lcall	_IIC_Start
	pop	ar7
;	source/CH549_TCS34725.c:51: IIC_Send_Byte((slaveAddress << 1) | 0x01);	   //发送从机地址读命令
	mov	a,r7
	add	a,r7
	mov	r7,a
	orl	ar7,#0x01
	mov	dpl,r7
	lcall	_IIC_Send_Byte
;	source/CH549_TCS34725.c:52: IIC_Wait_Ack();
	lcall	_IIC_Wait_Ack
;	source/CH549_TCS34725.c:53: for(i = 0; i < bytesNumber; i++)
	mov	r7,#0x00
00108$:
	mov	a,_bp
	add	a,#0xfa
	mov	r0,a
	clr	c
	mov	a,r7
	subb	a,@r0
	jc	00129$
	ljmp	00104$
00129$:
;	source/CH549_TCS34725.c:55: if(i == bytesNumber - 1)
	mov	a,_bp
	add	a,#0xfa
	mov	r0,a
	mov	ar5,@r0
	mov	r6,#0x00
	dec	r5
	cjne	r5,#0xff,00130$
	dec	r6
00130$:
	mov	ar3,r7
	mov	r4,#0x00
	mov	a,r3
	cjne	a,ar5,00102$
	mov	a,r4
	cjne	a,ar6,00102$
;	source/CH549_TCS34725.c:57: dataBuffer[i] = IIC_Read_Byte(0);//读取的最后一个字节发送NACK
	mov	a,_bp
	add	a,#0xfb
	mov	r0,a
	mov	a,r7
	add	a,@r0
	mov	r4,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r5,a
	inc	r0
	mov	ar6,@r0
	mov	dpl,#0x00
	push	ar7
	push	ar6
	push	ar5
	push	ar4
	lcall	_IIC_Read_Byte
	mov	r3,dpl
	pop	ar4
	pop	ar5
	pop	ar6
	pop	ar7
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	mov	a,r3
	lcall	__gptrput
	sjmp	00109$
00102$:
;	source/CH549_TCS34725.c:61: dataBuffer[i] = IIC_Read_Byte(1);
	mov	a,_bp
	add	a,#0xfb
	mov	r0,a
	mov	a,r7
	add	a,@r0
	mov	r4,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r5,a
	inc	r0
	mov	ar6,@r0
	mov	dpl,#0x01
	push	ar7
	push	ar6
	push	ar5
	push	ar4
	lcall	_IIC_Read_Byte
	mov	r3,dpl
	pop	ar4
	pop	ar5
	pop	ar6
	pop	ar7
	mov	dpl,r4
	mov	dph,r5
	mov	b,r6
	mov	a,r3
	lcall	__gptrput
00109$:
;	source/CH549_TCS34725.c:53: for(i = 0; i < bytesNumber; i++)
	inc	r7
	ljmp	00108$
00104$:
;	source/CH549_TCS34725.c:64: if(stopBit == 1) IIC_Stop();
	mov	a,_bp
	add	a,#0xf9
	mov	r0,a
	cjne	@r0,#0x01,00110$
	lcall	_IIC_Stop
00110$:
;	source/CH549_TCS34725.c:65: }
	pop	_bp
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'TCS34725_Write'
;------------------------------------------------------------
;dataBuffer                Allocated to stack - _bp -5
;bytesNumber               Allocated to stack - _bp -6
;subAddr                   Allocated to registers r7 
;sendBuffer                Allocated to stack - _bp +1
;byte                      Allocated to registers r7 
;------------------------------------------------------------
;	source/CH549_TCS34725.c:76: void TCS34725_Write(UINT8  subAddr, UINT8 * dataBuffer, UINT8  bytesNumber)
;	-----------------------------------------
;	 function TCS34725_Write
;	-----------------------------------------
_TCS34725_Write:
	push	_bp
	mov	a,sp
	mov	_bp,a
	add	a,#0x0a
	mov	sp,a
	mov	r7,dpl
;	source/CH549_TCS34725.c:78: UINT8  sendBuffer[10] = {0, };
	mov	r1,_bp
	inc	r1
	mov	@r1,#0x00
	mov	a,r1
	inc	a
	mov	r0,a
	mov	@r0,#0x00
	mov	a,#0x02
	add	a,r1
	mov	r0,a
	mov	@r0,#0x00
	mov	a,#0x03
	add	a,r1
	mov	r0,a
	mov	@r0,#0x00
	mov	a,#0x04
	add	a,r1
	mov	r0,a
	mov	@r0,#0x00
	mov	a,#0x05
	add	a,r1
	mov	r0,a
	mov	@r0,#0x00
	mov	a,#0x06
	add	a,r1
	mov	r0,a
	mov	@r0,#0x00
	mov	a,#0x07
	add	a,r1
	mov	r0,a
	mov	@r0,#0x00
	mov	a,#0x08
	add	a,r1
	mov	r0,a
	mov	@r0,#0x00
	mov	a,#0x09
	add	a,r1
	mov	r0,a
	mov	@r0,#0x00
;	source/CH549_TCS34725.c:81: sendBuffer[0] = subAddr | TCS34725_COMMAND_BIT;
	mov	a,#0x80
	orl	a,r7
	mov	@r1,a
;	source/CH549_TCS34725.c:82: for(byte = 1; byte <= bytesNumber; byte++)
	mov	r7,#0x01
00103$:
	mov	a,_bp
	add	a,#0xfa
	mov	r0,a
	clr	c
	mov	a,@r0
	subb	a,r7
	jc	00101$
;	source/CH549_TCS34725.c:84: sendBuffer[byte] = dataBuffer[byte - 1];
	mov	a,r7
	add	a,r1
	mov	r0,a
	mov	ar5,r7
	mov	r6,#0x00
	dec	r5
	cjne	r5,#0xff,00117$
	dec	r6
00117$:
	push	ar0
	mov	a,_bp
	add	a,#0xfb
	mov	r0,a
	mov	a,r5
	add	a,@r0
	mov	r5,a
	mov	a,r6
	inc	r0
	addc	a,@r0
	mov	r6,a
	inc	r0
	mov	ar4,@r0
	pop	ar0
	mov	dpl,r5
	mov	dph,r6
	mov	b,r4
	lcall	__gptrget
	mov	r5,a
	mov	@r0,a
;	source/CH549_TCS34725.c:82: for(byte = 1; byte <= bytesNumber; byte++)
	inc	r7
	sjmp	00103$
00101$:
;	source/CH549_TCS34725.c:86: TCS34725_I2C_Write(TCS34725_ADDRESS, sendBuffer, bytesNumber + 1, 1);
	mov	a,_bp
	add	a,#0xfa
	mov	r0,a
	mov	ar7,@r0
	inc	r7
	mov	ar6,r1
	mov	r5,#0x00
	mov	r4,#0x40
	mov	a,#0x01
	push	acc
	push	ar7
	push	ar6
	push	ar5
	push	ar4
	mov	dpl,#0x29
	lcall	_TCS34725_I2C_Write
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
;	source/CH549_TCS34725.c:87: }
	mov	sp,_bp
	pop	_bp
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'TCS34725_Read'
;------------------------------------------------------------
;dataBuffer                Allocated to stack - _bp -5
;bytesNumber               Allocated to stack - _bp -6
;subAddr                   Allocated to stack - _bp +1
;------------------------------------------------------------
;	source/CH549_TCS34725.c:98: void TCS34725_Read(UINT8  subAddr, UINT8 * dataBuffer, UINT8  bytesNumber)
;	-----------------------------------------
;	 function TCS34725_Read
;	-----------------------------------------
_TCS34725_Read:
	push	_bp
	mov	_bp,sp
	push	dpl
;	source/CH549_TCS34725.c:100: subAddr |= TCS34725_COMMAND_BIT;
	mov	r0,_bp
	inc	r0
	mov	a,@r0
	orl	a,#0x80
	mov	@r0,a
;	source/CH549_TCS34725.c:102: TCS34725_I2C_Write(TCS34725_ADDRESS, (UINT8 *)&subAddr, 1, 0);
	mov	r7,_bp
	inc	r7
	mov	r6,#0x00
	mov	r5,#0x40
	clr	a
	push	acc
	inc	a
	push	acc
	push	ar7
	push	ar6
	push	ar5
	mov	dpl,#0x29
	lcall	_TCS34725_I2C_Write
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
;	source/CH549_TCS34725.c:103: TCS34725_I2C_Read(TCS34725_ADDRESS, dataBuffer, bytesNumber, 1);
	mov	a,#0x01
	push	acc
	mov	a,_bp
	add	a,#0xfa
	mov	r0,a
	mov	a,@r0
	push	acc
	mov	a,_bp
	add	a,#0xfb
	mov	r0,a
	mov	a,@r0
	push	acc
	inc	r0
	mov	a,@r0
	push	acc
	inc	r0
	mov	a,@r0
	push	acc
	mov	dpl,#0x29
	lcall	_TCS34725_I2C_Read
	mov	a,sp
	add	a,#0xfb
	mov	sp,a
;	source/CH549_TCS34725.c:104: }
	dec	sp
	pop	_bp
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'TCS34725_SetIntegrationTime'
;------------------------------------------------------------
;time                      Allocated to stack - _bp +1
;------------------------------------------------------------
;	source/CH549_TCS34725.c:110: void TCS34725_SetIntegrationTime(UINT8  time)
;	-----------------------------------------
;	 function TCS34725_SetIntegrationTime
;	-----------------------------------------
_TCS34725_SetIntegrationTime:
	push	_bp
	mov	_bp,sp
	push	dpl
;	source/CH549_TCS34725.c:112: TCS34725_Write(TCS34725_ATIME, &time, 1);
	mov	r7,_bp
	inc	r7
	mov	r6,#0x00
	mov	r5,#0x40
	mov	a,#0x01
	push	acc
	push	ar7
	push	ar6
	push	ar5
	mov	dpl,#0x01
	lcall	_TCS34725_Write
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
;	source/CH549_TCS34725.c:113: }
	dec	sp
	pop	_bp
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'TCS34725_SetGain'
;------------------------------------------------------------
;gain                      Allocated to stack - _bp +1
;------------------------------------------------------------
;	source/CH549_TCS34725.c:119: void TCS34725_SetGain(UINT8  gain)
;	-----------------------------------------
;	 function TCS34725_SetGain
;	-----------------------------------------
_TCS34725_SetGain:
	push	_bp
	mov	_bp,sp
	push	dpl
;	source/CH549_TCS34725.c:121: TCS34725_Write(TCS34725_CONTROL, &gain, 1);
	mov	r7,_bp
	inc	r7
	mov	r6,#0x00
	mov	r5,#0x40
	mov	a,#0x01
	push	acc
	push	ar7
	push	ar6
	push	ar5
	mov	dpl,#0x0f
	lcall	_TCS34725_Write
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
;	source/CH549_TCS34725.c:122: }
	dec	sp
	pop	_bp
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'TCS34725_Enable'
;------------------------------------------------------------
;cmd                       Allocated to stack - _bp +1
;------------------------------------------------------------
;	source/CH549_TCS34725.c:128: void TCS34725_Enable(void)
;	-----------------------------------------
;	 function TCS34725_Enable
;	-----------------------------------------
_TCS34725_Enable:
	push	_bp
	mov	_bp,sp
	inc	sp
;	source/CH549_TCS34725.c:130: UINT8  cmd = TCS34725_ENABLE_PON;
	mov	r0,_bp
	inc	r0
	mov	@r0,#0x01
;	source/CH549_TCS34725.c:132: TCS34725_Write(TCS34725_ENABLE, &cmd, 1);
	mov	r7,_bp
	inc	r7
	mov	r6,#0x00
	mov	r5,#0x40
	mov	a,#0x01
	push	acc
	push	ar7
	push	ar6
	push	ar5
	mov	dpl,#0x00
	lcall	_TCS34725_Write
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
;	source/CH549_TCS34725.c:133: cmd = TCS34725_ENABLE_PON | TCS34725_ENABLE_AEN;
	mov	r0,_bp
	inc	r0
	mov	@r0,#0x03
;	source/CH549_TCS34725.c:134: TCS34725_Write(TCS34725_ENABLE, &cmd, 1);
	mov	r7,_bp
	inc	r7
	mov	r6,#0x00
	mov	r5,#0x40
	mov	a,#0x01
	push	acc
	push	ar7
	push	ar6
	push	ar5
	mov	dpl,#0x00
	lcall	_TCS34725_Write
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
;	source/CH549_TCS34725.c:136: }
	dec	sp
	pop	_bp
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'TCS34725_Disable'
;------------------------------------------------------------
;cmd                       Allocated to stack - _bp +1
;------------------------------------------------------------
;	source/CH549_TCS34725.c:142: void TCS34725_Disable(void)
;	-----------------------------------------
;	 function TCS34725_Disable
;	-----------------------------------------
_TCS34725_Disable:
	push	_bp
	mov	_bp,sp
	inc	sp
;	source/CH549_TCS34725.c:144: UINT8  cmd = 0;
	mov	r0,_bp
	inc	r0
	mov	@r0,#0x00
;	source/CH549_TCS34725.c:146: TCS34725_Read(TCS34725_ENABLE, &cmd, 1);
	mov	r7,_bp
	inc	r7
	mov	r6,#0x00
	mov	r5,#0x40
	mov	a,#0x01
	push	acc
	push	ar7
	push	ar6
	push	ar5
	mov	dpl,#0x00
	lcall	_TCS34725_Read
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
;	source/CH549_TCS34725.c:147: cmd = cmd & ~(TCS34725_ENABLE_PON | TCS34725_ENABLE_AEN);
	mov	r0,_bp
	inc	r0
	mov	a,@r0
	anl	a,#0xfc
	mov	@r0,a
;	source/CH549_TCS34725.c:148: TCS34725_Write(TCS34725_ENABLE, &cmd, 1);
	mov	r7,_bp
	inc	r7
	mov	r6,#0x00
	mov	r5,#0x40
	mov	a,#0x01
	push	acc
	push	ar7
	push	ar6
	push	ar5
	mov	dpl,#0x00
	lcall	_TCS34725_Write
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
;	source/CH549_TCS34725.c:149: }
	dec	sp
	pop	_bp
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'TCS34725_Init'
;------------------------------------------------------------
;id                        Allocated to stack - _bp +1
;------------------------------------------------------------
;	source/CH549_TCS34725.c:155: UINT8  TCS34725_Init(void)
;	-----------------------------------------
;	 function TCS34725_Init
;	-----------------------------------------
_TCS34725_Init:
	push	_bp
	mov	_bp,sp
	inc	sp
;	source/CH549_TCS34725.c:157: UINT8  id=0;
	mov	r0,_bp
	inc	r0
	mov	@r0,#0x00
;	source/CH549_TCS34725.c:159: IIC_Init(); 
	lcall	_IIC_Init
;	source/CH549_TCS34725.c:160: TCS34725_Read(TCS34725_ID, &id, 1);  //TCS34725 的 ID 是 0x44 可以根据这个来判断是否成功连接,0x4D是TCS34727;
	mov	r7,_bp
	inc	r7
	mov	r6,#0x00
	mov	r5,#0x40
	mov	a,#0x01
	push	acc
	push	ar7
	push	ar6
	push	ar5
	mov	dpl,#0x12
	lcall	_TCS34725_Read
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
;	source/CH549_TCS34725.c:161: if(id==0x4D | id==0x44)
	mov	r0,_bp
	inc	r0
	clr	a
	cjne	@r0,#0x4d,00109$
	inc	a
00109$:
	mov	r7,a
	mov	r0,_bp
	inc	r0
	clr	a
	cjne	@r0,#0x44,00111$
	inc	a
00111$:
	orl	a,r7
	jz	00102$
;	source/CH549_TCS34725.c:163: TCS34725_SetIntegrationTime(TCS34725_INTEGRATIONTIME_50MS);
	mov	dpl,#0xeb
	lcall	_TCS34725_SetIntegrationTime
;	source/CH549_TCS34725.c:164: TCS34725_SetGain(TCS34725_GAIN_1X);
	mov	dpl,#0x00
	lcall	_TCS34725_SetGain
;	source/CH549_TCS34725.c:165: TCS34725_Enable();
	lcall	_TCS34725_Enable
;	source/CH549_TCS34725.c:166: return 1;
	mov	dpl,#0x01
	sjmp	00103$
00102$:
;	source/CH549_TCS34725.c:168: return 0;
	mov	dpl,#0x00
00103$:
;	source/CH549_TCS34725.c:169: }
	dec	sp
	pop	_bp
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'TCS34725_GetChannelData'
;------------------------------------------------------------
;reg                       Allocated to registers r7 
;tmp                       Allocated to stack - _bp +1
;data                      Allocated to registers r7 r6 
;------------------------------------------------------------
;	source/CH549_TCS34725.c:175: UINT16 TCS34725_GetChannelData(UINT8  reg)
;	-----------------------------------------
;	 function TCS34725_GetChannelData
;	-----------------------------------------
_TCS34725_GetChannelData:
	push	_bp
	mov	_bp,sp
	inc	sp
	inc	sp
	mov	r7,dpl
;	source/CH549_TCS34725.c:177: UINT8  tmp[2] = {0,0};
	mov	r1,_bp
	inc	r1
	mov	@r1,#0x00
	mov	a,r1
	inc	a
	mov	r0,a
	mov	@r0,#0x00
;	source/CH549_TCS34725.c:180: TCS34725_Read(reg, tmp, 2);
	mov	ar6,r1
	mov	r5,#0x00
	mov	r4,#0x40
	push	ar1
	push	ar0
	mov	a,#0x02
	push	acc
	push	ar6
	push	ar5
	push	ar4
	mov	dpl,r7
	lcall	_TCS34725_Read
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
	pop	ar0
	pop	ar1
;	source/CH549_TCS34725.c:181: data = (tmp[0] << 8) | tmp[1];
	mov	ar7,@r1
	mov	ar6,r7
	mov	r7,#0x00
	mov	ar5,@r0
	mov	r4,#0x00
	mov	a,r5
	orl	ar7,a
	mov	a,r4
	orl	ar6,a
;	source/CH549_TCS34725.c:183: return data;
	mov	dpl,r7
	mov	dph,r6
;	source/CH549_TCS34725.c:184: }
	mov	sp,_bp
	pop	_bp
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'TCS34725_GetRawData'
;------------------------------------------------------------
;rgbc                      Allocated to stack - _bp +1
;status                    Allocated to stack - _bp +4
;------------------------------------------------------------
;	source/CH549_TCS34725.c:191: UINT8  TCS34725_GetRawData(COLOR_RGBC *rgbc)
;	-----------------------------------------
;	 function TCS34725_GetRawData
;	-----------------------------------------
_TCS34725_GetRawData:
	push	_bp
	mov	_bp,sp
	push	dpl
	push	dph
	push	b
	inc	sp
;	source/CH549_TCS34725.c:193: UINT8  status = TCS34725_STATUS_AVALID;
	mov	a,_bp
	add	a,#0x04
	mov	r0,a
	mov	@r0,#0x01
;	source/CH549_TCS34725.c:195: TCS34725_Read(TCS34725_STATUS, &status, 1);
	mov	a,_bp
	add	a,#0x04
	mov	r4,a
	mov	r3,#0x00
	mov	r2,#0x40
	mov	a,#0x01
	push	acc
	push	ar4
	push	ar3
	push	ar2
	mov	dpl,#0x13
	lcall	_TCS34725_Read
	mov	a,sp
	add	a,#0xfc
	mov	sp,a
;	source/CH549_TCS34725.c:197: if(status & TCS34725_STATUS_AVALID)
	mov	a,_bp
	add	a,#0x04
	mov	r0,a
	mov	a,@r0
	jb	acc.0,00109$
	ljmp	00102$
00109$:
;	source/CH549_TCS34725.c:199: rgbc->c = TCS34725_GetChannelData(TCS34725_CDATAL);	
	mov	dpl,#0x14
	lcall	_TCS34725_GetChannelData
	mov	r3,dpl
	mov	r4,dph
	mov	r0,_bp
	inc	r0
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	inc	r0
	mov	b,@r0
	mov	a,r3
	lcall	__gptrput
	inc	dptr
	mov	a,r4
	lcall	__gptrput
;	source/CH549_TCS34725.c:200: rgbc->r = TCS34725_GetChannelData(TCS34725_RDATAL);	
	mov	r0,_bp
	inc	r0
	mov	a,#0x02
	add	a,@r0
	mov	r2,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r3,a
	inc	r0
	mov	ar4,@r0
	mov	dpl,#0x16
	push	ar4
	push	ar3
	push	ar2
	lcall	_TCS34725_GetChannelData
	mov	r6,dpl
	mov	r7,dph
	pop	ar2
	pop	ar3
	pop	ar4
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	mov	a,r6
	lcall	__gptrput
	inc	dptr
	mov	a,r7
	lcall	__gptrput
;	source/CH549_TCS34725.c:201: rgbc->g = TCS34725_GetChannelData(TCS34725_GDATAL);	
	mov	r0,_bp
	inc	r0
	mov	a,#0x04
	add	a,@r0
	mov	r5,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r6,a
	inc	r0
	mov	ar7,@r0
	mov	dpl,#0x18
	push	ar7
	push	ar6
	push	ar5
	lcall	_TCS34725_GetChannelData
	mov	r3,dpl
	mov	r4,dph
	pop	ar5
	pop	ar6
	pop	ar7
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	mov	a,r3
	lcall	__gptrput
	inc	dptr
	mov	a,r4
	lcall	__gptrput
;	source/CH549_TCS34725.c:202: rgbc->b = TCS34725_GetChannelData(TCS34725_BDATAL);
	mov	r0,_bp
	inc	r0
	mov	a,#0x06
	add	a,@r0
	mov	r5,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r6,a
	inc	r0
	mov	ar7,@r0
	mov	dpl,#0x1a
	push	ar7
	push	ar6
	push	ar5
	lcall	_TCS34725_GetChannelData
	mov	r3,dpl
	mov	r4,dph
	pop	ar5
	pop	ar6
	pop	ar7
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	mov	a,r3
	lcall	__gptrput
	inc	dptr
	mov	a,r4
	lcall	__gptrput
;	source/CH549_TCS34725.c:203: return 1;
	mov	dpl,#0x01
	sjmp	00103$
00102$:
;	source/CH549_TCS34725.c:205: return 0;
	mov	dpl,#0x00
00103$:
;	source/CH549_TCS34725.c:206: }
	mov	sp,_bp
	pop	_bp
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'RGBtoHSL'
;------------------------------------------------------------
;Hsl                       Allocated to stack - _bp -5
;Rgb                       Allocated to stack - _bp +1
;maxVal                    Allocated to registers r2 
;minVal                    Allocated to registers r5 
;difVal                    Allocated to stack - _bp +14
;r                         Allocated to stack - _bp +15
;g                         Allocated to registers r4 
;b                         Allocated to registers r6 
;sloc0                     Allocated to stack - _bp +11
;sloc1                     Allocated to stack - _bp +4
;sloc2                     Allocated to stack - _bp +6
;sloc3                     Allocated to stack - _bp +8
;------------------------------------------------------------
;	source/CH549_TCS34725.c:209: void RGBtoHSL(COLOR_RGBC *Rgb, COLOR_HSL *Hsl)
;	-----------------------------------------
;	 function RGBtoHSL
;	-----------------------------------------
_RGBtoHSL:
	push	_bp
	mov	_bp,sp
	push	dpl
	push	dph
	push	b
	mov	a,sp
	add	a,#0x0c
	mov	sp,a
;	source/CH549_TCS34725.c:212: UINT8  r = Rgb->r*100/Rgb->c;   //[0-100]
	mov	r0,_bp
	inc	r0
	mov	a,#0x02
	add	a,@r0
	mov	r2,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r3,a
	inc	r0
	mov	ar4,@r0
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	lcall	__gptrget
	mov	r2,a
	inc	dptr
	lcall	__gptrget
	mov	r3,a
	push	ar2
	push	ar3
	mov	dptr,#0x0064
	lcall	__mulint
	mov	r3,dpl
	mov	r4,dph
	dec	sp
	dec	sp
	mov	r0,_bp
	inc	r0
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	inc	r0
	mov	b,@r0
	lcall	__gptrget
	mov	r2,a
	inc	dptr
	lcall	__gptrget
	mov	r7,a
	push	ar7
	push	ar2
	push	ar2
	push	ar7
	mov	dpl,r3
	mov	dph,r4
	lcall	__divuint
	mov	r5,dpl
	dec	sp
	dec	sp
	mov	a,_bp
	add	a,#0x0f
	mov	r0,a
	mov	@r0,ar5
;	source/CH549_TCS34725.c:213: UINT8  g = Rgb->g*100/Rgb->c;
	mov	r0,_bp
	inc	r0
	mov	a,#0x04
	add	a,@r0
	mov	r3,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r4,a
	inc	r0
	mov	ar6,@r0
	mov	dpl,r3
	mov	dph,r4
	mov	b,r6
	lcall	__gptrget
	mov	r3,a
	inc	dptr
	lcall	__gptrget
	mov	r4,a
	push	ar3
	push	ar4
	mov	dptr,#0x0064
	lcall	__mulint
	mov	r4,dpl
	mov	r6,dph
	dec	sp
	dec	sp
	pop	ar2
	pop	ar7
	push	ar7
	push	ar2
	push	ar2
	push	ar7
	mov	dpl,r4
	mov	dph,r6
	lcall	__divuint
	mov	r4,dpl
	dec	sp
	dec	sp
	pop	ar2
;	source/CH549_TCS34725.c:214: UINT8  b = Rgb->b*100/Rgb->c;
	mov	r0,_bp
	inc	r0
	mov	a,#0x06
	add	a,@r0
	mov	r3,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r5,a
	inc	r0
	mov	ar6,@r0
	mov	dpl,r3
	mov	dph,r5
	mov	b,r6
	lcall	__gptrget
	mov	r3,a
	inc	dptr
	lcall	__gptrget
	mov	r5,a
	push	ar4
	push	ar2
	push	ar3
	push	ar5
	mov	dptr,#0x0064
	lcall	__mulint
	mov	r5,dpl
	mov	r6,dph
	dec	sp
	dec	sp
	pop	ar2
	pop	ar4
	pop	ar7
	push	ar4
	push	ar2
	push	ar7
	mov	dpl,r5
	mov	dph,r6
	lcall	__divuint
	mov	r6,dpl
	dec	sp
	dec	sp
	pop	ar4
;	source/CH549_TCS34725.c:216: maxVal = max3v(r,g,b);
	mov	a,_bp
	add	a,#0x0f
	mov	r0,a
	clr	c
	mov	a,@r0
	subb	a,r4
	jnc	00120$
	clr	c
	mov	a,r4
	subb	a,r6
	jnc	00122$
	mov	ar7,r6
	sjmp	00121$
00122$:
	mov	ar7,r4
	sjmp	00121$
00120$:
	mov	a,_bp
	add	a,#0x0f
	mov	r0,a
	clr	c
	mov	a,@r0
	subb	a,r6
	jnc	00124$
	mov	ar5,r6
	sjmp	00125$
00124$:
	mov	a,_bp
	add	a,#0x0f
	mov	r0,a
	mov	ar5,@r0
00125$:
	mov	ar7,r5
00121$:
	mov	ar2,r7
;	source/CH549_TCS34725.c:217: minVal = min3v(r,g,b);
	mov	a,_bp
	add	a,#0x0f
	mov	r0,a
	clr	c
	mov	a,r4
	subb	a,@r0
	jnc	00126$
	clr	c
	mov	a,r6
	subb	a,r4
	jnc	00128$
	mov	ar5,r6
	sjmp	00127$
00128$:
	mov	ar5,r4
	sjmp	00127$
00126$:
	mov	a,_bp
	add	a,#0x0f
	mov	r0,a
	clr	c
	mov	a,r6
	subb	a,@r0
	jnc	00130$
	mov	ar3,r6
	sjmp	00131$
00130$:
	mov	a,_bp
	add	a,#0x0f
	mov	r0,a
	mov	ar3,@r0
00131$:
	mov	ar5,r3
00127$:
	push	ar4
;	source/CH549_TCS34725.c:218: difVal = maxVal-minVal;
	mov	a,r2
	clr	c
	subb	a,r5
	mov	r3,a
	mov	a,_bp
	add	a,#0x0e
	mov	r0,a
	mov	@r0,ar3
;	source/CH549_TCS34725.c:221: Hsl->l = (maxVal+minVal)/2;   //[0-100]
	mov	a,_bp
	add	a,#0xfb
	mov	r0,a
	mov	a,_bp
	add	a,#0x08
	mov	r1,a
	mov	a,@r0
	mov	@r1,a
	inc	r0
	inc	r1
	mov	a,@r0
	mov	@r1,a
	inc	r0
	inc	r1
	mov	a,@r0
	mov	@r1,a
	mov	a,_bp
	add	a,#0x08
	mov	r0,a
	mov	a,_bp
	add	a,#0x0b
	mov	r1,a
	mov	a,#0x03
	add	a,@r0
	mov	@r1,a
	clr	a
	inc	r0
	addc	a,@r0
	inc	r1
	mov	@r1,a
	inc	r0
	inc	r1
	mov	a,@r0
	mov	@r1,a
	mov	a,_bp
	add	a,#0x04
	mov	r0,a
	mov	@r0,ar2
	inc	r0
	mov	@r0,#0x00
	mov	a,_bp
	add	a,#0x06
	mov	r0,a
	mov	@r0,ar5
	inc	r0
	mov	@r0,#0x00
	mov	a,_bp
	add	a,#0x04
	mov	r0,a
	mov	a,_bp
	add	a,#0x06
	mov	r1,a
	mov	a,@r1
	add	a,@r0
	mov	r4,a
	inc	r1
	mov	a,@r1
	inc	r0
	addc	a,@r0
	mov	r7,a
	push	ar6
	push	ar5
	push	ar2
	mov	a,#0x02
	push	acc
	clr	a
	push	acc
	mov	dpl,r4
	mov	dph,r7
	lcall	__divsint
	mov	r4,dpl
	mov	r7,dph
	dec	sp
	dec	sp
	pop	ar2
	pop	ar5
	pop	ar6
	mov	a,_bp
	add	a,#0x0b
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	inc	r0
	mov	b,@r0
	mov	a,r4
	lcall	__gptrput
;	source/CH549_TCS34725.c:223: if(maxVal == minVal)//若r=g=b,灰度
	mov	a,r2
	cjne	a,ar5,00186$
	sjmp	00187$
00186$:
	pop	ar4
	sjmp	00116$
00187$:
	pop	ar4
;	source/CH549_TCS34725.c:225: Hsl->h = 0; 
	mov	a,_bp
	add	a,#0x08
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	inc	r0
	mov	b,@r0
	clr	a
	lcall	__gptrput
	inc	dptr
	lcall	__gptrput
;	source/CH549_TCS34725.c:226: Hsl->s = 0;
	mov	a,_bp
	add	a,#0x08
	mov	r0,a
	mov	a,#0x02
	add	a,@r0
	mov	r3,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r5,a
	inc	r0
	mov	ar7,@r0
	mov	dpl,r3
	mov	dph,r5
	mov	b,r7
	clr	a
	lcall	__gptrput
	ljmp	00118$
00116$:
;	source/CH549_TCS34725.c:231: if(maxVal==r)
	mov	a,_bp
	add	a,#0x0f
	mov	r0,a
	mov	a,@r0
	cjne	a,ar2,00188$
	sjmp	00189$
00188$:
	ljmp	00110$
00189$:
;	source/CH549_TCS34725.c:233: if(g>=b)
	clr	c
	mov	a,r4
	subb	a,r6
	jc	00102$
;	source/CH549_TCS34725.c:234: Hsl->h = 60*(g-b)/difVal;
	mov	ar5,r4
	mov	r7,#0x00
	mov	ar2,r6
	mov	r3,#0x00
	mov	a,r5
	clr	c
	subb	a,r2
	mov	r5,a
	mov	a,r7
	subb	a,r3
	mov	r7,a
	push	ar5
	push	ar7
	mov	dptr,#0x003c
	lcall	__mulint
	mov	r5,dpl
	mov	r7,dph
	dec	sp
	dec	sp
	mov	a,_bp
	add	a,#0x0e
	mov	r0,a
	mov	ar2,@r0
	mov	r3,#0x00
	push	ar2
	push	ar3
	mov	dpl,r5
	mov	dph,r7
	lcall	__divsint
	mov	r5,dpl
	mov	r7,dph
	dec	sp
	dec	sp
	mov	a,_bp
	add	a,#0x08
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	inc	r0
	mov	b,@r0
	mov	a,r5
	lcall	__gptrput
	inc	dptr
	mov	a,r7
	lcall	__gptrput
	ljmp	00111$
00102$:
;	source/CH549_TCS34725.c:236: Hsl->h = 60*(g-b)/difVal+360;
	mov	ar5,r4
	mov	r7,#0x00
	mov	ar2,r6
	mov	r3,#0x00
	mov	a,r5
	clr	c
	subb	a,r2
	mov	r5,a
	mov	a,r7
	subb	a,r3
	mov	r7,a
	push	ar5
	push	ar7
	mov	dptr,#0x003c
	lcall	__mulint
	mov	r5,dpl
	mov	r7,dph
	dec	sp
	dec	sp
	mov	a,_bp
	add	a,#0x0e
	mov	r0,a
	mov	ar2,@r0
	mov	r3,#0x00
	push	ar2
	push	ar3
	mov	dpl,r5
	mov	dph,r7
	lcall	__divsint
	mov	r5,dpl
	mov	r7,dph
	dec	sp
	dec	sp
	mov	a,#0x68
	add	a,r5
	mov	r5,a
	mov	a,#0x01
	addc	a,r7
	mov	r7,a
	mov	a,_bp
	add	a,#0x08
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	inc	r0
	mov	b,@r0
	mov	a,r5
	lcall	__gptrput
	inc	dptr
	mov	a,r7
	lcall	__gptrput
	ljmp	00111$
00110$:
;	source/CH549_TCS34725.c:240: if(maxVal==g)Hsl->h = 60*(b-r)/difVal+120;
	mov	a,r2
	cjne	a,ar4,00107$
	mov	ar5,r6
	mov	r7,#0x00
	mov	a,_bp
	add	a,#0x0f
	mov	r0,a
	mov	ar2,@r0
	mov	r3,#0x00
	mov	a,r5
	clr	c
	subb	a,r2
	mov	r5,a
	mov	a,r7
	subb	a,r3
	mov	r7,a
	push	ar5
	push	ar7
	mov	dptr,#0x003c
	lcall	__mulint
	mov	r5,dpl
	mov	r7,dph
	dec	sp
	dec	sp
	mov	a,_bp
	add	a,#0x0e
	mov	r0,a
	mov	ar2,@r0
	mov	r3,#0x00
	push	ar2
	push	ar3
	mov	dpl,r5
	mov	dph,r7
	lcall	__divsint
	mov	r5,dpl
	mov	r7,dph
	dec	sp
	dec	sp
	mov	a,#0x78
	add	a,r5
	mov	r5,a
	clr	a
	addc	a,r7
	mov	r7,a
	mov	a,_bp
	add	a,#0x08
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	inc	r0
	mov	b,@r0
	mov	a,r5
	lcall	__gptrput
	inc	dptr
	mov	a,r7
	lcall	__gptrput
	sjmp	00111$
00107$:
;	source/CH549_TCS34725.c:242: if(maxVal==b)Hsl->h = 60*(r-g)/difVal+240;
	mov	a,r2
	cjne	a,ar6,00111$
	mov	a,_bp
	add	a,#0x0f
	mov	r0,a
	mov	ar5,@r0
	clr	a
	mov	r7,a
	mov	r6,a
	mov	a,r5
	clr	c
	subb	a,r4
	mov	r5,a
	mov	a,r7
	subb	a,r6
	mov	r7,a
	push	ar5
	push	ar7
	mov	dptr,#0x003c
	lcall	__mulint
	mov	r6,dpl
	mov	r7,dph
	dec	sp
	dec	sp
	mov	a,_bp
	add	a,#0x0e
	mov	r0,a
	mov	ar3,@r0
	mov	r5,#0x00
	push	ar3
	push	ar5
	mov	dpl,r6
	mov	dph,r7
	lcall	__divsint
	mov	r6,dpl
	mov	r7,dph
	dec	sp
	dec	sp
	mov	a,#0xf0
	add	a,r6
	mov	r6,a
	clr	a
	addc	a,r7
	mov	r7,a
	mov	a,_bp
	add	a,#0x08
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	inc	r0
	mov	b,@r0
	mov	a,r6
	lcall	__gptrput
	inc	dptr
	mov	a,r7
	lcall	__gptrput
00111$:
;	source/CH549_TCS34725.c:246: if(Hsl->l<=50)Hsl->s=difVal*100/(maxVal+minVal);  //[0-100]
	mov	a,_bp
	add	a,#0x0b
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	inc	r0
	mov	b,@r0
	lcall	__gptrget
	add	a,#0xff - 0x32
	jc	00113$
	mov	a,_bp
	add	a,#0x08
	mov	r0,a
	mov	a,#0x02
	add	a,@r0
	mov	r5,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r6,a
	inc	r0
	mov	ar7,@r0
	mov	a,_bp
	add	a,#0x0e
	mov	r0,a
	mov	ar3,@r0
	mov	r4,#0x00
	push	ar7
	push	ar6
	push	ar5
	push	ar3
	push	ar4
	mov	dptr,#0x0064
	lcall	__mulint
	xch	a,r0
	mov	a,_bp
	add	a,#0x0b
	xch	a,r0
	mov	@r0,dpl
	inc	r0
	mov	@r0,dph
	dec	sp
	dec	sp
	mov	a,_bp
	add	a,#0x04
	mov	r0,a
	mov	a,_bp
	add	a,#0x06
	mov	r1,a
	mov	a,@r1
	add	a,@r0
	mov	r2,a
	inc	r1
	mov	a,@r1
	inc	r0
	addc	a,@r0
	mov	r4,a
	push	ar2
	push	ar4
	mov	a,_bp
	add	a,#0x0b
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	lcall	__divsint
	mov	r3,dpl
	mov	r4,dph
	dec	sp
	dec	sp
	pop	ar5
	pop	ar6
	pop	ar7
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	mov	a,r3
	lcall	__gptrput
	sjmp	00118$
00113$:
;	source/CH549_TCS34725.c:248: Hsl->s=difVal*100/(200-(maxVal+minVal));
	mov	a,_bp
	add	a,#0x08
	mov	r0,a
	mov	a,#0x02
	add	a,@r0
	mov	r5,a
	clr	a
	inc	r0
	addc	a,@r0
	mov	r6,a
	inc	r0
	mov	ar7,@r0
	mov	a,_bp
	add	a,#0x0e
	mov	r0,a
	mov	ar3,@r0
	mov	r4,#0x00
	push	ar7
	push	ar6
	push	ar5
	push	ar3
	push	ar4
	mov	dptr,#0x0064
	lcall	__mulint
	xch	a,r0
	mov	a,_bp
	add	a,#0x08
	xch	a,r0
	mov	@r0,dpl
	inc	r0
	mov	@r0,dph
	dec	sp
	dec	sp
	mov	a,_bp
	add	a,#0x04
	mov	r0,a
	mov	a,_bp
	add	a,#0x06
	mov	r1,a
	mov	a,@r1
	add	a,@r0
	mov	r2,a
	inc	r1
	mov	a,@r1
	inc	r0
	addc	a,@r0
	mov	r4,a
	mov	a,#0xc8
	clr	c
	subb	a,r2
	mov	r2,a
	clr	a
	subb	a,r4
	mov	r4,a
	push	ar2
	push	ar4
	mov	a,_bp
	add	a,#0x08
	mov	r0,a
	mov	dpl,@r0
	inc	r0
	mov	dph,@r0
	lcall	__divsint
	mov	r3,dpl
	mov	r4,dph
	dec	sp
	dec	sp
	pop	ar5
	pop	ar6
	pop	ar7
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	mov	a,r3
	lcall	__gptrput
00118$:
;	source/CH549_TCS34725.c:250: }
	mov	sp,_bp
	pop	_bp
	ret
	.area CSEG    (CODE)
	.area CONST   (CODE)
	.area CABS    (ABS,CODE)
