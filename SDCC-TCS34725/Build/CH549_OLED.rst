                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 4.0.0 #11528 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module CH549_OLED
                                      6 	.optsdcc -mmcs51 --model-large
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _Hzk
                                     12 	.globl _fontMatrix_8x16
                                     13 	.globl _fontMatrix_6x8
                                     14 	.globl _BMP2
                                     15 	.globl _BMP1
                                     16 	.globl _CH549SPIMasterWrite
                                     17 	.globl _UIF_BUS_RST
                                     18 	.globl _UIF_DETECT
                                     19 	.globl _UIF_TRANSFER
                                     20 	.globl _UIF_SUSPEND
                                     21 	.globl _UIF_HST_SOF
                                     22 	.globl _UIF_FIFO_OV
                                     23 	.globl _U_SIE_FREE
                                     24 	.globl _U_TOG_OK
                                     25 	.globl _U_IS_NAK
                                     26 	.globl _S0_R_FIFO
                                     27 	.globl _S0_T_FIFO
                                     28 	.globl _S0_FREE
                                     29 	.globl _S0_IF_BYTE
                                     30 	.globl _S0_IF_FIRST
                                     31 	.globl _S0_IF_OV
                                     32 	.globl _S0_FST_ACT
                                     33 	.globl _CP_RL2
                                     34 	.globl _C_T2
                                     35 	.globl _TR2
                                     36 	.globl _EXEN2
                                     37 	.globl _TCLK
                                     38 	.globl _RCLK
                                     39 	.globl _EXF2
                                     40 	.globl _CAP1F
                                     41 	.globl _TF2
                                     42 	.globl _RI
                                     43 	.globl _TI
                                     44 	.globl _RB8
                                     45 	.globl _TB8
                                     46 	.globl _REN
                                     47 	.globl _SM2
                                     48 	.globl _SM1
                                     49 	.globl _SM0
                                     50 	.globl _IT0
                                     51 	.globl _IE0
                                     52 	.globl _IT1
                                     53 	.globl _IE1
                                     54 	.globl _TR0
                                     55 	.globl _TF0
                                     56 	.globl _TR1
                                     57 	.globl _TF1
                                     58 	.globl _XI
                                     59 	.globl _XO
                                     60 	.globl _P4_0
                                     61 	.globl _P4_1
                                     62 	.globl _P4_2
                                     63 	.globl _P4_3
                                     64 	.globl _P4_4
                                     65 	.globl _P4_5
                                     66 	.globl _P4_6
                                     67 	.globl _RXD
                                     68 	.globl _TXD
                                     69 	.globl _INT0
                                     70 	.globl _INT1
                                     71 	.globl _T0
                                     72 	.globl _T1
                                     73 	.globl _CAP0
                                     74 	.globl _INT3
                                     75 	.globl _P3_0
                                     76 	.globl _P3_1
                                     77 	.globl _P3_2
                                     78 	.globl _P3_3
                                     79 	.globl _P3_4
                                     80 	.globl _P3_5
                                     81 	.globl _P3_6
                                     82 	.globl _P3_7
                                     83 	.globl _PWM5
                                     84 	.globl _PWM4
                                     85 	.globl _INT0_
                                     86 	.globl _PWM3
                                     87 	.globl _PWM2
                                     88 	.globl _CAP1_
                                     89 	.globl _T2_
                                     90 	.globl _PWM1
                                     91 	.globl _CAP2_
                                     92 	.globl _T2EX_
                                     93 	.globl _PWM0
                                     94 	.globl _RXD1
                                     95 	.globl _PWM6
                                     96 	.globl _TXD1
                                     97 	.globl _PWM7
                                     98 	.globl _P2_0
                                     99 	.globl _P2_1
                                    100 	.globl _P2_2
                                    101 	.globl _P2_3
                                    102 	.globl _P2_4
                                    103 	.globl _P2_5
                                    104 	.globl _P2_6
                                    105 	.globl _P2_7
                                    106 	.globl _AIN0
                                    107 	.globl _CAP1
                                    108 	.globl _T2
                                    109 	.globl _AIN1
                                    110 	.globl _CAP2
                                    111 	.globl _T2EX
                                    112 	.globl _AIN2
                                    113 	.globl _AIN3
                                    114 	.globl _AIN4
                                    115 	.globl _UCC1
                                    116 	.globl _SCS
                                    117 	.globl _AIN5
                                    118 	.globl _UCC2
                                    119 	.globl _PWM0_
                                    120 	.globl _MOSI
                                    121 	.globl _AIN6
                                    122 	.globl _VBUS
                                    123 	.globl _RXD1_
                                    124 	.globl _MISO
                                    125 	.globl _AIN7
                                    126 	.globl _TXD1_
                                    127 	.globl _SCK
                                    128 	.globl _P1_0
                                    129 	.globl _P1_1
                                    130 	.globl _P1_2
                                    131 	.globl _P1_3
                                    132 	.globl _P1_4
                                    133 	.globl _P1_5
                                    134 	.globl _P1_6
                                    135 	.globl _P1_7
                                    136 	.globl _AIN8
                                    137 	.globl _AIN9
                                    138 	.globl _AIN10
                                    139 	.globl _RXD_
                                    140 	.globl _AIN11
                                    141 	.globl _TXD_
                                    142 	.globl _AIN12
                                    143 	.globl _RXD2
                                    144 	.globl _AIN13
                                    145 	.globl _TXD2
                                    146 	.globl _AIN14
                                    147 	.globl _RXD3
                                    148 	.globl _AIN15
                                    149 	.globl _TXD3
                                    150 	.globl _P0_0
                                    151 	.globl _P0_1
                                    152 	.globl _P0_2
                                    153 	.globl _P0_3
                                    154 	.globl _P0_4
                                    155 	.globl _P0_5
                                    156 	.globl _P0_6
                                    157 	.globl _P0_7
                                    158 	.globl _IE_SPI0
                                    159 	.globl _IE_INT3
                                    160 	.globl _IE_USB
                                    161 	.globl _IE_UART2
                                    162 	.globl _IE_ADC
                                    163 	.globl _IE_UART1
                                    164 	.globl _IE_UART3
                                    165 	.globl _IE_PWMX
                                    166 	.globl _IE_GPIO
                                    167 	.globl _IE_WDOG
                                    168 	.globl _PX0
                                    169 	.globl _PT0
                                    170 	.globl _PX1
                                    171 	.globl _PT1
                                    172 	.globl _PS
                                    173 	.globl _PT2
                                    174 	.globl _PL_FLAG
                                    175 	.globl _PH_FLAG
                                    176 	.globl _EX0
                                    177 	.globl _ET0
                                    178 	.globl _EX1
                                    179 	.globl _ET1
                                    180 	.globl _ES
                                    181 	.globl _ET2
                                    182 	.globl _E_DIS
                                    183 	.globl _EA
                                    184 	.globl _P
                                    185 	.globl _F1
                                    186 	.globl _OV
                                    187 	.globl _RS0
                                    188 	.globl _RS1
                                    189 	.globl _F0
                                    190 	.globl _AC
                                    191 	.globl _CY
                                    192 	.globl _UEP1_DMA_H
                                    193 	.globl _UEP1_DMA_L
                                    194 	.globl _UEP1_DMA
                                    195 	.globl _UEP0_DMA_H
                                    196 	.globl _UEP0_DMA_L
                                    197 	.globl _UEP0_DMA
                                    198 	.globl _UEP2_3_MOD
                                    199 	.globl _UEP4_1_MOD
                                    200 	.globl _UEP3_DMA_H
                                    201 	.globl _UEP3_DMA_L
                                    202 	.globl _UEP3_DMA
                                    203 	.globl _UEP2_DMA_H
                                    204 	.globl _UEP2_DMA_L
                                    205 	.globl _UEP2_DMA
                                    206 	.globl _USB_DEV_AD
                                    207 	.globl _USB_CTRL
                                    208 	.globl _USB_INT_EN
                                    209 	.globl _UEP4_T_LEN
                                    210 	.globl _UEP4_CTRL
                                    211 	.globl _UEP0_T_LEN
                                    212 	.globl _UEP0_CTRL
                                    213 	.globl _USB_RX_LEN
                                    214 	.globl _USB_MIS_ST
                                    215 	.globl _USB_INT_ST
                                    216 	.globl _USB_INT_FG
                                    217 	.globl _UEP3_T_LEN
                                    218 	.globl _UEP3_CTRL
                                    219 	.globl _UEP2_T_LEN
                                    220 	.globl _UEP2_CTRL
                                    221 	.globl _UEP1_T_LEN
                                    222 	.globl _UEP1_CTRL
                                    223 	.globl _UDEV_CTRL
                                    224 	.globl _USB_C_CTRL
                                    225 	.globl _ADC_PIN
                                    226 	.globl _ADC_CHAN
                                    227 	.globl _ADC_DAT_H
                                    228 	.globl _ADC_DAT_L
                                    229 	.globl _ADC_DAT
                                    230 	.globl _ADC_CFG
                                    231 	.globl _ADC_CTRL
                                    232 	.globl _TKEY_CTRL
                                    233 	.globl _SIF3
                                    234 	.globl _SBAUD3
                                    235 	.globl _SBUF3
                                    236 	.globl _SCON3
                                    237 	.globl _SIF2
                                    238 	.globl _SBAUD2
                                    239 	.globl _SBUF2
                                    240 	.globl _SCON2
                                    241 	.globl _SIF1
                                    242 	.globl _SBAUD1
                                    243 	.globl _SBUF1
                                    244 	.globl _SCON1
                                    245 	.globl _SPI0_SETUP
                                    246 	.globl _SPI0_CK_SE
                                    247 	.globl _SPI0_CTRL
                                    248 	.globl _SPI0_DATA
                                    249 	.globl _SPI0_STAT
                                    250 	.globl _PWM_DATA7
                                    251 	.globl _PWM_DATA6
                                    252 	.globl _PWM_DATA5
                                    253 	.globl _PWM_DATA4
                                    254 	.globl _PWM_DATA3
                                    255 	.globl _PWM_CTRL2
                                    256 	.globl _PWM_CK_SE
                                    257 	.globl _PWM_CTRL
                                    258 	.globl _PWM_DATA0
                                    259 	.globl _PWM_DATA1
                                    260 	.globl _PWM_DATA2
                                    261 	.globl _T2CAP1H
                                    262 	.globl _T2CAP1L
                                    263 	.globl _T2CAP1
                                    264 	.globl _TH2
                                    265 	.globl _TL2
                                    266 	.globl _T2COUNT
                                    267 	.globl _RCAP2H
                                    268 	.globl _RCAP2L
                                    269 	.globl _RCAP2
                                    270 	.globl _T2MOD
                                    271 	.globl _T2CON
                                    272 	.globl _T2CAP0H
                                    273 	.globl _T2CAP0L
                                    274 	.globl _T2CAP0
                                    275 	.globl _T2CON2
                                    276 	.globl _SBUF
                                    277 	.globl _SCON
                                    278 	.globl _TH1
                                    279 	.globl _TH0
                                    280 	.globl _TL1
                                    281 	.globl _TL0
                                    282 	.globl _TMOD
                                    283 	.globl _TCON
                                    284 	.globl _XBUS_AUX
                                    285 	.globl _PIN_FUNC
                                    286 	.globl _P5
                                    287 	.globl _P4_DIR_PU
                                    288 	.globl _P4_MOD_OC
                                    289 	.globl _P4
                                    290 	.globl _P3_DIR_PU
                                    291 	.globl _P3_MOD_OC
                                    292 	.globl _P3
                                    293 	.globl _P2_DIR_PU
                                    294 	.globl _P2_MOD_OC
                                    295 	.globl _P2
                                    296 	.globl _P1_DIR_PU
                                    297 	.globl _P1_MOD_OC
                                    298 	.globl _P1
                                    299 	.globl _P0_DIR_PU
                                    300 	.globl _P0_MOD_OC
                                    301 	.globl _P0
                                    302 	.globl _ROM_CTRL
                                    303 	.globl _ROM_DATA_HH
                                    304 	.globl _ROM_DATA_HL
                                    305 	.globl _ROM_DATA_HI
                                    306 	.globl _ROM_ADDR_H
                                    307 	.globl _ROM_ADDR_L
                                    308 	.globl _ROM_ADDR
                                    309 	.globl _GPIO_IE
                                    310 	.globl _INTX
                                    311 	.globl _IP_EX
                                    312 	.globl _IE_EX
                                    313 	.globl _IP
                                    314 	.globl _IE
                                    315 	.globl _WDOG_COUNT
                                    316 	.globl _RESET_KEEP
                                    317 	.globl _WAKE_CTRL
                                    318 	.globl _CLOCK_CFG
                                    319 	.globl _POWER_CFG
                                    320 	.globl _PCON
                                    321 	.globl _GLOBAL_CFG
                                    322 	.globl _SAFE_MOD
                                    323 	.globl _DPH
                                    324 	.globl _DPL
                                    325 	.globl _SP
                                    326 	.globl _A_INV
                                    327 	.globl _B
                                    328 	.globl _ACC
                                    329 	.globl _PSW
                                    330 	.globl _fontSize
                                    331 	.globl _setFontSize
                                    332 	.globl _delay_ms
                                    333 	.globl _OLED_WR_Byte
                                    334 	.globl _load_one_command
                                    335 	.globl _load_commandList
                                    336 	.globl _OLED_Init
                                    337 	.globl _OLED_Display_On
                                    338 	.globl _OLED_Display_Off
                                    339 	.globl _OLED_Clear
                                    340 	.globl _OLED_Set_Pos
                                    341 	.globl _OLED_ShowChar
                                    342 	.globl _OLED_ShowString
                                    343 	.globl _OLED_ShowCHinese
                                    344 	.globl _OLED_DrawBMP
                                    345 ;--------------------------------------------------------
                                    346 ; special function registers
                                    347 ;--------------------------------------------------------
                                    348 	.area RSEG    (ABS,DATA)
      000000                        349 	.org 0x0000
                           0000D0   350 _PSW	=	0x00d0
                           0000E0   351 _ACC	=	0x00e0
                           0000F0   352 _B	=	0x00f0
                           0000FD   353 _A_INV	=	0x00fd
                           000081   354 _SP	=	0x0081
                           000082   355 _DPL	=	0x0082
                           000083   356 _DPH	=	0x0083
                           0000A1   357 _SAFE_MOD	=	0x00a1
                           0000B1   358 _GLOBAL_CFG	=	0x00b1
                           000087   359 _PCON	=	0x0087
                           0000BA   360 _POWER_CFG	=	0x00ba
                           0000B9   361 _CLOCK_CFG	=	0x00b9
                           0000A9   362 _WAKE_CTRL	=	0x00a9
                           0000FE   363 _RESET_KEEP	=	0x00fe
                           0000FF   364 _WDOG_COUNT	=	0x00ff
                           0000A8   365 _IE	=	0x00a8
                           0000B8   366 _IP	=	0x00b8
                           0000E8   367 _IE_EX	=	0x00e8
                           0000E9   368 _IP_EX	=	0x00e9
                           0000B3   369 _INTX	=	0x00b3
                           0000B2   370 _GPIO_IE	=	0x00b2
                           008584   371 _ROM_ADDR	=	0x8584
                           000084   372 _ROM_ADDR_L	=	0x0084
                           000085   373 _ROM_ADDR_H	=	0x0085
                           008F8E   374 _ROM_DATA_HI	=	0x8f8e
                           00008E   375 _ROM_DATA_HL	=	0x008e
                           00008F   376 _ROM_DATA_HH	=	0x008f
                           000086   377 _ROM_CTRL	=	0x0086
                           000080   378 _P0	=	0x0080
                           0000C4   379 _P0_MOD_OC	=	0x00c4
                           0000C5   380 _P0_DIR_PU	=	0x00c5
                           000090   381 _P1	=	0x0090
                           000092   382 _P1_MOD_OC	=	0x0092
                           000093   383 _P1_DIR_PU	=	0x0093
                           0000A0   384 _P2	=	0x00a0
                           000094   385 _P2_MOD_OC	=	0x0094
                           000095   386 _P2_DIR_PU	=	0x0095
                           0000B0   387 _P3	=	0x00b0
                           000096   388 _P3_MOD_OC	=	0x0096
                           000097   389 _P3_DIR_PU	=	0x0097
                           0000C0   390 _P4	=	0x00c0
                           0000C2   391 _P4_MOD_OC	=	0x00c2
                           0000C3   392 _P4_DIR_PU	=	0x00c3
                           0000AB   393 _P5	=	0x00ab
                           0000AA   394 _PIN_FUNC	=	0x00aa
                           0000A2   395 _XBUS_AUX	=	0x00a2
                           000088   396 _TCON	=	0x0088
                           000089   397 _TMOD	=	0x0089
                           00008A   398 _TL0	=	0x008a
                           00008B   399 _TL1	=	0x008b
                           00008C   400 _TH0	=	0x008c
                           00008D   401 _TH1	=	0x008d
                           000098   402 _SCON	=	0x0098
                           000099   403 _SBUF	=	0x0099
                           0000C1   404 _T2CON2	=	0x00c1
                           00C7C6   405 _T2CAP0	=	0xc7c6
                           0000C6   406 _T2CAP0L	=	0x00c6
                           0000C7   407 _T2CAP0H	=	0x00c7
                           0000C8   408 _T2CON	=	0x00c8
                           0000C9   409 _T2MOD	=	0x00c9
                           00CBCA   410 _RCAP2	=	0xcbca
                           0000CA   411 _RCAP2L	=	0x00ca
                           0000CB   412 _RCAP2H	=	0x00cb
                           00CDCC   413 _T2COUNT	=	0xcdcc
                           0000CC   414 _TL2	=	0x00cc
                           0000CD   415 _TH2	=	0x00cd
                           00CFCE   416 _T2CAP1	=	0xcfce
                           0000CE   417 _T2CAP1L	=	0x00ce
                           0000CF   418 _T2CAP1H	=	0x00cf
                           00009A   419 _PWM_DATA2	=	0x009a
                           00009B   420 _PWM_DATA1	=	0x009b
                           00009C   421 _PWM_DATA0	=	0x009c
                           00009D   422 _PWM_CTRL	=	0x009d
                           00009E   423 _PWM_CK_SE	=	0x009e
                           00009F   424 _PWM_CTRL2	=	0x009f
                           0000A3   425 _PWM_DATA3	=	0x00a3
                           0000A4   426 _PWM_DATA4	=	0x00a4
                           0000A5   427 _PWM_DATA5	=	0x00a5
                           0000A6   428 _PWM_DATA6	=	0x00a6
                           0000A7   429 _PWM_DATA7	=	0x00a7
                           0000F8   430 _SPI0_STAT	=	0x00f8
                           0000F9   431 _SPI0_DATA	=	0x00f9
                           0000FA   432 _SPI0_CTRL	=	0x00fa
                           0000FB   433 _SPI0_CK_SE	=	0x00fb
                           0000FC   434 _SPI0_SETUP	=	0x00fc
                           0000BC   435 _SCON1	=	0x00bc
                           0000BD   436 _SBUF1	=	0x00bd
                           0000BE   437 _SBAUD1	=	0x00be
                           0000BF   438 _SIF1	=	0x00bf
                           0000B4   439 _SCON2	=	0x00b4
                           0000B5   440 _SBUF2	=	0x00b5
                           0000B6   441 _SBAUD2	=	0x00b6
                           0000B7   442 _SIF2	=	0x00b7
                           0000AC   443 _SCON3	=	0x00ac
                           0000AD   444 _SBUF3	=	0x00ad
                           0000AE   445 _SBAUD3	=	0x00ae
                           0000AF   446 _SIF3	=	0x00af
                           0000F1   447 _TKEY_CTRL	=	0x00f1
                           0000F2   448 _ADC_CTRL	=	0x00f2
                           0000F3   449 _ADC_CFG	=	0x00f3
                           00F5F4   450 _ADC_DAT	=	0xf5f4
                           0000F4   451 _ADC_DAT_L	=	0x00f4
                           0000F5   452 _ADC_DAT_H	=	0x00f5
                           0000F6   453 _ADC_CHAN	=	0x00f6
                           0000F7   454 _ADC_PIN	=	0x00f7
                           000091   455 _USB_C_CTRL	=	0x0091
                           0000D1   456 _UDEV_CTRL	=	0x00d1
                           0000D2   457 _UEP1_CTRL	=	0x00d2
                           0000D3   458 _UEP1_T_LEN	=	0x00d3
                           0000D4   459 _UEP2_CTRL	=	0x00d4
                           0000D5   460 _UEP2_T_LEN	=	0x00d5
                           0000D6   461 _UEP3_CTRL	=	0x00d6
                           0000D7   462 _UEP3_T_LEN	=	0x00d7
                           0000D8   463 _USB_INT_FG	=	0x00d8
                           0000D9   464 _USB_INT_ST	=	0x00d9
                           0000DA   465 _USB_MIS_ST	=	0x00da
                           0000DB   466 _USB_RX_LEN	=	0x00db
                           0000DC   467 _UEP0_CTRL	=	0x00dc
                           0000DD   468 _UEP0_T_LEN	=	0x00dd
                           0000DE   469 _UEP4_CTRL	=	0x00de
                           0000DF   470 _UEP4_T_LEN	=	0x00df
                           0000E1   471 _USB_INT_EN	=	0x00e1
                           0000E2   472 _USB_CTRL	=	0x00e2
                           0000E3   473 _USB_DEV_AD	=	0x00e3
                           00E5E4   474 _UEP2_DMA	=	0xe5e4
                           0000E4   475 _UEP2_DMA_L	=	0x00e4
                           0000E5   476 _UEP2_DMA_H	=	0x00e5
                           00E7E6   477 _UEP3_DMA	=	0xe7e6
                           0000E6   478 _UEP3_DMA_L	=	0x00e6
                           0000E7   479 _UEP3_DMA_H	=	0x00e7
                           0000EA   480 _UEP4_1_MOD	=	0x00ea
                           0000EB   481 _UEP2_3_MOD	=	0x00eb
                           00EDEC   482 _UEP0_DMA	=	0xedec
                           0000EC   483 _UEP0_DMA_L	=	0x00ec
                           0000ED   484 _UEP0_DMA_H	=	0x00ed
                           00EFEE   485 _UEP1_DMA	=	0xefee
                           0000EE   486 _UEP1_DMA_L	=	0x00ee
                           0000EF   487 _UEP1_DMA_H	=	0x00ef
                                    488 ;--------------------------------------------------------
                                    489 ; special function bits
                                    490 ;--------------------------------------------------------
                                    491 	.area RSEG    (ABS,DATA)
      000000                        492 	.org 0x0000
                           0000D7   493 _CY	=	0x00d7
                           0000D6   494 _AC	=	0x00d6
                           0000D5   495 _F0	=	0x00d5
                           0000D4   496 _RS1	=	0x00d4
                           0000D3   497 _RS0	=	0x00d3
                           0000D2   498 _OV	=	0x00d2
                           0000D1   499 _F1	=	0x00d1
                           0000D0   500 _P	=	0x00d0
                           0000AF   501 _EA	=	0x00af
                           0000AE   502 _E_DIS	=	0x00ae
                           0000AD   503 _ET2	=	0x00ad
                           0000AC   504 _ES	=	0x00ac
                           0000AB   505 _ET1	=	0x00ab
                           0000AA   506 _EX1	=	0x00aa
                           0000A9   507 _ET0	=	0x00a9
                           0000A8   508 _EX0	=	0x00a8
                           0000BF   509 _PH_FLAG	=	0x00bf
                           0000BE   510 _PL_FLAG	=	0x00be
                           0000BD   511 _PT2	=	0x00bd
                           0000BC   512 _PS	=	0x00bc
                           0000BB   513 _PT1	=	0x00bb
                           0000BA   514 _PX1	=	0x00ba
                           0000B9   515 _PT0	=	0x00b9
                           0000B8   516 _PX0	=	0x00b8
                           0000EF   517 _IE_WDOG	=	0x00ef
                           0000EE   518 _IE_GPIO	=	0x00ee
                           0000ED   519 _IE_PWMX	=	0x00ed
                           0000ED   520 _IE_UART3	=	0x00ed
                           0000EC   521 _IE_UART1	=	0x00ec
                           0000EB   522 _IE_ADC	=	0x00eb
                           0000EB   523 _IE_UART2	=	0x00eb
                           0000EA   524 _IE_USB	=	0x00ea
                           0000E9   525 _IE_INT3	=	0x00e9
                           0000E8   526 _IE_SPI0	=	0x00e8
                           000087   527 _P0_7	=	0x0087
                           000086   528 _P0_6	=	0x0086
                           000085   529 _P0_5	=	0x0085
                           000084   530 _P0_4	=	0x0084
                           000083   531 _P0_3	=	0x0083
                           000082   532 _P0_2	=	0x0082
                           000081   533 _P0_1	=	0x0081
                           000080   534 _P0_0	=	0x0080
                           000087   535 _TXD3	=	0x0087
                           000087   536 _AIN15	=	0x0087
                           000086   537 _RXD3	=	0x0086
                           000086   538 _AIN14	=	0x0086
                           000085   539 _TXD2	=	0x0085
                           000085   540 _AIN13	=	0x0085
                           000084   541 _RXD2	=	0x0084
                           000084   542 _AIN12	=	0x0084
                           000083   543 _TXD_	=	0x0083
                           000083   544 _AIN11	=	0x0083
                           000082   545 _RXD_	=	0x0082
                           000082   546 _AIN10	=	0x0082
                           000081   547 _AIN9	=	0x0081
                           000080   548 _AIN8	=	0x0080
                           000097   549 _P1_7	=	0x0097
                           000096   550 _P1_6	=	0x0096
                           000095   551 _P1_5	=	0x0095
                           000094   552 _P1_4	=	0x0094
                           000093   553 _P1_3	=	0x0093
                           000092   554 _P1_2	=	0x0092
                           000091   555 _P1_1	=	0x0091
                           000090   556 _P1_0	=	0x0090
                           000097   557 _SCK	=	0x0097
                           000097   558 _TXD1_	=	0x0097
                           000097   559 _AIN7	=	0x0097
                           000096   560 _MISO	=	0x0096
                           000096   561 _RXD1_	=	0x0096
                           000096   562 _VBUS	=	0x0096
                           000096   563 _AIN6	=	0x0096
                           000095   564 _MOSI	=	0x0095
                           000095   565 _PWM0_	=	0x0095
                           000095   566 _UCC2	=	0x0095
                           000095   567 _AIN5	=	0x0095
                           000094   568 _SCS	=	0x0094
                           000094   569 _UCC1	=	0x0094
                           000094   570 _AIN4	=	0x0094
                           000093   571 _AIN3	=	0x0093
                           000092   572 _AIN2	=	0x0092
                           000091   573 _T2EX	=	0x0091
                           000091   574 _CAP2	=	0x0091
                           000091   575 _AIN1	=	0x0091
                           000090   576 _T2	=	0x0090
                           000090   577 _CAP1	=	0x0090
                           000090   578 _AIN0	=	0x0090
                           0000A7   579 _P2_7	=	0x00a7
                           0000A6   580 _P2_6	=	0x00a6
                           0000A5   581 _P2_5	=	0x00a5
                           0000A4   582 _P2_4	=	0x00a4
                           0000A3   583 _P2_3	=	0x00a3
                           0000A2   584 _P2_2	=	0x00a2
                           0000A1   585 _P2_1	=	0x00a1
                           0000A0   586 _P2_0	=	0x00a0
                           0000A7   587 _PWM7	=	0x00a7
                           0000A7   588 _TXD1	=	0x00a7
                           0000A6   589 _PWM6	=	0x00a6
                           0000A6   590 _RXD1	=	0x00a6
                           0000A5   591 _PWM0	=	0x00a5
                           0000A5   592 _T2EX_	=	0x00a5
                           0000A5   593 _CAP2_	=	0x00a5
                           0000A4   594 _PWM1	=	0x00a4
                           0000A4   595 _T2_	=	0x00a4
                           0000A4   596 _CAP1_	=	0x00a4
                           0000A3   597 _PWM2	=	0x00a3
                           0000A2   598 _PWM3	=	0x00a2
                           0000A2   599 _INT0_	=	0x00a2
                           0000A1   600 _PWM4	=	0x00a1
                           0000A0   601 _PWM5	=	0x00a0
                           0000B7   602 _P3_7	=	0x00b7
                           0000B6   603 _P3_6	=	0x00b6
                           0000B5   604 _P3_5	=	0x00b5
                           0000B4   605 _P3_4	=	0x00b4
                           0000B3   606 _P3_3	=	0x00b3
                           0000B2   607 _P3_2	=	0x00b2
                           0000B1   608 _P3_1	=	0x00b1
                           0000B0   609 _P3_0	=	0x00b0
                           0000B7   610 _INT3	=	0x00b7
                           0000B6   611 _CAP0	=	0x00b6
                           0000B5   612 _T1	=	0x00b5
                           0000B4   613 _T0	=	0x00b4
                           0000B3   614 _INT1	=	0x00b3
                           0000B2   615 _INT0	=	0x00b2
                           0000B1   616 _TXD	=	0x00b1
                           0000B0   617 _RXD	=	0x00b0
                           0000C6   618 _P4_6	=	0x00c6
                           0000C5   619 _P4_5	=	0x00c5
                           0000C4   620 _P4_4	=	0x00c4
                           0000C3   621 _P4_3	=	0x00c3
                           0000C2   622 _P4_2	=	0x00c2
                           0000C1   623 _P4_1	=	0x00c1
                           0000C0   624 _P4_0	=	0x00c0
                           0000C7   625 _XO	=	0x00c7
                           0000C6   626 _XI	=	0x00c6
                           00008F   627 _TF1	=	0x008f
                           00008E   628 _TR1	=	0x008e
                           00008D   629 _TF0	=	0x008d
                           00008C   630 _TR0	=	0x008c
                           00008B   631 _IE1	=	0x008b
                           00008A   632 _IT1	=	0x008a
                           000089   633 _IE0	=	0x0089
                           000088   634 _IT0	=	0x0088
                           00009F   635 _SM0	=	0x009f
                           00009E   636 _SM1	=	0x009e
                           00009D   637 _SM2	=	0x009d
                           00009C   638 _REN	=	0x009c
                           00009B   639 _TB8	=	0x009b
                           00009A   640 _RB8	=	0x009a
                           000099   641 _TI	=	0x0099
                           000098   642 _RI	=	0x0098
                           0000CF   643 _TF2	=	0x00cf
                           0000CF   644 _CAP1F	=	0x00cf
                           0000CE   645 _EXF2	=	0x00ce
                           0000CD   646 _RCLK	=	0x00cd
                           0000CC   647 _TCLK	=	0x00cc
                           0000CB   648 _EXEN2	=	0x00cb
                           0000CA   649 _TR2	=	0x00ca
                           0000C9   650 _C_T2	=	0x00c9
                           0000C8   651 _CP_RL2	=	0x00c8
                           0000FF   652 _S0_FST_ACT	=	0x00ff
                           0000FE   653 _S0_IF_OV	=	0x00fe
                           0000FD   654 _S0_IF_FIRST	=	0x00fd
                           0000FC   655 _S0_IF_BYTE	=	0x00fc
                           0000FB   656 _S0_FREE	=	0x00fb
                           0000FA   657 _S0_T_FIFO	=	0x00fa
                           0000F8   658 _S0_R_FIFO	=	0x00f8
                           0000DF   659 _U_IS_NAK	=	0x00df
                           0000DE   660 _U_TOG_OK	=	0x00de
                           0000DD   661 _U_SIE_FREE	=	0x00dd
                           0000DC   662 _UIF_FIFO_OV	=	0x00dc
                           0000DB   663 _UIF_HST_SOF	=	0x00db
                           0000DA   664 _UIF_SUSPEND	=	0x00da
                           0000D9   665 _UIF_TRANSFER	=	0x00d9
                           0000D8   666 _UIF_DETECT	=	0x00d8
                           0000D8   667 _UIF_BUS_RST	=	0x00d8
                                    668 ;--------------------------------------------------------
                                    669 ; overlayable register banks
                                    670 ;--------------------------------------------------------
                                    671 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        672 	.ds 8
                                    673 ;--------------------------------------------------------
                                    674 ; internal ram data
                                    675 ;--------------------------------------------------------
                                    676 	.area DSEG    (DATA)
                                    677 ;--------------------------------------------------------
                                    678 ; overlayable items in internal ram 
                                    679 ;--------------------------------------------------------
                                    680 ;--------------------------------------------------------
                                    681 ; indirectly addressable internal ram data
                                    682 ;--------------------------------------------------------
                                    683 	.area ISEG    (DATA)
                                    684 ;--------------------------------------------------------
                                    685 ; absolute internal ram data
                                    686 ;--------------------------------------------------------
                                    687 	.area IABS    (ABS,DATA)
                                    688 	.area IABS    (ABS,DATA)
                                    689 ;--------------------------------------------------------
                                    690 ; bit data
                                    691 ;--------------------------------------------------------
                                    692 	.area BSEG    (BIT)
                                    693 ;--------------------------------------------------------
                                    694 ; paged external ram data
                                    695 ;--------------------------------------------------------
                                    696 	.area PSEG    (PAG,XDATA)
                                    697 ;--------------------------------------------------------
                                    698 ; external ram data
                                    699 ;--------------------------------------------------------
                                    700 	.area XSEG    (XDATA)
      000015                        701 _fontSize::
      000015                        702 	.ds 1
                                    703 ;--------------------------------------------------------
                                    704 ; absolute external ram data
                                    705 ;--------------------------------------------------------
                                    706 	.area XABS    (ABS,XDATA)
                                    707 ;--------------------------------------------------------
                                    708 ; external initialized ram data
                                    709 ;--------------------------------------------------------
                                    710 	.area HOME    (CODE)
                                    711 	.area GSINIT0 (CODE)
                                    712 	.area GSINIT1 (CODE)
                                    713 	.area GSINIT2 (CODE)
                                    714 	.area GSINIT3 (CODE)
                                    715 	.area GSINIT4 (CODE)
                                    716 	.area GSINIT5 (CODE)
                                    717 	.area GSINIT  (CODE)
                                    718 	.area GSFINAL (CODE)
                                    719 	.area CSEG    (CODE)
                                    720 ;--------------------------------------------------------
                                    721 ; global & static initialisations
                                    722 ;--------------------------------------------------------
                                    723 	.area HOME    (CODE)
                                    724 	.area GSINIT  (CODE)
                                    725 	.area GSFINAL (CODE)
                                    726 	.area GSINIT  (CODE)
                                    727 ;--------------------------------------------------------
                                    728 ; Home
                                    729 ;--------------------------------------------------------
                                    730 	.area HOME    (CODE)
                                    731 	.area HOME    (CODE)
                                    732 ;--------------------------------------------------------
                                    733 ; code
                                    734 ;--------------------------------------------------------
                                    735 	.area CSEG    (CODE)
                                    736 ;------------------------------------------------------------
                                    737 ;Allocation info for local variables in function 'setFontSize'
                                    738 ;------------------------------------------------------------
                                    739 ;size                      Allocated to registers 
                                    740 ;------------------------------------------------------------
                                    741 ;	source/CH549_OLED.c:18: void setFontSize(u8 size){
                                    742 ;	-----------------------------------------
                                    743 ;	 function setFontSize
                                    744 ;	-----------------------------------------
      0015D1                        745 _setFontSize:
                           000007   746 	ar7 = 0x07
                           000006   747 	ar6 = 0x06
                           000005   748 	ar5 = 0x05
                           000004   749 	ar4 = 0x04
                           000003   750 	ar3 = 0x03
                           000002   751 	ar2 = 0x02
                           000001   752 	ar1 = 0x01
                           000000   753 	ar0 = 0x00
      0015D1 E5 82            [12]  754 	mov	a,dpl
      0015D3 90 00 15         [24]  755 	mov	dptr,#_fontSize
      0015D6 F0               [24]  756 	movx	@dptr,a
                                    757 ;	source/CH549_OLED.c:19: fontSize = size;
                                    758 ;	source/CH549_OLED.c:20: }
      0015D7 22               [24]  759 	ret
                                    760 ;------------------------------------------------------------
                                    761 ;Allocation info for local variables in function 'delay_ms'
                                    762 ;------------------------------------------------------------
                                    763 ;ms                        Allocated to registers 
                                    764 ;a                         Allocated to registers r4 r5 
                                    765 ;------------------------------------------------------------
                                    766 ;	source/CH549_OLED.c:23: void delay_ms(unsigned int ms)
                                    767 ;	-----------------------------------------
                                    768 ;	 function delay_ms
                                    769 ;	-----------------------------------------
      0015D8                        770 _delay_ms:
      0015D8 AE 82            [24]  771 	mov	r6,dpl
      0015DA AF 83            [24]  772 	mov	r7,dph
                                    773 ;	source/CH549_OLED.c:26: while(ms)
      0015DC                        774 00104$:
      0015DC EE               [12]  775 	mov	a,r6
      0015DD 4F               [12]  776 	orl	a,r7
      0015DE 60 18            [24]  777 	jz	00106$
                                    778 ;	source/CH549_OLED.c:29: while(a--);
      0015E0 7C 08            [12]  779 	mov	r4,#0x08
      0015E2 7D 07            [12]  780 	mov	r5,#0x07
      0015E4                        781 00101$:
      0015E4 8C 02            [24]  782 	mov	ar2,r4
      0015E6 8D 03            [24]  783 	mov	ar3,r5
      0015E8 1C               [12]  784 	dec	r4
      0015E9 BC FF 01         [24]  785 	cjne	r4,#0xff,00128$
      0015EC 1D               [12]  786 	dec	r5
      0015ED                        787 00128$:
      0015ED EA               [12]  788 	mov	a,r2
      0015EE 4B               [12]  789 	orl	a,r3
      0015EF 70 F3            [24]  790 	jnz	00101$
                                    791 ;	source/CH549_OLED.c:30: ms--;
      0015F1 1E               [12]  792 	dec	r6
      0015F2 BE FF 01         [24]  793 	cjne	r6,#0xff,00130$
      0015F5 1F               [12]  794 	dec	r7
      0015F6                        795 00130$:
      0015F6 80 E4            [24]  796 	sjmp	00104$
      0015F8                        797 00106$:
                                    798 ;	source/CH549_OLED.c:32: return;
                                    799 ;	source/CH549_OLED.c:33: }
      0015F8 22               [24]  800 	ret
                                    801 ;------------------------------------------------------------
                                    802 ;Allocation info for local variables in function 'OLED_WR_Byte'
                                    803 ;------------------------------------------------------------
                                    804 ;cmd                       Allocated to stack - _bp -3
                                    805 ;dat                       Allocated to registers r7 
                                    806 ;------------------------------------------------------------
                                    807 ;	source/CH549_OLED.c:39: void OLED_WR_Byte(u8 dat,u8 cmd)
                                    808 ;	-----------------------------------------
                                    809 ;	 function OLED_WR_Byte
                                    810 ;	-----------------------------------------
      0015F9                        811 _OLED_WR_Byte:
      0015F9 C0 16            [24]  812 	push	_bp
      0015FB 85 81 16         [24]  813 	mov	_bp,sp
      0015FE AF 82            [24]  814 	mov	r7,dpl
                                    815 ;	source/CH549_OLED.c:42: if(cmd)	OLED_MODE_DATA(); 	//命令模式
      001600 E5 16            [12]  816 	mov	a,_bp
      001602 24 FD            [12]  817 	add	a,#0xfd
      001604 F8               [12]  818 	mov	r0,a
      001605 E6               [12]  819 	mov	a,@r0
      001606 60 04            [24]  820 	jz	00102$
                                    821 ;	assignBit
      001608 D2 A7            [12]  822 	setb	_P2_7
      00160A 80 02            [24]  823 	sjmp	00103$
      00160C                        824 00102$:
                                    825 ;	source/CH549_OLED.c:43: else 	OLED_MODE_COMMAND(); 	//数据模式
                                    826 ;	assignBit
      00160C C2 A7            [12]  827 	clr	_P2_7
      00160E                        828 00103$:
                                    829 ;	source/CH549_OLED.c:44: OLED_SELECT();			    //片选设置为0,设备选择
                                    830 ;	assignBit
      00160E C2 94            [12]  831 	clr	_P1_4
                                    832 ;	source/CH549_OLED.c:45: CH549SPIMasterWrite(dat);       //使用CH549的官方函数写入8位数据
      001610 8F 82            [24]  833 	mov	dpl,r7
      001612 12 15 9C         [24]  834 	lcall	_CH549SPIMasterWrite
                                    835 ;	source/CH549_OLED.c:46: OLED_DESELECT();			    //片选设置为1,取消设备选择
                                    836 ;	assignBit
      001615 D2 94            [12]  837 	setb	_P1_4
                                    838 ;	source/CH549_OLED.c:47: OLED_MODE_DATA();   	  	    //转为数据模式
                                    839 ;	assignBit
      001617 D2 A7            [12]  840 	setb	_P2_7
                                    841 ;	source/CH549_OLED.c:60: } 
      001619 D0 16            [24]  842 	pop	_bp
      00161B 22               [24]  843 	ret
                                    844 ;------------------------------------------------------------
                                    845 ;Allocation info for local variables in function 'load_one_command'
                                    846 ;------------------------------------------------------------
                                    847 ;c                         Allocated to registers r7 
                                    848 ;------------------------------------------------------------
                                    849 ;	source/CH549_OLED.c:62: void load_one_command(u8 c){
                                    850 ;	-----------------------------------------
                                    851 ;	 function load_one_command
                                    852 ;	-----------------------------------------
      00161C                        853 _load_one_command:
      00161C AF 82            [24]  854 	mov	r7,dpl
                                    855 ;	source/CH549_OLED.c:63: OLED_WR_Byte(c,OLED_CMD);
      00161E E4               [12]  856 	clr	a
      00161F C0 E0            [24]  857 	push	acc
      001621 8F 82            [24]  858 	mov	dpl,r7
      001623 12 15 F9         [24]  859 	lcall	_OLED_WR_Byte
      001626 15 81            [12]  860 	dec	sp
                                    861 ;	source/CH549_OLED.c:64: }
      001628 22               [24]  862 	ret
                                    863 ;------------------------------------------------------------
                                    864 ;Allocation info for local variables in function 'load_commandList'
                                    865 ;------------------------------------------------------------
                                    866 ;n                         Allocated to stack - _bp -3
                                    867 ;c                         Allocated to registers 
                                    868 ;------------------------------------------------------------
                                    869 ;	source/CH549_OLED.c:66: void load_commandList(const u8 *c, u8 n){
                                    870 ;	-----------------------------------------
                                    871 ;	 function load_commandList
                                    872 ;	-----------------------------------------
      001629                        873 _load_commandList:
      001629 C0 16            [24]  874 	push	_bp
      00162B 85 81 16         [24]  875 	mov	_bp,sp
      00162E AD 82            [24]  876 	mov	r5,dpl
      001630 AE 83            [24]  877 	mov	r6,dph
      001632 AF F0            [24]  878 	mov	r7,b
                                    879 ;	source/CH549_OLED.c:67: while (n--) 
      001634 E5 16            [12]  880 	mov	a,_bp
      001636 24 FD            [12]  881 	add	a,#0xfd
      001638 F8               [12]  882 	mov	r0,a
      001639 86 04            [24]  883 	mov	ar4,@r0
      00163B                        884 00101$:
      00163B 8C 03            [24]  885 	mov	ar3,r4
      00163D 1C               [12]  886 	dec	r4
      00163E EB               [12]  887 	mov	a,r3
      00163F 60 2B            [24]  888 	jz	00104$
                                    889 ;	source/CH549_OLED.c:68: OLED_WR_Byte(pgm_read_byte(c++),OLED_CMD);
      001641 8D 82            [24]  890 	mov	dpl,r5
      001643 8E 83            [24]  891 	mov	dph,r6
      001645 8F F0            [24]  892 	mov	b,r7
      001647 12 22 F7         [24]  893 	lcall	__gptrget
      00164A FB               [12]  894 	mov	r3,a
      00164B A3               [24]  895 	inc	dptr
      00164C AD 82            [24]  896 	mov	r5,dpl
      00164E AE 83            [24]  897 	mov	r6,dph
      001650 C0 07            [24]  898 	push	ar7
      001652 C0 06            [24]  899 	push	ar6
      001654 C0 05            [24]  900 	push	ar5
      001656 C0 04            [24]  901 	push	ar4
      001658 E4               [12]  902 	clr	a
      001659 C0 E0            [24]  903 	push	acc
      00165B 8B 82            [24]  904 	mov	dpl,r3
      00165D 12 15 F9         [24]  905 	lcall	_OLED_WR_Byte
      001660 15 81            [12]  906 	dec	sp
      001662 D0 04            [24]  907 	pop	ar4
      001664 D0 05            [24]  908 	pop	ar5
      001666 D0 06            [24]  909 	pop	ar6
      001668 D0 07            [24]  910 	pop	ar7
      00166A 80 CF            [24]  911 	sjmp	00101$
      00166C                        912 00104$:
                                    913 ;	source/CH549_OLED.c:70: }
      00166C D0 16            [24]  914 	pop	_bp
      00166E 22               [24]  915 	ret
                                    916 ;------------------------------------------------------------
                                    917 ;Allocation info for local variables in function 'OLED_Init'
                                    918 ;------------------------------------------------------------
                                    919 ;	source/CH549_OLED.c:73: void OLED_Init(void)
                                    920 ;	-----------------------------------------
                                    921 ;	 function OLED_Init
                                    922 ;	-----------------------------------------
      00166F                        923 _OLED_Init:
                                    924 ;	source/CH549_OLED.c:76: OLED_RST_Set();
                                    925 ;	assignBit
      00166F D2 B5            [12]  926 	setb	_P3_5
                                    927 ;	source/CH549_OLED.c:77: delay_ms(100);
      001671 90 00 64         [24]  928 	mov	dptr,#0x0064
      001674 12 15 D8         [24]  929 	lcall	_delay_ms
                                    930 ;	source/CH549_OLED.c:78: OLED_RST_Clr();
                                    931 ;	assignBit
      001677 C2 B5            [12]  932 	clr	_P3_5
                                    933 ;	source/CH549_OLED.c:79: delay_ms(100);
      001679 90 00 64         [24]  934 	mov	dptr,#0x0064
      00167C 12 15 D8         [24]  935 	lcall	_delay_ms
                                    936 ;	source/CH549_OLED.c:80: OLED_RST_Set(); 
                                    937 ;	assignBit
      00167F D2 B5            [12]  938 	setb	_P3_5
                                    939 ;	source/CH549_OLED.c:116: load_commandList(init_commandList, sizeof(init_commandList));
      001681 74 19            [12]  940 	mov	a,#0x19
      001683 C0 E0            [24]  941 	push	acc
      001685 90 50 0A         [24]  942 	mov	dptr,#_OLED_Init_init_commandList_65537_73
      001688 75 F0 80         [24]  943 	mov	b,#0x80
      00168B 12 16 29         [24]  944 	lcall	_load_commandList
      00168E 15 81            [12]  945 	dec	sp
                                    946 ;	source/CH549_OLED.c:118: OLED_Clear();
      001690 12 16 C3         [24]  947 	lcall	_OLED_Clear
                                    948 ;	source/CH549_OLED.c:119: OLED_Set_Pos(0,0); 	
      001693 E4               [12]  949 	clr	a
      001694 C0 E0            [24]  950 	push	acc
      001696 75 82 00         [24]  951 	mov	dpl,#0x00
      001699 12 17 02         [24]  952 	lcall	_OLED_Set_Pos
      00169C 15 81            [12]  953 	dec	sp
                                    954 ;	source/CH549_OLED.c:120: }
      00169E 22               [24]  955 	ret
                                    956 ;------------------------------------------------------------
                                    957 ;Allocation info for local variables in function 'OLED_Display_On'
                                    958 ;------------------------------------------------------------
                                    959 ;	source/CH549_OLED.c:123: void OLED_Display_On(void)
                                    960 ;	-----------------------------------------
                                    961 ;	 function OLED_Display_On
                                    962 ;	-----------------------------------------
      00169F                        963 _OLED_Display_On:
                                    964 ;	source/CH549_OLED.c:125: load_one_command(SSD1306_CHARGEPUMP);	//SET DCDC命令
      00169F 75 82 8D         [24]  965 	mov	dpl,#0x8d
      0016A2 12 16 1C         [24]  966 	lcall	_load_one_command
                                    967 ;	source/CH549_OLED.c:126: load_one_command(SSD1306_0x10DISABLE);	//DCDC ON
      0016A5 75 82 14         [24]  968 	mov	dpl,#0x14
      0016A8 12 16 1C         [24]  969 	lcall	_load_one_command
                                    970 ;	source/CH549_OLED.c:127: load_one_command(SSD1306_DISPLAYON);	//DISPLAY ON
      0016AB 75 82 AF         [24]  971 	mov	dpl,#0xaf
                                    972 ;	source/CH549_OLED.c:128: }
      0016AE 02 16 1C         [24]  973 	ljmp	_load_one_command
                                    974 ;------------------------------------------------------------
                                    975 ;Allocation info for local variables in function 'OLED_Display_Off'
                                    976 ;------------------------------------------------------------
                                    977 ;	source/CH549_OLED.c:131: void OLED_Display_Off(void)
                                    978 ;	-----------------------------------------
                                    979 ;	 function OLED_Display_Off
                                    980 ;	-----------------------------------------
      0016B1                        981 _OLED_Display_Off:
                                    982 ;	source/CH549_OLED.c:133: load_one_command(SSD1306_CHARGEPUMP);	//SET DCDC命令
      0016B1 75 82 8D         [24]  983 	mov	dpl,#0x8d
      0016B4 12 16 1C         [24]  984 	lcall	_load_one_command
                                    985 ;	source/CH549_OLED.c:134: load_one_command(SSD1306_SETHIGHCOLUMN);	//DCDC OFF
      0016B7 75 82 10         [24]  986 	mov	dpl,#0x10
      0016BA 12 16 1C         [24]  987 	lcall	_load_one_command
                                    988 ;	source/CH549_OLED.c:135: load_one_command(0XAE);	//DISPLAY OFF
      0016BD 75 82 AE         [24]  989 	mov	dpl,#0xae
                                    990 ;	source/CH549_OLED.c:136: }	
      0016C0 02 16 1C         [24]  991 	ljmp	_load_one_command
                                    992 ;------------------------------------------------------------
                                    993 ;Allocation info for local variables in function 'OLED_Clear'
                                    994 ;------------------------------------------------------------
                                    995 ;i                         Allocated to registers r7 
                                    996 ;n                         Allocated to registers r6 
                                    997 ;------------------------------------------------------------
                                    998 ;	source/CH549_OLED.c:139: void OLED_Clear(void)
                                    999 ;	-----------------------------------------
                                   1000 ;	 function OLED_Clear
                                   1001 ;	-----------------------------------------
      0016C3                       1002 _OLED_Clear:
                                   1003 ;	source/CH549_OLED.c:142: for(i=0;i<8;i++)  
      0016C3 7F 00            [12] 1004 	mov	r7,#0x00
      0016C5                       1005 00105$:
                                   1006 ;	source/CH549_OLED.c:144: load_one_command(0xb0+i);	//设置页地址（0~7）
      0016C5 8F 06            [24] 1007 	mov	ar6,r7
      0016C7 74 B0            [12] 1008 	mov	a,#0xb0
      0016C9 2E               [12] 1009 	add	a,r6
      0016CA F5 82            [12] 1010 	mov	dpl,a
      0016CC C0 07            [24] 1011 	push	ar7
      0016CE 12 16 1C         [24] 1012 	lcall	_load_one_command
                                   1013 ;	source/CH549_OLED.c:145: load_one_command(SSD1306_SETLOWCOLUMN);		//设置显示位置—列低地址
      0016D1 75 82 00         [24] 1014 	mov	dpl,#0x00
      0016D4 12 16 1C         [24] 1015 	lcall	_load_one_command
                                   1016 ;	source/CH549_OLED.c:146: load_one_command(SSD1306_SETHIGHCOLUMN);		//设置显示位置—列高地址 
      0016D7 75 82 10         [24] 1017 	mov	dpl,#0x10
      0016DA 12 16 1C         [24] 1018 	lcall	_load_one_command
      0016DD D0 07            [24] 1019 	pop	ar7
                                   1020 ;	source/CH549_OLED.c:148: for(n=0;n<128;n++)OLED_WR_Byte(0,OLED_DATA); 
      0016DF 7E 00            [12] 1021 	mov	r6,#0x00
      0016E1                       1022 00103$:
      0016E1 C0 07            [24] 1023 	push	ar7
      0016E3 C0 06            [24] 1024 	push	ar6
      0016E5 74 01            [12] 1025 	mov	a,#0x01
      0016E7 C0 E0            [24] 1026 	push	acc
      0016E9 75 82 00         [24] 1027 	mov	dpl,#0x00
      0016EC 12 15 F9         [24] 1028 	lcall	_OLED_WR_Byte
      0016EF 15 81            [12] 1029 	dec	sp
      0016F1 D0 06            [24] 1030 	pop	ar6
      0016F3 D0 07            [24] 1031 	pop	ar7
      0016F5 0E               [12] 1032 	inc	r6
      0016F6 BE 80 00         [24] 1033 	cjne	r6,#0x80,00123$
      0016F9                       1034 00123$:
      0016F9 40 E6            [24] 1035 	jc	00103$
                                   1036 ;	source/CH549_OLED.c:142: for(i=0;i<8;i++)  
      0016FB 0F               [12] 1037 	inc	r7
      0016FC BF 08 00         [24] 1038 	cjne	r7,#0x08,00125$
      0016FF                       1039 00125$:
      0016FF 40 C4            [24] 1040 	jc	00105$
                                   1041 ;	source/CH549_OLED.c:150: }
      001701 22               [24] 1042 	ret
                                   1043 ;------------------------------------------------------------
                                   1044 ;Allocation info for local variables in function 'OLED_Set_Pos'
                                   1045 ;------------------------------------------------------------
                                   1046 ;row_index                 Allocated to stack - _bp -3
                                   1047 ;col_index                 Allocated to registers r7 
                                   1048 ;------------------------------------------------------------
                                   1049 ;	source/CH549_OLED.c:157: void OLED_Set_Pos(unsigned char col_index, unsigned char row_index) 
                                   1050 ;	-----------------------------------------
                                   1051 ;	 function OLED_Set_Pos
                                   1052 ;	-----------------------------------------
      001702                       1053 _OLED_Set_Pos:
      001702 C0 16            [24] 1054 	push	_bp
      001704 85 81 16         [24] 1055 	mov	_bp,sp
      001707 AF 82            [24] 1056 	mov	r7,dpl
                                   1057 ;	source/CH549_OLED.c:159: load_one_command(0xb0+row_index);
      001709 E5 16            [12] 1058 	mov	a,_bp
      00170B 24 FD            [12] 1059 	add	a,#0xfd
      00170D F8               [12] 1060 	mov	r0,a
      00170E 86 06            [24] 1061 	mov	ar6,@r0
      001710 74 B0            [12] 1062 	mov	a,#0xb0
      001712 2E               [12] 1063 	add	a,r6
      001713 F5 82            [12] 1064 	mov	dpl,a
      001715 C0 07            [24] 1065 	push	ar7
      001717 12 16 1C         [24] 1066 	lcall	_load_one_command
      00171A D0 07            [24] 1067 	pop	ar7
                                   1068 ;	source/CH549_OLED.c:160: load_one_command(((col_index&0xf0)>>4)|SSD1306_SETHIGHCOLUMN);
      00171C 8F 05            [24] 1069 	mov	ar5,r7
      00171E 53 05 F0         [24] 1070 	anl	ar5,#0xf0
      001721 E4               [12] 1071 	clr	a
      001722 C4               [12] 1072 	swap	a
      001723 CD               [12] 1073 	xch	a,r5
      001724 C4               [12] 1074 	swap	a
      001725 54 0F            [12] 1075 	anl	a,#0x0f
      001727 6D               [12] 1076 	xrl	a,r5
      001728 CD               [12] 1077 	xch	a,r5
      001729 54 0F            [12] 1078 	anl	a,#0x0f
      00172B CD               [12] 1079 	xch	a,r5
      00172C 6D               [12] 1080 	xrl	a,r5
      00172D CD               [12] 1081 	xch	a,r5
      00172E 30 E3 02         [24] 1082 	jnb	acc.3,00103$
      001731 44 F0            [12] 1083 	orl	a,#0xf0
      001733                       1084 00103$:
      001733 43 05 10         [24] 1085 	orl	ar5,#0x10
      001736 8D 82            [24] 1086 	mov	dpl,r5
      001738 C0 07            [24] 1087 	push	ar7
      00173A 12 16 1C         [24] 1088 	lcall	_load_one_command
      00173D D0 07            [24] 1089 	pop	ar7
                                   1090 ;	source/CH549_OLED.c:161: load_one_command((col_index&0x0f)|0x01);
      00173F 53 07 0F         [24] 1091 	anl	ar7,#0x0f
      001742 43 07 01         [24] 1092 	orl	ar7,#0x01
      001745 8F 82            [24] 1093 	mov	dpl,r7
      001747 12 16 1C         [24] 1094 	lcall	_load_one_command
                                   1095 ;	source/CH549_OLED.c:162: }  
      00174A D0 16            [24] 1096 	pop	_bp
      00174C 22               [24] 1097 	ret
                                   1098 ;------------------------------------------------------------
                                   1099 ;Allocation info for local variables in function 'OLED_ShowChar'
                                   1100 ;------------------------------------------------------------
                                   1101 ;row_index                 Allocated to stack - _bp -3
                                   1102 ;chr                       Allocated to stack - _bp -4
                                   1103 ;col_index                 Allocated to registers r7 
                                   1104 ;char_index                Allocated to registers r6 
                                   1105 ;i                         Allocated to registers r5 
                                   1106 ;------------------------------------------------------------
                                   1107 ;	source/CH549_OLED.c:167: void OLED_ShowChar(u8 col_index, u8 row_index, u8 chr)
                                   1108 ;	-----------------------------------------
                                   1109 ;	 function OLED_ShowChar
                                   1110 ;	-----------------------------------------
      00174D                       1111 _OLED_ShowChar:
      00174D C0 16            [24] 1112 	push	_bp
      00174F 85 81 16         [24] 1113 	mov	_bp,sp
      001752 AF 82            [24] 1114 	mov	r7,dpl
                                   1115 ;	source/CH549_OLED.c:170: char_index = chr - ' ';	//将希望输入的字符的ascii码减去空格的ascii码，得到偏移后的值	因为在ascii码中space之前的字符并不能显示出来，字库里面不会有他们，减去一个空格相当于从空格往后开始数		
      001754 E5 16            [12] 1116 	mov	a,_bp
      001756 24 FC            [12] 1117 	add	a,#0xfc
      001758 F8               [12] 1118 	mov	r0,a
      001759 E6               [12] 1119 	mov	a,@r0
      00175A 24 E0            [12] 1120 	add	a,#0xe0
      00175C FE               [12] 1121 	mov	r6,a
                                   1122 ;	source/CH549_OLED.c:172: if (col_index > Max_Column - 1) {
      00175D EF               [12] 1123 	mov	a,r7
      00175E 24 80            [12] 1124 	add	a,#0xff - 0x7f
      001760 50 12            [24] 1125 	jnc	00102$
                                   1126 ;	source/CH549_OLED.c:173: col_index = 0;
      001762 7F 00            [12] 1127 	mov	r7,#0x00
                                   1128 ;	source/CH549_OLED.c:174: row_index = row_index + 2;
      001764 E5 16            [12] 1129 	mov	a,_bp
      001766 24 FD            [12] 1130 	add	a,#0xfd
      001768 F8               [12] 1131 	mov	r0,a
      001769 86 05            [24] 1132 	mov	ar5,@r0
      00176B E5 16            [12] 1133 	mov	a,_bp
      00176D 24 FD            [12] 1134 	add	a,#0xfd
      00176F F8               [12] 1135 	mov	r0,a
      001770 74 02            [12] 1136 	mov	a,#0x02
      001772 2D               [12] 1137 	add	a,r5
      001773 F6               [12] 1138 	mov	@r0,a
      001774                       1139 00102$:
                                   1140 ;	source/CH549_OLED.c:177: if (fontSize == 16) {
      001774 90 00 15         [24] 1141 	mov	dptr,#_fontSize
      001777 E0               [24] 1142 	movx	a,@dptr
      001778 FD               [12] 1143 	mov	r5,a
      001779 BD 10 02         [24] 1144 	cjne	r5,#0x10,00149$
      00177C 80 03            [24] 1145 	sjmp	00150$
      00177E                       1146 00149$:
      00177E 02 18 42         [24] 1147 	ljmp	00107$
      001781                       1148 00150$:
                                   1149 ;	source/CH549_OLED.c:178: OLED_Set_Pos(col_index, row_index);	
      001781 C0 07            [24] 1150 	push	ar7
      001783 C0 06            [24] 1151 	push	ar6
      001785 E5 16            [12] 1152 	mov	a,_bp
      001787 24 FD            [12] 1153 	add	a,#0xfd
      001789 F8               [12] 1154 	mov	r0,a
      00178A E6               [12] 1155 	mov	a,@r0
      00178B C0 E0            [24] 1156 	push	acc
      00178D 8F 82            [24] 1157 	mov	dpl,r7
      00178F 12 17 02         [24] 1158 	lcall	_OLED_Set_Pos
      001792 15 81            [12] 1159 	dec	sp
      001794 D0 06            [24] 1160 	pop	ar6
      001796 D0 07            [24] 1161 	pop	ar7
                                   1162 ;	source/CH549_OLED.c:179: for (i = 0; i < 8; i++)
      001798 7D 00            [12] 1163 	mov	r5,#0x00
      00179A                       1164 00109$:
                                   1165 ;	source/CH549_OLED.c:180: OLED_WR_Byte(fontMatrix_8x16[char_index * 16 + i], OLED_DATA); //通过DATA模式写入矩阵数据就是在点亮特定的像素点
      00179A C0 07            [24] 1166 	push	ar7
      00179C 8E 03            [24] 1167 	mov	ar3,r6
      00179E E4               [12] 1168 	clr	a
      00179F C4               [12] 1169 	swap	a
      0017A0 54 F0            [12] 1170 	anl	a,#0xf0
      0017A2 CB               [12] 1171 	xch	a,r3
      0017A3 C4               [12] 1172 	swap	a
      0017A4 CB               [12] 1173 	xch	a,r3
      0017A5 6B               [12] 1174 	xrl	a,r3
      0017A6 CB               [12] 1175 	xch	a,r3
      0017A7 54 F0            [12] 1176 	anl	a,#0xf0
      0017A9 CB               [12] 1177 	xch	a,r3
      0017AA 6B               [12] 1178 	xrl	a,r3
      0017AB FC               [12] 1179 	mov	r4,a
      0017AC 8D 02            [24] 1180 	mov	ar2,r5
      0017AE 7F 00            [12] 1181 	mov	r7,#0x00
      0017B0 EA               [12] 1182 	mov	a,r2
      0017B1 2B               [12] 1183 	add	a,r3
      0017B2 FA               [12] 1184 	mov	r2,a
      0017B3 EF               [12] 1185 	mov	a,r7
      0017B4 3C               [12] 1186 	addc	a,r4
      0017B5 FF               [12] 1187 	mov	r7,a
      0017B6 EA               [12] 1188 	mov	a,r2
      0017B7 24 5A            [12] 1189 	add	a,#_fontMatrix_8x16
      0017B9 F5 82            [12] 1190 	mov	dpl,a
      0017BB EF               [12] 1191 	mov	a,r7
      0017BC 34 48            [12] 1192 	addc	a,#(_fontMatrix_8x16 >> 8)
      0017BE F5 83            [12] 1193 	mov	dph,a
      0017C0 E4               [12] 1194 	clr	a
      0017C1 93               [24] 1195 	movc	a,@a+dptr
      0017C2 FF               [12] 1196 	mov	r7,a
      0017C3 C0 07            [24] 1197 	push	ar7
      0017C5 C0 06            [24] 1198 	push	ar6
      0017C7 C0 05            [24] 1199 	push	ar5
      0017C9 C0 04            [24] 1200 	push	ar4
      0017CB C0 03            [24] 1201 	push	ar3
      0017CD 74 01            [12] 1202 	mov	a,#0x01
      0017CF C0 E0            [24] 1203 	push	acc
      0017D1 8F 82            [24] 1204 	mov	dpl,r7
      0017D3 12 15 F9         [24] 1205 	lcall	_OLED_WR_Byte
      0017D6 15 81            [12] 1206 	dec	sp
      0017D8 D0 03            [24] 1207 	pop	ar3
      0017DA D0 04            [24] 1208 	pop	ar4
      0017DC D0 05            [24] 1209 	pop	ar5
      0017DE D0 06            [24] 1210 	pop	ar6
      0017E0 D0 07            [24] 1211 	pop	ar7
                                   1212 ;	source/CH549_OLED.c:179: for (i = 0; i < 8; i++)
      0017E2 0D               [12] 1213 	inc	r5
      0017E3 BD 08 00         [24] 1214 	cjne	r5,#0x08,00151$
      0017E6                       1215 00151$:
      0017E6 D0 07            [24] 1216 	pop	ar7
      0017E8 40 B0            [24] 1217 	jc	00109$
                                   1218 ;	source/CH549_OLED.c:181: OLED_Set_Pos(col_index, row_index + 1);
      0017EA E5 16            [12] 1219 	mov	a,_bp
      0017EC 24 FD            [12] 1220 	add	a,#0xfd
      0017EE F8               [12] 1221 	mov	r0,a
      0017EF 86 05            [24] 1222 	mov	ar5,@r0
      0017F1 0D               [12] 1223 	inc	r5
      0017F2 C0 04            [24] 1224 	push	ar4
      0017F4 C0 03            [24] 1225 	push	ar3
      0017F6 C0 05            [24] 1226 	push	ar5
      0017F8 8F 82            [24] 1227 	mov	dpl,r7
      0017FA 12 17 02         [24] 1228 	lcall	_OLED_Set_Pos
      0017FD 15 81            [12] 1229 	dec	sp
      0017FF D0 03            [24] 1230 	pop	ar3
      001801 D0 04            [24] 1231 	pop	ar4
                                   1232 ;	source/CH549_OLED.c:182: for (i = 0; i < 8; i++)
      001803 7F 00            [12] 1233 	mov	r7,#0x00
      001805                       1234 00111$:
                                   1235 ;	source/CH549_OLED.c:183: OLED_WR_Byte(fontMatrix_8x16[char_index * 16 + i + 8], OLED_DATA);
      001805 8F 02            [24] 1236 	mov	ar2,r7
      001807 7D 00            [12] 1237 	mov	r5,#0x00
      001809 EA               [12] 1238 	mov	a,r2
      00180A 2B               [12] 1239 	add	a,r3
      00180B FA               [12] 1240 	mov	r2,a
      00180C ED               [12] 1241 	mov	a,r5
      00180D 3C               [12] 1242 	addc	a,r4
      00180E FD               [12] 1243 	mov	r5,a
      00180F 74 08            [12] 1244 	mov	a,#0x08
      001811 2A               [12] 1245 	add	a,r2
      001812 FA               [12] 1246 	mov	r2,a
      001813 E4               [12] 1247 	clr	a
      001814 3D               [12] 1248 	addc	a,r5
      001815 FD               [12] 1249 	mov	r5,a
      001816 EA               [12] 1250 	mov	a,r2
      001817 24 5A            [12] 1251 	add	a,#_fontMatrix_8x16
      001819 F5 82            [12] 1252 	mov	dpl,a
      00181B ED               [12] 1253 	mov	a,r5
      00181C 34 48            [12] 1254 	addc	a,#(_fontMatrix_8x16 >> 8)
      00181E F5 83            [12] 1255 	mov	dph,a
      001820 E4               [12] 1256 	clr	a
      001821 93               [24] 1257 	movc	a,@a+dptr
      001822 FD               [12] 1258 	mov	r5,a
      001823 C0 07            [24] 1259 	push	ar7
      001825 C0 04            [24] 1260 	push	ar4
      001827 C0 03            [24] 1261 	push	ar3
      001829 74 01            [12] 1262 	mov	a,#0x01
      00182B C0 E0            [24] 1263 	push	acc
      00182D 8D 82            [24] 1264 	mov	dpl,r5
      00182F 12 15 F9         [24] 1265 	lcall	_OLED_WR_Byte
      001832 15 81            [12] 1266 	dec	sp
      001834 D0 03            [24] 1267 	pop	ar3
      001836 D0 04            [24] 1268 	pop	ar4
      001838 D0 07            [24] 1269 	pop	ar7
                                   1270 ;	source/CH549_OLED.c:182: for (i = 0; i < 8; i++)
      00183A 0F               [12] 1271 	inc	r7
      00183B BF 08 00         [24] 1272 	cjne	r7,#0x08,00153$
      00183E                       1273 00153$:
      00183E 40 C5            [24] 1274 	jc	00111$
      001840 80 4C            [24] 1275 	sjmp	00115$
      001842                       1276 00107$:
                                   1277 ;	source/CH549_OLED.c:187: OLED_Set_Pos(col_index, row_index + 1);
      001842 E5 16            [12] 1278 	mov	a,_bp
      001844 24 FD            [12] 1279 	add	a,#0xfd
      001846 F8               [12] 1280 	mov	r0,a
      001847 86 05            [24] 1281 	mov	ar5,@r0
      001849 0D               [12] 1282 	inc	r5
      00184A C0 06            [24] 1283 	push	ar6
      00184C C0 05            [24] 1284 	push	ar5
      00184E 8F 82            [24] 1285 	mov	dpl,r7
      001850 12 17 02         [24] 1286 	lcall	_OLED_Set_Pos
      001853 15 81            [12] 1287 	dec	sp
      001855 D0 06            [24] 1288 	pop	ar6
                                   1289 ;	source/CH549_OLED.c:188: for (i = 0; i < 6; i++)
      001857 EE               [12] 1290 	mov	a,r6
      001858 75 F0 06         [24] 1291 	mov	b,#0x06
      00185B A4               [48] 1292 	mul	ab
      00185C 24 32            [12] 1293 	add	a,#_fontMatrix_6x8
      00185E FE               [12] 1294 	mov	r6,a
      00185F 74 46            [12] 1295 	mov	a,#(_fontMatrix_6x8 >> 8)
      001861 35 F0            [12] 1296 	addc	a,b
      001863 FF               [12] 1297 	mov	r7,a
      001864 7D 00            [12] 1298 	mov	r5,#0x00
      001866                       1299 00113$:
                                   1300 ;	source/CH549_OLED.c:189: OLED_WR_Byte(fontMatrix_6x8[char_index][i], OLED_DATA);	
      001866 ED               [12] 1301 	mov	a,r5
      001867 2E               [12] 1302 	add	a,r6
      001868 F5 82            [12] 1303 	mov	dpl,a
      00186A E4               [12] 1304 	clr	a
      00186B 3F               [12] 1305 	addc	a,r7
      00186C F5 83            [12] 1306 	mov	dph,a
      00186E E4               [12] 1307 	clr	a
      00186F 93               [24] 1308 	movc	a,@a+dptr
      001870 FC               [12] 1309 	mov	r4,a
      001871 C0 07            [24] 1310 	push	ar7
      001873 C0 06            [24] 1311 	push	ar6
      001875 C0 05            [24] 1312 	push	ar5
      001877 74 01            [12] 1313 	mov	a,#0x01
      001879 C0 E0            [24] 1314 	push	acc
      00187B 8C 82            [24] 1315 	mov	dpl,r4
      00187D 12 15 F9         [24] 1316 	lcall	_OLED_WR_Byte
      001880 15 81            [12] 1317 	dec	sp
      001882 D0 05            [24] 1318 	pop	ar5
      001884 D0 06            [24] 1319 	pop	ar6
      001886 D0 07            [24] 1320 	pop	ar7
                                   1321 ;	source/CH549_OLED.c:188: for (i = 0; i < 6; i++)
      001888 0D               [12] 1322 	inc	r5
      001889 BD 06 00         [24] 1323 	cjne	r5,#0x06,00155$
      00188C                       1324 00155$:
      00188C 40 D8            [24] 1325 	jc	00113$
      00188E                       1326 00115$:
                                   1327 ;	source/CH549_OLED.c:191: }
      00188E D0 16            [24] 1328 	pop	_bp
      001890 22               [24] 1329 	ret
                                   1330 ;------------------------------------------------------------
                                   1331 ;Allocation info for local variables in function 'OLED_ShowString'
                                   1332 ;------------------------------------------------------------
                                   1333 ;row_index                 Allocated to stack - _bp -3
                                   1334 ;chr                       Allocated to stack - _bp -6
                                   1335 ;col_index                 Allocated to registers r7 
                                   1336 ;j                         Allocated to registers r6 
                                   1337 ;------------------------------------------------------------
                                   1338 ;	source/CH549_OLED.c:193: void OLED_ShowString(u8 col_index, u8 row_index, u8 *chr)
                                   1339 ;	-----------------------------------------
                                   1340 ;	 function OLED_ShowString
                                   1341 ;	-----------------------------------------
      001891                       1342 _OLED_ShowString:
      001891 C0 16            [24] 1343 	push	_bp
      001893 85 81 16         [24] 1344 	mov	_bp,sp
      001896 AF 82            [24] 1345 	mov	r7,dpl
                                   1346 ;	source/CH549_OLED.c:196: while (chr[j]!='\0')
      001898 7E 00            [12] 1347 	mov	r6,#0x00
      00189A                       1348 00103$:
      00189A E5 16            [12] 1349 	mov	a,_bp
      00189C 24 FA            [12] 1350 	add	a,#0xfa
      00189E F8               [12] 1351 	mov	r0,a
      00189F EE               [12] 1352 	mov	a,r6
      0018A0 26               [12] 1353 	add	a,@r0
      0018A1 FB               [12] 1354 	mov	r3,a
      0018A2 E4               [12] 1355 	clr	a
      0018A3 08               [12] 1356 	inc	r0
      0018A4 36               [12] 1357 	addc	a,@r0
      0018A5 FC               [12] 1358 	mov	r4,a
      0018A6 08               [12] 1359 	inc	r0
      0018A7 86 05            [24] 1360 	mov	ar5,@r0
      0018A9 8B 82            [24] 1361 	mov	dpl,r3
      0018AB 8C 83            [24] 1362 	mov	dph,r4
      0018AD 8D F0            [24] 1363 	mov	b,r5
      0018AF 12 22 F7         [24] 1364 	lcall	__gptrget
      0018B2 FD               [12] 1365 	mov	r5,a
      0018B3 60 3A            [24] 1366 	jz	00106$
                                   1367 ;	source/CH549_OLED.c:197: {		OLED_ShowChar(col_index,row_index,chr[j]);
      0018B5 C0 07            [24] 1368 	push	ar7
      0018B7 C0 06            [24] 1369 	push	ar6
      0018B9 C0 05            [24] 1370 	push	ar5
      0018BB E5 16            [12] 1371 	mov	a,_bp
      0018BD 24 FD            [12] 1372 	add	a,#0xfd
      0018BF F8               [12] 1373 	mov	r0,a
      0018C0 E6               [12] 1374 	mov	a,@r0
      0018C1 C0 E0            [24] 1375 	push	acc
      0018C3 8F 82            [24] 1376 	mov	dpl,r7
      0018C5 12 17 4D         [24] 1377 	lcall	_OLED_ShowChar
      0018C8 15 81            [12] 1378 	dec	sp
      0018CA 15 81            [12] 1379 	dec	sp
      0018CC D0 06            [24] 1380 	pop	ar6
      0018CE D0 07            [24] 1381 	pop	ar7
                                   1382 ;	source/CH549_OLED.c:198: col_index+=8;
      0018D0 8F 05            [24] 1383 	mov	ar5,r7
      0018D2 74 08            [12] 1384 	mov	a,#0x08
      0018D4 2D               [12] 1385 	add	a,r5
                                   1386 ;	source/CH549_OLED.c:199: if (col_index>120){col_index=0;row_index+=2;}
      0018D5 FF               [12] 1387 	mov  r7,a
      0018D6 24 87            [12] 1388 	add	a,#0xff - 0x78
      0018D8 50 12            [24] 1389 	jnc	00102$
      0018DA 7F 00            [12] 1390 	mov	r7,#0x00
      0018DC E5 16            [12] 1391 	mov	a,_bp
      0018DE 24 FD            [12] 1392 	add	a,#0xfd
      0018E0 F8               [12] 1393 	mov	r0,a
      0018E1 86 05            [24] 1394 	mov	ar5,@r0
      0018E3 E5 16            [12] 1395 	mov	a,_bp
      0018E5 24 FD            [12] 1396 	add	a,#0xfd
      0018E7 F8               [12] 1397 	mov	r0,a
      0018E8 74 02            [12] 1398 	mov	a,#0x02
      0018EA 2D               [12] 1399 	add	a,r5
      0018EB F6               [12] 1400 	mov	@r0,a
      0018EC                       1401 00102$:
                                   1402 ;	source/CH549_OLED.c:200: j++;
      0018EC 0E               [12] 1403 	inc	r6
      0018ED 80 AB            [24] 1404 	sjmp	00103$
      0018EF                       1405 00106$:
                                   1406 ;	source/CH549_OLED.c:202: }
      0018EF D0 16            [24] 1407 	pop	_bp
      0018F1 22               [24] 1408 	ret
                                   1409 ;------------------------------------------------------------
                                   1410 ;Allocation info for local variables in function 'OLED_ShowCHinese'
                                   1411 ;------------------------------------------------------------
                                   1412 ;row_index                 Allocated to stack - _bp -3
                                   1413 ;no                        Allocated to stack - _bp -4
                                   1414 ;col_index                 Allocated to registers r7 
                                   1415 ;t                         Allocated to registers r5 
                                   1416 ;adder                     Allocated to registers r6 
                                   1417 ;------------------------------------------------------------
                                   1418 ;	source/CH549_OLED.c:206: void OLED_ShowCHinese(u8 col_index, u8 row_index, u8 no)
                                   1419 ;	-----------------------------------------
                                   1420 ;	 function OLED_ShowCHinese
                                   1421 ;	-----------------------------------------
      0018F2                       1422 _OLED_ShowCHinese:
      0018F2 C0 16            [24] 1423 	push	_bp
      0018F4 85 81 16         [24] 1424 	mov	_bp,sp
      0018F7 AF 82            [24] 1425 	mov	r7,dpl
                                   1426 ;	source/CH549_OLED.c:208: u8 t,adder=0;
      0018F9 7E 00            [12] 1427 	mov	r6,#0x00
                                   1428 ;	source/CH549_OLED.c:209: OLED_Set_Pos(col_index,row_index);	
      0018FB C0 07            [24] 1429 	push	ar7
      0018FD C0 06            [24] 1430 	push	ar6
      0018FF E5 16            [12] 1431 	mov	a,_bp
      001901 24 FD            [12] 1432 	add	a,#0xfd
      001903 F8               [12] 1433 	mov	r0,a
      001904 E6               [12] 1434 	mov	a,@r0
      001905 C0 E0            [24] 1435 	push	acc
      001907 8F 82            [24] 1436 	mov	dpl,r7
      001909 12 17 02         [24] 1437 	lcall	_OLED_Set_Pos
      00190C 15 81            [12] 1438 	dec	sp
      00190E D0 06            [24] 1439 	pop	ar6
      001910 D0 07            [24] 1440 	pop	ar7
                                   1441 ;	source/CH549_OLED.c:210: for(t=0;t<16;t++)
      001912 7D 00            [12] 1442 	mov	r5,#0x00
      001914                       1443 00103$:
                                   1444 ;	source/CH549_OLED.c:212: OLED_WR_Byte(Hzk[2*no][t],OLED_DATA);
      001914 C0 07            [24] 1445 	push	ar7
      001916 E5 16            [12] 1446 	mov	a,_bp
      001918 24 FC            [12] 1447 	add	a,#0xfc
      00191A F8               [12] 1448 	mov	r0,a
      00191B 86 03            [24] 1449 	mov	ar3,@r0
      00191D 7C 00            [12] 1450 	mov	r4,#0x00
      00191F EB               [12] 1451 	mov	a,r3
      001920 2B               [12] 1452 	add	a,r3
      001921 FB               [12] 1453 	mov	r3,a
      001922 EC               [12] 1454 	mov	a,r4
      001923 33               [12] 1455 	rlc	a
      001924 FC               [12] 1456 	mov	r4,a
      001925 8B 02            [24] 1457 	mov	ar2,r3
      001927 C4               [12] 1458 	swap	a
      001928 23               [12] 1459 	rl	a
      001929 54 E0            [12] 1460 	anl	a,#0xe0
      00192B CA               [12] 1461 	xch	a,r2
      00192C C4               [12] 1462 	swap	a
      00192D 23               [12] 1463 	rl	a
      00192E CA               [12] 1464 	xch	a,r2
      00192F 6A               [12] 1465 	xrl	a,r2
      001930 CA               [12] 1466 	xch	a,r2
      001931 54 E0            [12] 1467 	anl	a,#0xe0
      001933 CA               [12] 1468 	xch	a,r2
      001934 6A               [12] 1469 	xrl	a,r2
      001935 FF               [12] 1470 	mov	r7,a
      001936 EA               [12] 1471 	mov	a,r2
      001937 24 4A            [12] 1472 	add	a,#_Hzk
      001939 FA               [12] 1473 	mov	r2,a
      00193A EF               [12] 1474 	mov	a,r7
      00193B 34 4E            [12] 1475 	addc	a,#(_Hzk >> 8)
      00193D FF               [12] 1476 	mov	r7,a
      00193E ED               [12] 1477 	mov	a,r5
      00193F 2A               [12] 1478 	add	a,r2
      001940 F5 82            [12] 1479 	mov	dpl,a
      001942 E4               [12] 1480 	clr	a
      001943 3F               [12] 1481 	addc	a,r7
      001944 F5 83            [12] 1482 	mov	dph,a
      001946 E4               [12] 1483 	clr	a
      001947 93               [24] 1484 	movc	a,@a+dptr
      001948 FF               [12] 1485 	mov	r7,a
      001949 C0 07            [24] 1486 	push	ar7
      00194B C0 06            [24] 1487 	push	ar6
      00194D C0 05            [24] 1488 	push	ar5
      00194F C0 04            [24] 1489 	push	ar4
      001951 C0 03            [24] 1490 	push	ar3
      001953 74 01            [12] 1491 	mov	a,#0x01
      001955 C0 E0            [24] 1492 	push	acc
      001957 8F 82            [24] 1493 	mov	dpl,r7
      001959 12 15 F9         [24] 1494 	lcall	_OLED_WR_Byte
      00195C 15 81            [12] 1495 	dec	sp
      00195E D0 03            [24] 1496 	pop	ar3
      001960 D0 04            [24] 1497 	pop	ar4
      001962 D0 05            [24] 1498 	pop	ar5
      001964 D0 06            [24] 1499 	pop	ar6
      001966 D0 07            [24] 1500 	pop	ar7
                                   1501 ;	source/CH549_OLED.c:213: adder+=1;
      001968 8E 07            [24] 1502 	mov	ar7,r6
      00196A EF               [12] 1503 	mov	a,r7
      00196B 04               [12] 1504 	inc	a
      00196C FE               [12] 1505 	mov	r6,a
                                   1506 ;	source/CH549_OLED.c:210: for(t=0;t<16;t++)
      00196D 0D               [12] 1507 	inc	r5
      00196E BD 10 00         [24] 1508 	cjne	r5,#0x10,00123$
      001971                       1509 00123$:
      001971 D0 07            [24] 1510 	pop	ar7
      001973 40 9F            [24] 1511 	jc	00103$
                                   1512 ;	source/CH549_OLED.c:215: OLED_Set_Pos(col_index,row_index+1);	
      001975 E5 16            [12] 1513 	mov	a,_bp
      001977 24 FD            [12] 1514 	add	a,#0xfd
      001979 F8               [12] 1515 	mov	r0,a
      00197A 86 05            [24] 1516 	mov	ar5,@r0
      00197C 0D               [12] 1517 	inc	r5
      00197D C0 06            [24] 1518 	push	ar6
      00197F C0 04            [24] 1519 	push	ar4
      001981 C0 03            [24] 1520 	push	ar3
      001983 C0 05            [24] 1521 	push	ar5
      001985 8F 82            [24] 1522 	mov	dpl,r7
      001987 12 17 02         [24] 1523 	lcall	_OLED_Set_Pos
      00198A 15 81            [12] 1524 	dec	sp
      00198C D0 03            [24] 1525 	pop	ar3
      00198E D0 04            [24] 1526 	pop	ar4
      001990 D0 06            [24] 1527 	pop	ar6
                                   1528 ;	source/CH549_OLED.c:216: for(t=0;t<16;t++)
      001992 0B               [12] 1529 	inc	r3
      001993 BB 00 01         [24] 1530 	cjne	r3,#0x00,00125$
      001996 0C               [12] 1531 	inc	r4
      001997                       1532 00125$:
      001997 EC               [12] 1533 	mov	a,r4
      001998 C4               [12] 1534 	swap	a
      001999 23               [12] 1535 	rl	a
      00199A 54 E0            [12] 1536 	anl	a,#0xe0
      00199C CB               [12] 1537 	xch	a,r3
      00199D C4               [12] 1538 	swap	a
      00199E 23               [12] 1539 	rl	a
      00199F CB               [12] 1540 	xch	a,r3
      0019A0 6B               [12] 1541 	xrl	a,r3
      0019A1 CB               [12] 1542 	xch	a,r3
      0019A2 54 E0            [12] 1543 	anl	a,#0xe0
      0019A4 CB               [12] 1544 	xch	a,r3
      0019A5 6B               [12] 1545 	xrl	a,r3
      0019A6 FC               [12] 1546 	mov	r4,a
      0019A7 EB               [12] 1547 	mov	a,r3
      0019A8 24 4A            [12] 1548 	add	a,#_Hzk
      0019AA FD               [12] 1549 	mov	r5,a
      0019AB EC               [12] 1550 	mov	a,r4
      0019AC 34 4E            [12] 1551 	addc	a,#(_Hzk >> 8)
      0019AE FF               [12] 1552 	mov	r7,a
      0019AF 7C 00            [12] 1553 	mov	r4,#0x00
      0019B1                       1554 00105$:
                                   1555 ;	source/CH549_OLED.c:218: OLED_WR_Byte(Hzk[2*no+1][t],OLED_DATA);
      0019B1 EC               [12] 1556 	mov	a,r4
      0019B2 2D               [12] 1557 	add	a,r5
      0019B3 F5 82            [12] 1558 	mov	dpl,a
      0019B5 E4               [12] 1559 	clr	a
      0019B6 3F               [12] 1560 	addc	a,r7
      0019B7 F5 83            [12] 1561 	mov	dph,a
      0019B9 E4               [12] 1562 	clr	a
      0019BA 93               [24] 1563 	movc	a,@a+dptr
      0019BB FB               [12] 1564 	mov	r3,a
      0019BC C0 07            [24] 1565 	push	ar7
      0019BE C0 06            [24] 1566 	push	ar6
      0019C0 C0 05            [24] 1567 	push	ar5
      0019C2 C0 04            [24] 1568 	push	ar4
      0019C4 74 01            [12] 1569 	mov	a,#0x01
      0019C6 C0 E0            [24] 1570 	push	acc
      0019C8 8B 82            [24] 1571 	mov	dpl,r3
      0019CA 12 15 F9         [24] 1572 	lcall	_OLED_WR_Byte
      0019CD 15 81            [12] 1573 	dec	sp
      0019CF D0 04            [24] 1574 	pop	ar4
      0019D1 D0 05            [24] 1575 	pop	ar5
      0019D3 D0 06            [24] 1576 	pop	ar6
      0019D5 D0 07            [24] 1577 	pop	ar7
                                   1578 ;	source/CH549_OLED.c:219: adder+=1;
      0019D7 8E 03            [24] 1579 	mov	ar3,r6
      0019D9 EB               [12] 1580 	mov	a,r3
      0019DA 04               [12] 1581 	inc	a
      0019DB FE               [12] 1582 	mov	r6,a
                                   1583 ;	source/CH549_OLED.c:216: for(t=0;t<16;t++)
      0019DC 0C               [12] 1584 	inc	r4
      0019DD BC 10 00         [24] 1585 	cjne	r4,#0x10,00126$
      0019E0                       1586 00126$:
      0019E0 40 CF            [24] 1587 	jc	00105$
                                   1588 ;	source/CH549_OLED.c:221: }
      0019E2 D0 16            [24] 1589 	pop	_bp
      0019E4 22               [24] 1590 	ret
                                   1591 ;------------------------------------------------------------
                                   1592 ;Allocation info for local variables in function 'OLED_DrawBMP'
                                   1593 ;------------------------------------------------------------
                                   1594 ;y0                        Allocated to stack - _bp -3
                                   1595 ;x1                        Allocated to stack - _bp -4
                                   1596 ;y1                        Allocated to stack - _bp -5
                                   1597 ;BMP                       Allocated to stack - _bp -8
                                   1598 ;x0                        Allocated to registers r2 
                                   1599 ;j                         Allocated to registers r5 r6 
                                   1600 ;x                         Allocated to registers r6 
                                   1601 ;y                         Allocated to registers 
                                   1602 ;sloc0                     Allocated to stack - _bp +1
                                   1603 ;------------------------------------------------------------
                                   1604 ;	source/CH549_OLED.c:227: void OLED_DrawBMP(unsigned char x0, unsigned char y0,unsigned char x1, unsigned char y1,unsigned char BMP[])
                                   1605 ;	-----------------------------------------
                                   1606 ;	 function OLED_DrawBMP
                                   1607 ;	-----------------------------------------
      0019E5                       1608 _OLED_DrawBMP:
      0019E5 C0 16            [24] 1609 	push	_bp
      0019E7 85 81 16         [24] 1610 	mov	_bp,sp
      0019EA 05 81            [12] 1611 	inc	sp
      0019EC 05 81            [12] 1612 	inc	sp
      0019EE AA 82            [24] 1613 	mov	r2,dpl
                                   1614 ;	source/CH549_OLED.c:231: unsigned int j = 0;
                                   1615 ;	source/CH549_OLED.c:234: for(y = y0; y < y1; y++)	//这里的y和x是对二维数组行和列的两次for循环，遍历整个二维数组，并把每一位数据传给OLED
      0019F0 E4               [12] 1616 	clr	a
      0019F1 FD               [12] 1617 	mov	r5,a
      0019F2 FE               [12] 1618 	mov	r6,a
      0019F3 E5 16            [12] 1619 	mov	a,_bp
      0019F5 24 FD            [12] 1620 	add	a,#0xfd
      0019F7 F8               [12] 1621 	mov	r0,a
      0019F8 86 04            [24] 1622 	mov	ar4,@r0
      0019FA                       1623 00107$:
      0019FA E5 16            [12] 1624 	mov	a,_bp
      0019FC 24 FB            [12] 1625 	add	a,#0xfb
      0019FE F8               [12] 1626 	mov	r0,a
      0019FF C3               [12] 1627 	clr	c
      001A00 EC               [12] 1628 	mov	a,r4
      001A01 96               [12] 1629 	subb	a,@r0
      001A02 40 03            [24] 1630 	jc	00129$
      001A04 02 1A 84         [24] 1631 	ljmp	00109$
      001A07                       1632 00129$:
                                   1633 ;	source/CH549_OLED.c:237: OLED_Set_Pos(x0,y);
      001A07 C0 06            [24] 1634 	push	ar6
      001A09 C0 05            [24] 1635 	push	ar5
      001A0B C0 04            [24] 1636 	push	ar4
      001A0D C0 02            [24] 1637 	push	ar2
      001A0F C0 04            [24] 1638 	push	ar4
      001A11 8A 82            [24] 1639 	mov	dpl,r2
      001A13 12 17 02         [24] 1640 	lcall	_OLED_Set_Pos
      001A16 15 81            [12] 1641 	dec	sp
      001A18 D0 02            [24] 1642 	pop	ar2
      001A1A D0 04            [24] 1643 	pop	ar4
      001A1C D0 05            [24] 1644 	pop	ar5
      001A1E D0 06            [24] 1645 	pop	ar6
                                   1646 ;	source/CH549_OLED.c:238: for(x = x0; x < x1; x++)
      001A20 A8 16            [24] 1647 	mov	r0,_bp
      001A22 08               [12] 1648 	inc	r0
      001A23 A6 05            [24] 1649 	mov	@r0,ar5
      001A25 08               [12] 1650 	inc	r0
      001A26 A6 06            [24] 1651 	mov	@r0,ar6
      001A28 8A 06            [24] 1652 	mov	ar6,r2
      001A2A                       1653 00104$:
      001A2A E5 16            [12] 1654 	mov	a,_bp
      001A2C 24 FC            [12] 1655 	add	a,#0xfc
      001A2E F8               [12] 1656 	mov	r0,a
      001A2F C3               [12] 1657 	clr	c
      001A30 EE               [12] 1658 	mov	a,r6
      001A31 96               [12] 1659 	subb	a,@r0
      001A32 50 44            [24] 1660 	jnc	00115$
                                   1661 ;	source/CH549_OLED.c:240: OLED_WR_Byte(BMP[j++],OLED_DATA);	    	//向OLED输入BMP中 的一位数据，并逐次递增
      001A34 C0 04            [24] 1662 	push	ar4
      001A36 E5 16            [12] 1663 	mov	a,_bp
      001A38 24 F8            [12] 1664 	add	a,#0xf8
      001A3A F8               [12] 1665 	mov	r0,a
      001A3B A9 16            [24] 1666 	mov	r1,_bp
      001A3D 09               [12] 1667 	inc	r1
      001A3E E7               [12] 1668 	mov	a,@r1
      001A3F 26               [12] 1669 	add	a,@r0
      001A40 FB               [12] 1670 	mov	r3,a
      001A41 09               [12] 1671 	inc	r1
      001A42 E7               [12] 1672 	mov	a,@r1
      001A43 08               [12] 1673 	inc	r0
      001A44 36               [12] 1674 	addc	a,@r0
      001A45 FC               [12] 1675 	mov	r4,a
      001A46 08               [12] 1676 	inc	r0
      001A47 86 07            [24] 1677 	mov	ar7,@r0
      001A49 A8 16            [24] 1678 	mov	r0,_bp
      001A4B 08               [12] 1679 	inc	r0
      001A4C 06               [12] 1680 	inc	@r0
      001A4D B6 00 02         [24] 1681 	cjne	@r0,#0x00,00131$
      001A50 08               [12] 1682 	inc	r0
      001A51 06               [12] 1683 	inc	@r0
      001A52                       1684 00131$:
      001A52 8B 82            [24] 1685 	mov	dpl,r3
      001A54 8C 83            [24] 1686 	mov	dph,r4
      001A56 8F F0            [24] 1687 	mov	b,r7
      001A58 12 22 F7         [24] 1688 	lcall	__gptrget
      001A5B FB               [12] 1689 	mov	r3,a
      001A5C C0 06            [24] 1690 	push	ar6
      001A5E C0 04            [24] 1691 	push	ar4
      001A60 C0 02            [24] 1692 	push	ar2
      001A62 74 01            [12] 1693 	mov	a,#0x01
      001A64 C0 E0            [24] 1694 	push	acc
      001A66 8B 82            [24] 1695 	mov	dpl,r3
      001A68 12 15 F9         [24] 1696 	lcall	_OLED_WR_Byte
      001A6B 15 81            [12] 1697 	dec	sp
      001A6D D0 02            [24] 1698 	pop	ar2
      001A6F D0 04            [24] 1699 	pop	ar4
      001A71 D0 06            [24] 1700 	pop	ar6
                                   1701 ;	source/CH549_OLED.c:238: for(x = x0; x < x1; x++)
      001A73 0E               [12] 1702 	inc	r6
      001A74 D0 04            [24] 1703 	pop	ar4
      001A76 80 B2            [24] 1704 	sjmp	00104$
      001A78                       1705 00115$:
      001A78 A8 16            [24] 1706 	mov	r0,_bp
      001A7A 08               [12] 1707 	inc	r0
      001A7B 86 05            [24] 1708 	mov	ar5,@r0
      001A7D 08               [12] 1709 	inc	r0
      001A7E 86 06            [24] 1710 	mov	ar6,@r0
                                   1711 ;	source/CH549_OLED.c:234: for(y = y0; y < y1; y++)	//这里的y和x是对二维数组行和列的两次for循环，遍历整个二维数组，并把每一位数据传给OLED
      001A80 0C               [12] 1712 	inc	r4
      001A81 02 19 FA         [24] 1713 	ljmp	00107$
      001A84                       1714 00109$:
                                   1715 ;	source/CH549_OLED.c:243: } 
      001A84 85 16 81         [24] 1716 	mov	sp,_bp
      001A87 D0 16            [24] 1717 	pop	_bp
      001A89 22               [24] 1718 	ret
                                   1719 	.area CSEG    (CODE)
                                   1720 	.area CONST   (CODE)
      003E32                       1721 _BMP1:
      003E32 00                    1722 	.db #0x00	; 0
      003E33 03                    1723 	.db #0x03	; 3
      003E34 05                    1724 	.db #0x05	; 5
      003E35 09                    1725 	.db #0x09	; 9
      003E36 11                    1726 	.db #0x11	; 17
      003E37 FF                    1727 	.db #0xff	; 255
      003E38 11                    1728 	.db #0x11	; 17
      003E39 89                    1729 	.db #0x89	; 137
      003E3A 05                    1730 	.db #0x05	; 5
      003E3B C3                    1731 	.db #0xc3	; 195
      003E3C 00                    1732 	.db #0x00	; 0
      003E3D E0                    1733 	.db #0xe0	; 224
      003E3E 00                    1734 	.db #0x00	; 0
      003E3F F0                    1735 	.db #0xf0	; 240
      003E40 00                    1736 	.db #0x00	; 0
      003E41 F8                    1737 	.db #0xf8	; 248
      003E42 00                    1738 	.db #0x00	; 0
      003E43 00                    1739 	.db #0x00	; 0
      003E44 00                    1740 	.db #0x00	; 0
      003E45 00                    1741 	.db #0x00	; 0
      003E46 00                    1742 	.db #0x00	; 0
      003E47 00                    1743 	.db #0x00	; 0
      003E48 00                    1744 	.db #0x00	; 0
      003E49 44                    1745 	.db #0x44	; 68	'D'
      003E4A 28                    1746 	.db #0x28	; 40
      003E4B FF                    1747 	.db #0xff	; 255
      003E4C 11                    1748 	.db #0x11	; 17
      003E4D AA                    1749 	.db #0xaa	; 170
      003E4E 44                    1750 	.db #0x44	; 68	'D'
      003E4F 00                    1751 	.db #0x00	; 0
      003E50 00                    1752 	.db #0x00	; 0
      003E51 00                    1753 	.db #0x00	; 0
      003E52 00                    1754 	.db #0x00	; 0
      003E53 00                    1755 	.db #0x00	; 0
      003E54 00                    1756 	.db #0x00	; 0
      003E55 00                    1757 	.db #0x00	; 0
      003E56 00                    1758 	.db #0x00	; 0
      003E57 00                    1759 	.db #0x00	; 0
      003E58 00                    1760 	.db #0x00	; 0
      003E59 00                    1761 	.db #0x00	; 0
      003E5A 00                    1762 	.db #0x00	; 0
      003E5B 00                    1763 	.db #0x00	; 0
      003E5C 00                    1764 	.db #0x00	; 0
      003E5D 00                    1765 	.db #0x00	; 0
      003E5E 00                    1766 	.db #0x00	; 0
      003E5F 00                    1767 	.db #0x00	; 0
      003E60 00                    1768 	.db #0x00	; 0
      003E61 00                    1769 	.db #0x00	; 0
      003E62 00                    1770 	.db #0x00	; 0
      003E63 00                    1771 	.db #0x00	; 0
      003E64 00                    1772 	.db #0x00	; 0
      003E65 00                    1773 	.db #0x00	; 0
      003E66 00                    1774 	.db #0x00	; 0
      003E67 00                    1775 	.db #0x00	; 0
      003E68 00                    1776 	.db #0x00	; 0
      003E69 00                    1777 	.db #0x00	; 0
      003E6A 00                    1778 	.db #0x00	; 0
      003E6B 00                    1779 	.db #0x00	; 0
      003E6C 00                    1780 	.db #0x00	; 0
      003E6D 00                    1781 	.db #0x00	; 0
      003E6E 00                    1782 	.db #0x00	; 0
      003E6F 00                    1783 	.db #0x00	; 0
      003E70 00                    1784 	.db #0x00	; 0
      003E71 00                    1785 	.db #0x00	; 0
      003E72 00                    1786 	.db #0x00	; 0
      003E73 00                    1787 	.db #0x00	; 0
      003E74 00                    1788 	.db #0x00	; 0
      003E75 00                    1789 	.db #0x00	; 0
      003E76 00                    1790 	.db #0x00	; 0
      003E77 00                    1791 	.db #0x00	; 0
      003E78 00                    1792 	.db #0x00	; 0
      003E79 00                    1793 	.db #0x00	; 0
      003E7A 00                    1794 	.db #0x00	; 0
      003E7B 00                    1795 	.db #0x00	; 0
      003E7C 00                    1796 	.db #0x00	; 0
      003E7D 00                    1797 	.db #0x00	; 0
      003E7E 00                    1798 	.db #0x00	; 0
      003E7F 00                    1799 	.db #0x00	; 0
      003E80 00                    1800 	.db #0x00	; 0
      003E81 00                    1801 	.db #0x00	; 0
      003E82 00                    1802 	.db #0x00	; 0
      003E83 00                    1803 	.db #0x00	; 0
      003E84 00                    1804 	.db #0x00	; 0
      003E85 00                    1805 	.db #0x00	; 0
      003E86 00                    1806 	.db #0x00	; 0
      003E87 00                    1807 	.db #0x00	; 0
      003E88 00                    1808 	.db #0x00	; 0
      003E89 00                    1809 	.db #0x00	; 0
      003E8A 00                    1810 	.db #0x00	; 0
      003E8B 00                    1811 	.db #0x00	; 0
      003E8C 83                    1812 	.db #0x83	; 131
      003E8D 01                    1813 	.db #0x01	; 1
      003E8E 38                    1814 	.db #0x38	; 56	'8'
      003E8F 44                    1815 	.db #0x44	; 68	'D'
      003E90 82                    1816 	.db #0x82	; 130
      003E91 92                    1817 	.db #0x92	; 146
      003E92 92                    1818 	.db #0x92	; 146
      003E93 74                    1819 	.db #0x74	; 116	't'
      003E94 01                    1820 	.db #0x01	; 1
      003E95 83                    1821 	.db #0x83	; 131
      003E96 00                    1822 	.db #0x00	; 0
      003E97 00                    1823 	.db #0x00	; 0
      003E98 00                    1824 	.db #0x00	; 0
      003E99 00                    1825 	.db #0x00	; 0
      003E9A 00                    1826 	.db #0x00	; 0
      003E9B 00                    1827 	.db #0x00	; 0
      003E9C 00                    1828 	.db #0x00	; 0
      003E9D 7C                    1829 	.db #0x7c	; 124
      003E9E 44                    1830 	.db #0x44	; 68	'D'
      003E9F FF                    1831 	.db #0xff	; 255
      003EA0 01                    1832 	.db #0x01	; 1
      003EA1 7D                    1833 	.db #0x7d	; 125
      003EA2 7D                    1834 	.db #0x7d	; 125
      003EA3 7D                    1835 	.db #0x7d	; 125
      003EA4 01                    1836 	.db #0x01	; 1
      003EA5 7D                    1837 	.db #0x7d	; 125
      003EA6 7D                    1838 	.db #0x7d	; 125
      003EA7 7D                    1839 	.db #0x7d	; 125
      003EA8 7D                    1840 	.db #0x7d	; 125
      003EA9 01                    1841 	.db #0x01	; 1
      003EAA 7D                    1842 	.db #0x7d	; 125
      003EAB 7D                    1843 	.db #0x7d	; 125
      003EAC 7D                    1844 	.db #0x7d	; 125
      003EAD 7D                    1845 	.db #0x7d	; 125
      003EAE 7D                    1846 	.db #0x7d	; 125
      003EAF 01                    1847 	.db #0x01	; 1
      003EB0 FF                    1848 	.db #0xff	; 255
      003EB1 00                    1849 	.db #0x00	; 0
      003EB2 00                    1850 	.db #0x00	; 0
      003EB3 00                    1851 	.db #0x00	; 0
      003EB4 00                    1852 	.db #0x00	; 0
      003EB5 00                    1853 	.db #0x00	; 0
      003EB6 00                    1854 	.db #0x00	; 0
      003EB7 01                    1855 	.db #0x01	; 1
      003EB8 00                    1856 	.db #0x00	; 0
      003EB9 01                    1857 	.db #0x01	; 1
      003EBA 00                    1858 	.db #0x00	; 0
      003EBB 01                    1859 	.db #0x01	; 1
      003EBC 00                    1860 	.db #0x00	; 0
      003EBD 01                    1861 	.db #0x01	; 1
      003EBE 00                    1862 	.db #0x00	; 0
      003EBF 01                    1863 	.db #0x01	; 1
      003EC0 00                    1864 	.db #0x00	; 0
      003EC1 01                    1865 	.db #0x01	; 1
      003EC2 00                    1866 	.db #0x00	; 0
      003EC3 00                    1867 	.db #0x00	; 0
      003EC4 00                    1868 	.db #0x00	; 0
      003EC5 00                    1869 	.db #0x00	; 0
      003EC6 00                    1870 	.db #0x00	; 0
      003EC7 00                    1871 	.db #0x00	; 0
      003EC8 00                    1872 	.db #0x00	; 0
      003EC9 00                    1873 	.db #0x00	; 0
      003ECA 00                    1874 	.db #0x00	; 0
      003ECB 01                    1875 	.db #0x01	; 1
      003ECC 01                    1876 	.db #0x01	; 1
      003ECD 00                    1877 	.db #0x00	; 0
      003ECE 00                    1878 	.db #0x00	; 0
      003ECF 00                    1879 	.db #0x00	; 0
      003ED0 00                    1880 	.db #0x00	; 0
      003ED1 00                    1881 	.db #0x00	; 0
      003ED2 00                    1882 	.db #0x00	; 0
      003ED3 00                    1883 	.db #0x00	; 0
      003ED4 00                    1884 	.db #0x00	; 0
      003ED5 00                    1885 	.db #0x00	; 0
      003ED6 00                    1886 	.db #0x00	; 0
      003ED7 00                    1887 	.db #0x00	; 0
      003ED8 00                    1888 	.db #0x00	; 0
      003ED9 00                    1889 	.db #0x00	; 0
      003EDA 00                    1890 	.db #0x00	; 0
      003EDB 00                    1891 	.db #0x00	; 0
      003EDC 00                    1892 	.db #0x00	; 0
      003EDD 00                    1893 	.db #0x00	; 0
      003EDE 00                    1894 	.db #0x00	; 0
      003EDF 00                    1895 	.db #0x00	; 0
      003EE0 00                    1896 	.db #0x00	; 0
      003EE1 00                    1897 	.db #0x00	; 0
      003EE2 00                    1898 	.db #0x00	; 0
      003EE3 00                    1899 	.db #0x00	; 0
      003EE4 00                    1900 	.db #0x00	; 0
      003EE5 00                    1901 	.db #0x00	; 0
      003EE6 00                    1902 	.db #0x00	; 0
      003EE7 00                    1903 	.db #0x00	; 0
      003EE8 00                    1904 	.db #0x00	; 0
      003EE9 00                    1905 	.db #0x00	; 0
      003EEA 00                    1906 	.db #0x00	; 0
      003EEB 00                    1907 	.db #0x00	; 0
      003EEC 00                    1908 	.db #0x00	; 0
      003EED 00                    1909 	.db #0x00	; 0
      003EEE 00                    1910 	.db #0x00	; 0
      003EEF 00                    1911 	.db #0x00	; 0
      003EF0 00                    1912 	.db #0x00	; 0
      003EF1 00                    1913 	.db #0x00	; 0
      003EF2 00                    1914 	.db #0x00	; 0
      003EF3 00                    1915 	.db #0x00	; 0
      003EF4 00                    1916 	.db #0x00	; 0
      003EF5 00                    1917 	.db #0x00	; 0
      003EF6 00                    1918 	.db #0x00	; 0
      003EF7 00                    1919 	.db #0x00	; 0
      003EF8 00                    1920 	.db #0x00	; 0
      003EF9 00                    1921 	.db #0x00	; 0
      003EFA 00                    1922 	.db #0x00	; 0
      003EFB 00                    1923 	.db #0x00	; 0
      003EFC 00                    1924 	.db #0x00	; 0
      003EFD 00                    1925 	.db #0x00	; 0
      003EFE 00                    1926 	.db #0x00	; 0
      003EFF 00                    1927 	.db #0x00	; 0
      003F00 00                    1928 	.db #0x00	; 0
      003F01 00                    1929 	.db #0x00	; 0
      003F02 00                    1930 	.db #0x00	; 0
      003F03 00                    1931 	.db #0x00	; 0
      003F04 00                    1932 	.db #0x00	; 0
      003F05 00                    1933 	.db #0x00	; 0
      003F06 00                    1934 	.db #0x00	; 0
      003F07 00                    1935 	.db #0x00	; 0
      003F08 00                    1936 	.db #0x00	; 0
      003F09 00                    1937 	.db #0x00	; 0
      003F0A 00                    1938 	.db #0x00	; 0
      003F0B 00                    1939 	.db #0x00	; 0
      003F0C 01                    1940 	.db #0x01	; 1
      003F0D 01                    1941 	.db #0x01	; 1
      003F0E 00                    1942 	.db #0x00	; 0
      003F0F 00                    1943 	.db #0x00	; 0
      003F10 00                    1944 	.db #0x00	; 0
      003F11 00                    1945 	.db #0x00	; 0
      003F12 00                    1946 	.db #0x00	; 0
      003F13 00                    1947 	.db #0x00	; 0
      003F14 01                    1948 	.db #0x01	; 1
      003F15 01                    1949 	.db #0x01	; 1
      003F16 00                    1950 	.db #0x00	; 0
      003F17 00                    1951 	.db #0x00	; 0
      003F18 00                    1952 	.db #0x00	; 0
      003F19 00                    1953 	.db #0x00	; 0
      003F1A 00                    1954 	.db #0x00	; 0
      003F1B 00                    1955 	.db #0x00	; 0
      003F1C 00                    1956 	.db #0x00	; 0
      003F1D 00                    1957 	.db #0x00	; 0
      003F1E 00                    1958 	.db #0x00	; 0
      003F1F 01                    1959 	.db #0x01	; 1
      003F20 01                    1960 	.db #0x01	; 1
      003F21 01                    1961 	.db #0x01	; 1
      003F22 01                    1962 	.db #0x01	; 1
      003F23 01                    1963 	.db #0x01	; 1
      003F24 01                    1964 	.db #0x01	; 1
      003F25 01                    1965 	.db #0x01	; 1
      003F26 01                    1966 	.db #0x01	; 1
      003F27 01                    1967 	.db #0x01	; 1
      003F28 01                    1968 	.db #0x01	; 1
      003F29 01                    1969 	.db #0x01	; 1
      003F2A 01                    1970 	.db #0x01	; 1
      003F2B 01                    1971 	.db #0x01	; 1
      003F2C 01                    1972 	.db #0x01	; 1
      003F2D 01                    1973 	.db #0x01	; 1
      003F2E 01                    1974 	.db #0x01	; 1
      003F2F 01                    1975 	.db #0x01	; 1
      003F30 01                    1976 	.db #0x01	; 1
      003F31 00                    1977 	.db #0x00	; 0
      003F32 00                    1978 	.db #0x00	; 0
      003F33 00                    1979 	.db #0x00	; 0
      003F34 00                    1980 	.db #0x00	; 0
      003F35 00                    1981 	.db #0x00	; 0
      003F36 00                    1982 	.db #0x00	; 0
      003F37 00                    1983 	.db #0x00	; 0
      003F38 00                    1984 	.db #0x00	; 0
      003F39 00                    1985 	.db #0x00	; 0
      003F3A 00                    1986 	.db #0x00	; 0
      003F3B 00                    1987 	.db #0x00	; 0
      003F3C 00                    1988 	.db #0x00	; 0
      003F3D 00                    1989 	.db #0x00	; 0
      003F3E 00                    1990 	.db #0x00	; 0
      003F3F 00                    1991 	.db #0x00	; 0
      003F40 00                    1992 	.db #0x00	; 0
      003F41 00                    1993 	.db #0x00	; 0
      003F42 00                    1994 	.db #0x00	; 0
      003F43 00                    1995 	.db #0x00	; 0
      003F44 00                    1996 	.db #0x00	; 0
      003F45 00                    1997 	.db #0x00	; 0
      003F46 00                    1998 	.db #0x00	; 0
      003F47 00                    1999 	.db #0x00	; 0
      003F48 00                    2000 	.db #0x00	; 0
      003F49 00                    2001 	.db #0x00	; 0
      003F4A 00                    2002 	.db #0x00	; 0
      003F4B 00                    2003 	.db #0x00	; 0
      003F4C 00                    2004 	.db #0x00	; 0
      003F4D 00                    2005 	.db #0x00	; 0
      003F4E 3F                    2006 	.db #0x3f	; 63
      003F4F 3F                    2007 	.db #0x3f	; 63
      003F50 03                    2008 	.db #0x03	; 3
      003F51 03                    2009 	.db #0x03	; 3
      003F52 F3                    2010 	.db #0xf3	; 243
      003F53 13                    2011 	.db #0x13	; 19
      003F54 11                    2012 	.db #0x11	; 17
      003F55 11                    2013 	.db #0x11	; 17
      003F56 11                    2014 	.db #0x11	; 17
      003F57 11                    2015 	.db #0x11	; 17
      003F58 11                    2016 	.db #0x11	; 17
      003F59 11                    2017 	.db #0x11	; 17
      003F5A 01                    2018 	.db #0x01	; 1
      003F5B F1                    2019 	.db #0xf1	; 241
      003F5C 11                    2020 	.db #0x11	; 17
      003F5D 61                    2021 	.db #0x61	; 97	'a'
      003F5E 81                    2022 	.db #0x81	; 129
      003F5F 01                    2023 	.db #0x01	; 1
      003F60 01                    2024 	.db #0x01	; 1
      003F61 01                    2025 	.db #0x01	; 1
      003F62 81                    2026 	.db #0x81	; 129
      003F63 61                    2027 	.db #0x61	; 97	'a'
      003F64 11                    2028 	.db #0x11	; 17
      003F65 F1                    2029 	.db #0xf1	; 241
      003F66 01                    2030 	.db #0x01	; 1
      003F67 01                    2031 	.db #0x01	; 1
      003F68 01                    2032 	.db #0x01	; 1
      003F69 01                    2033 	.db #0x01	; 1
      003F6A 41                    2034 	.db #0x41	; 65	'A'
      003F6B 41                    2035 	.db #0x41	; 65	'A'
      003F6C F1                    2036 	.db #0xf1	; 241
      003F6D 01                    2037 	.db #0x01	; 1
      003F6E 01                    2038 	.db #0x01	; 1
      003F6F 01                    2039 	.db #0x01	; 1
      003F70 01                    2040 	.db #0x01	; 1
      003F71 01                    2041 	.db #0x01	; 1
      003F72 C1                    2042 	.db #0xc1	; 193
      003F73 21                    2043 	.db #0x21	; 33
      003F74 11                    2044 	.db #0x11	; 17
      003F75 11                    2045 	.db #0x11	; 17
      003F76 11                    2046 	.db #0x11	; 17
      003F77 11                    2047 	.db #0x11	; 17
      003F78 21                    2048 	.db #0x21	; 33
      003F79 C1                    2049 	.db #0xc1	; 193
      003F7A 01                    2050 	.db #0x01	; 1
      003F7B 01                    2051 	.db #0x01	; 1
      003F7C 01                    2052 	.db #0x01	; 1
      003F7D 01                    2053 	.db #0x01	; 1
      003F7E 41                    2054 	.db #0x41	; 65	'A'
      003F7F 41                    2055 	.db #0x41	; 65	'A'
      003F80 F1                    2056 	.db #0xf1	; 241
      003F81 01                    2057 	.db #0x01	; 1
      003F82 01                    2058 	.db #0x01	; 1
      003F83 01                    2059 	.db #0x01	; 1
      003F84 01                    2060 	.db #0x01	; 1
      003F85 01                    2061 	.db #0x01	; 1
      003F86 01                    2062 	.db #0x01	; 1
      003F87 01                    2063 	.db #0x01	; 1
      003F88 01                    2064 	.db #0x01	; 1
      003F89 01                    2065 	.db #0x01	; 1
      003F8A 01                    2066 	.db #0x01	; 1
      003F8B 11                    2067 	.db #0x11	; 17
      003F8C 11                    2068 	.db #0x11	; 17
      003F8D 11                    2069 	.db #0x11	; 17
      003F8E 11                    2070 	.db #0x11	; 17
      003F8F 11                    2071 	.db #0x11	; 17
      003F90 D3                    2072 	.db #0xd3	; 211
      003F91 33                    2073 	.db #0x33	; 51	'3'
      003F92 03                    2074 	.db #0x03	; 3
      003F93 03                    2075 	.db #0x03	; 3
      003F94 3F                    2076 	.db #0x3f	; 63
      003F95 3F                    2077 	.db #0x3f	; 63
      003F96 00                    2078 	.db #0x00	; 0
      003F97 00                    2079 	.db #0x00	; 0
      003F98 00                    2080 	.db #0x00	; 0
      003F99 00                    2081 	.db #0x00	; 0
      003F9A 00                    2082 	.db #0x00	; 0
      003F9B 00                    2083 	.db #0x00	; 0
      003F9C 00                    2084 	.db #0x00	; 0
      003F9D 00                    2085 	.db #0x00	; 0
      003F9E 00                    2086 	.db #0x00	; 0
      003F9F 00                    2087 	.db #0x00	; 0
      003FA0 00                    2088 	.db #0x00	; 0
      003FA1 00                    2089 	.db #0x00	; 0
      003FA2 00                    2090 	.db #0x00	; 0
      003FA3 00                    2091 	.db #0x00	; 0
      003FA4 00                    2092 	.db #0x00	; 0
      003FA5 00                    2093 	.db #0x00	; 0
      003FA6 00                    2094 	.db #0x00	; 0
      003FA7 00                    2095 	.db #0x00	; 0
      003FA8 00                    2096 	.db #0x00	; 0
      003FA9 00                    2097 	.db #0x00	; 0
      003FAA 00                    2098 	.db #0x00	; 0
      003FAB 00                    2099 	.db #0x00	; 0
      003FAC 00                    2100 	.db #0x00	; 0
      003FAD 00                    2101 	.db #0x00	; 0
      003FAE 00                    2102 	.db #0x00	; 0
      003FAF 00                    2103 	.db #0x00	; 0
      003FB0 00                    2104 	.db #0x00	; 0
      003FB1 00                    2105 	.db #0x00	; 0
      003FB2 00                    2106 	.db #0x00	; 0
      003FB3 00                    2107 	.db #0x00	; 0
      003FB4 00                    2108 	.db #0x00	; 0
      003FB5 00                    2109 	.db #0x00	; 0
      003FB6 00                    2110 	.db #0x00	; 0
      003FB7 00                    2111 	.db #0x00	; 0
      003FB8 00                    2112 	.db #0x00	; 0
      003FB9 00                    2113 	.db #0x00	; 0
      003FBA 00                    2114 	.db #0x00	; 0
      003FBB 00                    2115 	.db #0x00	; 0
      003FBC 00                    2116 	.db #0x00	; 0
      003FBD 00                    2117 	.db #0x00	; 0
      003FBE 00                    2118 	.db #0x00	; 0
      003FBF 00                    2119 	.db #0x00	; 0
      003FC0 00                    2120 	.db #0x00	; 0
      003FC1 00                    2121 	.db #0x00	; 0
      003FC2 00                    2122 	.db #0x00	; 0
      003FC3 00                    2123 	.db #0x00	; 0
      003FC4 00                    2124 	.db #0x00	; 0
      003FC5 00                    2125 	.db #0x00	; 0
      003FC6 00                    2126 	.db #0x00	; 0
      003FC7 00                    2127 	.db #0x00	; 0
      003FC8 00                    2128 	.db #0x00	; 0
      003FC9 00                    2129 	.db #0x00	; 0
      003FCA 00                    2130 	.db #0x00	; 0
      003FCB 00                    2131 	.db #0x00	; 0
      003FCC 00                    2132 	.db #0x00	; 0
      003FCD 00                    2133 	.db #0x00	; 0
      003FCE E0                    2134 	.db #0xe0	; 224
      003FCF E0                    2135 	.db #0xe0	; 224
      003FD0 00                    2136 	.db #0x00	; 0
      003FD1 00                    2137 	.db #0x00	; 0
      003FD2 7F                    2138 	.db #0x7f	; 127
      003FD3 01                    2139 	.db #0x01	; 1
      003FD4 01                    2140 	.db #0x01	; 1
      003FD5 01                    2141 	.db #0x01	; 1
      003FD6 01                    2142 	.db #0x01	; 1
      003FD7 01                    2143 	.db #0x01	; 1
      003FD8 01                    2144 	.db #0x01	; 1
      003FD9 00                    2145 	.db #0x00	; 0
      003FDA 00                    2146 	.db #0x00	; 0
      003FDB 7F                    2147 	.db #0x7f	; 127
      003FDC 00                    2148 	.db #0x00	; 0
      003FDD 00                    2149 	.db #0x00	; 0
      003FDE 01                    2150 	.db #0x01	; 1
      003FDF 06                    2151 	.db #0x06	; 6
      003FE0 18                    2152 	.db #0x18	; 24
      003FE1 06                    2153 	.db #0x06	; 6
      003FE2 01                    2154 	.db #0x01	; 1
      003FE3 00                    2155 	.db #0x00	; 0
      003FE4 00                    2156 	.db #0x00	; 0
      003FE5 7F                    2157 	.db #0x7f	; 127
      003FE6 00                    2158 	.db #0x00	; 0
      003FE7 00                    2159 	.db #0x00	; 0
      003FE8 00                    2160 	.db #0x00	; 0
      003FE9 00                    2161 	.db #0x00	; 0
      003FEA 40                    2162 	.db #0x40	; 64
      003FEB 40                    2163 	.db #0x40	; 64
      003FEC 7F                    2164 	.db #0x7f	; 127
      003FED 40                    2165 	.db #0x40	; 64
      003FEE 40                    2166 	.db #0x40	; 64
      003FEF 00                    2167 	.db #0x00	; 0
      003FF0 00                    2168 	.db #0x00	; 0
      003FF1 00                    2169 	.db #0x00	; 0
      003FF2 1F                    2170 	.db #0x1f	; 31
      003FF3 20                    2171 	.db #0x20	; 32
      003FF4 40                    2172 	.db #0x40	; 64
      003FF5 40                    2173 	.db #0x40	; 64
      003FF6 40                    2174 	.db #0x40	; 64
      003FF7 40                    2175 	.db #0x40	; 64
      003FF8 20                    2176 	.db #0x20	; 32
      003FF9 1F                    2177 	.db #0x1f	; 31
      003FFA 00                    2178 	.db #0x00	; 0
      003FFB 00                    2179 	.db #0x00	; 0
      003FFC 00                    2180 	.db #0x00	; 0
      003FFD 00                    2181 	.db #0x00	; 0
      003FFE 40                    2182 	.db #0x40	; 64
      003FFF 40                    2183 	.db #0x40	; 64
      004000 7F                    2184 	.db #0x7f	; 127
      004001 40                    2185 	.db #0x40	; 64
      004002 40                    2186 	.db #0x40	; 64
      004003 00                    2187 	.db #0x00	; 0
      004004 00                    2188 	.db #0x00	; 0
      004005 00                    2189 	.db #0x00	; 0
      004006 00                    2190 	.db #0x00	; 0
      004007 60                    2191 	.db #0x60	; 96
      004008 00                    2192 	.db #0x00	; 0
      004009 00                    2193 	.db #0x00	; 0
      00400A 00                    2194 	.db #0x00	; 0
      00400B 00                    2195 	.db #0x00	; 0
      00400C 40                    2196 	.db #0x40	; 64
      00400D 30                    2197 	.db #0x30	; 48	'0'
      00400E 0C                    2198 	.db #0x0c	; 12
      00400F 03                    2199 	.db #0x03	; 3
      004010 00                    2200 	.db #0x00	; 0
      004011 00                    2201 	.db #0x00	; 0
      004012 00                    2202 	.db #0x00	; 0
      004013 00                    2203 	.db #0x00	; 0
      004014 E0                    2204 	.db #0xe0	; 224
      004015 E0                    2205 	.db #0xe0	; 224
      004016 00                    2206 	.db #0x00	; 0
      004017 00                    2207 	.db #0x00	; 0
      004018 00                    2208 	.db #0x00	; 0
      004019 00                    2209 	.db #0x00	; 0
      00401A 00                    2210 	.db #0x00	; 0
      00401B 00                    2211 	.db #0x00	; 0
      00401C 00                    2212 	.db #0x00	; 0
      00401D 00                    2213 	.db #0x00	; 0
      00401E 00                    2214 	.db #0x00	; 0
      00401F 00                    2215 	.db #0x00	; 0
      004020 00                    2216 	.db #0x00	; 0
      004021 00                    2217 	.db #0x00	; 0
      004022 00                    2218 	.db #0x00	; 0
      004023 00                    2219 	.db #0x00	; 0
      004024 00                    2220 	.db #0x00	; 0
      004025 00                    2221 	.db #0x00	; 0
      004026 00                    2222 	.db #0x00	; 0
      004027 00                    2223 	.db #0x00	; 0
      004028 00                    2224 	.db #0x00	; 0
      004029 00                    2225 	.db #0x00	; 0
      00402A 00                    2226 	.db #0x00	; 0
      00402B 00                    2227 	.db #0x00	; 0
      00402C 00                    2228 	.db #0x00	; 0
      00402D 00                    2229 	.db #0x00	; 0
      00402E 00                    2230 	.db #0x00	; 0
      00402F 00                    2231 	.db #0x00	; 0
      004030 00                    2232 	.db #0x00	; 0
      004031 00                    2233 	.db #0x00	; 0
      004032 00                    2234 	.db #0x00	; 0
      004033 00                    2235 	.db #0x00	; 0
      004034 00                    2236 	.db #0x00	; 0
      004035 00                    2237 	.db #0x00	; 0
      004036 00                    2238 	.db #0x00	; 0
      004037 00                    2239 	.db #0x00	; 0
      004038 00                    2240 	.db #0x00	; 0
      004039 00                    2241 	.db #0x00	; 0
      00403A 00                    2242 	.db #0x00	; 0
      00403B 00                    2243 	.db #0x00	; 0
      00403C 00                    2244 	.db #0x00	; 0
      00403D 00                    2245 	.db #0x00	; 0
      00403E 00                    2246 	.db #0x00	; 0
      00403F 00                    2247 	.db #0x00	; 0
      004040 00                    2248 	.db #0x00	; 0
      004041 00                    2249 	.db #0x00	; 0
      004042 00                    2250 	.db #0x00	; 0
      004043 00                    2251 	.db #0x00	; 0
      004044 00                    2252 	.db #0x00	; 0
      004045 00                    2253 	.db #0x00	; 0
      004046 00                    2254 	.db #0x00	; 0
      004047 00                    2255 	.db #0x00	; 0
      004048 00                    2256 	.db #0x00	; 0
      004049 00                    2257 	.db #0x00	; 0
      00404A 00                    2258 	.db #0x00	; 0
      00404B 00                    2259 	.db #0x00	; 0
      00404C 00                    2260 	.db #0x00	; 0
      00404D 00                    2261 	.db #0x00	; 0
      00404E 07                    2262 	.db #0x07	; 7
      00404F 07                    2263 	.db #0x07	; 7
      004050 06                    2264 	.db #0x06	; 6
      004051 06                    2265 	.db #0x06	; 6
      004052 06                    2266 	.db #0x06	; 6
      004053 06                    2267 	.db #0x06	; 6
      004054 04                    2268 	.db #0x04	; 4
      004055 04                    2269 	.db #0x04	; 4
      004056 04                    2270 	.db #0x04	; 4
      004057 84                    2271 	.db #0x84	; 132
      004058 44                    2272 	.db #0x44	; 68	'D'
      004059 44                    2273 	.db #0x44	; 68	'D'
      00405A 44                    2274 	.db #0x44	; 68	'D'
      00405B 84                    2275 	.db #0x84	; 132
      00405C 04                    2276 	.db #0x04	; 4
      00405D 04                    2277 	.db #0x04	; 4
      00405E 84                    2278 	.db #0x84	; 132
      00405F 44                    2279 	.db #0x44	; 68	'D'
      004060 44                    2280 	.db #0x44	; 68	'D'
      004061 44                    2281 	.db #0x44	; 68	'D'
      004062 84                    2282 	.db #0x84	; 132
      004063 04                    2283 	.db #0x04	; 4
      004064 04                    2284 	.db #0x04	; 4
      004065 04                    2285 	.db #0x04	; 4
      004066 84                    2286 	.db #0x84	; 132
      004067 C4                    2287 	.db #0xc4	; 196
      004068 04                    2288 	.db #0x04	; 4
      004069 04                    2289 	.db #0x04	; 4
      00406A 04                    2290 	.db #0x04	; 4
      00406B 04                    2291 	.db #0x04	; 4
      00406C 84                    2292 	.db #0x84	; 132
      00406D 44                    2293 	.db #0x44	; 68	'D'
      00406E 44                    2294 	.db #0x44	; 68	'D'
      00406F 44                    2295 	.db #0x44	; 68	'D'
      004070 84                    2296 	.db #0x84	; 132
      004071 04                    2297 	.db #0x04	; 4
      004072 04                    2298 	.db #0x04	; 4
      004073 04                    2299 	.db #0x04	; 4
      004074 04                    2300 	.db #0x04	; 4
      004075 04                    2301 	.db #0x04	; 4
      004076 84                    2302 	.db #0x84	; 132
      004077 44                    2303 	.db #0x44	; 68	'D'
      004078 44                    2304 	.db #0x44	; 68	'D'
      004079 44                    2305 	.db #0x44	; 68	'D'
      00407A 84                    2306 	.db #0x84	; 132
      00407B 04                    2307 	.db #0x04	; 4
      00407C 04                    2308 	.db #0x04	; 4
      00407D 04                    2309 	.db #0x04	; 4
      00407E 04                    2310 	.db #0x04	; 4
      00407F 04                    2311 	.db #0x04	; 4
      004080 84                    2312 	.db #0x84	; 132
      004081 44                    2313 	.db #0x44	; 68	'D'
      004082 44                    2314 	.db #0x44	; 68	'D'
      004083 44                    2315 	.db #0x44	; 68	'D'
      004084 84                    2316 	.db #0x84	; 132
      004085 04                    2317 	.db #0x04	; 4
      004086 04                    2318 	.db #0x04	; 4
      004087 84                    2319 	.db #0x84	; 132
      004088 44                    2320 	.db #0x44	; 68	'D'
      004089 44                    2321 	.db #0x44	; 68	'D'
      00408A 44                    2322 	.db #0x44	; 68	'D'
      00408B 84                    2323 	.db #0x84	; 132
      00408C 04                    2324 	.db #0x04	; 4
      00408D 04                    2325 	.db #0x04	; 4
      00408E 04                    2326 	.db #0x04	; 4
      00408F 04                    2327 	.db #0x04	; 4
      004090 06                    2328 	.db #0x06	; 6
      004091 06                    2329 	.db #0x06	; 6
      004092 06                    2330 	.db #0x06	; 6
      004093 06                    2331 	.db #0x06	; 6
      004094 07                    2332 	.db #0x07	; 7
      004095 07                    2333 	.db #0x07	; 7
      004096 00                    2334 	.db #0x00	; 0
      004097 00                    2335 	.db #0x00	; 0
      004098 00                    2336 	.db #0x00	; 0
      004099 00                    2337 	.db #0x00	; 0
      00409A 00                    2338 	.db #0x00	; 0
      00409B 00                    2339 	.db #0x00	; 0
      00409C 00                    2340 	.db #0x00	; 0
      00409D 00                    2341 	.db #0x00	; 0
      00409E 00                    2342 	.db #0x00	; 0
      00409F 00                    2343 	.db #0x00	; 0
      0040A0 00                    2344 	.db #0x00	; 0
      0040A1 00                    2345 	.db #0x00	; 0
      0040A2 00                    2346 	.db #0x00	; 0
      0040A3 00                    2347 	.db #0x00	; 0
      0040A4 00                    2348 	.db #0x00	; 0
      0040A5 00                    2349 	.db #0x00	; 0
      0040A6 00                    2350 	.db #0x00	; 0
      0040A7 00                    2351 	.db #0x00	; 0
      0040A8 00                    2352 	.db #0x00	; 0
      0040A9 00                    2353 	.db #0x00	; 0
      0040AA 00                    2354 	.db #0x00	; 0
      0040AB 00                    2355 	.db #0x00	; 0
      0040AC 00                    2356 	.db #0x00	; 0
      0040AD 00                    2357 	.db #0x00	; 0
      0040AE 00                    2358 	.db #0x00	; 0
      0040AF 00                    2359 	.db #0x00	; 0
      0040B0 00                    2360 	.db #0x00	; 0
      0040B1 00                    2361 	.db #0x00	; 0
      0040B2 00                    2362 	.db #0x00	; 0
      0040B3 00                    2363 	.db #0x00	; 0
      0040B4 00                    2364 	.db #0x00	; 0
      0040B5 00                    2365 	.db #0x00	; 0
      0040B6 00                    2366 	.db #0x00	; 0
      0040B7 00                    2367 	.db #0x00	; 0
      0040B8 00                    2368 	.db #0x00	; 0
      0040B9 00                    2369 	.db #0x00	; 0
      0040BA 00                    2370 	.db #0x00	; 0
      0040BB 00                    2371 	.db #0x00	; 0
      0040BC 00                    2372 	.db #0x00	; 0
      0040BD 00                    2373 	.db #0x00	; 0
      0040BE 00                    2374 	.db #0x00	; 0
      0040BF 00                    2375 	.db #0x00	; 0
      0040C0 00                    2376 	.db #0x00	; 0
      0040C1 00                    2377 	.db #0x00	; 0
      0040C2 00                    2378 	.db #0x00	; 0
      0040C3 00                    2379 	.db #0x00	; 0
      0040C4 00                    2380 	.db #0x00	; 0
      0040C5 00                    2381 	.db #0x00	; 0
      0040C6 00                    2382 	.db #0x00	; 0
      0040C7 00                    2383 	.db #0x00	; 0
      0040C8 00                    2384 	.db #0x00	; 0
      0040C9 00                    2385 	.db #0x00	; 0
      0040CA 00                    2386 	.db #0x00	; 0
      0040CB 00                    2387 	.db #0x00	; 0
      0040CC 00                    2388 	.db #0x00	; 0
      0040CD 00                    2389 	.db #0x00	; 0
      0040CE 00                    2390 	.db #0x00	; 0
      0040CF 00                    2391 	.db #0x00	; 0
      0040D0 00                    2392 	.db #0x00	; 0
      0040D1 00                    2393 	.db #0x00	; 0
      0040D2 00                    2394 	.db #0x00	; 0
      0040D3 00                    2395 	.db #0x00	; 0
      0040D4 00                    2396 	.db #0x00	; 0
      0040D5 00                    2397 	.db #0x00	; 0
      0040D6 00                    2398 	.db #0x00	; 0
      0040D7 10                    2399 	.db #0x10	; 16
      0040D8 18                    2400 	.db #0x18	; 24
      0040D9 14                    2401 	.db #0x14	; 20
      0040DA 12                    2402 	.db #0x12	; 18
      0040DB 11                    2403 	.db #0x11	; 17
      0040DC 00                    2404 	.db #0x00	; 0
      0040DD 00                    2405 	.db #0x00	; 0
      0040DE 0F                    2406 	.db #0x0f	; 15
      0040DF 10                    2407 	.db #0x10	; 16
      0040E0 10                    2408 	.db #0x10	; 16
      0040E1 10                    2409 	.db #0x10	; 16
      0040E2 0F                    2410 	.db #0x0f	; 15
      0040E3 00                    2411 	.db #0x00	; 0
      0040E4 00                    2412 	.db #0x00	; 0
      0040E5 00                    2413 	.db #0x00	; 0
      0040E6 10                    2414 	.db #0x10	; 16
      0040E7 1F                    2415 	.db #0x1f	; 31
      0040E8 10                    2416 	.db #0x10	; 16
      0040E9 00                    2417 	.db #0x00	; 0
      0040EA 00                    2418 	.db #0x00	; 0
      0040EB 00                    2419 	.db #0x00	; 0
      0040EC 08                    2420 	.db #0x08	; 8
      0040ED 10                    2421 	.db #0x10	; 16
      0040EE 12                    2422 	.db #0x12	; 18
      0040EF 12                    2423 	.db #0x12	; 18
      0040F0 0D                    2424 	.db #0x0d	; 13
      0040F1 00                    2425 	.db #0x00	; 0
      0040F2 00                    2426 	.db #0x00	; 0
      0040F3 18                    2427 	.db #0x18	; 24
      0040F4 00                    2428 	.db #0x00	; 0
      0040F5 00                    2429 	.db #0x00	; 0
      0040F6 0D                    2430 	.db #0x0d	; 13
      0040F7 12                    2431 	.db #0x12	; 18
      0040F8 12                    2432 	.db #0x12	; 18
      0040F9 12                    2433 	.db #0x12	; 18
      0040FA 0D                    2434 	.db #0x0d	; 13
      0040FB 00                    2435 	.db #0x00	; 0
      0040FC 00                    2436 	.db #0x00	; 0
      0040FD 18                    2437 	.db #0x18	; 24
      0040FE 00                    2438 	.db #0x00	; 0
      0040FF 00                    2439 	.db #0x00	; 0
      004100 10                    2440 	.db #0x10	; 16
      004101 18                    2441 	.db #0x18	; 24
      004102 14                    2442 	.db #0x14	; 20
      004103 12                    2443 	.db #0x12	; 18
      004104 11                    2444 	.db #0x11	; 17
      004105 00                    2445 	.db #0x00	; 0
      004106 00                    2446 	.db #0x00	; 0
      004107 10                    2447 	.db #0x10	; 16
      004108 18                    2448 	.db #0x18	; 24
      004109 14                    2449 	.db #0x14	; 20
      00410A 12                    2450 	.db #0x12	; 18
      00410B 11                    2451 	.db #0x11	; 17
      00410C 00                    2452 	.db #0x00	; 0
      00410D 00                    2453 	.db #0x00	; 0
      00410E 00                    2454 	.db #0x00	; 0
      00410F 00                    2455 	.db #0x00	; 0
      004110 00                    2456 	.db #0x00	; 0
      004111 00                    2457 	.db #0x00	; 0
      004112 00                    2458 	.db #0x00	; 0
      004113 00                    2459 	.db #0x00	; 0
      004114 00                    2460 	.db #0x00	; 0
      004115 00                    2461 	.db #0x00	; 0
      004116 00                    2462 	.db #0x00	; 0
      004117 00                    2463 	.db #0x00	; 0
      004118 00                    2464 	.db #0x00	; 0
      004119 00                    2465 	.db #0x00	; 0
      00411A 00                    2466 	.db #0x00	; 0
      00411B 00                    2467 	.db #0x00	; 0
      00411C 00                    2468 	.db #0x00	; 0
      00411D 00                    2469 	.db #0x00	; 0
      00411E 00                    2470 	.db #0x00	; 0
      00411F 00                    2471 	.db #0x00	; 0
      004120 00                    2472 	.db #0x00	; 0
      004121 00                    2473 	.db #0x00	; 0
      004122 00                    2474 	.db #0x00	; 0
      004123 00                    2475 	.db #0x00	; 0
      004124 00                    2476 	.db #0x00	; 0
      004125 00                    2477 	.db #0x00	; 0
      004126 00                    2478 	.db #0x00	; 0
      004127 00                    2479 	.db #0x00	; 0
      004128 00                    2480 	.db #0x00	; 0
      004129 00                    2481 	.db #0x00	; 0
      00412A 00                    2482 	.db #0x00	; 0
      00412B 00                    2483 	.db #0x00	; 0
      00412C 00                    2484 	.db #0x00	; 0
      00412D 00                    2485 	.db #0x00	; 0
      00412E 00                    2486 	.db #0x00	; 0
      00412F 00                    2487 	.db #0x00	; 0
      004130 00                    2488 	.db #0x00	; 0
      004131 00                    2489 	.db #0x00	; 0
      004132 00                    2490 	.db #0x00	; 0
      004133 00                    2491 	.db #0x00	; 0
      004134 00                    2492 	.db #0x00	; 0
      004135 00                    2493 	.db #0x00	; 0
      004136 00                    2494 	.db #0x00	; 0
      004137 00                    2495 	.db #0x00	; 0
      004138 00                    2496 	.db #0x00	; 0
      004139 00                    2497 	.db #0x00	; 0
      00413A 00                    2498 	.db #0x00	; 0
      00413B 00                    2499 	.db #0x00	; 0
      00413C 00                    2500 	.db #0x00	; 0
      00413D 00                    2501 	.db #0x00	; 0
      00413E 00                    2502 	.db #0x00	; 0
      00413F 00                    2503 	.db #0x00	; 0
      004140 00                    2504 	.db #0x00	; 0
      004141 00                    2505 	.db #0x00	; 0
      004142 00                    2506 	.db #0x00	; 0
      004143 00                    2507 	.db #0x00	; 0
      004144 00                    2508 	.db #0x00	; 0
      004145 00                    2509 	.db #0x00	; 0
      004146 00                    2510 	.db #0x00	; 0
      004147 00                    2511 	.db #0x00	; 0
      004148 00                    2512 	.db #0x00	; 0
      004149 00                    2513 	.db #0x00	; 0
      00414A 00                    2514 	.db #0x00	; 0
      00414B 00                    2515 	.db #0x00	; 0
      00414C 00                    2516 	.db #0x00	; 0
      00414D 00                    2517 	.db #0x00	; 0
      00414E 00                    2518 	.db #0x00	; 0
      00414F 00                    2519 	.db #0x00	; 0
      004150 00                    2520 	.db #0x00	; 0
      004151 00                    2521 	.db #0x00	; 0
      004152 00                    2522 	.db #0x00	; 0
      004153 00                    2523 	.db #0x00	; 0
      004154 00                    2524 	.db #0x00	; 0
      004155 00                    2525 	.db #0x00	; 0
      004156 00                    2526 	.db #0x00	; 0
      004157 00                    2527 	.db #0x00	; 0
      004158 00                    2528 	.db #0x00	; 0
      004159 00                    2529 	.db #0x00	; 0
      00415A 00                    2530 	.db #0x00	; 0
      00415B 00                    2531 	.db #0x00	; 0
      00415C 00                    2532 	.db #0x00	; 0
      00415D 00                    2533 	.db #0x00	; 0
      00415E 00                    2534 	.db #0x00	; 0
      00415F 00                    2535 	.db #0x00	; 0
      004160 00                    2536 	.db #0x00	; 0
      004161 00                    2537 	.db #0x00	; 0
      004162 00                    2538 	.db #0x00	; 0
      004163 00                    2539 	.db #0x00	; 0
      004164 00                    2540 	.db #0x00	; 0
      004165 00                    2541 	.db #0x00	; 0
      004166 00                    2542 	.db #0x00	; 0
      004167 00                    2543 	.db #0x00	; 0
      004168 00                    2544 	.db #0x00	; 0
      004169 00                    2545 	.db #0x00	; 0
      00416A 00                    2546 	.db #0x00	; 0
      00416B 00                    2547 	.db #0x00	; 0
      00416C 00                    2548 	.db #0x00	; 0
      00416D 00                    2549 	.db #0x00	; 0
      00416E 80                    2550 	.db #0x80	; 128
      00416F 80                    2551 	.db #0x80	; 128
      004170 80                    2552 	.db #0x80	; 128
      004171 80                    2553 	.db #0x80	; 128
      004172 80                    2554 	.db #0x80	; 128
      004173 80                    2555 	.db #0x80	; 128
      004174 80                    2556 	.db #0x80	; 128
      004175 80                    2557 	.db #0x80	; 128
      004176 00                    2558 	.db #0x00	; 0
      004177 00                    2559 	.db #0x00	; 0
      004178 00                    2560 	.db #0x00	; 0
      004179 00                    2561 	.db #0x00	; 0
      00417A 00                    2562 	.db #0x00	; 0
      00417B 00                    2563 	.db #0x00	; 0
      00417C 00                    2564 	.db #0x00	; 0
      00417D 00                    2565 	.db #0x00	; 0
      00417E 00                    2566 	.db #0x00	; 0
      00417F 00                    2567 	.db #0x00	; 0
      004180 00                    2568 	.db #0x00	; 0
      004181 00                    2569 	.db #0x00	; 0
      004182 00                    2570 	.db #0x00	; 0
      004183 00                    2571 	.db #0x00	; 0
      004184 00                    2572 	.db #0x00	; 0
      004185 00                    2573 	.db #0x00	; 0
      004186 00                    2574 	.db #0x00	; 0
      004187 00                    2575 	.db #0x00	; 0
      004188 00                    2576 	.db #0x00	; 0
      004189 00                    2577 	.db #0x00	; 0
      00418A 00                    2578 	.db #0x00	; 0
      00418B 00                    2579 	.db #0x00	; 0
      00418C 00                    2580 	.db #0x00	; 0
      00418D 00                    2581 	.db #0x00	; 0
      00418E 00                    2582 	.db #0x00	; 0
      00418F 00                    2583 	.db #0x00	; 0
      004190 00                    2584 	.db #0x00	; 0
      004191 00                    2585 	.db #0x00	; 0
      004192 00                    2586 	.db #0x00	; 0
      004193 00                    2587 	.db #0x00	; 0
      004194 00                    2588 	.db #0x00	; 0
      004195 00                    2589 	.db #0x00	; 0
      004196 00                    2590 	.db #0x00	; 0
      004197 00                    2591 	.db #0x00	; 0
      004198 00                    2592 	.db #0x00	; 0
      004199 00                    2593 	.db #0x00	; 0
      00419A 00                    2594 	.db #0x00	; 0
      00419B 00                    2595 	.db #0x00	; 0
      00419C 00                    2596 	.db #0x00	; 0
      00419D 00                    2597 	.db #0x00	; 0
      00419E 00                    2598 	.db #0x00	; 0
      00419F 00                    2599 	.db #0x00	; 0
      0041A0 00                    2600 	.db #0x00	; 0
      0041A1 00                    2601 	.db #0x00	; 0
      0041A2 00                    2602 	.db #0x00	; 0
      0041A3 00                    2603 	.db #0x00	; 0
      0041A4 00                    2604 	.db #0x00	; 0
      0041A5 00                    2605 	.db #0x00	; 0
      0041A6 00                    2606 	.db #0x00	; 0
      0041A7 00                    2607 	.db #0x00	; 0
      0041A8 00                    2608 	.db #0x00	; 0
      0041A9 00                    2609 	.db #0x00	; 0
      0041AA 00                    2610 	.db #0x00	; 0
      0041AB 00                    2611 	.db #0x00	; 0
      0041AC 00                    2612 	.db #0x00	; 0
      0041AD 00                    2613 	.db #0x00	; 0
      0041AE 00                    2614 	.db #0x00	; 0
      0041AF 00                    2615 	.db #0x00	; 0
      0041B0 00                    2616 	.db #0x00	; 0
      0041B1 00                    2617 	.db #0x00	; 0
      0041B2 00                    2618 	.db #0x00	; 0
      0041B3 7F                    2619 	.db #0x7f	; 127
      0041B4 03                    2620 	.db #0x03	; 3
      0041B5 0C                    2621 	.db #0x0c	; 12
      0041B6 30                    2622 	.db #0x30	; 48	'0'
      0041B7 0C                    2623 	.db #0x0c	; 12
      0041B8 03                    2624 	.db #0x03	; 3
      0041B9 7F                    2625 	.db #0x7f	; 127
      0041BA 00                    2626 	.db #0x00	; 0
      0041BB 00                    2627 	.db #0x00	; 0
      0041BC 38                    2628 	.db #0x38	; 56	'8'
      0041BD 54                    2629 	.db #0x54	; 84	'T'
      0041BE 54                    2630 	.db #0x54	; 84	'T'
      0041BF 58                    2631 	.db #0x58	; 88	'X'
      0041C0 00                    2632 	.db #0x00	; 0
      0041C1 00                    2633 	.db #0x00	; 0
      0041C2 7C                    2634 	.db #0x7c	; 124
      0041C3 04                    2635 	.db #0x04	; 4
      0041C4 04                    2636 	.db #0x04	; 4
      0041C5 78                    2637 	.db #0x78	; 120	'x'
      0041C6 00                    2638 	.db #0x00	; 0
      0041C7 00                    2639 	.db #0x00	; 0
      0041C8 3C                    2640 	.db #0x3c	; 60
      0041C9 40                    2641 	.db #0x40	; 64
      0041CA 40                    2642 	.db #0x40	; 64
      0041CB 7C                    2643 	.db #0x7c	; 124
      0041CC 00                    2644 	.db #0x00	; 0
      0041CD 00                    2645 	.db #0x00	; 0
      0041CE 00                    2646 	.db #0x00	; 0
      0041CF 00                    2647 	.db #0x00	; 0
      0041D0 00                    2648 	.db #0x00	; 0
      0041D1 00                    2649 	.db #0x00	; 0
      0041D2 00                    2650 	.db #0x00	; 0
      0041D3 00                    2651 	.db #0x00	; 0
      0041D4 00                    2652 	.db #0x00	; 0
      0041D5 00                    2653 	.db #0x00	; 0
      0041D6 00                    2654 	.db #0x00	; 0
      0041D7 00                    2655 	.db #0x00	; 0
      0041D8 00                    2656 	.db #0x00	; 0
      0041D9 00                    2657 	.db #0x00	; 0
      0041DA 00                    2658 	.db #0x00	; 0
      0041DB 00                    2659 	.db #0x00	; 0
      0041DC 00                    2660 	.db #0x00	; 0
      0041DD 00                    2661 	.db #0x00	; 0
      0041DE 00                    2662 	.db #0x00	; 0
      0041DF 00                    2663 	.db #0x00	; 0
      0041E0 00                    2664 	.db #0x00	; 0
      0041E1 00                    2665 	.db #0x00	; 0
      0041E2 00                    2666 	.db #0x00	; 0
      0041E3 00                    2667 	.db #0x00	; 0
      0041E4 00                    2668 	.db #0x00	; 0
      0041E5 00                    2669 	.db #0x00	; 0
      0041E6 00                    2670 	.db #0x00	; 0
      0041E7 00                    2671 	.db #0x00	; 0
      0041E8 00                    2672 	.db #0x00	; 0
      0041E9 00                    2673 	.db #0x00	; 0
      0041EA 00                    2674 	.db #0x00	; 0
      0041EB 00                    2675 	.db #0x00	; 0
      0041EC 00                    2676 	.db #0x00	; 0
      0041ED 00                    2677 	.db #0x00	; 0
      0041EE FF                    2678 	.db #0xff	; 255
      0041EF AA                    2679 	.db #0xaa	; 170
      0041F0 AA                    2680 	.db #0xaa	; 170
      0041F1 AA                    2681 	.db #0xaa	; 170
      0041F2 28                    2682 	.db #0x28	; 40
      0041F3 08                    2683 	.db #0x08	; 8
      0041F4 00                    2684 	.db #0x00	; 0
      0041F5 FF                    2685 	.db #0xff	; 255
      0041F6 00                    2686 	.db #0x00	; 0
      0041F7 00                    2687 	.db #0x00	; 0
      0041F8 00                    2688 	.db #0x00	; 0
      0041F9 00                    2689 	.db #0x00	; 0
      0041FA 00                    2690 	.db #0x00	; 0
      0041FB 00                    2691 	.db #0x00	; 0
      0041FC 00                    2692 	.db #0x00	; 0
      0041FD 00                    2693 	.db #0x00	; 0
      0041FE 00                    2694 	.db #0x00	; 0
      0041FF 00                    2695 	.db #0x00	; 0
      004200 00                    2696 	.db #0x00	; 0
      004201 00                    2697 	.db #0x00	; 0
      004202 00                    2698 	.db #0x00	; 0
      004203 00                    2699 	.db #0x00	; 0
      004204 00                    2700 	.db #0x00	; 0
      004205 00                    2701 	.db #0x00	; 0
      004206 00                    2702 	.db #0x00	; 0
      004207 00                    2703 	.db #0x00	; 0
      004208 00                    2704 	.db #0x00	; 0
      004209 00                    2705 	.db #0x00	; 0
      00420A 00                    2706 	.db #0x00	; 0
      00420B 00                    2707 	.db #0x00	; 0
      00420C 00                    2708 	.db #0x00	; 0
      00420D 00                    2709 	.db #0x00	; 0
      00420E 00                    2710 	.db #0x00	; 0
      00420F 00                    2711 	.db #0x00	; 0
      004210 00                    2712 	.db #0x00	; 0
      004211 00                    2713 	.db #0x00	; 0
      004212 00                    2714 	.db #0x00	; 0
      004213 00                    2715 	.db #0x00	; 0
      004214 00                    2716 	.db #0x00	; 0
      004215 00                    2717 	.db #0x00	; 0
      004216 00                    2718 	.db #0x00	; 0
      004217 00                    2719 	.db #0x00	; 0
      004218 00                    2720 	.db #0x00	; 0
      004219 00                    2721 	.db #0x00	; 0
      00421A 00                    2722 	.db #0x00	; 0
      00421B 7F                    2723 	.db #0x7f	; 127
      00421C 03                    2724 	.db #0x03	; 3
      00421D 0C                    2725 	.db #0x0c	; 12
      00421E 30                    2726 	.db #0x30	; 48	'0'
      00421F 0C                    2727 	.db #0x0c	; 12
      004220 03                    2728 	.db #0x03	; 3
      004221 7F                    2729 	.db #0x7f	; 127
      004222 00                    2730 	.db #0x00	; 0
      004223 00                    2731 	.db #0x00	; 0
      004224 26                    2732 	.db #0x26	; 38
      004225 49                    2733 	.db #0x49	; 73	'I'
      004226 49                    2734 	.db #0x49	; 73	'I'
      004227 49                    2735 	.db #0x49	; 73	'I'
      004228 32                    2736 	.db #0x32	; 50	'2'
      004229 00                    2737 	.db #0x00	; 0
      00422A 00                    2738 	.db #0x00	; 0
      00422B 7F                    2739 	.db #0x7f	; 127
      00422C 02                    2740 	.db #0x02	; 2
      00422D 04                    2741 	.db #0x04	; 4
      00422E 08                    2742 	.db #0x08	; 8
      00422F 10                    2743 	.db #0x10	; 16
      004230 7F                    2744 	.db #0x7f	; 127
      004231 00                    2745 	.db #0x00	; 0
      004232                       2746 _BMP2:
      004232 00                    2747 	.db #0x00	; 0
      004233 03                    2748 	.db #0x03	; 3
      004234 05                    2749 	.db #0x05	; 5
      004235 09                    2750 	.db #0x09	; 9
      004236 11                    2751 	.db #0x11	; 17
      004237 FF                    2752 	.db #0xff	; 255
      004238 11                    2753 	.db #0x11	; 17
      004239 89                    2754 	.db #0x89	; 137
      00423A 05                    2755 	.db #0x05	; 5
      00423B C3                    2756 	.db #0xc3	; 195
      00423C 00                    2757 	.db #0x00	; 0
      00423D E0                    2758 	.db #0xe0	; 224
      00423E 00                    2759 	.db #0x00	; 0
      00423F F0                    2760 	.db #0xf0	; 240
      004240 00                    2761 	.db #0x00	; 0
      004241 F8                    2762 	.db #0xf8	; 248
      004242 00                    2763 	.db #0x00	; 0
      004243 00                    2764 	.db #0x00	; 0
      004244 00                    2765 	.db #0x00	; 0
      004245 00                    2766 	.db #0x00	; 0
      004246 00                    2767 	.db #0x00	; 0
      004247 00                    2768 	.db #0x00	; 0
      004248 00                    2769 	.db #0x00	; 0
      004249 44                    2770 	.db #0x44	; 68	'D'
      00424A 28                    2771 	.db #0x28	; 40
      00424B FF                    2772 	.db #0xff	; 255
      00424C 11                    2773 	.db #0x11	; 17
      00424D AA                    2774 	.db #0xaa	; 170
      00424E 44                    2775 	.db #0x44	; 68	'D'
      00424F 00                    2776 	.db #0x00	; 0
      004250 00                    2777 	.db #0x00	; 0
      004251 00                    2778 	.db #0x00	; 0
      004252 00                    2779 	.db #0x00	; 0
      004253 00                    2780 	.db #0x00	; 0
      004254 00                    2781 	.db #0x00	; 0
      004255 00                    2782 	.db #0x00	; 0
      004256 00                    2783 	.db #0x00	; 0
      004257 00                    2784 	.db #0x00	; 0
      004258 00                    2785 	.db #0x00	; 0
      004259 00                    2786 	.db #0x00	; 0
      00425A 00                    2787 	.db #0x00	; 0
      00425B 00                    2788 	.db #0x00	; 0
      00425C 00                    2789 	.db #0x00	; 0
      00425D 00                    2790 	.db #0x00	; 0
      00425E 00                    2791 	.db #0x00	; 0
      00425F 00                    2792 	.db #0x00	; 0
      004260 00                    2793 	.db #0x00	; 0
      004261 00                    2794 	.db #0x00	; 0
      004262 00                    2795 	.db #0x00	; 0
      004263 00                    2796 	.db #0x00	; 0
      004264 00                    2797 	.db #0x00	; 0
      004265 00                    2798 	.db #0x00	; 0
      004266 00                    2799 	.db #0x00	; 0
      004267 00                    2800 	.db #0x00	; 0
      004268 00                    2801 	.db #0x00	; 0
      004269 00                    2802 	.db #0x00	; 0
      00426A 00                    2803 	.db #0x00	; 0
      00426B 00                    2804 	.db #0x00	; 0
      00426C 00                    2805 	.db #0x00	; 0
      00426D 00                    2806 	.db #0x00	; 0
      00426E 00                    2807 	.db #0x00	; 0
      00426F 00                    2808 	.db #0x00	; 0
      004270 00                    2809 	.db #0x00	; 0
      004271 00                    2810 	.db #0x00	; 0
      004272 00                    2811 	.db #0x00	; 0
      004273 00                    2812 	.db #0x00	; 0
      004274 00                    2813 	.db #0x00	; 0
      004275 00                    2814 	.db #0x00	; 0
      004276 00                    2815 	.db #0x00	; 0
      004277 00                    2816 	.db #0x00	; 0
      004278 00                    2817 	.db #0x00	; 0
      004279 00                    2818 	.db #0x00	; 0
      00427A 00                    2819 	.db #0x00	; 0
      00427B 00                    2820 	.db #0x00	; 0
      00427C 00                    2821 	.db #0x00	; 0
      00427D 00                    2822 	.db #0x00	; 0
      00427E 00                    2823 	.db #0x00	; 0
      00427F 00                    2824 	.db #0x00	; 0
      004280 00                    2825 	.db #0x00	; 0
      004281 00                    2826 	.db #0x00	; 0
      004282 00                    2827 	.db #0x00	; 0
      004283 00                    2828 	.db #0x00	; 0
      004284 00                    2829 	.db #0x00	; 0
      004285 00                    2830 	.db #0x00	; 0
      004286 00                    2831 	.db #0x00	; 0
      004287 00                    2832 	.db #0x00	; 0
      004288 00                    2833 	.db #0x00	; 0
      004289 00                    2834 	.db #0x00	; 0
      00428A 00                    2835 	.db #0x00	; 0
      00428B 00                    2836 	.db #0x00	; 0
      00428C 83                    2837 	.db #0x83	; 131
      00428D 01                    2838 	.db #0x01	; 1
      00428E 38                    2839 	.db #0x38	; 56	'8'
      00428F 44                    2840 	.db #0x44	; 68	'D'
      004290 82                    2841 	.db #0x82	; 130
      004291 92                    2842 	.db #0x92	; 146
      004292 92                    2843 	.db #0x92	; 146
      004293 74                    2844 	.db #0x74	; 116	't'
      004294 01                    2845 	.db #0x01	; 1
      004295 83                    2846 	.db #0x83	; 131
      004296 00                    2847 	.db #0x00	; 0
      004297 00                    2848 	.db #0x00	; 0
      004298 00                    2849 	.db #0x00	; 0
      004299 00                    2850 	.db #0x00	; 0
      00429A 00                    2851 	.db #0x00	; 0
      00429B 00                    2852 	.db #0x00	; 0
      00429C 00                    2853 	.db #0x00	; 0
      00429D 7C                    2854 	.db #0x7c	; 124
      00429E 44                    2855 	.db #0x44	; 68	'D'
      00429F FF                    2856 	.db #0xff	; 255
      0042A0 01                    2857 	.db #0x01	; 1
      0042A1 7D                    2858 	.db #0x7d	; 125
      0042A2 7D                    2859 	.db #0x7d	; 125
      0042A3 7D                    2860 	.db #0x7d	; 125
      0042A4 7D                    2861 	.db #0x7d	; 125
      0042A5 01                    2862 	.db #0x01	; 1
      0042A6 7D                    2863 	.db #0x7d	; 125
      0042A7 7D                    2864 	.db #0x7d	; 125
      0042A8 7D                    2865 	.db #0x7d	; 125
      0042A9 7D                    2866 	.db #0x7d	; 125
      0042AA 01                    2867 	.db #0x01	; 1
      0042AB 7D                    2868 	.db #0x7d	; 125
      0042AC 7D                    2869 	.db #0x7d	; 125
      0042AD 7D                    2870 	.db #0x7d	; 125
      0042AE 7D                    2871 	.db #0x7d	; 125
      0042AF 01                    2872 	.db #0x01	; 1
      0042B0 FF                    2873 	.db #0xff	; 255
      0042B1 00                    2874 	.db #0x00	; 0
      0042B2 00                    2875 	.db #0x00	; 0
      0042B3 00                    2876 	.db #0x00	; 0
      0042B4 00                    2877 	.db #0x00	; 0
      0042B5 00                    2878 	.db #0x00	; 0
      0042B6 00                    2879 	.db #0x00	; 0
      0042B7 01                    2880 	.db #0x01	; 1
      0042B8 00                    2881 	.db #0x00	; 0
      0042B9 01                    2882 	.db #0x01	; 1
      0042BA 00                    2883 	.db #0x00	; 0
      0042BB 01                    2884 	.db #0x01	; 1
      0042BC 00                    2885 	.db #0x00	; 0
      0042BD 01                    2886 	.db #0x01	; 1
      0042BE 00                    2887 	.db #0x00	; 0
      0042BF 01                    2888 	.db #0x01	; 1
      0042C0 00                    2889 	.db #0x00	; 0
      0042C1 01                    2890 	.db #0x01	; 1
      0042C2 00                    2891 	.db #0x00	; 0
      0042C3 00                    2892 	.db #0x00	; 0
      0042C4 00                    2893 	.db #0x00	; 0
      0042C5 00                    2894 	.db #0x00	; 0
      0042C6 00                    2895 	.db #0x00	; 0
      0042C7 00                    2896 	.db #0x00	; 0
      0042C8 00                    2897 	.db #0x00	; 0
      0042C9 00                    2898 	.db #0x00	; 0
      0042CA 00                    2899 	.db #0x00	; 0
      0042CB 01                    2900 	.db #0x01	; 1
      0042CC 01                    2901 	.db #0x01	; 1
      0042CD 00                    2902 	.db #0x00	; 0
      0042CE 00                    2903 	.db #0x00	; 0
      0042CF 00                    2904 	.db #0x00	; 0
      0042D0 00                    2905 	.db #0x00	; 0
      0042D1 00                    2906 	.db #0x00	; 0
      0042D2 00                    2907 	.db #0x00	; 0
      0042D3 00                    2908 	.db #0x00	; 0
      0042D4 00                    2909 	.db #0x00	; 0
      0042D5 00                    2910 	.db #0x00	; 0
      0042D6 00                    2911 	.db #0x00	; 0
      0042D7 00                    2912 	.db #0x00	; 0
      0042D8 00                    2913 	.db #0x00	; 0
      0042D9 00                    2914 	.db #0x00	; 0
      0042DA 00                    2915 	.db #0x00	; 0
      0042DB 00                    2916 	.db #0x00	; 0
      0042DC 00                    2917 	.db #0x00	; 0
      0042DD 00                    2918 	.db #0x00	; 0
      0042DE 00                    2919 	.db #0x00	; 0
      0042DF 00                    2920 	.db #0x00	; 0
      0042E0 00                    2921 	.db #0x00	; 0
      0042E1 00                    2922 	.db #0x00	; 0
      0042E2 00                    2923 	.db #0x00	; 0
      0042E3 00                    2924 	.db #0x00	; 0
      0042E4 00                    2925 	.db #0x00	; 0
      0042E5 00                    2926 	.db #0x00	; 0
      0042E6 00                    2927 	.db #0x00	; 0
      0042E7 00                    2928 	.db #0x00	; 0
      0042E8 00                    2929 	.db #0x00	; 0
      0042E9 00                    2930 	.db #0x00	; 0
      0042EA 00                    2931 	.db #0x00	; 0
      0042EB 00                    2932 	.db #0x00	; 0
      0042EC 00                    2933 	.db #0x00	; 0
      0042ED 00                    2934 	.db #0x00	; 0
      0042EE 00                    2935 	.db #0x00	; 0
      0042EF 00                    2936 	.db #0x00	; 0
      0042F0 00                    2937 	.db #0x00	; 0
      0042F1 00                    2938 	.db #0x00	; 0
      0042F2 00                    2939 	.db #0x00	; 0
      0042F3 00                    2940 	.db #0x00	; 0
      0042F4 00                    2941 	.db #0x00	; 0
      0042F5 00                    2942 	.db #0x00	; 0
      0042F6 00                    2943 	.db #0x00	; 0
      0042F7 00                    2944 	.db #0x00	; 0
      0042F8 00                    2945 	.db #0x00	; 0
      0042F9 00                    2946 	.db #0x00	; 0
      0042FA 00                    2947 	.db #0x00	; 0
      0042FB 00                    2948 	.db #0x00	; 0
      0042FC 00                    2949 	.db #0x00	; 0
      0042FD 00                    2950 	.db #0x00	; 0
      0042FE 00                    2951 	.db #0x00	; 0
      0042FF 00                    2952 	.db #0x00	; 0
      004300 00                    2953 	.db #0x00	; 0
      004301 00                    2954 	.db #0x00	; 0
      004302 00                    2955 	.db #0x00	; 0
      004303 00                    2956 	.db #0x00	; 0
      004304 00                    2957 	.db #0x00	; 0
      004305 00                    2958 	.db #0x00	; 0
      004306 00                    2959 	.db #0x00	; 0
      004307 00                    2960 	.db #0x00	; 0
      004308 00                    2961 	.db #0x00	; 0
      004309 00                    2962 	.db #0x00	; 0
      00430A 00                    2963 	.db #0x00	; 0
      00430B 00                    2964 	.db #0x00	; 0
      00430C 01                    2965 	.db #0x01	; 1
      00430D 01                    2966 	.db #0x01	; 1
      00430E 00                    2967 	.db #0x00	; 0
      00430F 00                    2968 	.db #0x00	; 0
      004310 00                    2969 	.db #0x00	; 0
      004311 00                    2970 	.db #0x00	; 0
      004312 00                    2971 	.db #0x00	; 0
      004313 00                    2972 	.db #0x00	; 0
      004314 01                    2973 	.db #0x01	; 1
      004315 01                    2974 	.db #0x01	; 1
      004316 00                    2975 	.db #0x00	; 0
      004317 00                    2976 	.db #0x00	; 0
      004318 00                    2977 	.db #0x00	; 0
      004319 00                    2978 	.db #0x00	; 0
      00431A 00                    2979 	.db #0x00	; 0
      00431B 00                    2980 	.db #0x00	; 0
      00431C 00                    2981 	.db #0x00	; 0
      00431D 00                    2982 	.db #0x00	; 0
      00431E 00                    2983 	.db #0x00	; 0
      00431F 01                    2984 	.db #0x01	; 1
      004320 01                    2985 	.db #0x01	; 1
      004321 01                    2986 	.db #0x01	; 1
      004322 01                    2987 	.db #0x01	; 1
      004323 01                    2988 	.db #0x01	; 1
      004324 01                    2989 	.db #0x01	; 1
      004325 01                    2990 	.db #0x01	; 1
      004326 01                    2991 	.db #0x01	; 1
      004327 01                    2992 	.db #0x01	; 1
      004328 01                    2993 	.db #0x01	; 1
      004329 01                    2994 	.db #0x01	; 1
      00432A 01                    2995 	.db #0x01	; 1
      00432B 01                    2996 	.db #0x01	; 1
      00432C 01                    2997 	.db #0x01	; 1
      00432D 01                    2998 	.db #0x01	; 1
      00432E 01                    2999 	.db #0x01	; 1
      00432F 01                    3000 	.db #0x01	; 1
      004330 01                    3001 	.db #0x01	; 1
      004331 00                    3002 	.db #0x00	; 0
      004332 00                    3003 	.db #0x00	; 0
      004333 00                    3004 	.db #0x00	; 0
      004334 00                    3005 	.db #0x00	; 0
      004335 00                    3006 	.db #0x00	; 0
      004336 00                    3007 	.db #0x00	; 0
      004337 00                    3008 	.db #0x00	; 0
      004338 00                    3009 	.db #0x00	; 0
      004339 00                    3010 	.db #0x00	; 0
      00433A 00                    3011 	.db #0x00	; 0
      00433B 00                    3012 	.db #0x00	; 0
      00433C 00                    3013 	.db #0x00	; 0
      00433D 00                    3014 	.db #0x00	; 0
      00433E 00                    3015 	.db #0x00	; 0
      00433F 00                    3016 	.db #0x00	; 0
      004340 00                    3017 	.db #0x00	; 0
      004341 00                    3018 	.db #0x00	; 0
      004342 00                    3019 	.db #0x00	; 0
      004343 00                    3020 	.db #0x00	; 0
      004344 00                    3021 	.db #0x00	; 0
      004345 00                    3022 	.db #0x00	; 0
      004346 00                    3023 	.db #0x00	; 0
      004347 00                    3024 	.db #0x00	; 0
      004348 00                    3025 	.db #0x00	; 0
      004349 00                    3026 	.db #0x00	; 0
      00434A 00                    3027 	.db #0x00	; 0
      00434B 00                    3028 	.db #0x00	; 0
      00434C 00                    3029 	.db #0x00	; 0
      00434D 00                    3030 	.db #0x00	; 0
      00434E 00                    3031 	.db #0x00	; 0
      00434F 00                    3032 	.db #0x00	; 0
      004350 00                    3033 	.db #0x00	; 0
      004351 F8                    3034 	.db #0xf8	; 248
      004352 08                    3035 	.db #0x08	; 8
      004353 08                    3036 	.db #0x08	; 8
      004354 08                    3037 	.db #0x08	; 8
      004355 08                    3038 	.db #0x08	; 8
      004356 08                    3039 	.db #0x08	; 8
      004357 08                    3040 	.db #0x08	; 8
      004358 08                    3041 	.db #0x08	; 8
      004359 00                    3042 	.db #0x00	; 0
      00435A F8                    3043 	.db #0xf8	; 248
      00435B 18                    3044 	.db #0x18	; 24
      00435C 60                    3045 	.db #0x60	; 96
      00435D 80                    3046 	.db #0x80	; 128
      00435E 00                    3047 	.db #0x00	; 0
      00435F 00                    3048 	.db #0x00	; 0
      004360 00                    3049 	.db #0x00	; 0
      004361 80                    3050 	.db #0x80	; 128
      004362 60                    3051 	.db #0x60	; 96
      004363 18                    3052 	.db #0x18	; 24
      004364 F8                    3053 	.db #0xf8	; 248
      004365 00                    3054 	.db #0x00	; 0
      004366 00                    3055 	.db #0x00	; 0
      004367 00                    3056 	.db #0x00	; 0
      004368 20                    3057 	.db #0x20	; 32
      004369 20                    3058 	.db #0x20	; 32
      00436A F8                    3059 	.db #0xf8	; 248
      00436B 00                    3060 	.db #0x00	; 0
      00436C 00                    3061 	.db #0x00	; 0
      00436D 00                    3062 	.db #0x00	; 0
      00436E 00                    3063 	.db #0x00	; 0
      00436F 00                    3064 	.db #0x00	; 0
      004370 00                    3065 	.db #0x00	; 0
      004371 E0                    3066 	.db #0xe0	; 224
      004372 10                    3067 	.db #0x10	; 16
      004373 08                    3068 	.db #0x08	; 8
      004374 08                    3069 	.db #0x08	; 8
      004375 08                    3070 	.db #0x08	; 8
      004376 08                    3071 	.db #0x08	; 8
      004377 10                    3072 	.db #0x10	; 16
      004378 E0                    3073 	.db #0xe0	; 224
      004379 00                    3074 	.db #0x00	; 0
      00437A 00                    3075 	.db #0x00	; 0
      00437B 00                    3076 	.db #0x00	; 0
      00437C 20                    3077 	.db #0x20	; 32
      00437D 20                    3078 	.db #0x20	; 32
      00437E F8                    3079 	.db #0xf8	; 248
      00437F 00                    3080 	.db #0x00	; 0
      004380 00                    3081 	.db #0x00	; 0
      004381 00                    3082 	.db #0x00	; 0
      004382 00                    3083 	.db #0x00	; 0
      004383 00                    3084 	.db #0x00	; 0
      004384 00                    3085 	.db #0x00	; 0
      004385 00                    3086 	.db #0x00	; 0
      004386 00                    3087 	.db #0x00	; 0
      004387 00                    3088 	.db #0x00	; 0
      004388 00                    3089 	.db #0x00	; 0
      004389 00                    3090 	.db #0x00	; 0
      00438A 00                    3091 	.db #0x00	; 0
      00438B 08                    3092 	.db #0x08	; 8
      00438C 08                    3093 	.db #0x08	; 8
      00438D 08                    3094 	.db #0x08	; 8
      00438E 08                    3095 	.db #0x08	; 8
      00438F 08                    3096 	.db #0x08	; 8
      004390 88                    3097 	.db #0x88	; 136
      004391 68                    3098 	.db #0x68	; 104	'h'
      004392 18                    3099 	.db #0x18	; 24
      004393 00                    3100 	.db #0x00	; 0
      004394 00                    3101 	.db #0x00	; 0
      004395 00                    3102 	.db #0x00	; 0
      004396 00                    3103 	.db #0x00	; 0
      004397 00                    3104 	.db #0x00	; 0
      004398 00                    3105 	.db #0x00	; 0
      004399 00                    3106 	.db #0x00	; 0
      00439A 00                    3107 	.db #0x00	; 0
      00439B 00                    3108 	.db #0x00	; 0
      00439C 00                    3109 	.db #0x00	; 0
      00439D 00                    3110 	.db #0x00	; 0
      00439E 00                    3111 	.db #0x00	; 0
      00439F 00                    3112 	.db #0x00	; 0
      0043A0 00                    3113 	.db #0x00	; 0
      0043A1 00                    3114 	.db #0x00	; 0
      0043A2 00                    3115 	.db #0x00	; 0
      0043A3 00                    3116 	.db #0x00	; 0
      0043A4 00                    3117 	.db #0x00	; 0
      0043A5 00                    3118 	.db #0x00	; 0
      0043A6 00                    3119 	.db #0x00	; 0
      0043A7 00                    3120 	.db #0x00	; 0
      0043A8 00                    3121 	.db #0x00	; 0
      0043A9 00                    3122 	.db #0x00	; 0
      0043AA 00                    3123 	.db #0x00	; 0
      0043AB 00                    3124 	.db #0x00	; 0
      0043AC 00                    3125 	.db #0x00	; 0
      0043AD 00                    3126 	.db #0x00	; 0
      0043AE 00                    3127 	.db #0x00	; 0
      0043AF 00                    3128 	.db #0x00	; 0
      0043B0 00                    3129 	.db #0x00	; 0
      0043B1 00                    3130 	.db #0x00	; 0
      0043B2 00                    3131 	.db #0x00	; 0
      0043B3 00                    3132 	.db #0x00	; 0
      0043B4 00                    3133 	.db #0x00	; 0
      0043B5 00                    3134 	.db #0x00	; 0
      0043B6 00                    3135 	.db #0x00	; 0
      0043B7 00                    3136 	.db #0x00	; 0
      0043B8 00                    3137 	.db #0x00	; 0
      0043B9 00                    3138 	.db #0x00	; 0
      0043BA 00                    3139 	.db #0x00	; 0
      0043BB 00                    3140 	.db #0x00	; 0
      0043BC 00                    3141 	.db #0x00	; 0
      0043BD 00                    3142 	.db #0x00	; 0
      0043BE 00                    3143 	.db #0x00	; 0
      0043BF 00                    3144 	.db #0x00	; 0
      0043C0 00                    3145 	.db #0x00	; 0
      0043C1 00                    3146 	.db #0x00	; 0
      0043C2 00                    3147 	.db #0x00	; 0
      0043C3 00                    3148 	.db #0x00	; 0
      0043C4 00                    3149 	.db #0x00	; 0
      0043C5 00                    3150 	.db #0x00	; 0
      0043C6 00                    3151 	.db #0x00	; 0
      0043C7 00                    3152 	.db #0x00	; 0
      0043C8 00                    3153 	.db #0x00	; 0
      0043C9 00                    3154 	.db #0x00	; 0
      0043CA 00                    3155 	.db #0x00	; 0
      0043CB 00                    3156 	.db #0x00	; 0
      0043CC 00                    3157 	.db #0x00	; 0
      0043CD 00                    3158 	.db #0x00	; 0
      0043CE 00                    3159 	.db #0x00	; 0
      0043CF 00                    3160 	.db #0x00	; 0
      0043D0 00                    3161 	.db #0x00	; 0
      0043D1 7F                    3162 	.db #0x7f	; 127
      0043D2 01                    3163 	.db #0x01	; 1
      0043D3 01                    3164 	.db #0x01	; 1
      0043D4 01                    3165 	.db #0x01	; 1
      0043D5 01                    3166 	.db #0x01	; 1
      0043D6 01                    3167 	.db #0x01	; 1
      0043D7 01                    3168 	.db #0x01	; 1
      0043D8 00                    3169 	.db #0x00	; 0
      0043D9 00                    3170 	.db #0x00	; 0
      0043DA 7F                    3171 	.db #0x7f	; 127
      0043DB 00                    3172 	.db #0x00	; 0
      0043DC 00                    3173 	.db #0x00	; 0
      0043DD 01                    3174 	.db #0x01	; 1
      0043DE 06                    3175 	.db #0x06	; 6
      0043DF 18                    3176 	.db #0x18	; 24
      0043E0 06                    3177 	.db #0x06	; 6
      0043E1 01                    3178 	.db #0x01	; 1
      0043E2 00                    3179 	.db #0x00	; 0
      0043E3 00                    3180 	.db #0x00	; 0
      0043E4 7F                    3181 	.db #0x7f	; 127
      0043E5 00                    3182 	.db #0x00	; 0
      0043E6 00                    3183 	.db #0x00	; 0
      0043E7 00                    3184 	.db #0x00	; 0
      0043E8 40                    3185 	.db #0x40	; 64
      0043E9 40                    3186 	.db #0x40	; 64
      0043EA 7F                    3187 	.db #0x7f	; 127
      0043EB 40                    3188 	.db #0x40	; 64
      0043EC 40                    3189 	.db #0x40	; 64
      0043ED 00                    3190 	.db #0x00	; 0
      0043EE 00                    3191 	.db #0x00	; 0
      0043EF 00                    3192 	.db #0x00	; 0
      0043F0 00                    3193 	.db #0x00	; 0
      0043F1 1F                    3194 	.db #0x1f	; 31
      0043F2 20                    3195 	.db #0x20	; 32
      0043F3 40                    3196 	.db #0x40	; 64
      0043F4 40                    3197 	.db #0x40	; 64
      0043F5 40                    3198 	.db #0x40	; 64
      0043F6 40                    3199 	.db #0x40	; 64
      0043F7 20                    3200 	.db #0x20	; 32
      0043F8 1F                    3201 	.db #0x1f	; 31
      0043F9 00                    3202 	.db #0x00	; 0
      0043FA 00                    3203 	.db #0x00	; 0
      0043FB 00                    3204 	.db #0x00	; 0
      0043FC 40                    3205 	.db #0x40	; 64
      0043FD 40                    3206 	.db #0x40	; 64
      0043FE 7F                    3207 	.db #0x7f	; 127
      0043FF 40                    3208 	.db #0x40	; 64
      004400 40                    3209 	.db #0x40	; 64
      004401 00                    3210 	.db #0x00	; 0
      004402 00                    3211 	.db #0x00	; 0
      004403 00                    3212 	.db #0x00	; 0
      004404 00                    3213 	.db #0x00	; 0
      004405 00                    3214 	.db #0x00	; 0
      004406 60                    3215 	.db #0x60	; 96
      004407 00                    3216 	.db #0x00	; 0
      004408 00                    3217 	.db #0x00	; 0
      004409 00                    3218 	.db #0x00	; 0
      00440A 00                    3219 	.db #0x00	; 0
      00440B 00                    3220 	.db #0x00	; 0
      00440C 00                    3221 	.db #0x00	; 0
      00440D 60                    3222 	.db #0x60	; 96
      00440E 18                    3223 	.db #0x18	; 24
      00440F 06                    3224 	.db #0x06	; 6
      004410 01                    3225 	.db #0x01	; 1
      004411 00                    3226 	.db #0x00	; 0
      004412 00                    3227 	.db #0x00	; 0
      004413 00                    3228 	.db #0x00	; 0
      004414 00                    3229 	.db #0x00	; 0
      004415 00                    3230 	.db #0x00	; 0
      004416 00                    3231 	.db #0x00	; 0
      004417 00                    3232 	.db #0x00	; 0
      004418 00                    3233 	.db #0x00	; 0
      004419 00                    3234 	.db #0x00	; 0
      00441A 00                    3235 	.db #0x00	; 0
      00441B 00                    3236 	.db #0x00	; 0
      00441C 00                    3237 	.db #0x00	; 0
      00441D 00                    3238 	.db #0x00	; 0
      00441E 00                    3239 	.db #0x00	; 0
      00441F 00                    3240 	.db #0x00	; 0
      004420 00                    3241 	.db #0x00	; 0
      004421 00                    3242 	.db #0x00	; 0
      004422 00                    3243 	.db #0x00	; 0
      004423 00                    3244 	.db #0x00	; 0
      004424 00                    3245 	.db #0x00	; 0
      004425 00                    3246 	.db #0x00	; 0
      004426 00                    3247 	.db #0x00	; 0
      004427 00                    3248 	.db #0x00	; 0
      004428 00                    3249 	.db #0x00	; 0
      004429 00                    3250 	.db #0x00	; 0
      00442A 00                    3251 	.db #0x00	; 0
      00442B 00                    3252 	.db #0x00	; 0
      00442C 00                    3253 	.db #0x00	; 0
      00442D 00                    3254 	.db #0x00	; 0
      00442E 00                    3255 	.db #0x00	; 0
      00442F 00                    3256 	.db #0x00	; 0
      004430 00                    3257 	.db #0x00	; 0
      004431 00                    3258 	.db #0x00	; 0
      004432 00                    3259 	.db #0x00	; 0
      004433 00                    3260 	.db #0x00	; 0
      004434 00                    3261 	.db #0x00	; 0
      004435 00                    3262 	.db #0x00	; 0
      004436 00                    3263 	.db #0x00	; 0
      004437 00                    3264 	.db #0x00	; 0
      004438 00                    3265 	.db #0x00	; 0
      004439 00                    3266 	.db #0x00	; 0
      00443A 00                    3267 	.db #0x00	; 0
      00443B 00                    3268 	.db #0x00	; 0
      00443C 00                    3269 	.db #0x00	; 0
      00443D 00                    3270 	.db #0x00	; 0
      00443E 00                    3271 	.db #0x00	; 0
      00443F 00                    3272 	.db #0x00	; 0
      004440 00                    3273 	.db #0x00	; 0
      004441 00                    3274 	.db #0x00	; 0
      004442 00                    3275 	.db #0x00	; 0
      004443 00                    3276 	.db #0x00	; 0
      004444 00                    3277 	.db #0x00	; 0
      004445 00                    3278 	.db #0x00	; 0
      004446 00                    3279 	.db #0x00	; 0
      004447 00                    3280 	.db #0x00	; 0
      004448 00                    3281 	.db #0x00	; 0
      004449 00                    3282 	.db #0x00	; 0
      00444A 00                    3283 	.db #0x00	; 0
      00444B 00                    3284 	.db #0x00	; 0
      00444C 00                    3285 	.db #0x00	; 0
      00444D 00                    3286 	.db #0x00	; 0
      00444E 00                    3287 	.db #0x00	; 0
      00444F 00                    3288 	.db #0x00	; 0
      004450 00                    3289 	.db #0x00	; 0
      004451 00                    3290 	.db #0x00	; 0
      004452 00                    3291 	.db #0x00	; 0
      004453 00                    3292 	.db #0x00	; 0
      004454 00                    3293 	.db #0x00	; 0
      004455 00                    3294 	.db #0x00	; 0
      004456 00                    3295 	.db #0x00	; 0
      004457 40                    3296 	.db #0x40	; 64
      004458 20                    3297 	.db #0x20	; 32
      004459 20                    3298 	.db #0x20	; 32
      00445A 20                    3299 	.db #0x20	; 32
      00445B C0                    3300 	.db #0xc0	; 192
      00445C 00                    3301 	.db #0x00	; 0
      00445D 00                    3302 	.db #0x00	; 0
      00445E E0                    3303 	.db #0xe0	; 224
      00445F 20                    3304 	.db #0x20	; 32
      004460 20                    3305 	.db #0x20	; 32
      004461 20                    3306 	.db #0x20	; 32
      004462 E0                    3307 	.db #0xe0	; 224
      004463 00                    3308 	.db #0x00	; 0
      004464 00                    3309 	.db #0x00	; 0
      004465 00                    3310 	.db #0x00	; 0
      004466 40                    3311 	.db #0x40	; 64
      004467 E0                    3312 	.db #0xe0	; 224
      004468 00                    3313 	.db #0x00	; 0
      004469 00                    3314 	.db #0x00	; 0
      00446A 00                    3315 	.db #0x00	; 0
      00446B 00                    3316 	.db #0x00	; 0
      00446C 60                    3317 	.db #0x60	; 96
      00446D 20                    3318 	.db #0x20	; 32
      00446E 20                    3319 	.db #0x20	; 32
      00446F 20                    3320 	.db #0x20	; 32
      004470 E0                    3321 	.db #0xe0	; 224
      004471 00                    3322 	.db #0x00	; 0
      004472 00                    3323 	.db #0x00	; 0
      004473 00                    3324 	.db #0x00	; 0
      004474 00                    3325 	.db #0x00	; 0
      004475 00                    3326 	.db #0x00	; 0
      004476 E0                    3327 	.db #0xe0	; 224
      004477 20                    3328 	.db #0x20	; 32
      004478 20                    3329 	.db #0x20	; 32
      004479 20                    3330 	.db #0x20	; 32
      00447A E0                    3331 	.db #0xe0	; 224
      00447B 00                    3332 	.db #0x00	; 0
      00447C 00                    3333 	.db #0x00	; 0
      00447D 00                    3334 	.db #0x00	; 0
      00447E 00                    3335 	.db #0x00	; 0
      00447F 00                    3336 	.db #0x00	; 0
      004480 40                    3337 	.db #0x40	; 64
      004481 20                    3338 	.db #0x20	; 32
      004482 20                    3339 	.db #0x20	; 32
      004483 20                    3340 	.db #0x20	; 32
      004484 C0                    3341 	.db #0xc0	; 192
      004485 00                    3342 	.db #0x00	; 0
      004486 00                    3343 	.db #0x00	; 0
      004487 40                    3344 	.db #0x40	; 64
      004488 20                    3345 	.db #0x20	; 32
      004489 20                    3346 	.db #0x20	; 32
      00448A 20                    3347 	.db #0x20	; 32
      00448B C0                    3348 	.db #0xc0	; 192
      00448C 00                    3349 	.db #0x00	; 0
      00448D 00                    3350 	.db #0x00	; 0
      00448E 00                    3351 	.db #0x00	; 0
      00448F 00                    3352 	.db #0x00	; 0
      004490 00                    3353 	.db #0x00	; 0
      004491 00                    3354 	.db #0x00	; 0
      004492 00                    3355 	.db #0x00	; 0
      004493 00                    3356 	.db #0x00	; 0
      004494 00                    3357 	.db #0x00	; 0
      004495 00                    3358 	.db #0x00	; 0
      004496 00                    3359 	.db #0x00	; 0
      004497 00                    3360 	.db #0x00	; 0
      004498 00                    3361 	.db #0x00	; 0
      004499 00                    3362 	.db #0x00	; 0
      00449A 00                    3363 	.db #0x00	; 0
      00449B 00                    3364 	.db #0x00	; 0
      00449C 00                    3365 	.db #0x00	; 0
      00449D 00                    3366 	.db #0x00	; 0
      00449E 00                    3367 	.db #0x00	; 0
      00449F 00                    3368 	.db #0x00	; 0
      0044A0 00                    3369 	.db #0x00	; 0
      0044A1 00                    3370 	.db #0x00	; 0
      0044A2 00                    3371 	.db #0x00	; 0
      0044A3 00                    3372 	.db #0x00	; 0
      0044A4 00                    3373 	.db #0x00	; 0
      0044A5 00                    3374 	.db #0x00	; 0
      0044A6 00                    3375 	.db #0x00	; 0
      0044A7 00                    3376 	.db #0x00	; 0
      0044A8 00                    3377 	.db #0x00	; 0
      0044A9 00                    3378 	.db #0x00	; 0
      0044AA 00                    3379 	.db #0x00	; 0
      0044AB 00                    3380 	.db #0x00	; 0
      0044AC 00                    3381 	.db #0x00	; 0
      0044AD 00                    3382 	.db #0x00	; 0
      0044AE 00                    3383 	.db #0x00	; 0
      0044AF 00                    3384 	.db #0x00	; 0
      0044B0 00                    3385 	.db #0x00	; 0
      0044B1 00                    3386 	.db #0x00	; 0
      0044B2 00                    3387 	.db #0x00	; 0
      0044B3 00                    3388 	.db #0x00	; 0
      0044B4 00                    3389 	.db #0x00	; 0
      0044B5 00                    3390 	.db #0x00	; 0
      0044B6 00                    3391 	.db #0x00	; 0
      0044B7 00                    3392 	.db #0x00	; 0
      0044B8 00                    3393 	.db #0x00	; 0
      0044B9 00                    3394 	.db #0x00	; 0
      0044BA 00                    3395 	.db #0x00	; 0
      0044BB 00                    3396 	.db #0x00	; 0
      0044BC 00                    3397 	.db #0x00	; 0
      0044BD 00                    3398 	.db #0x00	; 0
      0044BE 00                    3399 	.db #0x00	; 0
      0044BF 00                    3400 	.db #0x00	; 0
      0044C0 00                    3401 	.db #0x00	; 0
      0044C1 00                    3402 	.db #0x00	; 0
      0044C2 00                    3403 	.db #0x00	; 0
      0044C3 00                    3404 	.db #0x00	; 0
      0044C4 00                    3405 	.db #0x00	; 0
      0044C5 00                    3406 	.db #0x00	; 0
      0044C6 00                    3407 	.db #0x00	; 0
      0044C7 00                    3408 	.db #0x00	; 0
      0044C8 00                    3409 	.db #0x00	; 0
      0044C9 00                    3410 	.db #0x00	; 0
      0044CA 00                    3411 	.db #0x00	; 0
      0044CB 00                    3412 	.db #0x00	; 0
      0044CC 00                    3413 	.db #0x00	; 0
      0044CD 00                    3414 	.db #0x00	; 0
      0044CE 00                    3415 	.db #0x00	; 0
      0044CF 00                    3416 	.db #0x00	; 0
      0044D0 00                    3417 	.db #0x00	; 0
      0044D1 00                    3418 	.db #0x00	; 0
      0044D2 00                    3419 	.db #0x00	; 0
      0044D3 00                    3420 	.db #0x00	; 0
      0044D4 00                    3421 	.db #0x00	; 0
      0044D5 00                    3422 	.db #0x00	; 0
      0044D6 00                    3423 	.db #0x00	; 0
      0044D7 0C                    3424 	.db #0x0c	; 12
      0044D8 0A                    3425 	.db #0x0a	; 10
      0044D9 0A                    3426 	.db #0x0a	; 10
      0044DA 09                    3427 	.db #0x09	; 9
      0044DB 0C                    3428 	.db #0x0c	; 12
      0044DC 00                    3429 	.db #0x00	; 0
      0044DD 00                    3430 	.db #0x00	; 0
      0044DE 0F                    3431 	.db #0x0f	; 15
      0044DF 08                    3432 	.db #0x08	; 8
      0044E0 08                    3433 	.db #0x08	; 8
      0044E1 08                    3434 	.db #0x08	; 8
      0044E2 0F                    3435 	.db #0x0f	; 15
      0044E3 00                    3436 	.db #0x00	; 0
      0044E4 00                    3437 	.db #0x00	; 0
      0044E5 00                    3438 	.db #0x00	; 0
      0044E6 08                    3439 	.db #0x08	; 8
      0044E7 0F                    3440 	.db #0x0f	; 15
      0044E8 08                    3441 	.db #0x08	; 8
      0044E9 00                    3442 	.db #0x00	; 0
      0044EA 00                    3443 	.db #0x00	; 0
      0044EB 00                    3444 	.db #0x00	; 0
      0044EC 0C                    3445 	.db #0x0c	; 12
      0044ED 08                    3446 	.db #0x08	; 8
      0044EE 09                    3447 	.db #0x09	; 9
      0044EF 09                    3448 	.db #0x09	; 9
      0044F0 0E                    3449 	.db #0x0e	; 14
      0044F1 00                    3450 	.db #0x00	; 0
      0044F2 00                    3451 	.db #0x00	; 0
      0044F3 0C                    3452 	.db #0x0c	; 12
      0044F4 00                    3453 	.db #0x00	; 0
      0044F5 00                    3454 	.db #0x00	; 0
      0044F6 0F                    3455 	.db #0x0f	; 15
      0044F7 09                    3456 	.db #0x09	; 9
      0044F8 09                    3457 	.db #0x09	; 9
      0044F9 09                    3458 	.db #0x09	; 9
      0044FA 0F                    3459 	.db #0x0f	; 15
      0044FB 00                    3460 	.db #0x00	; 0
      0044FC 00                    3461 	.db #0x00	; 0
      0044FD 0C                    3462 	.db #0x0c	; 12
      0044FE 00                    3463 	.db #0x00	; 0
      0044FF 00                    3464 	.db #0x00	; 0
      004500 0C                    3465 	.db #0x0c	; 12
      004501 0A                    3466 	.db #0x0a	; 10
      004502 0A                    3467 	.db #0x0a	; 10
      004503 09                    3468 	.db #0x09	; 9
      004504 0C                    3469 	.db #0x0c	; 12
      004505 00                    3470 	.db #0x00	; 0
      004506 00                    3471 	.db #0x00	; 0
      004507 0C                    3472 	.db #0x0c	; 12
      004508 0A                    3473 	.db #0x0a	; 10
      004509 0A                    3474 	.db #0x0a	; 10
      00450A 09                    3475 	.db #0x09	; 9
      00450B 0C                    3476 	.db #0x0c	; 12
      00450C 00                    3477 	.db #0x00	; 0
      00450D 00                    3478 	.db #0x00	; 0
      00450E 00                    3479 	.db #0x00	; 0
      00450F 00                    3480 	.db #0x00	; 0
      004510 00                    3481 	.db #0x00	; 0
      004511 00                    3482 	.db #0x00	; 0
      004512 00                    3483 	.db #0x00	; 0
      004513 00                    3484 	.db #0x00	; 0
      004514 00                    3485 	.db #0x00	; 0
      004515 00                    3486 	.db #0x00	; 0
      004516 00                    3487 	.db #0x00	; 0
      004517 00                    3488 	.db #0x00	; 0
      004518 00                    3489 	.db #0x00	; 0
      004519 00                    3490 	.db #0x00	; 0
      00451A 00                    3491 	.db #0x00	; 0
      00451B 00                    3492 	.db #0x00	; 0
      00451C 00                    3493 	.db #0x00	; 0
      00451D 00                    3494 	.db #0x00	; 0
      00451E 00                    3495 	.db #0x00	; 0
      00451F 00                    3496 	.db #0x00	; 0
      004520 00                    3497 	.db #0x00	; 0
      004521 00                    3498 	.db #0x00	; 0
      004522 00                    3499 	.db #0x00	; 0
      004523 00                    3500 	.db #0x00	; 0
      004524 00                    3501 	.db #0x00	; 0
      004525 00                    3502 	.db #0x00	; 0
      004526 00                    3503 	.db #0x00	; 0
      004527 00                    3504 	.db #0x00	; 0
      004528 00                    3505 	.db #0x00	; 0
      004529 00                    3506 	.db #0x00	; 0
      00452A 00                    3507 	.db #0x00	; 0
      00452B 00                    3508 	.db #0x00	; 0
      00452C 00                    3509 	.db #0x00	; 0
      00452D 00                    3510 	.db #0x00	; 0
      00452E 00                    3511 	.db #0x00	; 0
      00452F 00                    3512 	.db #0x00	; 0
      004530 00                    3513 	.db #0x00	; 0
      004531 00                    3514 	.db #0x00	; 0
      004532 00                    3515 	.db #0x00	; 0
      004533 00                    3516 	.db #0x00	; 0
      004534 00                    3517 	.db #0x00	; 0
      004535 00                    3518 	.db #0x00	; 0
      004536 00                    3519 	.db #0x00	; 0
      004537 00                    3520 	.db #0x00	; 0
      004538 00                    3521 	.db #0x00	; 0
      004539 00                    3522 	.db #0x00	; 0
      00453A 00                    3523 	.db #0x00	; 0
      00453B 00                    3524 	.db #0x00	; 0
      00453C 00                    3525 	.db #0x00	; 0
      00453D 00                    3526 	.db #0x00	; 0
      00453E 00                    3527 	.db #0x00	; 0
      00453F 00                    3528 	.db #0x00	; 0
      004540 00                    3529 	.db #0x00	; 0
      004541 00                    3530 	.db #0x00	; 0
      004542 00                    3531 	.db #0x00	; 0
      004543 00                    3532 	.db #0x00	; 0
      004544 00                    3533 	.db #0x00	; 0
      004545 00                    3534 	.db #0x00	; 0
      004546 00                    3535 	.db #0x00	; 0
      004547 00                    3536 	.db #0x00	; 0
      004548 00                    3537 	.db #0x00	; 0
      004549 00                    3538 	.db #0x00	; 0
      00454A 00                    3539 	.db #0x00	; 0
      00454B 00                    3540 	.db #0x00	; 0
      00454C 00                    3541 	.db #0x00	; 0
      00454D 00                    3542 	.db #0x00	; 0
      00454E 00                    3543 	.db #0x00	; 0
      00454F 00                    3544 	.db #0x00	; 0
      004550 00                    3545 	.db #0x00	; 0
      004551 00                    3546 	.db #0x00	; 0
      004552 00                    3547 	.db #0x00	; 0
      004553 00                    3548 	.db #0x00	; 0
      004554 00                    3549 	.db #0x00	; 0
      004555 00                    3550 	.db #0x00	; 0
      004556 00                    3551 	.db #0x00	; 0
      004557 00                    3552 	.db #0x00	; 0
      004558 00                    3553 	.db #0x00	; 0
      004559 00                    3554 	.db #0x00	; 0
      00455A 00                    3555 	.db #0x00	; 0
      00455B 00                    3556 	.db #0x00	; 0
      00455C 00                    3557 	.db #0x00	; 0
      00455D 00                    3558 	.db #0x00	; 0
      00455E 00                    3559 	.db #0x00	; 0
      00455F 00                    3560 	.db #0x00	; 0
      004560 00                    3561 	.db #0x00	; 0
      004561 00                    3562 	.db #0x00	; 0
      004562 00                    3563 	.db #0x00	; 0
      004563 00                    3564 	.db #0x00	; 0
      004564 00                    3565 	.db #0x00	; 0
      004565 00                    3566 	.db #0x00	; 0
      004566 00                    3567 	.db #0x00	; 0
      004567 00                    3568 	.db #0x00	; 0
      004568 00                    3569 	.db #0x00	; 0
      004569 00                    3570 	.db #0x00	; 0
      00456A 00                    3571 	.db #0x00	; 0
      00456B 00                    3572 	.db #0x00	; 0
      00456C 00                    3573 	.db #0x00	; 0
      00456D 00                    3574 	.db #0x00	; 0
      00456E 80                    3575 	.db #0x80	; 128
      00456F 80                    3576 	.db #0x80	; 128
      004570 80                    3577 	.db #0x80	; 128
      004571 80                    3578 	.db #0x80	; 128
      004572 80                    3579 	.db #0x80	; 128
      004573 80                    3580 	.db #0x80	; 128
      004574 80                    3581 	.db #0x80	; 128
      004575 80                    3582 	.db #0x80	; 128
      004576 00                    3583 	.db #0x00	; 0
      004577 00                    3584 	.db #0x00	; 0
      004578 00                    3585 	.db #0x00	; 0
      004579 00                    3586 	.db #0x00	; 0
      00457A 00                    3587 	.db #0x00	; 0
      00457B 00                    3588 	.db #0x00	; 0
      00457C 00                    3589 	.db #0x00	; 0
      00457D 00                    3590 	.db #0x00	; 0
      00457E 00                    3591 	.db #0x00	; 0
      00457F 00                    3592 	.db #0x00	; 0
      004580 00                    3593 	.db #0x00	; 0
      004581 00                    3594 	.db #0x00	; 0
      004582 00                    3595 	.db #0x00	; 0
      004583 00                    3596 	.db #0x00	; 0
      004584 00                    3597 	.db #0x00	; 0
      004585 00                    3598 	.db #0x00	; 0
      004586 00                    3599 	.db #0x00	; 0
      004587 00                    3600 	.db #0x00	; 0
      004588 00                    3601 	.db #0x00	; 0
      004589 00                    3602 	.db #0x00	; 0
      00458A 00                    3603 	.db #0x00	; 0
      00458B 00                    3604 	.db #0x00	; 0
      00458C 00                    3605 	.db #0x00	; 0
      00458D 00                    3606 	.db #0x00	; 0
      00458E 00                    3607 	.db #0x00	; 0
      00458F 00                    3608 	.db #0x00	; 0
      004590 00                    3609 	.db #0x00	; 0
      004591 00                    3610 	.db #0x00	; 0
      004592 00                    3611 	.db #0x00	; 0
      004593 00                    3612 	.db #0x00	; 0
      004594 00                    3613 	.db #0x00	; 0
      004595 00                    3614 	.db #0x00	; 0
      004596 00                    3615 	.db #0x00	; 0
      004597 00                    3616 	.db #0x00	; 0
      004598 00                    3617 	.db #0x00	; 0
      004599 00                    3618 	.db #0x00	; 0
      00459A 00                    3619 	.db #0x00	; 0
      00459B 00                    3620 	.db #0x00	; 0
      00459C 00                    3621 	.db #0x00	; 0
      00459D 00                    3622 	.db #0x00	; 0
      00459E 00                    3623 	.db #0x00	; 0
      00459F 00                    3624 	.db #0x00	; 0
      0045A0 00                    3625 	.db #0x00	; 0
      0045A1 00                    3626 	.db #0x00	; 0
      0045A2 00                    3627 	.db #0x00	; 0
      0045A3 00                    3628 	.db #0x00	; 0
      0045A4 00                    3629 	.db #0x00	; 0
      0045A5 00                    3630 	.db #0x00	; 0
      0045A6 00                    3631 	.db #0x00	; 0
      0045A7 00                    3632 	.db #0x00	; 0
      0045A8 00                    3633 	.db #0x00	; 0
      0045A9 00                    3634 	.db #0x00	; 0
      0045AA 00                    3635 	.db #0x00	; 0
      0045AB 00                    3636 	.db #0x00	; 0
      0045AC 00                    3637 	.db #0x00	; 0
      0045AD 00                    3638 	.db #0x00	; 0
      0045AE 00                    3639 	.db #0x00	; 0
      0045AF 00                    3640 	.db #0x00	; 0
      0045B0 00                    3641 	.db #0x00	; 0
      0045B1 00                    3642 	.db #0x00	; 0
      0045B2 00                    3643 	.db #0x00	; 0
      0045B3 7F                    3644 	.db #0x7f	; 127
      0045B4 03                    3645 	.db #0x03	; 3
      0045B5 0C                    3646 	.db #0x0c	; 12
      0045B6 30                    3647 	.db #0x30	; 48	'0'
      0045B7 0C                    3648 	.db #0x0c	; 12
      0045B8 03                    3649 	.db #0x03	; 3
      0045B9 7F                    3650 	.db #0x7f	; 127
      0045BA 00                    3651 	.db #0x00	; 0
      0045BB 00                    3652 	.db #0x00	; 0
      0045BC 38                    3653 	.db #0x38	; 56	'8'
      0045BD 54                    3654 	.db #0x54	; 84	'T'
      0045BE 54                    3655 	.db #0x54	; 84	'T'
      0045BF 58                    3656 	.db #0x58	; 88	'X'
      0045C0 00                    3657 	.db #0x00	; 0
      0045C1 00                    3658 	.db #0x00	; 0
      0045C2 7C                    3659 	.db #0x7c	; 124
      0045C3 04                    3660 	.db #0x04	; 4
      0045C4 04                    3661 	.db #0x04	; 4
      0045C5 78                    3662 	.db #0x78	; 120	'x'
      0045C6 00                    3663 	.db #0x00	; 0
      0045C7 00                    3664 	.db #0x00	; 0
      0045C8 3C                    3665 	.db #0x3c	; 60
      0045C9 40                    3666 	.db #0x40	; 64
      0045CA 40                    3667 	.db #0x40	; 64
      0045CB 7C                    3668 	.db #0x7c	; 124
      0045CC 00                    3669 	.db #0x00	; 0
      0045CD 00                    3670 	.db #0x00	; 0
      0045CE 00                    3671 	.db #0x00	; 0
      0045CF 00                    3672 	.db #0x00	; 0
      0045D0 00                    3673 	.db #0x00	; 0
      0045D1 00                    3674 	.db #0x00	; 0
      0045D2 00                    3675 	.db #0x00	; 0
      0045D3 00                    3676 	.db #0x00	; 0
      0045D4 00                    3677 	.db #0x00	; 0
      0045D5 00                    3678 	.db #0x00	; 0
      0045D6 00                    3679 	.db #0x00	; 0
      0045D7 00                    3680 	.db #0x00	; 0
      0045D8 00                    3681 	.db #0x00	; 0
      0045D9 00                    3682 	.db #0x00	; 0
      0045DA 00                    3683 	.db #0x00	; 0
      0045DB 00                    3684 	.db #0x00	; 0
      0045DC 00                    3685 	.db #0x00	; 0
      0045DD 00                    3686 	.db #0x00	; 0
      0045DE 00                    3687 	.db #0x00	; 0
      0045DF 00                    3688 	.db #0x00	; 0
      0045E0 00                    3689 	.db #0x00	; 0
      0045E1 00                    3690 	.db #0x00	; 0
      0045E2 00                    3691 	.db #0x00	; 0
      0045E3 00                    3692 	.db #0x00	; 0
      0045E4 00                    3693 	.db #0x00	; 0
      0045E5 00                    3694 	.db #0x00	; 0
      0045E6 00                    3695 	.db #0x00	; 0
      0045E7 00                    3696 	.db #0x00	; 0
      0045E8 00                    3697 	.db #0x00	; 0
      0045E9 00                    3698 	.db #0x00	; 0
      0045EA 00                    3699 	.db #0x00	; 0
      0045EB 00                    3700 	.db #0x00	; 0
      0045EC 00                    3701 	.db #0x00	; 0
      0045ED 00                    3702 	.db #0x00	; 0
      0045EE FF                    3703 	.db #0xff	; 255
      0045EF AA                    3704 	.db #0xaa	; 170
      0045F0 AA                    3705 	.db #0xaa	; 170
      0045F1 AA                    3706 	.db #0xaa	; 170
      0045F2 28                    3707 	.db #0x28	; 40
      0045F3 08                    3708 	.db #0x08	; 8
      0045F4 00                    3709 	.db #0x00	; 0
      0045F5 FF                    3710 	.db #0xff	; 255
      0045F6 00                    3711 	.db #0x00	; 0
      0045F7 00                    3712 	.db #0x00	; 0
      0045F8 00                    3713 	.db #0x00	; 0
      0045F9 00                    3714 	.db #0x00	; 0
      0045FA 00                    3715 	.db #0x00	; 0
      0045FB 00                    3716 	.db #0x00	; 0
      0045FC 00                    3717 	.db #0x00	; 0
      0045FD 00                    3718 	.db #0x00	; 0
      0045FE 00                    3719 	.db #0x00	; 0
      0045FF 00                    3720 	.db #0x00	; 0
      004600 00                    3721 	.db #0x00	; 0
      004601 00                    3722 	.db #0x00	; 0
      004602 00                    3723 	.db #0x00	; 0
      004603 00                    3724 	.db #0x00	; 0
      004604 00                    3725 	.db #0x00	; 0
      004605 00                    3726 	.db #0x00	; 0
      004606 00                    3727 	.db #0x00	; 0
      004607 00                    3728 	.db #0x00	; 0
      004608 00                    3729 	.db #0x00	; 0
      004609 00                    3730 	.db #0x00	; 0
      00460A 00                    3731 	.db #0x00	; 0
      00460B 00                    3732 	.db #0x00	; 0
      00460C 00                    3733 	.db #0x00	; 0
      00460D 00                    3734 	.db #0x00	; 0
      00460E 00                    3735 	.db #0x00	; 0
      00460F 00                    3736 	.db #0x00	; 0
      004610 00                    3737 	.db #0x00	; 0
      004611 00                    3738 	.db #0x00	; 0
      004612 00                    3739 	.db #0x00	; 0
      004613 00                    3740 	.db #0x00	; 0
      004614 00                    3741 	.db #0x00	; 0
      004615 00                    3742 	.db #0x00	; 0
      004616 00                    3743 	.db #0x00	; 0
      004617 00                    3744 	.db #0x00	; 0
      004618 00                    3745 	.db #0x00	; 0
      004619 00                    3746 	.db #0x00	; 0
      00461A 00                    3747 	.db #0x00	; 0
      00461B 7F                    3748 	.db #0x7f	; 127
      00461C 03                    3749 	.db #0x03	; 3
      00461D 0C                    3750 	.db #0x0c	; 12
      00461E 30                    3751 	.db #0x30	; 48	'0'
      00461F 0C                    3752 	.db #0x0c	; 12
      004620 03                    3753 	.db #0x03	; 3
      004621 7F                    3754 	.db #0x7f	; 127
      004622 00                    3755 	.db #0x00	; 0
      004623 00                    3756 	.db #0x00	; 0
      004624 26                    3757 	.db #0x26	; 38
      004625 49                    3758 	.db #0x49	; 73	'I'
      004626 49                    3759 	.db #0x49	; 73	'I'
      004627 49                    3760 	.db #0x49	; 73	'I'
      004628 32                    3761 	.db #0x32	; 50	'2'
      004629 00                    3762 	.db #0x00	; 0
      00462A 00                    3763 	.db #0x00	; 0
      00462B 7F                    3764 	.db #0x7f	; 127
      00462C 02                    3765 	.db #0x02	; 2
      00462D 04                    3766 	.db #0x04	; 4
      00462E 08                    3767 	.db #0x08	; 8
      00462F 10                    3768 	.db #0x10	; 16
      004630 7F                    3769 	.db #0x7f	; 127
      004631 00                    3770 	.db #0x00	; 0
      004632                       3771 _fontMatrix_6x8:
      004632 00                    3772 	.db #0x00	; 0
      004633 00                    3773 	.db #0x00	; 0
      004634 00                    3774 	.db #0x00	; 0
      004635 00                    3775 	.db #0x00	; 0
      004636 00                    3776 	.db #0x00	; 0
      004637 00                    3777 	.db #0x00	; 0
      004638 00                    3778 	.db #0x00	; 0
      004639 00                    3779 	.db #0x00	; 0
      00463A 2F                    3780 	.db #0x2f	; 47
      00463B 00                    3781 	.db #0x00	; 0
      00463C 00                    3782 	.db #0x00	; 0
      00463D 00                    3783 	.db #0x00	; 0
      00463E 00                    3784 	.db #0x00	; 0
      00463F 00                    3785 	.db #0x00	; 0
      004640 07                    3786 	.db #0x07	; 7
      004641 00                    3787 	.db #0x00	; 0
      004642 07                    3788 	.db #0x07	; 7
      004643 00                    3789 	.db #0x00	; 0
      004644 00                    3790 	.db #0x00	; 0
      004645 14                    3791 	.db #0x14	; 20
      004646 7F                    3792 	.db #0x7f	; 127
      004647 14                    3793 	.db #0x14	; 20
      004648 7F                    3794 	.db #0x7f	; 127
      004649 14                    3795 	.db #0x14	; 20
      00464A 00                    3796 	.db #0x00	; 0
      00464B 24                    3797 	.db #0x24	; 36
      00464C 2A                    3798 	.db #0x2a	; 42
      00464D 7F                    3799 	.db #0x7f	; 127
      00464E 2A                    3800 	.db #0x2a	; 42
      00464F 12                    3801 	.db #0x12	; 18
      004650 00                    3802 	.db #0x00	; 0
      004651 62                    3803 	.db #0x62	; 98	'b'
      004652 64                    3804 	.db #0x64	; 100	'd'
      004653 08                    3805 	.db #0x08	; 8
      004654 13                    3806 	.db #0x13	; 19
      004655 23                    3807 	.db #0x23	; 35
      004656 00                    3808 	.db #0x00	; 0
      004657 36                    3809 	.db #0x36	; 54	'6'
      004658 49                    3810 	.db #0x49	; 73	'I'
      004659 55                    3811 	.db #0x55	; 85	'U'
      00465A 22                    3812 	.db #0x22	; 34
      00465B 50                    3813 	.db #0x50	; 80	'P'
      00465C 00                    3814 	.db #0x00	; 0
      00465D 00                    3815 	.db #0x00	; 0
      00465E 05                    3816 	.db #0x05	; 5
      00465F 03                    3817 	.db #0x03	; 3
      004660 00                    3818 	.db #0x00	; 0
      004661 00                    3819 	.db #0x00	; 0
      004662 00                    3820 	.db #0x00	; 0
      004663 00                    3821 	.db #0x00	; 0
      004664 1C                    3822 	.db #0x1c	; 28
      004665 22                    3823 	.db #0x22	; 34
      004666 41                    3824 	.db #0x41	; 65	'A'
      004667 00                    3825 	.db #0x00	; 0
      004668 00                    3826 	.db #0x00	; 0
      004669 00                    3827 	.db #0x00	; 0
      00466A 41                    3828 	.db #0x41	; 65	'A'
      00466B 22                    3829 	.db #0x22	; 34
      00466C 1C                    3830 	.db #0x1c	; 28
      00466D 00                    3831 	.db #0x00	; 0
      00466E 00                    3832 	.db #0x00	; 0
      00466F 14                    3833 	.db #0x14	; 20
      004670 08                    3834 	.db #0x08	; 8
      004671 3E                    3835 	.db #0x3e	; 62
      004672 08                    3836 	.db #0x08	; 8
      004673 14                    3837 	.db #0x14	; 20
      004674 00                    3838 	.db #0x00	; 0
      004675 08                    3839 	.db #0x08	; 8
      004676 08                    3840 	.db #0x08	; 8
      004677 3E                    3841 	.db #0x3e	; 62
      004678 08                    3842 	.db #0x08	; 8
      004679 08                    3843 	.db #0x08	; 8
      00467A 00                    3844 	.db #0x00	; 0
      00467B 00                    3845 	.db #0x00	; 0
      00467C 00                    3846 	.db #0x00	; 0
      00467D A0                    3847 	.db #0xa0	; 160
      00467E 60                    3848 	.db #0x60	; 96
      00467F 00                    3849 	.db #0x00	; 0
      004680 00                    3850 	.db #0x00	; 0
      004681 08                    3851 	.db #0x08	; 8
      004682 08                    3852 	.db #0x08	; 8
      004683 08                    3853 	.db #0x08	; 8
      004684 08                    3854 	.db #0x08	; 8
      004685 08                    3855 	.db #0x08	; 8
      004686 00                    3856 	.db #0x00	; 0
      004687 00                    3857 	.db #0x00	; 0
      004688 60                    3858 	.db #0x60	; 96
      004689 60                    3859 	.db #0x60	; 96
      00468A 00                    3860 	.db #0x00	; 0
      00468B 00                    3861 	.db #0x00	; 0
      00468C 00                    3862 	.db #0x00	; 0
      00468D 20                    3863 	.db #0x20	; 32
      00468E 10                    3864 	.db #0x10	; 16
      00468F 08                    3865 	.db #0x08	; 8
      004690 04                    3866 	.db #0x04	; 4
      004691 02                    3867 	.db #0x02	; 2
      004692 00                    3868 	.db #0x00	; 0
      004693 3E                    3869 	.db #0x3e	; 62
      004694 51                    3870 	.db #0x51	; 81	'Q'
      004695 49                    3871 	.db #0x49	; 73	'I'
      004696 45                    3872 	.db #0x45	; 69	'E'
      004697 3E                    3873 	.db #0x3e	; 62
      004698 00                    3874 	.db #0x00	; 0
      004699 00                    3875 	.db #0x00	; 0
      00469A 42                    3876 	.db #0x42	; 66	'B'
      00469B 7F                    3877 	.db #0x7f	; 127
      00469C 40                    3878 	.db #0x40	; 64
      00469D 00                    3879 	.db #0x00	; 0
      00469E 00                    3880 	.db #0x00	; 0
      00469F 42                    3881 	.db #0x42	; 66	'B'
      0046A0 61                    3882 	.db #0x61	; 97	'a'
      0046A1 51                    3883 	.db #0x51	; 81	'Q'
      0046A2 49                    3884 	.db #0x49	; 73	'I'
      0046A3 46                    3885 	.db #0x46	; 70	'F'
      0046A4 00                    3886 	.db #0x00	; 0
      0046A5 21                    3887 	.db #0x21	; 33
      0046A6 41                    3888 	.db #0x41	; 65	'A'
      0046A7 45                    3889 	.db #0x45	; 69	'E'
      0046A8 4B                    3890 	.db #0x4b	; 75	'K'
      0046A9 31                    3891 	.db #0x31	; 49	'1'
      0046AA 00                    3892 	.db #0x00	; 0
      0046AB 18                    3893 	.db #0x18	; 24
      0046AC 14                    3894 	.db #0x14	; 20
      0046AD 12                    3895 	.db #0x12	; 18
      0046AE 7F                    3896 	.db #0x7f	; 127
      0046AF 10                    3897 	.db #0x10	; 16
      0046B0 00                    3898 	.db #0x00	; 0
      0046B1 27                    3899 	.db #0x27	; 39
      0046B2 45                    3900 	.db #0x45	; 69	'E'
      0046B3 45                    3901 	.db #0x45	; 69	'E'
      0046B4 45                    3902 	.db #0x45	; 69	'E'
      0046B5 39                    3903 	.db #0x39	; 57	'9'
      0046B6 00                    3904 	.db #0x00	; 0
      0046B7 3C                    3905 	.db #0x3c	; 60
      0046B8 4A                    3906 	.db #0x4a	; 74	'J'
      0046B9 49                    3907 	.db #0x49	; 73	'I'
      0046BA 49                    3908 	.db #0x49	; 73	'I'
      0046BB 30                    3909 	.db #0x30	; 48	'0'
      0046BC 00                    3910 	.db #0x00	; 0
      0046BD 01                    3911 	.db #0x01	; 1
      0046BE 71                    3912 	.db #0x71	; 113	'q'
      0046BF 09                    3913 	.db #0x09	; 9
      0046C0 05                    3914 	.db #0x05	; 5
      0046C1 03                    3915 	.db #0x03	; 3
      0046C2 00                    3916 	.db #0x00	; 0
      0046C3 36                    3917 	.db #0x36	; 54	'6'
      0046C4 49                    3918 	.db #0x49	; 73	'I'
      0046C5 49                    3919 	.db #0x49	; 73	'I'
      0046C6 49                    3920 	.db #0x49	; 73	'I'
      0046C7 36                    3921 	.db #0x36	; 54	'6'
      0046C8 00                    3922 	.db #0x00	; 0
      0046C9 06                    3923 	.db #0x06	; 6
      0046CA 49                    3924 	.db #0x49	; 73	'I'
      0046CB 49                    3925 	.db #0x49	; 73	'I'
      0046CC 29                    3926 	.db #0x29	; 41
      0046CD 1E                    3927 	.db #0x1e	; 30
      0046CE 00                    3928 	.db #0x00	; 0
      0046CF 00                    3929 	.db #0x00	; 0
      0046D0 36                    3930 	.db #0x36	; 54	'6'
      0046D1 36                    3931 	.db #0x36	; 54	'6'
      0046D2 00                    3932 	.db #0x00	; 0
      0046D3 00                    3933 	.db #0x00	; 0
      0046D4 00                    3934 	.db #0x00	; 0
      0046D5 00                    3935 	.db #0x00	; 0
      0046D6 56                    3936 	.db #0x56	; 86	'V'
      0046D7 36                    3937 	.db #0x36	; 54	'6'
      0046D8 00                    3938 	.db #0x00	; 0
      0046D9 00                    3939 	.db #0x00	; 0
      0046DA 00                    3940 	.db #0x00	; 0
      0046DB 08                    3941 	.db #0x08	; 8
      0046DC 14                    3942 	.db #0x14	; 20
      0046DD 22                    3943 	.db #0x22	; 34
      0046DE 41                    3944 	.db #0x41	; 65	'A'
      0046DF 00                    3945 	.db #0x00	; 0
      0046E0 00                    3946 	.db #0x00	; 0
      0046E1 14                    3947 	.db #0x14	; 20
      0046E2 14                    3948 	.db #0x14	; 20
      0046E3 14                    3949 	.db #0x14	; 20
      0046E4 14                    3950 	.db #0x14	; 20
      0046E5 14                    3951 	.db #0x14	; 20
      0046E6 00                    3952 	.db #0x00	; 0
      0046E7 00                    3953 	.db #0x00	; 0
      0046E8 41                    3954 	.db #0x41	; 65	'A'
      0046E9 22                    3955 	.db #0x22	; 34
      0046EA 14                    3956 	.db #0x14	; 20
      0046EB 08                    3957 	.db #0x08	; 8
      0046EC 00                    3958 	.db #0x00	; 0
      0046ED 02                    3959 	.db #0x02	; 2
      0046EE 01                    3960 	.db #0x01	; 1
      0046EF 51                    3961 	.db #0x51	; 81	'Q'
      0046F0 09                    3962 	.db #0x09	; 9
      0046F1 06                    3963 	.db #0x06	; 6
      0046F2 00                    3964 	.db #0x00	; 0
      0046F3 32                    3965 	.db #0x32	; 50	'2'
      0046F4 49                    3966 	.db #0x49	; 73	'I'
      0046F5 59                    3967 	.db #0x59	; 89	'Y'
      0046F6 51                    3968 	.db #0x51	; 81	'Q'
      0046F7 3E                    3969 	.db #0x3e	; 62
      0046F8 00                    3970 	.db #0x00	; 0
      0046F9 7C                    3971 	.db #0x7c	; 124
      0046FA 12                    3972 	.db #0x12	; 18
      0046FB 11                    3973 	.db #0x11	; 17
      0046FC 12                    3974 	.db #0x12	; 18
      0046FD 7C                    3975 	.db #0x7c	; 124
      0046FE 00                    3976 	.db #0x00	; 0
      0046FF 7F                    3977 	.db #0x7f	; 127
      004700 49                    3978 	.db #0x49	; 73	'I'
      004701 49                    3979 	.db #0x49	; 73	'I'
      004702 49                    3980 	.db #0x49	; 73	'I'
      004703 36                    3981 	.db #0x36	; 54	'6'
      004704 00                    3982 	.db #0x00	; 0
      004705 3E                    3983 	.db #0x3e	; 62
      004706 41                    3984 	.db #0x41	; 65	'A'
      004707 41                    3985 	.db #0x41	; 65	'A'
      004708 41                    3986 	.db #0x41	; 65	'A'
      004709 22                    3987 	.db #0x22	; 34
      00470A 00                    3988 	.db #0x00	; 0
      00470B 7F                    3989 	.db #0x7f	; 127
      00470C 41                    3990 	.db #0x41	; 65	'A'
      00470D 41                    3991 	.db #0x41	; 65	'A'
      00470E 22                    3992 	.db #0x22	; 34
      00470F 1C                    3993 	.db #0x1c	; 28
      004710 00                    3994 	.db #0x00	; 0
      004711 7F                    3995 	.db #0x7f	; 127
      004712 49                    3996 	.db #0x49	; 73	'I'
      004713 49                    3997 	.db #0x49	; 73	'I'
      004714 49                    3998 	.db #0x49	; 73	'I'
      004715 41                    3999 	.db #0x41	; 65	'A'
      004716 00                    4000 	.db #0x00	; 0
      004717 7F                    4001 	.db #0x7f	; 127
      004718 09                    4002 	.db #0x09	; 9
      004719 09                    4003 	.db #0x09	; 9
      00471A 09                    4004 	.db #0x09	; 9
      00471B 01                    4005 	.db #0x01	; 1
      00471C 00                    4006 	.db #0x00	; 0
      00471D 3E                    4007 	.db #0x3e	; 62
      00471E 41                    4008 	.db #0x41	; 65	'A'
      00471F 49                    4009 	.db #0x49	; 73	'I'
      004720 49                    4010 	.db #0x49	; 73	'I'
      004721 7A                    4011 	.db #0x7a	; 122	'z'
      004722 00                    4012 	.db #0x00	; 0
      004723 7F                    4013 	.db #0x7f	; 127
      004724 08                    4014 	.db #0x08	; 8
      004725 08                    4015 	.db #0x08	; 8
      004726 08                    4016 	.db #0x08	; 8
      004727 7F                    4017 	.db #0x7f	; 127
      004728 00                    4018 	.db #0x00	; 0
      004729 00                    4019 	.db #0x00	; 0
      00472A 41                    4020 	.db #0x41	; 65	'A'
      00472B 7F                    4021 	.db #0x7f	; 127
      00472C 41                    4022 	.db #0x41	; 65	'A'
      00472D 00                    4023 	.db #0x00	; 0
      00472E 00                    4024 	.db #0x00	; 0
      00472F 20                    4025 	.db #0x20	; 32
      004730 40                    4026 	.db #0x40	; 64
      004731 41                    4027 	.db #0x41	; 65	'A'
      004732 3F                    4028 	.db #0x3f	; 63
      004733 01                    4029 	.db #0x01	; 1
      004734 00                    4030 	.db #0x00	; 0
      004735 7F                    4031 	.db #0x7f	; 127
      004736 08                    4032 	.db #0x08	; 8
      004737 14                    4033 	.db #0x14	; 20
      004738 22                    4034 	.db #0x22	; 34
      004739 41                    4035 	.db #0x41	; 65	'A'
      00473A 00                    4036 	.db #0x00	; 0
      00473B 7F                    4037 	.db #0x7f	; 127
      00473C 40                    4038 	.db #0x40	; 64
      00473D 40                    4039 	.db #0x40	; 64
      00473E 40                    4040 	.db #0x40	; 64
      00473F 40                    4041 	.db #0x40	; 64
      004740 00                    4042 	.db #0x00	; 0
      004741 7F                    4043 	.db #0x7f	; 127
      004742 02                    4044 	.db #0x02	; 2
      004743 0C                    4045 	.db #0x0c	; 12
      004744 02                    4046 	.db #0x02	; 2
      004745 7F                    4047 	.db #0x7f	; 127
      004746 00                    4048 	.db #0x00	; 0
      004747 7F                    4049 	.db #0x7f	; 127
      004748 04                    4050 	.db #0x04	; 4
      004749 08                    4051 	.db #0x08	; 8
      00474A 10                    4052 	.db #0x10	; 16
      00474B 7F                    4053 	.db #0x7f	; 127
      00474C 00                    4054 	.db #0x00	; 0
      00474D 3E                    4055 	.db #0x3e	; 62
      00474E 41                    4056 	.db #0x41	; 65	'A'
      00474F 41                    4057 	.db #0x41	; 65	'A'
      004750 41                    4058 	.db #0x41	; 65	'A'
      004751 3E                    4059 	.db #0x3e	; 62
      004752 00                    4060 	.db #0x00	; 0
      004753 7F                    4061 	.db #0x7f	; 127
      004754 09                    4062 	.db #0x09	; 9
      004755 09                    4063 	.db #0x09	; 9
      004756 09                    4064 	.db #0x09	; 9
      004757 06                    4065 	.db #0x06	; 6
      004758 00                    4066 	.db #0x00	; 0
      004759 3E                    4067 	.db #0x3e	; 62
      00475A 41                    4068 	.db #0x41	; 65	'A'
      00475B 51                    4069 	.db #0x51	; 81	'Q'
      00475C 21                    4070 	.db #0x21	; 33
      00475D 5E                    4071 	.db #0x5e	; 94
      00475E 00                    4072 	.db #0x00	; 0
      00475F 7F                    4073 	.db #0x7f	; 127
      004760 09                    4074 	.db #0x09	; 9
      004761 19                    4075 	.db #0x19	; 25
      004762 29                    4076 	.db #0x29	; 41
      004763 46                    4077 	.db #0x46	; 70	'F'
      004764 00                    4078 	.db #0x00	; 0
      004765 46                    4079 	.db #0x46	; 70	'F'
      004766 49                    4080 	.db #0x49	; 73	'I'
      004767 49                    4081 	.db #0x49	; 73	'I'
      004768 49                    4082 	.db #0x49	; 73	'I'
      004769 31                    4083 	.db #0x31	; 49	'1'
      00476A 00                    4084 	.db #0x00	; 0
      00476B 01                    4085 	.db #0x01	; 1
      00476C 01                    4086 	.db #0x01	; 1
      00476D 7F                    4087 	.db #0x7f	; 127
      00476E 01                    4088 	.db #0x01	; 1
      00476F 01                    4089 	.db #0x01	; 1
      004770 00                    4090 	.db #0x00	; 0
      004771 3F                    4091 	.db #0x3f	; 63
      004772 40                    4092 	.db #0x40	; 64
      004773 40                    4093 	.db #0x40	; 64
      004774 40                    4094 	.db #0x40	; 64
      004775 3F                    4095 	.db #0x3f	; 63
      004776 00                    4096 	.db #0x00	; 0
      004777 1F                    4097 	.db #0x1f	; 31
      004778 20                    4098 	.db #0x20	; 32
      004779 40                    4099 	.db #0x40	; 64
      00477A 20                    4100 	.db #0x20	; 32
      00477B 1F                    4101 	.db #0x1f	; 31
      00477C 00                    4102 	.db #0x00	; 0
      00477D 3F                    4103 	.db #0x3f	; 63
      00477E 40                    4104 	.db #0x40	; 64
      00477F 38                    4105 	.db #0x38	; 56	'8'
      004780 40                    4106 	.db #0x40	; 64
      004781 3F                    4107 	.db #0x3f	; 63
      004782 00                    4108 	.db #0x00	; 0
      004783 63                    4109 	.db #0x63	; 99	'c'
      004784 14                    4110 	.db #0x14	; 20
      004785 08                    4111 	.db #0x08	; 8
      004786 14                    4112 	.db #0x14	; 20
      004787 63                    4113 	.db #0x63	; 99	'c'
      004788 00                    4114 	.db #0x00	; 0
      004789 07                    4115 	.db #0x07	; 7
      00478A 08                    4116 	.db #0x08	; 8
      00478B 70                    4117 	.db #0x70	; 112	'p'
      00478C 08                    4118 	.db #0x08	; 8
      00478D 07                    4119 	.db #0x07	; 7
      00478E 00                    4120 	.db #0x00	; 0
      00478F 61                    4121 	.db #0x61	; 97	'a'
      004790 51                    4122 	.db #0x51	; 81	'Q'
      004791 49                    4123 	.db #0x49	; 73	'I'
      004792 45                    4124 	.db #0x45	; 69	'E'
      004793 43                    4125 	.db #0x43	; 67	'C'
      004794 00                    4126 	.db #0x00	; 0
      004795 00                    4127 	.db #0x00	; 0
      004796 7F                    4128 	.db #0x7f	; 127
      004797 41                    4129 	.db #0x41	; 65	'A'
      004798 41                    4130 	.db #0x41	; 65	'A'
      004799 00                    4131 	.db #0x00	; 0
      00479A 00                    4132 	.db #0x00	; 0
      00479B 55                    4133 	.db #0x55	; 85	'U'
      00479C 2A                    4134 	.db #0x2a	; 42
      00479D 55                    4135 	.db #0x55	; 85	'U'
      00479E 2A                    4136 	.db #0x2a	; 42
      00479F 55                    4137 	.db #0x55	; 85	'U'
      0047A0 00                    4138 	.db #0x00	; 0
      0047A1 00                    4139 	.db #0x00	; 0
      0047A2 41                    4140 	.db #0x41	; 65	'A'
      0047A3 41                    4141 	.db #0x41	; 65	'A'
      0047A4 7F                    4142 	.db #0x7f	; 127
      0047A5 00                    4143 	.db #0x00	; 0
      0047A6 00                    4144 	.db #0x00	; 0
      0047A7 04                    4145 	.db #0x04	; 4
      0047A8 02                    4146 	.db #0x02	; 2
      0047A9 01                    4147 	.db #0x01	; 1
      0047AA 02                    4148 	.db #0x02	; 2
      0047AB 04                    4149 	.db #0x04	; 4
      0047AC 00                    4150 	.db #0x00	; 0
      0047AD 40                    4151 	.db #0x40	; 64
      0047AE 40                    4152 	.db #0x40	; 64
      0047AF 40                    4153 	.db #0x40	; 64
      0047B0 40                    4154 	.db #0x40	; 64
      0047B1 40                    4155 	.db #0x40	; 64
      0047B2 00                    4156 	.db #0x00	; 0
      0047B3 00                    4157 	.db #0x00	; 0
      0047B4 01                    4158 	.db #0x01	; 1
      0047B5 02                    4159 	.db #0x02	; 2
      0047B6 04                    4160 	.db #0x04	; 4
      0047B7 00                    4161 	.db #0x00	; 0
      0047B8 00                    4162 	.db #0x00	; 0
      0047B9 20                    4163 	.db #0x20	; 32
      0047BA 54                    4164 	.db #0x54	; 84	'T'
      0047BB 54                    4165 	.db #0x54	; 84	'T'
      0047BC 54                    4166 	.db #0x54	; 84	'T'
      0047BD 78                    4167 	.db #0x78	; 120	'x'
      0047BE 00                    4168 	.db #0x00	; 0
      0047BF 7F                    4169 	.db #0x7f	; 127
      0047C0 48                    4170 	.db #0x48	; 72	'H'
      0047C1 44                    4171 	.db #0x44	; 68	'D'
      0047C2 44                    4172 	.db #0x44	; 68	'D'
      0047C3 38                    4173 	.db #0x38	; 56	'8'
      0047C4 00                    4174 	.db #0x00	; 0
      0047C5 38                    4175 	.db #0x38	; 56	'8'
      0047C6 44                    4176 	.db #0x44	; 68	'D'
      0047C7 44                    4177 	.db #0x44	; 68	'D'
      0047C8 44                    4178 	.db #0x44	; 68	'D'
      0047C9 20                    4179 	.db #0x20	; 32
      0047CA 00                    4180 	.db #0x00	; 0
      0047CB 38                    4181 	.db #0x38	; 56	'8'
      0047CC 44                    4182 	.db #0x44	; 68	'D'
      0047CD 44                    4183 	.db #0x44	; 68	'D'
      0047CE 48                    4184 	.db #0x48	; 72	'H'
      0047CF 7F                    4185 	.db #0x7f	; 127
      0047D0 00                    4186 	.db #0x00	; 0
      0047D1 38                    4187 	.db #0x38	; 56	'8'
      0047D2 54                    4188 	.db #0x54	; 84	'T'
      0047D3 54                    4189 	.db #0x54	; 84	'T'
      0047D4 54                    4190 	.db #0x54	; 84	'T'
      0047D5 18                    4191 	.db #0x18	; 24
      0047D6 00                    4192 	.db #0x00	; 0
      0047D7 08                    4193 	.db #0x08	; 8
      0047D8 7E                    4194 	.db #0x7e	; 126
      0047D9 09                    4195 	.db #0x09	; 9
      0047DA 01                    4196 	.db #0x01	; 1
      0047DB 02                    4197 	.db #0x02	; 2
      0047DC 00                    4198 	.db #0x00	; 0
      0047DD 18                    4199 	.db #0x18	; 24
      0047DE A4                    4200 	.db #0xa4	; 164
      0047DF A4                    4201 	.db #0xa4	; 164
      0047E0 A4                    4202 	.db #0xa4	; 164
      0047E1 7C                    4203 	.db #0x7c	; 124
      0047E2 00                    4204 	.db #0x00	; 0
      0047E3 7F                    4205 	.db #0x7f	; 127
      0047E4 08                    4206 	.db #0x08	; 8
      0047E5 04                    4207 	.db #0x04	; 4
      0047E6 04                    4208 	.db #0x04	; 4
      0047E7 78                    4209 	.db #0x78	; 120	'x'
      0047E8 00                    4210 	.db #0x00	; 0
      0047E9 00                    4211 	.db #0x00	; 0
      0047EA 44                    4212 	.db #0x44	; 68	'D'
      0047EB 7D                    4213 	.db #0x7d	; 125
      0047EC 40                    4214 	.db #0x40	; 64
      0047ED 00                    4215 	.db #0x00	; 0
      0047EE 00                    4216 	.db #0x00	; 0
      0047EF 40                    4217 	.db #0x40	; 64
      0047F0 80                    4218 	.db #0x80	; 128
      0047F1 84                    4219 	.db #0x84	; 132
      0047F2 7D                    4220 	.db #0x7d	; 125
      0047F3 00                    4221 	.db #0x00	; 0
      0047F4 00                    4222 	.db #0x00	; 0
      0047F5 7F                    4223 	.db #0x7f	; 127
      0047F6 10                    4224 	.db #0x10	; 16
      0047F7 28                    4225 	.db #0x28	; 40
      0047F8 44                    4226 	.db #0x44	; 68	'D'
      0047F9 00                    4227 	.db #0x00	; 0
      0047FA 00                    4228 	.db #0x00	; 0
      0047FB 00                    4229 	.db #0x00	; 0
      0047FC 41                    4230 	.db #0x41	; 65	'A'
      0047FD 7F                    4231 	.db #0x7f	; 127
      0047FE 40                    4232 	.db #0x40	; 64
      0047FF 00                    4233 	.db #0x00	; 0
      004800 00                    4234 	.db #0x00	; 0
      004801 7C                    4235 	.db #0x7c	; 124
      004802 04                    4236 	.db #0x04	; 4
      004803 18                    4237 	.db #0x18	; 24
      004804 04                    4238 	.db #0x04	; 4
      004805 78                    4239 	.db #0x78	; 120	'x'
      004806 00                    4240 	.db #0x00	; 0
      004807 7C                    4241 	.db #0x7c	; 124
      004808 08                    4242 	.db #0x08	; 8
      004809 04                    4243 	.db #0x04	; 4
      00480A 04                    4244 	.db #0x04	; 4
      00480B 78                    4245 	.db #0x78	; 120	'x'
      00480C 00                    4246 	.db #0x00	; 0
      00480D 38                    4247 	.db #0x38	; 56	'8'
      00480E 44                    4248 	.db #0x44	; 68	'D'
      00480F 44                    4249 	.db #0x44	; 68	'D'
      004810 44                    4250 	.db #0x44	; 68	'D'
      004811 38                    4251 	.db #0x38	; 56	'8'
      004812 00                    4252 	.db #0x00	; 0
      004813 FC                    4253 	.db #0xfc	; 252
      004814 24                    4254 	.db #0x24	; 36
      004815 24                    4255 	.db #0x24	; 36
      004816 24                    4256 	.db #0x24	; 36
      004817 18                    4257 	.db #0x18	; 24
      004818 00                    4258 	.db #0x00	; 0
      004819 18                    4259 	.db #0x18	; 24
      00481A 24                    4260 	.db #0x24	; 36
      00481B 24                    4261 	.db #0x24	; 36
      00481C 18                    4262 	.db #0x18	; 24
      00481D FC                    4263 	.db #0xfc	; 252
      00481E 00                    4264 	.db #0x00	; 0
      00481F 7C                    4265 	.db #0x7c	; 124
      004820 08                    4266 	.db #0x08	; 8
      004821 04                    4267 	.db #0x04	; 4
      004822 04                    4268 	.db #0x04	; 4
      004823 08                    4269 	.db #0x08	; 8
      004824 00                    4270 	.db #0x00	; 0
      004825 48                    4271 	.db #0x48	; 72	'H'
      004826 54                    4272 	.db #0x54	; 84	'T'
      004827 54                    4273 	.db #0x54	; 84	'T'
      004828 54                    4274 	.db #0x54	; 84	'T'
      004829 20                    4275 	.db #0x20	; 32
      00482A 00                    4276 	.db #0x00	; 0
      00482B 04                    4277 	.db #0x04	; 4
      00482C 3F                    4278 	.db #0x3f	; 63
      00482D 44                    4279 	.db #0x44	; 68	'D'
      00482E 40                    4280 	.db #0x40	; 64
      00482F 20                    4281 	.db #0x20	; 32
      004830 00                    4282 	.db #0x00	; 0
      004831 3C                    4283 	.db #0x3c	; 60
      004832 40                    4284 	.db #0x40	; 64
      004833 40                    4285 	.db #0x40	; 64
      004834 20                    4286 	.db #0x20	; 32
      004835 7C                    4287 	.db #0x7c	; 124
      004836 00                    4288 	.db #0x00	; 0
      004837 1C                    4289 	.db #0x1c	; 28
      004838 20                    4290 	.db #0x20	; 32
      004839 40                    4291 	.db #0x40	; 64
      00483A 20                    4292 	.db #0x20	; 32
      00483B 1C                    4293 	.db #0x1c	; 28
      00483C 00                    4294 	.db #0x00	; 0
      00483D 3C                    4295 	.db #0x3c	; 60
      00483E 40                    4296 	.db #0x40	; 64
      00483F 30                    4297 	.db #0x30	; 48	'0'
      004840 40                    4298 	.db #0x40	; 64
      004841 3C                    4299 	.db #0x3c	; 60
      004842 00                    4300 	.db #0x00	; 0
      004843 44                    4301 	.db #0x44	; 68	'D'
      004844 28                    4302 	.db #0x28	; 40
      004845 10                    4303 	.db #0x10	; 16
      004846 28                    4304 	.db #0x28	; 40
      004847 44                    4305 	.db #0x44	; 68	'D'
      004848 00                    4306 	.db #0x00	; 0
      004849 1C                    4307 	.db #0x1c	; 28
      00484A A0                    4308 	.db #0xa0	; 160
      00484B A0                    4309 	.db #0xa0	; 160
      00484C A0                    4310 	.db #0xa0	; 160
      00484D 7C                    4311 	.db #0x7c	; 124
      00484E 00                    4312 	.db #0x00	; 0
      00484F 44                    4313 	.db #0x44	; 68	'D'
      004850 64                    4314 	.db #0x64	; 100	'd'
      004851 54                    4315 	.db #0x54	; 84	'T'
      004852 4C                    4316 	.db #0x4c	; 76	'L'
      004853 44                    4317 	.db #0x44	; 68	'D'
      004854 14                    4318 	.db #0x14	; 20
      004855 14                    4319 	.db #0x14	; 20
      004856 14                    4320 	.db #0x14	; 20
      004857 14                    4321 	.db #0x14	; 20
      004858 14                    4322 	.db #0x14	; 20
      004859 14                    4323 	.db #0x14	; 20
      00485A                       4324 _fontMatrix_8x16:
      00485A 00                    4325 	.db #0x00	; 0
      00485B 00                    4326 	.db #0x00	; 0
      00485C 00                    4327 	.db #0x00	; 0
      00485D 00                    4328 	.db #0x00	; 0
      00485E 00                    4329 	.db #0x00	; 0
      00485F 00                    4330 	.db #0x00	; 0
      004860 00                    4331 	.db #0x00	; 0
      004861 00                    4332 	.db #0x00	; 0
      004862 00                    4333 	.db #0x00	; 0
      004863 00                    4334 	.db #0x00	; 0
      004864 00                    4335 	.db #0x00	; 0
      004865 00                    4336 	.db #0x00	; 0
      004866 00                    4337 	.db #0x00	; 0
      004867 00                    4338 	.db #0x00	; 0
      004868 00                    4339 	.db #0x00	; 0
      004869 00                    4340 	.db #0x00	; 0
      00486A 00                    4341 	.db #0x00	; 0
      00486B 00                    4342 	.db #0x00	; 0
      00486C 00                    4343 	.db #0x00	; 0
      00486D F8                    4344 	.db #0xf8	; 248
      00486E 00                    4345 	.db #0x00	; 0
      00486F 00                    4346 	.db #0x00	; 0
      004870 00                    4347 	.db #0x00	; 0
      004871 00                    4348 	.db #0x00	; 0
      004872 00                    4349 	.db #0x00	; 0
      004873 00                    4350 	.db #0x00	; 0
      004874 00                    4351 	.db #0x00	; 0
      004875 30                    4352 	.db #0x30	; 48	'0'
      004876 00                    4353 	.db #0x00	; 0
      004877 00                    4354 	.db #0x00	; 0
      004878 00                    4355 	.db #0x00	; 0
      004879 00                    4356 	.db #0x00	; 0
      00487A 00                    4357 	.db #0x00	; 0
      00487B 10                    4358 	.db #0x10	; 16
      00487C 0C                    4359 	.db #0x0c	; 12
      00487D 06                    4360 	.db #0x06	; 6
      00487E 10                    4361 	.db #0x10	; 16
      00487F 0C                    4362 	.db #0x0c	; 12
      004880 06                    4363 	.db #0x06	; 6
      004881 00                    4364 	.db #0x00	; 0
      004882 00                    4365 	.db #0x00	; 0
      004883 00                    4366 	.db #0x00	; 0
      004884 00                    4367 	.db #0x00	; 0
      004885 00                    4368 	.db #0x00	; 0
      004886 00                    4369 	.db #0x00	; 0
      004887 00                    4370 	.db #0x00	; 0
      004888 00                    4371 	.db #0x00	; 0
      004889 00                    4372 	.db #0x00	; 0
      00488A 40                    4373 	.db #0x40	; 64
      00488B C0                    4374 	.db #0xc0	; 192
      00488C 78                    4375 	.db #0x78	; 120	'x'
      00488D 40                    4376 	.db #0x40	; 64
      00488E C0                    4377 	.db #0xc0	; 192
      00488F 78                    4378 	.db #0x78	; 120	'x'
      004890 40                    4379 	.db #0x40	; 64
      004891 00                    4380 	.db #0x00	; 0
      004892 04                    4381 	.db #0x04	; 4
      004893 3F                    4382 	.db #0x3f	; 63
      004894 04                    4383 	.db #0x04	; 4
      004895 04                    4384 	.db #0x04	; 4
      004896 3F                    4385 	.db #0x3f	; 63
      004897 04                    4386 	.db #0x04	; 4
      004898 04                    4387 	.db #0x04	; 4
      004899 00                    4388 	.db #0x00	; 0
      00489A 00                    4389 	.db #0x00	; 0
      00489B 70                    4390 	.db #0x70	; 112	'p'
      00489C 88                    4391 	.db #0x88	; 136
      00489D FC                    4392 	.db #0xfc	; 252
      00489E 08                    4393 	.db #0x08	; 8
      00489F 30                    4394 	.db #0x30	; 48	'0'
      0048A0 00                    4395 	.db #0x00	; 0
      0048A1 00                    4396 	.db #0x00	; 0
      0048A2 00                    4397 	.db #0x00	; 0
      0048A3 18                    4398 	.db #0x18	; 24
      0048A4 20                    4399 	.db #0x20	; 32
      0048A5 FF                    4400 	.db #0xff	; 255
      0048A6 21                    4401 	.db #0x21	; 33
      0048A7 1E                    4402 	.db #0x1e	; 30
      0048A8 00                    4403 	.db #0x00	; 0
      0048A9 00                    4404 	.db #0x00	; 0
      0048AA F0                    4405 	.db #0xf0	; 240
      0048AB 08                    4406 	.db #0x08	; 8
      0048AC F0                    4407 	.db #0xf0	; 240
      0048AD 00                    4408 	.db #0x00	; 0
      0048AE E0                    4409 	.db #0xe0	; 224
      0048AF 18                    4410 	.db #0x18	; 24
      0048B0 00                    4411 	.db #0x00	; 0
      0048B1 00                    4412 	.db #0x00	; 0
      0048B2 00                    4413 	.db #0x00	; 0
      0048B3 21                    4414 	.db #0x21	; 33
      0048B4 1C                    4415 	.db #0x1c	; 28
      0048B5 03                    4416 	.db #0x03	; 3
      0048B6 1E                    4417 	.db #0x1e	; 30
      0048B7 21                    4418 	.db #0x21	; 33
      0048B8 1E                    4419 	.db #0x1e	; 30
      0048B9 00                    4420 	.db #0x00	; 0
      0048BA 00                    4421 	.db #0x00	; 0
      0048BB F0                    4422 	.db #0xf0	; 240
      0048BC 08                    4423 	.db #0x08	; 8
      0048BD 88                    4424 	.db #0x88	; 136
      0048BE 70                    4425 	.db #0x70	; 112	'p'
      0048BF 00                    4426 	.db #0x00	; 0
      0048C0 00                    4427 	.db #0x00	; 0
      0048C1 00                    4428 	.db #0x00	; 0
      0048C2 1E                    4429 	.db #0x1e	; 30
      0048C3 21                    4430 	.db #0x21	; 33
      0048C4 23                    4431 	.db #0x23	; 35
      0048C5 24                    4432 	.db #0x24	; 36
      0048C6 19                    4433 	.db #0x19	; 25
      0048C7 27                    4434 	.db #0x27	; 39
      0048C8 21                    4435 	.db #0x21	; 33
      0048C9 10                    4436 	.db #0x10	; 16
      0048CA 10                    4437 	.db #0x10	; 16
      0048CB 16                    4438 	.db #0x16	; 22
      0048CC 0E                    4439 	.db #0x0e	; 14
      0048CD 00                    4440 	.db #0x00	; 0
      0048CE 00                    4441 	.db #0x00	; 0
      0048CF 00                    4442 	.db #0x00	; 0
      0048D0 00                    4443 	.db #0x00	; 0
      0048D1 00                    4444 	.db #0x00	; 0
      0048D2 00                    4445 	.db #0x00	; 0
      0048D3 00                    4446 	.db #0x00	; 0
      0048D4 00                    4447 	.db #0x00	; 0
      0048D5 00                    4448 	.db #0x00	; 0
      0048D6 00                    4449 	.db #0x00	; 0
      0048D7 00                    4450 	.db #0x00	; 0
      0048D8 00                    4451 	.db #0x00	; 0
      0048D9 00                    4452 	.db #0x00	; 0
      0048DA 00                    4453 	.db #0x00	; 0
      0048DB 00                    4454 	.db #0x00	; 0
      0048DC 00                    4455 	.db #0x00	; 0
      0048DD E0                    4456 	.db #0xe0	; 224
      0048DE 18                    4457 	.db #0x18	; 24
      0048DF 04                    4458 	.db #0x04	; 4
      0048E0 02                    4459 	.db #0x02	; 2
      0048E1 00                    4460 	.db #0x00	; 0
      0048E2 00                    4461 	.db #0x00	; 0
      0048E3 00                    4462 	.db #0x00	; 0
      0048E4 00                    4463 	.db #0x00	; 0
      0048E5 07                    4464 	.db #0x07	; 7
      0048E6 18                    4465 	.db #0x18	; 24
      0048E7 20                    4466 	.db #0x20	; 32
      0048E8 40                    4467 	.db #0x40	; 64
      0048E9 00                    4468 	.db #0x00	; 0
      0048EA 00                    4469 	.db #0x00	; 0
      0048EB 02                    4470 	.db #0x02	; 2
      0048EC 04                    4471 	.db #0x04	; 4
      0048ED 18                    4472 	.db #0x18	; 24
      0048EE E0                    4473 	.db #0xe0	; 224
      0048EF 00                    4474 	.db #0x00	; 0
      0048F0 00                    4475 	.db #0x00	; 0
      0048F1 00                    4476 	.db #0x00	; 0
      0048F2 00                    4477 	.db #0x00	; 0
      0048F3 40                    4478 	.db #0x40	; 64
      0048F4 20                    4479 	.db #0x20	; 32
      0048F5 18                    4480 	.db #0x18	; 24
      0048F6 07                    4481 	.db #0x07	; 7
      0048F7 00                    4482 	.db #0x00	; 0
      0048F8 00                    4483 	.db #0x00	; 0
      0048F9 00                    4484 	.db #0x00	; 0
      0048FA 40                    4485 	.db #0x40	; 64
      0048FB 40                    4486 	.db #0x40	; 64
      0048FC 80                    4487 	.db #0x80	; 128
      0048FD F0                    4488 	.db #0xf0	; 240
      0048FE 80                    4489 	.db #0x80	; 128
      0048FF 40                    4490 	.db #0x40	; 64
      004900 40                    4491 	.db #0x40	; 64
      004901 00                    4492 	.db #0x00	; 0
      004902 02                    4493 	.db #0x02	; 2
      004903 02                    4494 	.db #0x02	; 2
      004904 01                    4495 	.db #0x01	; 1
      004905 0F                    4496 	.db #0x0f	; 15
      004906 01                    4497 	.db #0x01	; 1
      004907 02                    4498 	.db #0x02	; 2
      004908 02                    4499 	.db #0x02	; 2
      004909 00                    4500 	.db #0x00	; 0
      00490A 00                    4501 	.db #0x00	; 0
      00490B 00                    4502 	.db #0x00	; 0
      00490C 00                    4503 	.db #0x00	; 0
      00490D F0                    4504 	.db #0xf0	; 240
      00490E 00                    4505 	.db #0x00	; 0
      00490F 00                    4506 	.db #0x00	; 0
      004910 00                    4507 	.db #0x00	; 0
      004911 00                    4508 	.db #0x00	; 0
      004912 01                    4509 	.db #0x01	; 1
      004913 01                    4510 	.db #0x01	; 1
      004914 01                    4511 	.db #0x01	; 1
      004915 1F                    4512 	.db #0x1f	; 31
      004916 01                    4513 	.db #0x01	; 1
      004917 01                    4514 	.db #0x01	; 1
      004918 01                    4515 	.db #0x01	; 1
      004919 00                    4516 	.db #0x00	; 0
      00491A 00                    4517 	.db #0x00	; 0
      00491B 00                    4518 	.db #0x00	; 0
      00491C 00                    4519 	.db #0x00	; 0
      00491D 00                    4520 	.db #0x00	; 0
      00491E 00                    4521 	.db #0x00	; 0
      00491F 00                    4522 	.db #0x00	; 0
      004920 00                    4523 	.db #0x00	; 0
      004921 00                    4524 	.db #0x00	; 0
      004922 80                    4525 	.db #0x80	; 128
      004923 B0                    4526 	.db #0xb0	; 176
      004924 70                    4527 	.db #0x70	; 112	'p'
      004925 00                    4528 	.db #0x00	; 0
      004926 00                    4529 	.db #0x00	; 0
      004927 00                    4530 	.db #0x00	; 0
      004928 00                    4531 	.db #0x00	; 0
      004929 00                    4532 	.db #0x00	; 0
      00492A 00                    4533 	.db #0x00	; 0
      00492B 00                    4534 	.db #0x00	; 0
      00492C 00                    4535 	.db #0x00	; 0
      00492D 00                    4536 	.db #0x00	; 0
      00492E 00                    4537 	.db #0x00	; 0
      00492F 00                    4538 	.db #0x00	; 0
      004930 00                    4539 	.db #0x00	; 0
      004931 00                    4540 	.db #0x00	; 0
      004932 00                    4541 	.db #0x00	; 0
      004933 01                    4542 	.db #0x01	; 1
      004934 01                    4543 	.db #0x01	; 1
      004935 01                    4544 	.db #0x01	; 1
      004936 01                    4545 	.db #0x01	; 1
      004937 01                    4546 	.db #0x01	; 1
      004938 01                    4547 	.db #0x01	; 1
      004939 01                    4548 	.db #0x01	; 1
      00493A 00                    4549 	.db #0x00	; 0
      00493B 00                    4550 	.db #0x00	; 0
      00493C 00                    4551 	.db #0x00	; 0
      00493D 00                    4552 	.db #0x00	; 0
      00493E 00                    4553 	.db #0x00	; 0
      00493F 00                    4554 	.db #0x00	; 0
      004940 00                    4555 	.db #0x00	; 0
      004941 00                    4556 	.db #0x00	; 0
      004942 00                    4557 	.db #0x00	; 0
      004943 30                    4558 	.db #0x30	; 48	'0'
      004944 30                    4559 	.db #0x30	; 48	'0'
      004945 00                    4560 	.db #0x00	; 0
      004946 00                    4561 	.db #0x00	; 0
      004947 00                    4562 	.db #0x00	; 0
      004948 00                    4563 	.db #0x00	; 0
      004949 00                    4564 	.db #0x00	; 0
      00494A 00                    4565 	.db #0x00	; 0
      00494B 00                    4566 	.db #0x00	; 0
      00494C 00                    4567 	.db #0x00	; 0
      00494D 00                    4568 	.db #0x00	; 0
      00494E 80                    4569 	.db #0x80	; 128
      00494F 60                    4570 	.db #0x60	; 96
      004950 18                    4571 	.db #0x18	; 24
      004951 04                    4572 	.db #0x04	; 4
      004952 00                    4573 	.db #0x00	; 0
      004953 60                    4574 	.db #0x60	; 96
      004954 18                    4575 	.db #0x18	; 24
      004955 06                    4576 	.db #0x06	; 6
      004956 01                    4577 	.db #0x01	; 1
      004957 00                    4578 	.db #0x00	; 0
      004958 00                    4579 	.db #0x00	; 0
      004959 00                    4580 	.db #0x00	; 0
      00495A 00                    4581 	.db #0x00	; 0
      00495B E0                    4582 	.db #0xe0	; 224
      00495C 10                    4583 	.db #0x10	; 16
      00495D 08                    4584 	.db #0x08	; 8
      00495E 08                    4585 	.db #0x08	; 8
      00495F 10                    4586 	.db #0x10	; 16
      004960 E0                    4587 	.db #0xe0	; 224
      004961 00                    4588 	.db #0x00	; 0
      004962 00                    4589 	.db #0x00	; 0
      004963 0F                    4590 	.db #0x0f	; 15
      004964 10                    4591 	.db #0x10	; 16
      004965 20                    4592 	.db #0x20	; 32
      004966 20                    4593 	.db #0x20	; 32
      004967 10                    4594 	.db #0x10	; 16
      004968 0F                    4595 	.db #0x0f	; 15
      004969 00                    4596 	.db #0x00	; 0
      00496A 00                    4597 	.db #0x00	; 0
      00496B 10                    4598 	.db #0x10	; 16
      00496C 10                    4599 	.db #0x10	; 16
      00496D F8                    4600 	.db #0xf8	; 248
      00496E 00                    4601 	.db #0x00	; 0
      00496F 00                    4602 	.db #0x00	; 0
      004970 00                    4603 	.db #0x00	; 0
      004971 00                    4604 	.db #0x00	; 0
      004972 00                    4605 	.db #0x00	; 0
      004973 20                    4606 	.db #0x20	; 32
      004974 20                    4607 	.db #0x20	; 32
      004975 3F                    4608 	.db #0x3f	; 63
      004976 20                    4609 	.db #0x20	; 32
      004977 20                    4610 	.db #0x20	; 32
      004978 00                    4611 	.db #0x00	; 0
      004979 00                    4612 	.db #0x00	; 0
      00497A 00                    4613 	.db #0x00	; 0
      00497B 70                    4614 	.db #0x70	; 112	'p'
      00497C 08                    4615 	.db #0x08	; 8
      00497D 08                    4616 	.db #0x08	; 8
      00497E 08                    4617 	.db #0x08	; 8
      00497F 88                    4618 	.db #0x88	; 136
      004980 70                    4619 	.db #0x70	; 112	'p'
      004981 00                    4620 	.db #0x00	; 0
      004982 00                    4621 	.db #0x00	; 0
      004983 30                    4622 	.db #0x30	; 48	'0'
      004984 28                    4623 	.db #0x28	; 40
      004985 24                    4624 	.db #0x24	; 36
      004986 22                    4625 	.db #0x22	; 34
      004987 21                    4626 	.db #0x21	; 33
      004988 30                    4627 	.db #0x30	; 48	'0'
      004989 00                    4628 	.db #0x00	; 0
      00498A 00                    4629 	.db #0x00	; 0
      00498B 30                    4630 	.db #0x30	; 48	'0'
      00498C 08                    4631 	.db #0x08	; 8
      00498D 88                    4632 	.db #0x88	; 136
      00498E 88                    4633 	.db #0x88	; 136
      00498F 48                    4634 	.db #0x48	; 72	'H'
      004990 30                    4635 	.db #0x30	; 48	'0'
      004991 00                    4636 	.db #0x00	; 0
      004992 00                    4637 	.db #0x00	; 0
      004993 18                    4638 	.db #0x18	; 24
      004994 20                    4639 	.db #0x20	; 32
      004995 20                    4640 	.db #0x20	; 32
      004996 20                    4641 	.db #0x20	; 32
      004997 11                    4642 	.db #0x11	; 17
      004998 0E                    4643 	.db #0x0e	; 14
      004999 00                    4644 	.db #0x00	; 0
      00499A 00                    4645 	.db #0x00	; 0
      00499B 00                    4646 	.db #0x00	; 0
      00499C C0                    4647 	.db #0xc0	; 192
      00499D 20                    4648 	.db #0x20	; 32
      00499E 10                    4649 	.db #0x10	; 16
      00499F F8                    4650 	.db #0xf8	; 248
      0049A0 00                    4651 	.db #0x00	; 0
      0049A1 00                    4652 	.db #0x00	; 0
      0049A2 00                    4653 	.db #0x00	; 0
      0049A3 07                    4654 	.db #0x07	; 7
      0049A4 04                    4655 	.db #0x04	; 4
      0049A5 24                    4656 	.db #0x24	; 36
      0049A6 24                    4657 	.db #0x24	; 36
      0049A7 3F                    4658 	.db #0x3f	; 63
      0049A8 24                    4659 	.db #0x24	; 36
      0049A9 00                    4660 	.db #0x00	; 0
      0049AA 00                    4661 	.db #0x00	; 0
      0049AB F8                    4662 	.db #0xf8	; 248
      0049AC 08                    4663 	.db #0x08	; 8
      0049AD 88                    4664 	.db #0x88	; 136
      0049AE 88                    4665 	.db #0x88	; 136
      0049AF 08                    4666 	.db #0x08	; 8
      0049B0 08                    4667 	.db #0x08	; 8
      0049B1 00                    4668 	.db #0x00	; 0
      0049B2 00                    4669 	.db #0x00	; 0
      0049B3 19                    4670 	.db #0x19	; 25
      0049B4 21                    4671 	.db #0x21	; 33
      0049B5 20                    4672 	.db #0x20	; 32
      0049B6 20                    4673 	.db #0x20	; 32
      0049B7 11                    4674 	.db #0x11	; 17
      0049B8 0E                    4675 	.db #0x0e	; 14
      0049B9 00                    4676 	.db #0x00	; 0
      0049BA 00                    4677 	.db #0x00	; 0
      0049BB E0                    4678 	.db #0xe0	; 224
      0049BC 10                    4679 	.db #0x10	; 16
      0049BD 88                    4680 	.db #0x88	; 136
      0049BE 88                    4681 	.db #0x88	; 136
      0049BF 18                    4682 	.db #0x18	; 24
      0049C0 00                    4683 	.db #0x00	; 0
      0049C1 00                    4684 	.db #0x00	; 0
      0049C2 00                    4685 	.db #0x00	; 0
      0049C3 0F                    4686 	.db #0x0f	; 15
      0049C4 11                    4687 	.db #0x11	; 17
      0049C5 20                    4688 	.db #0x20	; 32
      0049C6 20                    4689 	.db #0x20	; 32
      0049C7 11                    4690 	.db #0x11	; 17
      0049C8 0E                    4691 	.db #0x0e	; 14
      0049C9 00                    4692 	.db #0x00	; 0
      0049CA 00                    4693 	.db #0x00	; 0
      0049CB 38                    4694 	.db #0x38	; 56	'8'
      0049CC 08                    4695 	.db #0x08	; 8
      0049CD 08                    4696 	.db #0x08	; 8
      0049CE C8                    4697 	.db #0xc8	; 200
      0049CF 38                    4698 	.db #0x38	; 56	'8'
      0049D0 08                    4699 	.db #0x08	; 8
      0049D1 00                    4700 	.db #0x00	; 0
      0049D2 00                    4701 	.db #0x00	; 0
      0049D3 00                    4702 	.db #0x00	; 0
      0049D4 00                    4703 	.db #0x00	; 0
      0049D5 3F                    4704 	.db #0x3f	; 63
      0049D6 00                    4705 	.db #0x00	; 0
      0049D7 00                    4706 	.db #0x00	; 0
      0049D8 00                    4707 	.db #0x00	; 0
      0049D9 00                    4708 	.db #0x00	; 0
      0049DA 00                    4709 	.db #0x00	; 0
      0049DB 70                    4710 	.db #0x70	; 112	'p'
      0049DC 88                    4711 	.db #0x88	; 136
      0049DD 08                    4712 	.db #0x08	; 8
      0049DE 08                    4713 	.db #0x08	; 8
      0049DF 88                    4714 	.db #0x88	; 136
      0049E0 70                    4715 	.db #0x70	; 112	'p'
      0049E1 00                    4716 	.db #0x00	; 0
      0049E2 00                    4717 	.db #0x00	; 0
      0049E3 1C                    4718 	.db #0x1c	; 28
      0049E4 22                    4719 	.db #0x22	; 34
      0049E5 21                    4720 	.db #0x21	; 33
      0049E6 21                    4721 	.db #0x21	; 33
      0049E7 22                    4722 	.db #0x22	; 34
      0049E8 1C                    4723 	.db #0x1c	; 28
      0049E9 00                    4724 	.db #0x00	; 0
      0049EA 00                    4725 	.db #0x00	; 0
      0049EB E0                    4726 	.db #0xe0	; 224
      0049EC 10                    4727 	.db #0x10	; 16
      0049ED 08                    4728 	.db #0x08	; 8
      0049EE 08                    4729 	.db #0x08	; 8
      0049EF 10                    4730 	.db #0x10	; 16
      0049F0 E0                    4731 	.db #0xe0	; 224
      0049F1 00                    4732 	.db #0x00	; 0
      0049F2 00                    4733 	.db #0x00	; 0
      0049F3 00                    4734 	.db #0x00	; 0
      0049F4 31                    4735 	.db #0x31	; 49	'1'
      0049F5 22                    4736 	.db #0x22	; 34
      0049F6 22                    4737 	.db #0x22	; 34
      0049F7 11                    4738 	.db #0x11	; 17
      0049F8 0F                    4739 	.db #0x0f	; 15
      0049F9 00                    4740 	.db #0x00	; 0
      0049FA 00                    4741 	.db #0x00	; 0
      0049FB 00                    4742 	.db #0x00	; 0
      0049FC 00                    4743 	.db #0x00	; 0
      0049FD C0                    4744 	.db #0xc0	; 192
      0049FE C0                    4745 	.db #0xc0	; 192
      0049FF 00                    4746 	.db #0x00	; 0
      004A00 00                    4747 	.db #0x00	; 0
      004A01 00                    4748 	.db #0x00	; 0
      004A02 00                    4749 	.db #0x00	; 0
      004A03 00                    4750 	.db #0x00	; 0
      004A04 00                    4751 	.db #0x00	; 0
      004A05 30                    4752 	.db #0x30	; 48	'0'
      004A06 30                    4753 	.db #0x30	; 48	'0'
      004A07 00                    4754 	.db #0x00	; 0
      004A08 00                    4755 	.db #0x00	; 0
      004A09 00                    4756 	.db #0x00	; 0
      004A0A 00                    4757 	.db #0x00	; 0
      004A0B 00                    4758 	.db #0x00	; 0
      004A0C 00                    4759 	.db #0x00	; 0
      004A0D 80                    4760 	.db #0x80	; 128
      004A0E 00                    4761 	.db #0x00	; 0
      004A0F 00                    4762 	.db #0x00	; 0
      004A10 00                    4763 	.db #0x00	; 0
      004A11 00                    4764 	.db #0x00	; 0
      004A12 00                    4765 	.db #0x00	; 0
      004A13 00                    4766 	.db #0x00	; 0
      004A14 80                    4767 	.db #0x80	; 128
      004A15 60                    4768 	.db #0x60	; 96
      004A16 00                    4769 	.db #0x00	; 0
      004A17 00                    4770 	.db #0x00	; 0
      004A18 00                    4771 	.db #0x00	; 0
      004A19 00                    4772 	.db #0x00	; 0
      004A1A 00                    4773 	.db #0x00	; 0
      004A1B 00                    4774 	.db #0x00	; 0
      004A1C 80                    4775 	.db #0x80	; 128
      004A1D 40                    4776 	.db #0x40	; 64
      004A1E 20                    4777 	.db #0x20	; 32
      004A1F 10                    4778 	.db #0x10	; 16
      004A20 08                    4779 	.db #0x08	; 8
      004A21 00                    4780 	.db #0x00	; 0
      004A22 00                    4781 	.db #0x00	; 0
      004A23 01                    4782 	.db #0x01	; 1
      004A24 02                    4783 	.db #0x02	; 2
      004A25 04                    4784 	.db #0x04	; 4
      004A26 08                    4785 	.db #0x08	; 8
      004A27 10                    4786 	.db #0x10	; 16
      004A28 20                    4787 	.db #0x20	; 32
      004A29 00                    4788 	.db #0x00	; 0
      004A2A 40                    4789 	.db #0x40	; 64
      004A2B 40                    4790 	.db #0x40	; 64
      004A2C 40                    4791 	.db #0x40	; 64
      004A2D 40                    4792 	.db #0x40	; 64
      004A2E 40                    4793 	.db #0x40	; 64
      004A2F 40                    4794 	.db #0x40	; 64
      004A30 40                    4795 	.db #0x40	; 64
      004A31 00                    4796 	.db #0x00	; 0
      004A32 04                    4797 	.db #0x04	; 4
      004A33 04                    4798 	.db #0x04	; 4
      004A34 04                    4799 	.db #0x04	; 4
      004A35 04                    4800 	.db #0x04	; 4
      004A36 04                    4801 	.db #0x04	; 4
      004A37 04                    4802 	.db #0x04	; 4
      004A38 04                    4803 	.db #0x04	; 4
      004A39 00                    4804 	.db #0x00	; 0
      004A3A 00                    4805 	.db #0x00	; 0
      004A3B 08                    4806 	.db #0x08	; 8
      004A3C 10                    4807 	.db #0x10	; 16
      004A3D 20                    4808 	.db #0x20	; 32
      004A3E 40                    4809 	.db #0x40	; 64
      004A3F 80                    4810 	.db #0x80	; 128
      004A40 00                    4811 	.db #0x00	; 0
      004A41 00                    4812 	.db #0x00	; 0
      004A42 00                    4813 	.db #0x00	; 0
      004A43 20                    4814 	.db #0x20	; 32
      004A44 10                    4815 	.db #0x10	; 16
      004A45 08                    4816 	.db #0x08	; 8
      004A46 04                    4817 	.db #0x04	; 4
      004A47 02                    4818 	.db #0x02	; 2
      004A48 01                    4819 	.db #0x01	; 1
      004A49 00                    4820 	.db #0x00	; 0
      004A4A 00                    4821 	.db #0x00	; 0
      004A4B 70                    4822 	.db #0x70	; 112	'p'
      004A4C 48                    4823 	.db #0x48	; 72	'H'
      004A4D 08                    4824 	.db #0x08	; 8
      004A4E 08                    4825 	.db #0x08	; 8
      004A4F 08                    4826 	.db #0x08	; 8
      004A50 F0                    4827 	.db #0xf0	; 240
      004A51 00                    4828 	.db #0x00	; 0
      004A52 00                    4829 	.db #0x00	; 0
      004A53 00                    4830 	.db #0x00	; 0
      004A54 00                    4831 	.db #0x00	; 0
      004A55 30                    4832 	.db #0x30	; 48	'0'
      004A56 36                    4833 	.db #0x36	; 54	'6'
      004A57 01                    4834 	.db #0x01	; 1
      004A58 00                    4835 	.db #0x00	; 0
      004A59 00                    4836 	.db #0x00	; 0
      004A5A C0                    4837 	.db #0xc0	; 192
      004A5B 30                    4838 	.db #0x30	; 48	'0'
      004A5C C8                    4839 	.db #0xc8	; 200
      004A5D 28                    4840 	.db #0x28	; 40
      004A5E E8                    4841 	.db #0xe8	; 232
      004A5F 10                    4842 	.db #0x10	; 16
      004A60 E0                    4843 	.db #0xe0	; 224
      004A61 00                    4844 	.db #0x00	; 0
      004A62 07                    4845 	.db #0x07	; 7
      004A63 18                    4846 	.db #0x18	; 24
      004A64 27                    4847 	.db #0x27	; 39
      004A65 24                    4848 	.db #0x24	; 36
      004A66 23                    4849 	.db #0x23	; 35
      004A67 14                    4850 	.db #0x14	; 20
      004A68 0B                    4851 	.db #0x0b	; 11
      004A69 00                    4852 	.db #0x00	; 0
      004A6A 00                    4853 	.db #0x00	; 0
      004A6B 00                    4854 	.db #0x00	; 0
      004A6C C0                    4855 	.db #0xc0	; 192
      004A6D 38                    4856 	.db #0x38	; 56	'8'
      004A6E E0                    4857 	.db #0xe0	; 224
      004A6F 00                    4858 	.db #0x00	; 0
      004A70 00                    4859 	.db #0x00	; 0
      004A71 00                    4860 	.db #0x00	; 0
      004A72 20                    4861 	.db #0x20	; 32
      004A73 3C                    4862 	.db #0x3c	; 60
      004A74 23                    4863 	.db #0x23	; 35
      004A75 02                    4864 	.db #0x02	; 2
      004A76 02                    4865 	.db #0x02	; 2
      004A77 27                    4866 	.db #0x27	; 39
      004A78 38                    4867 	.db #0x38	; 56	'8'
      004A79 20                    4868 	.db #0x20	; 32
      004A7A 08                    4869 	.db #0x08	; 8
      004A7B F8                    4870 	.db #0xf8	; 248
      004A7C 88                    4871 	.db #0x88	; 136
      004A7D 88                    4872 	.db #0x88	; 136
      004A7E 88                    4873 	.db #0x88	; 136
      004A7F 70                    4874 	.db #0x70	; 112	'p'
      004A80 00                    4875 	.db #0x00	; 0
      004A81 00                    4876 	.db #0x00	; 0
      004A82 20                    4877 	.db #0x20	; 32
      004A83 3F                    4878 	.db #0x3f	; 63
      004A84 20                    4879 	.db #0x20	; 32
      004A85 20                    4880 	.db #0x20	; 32
      004A86 20                    4881 	.db #0x20	; 32
      004A87 11                    4882 	.db #0x11	; 17
      004A88 0E                    4883 	.db #0x0e	; 14
      004A89 00                    4884 	.db #0x00	; 0
      004A8A C0                    4885 	.db #0xc0	; 192
      004A8B 30                    4886 	.db #0x30	; 48	'0'
      004A8C 08                    4887 	.db #0x08	; 8
      004A8D 08                    4888 	.db #0x08	; 8
      004A8E 08                    4889 	.db #0x08	; 8
      004A8F 08                    4890 	.db #0x08	; 8
      004A90 38                    4891 	.db #0x38	; 56	'8'
      004A91 00                    4892 	.db #0x00	; 0
      004A92 07                    4893 	.db #0x07	; 7
      004A93 18                    4894 	.db #0x18	; 24
      004A94 20                    4895 	.db #0x20	; 32
      004A95 20                    4896 	.db #0x20	; 32
      004A96 20                    4897 	.db #0x20	; 32
      004A97 10                    4898 	.db #0x10	; 16
      004A98 08                    4899 	.db #0x08	; 8
      004A99 00                    4900 	.db #0x00	; 0
      004A9A 08                    4901 	.db #0x08	; 8
      004A9B F8                    4902 	.db #0xf8	; 248
      004A9C 08                    4903 	.db #0x08	; 8
      004A9D 08                    4904 	.db #0x08	; 8
      004A9E 08                    4905 	.db #0x08	; 8
      004A9F 10                    4906 	.db #0x10	; 16
      004AA0 E0                    4907 	.db #0xe0	; 224
      004AA1 00                    4908 	.db #0x00	; 0
      004AA2 20                    4909 	.db #0x20	; 32
      004AA3 3F                    4910 	.db #0x3f	; 63
      004AA4 20                    4911 	.db #0x20	; 32
      004AA5 20                    4912 	.db #0x20	; 32
      004AA6 20                    4913 	.db #0x20	; 32
      004AA7 10                    4914 	.db #0x10	; 16
      004AA8 0F                    4915 	.db #0x0f	; 15
      004AA9 00                    4916 	.db #0x00	; 0
      004AAA 08                    4917 	.db #0x08	; 8
      004AAB F8                    4918 	.db #0xf8	; 248
      004AAC 88                    4919 	.db #0x88	; 136
      004AAD 88                    4920 	.db #0x88	; 136
      004AAE E8                    4921 	.db #0xe8	; 232
      004AAF 08                    4922 	.db #0x08	; 8
      004AB0 10                    4923 	.db #0x10	; 16
      004AB1 00                    4924 	.db #0x00	; 0
      004AB2 20                    4925 	.db #0x20	; 32
      004AB3 3F                    4926 	.db #0x3f	; 63
      004AB4 20                    4927 	.db #0x20	; 32
      004AB5 20                    4928 	.db #0x20	; 32
      004AB6 23                    4929 	.db #0x23	; 35
      004AB7 20                    4930 	.db #0x20	; 32
      004AB8 18                    4931 	.db #0x18	; 24
      004AB9 00                    4932 	.db #0x00	; 0
      004ABA 08                    4933 	.db #0x08	; 8
      004ABB F8                    4934 	.db #0xf8	; 248
      004ABC 88                    4935 	.db #0x88	; 136
      004ABD 88                    4936 	.db #0x88	; 136
      004ABE E8                    4937 	.db #0xe8	; 232
      004ABF 08                    4938 	.db #0x08	; 8
      004AC0 10                    4939 	.db #0x10	; 16
      004AC1 00                    4940 	.db #0x00	; 0
      004AC2 20                    4941 	.db #0x20	; 32
      004AC3 3F                    4942 	.db #0x3f	; 63
      004AC4 20                    4943 	.db #0x20	; 32
      004AC5 00                    4944 	.db #0x00	; 0
      004AC6 03                    4945 	.db #0x03	; 3
      004AC7 00                    4946 	.db #0x00	; 0
      004AC8 00                    4947 	.db #0x00	; 0
      004AC9 00                    4948 	.db #0x00	; 0
      004ACA C0                    4949 	.db #0xc0	; 192
      004ACB 30                    4950 	.db #0x30	; 48	'0'
      004ACC 08                    4951 	.db #0x08	; 8
      004ACD 08                    4952 	.db #0x08	; 8
      004ACE 08                    4953 	.db #0x08	; 8
      004ACF 38                    4954 	.db #0x38	; 56	'8'
      004AD0 00                    4955 	.db #0x00	; 0
      004AD1 00                    4956 	.db #0x00	; 0
      004AD2 07                    4957 	.db #0x07	; 7
      004AD3 18                    4958 	.db #0x18	; 24
      004AD4 20                    4959 	.db #0x20	; 32
      004AD5 20                    4960 	.db #0x20	; 32
      004AD6 22                    4961 	.db #0x22	; 34
      004AD7 1E                    4962 	.db #0x1e	; 30
      004AD8 02                    4963 	.db #0x02	; 2
      004AD9 00                    4964 	.db #0x00	; 0
      004ADA 08                    4965 	.db #0x08	; 8
      004ADB F8                    4966 	.db #0xf8	; 248
      004ADC 08                    4967 	.db #0x08	; 8
      004ADD 00                    4968 	.db #0x00	; 0
      004ADE 00                    4969 	.db #0x00	; 0
      004ADF 08                    4970 	.db #0x08	; 8
      004AE0 F8                    4971 	.db #0xf8	; 248
      004AE1 08                    4972 	.db #0x08	; 8
      004AE2 20                    4973 	.db #0x20	; 32
      004AE3 3F                    4974 	.db #0x3f	; 63
      004AE4 21                    4975 	.db #0x21	; 33
      004AE5 01                    4976 	.db #0x01	; 1
      004AE6 01                    4977 	.db #0x01	; 1
      004AE7 21                    4978 	.db #0x21	; 33
      004AE8 3F                    4979 	.db #0x3f	; 63
      004AE9 20                    4980 	.db #0x20	; 32
      004AEA 00                    4981 	.db #0x00	; 0
      004AEB 08                    4982 	.db #0x08	; 8
      004AEC 08                    4983 	.db #0x08	; 8
      004AED F8                    4984 	.db #0xf8	; 248
      004AEE 08                    4985 	.db #0x08	; 8
      004AEF 08                    4986 	.db #0x08	; 8
      004AF0 00                    4987 	.db #0x00	; 0
      004AF1 00                    4988 	.db #0x00	; 0
      004AF2 00                    4989 	.db #0x00	; 0
      004AF3 20                    4990 	.db #0x20	; 32
      004AF4 20                    4991 	.db #0x20	; 32
      004AF5 3F                    4992 	.db #0x3f	; 63
      004AF6 20                    4993 	.db #0x20	; 32
      004AF7 20                    4994 	.db #0x20	; 32
      004AF8 00                    4995 	.db #0x00	; 0
      004AF9 00                    4996 	.db #0x00	; 0
      004AFA 00                    4997 	.db #0x00	; 0
      004AFB 00                    4998 	.db #0x00	; 0
      004AFC 08                    4999 	.db #0x08	; 8
      004AFD 08                    5000 	.db #0x08	; 8
      004AFE F8                    5001 	.db #0xf8	; 248
      004AFF 08                    5002 	.db #0x08	; 8
      004B00 08                    5003 	.db #0x08	; 8
      004B01 00                    5004 	.db #0x00	; 0
      004B02 C0                    5005 	.db #0xc0	; 192
      004B03 80                    5006 	.db #0x80	; 128
      004B04 80                    5007 	.db #0x80	; 128
      004B05 80                    5008 	.db #0x80	; 128
      004B06 7F                    5009 	.db #0x7f	; 127
      004B07 00                    5010 	.db #0x00	; 0
      004B08 00                    5011 	.db #0x00	; 0
      004B09 00                    5012 	.db #0x00	; 0
      004B0A 08                    5013 	.db #0x08	; 8
      004B0B F8                    5014 	.db #0xf8	; 248
      004B0C 88                    5015 	.db #0x88	; 136
      004B0D C0                    5016 	.db #0xc0	; 192
      004B0E 28                    5017 	.db #0x28	; 40
      004B0F 18                    5018 	.db #0x18	; 24
      004B10 08                    5019 	.db #0x08	; 8
      004B11 00                    5020 	.db #0x00	; 0
      004B12 20                    5021 	.db #0x20	; 32
      004B13 3F                    5022 	.db #0x3f	; 63
      004B14 20                    5023 	.db #0x20	; 32
      004B15 01                    5024 	.db #0x01	; 1
      004B16 26                    5025 	.db #0x26	; 38
      004B17 38                    5026 	.db #0x38	; 56	'8'
      004B18 20                    5027 	.db #0x20	; 32
      004B19 00                    5028 	.db #0x00	; 0
      004B1A 08                    5029 	.db #0x08	; 8
      004B1B F8                    5030 	.db #0xf8	; 248
      004B1C 08                    5031 	.db #0x08	; 8
      004B1D 00                    5032 	.db #0x00	; 0
      004B1E 00                    5033 	.db #0x00	; 0
      004B1F 00                    5034 	.db #0x00	; 0
      004B20 00                    5035 	.db #0x00	; 0
      004B21 00                    5036 	.db #0x00	; 0
      004B22 20                    5037 	.db #0x20	; 32
      004B23 3F                    5038 	.db #0x3f	; 63
      004B24 20                    5039 	.db #0x20	; 32
      004B25 20                    5040 	.db #0x20	; 32
      004B26 20                    5041 	.db #0x20	; 32
      004B27 20                    5042 	.db #0x20	; 32
      004B28 30                    5043 	.db #0x30	; 48	'0'
      004B29 00                    5044 	.db #0x00	; 0
      004B2A 08                    5045 	.db #0x08	; 8
      004B2B F8                    5046 	.db #0xf8	; 248
      004B2C F8                    5047 	.db #0xf8	; 248
      004B2D 00                    5048 	.db #0x00	; 0
      004B2E F8                    5049 	.db #0xf8	; 248
      004B2F F8                    5050 	.db #0xf8	; 248
      004B30 08                    5051 	.db #0x08	; 8
      004B31 00                    5052 	.db #0x00	; 0
      004B32 20                    5053 	.db #0x20	; 32
      004B33 3F                    5054 	.db #0x3f	; 63
      004B34 00                    5055 	.db #0x00	; 0
      004B35 3F                    5056 	.db #0x3f	; 63
      004B36 00                    5057 	.db #0x00	; 0
      004B37 3F                    5058 	.db #0x3f	; 63
      004B38 20                    5059 	.db #0x20	; 32
      004B39 00                    5060 	.db #0x00	; 0
      004B3A 08                    5061 	.db #0x08	; 8
      004B3B F8                    5062 	.db #0xf8	; 248
      004B3C 30                    5063 	.db #0x30	; 48	'0'
      004B3D C0                    5064 	.db #0xc0	; 192
      004B3E 00                    5065 	.db #0x00	; 0
      004B3F 08                    5066 	.db #0x08	; 8
      004B40 F8                    5067 	.db #0xf8	; 248
      004B41 08                    5068 	.db #0x08	; 8
      004B42 20                    5069 	.db #0x20	; 32
      004B43 3F                    5070 	.db #0x3f	; 63
      004B44 20                    5071 	.db #0x20	; 32
      004B45 00                    5072 	.db #0x00	; 0
      004B46 07                    5073 	.db #0x07	; 7
      004B47 18                    5074 	.db #0x18	; 24
      004B48 3F                    5075 	.db #0x3f	; 63
      004B49 00                    5076 	.db #0x00	; 0
      004B4A E0                    5077 	.db #0xe0	; 224
      004B4B 10                    5078 	.db #0x10	; 16
      004B4C 08                    5079 	.db #0x08	; 8
      004B4D 08                    5080 	.db #0x08	; 8
      004B4E 08                    5081 	.db #0x08	; 8
      004B4F 10                    5082 	.db #0x10	; 16
      004B50 E0                    5083 	.db #0xe0	; 224
      004B51 00                    5084 	.db #0x00	; 0
      004B52 0F                    5085 	.db #0x0f	; 15
      004B53 10                    5086 	.db #0x10	; 16
      004B54 20                    5087 	.db #0x20	; 32
      004B55 20                    5088 	.db #0x20	; 32
      004B56 20                    5089 	.db #0x20	; 32
      004B57 10                    5090 	.db #0x10	; 16
      004B58 0F                    5091 	.db #0x0f	; 15
      004B59 00                    5092 	.db #0x00	; 0
      004B5A 08                    5093 	.db #0x08	; 8
      004B5B F8                    5094 	.db #0xf8	; 248
      004B5C 08                    5095 	.db #0x08	; 8
      004B5D 08                    5096 	.db #0x08	; 8
      004B5E 08                    5097 	.db #0x08	; 8
      004B5F 08                    5098 	.db #0x08	; 8
      004B60 F0                    5099 	.db #0xf0	; 240
      004B61 00                    5100 	.db #0x00	; 0
      004B62 20                    5101 	.db #0x20	; 32
      004B63 3F                    5102 	.db #0x3f	; 63
      004B64 21                    5103 	.db #0x21	; 33
      004B65 01                    5104 	.db #0x01	; 1
      004B66 01                    5105 	.db #0x01	; 1
      004B67 01                    5106 	.db #0x01	; 1
      004B68 00                    5107 	.db #0x00	; 0
      004B69 00                    5108 	.db #0x00	; 0
      004B6A E0                    5109 	.db #0xe0	; 224
      004B6B 10                    5110 	.db #0x10	; 16
      004B6C 08                    5111 	.db #0x08	; 8
      004B6D 08                    5112 	.db #0x08	; 8
      004B6E 08                    5113 	.db #0x08	; 8
      004B6F 10                    5114 	.db #0x10	; 16
      004B70 E0                    5115 	.db #0xe0	; 224
      004B71 00                    5116 	.db #0x00	; 0
      004B72 0F                    5117 	.db #0x0f	; 15
      004B73 18                    5118 	.db #0x18	; 24
      004B74 24                    5119 	.db #0x24	; 36
      004B75 24                    5120 	.db #0x24	; 36
      004B76 38                    5121 	.db #0x38	; 56	'8'
      004B77 50                    5122 	.db #0x50	; 80	'P'
      004B78 4F                    5123 	.db #0x4f	; 79	'O'
      004B79 00                    5124 	.db #0x00	; 0
      004B7A 08                    5125 	.db #0x08	; 8
      004B7B F8                    5126 	.db #0xf8	; 248
      004B7C 88                    5127 	.db #0x88	; 136
      004B7D 88                    5128 	.db #0x88	; 136
      004B7E 88                    5129 	.db #0x88	; 136
      004B7F 88                    5130 	.db #0x88	; 136
      004B80 70                    5131 	.db #0x70	; 112	'p'
      004B81 00                    5132 	.db #0x00	; 0
      004B82 20                    5133 	.db #0x20	; 32
      004B83 3F                    5134 	.db #0x3f	; 63
      004B84 20                    5135 	.db #0x20	; 32
      004B85 00                    5136 	.db #0x00	; 0
      004B86 03                    5137 	.db #0x03	; 3
      004B87 0C                    5138 	.db #0x0c	; 12
      004B88 30                    5139 	.db #0x30	; 48	'0'
      004B89 20                    5140 	.db #0x20	; 32
      004B8A 00                    5141 	.db #0x00	; 0
      004B8B 70                    5142 	.db #0x70	; 112	'p'
      004B8C 88                    5143 	.db #0x88	; 136
      004B8D 08                    5144 	.db #0x08	; 8
      004B8E 08                    5145 	.db #0x08	; 8
      004B8F 08                    5146 	.db #0x08	; 8
      004B90 38                    5147 	.db #0x38	; 56	'8'
      004B91 00                    5148 	.db #0x00	; 0
      004B92 00                    5149 	.db #0x00	; 0
      004B93 38                    5150 	.db #0x38	; 56	'8'
      004B94 20                    5151 	.db #0x20	; 32
      004B95 21                    5152 	.db #0x21	; 33
      004B96 21                    5153 	.db #0x21	; 33
      004B97 22                    5154 	.db #0x22	; 34
      004B98 1C                    5155 	.db #0x1c	; 28
      004B99 00                    5156 	.db #0x00	; 0
      004B9A 18                    5157 	.db #0x18	; 24
      004B9B 08                    5158 	.db #0x08	; 8
      004B9C 08                    5159 	.db #0x08	; 8
      004B9D F8                    5160 	.db #0xf8	; 248
      004B9E 08                    5161 	.db #0x08	; 8
      004B9F 08                    5162 	.db #0x08	; 8
      004BA0 18                    5163 	.db #0x18	; 24
      004BA1 00                    5164 	.db #0x00	; 0
      004BA2 00                    5165 	.db #0x00	; 0
      004BA3 00                    5166 	.db #0x00	; 0
      004BA4 20                    5167 	.db #0x20	; 32
      004BA5 3F                    5168 	.db #0x3f	; 63
      004BA6 20                    5169 	.db #0x20	; 32
      004BA7 00                    5170 	.db #0x00	; 0
      004BA8 00                    5171 	.db #0x00	; 0
      004BA9 00                    5172 	.db #0x00	; 0
      004BAA 08                    5173 	.db #0x08	; 8
      004BAB F8                    5174 	.db #0xf8	; 248
      004BAC 08                    5175 	.db #0x08	; 8
      004BAD 00                    5176 	.db #0x00	; 0
      004BAE 00                    5177 	.db #0x00	; 0
      004BAF 08                    5178 	.db #0x08	; 8
      004BB0 F8                    5179 	.db #0xf8	; 248
      004BB1 08                    5180 	.db #0x08	; 8
      004BB2 00                    5181 	.db #0x00	; 0
      004BB3 1F                    5182 	.db #0x1f	; 31
      004BB4 20                    5183 	.db #0x20	; 32
      004BB5 20                    5184 	.db #0x20	; 32
      004BB6 20                    5185 	.db #0x20	; 32
      004BB7 20                    5186 	.db #0x20	; 32
      004BB8 1F                    5187 	.db #0x1f	; 31
      004BB9 00                    5188 	.db #0x00	; 0
      004BBA 08                    5189 	.db #0x08	; 8
      004BBB 78                    5190 	.db #0x78	; 120	'x'
      004BBC 88                    5191 	.db #0x88	; 136
      004BBD 00                    5192 	.db #0x00	; 0
      004BBE 00                    5193 	.db #0x00	; 0
      004BBF C8                    5194 	.db #0xc8	; 200
      004BC0 38                    5195 	.db #0x38	; 56	'8'
      004BC1 08                    5196 	.db #0x08	; 8
      004BC2 00                    5197 	.db #0x00	; 0
      004BC3 00                    5198 	.db #0x00	; 0
      004BC4 07                    5199 	.db #0x07	; 7
      004BC5 38                    5200 	.db #0x38	; 56	'8'
      004BC6 0E                    5201 	.db #0x0e	; 14
      004BC7 01                    5202 	.db #0x01	; 1
      004BC8 00                    5203 	.db #0x00	; 0
      004BC9 00                    5204 	.db #0x00	; 0
      004BCA F8                    5205 	.db #0xf8	; 248
      004BCB 08                    5206 	.db #0x08	; 8
      004BCC 00                    5207 	.db #0x00	; 0
      004BCD F8                    5208 	.db #0xf8	; 248
      004BCE 00                    5209 	.db #0x00	; 0
      004BCF 08                    5210 	.db #0x08	; 8
      004BD0 F8                    5211 	.db #0xf8	; 248
      004BD1 00                    5212 	.db #0x00	; 0
      004BD2 03                    5213 	.db #0x03	; 3
      004BD3 3C                    5214 	.db #0x3c	; 60
      004BD4 07                    5215 	.db #0x07	; 7
      004BD5 00                    5216 	.db #0x00	; 0
      004BD6 07                    5217 	.db #0x07	; 7
      004BD7 3C                    5218 	.db #0x3c	; 60
      004BD8 03                    5219 	.db #0x03	; 3
      004BD9 00                    5220 	.db #0x00	; 0
      004BDA 08                    5221 	.db #0x08	; 8
      004BDB 18                    5222 	.db #0x18	; 24
      004BDC 68                    5223 	.db #0x68	; 104	'h'
      004BDD 80                    5224 	.db #0x80	; 128
      004BDE 80                    5225 	.db #0x80	; 128
      004BDF 68                    5226 	.db #0x68	; 104	'h'
      004BE0 18                    5227 	.db #0x18	; 24
      004BE1 08                    5228 	.db #0x08	; 8
      004BE2 20                    5229 	.db #0x20	; 32
      004BE3 30                    5230 	.db #0x30	; 48	'0'
      004BE4 2C                    5231 	.db #0x2c	; 44
      004BE5 03                    5232 	.db #0x03	; 3
      004BE6 03                    5233 	.db #0x03	; 3
      004BE7 2C                    5234 	.db #0x2c	; 44
      004BE8 30                    5235 	.db #0x30	; 48	'0'
      004BE9 20                    5236 	.db #0x20	; 32
      004BEA 08                    5237 	.db #0x08	; 8
      004BEB 38                    5238 	.db #0x38	; 56	'8'
      004BEC C8                    5239 	.db #0xc8	; 200
      004BED 00                    5240 	.db #0x00	; 0
      004BEE C8                    5241 	.db #0xc8	; 200
      004BEF 38                    5242 	.db #0x38	; 56	'8'
      004BF0 08                    5243 	.db #0x08	; 8
      004BF1 00                    5244 	.db #0x00	; 0
      004BF2 00                    5245 	.db #0x00	; 0
      004BF3 00                    5246 	.db #0x00	; 0
      004BF4 20                    5247 	.db #0x20	; 32
      004BF5 3F                    5248 	.db #0x3f	; 63
      004BF6 20                    5249 	.db #0x20	; 32
      004BF7 00                    5250 	.db #0x00	; 0
      004BF8 00                    5251 	.db #0x00	; 0
      004BF9 00                    5252 	.db #0x00	; 0
      004BFA 10                    5253 	.db #0x10	; 16
      004BFB 08                    5254 	.db #0x08	; 8
      004BFC 08                    5255 	.db #0x08	; 8
      004BFD 08                    5256 	.db #0x08	; 8
      004BFE C8                    5257 	.db #0xc8	; 200
      004BFF 38                    5258 	.db #0x38	; 56	'8'
      004C00 08                    5259 	.db #0x08	; 8
      004C01 00                    5260 	.db #0x00	; 0
      004C02 20                    5261 	.db #0x20	; 32
      004C03 38                    5262 	.db #0x38	; 56	'8'
      004C04 26                    5263 	.db #0x26	; 38
      004C05 21                    5264 	.db #0x21	; 33
      004C06 20                    5265 	.db #0x20	; 32
      004C07 20                    5266 	.db #0x20	; 32
      004C08 18                    5267 	.db #0x18	; 24
      004C09 00                    5268 	.db #0x00	; 0
      004C0A 00                    5269 	.db #0x00	; 0
      004C0B 00                    5270 	.db #0x00	; 0
      004C0C 00                    5271 	.db #0x00	; 0
      004C0D FE                    5272 	.db #0xfe	; 254
      004C0E 02                    5273 	.db #0x02	; 2
      004C0F 02                    5274 	.db #0x02	; 2
      004C10 02                    5275 	.db #0x02	; 2
      004C11 00                    5276 	.db #0x00	; 0
      004C12 00                    5277 	.db #0x00	; 0
      004C13 00                    5278 	.db #0x00	; 0
      004C14 00                    5279 	.db #0x00	; 0
      004C15 7F                    5280 	.db #0x7f	; 127
      004C16 40                    5281 	.db #0x40	; 64
      004C17 40                    5282 	.db #0x40	; 64
      004C18 40                    5283 	.db #0x40	; 64
      004C19 00                    5284 	.db #0x00	; 0
      004C1A 00                    5285 	.db #0x00	; 0
      004C1B 0C                    5286 	.db #0x0c	; 12
      004C1C 30                    5287 	.db #0x30	; 48	'0'
      004C1D C0                    5288 	.db #0xc0	; 192
      004C1E 00                    5289 	.db #0x00	; 0
      004C1F 00                    5290 	.db #0x00	; 0
      004C20 00                    5291 	.db #0x00	; 0
      004C21 00                    5292 	.db #0x00	; 0
      004C22 00                    5293 	.db #0x00	; 0
      004C23 00                    5294 	.db #0x00	; 0
      004C24 00                    5295 	.db #0x00	; 0
      004C25 01                    5296 	.db #0x01	; 1
      004C26 06                    5297 	.db #0x06	; 6
      004C27 38                    5298 	.db #0x38	; 56	'8'
      004C28 C0                    5299 	.db #0xc0	; 192
      004C29 00                    5300 	.db #0x00	; 0
      004C2A 00                    5301 	.db #0x00	; 0
      004C2B 02                    5302 	.db #0x02	; 2
      004C2C 02                    5303 	.db #0x02	; 2
      004C2D 02                    5304 	.db #0x02	; 2
      004C2E FE                    5305 	.db #0xfe	; 254
      004C2F 00                    5306 	.db #0x00	; 0
      004C30 00                    5307 	.db #0x00	; 0
      004C31 00                    5308 	.db #0x00	; 0
      004C32 00                    5309 	.db #0x00	; 0
      004C33 40                    5310 	.db #0x40	; 64
      004C34 40                    5311 	.db #0x40	; 64
      004C35 40                    5312 	.db #0x40	; 64
      004C36 7F                    5313 	.db #0x7f	; 127
      004C37 00                    5314 	.db #0x00	; 0
      004C38 00                    5315 	.db #0x00	; 0
      004C39 00                    5316 	.db #0x00	; 0
      004C3A 00                    5317 	.db #0x00	; 0
      004C3B 00                    5318 	.db #0x00	; 0
      004C3C 04                    5319 	.db #0x04	; 4
      004C3D 02                    5320 	.db #0x02	; 2
      004C3E 02                    5321 	.db #0x02	; 2
      004C3F 02                    5322 	.db #0x02	; 2
      004C40 04                    5323 	.db #0x04	; 4
      004C41 00                    5324 	.db #0x00	; 0
      004C42 00                    5325 	.db #0x00	; 0
      004C43 00                    5326 	.db #0x00	; 0
      004C44 00                    5327 	.db #0x00	; 0
      004C45 00                    5328 	.db #0x00	; 0
      004C46 00                    5329 	.db #0x00	; 0
      004C47 00                    5330 	.db #0x00	; 0
      004C48 00                    5331 	.db #0x00	; 0
      004C49 00                    5332 	.db #0x00	; 0
      004C4A 00                    5333 	.db #0x00	; 0
      004C4B 00                    5334 	.db #0x00	; 0
      004C4C 00                    5335 	.db #0x00	; 0
      004C4D 00                    5336 	.db #0x00	; 0
      004C4E 00                    5337 	.db #0x00	; 0
      004C4F 00                    5338 	.db #0x00	; 0
      004C50 00                    5339 	.db #0x00	; 0
      004C51 00                    5340 	.db #0x00	; 0
      004C52 80                    5341 	.db #0x80	; 128
      004C53 80                    5342 	.db #0x80	; 128
      004C54 80                    5343 	.db #0x80	; 128
      004C55 80                    5344 	.db #0x80	; 128
      004C56 80                    5345 	.db #0x80	; 128
      004C57 80                    5346 	.db #0x80	; 128
      004C58 80                    5347 	.db #0x80	; 128
      004C59 80                    5348 	.db #0x80	; 128
      004C5A 00                    5349 	.db #0x00	; 0
      004C5B 02                    5350 	.db #0x02	; 2
      004C5C 02                    5351 	.db #0x02	; 2
      004C5D 04                    5352 	.db #0x04	; 4
      004C5E 00                    5353 	.db #0x00	; 0
      004C5F 00                    5354 	.db #0x00	; 0
      004C60 00                    5355 	.db #0x00	; 0
      004C61 00                    5356 	.db #0x00	; 0
      004C62 00                    5357 	.db #0x00	; 0
      004C63 00                    5358 	.db #0x00	; 0
      004C64 00                    5359 	.db #0x00	; 0
      004C65 00                    5360 	.db #0x00	; 0
      004C66 00                    5361 	.db #0x00	; 0
      004C67 00                    5362 	.db #0x00	; 0
      004C68 00                    5363 	.db #0x00	; 0
      004C69 00                    5364 	.db #0x00	; 0
      004C6A 00                    5365 	.db #0x00	; 0
      004C6B 00                    5366 	.db #0x00	; 0
      004C6C 80                    5367 	.db #0x80	; 128
      004C6D 80                    5368 	.db #0x80	; 128
      004C6E 80                    5369 	.db #0x80	; 128
      004C6F 80                    5370 	.db #0x80	; 128
      004C70 00                    5371 	.db #0x00	; 0
      004C71 00                    5372 	.db #0x00	; 0
      004C72 00                    5373 	.db #0x00	; 0
      004C73 19                    5374 	.db #0x19	; 25
      004C74 24                    5375 	.db #0x24	; 36
      004C75 22                    5376 	.db #0x22	; 34
      004C76 22                    5377 	.db #0x22	; 34
      004C77 22                    5378 	.db #0x22	; 34
      004C78 3F                    5379 	.db #0x3f	; 63
      004C79 20                    5380 	.db #0x20	; 32
      004C7A 08                    5381 	.db #0x08	; 8
      004C7B F8                    5382 	.db #0xf8	; 248
      004C7C 00                    5383 	.db #0x00	; 0
      004C7D 80                    5384 	.db #0x80	; 128
      004C7E 80                    5385 	.db #0x80	; 128
      004C7F 00                    5386 	.db #0x00	; 0
      004C80 00                    5387 	.db #0x00	; 0
      004C81 00                    5388 	.db #0x00	; 0
      004C82 00                    5389 	.db #0x00	; 0
      004C83 3F                    5390 	.db #0x3f	; 63
      004C84 11                    5391 	.db #0x11	; 17
      004C85 20                    5392 	.db #0x20	; 32
      004C86 20                    5393 	.db #0x20	; 32
      004C87 11                    5394 	.db #0x11	; 17
      004C88 0E                    5395 	.db #0x0e	; 14
      004C89 00                    5396 	.db #0x00	; 0
      004C8A 00                    5397 	.db #0x00	; 0
      004C8B 00                    5398 	.db #0x00	; 0
      004C8C 00                    5399 	.db #0x00	; 0
      004C8D 80                    5400 	.db #0x80	; 128
      004C8E 80                    5401 	.db #0x80	; 128
      004C8F 80                    5402 	.db #0x80	; 128
      004C90 00                    5403 	.db #0x00	; 0
      004C91 00                    5404 	.db #0x00	; 0
      004C92 00                    5405 	.db #0x00	; 0
      004C93 0E                    5406 	.db #0x0e	; 14
      004C94 11                    5407 	.db #0x11	; 17
      004C95 20                    5408 	.db #0x20	; 32
      004C96 20                    5409 	.db #0x20	; 32
      004C97 20                    5410 	.db #0x20	; 32
      004C98 11                    5411 	.db #0x11	; 17
      004C99 00                    5412 	.db #0x00	; 0
      004C9A 00                    5413 	.db #0x00	; 0
      004C9B 00                    5414 	.db #0x00	; 0
      004C9C 00                    5415 	.db #0x00	; 0
      004C9D 80                    5416 	.db #0x80	; 128
      004C9E 80                    5417 	.db #0x80	; 128
      004C9F 88                    5418 	.db #0x88	; 136
      004CA0 F8                    5419 	.db #0xf8	; 248
      004CA1 00                    5420 	.db #0x00	; 0
      004CA2 00                    5421 	.db #0x00	; 0
      004CA3 0E                    5422 	.db #0x0e	; 14
      004CA4 11                    5423 	.db #0x11	; 17
      004CA5 20                    5424 	.db #0x20	; 32
      004CA6 20                    5425 	.db #0x20	; 32
      004CA7 10                    5426 	.db #0x10	; 16
      004CA8 3F                    5427 	.db #0x3f	; 63
      004CA9 20                    5428 	.db #0x20	; 32
      004CAA 00                    5429 	.db #0x00	; 0
      004CAB 00                    5430 	.db #0x00	; 0
      004CAC 80                    5431 	.db #0x80	; 128
      004CAD 80                    5432 	.db #0x80	; 128
      004CAE 80                    5433 	.db #0x80	; 128
      004CAF 80                    5434 	.db #0x80	; 128
      004CB0 00                    5435 	.db #0x00	; 0
      004CB1 00                    5436 	.db #0x00	; 0
      004CB2 00                    5437 	.db #0x00	; 0
      004CB3 1F                    5438 	.db #0x1f	; 31
      004CB4 22                    5439 	.db #0x22	; 34
      004CB5 22                    5440 	.db #0x22	; 34
      004CB6 22                    5441 	.db #0x22	; 34
      004CB7 22                    5442 	.db #0x22	; 34
      004CB8 13                    5443 	.db #0x13	; 19
      004CB9 00                    5444 	.db #0x00	; 0
      004CBA 00                    5445 	.db #0x00	; 0
      004CBB 80                    5446 	.db #0x80	; 128
      004CBC 80                    5447 	.db #0x80	; 128
      004CBD F0                    5448 	.db #0xf0	; 240
      004CBE 88                    5449 	.db #0x88	; 136
      004CBF 88                    5450 	.db #0x88	; 136
      004CC0 88                    5451 	.db #0x88	; 136
      004CC1 18                    5452 	.db #0x18	; 24
      004CC2 00                    5453 	.db #0x00	; 0
      004CC3 20                    5454 	.db #0x20	; 32
      004CC4 20                    5455 	.db #0x20	; 32
      004CC5 3F                    5456 	.db #0x3f	; 63
      004CC6 20                    5457 	.db #0x20	; 32
      004CC7 20                    5458 	.db #0x20	; 32
      004CC8 00                    5459 	.db #0x00	; 0
      004CC9 00                    5460 	.db #0x00	; 0
      004CCA 00                    5461 	.db #0x00	; 0
      004CCB 00                    5462 	.db #0x00	; 0
      004CCC 80                    5463 	.db #0x80	; 128
      004CCD 80                    5464 	.db #0x80	; 128
      004CCE 80                    5465 	.db #0x80	; 128
      004CCF 80                    5466 	.db #0x80	; 128
      004CD0 80                    5467 	.db #0x80	; 128
      004CD1 00                    5468 	.db #0x00	; 0
      004CD2 00                    5469 	.db #0x00	; 0
      004CD3 6B                    5470 	.db #0x6b	; 107	'k'
      004CD4 94                    5471 	.db #0x94	; 148
      004CD5 94                    5472 	.db #0x94	; 148
      004CD6 94                    5473 	.db #0x94	; 148
      004CD7 93                    5474 	.db #0x93	; 147
      004CD8 60                    5475 	.db #0x60	; 96
      004CD9 00                    5476 	.db #0x00	; 0
      004CDA 08                    5477 	.db #0x08	; 8
      004CDB F8                    5478 	.db #0xf8	; 248
      004CDC 00                    5479 	.db #0x00	; 0
      004CDD 80                    5480 	.db #0x80	; 128
      004CDE 80                    5481 	.db #0x80	; 128
      004CDF 80                    5482 	.db #0x80	; 128
      004CE0 00                    5483 	.db #0x00	; 0
      004CE1 00                    5484 	.db #0x00	; 0
      004CE2 20                    5485 	.db #0x20	; 32
      004CE3 3F                    5486 	.db #0x3f	; 63
      004CE4 21                    5487 	.db #0x21	; 33
      004CE5 00                    5488 	.db #0x00	; 0
      004CE6 00                    5489 	.db #0x00	; 0
      004CE7 20                    5490 	.db #0x20	; 32
      004CE8 3F                    5491 	.db #0x3f	; 63
      004CE9 20                    5492 	.db #0x20	; 32
      004CEA 00                    5493 	.db #0x00	; 0
      004CEB 80                    5494 	.db #0x80	; 128
      004CEC 98                    5495 	.db #0x98	; 152
      004CED 98                    5496 	.db #0x98	; 152
      004CEE 00                    5497 	.db #0x00	; 0
      004CEF 00                    5498 	.db #0x00	; 0
      004CF0 00                    5499 	.db #0x00	; 0
      004CF1 00                    5500 	.db #0x00	; 0
      004CF2 00                    5501 	.db #0x00	; 0
      004CF3 20                    5502 	.db #0x20	; 32
      004CF4 20                    5503 	.db #0x20	; 32
      004CF5 3F                    5504 	.db #0x3f	; 63
      004CF6 20                    5505 	.db #0x20	; 32
      004CF7 20                    5506 	.db #0x20	; 32
      004CF8 00                    5507 	.db #0x00	; 0
      004CF9 00                    5508 	.db #0x00	; 0
      004CFA 00                    5509 	.db #0x00	; 0
      004CFB 00                    5510 	.db #0x00	; 0
      004CFC 00                    5511 	.db #0x00	; 0
      004CFD 80                    5512 	.db #0x80	; 128
      004CFE 98                    5513 	.db #0x98	; 152
      004CFF 98                    5514 	.db #0x98	; 152
      004D00 00                    5515 	.db #0x00	; 0
      004D01 00                    5516 	.db #0x00	; 0
      004D02 00                    5517 	.db #0x00	; 0
      004D03 C0                    5518 	.db #0xc0	; 192
      004D04 80                    5519 	.db #0x80	; 128
      004D05 80                    5520 	.db #0x80	; 128
      004D06 80                    5521 	.db #0x80	; 128
      004D07 7F                    5522 	.db #0x7f	; 127
      004D08 00                    5523 	.db #0x00	; 0
      004D09 00                    5524 	.db #0x00	; 0
      004D0A 08                    5525 	.db #0x08	; 8
      004D0B F8                    5526 	.db #0xf8	; 248
      004D0C 00                    5527 	.db #0x00	; 0
      004D0D 00                    5528 	.db #0x00	; 0
      004D0E 80                    5529 	.db #0x80	; 128
      004D0F 80                    5530 	.db #0x80	; 128
      004D10 80                    5531 	.db #0x80	; 128
      004D11 00                    5532 	.db #0x00	; 0
      004D12 20                    5533 	.db #0x20	; 32
      004D13 3F                    5534 	.db #0x3f	; 63
      004D14 24                    5535 	.db #0x24	; 36
      004D15 02                    5536 	.db #0x02	; 2
      004D16 2D                    5537 	.db #0x2d	; 45
      004D17 30                    5538 	.db #0x30	; 48	'0'
      004D18 20                    5539 	.db #0x20	; 32
      004D19 00                    5540 	.db #0x00	; 0
      004D1A 00                    5541 	.db #0x00	; 0
      004D1B 08                    5542 	.db #0x08	; 8
      004D1C 08                    5543 	.db #0x08	; 8
      004D1D F8                    5544 	.db #0xf8	; 248
      004D1E 00                    5545 	.db #0x00	; 0
      004D1F 00                    5546 	.db #0x00	; 0
      004D20 00                    5547 	.db #0x00	; 0
      004D21 00                    5548 	.db #0x00	; 0
      004D22 00                    5549 	.db #0x00	; 0
      004D23 20                    5550 	.db #0x20	; 32
      004D24 20                    5551 	.db #0x20	; 32
      004D25 3F                    5552 	.db #0x3f	; 63
      004D26 20                    5553 	.db #0x20	; 32
      004D27 20                    5554 	.db #0x20	; 32
      004D28 00                    5555 	.db #0x00	; 0
      004D29 00                    5556 	.db #0x00	; 0
      004D2A 80                    5557 	.db #0x80	; 128
      004D2B 80                    5558 	.db #0x80	; 128
      004D2C 80                    5559 	.db #0x80	; 128
      004D2D 80                    5560 	.db #0x80	; 128
      004D2E 80                    5561 	.db #0x80	; 128
      004D2F 80                    5562 	.db #0x80	; 128
      004D30 80                    5563 	.db #0x80	; 128
      004D31 00                    5564 	.db #0x00	; 0
      004D32 20                    5565 	.db #0x20	; 32
      004D33 3F                    5566 	.db #0x3f	; 63
      004D34 20                    5567 	.db #0x20	; 32
      004D35 00                    5568 	.db #0x00	; 0
      004D36 3F                    5569 	.db #0x3f	; 63
      004D37 20                    5570 	.db #0x20	; 32
      004D38 00                    5571 	.db #0x00	; 0
      004D39 3F                    5572 	.db #0x3f	; 63
      004D3A 80                    5573 	.db #0x80	; 128
      004D3B 80                    5574 	.db #0x80	; 128
      004D3C 00                    5575 	.db #0x00	; 0
      004D3D 80                    5576 	.db #0x80	; 128
      004D3E 80                    5577 	.db #0x80	; 128
      004D3F 80                    5578 	.db #0x80	; 128
      004D40 00                    5579 	.db #0x00	; 0
      004D41 00                    5580 	.db #0x00	; 0
      004D42 20                    5581 	.db #0x20	; 32
      004D43 3F                    5582 	.db #0x3f	; 63
      004D44 21                    5583 	.db #0x21	; 33
      004D45 00                    5584 	.db #0x00	; 0
      004D46 00                    5585 	.db #0x00	; 0
      004D47 20                    5586 	.db #0x20	; 32
      004D48 3F                    5587 	.db #0x3f	; 63
      004D49 20                    5588 	.db #0x20	; 32
      004D4A 00                    5589 	.db #0x00	; 0
      004D4B 00                    5590 	.db #0x00	; 0
      004D4C 80                    5591 	.db #0x80	; 128
      004D4D 80                    5592 	.db #0x80	; 128
      004D4E 80                    5593 	.db #0x80	; 128
      004D4F 80                    5594 	.db #0x80	; 128
      004D50 00                    5595 	.db #0x00	; 0
      004D51 00                    5596 	.db #0x00	; 0
      004D52 00                    5597 	.db #0x00	; 0
      004D53 1F                    5598 	.db #0x1f	; 31
      004D54 20                    5599 	.db #0x20	; 32
      004D55 20                    5600 	.db #0x20	; 32
      004D56 20                    5601 	.db #0x20	; 32
      004D57 20                    5602 	.db #0x20	; 32
      004D58 1F                    5603 	.db #0x1f	; 31
      004D59 00                    5604 	.db #0x00	; 0
      004D5A 80                    5605 	.db #0x80	; 128
      004D5B 80                    5606 	.db #0x80	; 128
      004D5C 00                    5607 	.db #0x00	; 0
      004D5D 80                    5608 	.db #0x80	; 128
      004D5E 80                    5609 	.db #0x80	; 128
      004D5F 00                    5610 	.db #0x00	; 0
      004D60 00                    5611 	.db #0x00	; 0
      004D61 00                    5612 	.db #0x00	; 0
      004D62 80                    5613 	.db #0x80	; 128
      004D63 FF                    5614 	.db #0xff	; 255
      004D64 A1                    5615 	.db #0xa1	; 161
      004D65 20                    5616 	.db #0x20	; 32
      004D66 20                    5617 	.db #0x20	; 32
      004D67 11                    5618 	.db #0x11	; 17
      004D68 0E                    5619 	.db #0x0e	; 14
      004D69 00                    5620 	.db #0x00	; 0
      004D6A 00                    5621 	.db #0x00	; 0
      004D6B 00                    5622 	.db #0x00	; 0
      004D6C 00                    5623 	.db #0x00	; 0
      004D6D 80                    5624 	.db #0x80	; 128
      004D6E 80                    5625 	.db #0x80	; 128
      004D6F 80                    5626 	.db #0x80	; 128
      004D70 80                    5627 	.db #0x80	; 128
      004D71 00                    5628 	.db #0x00	; 0
      004D72 00                    5629 	.db #0x00	; 0
      004D73 0E                    5630 	.db #0x0e	; 14
      004D74 11                    5631 	.db #0x11	; 17
      004D75 20                    5632 	.db #0x20	; 32
      004D76 20                    5633 	.db #0x20	; 32
      004D77 A0                    5634 	.db #0xa0	; 160
      004D78 FF                    5635 	.db #0xff	; 255
      004D79 80                    5636 	.db #0x80	; 128
      004D7A 80                    5637 	.db #0x80	; 128
      004D7B 80                    5638 	.db #0x80	; 128
      004D7C 80                    5639 	.db #0x80	; 128
      004D7D 00                    5640 	.db #0x00	; 0
      004D7E 80                    5641 	.db #0x80	; 128
      004D7F 80                    5642 	.db #0x80	; 128
      004D80 80                    5643 	.db #0x80	; 128
      004D81 00                    5644 	.db #0x00	; 0
      004D82 20                    5645 	.db #0x20	; 32
      004D83 20                    5646 	.db #0x20	; 32
      004D84 3F                    5647 	.db #0x3f	; 63
      004D85 21                    5648 	.db #0x21	; 33
      004D86 20                    5649 	.db #0x20	; 32
      004D87 00                    5650 	.db #0x00	; 0
      004D88 01                    5651 	.db #0x01	; 1
      004D89 00                    5652 	.db #0x00	; 0
      004D8A 00                    5653 	.db #0x00	; 0
      004D8B 00                    5654 	.db #0x00	; 0
      004D8C 80                    5655 	.db #0x80	; 128
      004D8D 80                    5656 	.db #0x80	; 128
      004D8E 80                    5657 	.db #0x80	; 128
      004D8F 80                    5658 	.db #0x80	; 128
      004D90 80                    5659 	.db #0x80	; 128
      004D91 00                    5660 	.db #0x00	; 0
      004D92 00                    5661 	.db #0x00	; 0
      004D93 33                    5662 	.db #0x33	; 51	'3'
      004D94 24                    5663 	.db #0x24	; 36
      004D95 24                    5664 	.db #0x24	; 36
      004D96 24                    5665 	.db #0x24	; 36
      004D97 24                    5666 	.db #0x24	; 36
      004D98 19                    5667 	.db #0x19	; 25
      004D99 00                    5668 	.db #0x00	; 0
      004D9A 00                    5669 	.db #0x00	; 0
      004D9B 80                    5670 	.db #0x80	; 128
      004D9C 80                    5671 	.db #0x80	; 128
      004D9D E0                    5672 	.db #0xe0	; 224
      004D9E 80                    5673 	.db #0x80	; 128
      004D9F 80                    5674 	.db #0x80	; 128
      004DA0 00                    5675 	.db #0x00	; 0
      004DA1 00                    5676 	.db #0x00	; 0
      004DA2 00                    5677 	.db #0x00	; 0
      004DA3 00                    5678 	.db #0x00	; 0
      004DA4 00                    5679 	.db #0x00	; 0
      004DA5 1F                    5680 	.db #0x1f	; 31
      004DA6 20                    5681 	.db #0x20	; 32
      004DA7 20                    5682 	.db #0x20	; 32
      004DA8 00                    5683 	.db #0x00	; 0
      004DA9 00                    5684 	.db #0x00	; 0
      004DAA 80                    5685 	.db #0x80	; 128
      004DAB 80                    5686 	.db #0x80	; 128
      004DAC 00                    5687 	.db #0x00	; 0
      004DAD 00                    5688 	.db #0x00	; 0
      004DAE 00                    5689 	.db #0x00	; 0
      004DAF 80                    5690 	.db #0x80	; 128
      004DB0 80                    5691 	.db #0x80	; 128
      004DB1 00                    5692 	.db #0x00	; 0
      004DB2 00                    5693 	.db #0x00	; 0
      004DB3 1F                    5694 	.db #0x1f	; 31
      004DB4 20                    5695 	.db #0x20	; 32
      004DB5 20                    5696 	.db #0x20	; 32
      004DB6 20                    5697 	.db #0x20	; 32
      004DB7 10                    5698 	.db #0x10	; 16
      004DB8 3F                    5699 	.db #0x3f	; 63
      004DB9 20                    5700 	.db #0x20	; 32
      004DBA 80                    5701 	.db #0x80	; 128
      004DBB 80                    5702 	.db #0x80	; 128
      004DBC 80                    5703 	.db #0x80	; 128
      004DBD 00                    5704 	.db #0x00	; 0
      004DBE 00                    5705 	.db #0x00	; 0
      004DBF 80                    5706 	.db #0x80	; 128
      004DC0 80                    5707 	.db #0x80	; 128
      004DC1 80                    5708 	.db #0x80	; 128
      004DC2 00                    5709 	.db #0x00	; 0
      004DC3 01                    5710 	.db #0x01	; 1
      004DC4 0E                    5711 	.db #0x0e	; 14
      004DC5 30                    5712 	.db #0x30	; 48	'0'
      004DC6 08                    5713 	.db #0x08	; 8
      004DC7 06                    5714 	.db #0x06	; 6
      004DC8 01                    5715 	.db #0x01	; 1
      004DC9 00                    5716 	.db #0x00	; 0
      004DCA 80                    5717 	.db #0x80	; 128
      004DCB 80                    5718 	.db #0x80	; 128
      004DCC 00                    5719 	.db #0x00	; 0
      004DCD 80                    5720 	.db #0x80	; 128
      004DCE 00                    5721 	.db #0x00	; 0
      004DCF 80                    5722 	.db #0x80	; 128
      004DD0 80                    5723 	.db #0x80	; 128
      004DD1 80                    5724 	.db #0x80	; 128
      004DD2 0F                    5725 	.db #0x0f	; 15
      004DD3 30                    5726 	.db #0x30	; 48	'0'
      004DD4 0C                    5727 	.db #0x0c	; 12
      004DD5 03                    5728 	.db #0x03	; 3
      004DD6 0C                    5729 	.db #0x0c	; 12
      004DD7 30                    5730 	.db #0x30	; 48	'0'
      004DD8 0F                    5731 	.db #0x0f	; 15
      004DD9 00                    5732 	.db #0x00	; 0
      004DDA 00                    5733 	.db #0x00	; 0
      004DDB 80                    5734 	.db #0x80	; 128
      004DDC 80                    5735 	.db #0x80	; 128
      004DDD 00                    5736 	.db #0x00	; 0
      004DDE 80                    5737 	.db #0x80	; 128
      004DDF 80                    5738 	.db #0x80	; 128
      004DE0 80                    5739 	.db #0x80	; 128
      004DE1 00                    5740 	.db #0x00	; 0
      004DE2 00                    5741 	.db #0x00	; 0
      004DE3 20                    5742 	.db #0x20	; 32
      004DE4 31                    5743 	.db #0x31	; 49	'1'
      004DE5 2E                    5744 	.db #0x2e	; 46
      004DE6 0E                    5745 	.db #0x0e	; 14
      004DE7 31                    5746 	.db #0x31	; 49	'1'
      004DE8 20                    5747 	.db #0x20	; 32
      004DE9 00                    5748 	.db #0x00	; 0
      004DEA 80                    5749 	.db #0x80	; 128
      004DEB 80                    5750 	.db #0x80	; 128
      004DEC 80                    5751 	.db #0x80	; 128
      004DED 00                    5752 	.db #0x00	; 0
      004DEE 00                    5753 	.db #0x00	; 0
      004DEF 80                    5754 	.db #0x80	; 128
      004DF0 80                    5755 	.db #0x80	; 128
      004DF1 80                    5756 	.db #0x80	; 128
      004DF2 80                    5757 	.db #0x80	; 128
      004DF3 81                    5758 	.db #0x81	; 129
      004DF4 8E                    5759 	.db #0x8e	; 142
      004DF5 70                    5760 	.db #0x70	; 112	'p'
      004DF6 18                    5761 	.db #0x18	; 24
      004DF7 06                    5762 	.db #0x06	; 6
      004DF8 01                    5763 	.db #0x01	; 1
      004DF9 00                    5764 	.db #0x00	; 0
      004DFA 00                    5765 	.db #0x00	; 0
      004DFB 80                    5766 	.db #0x80	; 128
      004DFC 80                    5767 	.db #0x80	; 128
      004DFD 80                    5768 	.db #0x80	; 128
      004DFE 80                    5769 	.db #0x80	; 128
      004DFF 80                    5770 	.db #0x80	; 128
      004E00 80                    5771 	.db #0x80	; 128
      004E01 00                    5772 	.db #0x00	; 0
      004E02 00                    5773 	.db #0x00	; 0
      004E03 21                    5774 	.db #0x21	; 33
      004E04 30                    5775 	.db #0x30	; 48	'0'
      004E05 2C                    5776 	.db #0x2c	; 44
      004E06 22                    5777 	.db #0x22	; 34
      004E07 21                    5778 	.db #0x21	; 33
      004E08 30                    5779 	.db #0x30	; 48	'0'
      004E09 00                    5780 	.db #0x00	; 0
      004E0A 00                    5781 	.db #0x00	; 0
      004E0B 00                    5782 	.db #0x00	; 0
      004E0C 00                    5783 	.db #0x00	; 0
      004E0D 00                    5784 	.db #0x00	; 0
      004E0E 80                    5785 	.db #0x80	; 128
      004E0F 7C                    5786 	.db #0x7c	; 124
      004E10 02                    5787 	.db #0x02	; 2
      004E11 02                    5788 	.db #0x02	; 2
      004E12 00                    5789 	.db #0x00	; 0
      004E13 00                    5790 	.db #0x00	; 0
      004E14 00                    5791 	.db #0x00	; 0
      004E15 00                    5792 	.db #0x00	; 0
      004E16 00                    5793 	.db #0x00	; 0
      004E17 3F                    5794 	.db #0x3f	; 63
      004E18 40                    5795 	.db #0x40	; 64
      004E19 40                    5796 	.db #0x40	; 64
      004E1A 00                    5797 	.db #0x00	; 0
      004E1B 00                    5798 	.db #0x00	; 0
      004E1C 00                    5799 	.db #0x00	; 0
      004E1D 00                    5800 	.db #0x00	; 0
      004E1E FF                    5801 	.db #0xff	; 255
      004E1F 00                    5802 	.db #0x00	; 0
      004E20 00                    5803 	.db #0x00	; 0
      004E21 00                    5804 	.db #0x00	; 0
      004E22 00                    5805 	.db #0x00	; 0
      004E23 00                    5806 	.db #0x00	; 0
      004E24 00                    5807 	.db #0x00	; 0
      004E25 00                    5808 	.db #0x00	; 0
      004E26 FF                    5809 	.db #0xff	; 255
      004E27 00                    5810 	.db #0x00	; 0
      004E28 00                    5811 	.db #0x00	; 0
      004E29 00                    5812 	.db #0x00	; 0
      004E2A 00                    5813 	.db #0x00	; 0
      004E2B 02                    5814 	.db #0x02	; 2
      004E2C 02                    5815 	.db #0x02	; 2
      004E2D 7C                    5816 	.db #0x7c	; 124
      004E2E 80                    5817 	.db #0x80	; 128
      004E2F 00                    5818 	.db #0x00	; 0
      004E30 00                    5819 	.db #0x00	; 0
      004E31 00                    5820 	.db #0x00	; 0
      004E32 00                    5821 	.db #0x00	; 0
      004E33 40                    5822 	.db #0x40	; 64
      004E34 40                    5823 	.db #0x40	; 64
      004E35 3F                    5824 	.db #0x3f	; 63
      004E36 00                    5825 	.db #0x00	; 0
      004E37 00                    5826 	.db #0x00	; 0
      004E38 00                    5827 	.db #0x00	; 0
      004E39 00                    5828 	.db #0x00	; 0
      004E3A 00                    5829 	.db #0x00	; 0
      004E3B 06                    5830 	.db #0x06	; 6
      004E3C 01                    5831 	.db #0x01	; 1
      004E3D 01                    5832 	.db #0x01	; 1
      004E3E 02                    5833 	.db #0x02	; 2
      004E3F 02                    5834 	.db #0x02	; 2
      004E40 04                    5835 	.db #0x04	; 4
      004E41 04                    5836 	.db #0x04	; 4
      004E42 00                    5837 	.db #0x00	; 0
      004E43 00                    5838 	.db #0x00	; 0
      004E44 00                    5839 	.db #0x00	; 0
      004E45 00                    5840 	.db #0x00	; 0
      004E46 00                    5841 	.db #0x00	; 0
      004E47 00                    5842 	.db #0x00	; 0
      004E48 00                    5843 	.db #0x00	; 0
      004E49 00                    5844 	.db #0x00	; 0
      004E4A                       5845 _Hzk:
      004E4A 00                    5846 	.db #0x00	; 0
      004E4B 00                    5847 	.db #0x00	; 0
      004E4C F0                    5848 	.db #0xf0	; 240
      004E4D 10                    5849 	.db #0x10	; 16
      004E4E 10                    5850 	.db #0x10	; 16
      004E4F 10                    5851 	.db #0x10	; 16
      004E50 10                    5852 	.db #0x10	; 16
      004E51 FF                    5853 	.db #0xff	; 255
      004E52 10                    5854 	.db #0x10	; 16
      004E53 10                    5855 	.db #0x10	; 16
      004E54 10                    5856 	.db #0x10	; 16
      004E55 10                    5857 	.db #0x10	; 16
      004E56 F0                    5858 	.db #0xf0	; 240
      004E57 00                    5859 	.db #0x00	; 0
      004E58 00                    5860 	.db #0x00	; 0
      004E59 00                    5861 	.db #0x00	; 0
      004E5A 00                    5862 	.db 0x00
      004E5B 00                    5863 	.db 0x00
      004E5C 00                    5864 	.db 0x00
      004E5D 00                    5865 	.db 0x00
      004E5E 00                    5866 	.db 0x00
      004E5F 00                    5867 	.db 0x00
      004E60 00                    5868 	.db 0x00
      004E61 00                    5869 	.db 0x00
      004E62 00                    5870 	.db 0x00
      004E63 00                    5871 	.db 0x00
      004E64 00                    5872 	.db 0x00
      004E65 00                    5873 	.db 0x00
      004E66 00                    5874 	.db 0x00
      004E67 00                    5875 	.db 0x00
      004E68 00                    5876 	.db 0x00
      004E69 00                    5877 	.db 0x00
      004E6A 00                    5878 	.db #0x00	; 0
      004E6B 00                    5879 	.db #0x00	; 0
      004E6C 0F                    5880 	.db #0x0f	; 15
      004E6D 04                    5881 	.db #0x04	; 4
      004E6E 04                    5882 	.db #0x04	; 4
      004E6F 04                    5883 	.db #0x04	; 4
      004E70 04                    5884 	.db #0x04	; 4
      004E71 FF                    5885 	.db #0xff	; 255
      004E72 04                    5886 	.db #0x04	; 4
      004E73 04                    5887 	.db #0x04	; 4
      004E74 04                    5888 	.db #0x04	; 4
      004E75 04                    5889 	.db #0x04	; 4
      004E76 0F                    5890 	.db #0x0f	; 15
      004E77 00                    5891 	.db #0x00	; 0
      004E78 00                    5892 	.db #0x00	; 0
      004E79 00                    5893 	.db #0x00	; 0
      004E7A 00                    5894 	.db 0x00
      004E7B 00                    5895 	.db 0x00
      004E7C 00                    5896 	.db 0x00
      004E7D 00                    5897 	.db 0x00
      004E7E 00                    5898 	.db 0x00
      004E7F 00                    5899 	.db 0x00
      004E80 00                    5900 	.db 0x00
      004E81 00                    5901 	.db 0x00
      004E82 00                    5902 	.db 0x00
      004E83 00                    5903 	.db 0x00
      004E84 00                    5904 	.db 0x00
      004E85 00                    5905 	.db 0x00
      004E86 00                    5906 	.db 0x00
      004E87 00                    5907 	.db 0x00
      004E88 00                    5908 	.db 0x00
      004E89 00                    5909 	.db 0x00
      004E8A 40                    5910 	.db #0x40	; 64
      004E8B 40                    5911 	.db #0x40	; 64
      004E8C 40                    5912 	.db #0x40	; 64
      004E8D 5F                    5913 	.db #0x5f	; 95
      004E8E 55                    5914 	.db #0x55	; 85	'U'
      004E8F 55                    5915 	.db #0x55	; 85	'U'
      004E90 55                    5916 	.db #0x55	; 85	'U'
      004E91 75                    5917 	.db #0x75	; 117	'u'
      004E92 55                    5918 	.db #0x55	; 85	'U'
      004E93 55                    5919 	.db #0x55	; 85	'U'
      004E94 55                    5920 	.db #0x55	; 85	'U'
      004E95 5F                    5921 	.db #0x5f	; 95
      004E96 40                    5922 	.db #0x40	; 64
      004E97 40                    5923 	.db #0x40	; 64
      004E98 40                    5924 	.db #0x40	; 64
      004E99 00                    5925 	.db #0x00	; 0
      004E9A 00                    5926 	.db 0x00
      004E9B 00                    5927 	.db 0x00
      004E9C 00                    5928 	.db 0x00
      004E9D 00                    5929 	.db 0x00
      004E9E 00                    5930 	.db 0x00
      004E9F 00                    5931 	.db 0x00
      004EA0 00                    5932 	.db 0x00
      004EA1 00                    5933 	.db 0x00
      004EA2 00                    5934 	.db 0x00
      004EA3 00                    5935 	.db 0x00
      004EA4 00                    5936 	.db 0x00
      004EA5 00                    5937 	.db 0x00
      004EA6 00                    5938 	.db 0x00
      004EA7 00                    5939 	.db 0x00
      004EA8 00                    5940 	.db 0x00
      004EA9 00                    5941 	.db 0x00
      004EAA 00                    5942 	.db #0x00	; 0
      004EAB 40                    5943 	.db #0x40	; 64
      004EAC 20                    5944 	.db #0x20	; 32
      004EAD 0F                    5945 	.db #0x0f	; 15
      004EAE 09                    5946 	.db #0x09	; 9
      004EAF 49                    5947 	.db #0x49	; 73	'I'
      004EB0 89                    5948 	.db #0x89	; 137
      004EB1 79                    5949 	.db #0x79	; 121	'y'
      004EB2 09                    5950 	.db #0x09	; 9
      004EB3 09                    5951 	.db #0x09	; 9
      004EB4 09                    5952 	.db #0x09	; 9
      004EB5 0F                    5953 	.db #0x0f	; 15
      004EB6 20                    5954 	.db #0x20	; 32
      004EB7 40                    5955 	.db #0x40	; 64
      004EB8 00                    5956 	.db #0x00	; 0
      004EB9 00                    5957 	.db #0x00	; 0
      004EBA 00                    5958 	.db 0x00
      004EBB 00                    5959 	.db 0x00
      004EBC 00                    5960 	.db 0x00
      004EBD 00                    5961 	.db 0x00
      004EBE 00                    5962 	.db 0x00
      004EBF 00                    5963 	.db 0x00
      004EC0 00                    5964 	.db 0x00
      004EC1 00                    5965 	.db 0x00
      004EC2 00                    5966 	.db 0x00
      004EC3 00                    5967 	.db 0x00
      004EC4 00                    5968 	.db 0x00
      004EC5 00                    5969 	.db 0x00
      004EC6 00                    5970 	.db 0x00
      004EC7 00                    5971 	.db 0x00
      004EC8 00                    5972 	.db 0x00
      004EC9 00                    5973 	.db 0x00
      004ECA 00                    5974 	.db #0x00	; 0
      004ECB FE                    5975 	.db #0xfe	; 254
      004ECC 02                    5976 	.db #0x02	; 2
      004ECD 42                    5977 	.db #0x42	; 66	'B'
      004ECE 4A                    5978 	.db #0x4a	; 74	'J'
      004ECF CA                    5979 	.db #0xca	; 202
      004ED0 4A                    5980 	.db #0x4a	; 74	'J'
      004ED1 4A                    5981 	.db #0x4a	; 74	'J'
      004ED2 CA                    5982 	.db #0xca	; 202
      004ED3 4A                    5983 	.db #0x4a	; 74	'J'
      004ED4 4A                    5984 	.db #0x4a	; 74	'J'
      004ED5 42                    5985 	.db #0x42	; 66	'B'
      004ED6 02                    5986 	.db #0x02	; 2
      004ED7 FE                    5987 	.db #0xfe	; 254
      004ED8 00                    5988 	.db #0x00	; 0
      004ED9 00                    5989 	.db #0x00	; 0
      004EDA 00                    5990 	.db 0x00
      004EDB 00                    5991 	.db 0x00
      004EDC 00                    5992 	.db 0x00
      004EDD 00                    5993 	.db 0x00
      004EDE 00                    5994 	.db 0x00
      004EDF 00                    5995 	.db 0x00
      004EE0 00                    5996 	.db 0x00
      004EE1 00                    5997 	.db 0x00
      004EE2 00                    5998 	.db 0x00
      004EE3 00                    5999 	.db 0x00
      004EE4 00                    6000 	.db 0x00
      004EE5 00                    6001 	.db 0x00
      004EE6 00                    6002 	.db 0x00
      004EE7 00                    6003 	.db 0x00
      004EE8 00                    6004 	.db 0x00
      004EE9 00                    6005 	.db 0x00
      004EEA 00                    6006 	.db #0x00	; 0
      004EEB FF                    6007 	.db #0xff	; 255
      004EEC 40                    6008 	.db #0x40	; 64
      004EED 50                    6009 	.db #0x50	; 80	'P'
      004EEE 4C                    6010 	.db #0x4c	; 76	'L'
      004EEF 43                    6011 	.db #0x43	; 67	'C'
      004EF0 40                    6012 	.db #0x40	; 64
      004EF1 40                    6013 	.db #0x40	; 64
      004EF2 4F                    6014 	.db #0x4f	; 79	'O'
      004EF3 50                    6015 	.db #0x50	; 80	'P'
      004EF4 50                    6016 	.db #0x50	; 80	'P'
      004EF5 5C                    6017 	.db #0x5c	; 92
      004EF6 40                    6018 	.db #0x40	; 64
      004EF7 FF                    6019 	.db #0xff	; 255
      004EF8 00                    6020 	.db #0x00	; 0
      004EF9 00                    6021 	.db #0x00	; 0
      004EFA 00                    6022 	.db 0x00
      004EFB 00                    6023 	.db 0x00
      004EFC 00                    6024 	.db 0x00
      004EFD 00                    6025 	.db 0x00
      004EFE 00                    6026 	.db 0x00
      004EFF 00                    6027 	.db 0x00
      004F00 00                    6028 	.db 0x00
      004F01 00                    6029 	.db 0x00
      004F02 00                    6030 	.db 0x00
      004F03 00                    6031 	.db 0x00
      004F04 00                    6032 	.db 0x00
      004F05 00                    6033 	.db 0x00
      004F06 00                    6034 	.db 0x00
      004F07 00                    6035 	.db 0x00
      004F08 00                    6036 	.db 0x00
      004F09 00                    6037 	.db 0x00
      004F0A 00                    6038 	.db #0x00	; 0
      004F0B 00                    6039 	.db #0x00	; 0
      004F0C F8                    6040 	.db #0xf8	; 248
      004F0D 88                    6041 	.db #0x88	; 136
      004F0E 88                    6042 	.db #0x88	; 136
      004F0F 88                    6043 	.db #0x88	; 136
      004F10 88                    6044 	.db #0x88	; 136
      004F11 FF                    6045 	.db #0xff	; 255
      004F12 88                    6046 	.db #0x88	; 136
      004F13 88                    6047 	.db #0x88	; 136
      004F14 88                    6048 	.db #0x88	; 136
      004F15 88                    6049 	.db #0x88	; 136
      004F16 F8                    6050 	.db #0xf8	; 248
      004F17 00                    6051 	.db #0x00	; 0
      004F18 00                    6052 	.db #0x00	; 0
      004F19 00                    6053 	.db #0x00	; 0
      004F1A 00                    6054 	.db 0x00
      004F1B 00                    6055 	.db 0x00
      004F1C 00                    6056 	.db 0x00
      004F1D 00                    6057 	.db 0x00
      004F1E 00                    6058 	.db 0x00
      004F1F 00                    6059 	.db 0x00
      004F20 00                    6060 	.db 0x00
      004F21 00                    6061 	.db 0x00
      004F22 00                    6062 	.db 0x00
      004F23 00                    6063 	.db 0x00
      004F24 00                    6064 	.db 0x00
      004F25 00                    6065 	.db 0x00
      004F26 00                    6066 	.db 0x00
      004F27 00                    6067 	.db 0x00
      004F28 00                    6068 	.db 0x00
      004F29 00                    6069 	.db 0x00
      004F2A 00                    6070 	.db #0x00	; 0
      004F2B 00                    6071 	.db #0x00	; 0
      004F2C 1F                    6072 	.db #0x1f	; 31
      004F2D 08                    6073 	.db #0x08	; 8
      004F2E 08                    6074 	.db #0x08	; 8
      004F2F 08                    6075 	.db #0x08	; 8
      004F30 08                    6076 	.db #0x08	; 8
      004F31 7F                    6077 	.db #0x7f	; 127
      004F32 88                    6078 	.db #0x88	; 136
      004F33 88                    6079 	.db #0x88	; 136
      004F34 88                    6080 	.db #0x88	; 136
      004F35 88                    6081 	.db #0x88	; 136
      004F36 9F                    6082 	.db #0x9f	; 159
      004F37 80                    6083 	.db #0x80	; 128
      004F38 F0                    6084 	.db #0xf0	; 240
      004F39 00                    6085 	.db #0x00	; 0
      004F3A 00                    6086 	.db 0x00
      004F3B 00                    6087 	.db 0x00
      004F3C 00                    6088 	.db 0x00
      004F3D 00                    6089 	.db 0x00
      004F3E 00                    6090 	.db 0x00
      004F3F 00                    6091 	.db 0x00
      004F40 00                    6092 	.db 0x00
      004F41 00                    6093 	.db 0x00
      004F42 00                    6094 	.db 0x00
      004F43 00                    6095 	.db 0x00
      004F44 00                    6096 	.db 0x00
      004F45 00                    6097 	.db 0x00
      004F46 00                    6098 	.db 0x00
      004F47 00                    6099 	.db 0x00
      004F48 00                    6100 	.db 0x00
      004F49 00                    6101 	.db 0x00
      004F4A 80                    6102 	.db #0x80	; 128
      004F4B 82                    6103 	.db #0x82	; 130
      004F4C 82                    6104 	.db #0x82	; 130
      004F4D 82                    6105 	.db #0x82	; 130
      004F4E 82                    6106 	.db #0x82	; 130
      004F4F 82                    6107 	.db #0x82	; 130
      004F50 82                    6108 	.db #0x82	; 130
      004F51 E2                    6109 	.db #0xe2	; 226
      004F52 A2                    6110 	.db #0xa2	; 162
      004F53 92                    6111 	.db #0x92	; 146
      004F54 8A                    6112 	.db #0x8a	; 138
      004F55 86                    6113 	.db #0x86	; 134
      004F56 82                    6114 	.db #0x82	; 130
      004F57 80                    6115 	.db #0x80	; 128
      004F58 80                    6116 	.db #0x80	; 128
      004F59 00                    6117 	.db #0x00	; 0
      004F5A 00                    6118 	.db 0x00
      004F5B 00                    6119 	.db 0x00
      004F5C 00                    6120 	.db 0x00
      004F5D 00                    6121 	.db 0x00
      004F5E 00                    6122 	.db 0x00
      004F5F 00                    6123 	.db 0x00
      004F60 00                    6124 	.db 0x00
      004F61 00                    6125 	.db 0x00
      004F62 00                    6126 	.db 0x00
      004F63 00                    6127 	.db 0x00
      004F64 00                    6128 	.db 0x00
      004F65 00                    6129 	.db 0x00
      004F66 00                    6130 	.db 0x00
      004F67 00                    6131 	.db 0x00
      004F68 00                    6132 	.db 0x00
      004F69 00                    6133 	.db 0x00
      004F6A 00                    6134 	.db #0x00	; 0
      004F6B 00                    6135 	.db #0x00	; 0
      004F6C 00                    6136 	.db #0x00	; 0
      004F6D 00                    6137 	.db #0x00	; 0
      004F6E 00                    6138 	.db #0x00	; 0
      004F6F 40                    6139 	.db #0x40	; 64
      004F70 80                    6140 	.db #0x80	; 128
      004F71 7F                    6141 	.db #0x7f	; 127
      004F72 00                    6142 	.db #0x00	; 0
      004F73 00                    6143 	.db #0x00	; 0
      004F74 00                    6144 	.db #0x00	; 0
      004F75 00                    6145 	.db #0x00	; 0
      004F76 00                    6146 	.db #0x00	; 0
      004F77 00                    6147 	.db #0x00	; 0
      004F78 00                    6148 	.db #0x00	; 0
      004F79 00                    6149 	.db #0x00	; 0
      004F7A 00                    6150 	.db 0x00
      004F7B 00                    6151 	.db 0x00
      004F7C 00                    6152 	.db 0x00
      004F7D 00                    6153 	.db 0x00
      004F7E 00                    6154 	.db 0x00
      004F7F 00                    6155 	.db 0x00
      004F80 00                    6156 	.db 0x00
      004F81 00                    6157 	.db 0x00
      004F82 00                    6158 	.db 0x00
      004F83 00                    6159 	.db 0x00
      004F84 00                    6160 	.db 0x00
      004F85 00                    6161 	.db 0x00
      004F86 00                    6162 	.db 0x00
      004F87 00                    6163 	.db 0x00
      004F88 00                    6164 	.db 0x00
      004F89 00                    6165 	.db 0x00
      004F8A 24                    6166 	.db #0x24	; 36
      004F8B 24                    6167 	.db #0x24	; 36
      004F8C A4                    6168 	.db #0xa4	; 164
      004F8D FE                    6169 	.db #0xfe	; 254
      004F8E A3                    6170 	.db #0xa3	; 163
      004F8F 22                    6171 	.db #0x22	; 34
      004F90 00                    6172 	.db #0x00	; 0
      004F91 22                    6173 	.db #0x22	; 34
      004F92 CC                    6174 	.db #0xcc	; 204
      004F93 00                    6175 	.db #0x00	; 0
      004F94 00                    6176 	.db #0x00	; 0
      004F95 FF                    6177 	.db #0xff	; 255
      004F96 00                    6178 	.db #0x00	; 0
      004F97 00                    6179 	.db #0x00	; 0
      004F98 00                    6180 	.db #0x00	; 0
      004F99 00                    6181 	.db #0x00	; 0
      004F9A 00                    6182 	.db 0x00
      004F9B 00                    6183 	.db 0x00
      004F9C 00                    6184 	.db 0x00
      004F9D 00                    6185 	.db 0x00
      004F9E 00                    6186 	.db 0x00
      004F9F 00                    6187 	.db 0x00
      004FA0 00                    6188 	.db 0x00
      004FA1 00                    6189 	.db 0x00
      004FA2 00                    6190 	.db 0x00
      004FA3 00                    6191 	.db 0x00
      004FA4 00                    6192 	.db 0x00
      004FA5 00                    6193 	.db 0x00
      004FA6 00                    6194 	.db 0x00
      004FA7 00                    6195 	.db 0x00
      004FA8 00                    6196 	.db 0x00
      004FA9 00                    6197 	.db 0x00
      004FAA 08                    6198 	.db #0x08	; 8
      004FAB 06                    6199 	.db #0x06	; 6
      004FAC 01                    6200 	.db #0x01	; 1
      004FAD FF                    6201 	.db #0xff	; 255
      004FAE 00                    6202 	.db #0x00	; 0
      004FAF 01                    6203 	.db #0x01	; 1
      004FB0 04                    6204 	.db #0x04	; 4
      004FB1 04                    6205 	.db #0x04	; 4
      004FB2 04                    6206 	.db #0x04	; 4
      004FB3 04                    6207 	.db #0x04	; 4
      004FB4 04                    6208 	.db #0x04	; 4
      004FB5 FF                    6209 	.db #0xff	; 255
      004FB6 02                    6210 	.db #0x02	; 2
      004FB7 02                    6211 	.db #0x02	; 2
      004FB8 02                    6212 	.db #0x02	; 2
      004FB9 00                    6213 	.db #0x00	; 0
      004FBA 00                    6214 	.db 0x00
      004FBB 00                    6215 	.db 0x00
      004FBC 00                    6216 	.db 0x00
      004FBD 00                    6217 	.db 0x00
      004FBE 00                    6218 	.db 0x00
      004FBF 00                    6219 	.db 0x00
      004FC0 00                    6220 	.db 0x00
      004FC1 00                    6221 	.db 0x00
      004FC2 00                    6222 	.db 0x00
      004FC3 00                    6223 	.db 0x00
      004FC4 00                    6224 	.db 0x00
      004FC5 00                    6225 	.db 0x00
      004FC6 00                    6226 	.db 0x00
      004FC7 00                    6227 	.db 0x00
      004FC8 00                    6228 	.db 0x00
      004FC9 00                    6229 	.db 0x00
      004FCA 10                    6230 	.db #0x10	; 16
      004FCB 10                    6231 	.db #0x10	; 16
      004FCC 10                    6232 	.db #0x10	; 16
      004FCD FF                    6233 	.db #0xff	; 255
      004FCE 10                    6234 	.db #0x10	; 16
      004FCF 90                    6235 	.db #0x90	; 144
      004FD0 08                    6236 	.db #0x08	; 8
      004FD1 88                    6237 	.db #0x88	; 136
      004FD2 88                    6238 	.db #0x88	; 136
      004FD3 88                    6239 	.db #0x88	; 136
      004FD4 FF                    6240 	.db #0xff	; 255
      004FD5 88                    6241 	.db #0x88	; 136
      004FD6 88                    6242 	.db #0x88	; 136
      004FD7 88                    6243 	.db #0x88	; 136
      004FD8 08                    6244 	.db #0x08	; 8
      004FD9 00                    6245 	.db #0x00	; 0
      004FDA 00                    6246 	.db 0x00
      004FDB 00                    6247 	.db 0x00
      004FDC 00                    6248 	.db 0x00
      004FDD 00                    6249 	.db 0x00
      004FDE 00                    6250 	.db 0x00
      004FDF 00                    6251 	.db 0x00
      004FE0 00                    6252 	.db 0x00
      004FE1 00                    6253 	.db 0x00
      004FE2 00                    6254 	.db 0x00
      004FE3 00                    6255 	.db 0x00
      004FE4 00                    6256 	.db 0x00
      004FE5 00                    6257 	.db 0x00
      004FE6 00                    6258 	.db 0x00
      004FE7 00                    6259 	.db 0x00
      004FE8 00                    6260 	.db 0x00
      004FE9 00                    6261 	.db 0x00
      004FEA 04                    6262 	.db #0x04	; 4
      004FEB 44                    6263 	.db #0x44	; 68	'D'
      004FEC 82                    6264 	.db #0x82	; 130
      004FED 7F                    6265 	.db #0x7f	; 127
      004FEE 01                    6266 	.db #0x01	; 1
      004FEF 80                    6267 	.db #0x80	; 128
      004FF0 80                    6268 	.db #0x80	; 128
      004FF1 40                    6269 	.db #0x40	; 64
      004FF2 43                    6270 	.db #0x43	; 67	'C'
      004FF3 2C                    6271 	.db #0x2c	; 44
      004FF4 10                    6272 	.db #0x10	; 16
      004FF5 28                    6273 	.db #0x28	; 40
      004FF6 46                    6274 	.db #0x46	; 70	'F'
      004FF7 81                    6275 	.db #0x81	; 129
      004FF8 80                    6276 	.db #0x80	; 128
      004FF9 00                    6277 	.db #0x00	; 0
      004FFA 00                    6278 	.db 0x00
      004FFB 00                    6279 	.db 0x00
      004FFC 00                    6280 	.db 0x00
      004FFD 00                    6281 	.db 0x00
      004FFE 00                    6282 	.db 0x00
      004FFF 00                    6283 	.db 0x00
      005000 00                    6284 	.db 0x00
      005001 00                    6285 	.db 0x00
      005002 00                    6286 	.db 0x00
      005003 00                    6287 	.db 0x00
      005004 00                    6288 	.db 0x00
      005005 00                    6289 	.db 0x00
      005006 00                    6290 	.db 0x00
      005007 00                    6291 	.db 0x00
      005008 00                    6292 	.db 0x00
      005009 00                    6293 	.db 0x00
      00500A                       6294 _OLED_Init_init_commandList_65537_73:
      00500A AE                    6295 	.db #0xae	; 174
      00500B 40                    6296 	.db #0x40	; 64
      00500C 81                    6297 	.db #0x81	; 129
      00500D CF                    6298 	.db #0xcf	; 207
      00500E A1                    6299 	.db #0xa1	; 161
      00500F C8                    6300 	.db #0xc8	; 200
      005010 A6                    6301 	.db #0xa6	; 166
      005011 A8                    6302 	.db #0xa8	; 168
      005012 3F                    6303 	.db #0x3f	; 63
      005013 D3                    6304 	.db #0xd3	; 211
      005014 00                    6305 	.db #0x00	; 0
      005015 D5                    6306 	.db #0xd5	; 213
      005016 80                    6307 	.db #0x80	; 128
      005017 D9                    6308 	.db #0xd9	; 217
      005018 F1                    6309 	.db #0xf1	; 241
      005019 DA                    6310 	.db #0xda	; 218
      00501A DB                    6311 	.db #0xdb	; 219
      00501B 40                    6312 	.db #0x40	; 64
      00501C 20                    6313 	.db #0x20	; 32
      00501D 02                    6314 	.db #0x02	; 2
      00501E 8D                    6315 	.db #0x8d	; 141
      00501F A4                    6316 	.db #0xa4	; 164
      005020 A6                    6317 	.db #0xa6	; 166
      005021 AF                    6318 	.db #0xaf	; 175
      005022 AF                    6319 	.db #0xaf	; 175
                                   6320 	.area CABS    (ABS,CODE)
