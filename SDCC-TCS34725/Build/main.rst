                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 4.0.0 #11528 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module main
                                      6 	.optsdcc -mmcs51 --model-large
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _BMP2
                                     12 	.globl _BMP1
                                     13 	.globl _main
                                     14 	.globl _SPIMasterModeSet
                                     15 	.globl _setFontSize
                                     16 	.globl _OLED_Clear
                                     17 	.globl _OLED_Init
                                     18 	.globl _OLED_ShowChar
                                     19 	.globl _PWM_SEL_CHANNEL
                                     20 	.globl _TCS34725_GetRawData
                                     21 	.globl _TCS34725_Init
                                     22 	.globl _TCS34725_SetGain
                                     23 	.globl _TCS34725_SetIntegrationTime
                                     24 	.globl _mDelaymS
                                     25 	.globl _CfgFsys
                                     26 	.globl _printf_fast_f
                                     27 	.globl _UIF_BUS_RST
                                     28 	.globl _UIF_DETECT
                                     29 	.globl _UIF_TRANSFER
                                     30 	.globl _UIF_SUSPEND
                                     31 	.globl _UIF_HST_SOF
                                     32 	.globl _UIF_FIFO_OV
                                     33 	.globl _U_SIE_FREE
                                     34 	.globl _U_TOG_OK
                                     35 	.globl _U_IS_NAK
                                     36 	.globl _S0_R_FIFO
                                     37 	.globl _S0_T_FIFO
                                     38 	.globl _S0_FREE
                                     39 	.globl _S0_IF_BYTE
                                     40 	.globl _S0_IF_FIRST
                                     41 	.globl _S0_IF_OV
                                     42 	.globl _S0_FST_ACT
                                     43 	.globl _CP_RL2
                                     44 	.globl _C_T2
                                     45 	.globl _TR2
                                     46 	.globl _EXEN2
                                     47 	.globl _TCLK
                                     48 	.globl _RCLK
                                     49 	.globl _EXF2
                                     50 	.globl _CAP1F
                                     51 	.globl _TF2
                                     52 	.globl _RI
                                     53 	.globl _TI
                                     54 	.globl _RB8
                                     55 	.globl _TB8
                                     56 	.globl _REN
                                     57 	.globl _SM2
                                     58 	.globl _SM1
                                     59 	.globl _SM0
                                     60 	.globl _IT0
                                     61 	.globl _IE0
                                     62 	.globl _IT1
                                     63 	.globl _IE1
                                     64 	.globl _TR0
                                     65 	.globl _TF0
                                     66 	.globl _TR1
                                     67 	.globl _TF1
                                     68 	.globl _XI
                                     69 	.globl _XO
                                     70 	.globl _P4_0
                                     71 	.globl _P4_1
                                     72 	.globl _P4_2
                                     73 	.globl _P4_3
                                     74 	.globl _P4_4
                                     75 	.globl _P4_5
                                     76 	.globl _P4_6
                                     77 	.globl _RXD
                                     78 	.globl _TXD
                                     79 	.globl _INT0
                                     80 	.globl _INT1
                                     81 	.globl _T0
                                     82 	.globl _T1
                                     83 	.globl _CAP0
                                     84 	.globl _INT3
                                     85 	.globl _P3_0
                                     86 	.globl _P3_1
                                     87 	.globl _P3_2
                                     88 	.globl _P3_3
                                     89 	.globl _P3_4
                                     90 	.globl _P3_5
                                     91 	.globl _P3_6
                                     92 	.globl _P3_7
                                     93 	.globl _PWM5
                                     94 	.globl _PWM4
                                     95 	.globl _INT0_
                                     96 	.globl _PWM3
                                     97 	.globl _PWM2
                                     98 	.globl _CAP1_
                                     99 	.globl _T2_
                                    100 	.globl _PWM1
                                    101 	.globl _CAP2_
                                    102 	.globl _T2EX_
                                    103 	.globl _PWM0
                                    104 	.globl _RXD1
                                    105 	.globl _PWM6
                                    106 	.globl _TXD1
                                    107 	.globl _PWM7
                                    108 	.globl _P2_0
                                    109 	.globl _P2_1
                                    110 	.globl _P2_2
                                    111 	.globl _P2_3
                                    112 	.globl _P2_4
                                    113 	.globl _P2_5
                                    114 	.globl _P2_6
                                    115 	.globl _P2_7
                                    116 	.globl _AIN0
                                    117 	.globl _CAP1
                                    118 	.globl _T2
                                    119 	.globl _AIN1
                                    120 	.globl _CAP2
                                    121 	.globl _T2EX
                                    122 	.globl _AIN2
                                    123 	.globl _AIN3
                                    124 	.globl _AIN4
                                    125 	.globl _UCC1
                                    126 	.globl _SCS
                                    127 	.globl _AIN5
                                    128 	.globl _UCC2
                                    129 	.globl _PWM0_
                                    130 	.globl _MOSI
                                    131 	.globl _AIN6
                                    132 	.globl _VBUS
                                    133 	.globl _RXD1_
                                    134 	.globl _MISO
                                    135 	.globl _AIN7
                                    136 	.globl _TXD1_
                                    137 	.globl _SCK
                                    138 	.globl _P1_0
                                    139 	.globl _P1_1
                                    140 	.globl _P1_2
                                    141 	.globl _P1_3
                                    142 	.globl _P1_4
                                    143 	.globl _P1_5
                                    144 	.globl _P1_6
                                    145 	.globl _P1_7
                                    146 	.globl _AIN8
                                    147 	.globl _AIN9
                                    148 	.globl _AIN10
                                    149 	.globl _RXD_
                                    150 	.globl _AIN11
                                    151 	.globl _TXD_
                                    152 	.globl _AIN12
                                    153 	.globl _RXD2
                                    154 	.globl _AIN13
                                    155 	.globl _TXD2
                                    156 	.globl _AIN14
                                    157 	.globl _RXD3
                                    158 	.globl _AIN15
                                    159 	.globl _TXD3
                                    160 	.globl _P0_0
                                    161 	.globl _P0_1
                                    162 	.globl _P0_2
                                    163 	.globl _P0_3
                                    164 	.globl _P0_4
                                    165 	.globl _P0_5
                                    166 	.globl _P0_6
                                    167 	.globl _P0_7
                                    168 	.globl _IE_SPI0
                                    169 	.globl _IE_INT3
                                    170 	.globl _IE_USB
                                    171 	.globl _IE_UART2
                                    172 	.globl _IE_ADC
                                    173 	.globl _IE_UART1
                                    174 	.globl _IE_UART3
                                    175 	.globl _IE_PWMX
                                    176 	.globl _IE_GPIO
                                    177 	.globl _IE_WDOG
                                    178 	.globl _PX0
                                    179 	.globl _PT0
                                    180 	.globl _PX1
                                    181 	.globl _PT1
                                    182 	.globl _PS
                                    183 	.globl _PT2
                                    184 	.globl _PL_FLAG
                                    185 	.globl _PH_FLAG
                                    186 	.globl _EX0
                                    187 	.globl _ET0
                                    188 	.globl _EX1
                                    189 	.globl _ET1
                                    190 	.globl _ES
                                    191 	.globl _ET2
                                    192 	.globl _E_DIS
                                    193 	.globl _EA
                                    194 	.globl _P
                                    195 	.globl _F1
                                    196 	.globl _OV
                                    197 	.globl _RS0
                                    198 	.globl _RS1
                                    199 	.globl _F0
                                    200 	.globl _AC
                                    201 	.globl _CY
                                    202 	.globl _UEP1_DMA_H
                                    203 	.globl _UEP1_DMA_L
                                    204 	.globl _UEP1_DMA
                                    205 	.globl _UEP0_DMA_H
                                    206 	.globl _UEP0_DMA_L
                                    207 	.globl _UEP0_DMA
                                    208 	.globl _UEP2_3_MOD
                                    209 	.globl _UEP4_1_MOD
                                    210 	.globl _UEP3_DMA_H
                                    211 	.globl _UEP3_DMA_L
                                    212 	.globl _UEP3_DMA
                                    213 	.globl _UEP2_DMA_H
                                    214 	.globl _UEP2_DMA_L
                                    215 	.globl _UEP2_DMA
                                    216 	.globl _USB_DEV_AD
                                    217 	.globl _USB_CTRL
                                    218 	.globl _USB_INT_EN
                                    219 	.globl _UEP4_T_LEN
                                    220 	.globl _UEP4_CTRL
                                    221 	.globl _UEP0_T_LEN
                                    222 	.globl _UEP0_CTRL
                                    223 	.globl _USB_RX_LEN
                                    224 	.globl _USB_MIS_ST
                                    225 	.globl _USB_INT_ST
                                    226 	.globl _USB_INT_FG
                                    227 	.globl _UEP3_T_LEN
                                    228 	.globl _UEP3_CTRL
                                    229 	.globl _UEP2_T_LEN
                                    230 	.globl _UEP2_CTRL
                                    231 	.globl _UEP1_T_LEN
                                    232 	.globl _UEP1_CTRL
                                    233 	.globl _UDEV_CTRL
                                    234 	.globl _USB_C_CTRL
                                    235 	.globl _ADC_PIN
                                    236 	.globl _ADC_CHAN
                                    237 	.globl _ADC_DAT_H
                                    238 	.globl _ADC_DAT_L
                                    239 	.globl _ADC_DAT
                                    240 	.globl _ADC_CFG
                                    241 	.globl _ADC_CTRL
                                    242 	.globl _TKEY_CTRL
                                    243 	.globl _SIF3
                                    244 	.globl _SBAUD3
                                    245 	.globl _SBUF3
                                    246 	.globl _SCON3
                                    247 	.globl _SIF2
                                    248 	.globl _SBAUD2
                                    249 	.globl _SBUF2
                                    250 	.globl _SCON2
                                    251 	.globl _SIF1
                                    252 	.globl _SBAUD1
                                    253 	.globl _SBUF1
                                    254 	.globl _SCON1
                                    255 	.globl _SPI0_SETUP
                                    256 	.globl _SPI0_CK_SE
                                    257 	.globl _SPI0_CTRL
                                    258 	.globl _SPI0_DATA
                                    259 	.globl _SPI0_STAT
                                    260 	.globl _PWM_DATA7
                                    261 	.globl _PWM_DATA6
                                    262 	.globl _PWM_DATA5
                                    263 	.globl _PWM_DATA4
                                    264 	.globl _PWM_DATA3
                                    265 	.globl _PWM_CTRL2
                                    266 	.globl _PWM_CK_SE
                                    267 	.globl _PWM_CTRL
                                    268 	.globl _PWM_DATA0
                                    269 	.globl _PWM_DATA1
                                    270 	.globl _PWM_DATA2
                                    271 	.globl _T2CAP1H
                                    272 	.globl _T2CAP1L
                                    273 	.globl _T2CAP1
                                    274 	.globl _TH2
                                    275 	.globl _TL2
                                    276 	.globl _T2COUNT
                                    277 	.globl _RCAP2H
                                    278 	.globl _RCAP2L
                                    279 	.globl _RCAP2
                                    280 	.globl _T2MOD
                                    281 	.globl _T2CON
                                    282 	.globl _T2CAP0H
                                    283 	.globl _T2CAP0L
                                    284 	.globl _T2CAP0
                                    285 	.globl _T2CON2
                                    286 	.globl _SBUF
                                    287 	.globl _SCON
                                    288 	.globl _TH1
                                    289 	.globl _TH0
                                    290 	.globl _TL1
                                    291 	.globl _TL0
                                    292 	.globl _TMOD
                                    293 	.globl _TCON
                                    294 	.globl _XBUS_AUX
                                    295 	.globl _PIN_FUNC
                                    296 	.globl _P5
                                    297 	.globl _P4_DIR_PU
                                    298 	.globl _P4_MOD_OC
                                    299 	.globl _P4
                                    300 	.globl _P3_DIR_PU
                                    301 	.globl _P3_MOD_OC
                                    302 	.globl _P3
                                    303 	.globl _P2_DIR_PU
                                    304 	.globl _P2_MOD_OC
                                    305 	.globl _P2
                                    306 	.globl _P1_DIR_PU
                                    307 	.globl _P1_MOD_OC
                                    308 	.globl _P1
                                    309 	.globl _P0_DIR_PU
                                    310 	.globl _P0_MOD_OC
                                    311 	.globl _P0
                                    312 	.globl _ROM_CTRL
                                    313 	.globl _ROM_DATA_HH
                                    314 	.globl _ROM_DATA_HL
                                    315 	.globl _ROM_DATA_HI
                                    316 	.globl _ROM_ADDR_H
                                    317 	.globl _ROM_ADDR_L
                                    318 	.globl _ROM_ADDR
                                    319 	.globl _GPIO_IE
                                    320 	.globl _INTX
                                    321 	.globl _IP_EX
                                    322 	.globl _IE_EX
                                    323 	.globl _IP
                                    324 	.globl _IE
                                    325 	.globl _WDOG_COUNT
                                    326 	.globl _RESET_KEEP
                                    327 	.globl _WAKE_CTRL
                                    328 	.globl _CLOCK_CFG
                                    329 	.globl _POWER_CFG
                                    330 	.globl _PCON
                                    331 	.globl _GLOBAL_CFG
                                    332 	.globl _SAFE_MOD
                                    333 	.globl _DPH
                                    334 	.globl _DPL
                                    335 	.globl _SP
                                    336 	.globl _A_INV
                                    337 	.globl _B
                                    338 	.globl _ACC
                                    339 	.globl _PSW
                                    340 	.globl _oled_row
                                    341 	.globl _oled_colum
                                    342 	.globl _hsl
                                    343 	.globl _rgb
                                    344 	.globl _box
                                    345 	.globl _putchar
                                    346 	.globl _setCursor
                                    347 ;--------------------------------------------------------
                                    348 ; special function registers
                                    349 ;--------------------------------------------------------
                                    350 	.area RSEG    (ABS,DATA)
      000000                        351 	.org 0x0000
                           0000D0   352 _PSW	=	0x00d0
                           0000E0   353 _ACC	=	0x00e0
                           0000F0   354 _B	=	0x00f0
                           0000FD   355 _A_INV	=	0x00fd
                           000081   356 _SP	=	0x0081
                           000082   357 _DPL	=	0x0082
                           000083   358 _DPH	=	0x0083
                           0000A1   359 _SAFE_MOD	=	0x00a1
                           0000B1   360 _GLOBAL_CFG	=	0x00b1
                           000087   361 _PCON	=	0x0087
                           0000BA   362 _POWER_CFG	=	0x00ba
                           0000B9   363 _CLOCK_CFG	=	0x00b9
                           0000A9   364 _WAKE_CTRL	=	0x00a9
                           0000FE   365 _RESET_KEEP	=	0x00fe
                           0000FF   366 _WDOG_COUNT	=	0x00ff
                           0000A8   367 _IE	=	0x00a8
                           0000B8   368 _IP	=	0x00b8
                           0000E8   369 _IE_EX	=	0x00e8
                           0000E9   370 _IP_EX	=	0x00e9
                           0000B3   371 _INTX	=	0x00b3
                           0000B2   372 _GPIO_IE	=	0x00b2
                           008584   373 _ROM_ADDR	=	0x8584
                           000084   374 _ROM_ADDR_L	=	0x0084
                           000085   375 _ROM_ADDR_H	=	0x0085
                           008F8E   376 _ROM_DATA_HI	=	0x8f8e
                           00008E   377 _ROM_DATA_HL	=	0x008e
                           00008F   378 _ROM_DATA_HH	=	0x008f
                           000086   379 _ROM_CTRL	=	0x0086
                           000080   380 _P0	=	0x0080
                           0000C4   381 _P0_MOD_OC	=	0x00c4
                           0000C5   382 _P0_DIR_PU	=	0x00c5
                           000090   383 _P1	=	0x0090
                           000092   384 _P1_MOD_OC	=	0x0092
                           000093   385 _P1_DIR_PU	=	0x0093
                           0000A0   386 _P2	=	0x00a0
                           000094   387 _P2_MOD_OC	=	0x0094
                           000095   388 _P2_DIR_PU	=	0x0095
                           0000B0   389 _P3	=	0x00b0
                           000096   390 _P3_MOD_OC	=	0x0096
                           000097   391 _P3_DIR_PU	=	0x0097
                           0000C0   392 _P4	=	0x00c0
                           0000C2   393 _P4_MOD_OC	=	0x00c2
                           0000C3   394 _P4_DIR_PU	=	0x00c3
                           0000AB   395 _P5	=	0x00ab
                           0000AA   396 _PIN_FUNC	=	0x00aa
                           0000A2   397 _XBUS_AUX	=	0x00a2
                           000088   398 _TCON	=	0x0088
                           000089   399 _TMOD	=	0x0089
                           00008A   400 _TL0	=	0x008a
                           00008B   401 _TL1	=	0x008b
                           00008C   402 _TH0	=	0x008c
                           00008D   403 _TH1	=	0x008d
                           000098   404 _SCON	=	0x0098
                           000099   405 _SBUF	=	0x0099
                           0000C1   406 _T2CON2	=	0x00c1
                           00C7C6   407 _T2CAP0	=	0xc7c6
                           0000C6   408 _T2CAP0L	=	0x00c6
                           0000C7   409 _T2CAP0H	=	0x00c7
                           0000C8   410 _T2CON	=	0x00c8
                           0000C9   411 _T2MOD	=	0x00c9
                           00CBCA   412 _RCAP2	=	0xcbca
                           0000CA   413 _RCAP2L	=	0x00ca
                           0000CB   414 _RCAP2H	=	0x00cb
                           00CDCC   415 _T2COUNT	=	0xcdcc
                           0000CC   416 _TL2	=	0x00cc
                           0000CD   417 _TH2	=	0x00cd
                           00CFCE   418 _T2CAP1	=	0xcfce
                           0000CE   419 _T2CAP1L	=	0x00ce
                           0000CF   420 _T2CAP1H	=	0x00cf
                           00009A   421 _PWM_DATA2	=	0x009a
                           00009B   422 _PWM_DATA1	=	0x009b
                           00009C   423 _PWM_DATA0	=	0x009c
                           00009D   424 _PWM_CTRL	=	0x009d
                           00009E   425 _PWM_CK_SE	=	0x009e
                           00009F   426 _PWM_CTRL2	=	0x009f
                           0000A3   427 _PWM_DATA3	=	0x00a3
                           0000A4   428 _PWM_DATA4	=	0x00a4
                           0000A5   429 _PWM_DATA5	=	0x00a5
                           0000A6   430 _PWM_DATA6	=	0x00a6
                           0000A7   431 _PWM_DATA7	=	0x00a7
                           0000F8   432 _SPI0_STAT	=	0x00f8
                           0000F9   433 _SPI0_DATA	=	0x00f9
                           0000FA   434 _SPI0_CTRL	=	0x00fa
                           0000FB   435 _SPI0_CK_SE	=	0x00fb
                           0000FC   436 _SPI0_SETUP	=	0x00fc
                           0000BC   437 _SCON1	=	0x00bc
                           0000BD   438 _SBUF1	=	0x00bd
                           0000BE   439 _SBAUD1	=	0x00be
                           0000BF   440 _SIF1	=	0x00bf
                           0000B4   441 _SCON2	=	0x00b4
                           0000B5   442 _SBUF2	=	0x00b5
                           0000B6   443 _SBAUD2	=	0x00b6
                           0000B7   444 _SIF2	=	0x00b7
                           0000AC   445 _SCON3	=	0x00ac
                           0000AD   446 _SBUF3	=	0x00ad
                           0000AE   447 _SBAUD3	=	0x00ae
                           0000AF   448 _SIF3	=	0x00af
                           0000F1   449 _TKEY_CTRL	=	0x00f1
                           0000F2   450 _ADC_CTRL	=	0x00f2
                           0000F3   451 _ADC_CFG	=	0x00f3
                           00F5F4   452 _ADC_DAT	=	0xf5f4
                           0000F4   453 _ADC_DAT_L	=	0x00f4
                           0000F5   454 _ADC_DAT_H	=	0x00f5
                           0000F6   455 _ADC_CHAN	=	0x00f6
                           0000F7   456 _ADC_PIN	=	0x00f7
                           000091   457 _USB_C_CTRL	=	0x0091
                           0000D1   458 _UDEV_CTRL	=	0x00d1
                           0000D2   459 _UEP1_CTRL	=	0x00d2
                           0000D3   460 _UEP1_T_LEN	=	0x00d3
                           0000D4   461 _UEP2_CTRL	=	0x00d4
                           0000D5   462 _UEP2_T_LEN	=	0x00d5
                           0000D6   463 _UEP3_CTRL	=	0x00d6
                           0000D7   464 _UEP3_T_LEN	=	0x00d7
                           0000D8   465 _USB_INT_FG	=	0x00d8
                           0000D9   466 _USB_INT_ST	=	0x00d9
                           0000DA   467 _USB_MIS_ST	=	0x00da
                           0000DB   468 _USB_RX_LEN	=	0x00db
                           0000DC   469 _UEP0_CTRL	=	0x00dc
                           0000DD   470 _UEP0_T_LEN	=	0x00dd
                           0000DE   471 _UEP4_CTRL	=	0x00de
                           0000DF   472 _UEP4_T_LEN	=	0x00df
                           0000E1   473 _USB_INT_EN	=	0x00e1
                           0000E2   474 _USB_CTRL	=	0x00e2
                           0000E3   475 _USB_DEV_AD	=	0x00e3
                           00E5E4   476 _UEP2_DMA	=	0xe5e4
                           0000E4   477 _UEP2_DMA_L	=	0x00e4
                           0000E5   478 _UEP2_DMA_H	=	0x00e5
                           00E7E6   479 _UEP3_DMA	=	0xe7e6
                           0000E6   480 _UEP3_DMA_L	=	0x00e6
                           0000E7   481 _UEP3_DMA_H	=	0x00e7
                           0000EA   482 _UEP4_1_MOD	=	0x00ea
                           0000EB   483 _UEP2_3_MOD	=	0x00eb
                           00EDEC   484 _UEP0_DMA	=	0xedec
                           0000EC   485 _UEP0_DMA_L	=	0x00ec
                           0000ED   486 _UEP0_DMA_H	=	0x00ed
                           00EFEE   487 _UEP1_DMA	=	0xefee
                           0000EE   488 _UEP1_DMA_L	=	0x00ee
                           0000EF   489 _UEP1_DMA_H	=	0x00ef
                                    490 ;--------------------------------------------------------
                                    491 ; special function bits
                                    492 ;--------------------------------------------------------
                                    493 	.area RSEG    (ABS,DATA)
      000000                        494 	.org 0x0000
                           0000D7   495 _CY	=	0x00d7
                           0000D6   496 _AC	=	0x00d6
                           0000D5   497 _F0	=	0x00d5
                           0000D4   498 _RS1	=	0x00d4
                           0000D3   499 _RS0	=	0x00d3
                           0000D2   500 _OV	=	0x00d2
                           0000D1   501 _F1	=	0x00d1
                           0000D0   502 _P	=	0x00d0
                           0000AF   503 _EA	=	0x00af
                           0000AE   504 _E_DIS	=	0x00ae
                           0000AD   505 _ET2	=	0x00ad
                           0000AC   506 _ES	=	0x00ac
                           0000AB   507 _ET1	=	0x00ab
                           0000AA   508 _EX1	=	0x00aa
                           0000A9   509 _ET0	=	0x00a9
                           0000A8   510 _EX0	=	0x00a8
                           0000BF   511 _PH_FLAG	=	0x00bf
                           0000BE   512 _PL_FLAG	=	0x00be
                           0000BD   513 _PT2	=	0x00bd
                           0000BC   514 _PS	=	0x00bc
                           0000BB   515 _PT1	=	0x00bb
                           0000BA   516 _PX1	=	0x00ba
                           0000B9   517 _PT0	=	0x00b9
                           0000B8   518 _PX0	=	0x00b8
                           0000EF   519 _IE_WDOG	=	0x00ef
                           0000EE   520 _IE_GPIO	=	0x00ee
                           0000ED   521 _IE_PWMX	=	0x00ed
                           0000ED   522 _IE_UART3	=	0x00ed
                           0000EC   523 _IE_UART1	=	0x00ec
                           0000EB   524 _IE_ADC	=	0x00eb
                           0000EB   525 _IE_UART2	=	0x00eb
                           0000EA   526 _IE_USB	=	0x00ea
                           0000E9   527 _IE_INT3	=	0x00e9
                           0000E8   528 _IE_SPI0	=	0x00e8
                           000087   529 _P0_7	=	0x0087
                           000086   530 _P0_6	=	0x0086
                           000085   531 _P0_5	=	0x0085
                           000084   532 _P0_4	=	0x0084
                           000083   533 _P0_3	=	0x0083
                           000082   534 _P0_2	=	0x0082
                           000081   535 _P0_1	=	0x0081
                           000080   536 _P0_0	=	0x0080
                           000087   537 _TXD3	=	0x0087
                           000087   538 _AIN15	=	0x0087
                           000086   539 _RXD3	=	0x0086
                           000086   540 _AIN14	=	0x0086
                           000085   541 _TXD2	=	0x0085
                           000085   542 _AIN13	=	0x0085
                           000084   543 _RXD2	=	0x0084
                           000084   544 _AIN12	=	0x0084
                           000083   545 _TXD_	=	0x0083
                           000083   546 _AIN11	=	0x0083
                           000082   547 _RXD_	=	0x0082
                           000082   548 _AIN10	=	0x0082
                           000081   549 _AIN9	=	0x0081
                           000080   550 _AIN8	=	0x0080
                           000097   551 _P1_7	=	0x0097
                           000096   552 _P1_6	=	0x0096
                           000095   553 _P1_5	=	0x0095
                           000094   554 _P1_4	=	0x0094
                           000093   555 _P1_3	=	0x0093
                           000092   556 _P1_2	=	0x0092
                           000091   557 _P1_1	=	0x0091
                           000090   558 _P1_0	=	0x0090
                           000097   559 _SCK	=	0x0097
                           000097   560 _TXD1_	=	0x0097
                           000097   561 _AIN7	=	0x0097
                           000096   562 _MISO	=	0x0096
                           000096   563 _RXD1_	=	0x0096
                           000096   564 _VBUS	=	0x0096
                           000096   565 _AIN6	=	0x0096
                           000095   566 _MOSI	=	0x0095
                           000095   567 _PWM0_	=	0x0095
                           000095   568 _UCC2	=	0x0095
                           000095   569 _AIN5	=	0x0095
                           000094   570 _SCS	=	0x0094
                           000094   571 _UCC1	=	0x0094
                           000094   572 _AIN4	=	0x0094
                           000093   573 _AIN3	=	0x0093
                           000092   574 _AIN2	=	0x0092
                           000091   575 _T2EX	=	0x0091
                           000091   576 _CAP2	=	0x0091
                           000091   577 _AIN1	=	0x0091
                           000090   578 _T2	=	0x0090
                           000090   579 _CAP1	=	0x0090
                           000090   580 _AIN0	=	0x0090
                           0000A7   581 _P2_7	=	0x00a7
                           0000A6   582 _P2_6	=	0x00a6
                           0000A5   583 _P2_5	=	0x00a5
                           0000A4   584 _P2_4	=	0x00a4
                           0000A3   585 _P2_3	=	0x00a3
                           0000A2   586 _P2_2	=	0x00a2
                           0000A1   587 _P2_1	=	0x00a1
                           0000A0   588 _P2_0	=	0x00a0
                           0000A7   589 _PWM7	=	0x00a7
                           0000A7   590 _TXD1	=	0x00a7
                           0000A6   591 _PWM6	=	0x00a6
                           0000A6   592 _RXD1	=	0x00a6
                           0000A5   593 _PWM0	=	0x00a5
                           0000A5   594 _T2EX_	=	0x00a5
                           0000A5   595 _CAP2_	=	0x00a5
                           0000A4   596 _PWM1	=	0x00a4
                           0000A4   597 _T2_	=	0x00a4
                           0000A4   598 _CAP1_	=	0x00a4
                           0000A3   599 _PWM2	=	0x00a3
                           0000A2   600 _PWM3	=	0x00a2
                           0000A2   601 _INT0_	=	0x00a2
                           0000A1   602 _PWM4	=	0x00a1
                           0000A0   603 _PWM5	=	0x00a0
                           0000B7   604 _P3_7	=	0x00b7
                           0000B6   605 _P3_6	=	0x00b6
                           0000B5   606 _P3_5	=	0x00b5
                           0000B4   607 _P3_4	=	0x00b4
                           0000B3   608 _P3_3	=	0x00b3
                           0000B2   609 _P3_2	=	0x00b2
                           0000B1   610 _P3_1	=	0x00b1
                           0000B0   611 _P3_0	=	0x00b0
                           0000B7   612 _INT3	=	0x00b7
                           0000B6   613 _CAP0	=	0x00b6
                           0000B5   614 _T1	=	0x00b5
                           0000B4   615 _T0	=	0x00b4
                           0000B3   616 _INT1	=	0x00b3
                           0000B2   617 _INT0	=	0x00b2
                           0000B1   618 _TXD	=	0x00b1
                           0000B0   619 _RXD	=	0x00b0
                           0000C6   620 _P4_6	=	0x00c6
                           0000C5   621 _P4_5	=	0x00c5
                           0000C4   622 _P4_4	=	0x00c4
                           0000C3   623 _P4_3	=	0x00c3
                           0000C2   624 _P4_2	=	0x00c2
                           0000C1   625 _P4_1	=	0x00c1
                           0000C0   626 _P4_0	=	0x00c0
                           0000C7   627 _XO	=	0x00c7
                           0000C6   628 _XI	=	0x00c6
                           00008F   629 _TF1	=	0x008f
                           00008E   630 _TR1	=	0x008e
                           00008D   631 _TF0	=	0x008d
                           00008C   632 _TR0	=	0x008c
                           00008B   633 _IE1	=	0x008b
                           00008A   634 _IT1	=	0x008a
                           000089   635 _IE0	=	0x0089
                           000088   636 _IT0	=	0x0088
                           00009F   637 _SM0	=	0x009f
                           00009E   638 _SM1	=	0x009e
                           00009D   639 _SM2	=	0x009d
                           00009C   640 _REN	=	0x009c
                           00009B   641 _TB8	=	0x009b
                           00009A   642 _RB8	=	0x009a
                           000099   643 _TI	=	0x0099
                           000098   644 _RI	=	0x0098
                           0000CF   645 _TF2	=	0x00cf
                           0000CF   646 _CAP1F	=	0x00cf
                           0000CE   647 _EXF2	=	0x00ce
                           0000CD   648 _RCLK	=	0x00cd
                           0000CC   649 _TCLK	=	0x00cc
                           0000CB   650 _EXEN2	=	0x00cb
                           0000CA   651 _TR2	=	0x00ca
                           0000C9   652 _C_T2	=	0x00c9
                           0000C8   653 _CP_RL2	=	0x00c8
                           0000FF   654 _S0_FST_ACT	=	0x00ff
                           0000FE   655 _S0_IF_OV	=	0x00fe
                           0000FD   656 _S0_IF_FIRST	=	0x00fd
                           0000FC   657 _S0_IF_BYTE	=	0x00fc
                           0000FB   658 _S0_FREE	=	0x00fb
                           0000FA   659 _S0_T_FIFO	=	0x00fa
                           0000F8   660 _S0_R_FIFO	=	0x00f8
                           0000DF   661 _U_IS_NAK	=	0x00df
                           0000DE   662 _U_TOG_OK	=	0x00de
                           0000DD   663 _U_SIE_FREE	=	0x00dd
                           0000DC   664 _UIF_FIFO_OV	=	0x00dc
                           0000DB   665 _UIF_HST_SOF	=	0x00db
                           0000DA   666 _UIF_SUSPEND	=	0x00da
                           0000D9   667 _UIF_TRANSFER	=	0x00d9
                           0000D8   668 _UIF_DETECT	=	0x00d8
                           0000D8   669 _UIF_BUS_RST	=	0x00d8
                                    670 ;--------------------------------------------------------
                                    671 ; overlayable register banks
                                    672 ;--------------------------------------------------------
                                    673 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        674 	.ds 8
                                    675 ;--------------------------------------------------------
                                    676 ; internal ram data
                                    677 ;--------------------------------------------------------
                                    678 	.area DSEG    (DATA)
                                    679 ;--------------------------------------------------------
                                    680 ; overlayable items in internal ram 
                                    681 ;--------------------------------------------------------
                                    682 ;--------------------------------------------------------
                                    683 ; Stack segment in internal ram 
                                    684 ;--------------------------------------------------------
                                    685 	.area	SSEG
      000022                        686 __start__stack:
      000022                        687 	.ds	1
                                    688 
                                    689 ;--------------------------------------------------------
                                    690 ; indirectly addressable internal ram data
                                    691 ;--------------------------------------------------------
                                    692 	.area ISEG    (DATA)
                                    693 ;--------------------------------------------------------
                                    694 ; absolute internal ram data
                                    695 ;--------------------------------------------------------
                                    696 	.area IABS    (ABS,DATA)
                                    697 	.area IABS    (ABS,DATA)
                                    698 ;--------------------------------------------------------
                                    699 ; bit data
                                    700 ;--------------------------------------------------------
                                    701 	.area BSEG    (BIT)
                                    702 ;--------------------------------------------------------
                                    703 ; paged external ram data
                                    704 ;--------------------------------------------------------
                                    705 	.area PSEG    (PAG,XDATA)
                                    706 ;--------------------------------------------------------
                                    707 ; external ram data
                                    708 ;--------------------------------------------------------
                                    709 	.area XSEG    (XDATA)
      000001                        710 _box::
      000001                        711 	.ds 4
      000005                        712 _rgb::
      000005                        713 	.ds 8
      00000D                        714 _hsl::
      00000D                        715 	.ds 4
      000011                        716 _oled_colum::
      000011                        717 	.ds 2
      000013                        718 _oled_row::
      000013                        719 	.ds 2
                                    720 ;--------------------------------------------------------
                                    721 ; absolute external ram data
                                    722 ;--------------------------------------------------------
                                    723 	.area XABS    (ABS,XDATA)
                                    724 ;--------------------------------------------------------
                                    725 ; external initialized ram data
                                    726 ;--------------------------------------------------------
                                    727 	.area HOME    (CODE)
                                    728 	.area GSINIT0 (CODE)
                                    729 	.area GSINIT1 (CODE)
                                    730 	.area GSINIT2 (CODE)
                                    731 	.area GSINIT3 (CODE)
                                    732 	.area GSINIT4 (CODE)
                                    733 	.area GSINIT5 (CODE)
                                    734 	.area GSINIT  (CODE)
                                    735 	.area GSFINAL (CODE)
                                    736 	.area CSEG    (CODE)
                                    737 ;--------------------------------------------------------
                                    738 ; interrupt vector 
                                    739 ;--------------------------------------------------------
                                    740 	.area HOME    (CODE)
      000000                        741 __interrupt_vect:
      000000 02 00 06         [24]  742 	ljmp	__sdcc_gsinit_startup
                                    743 ;--------------------------------------------------------
                                    744 ; global & static initialisations
                                    745 ;--------------------------------------------------------
                                    746 	.area HOME    (CODE)
                                    747 	.area GSINIT  (CODE)
                                    748 	.area GSFINAL (CODE)
                                    749 	.area GSINIT  (CODE)
                                    750 	.globl __sdcc_gsinit_startup
                                    751 	.globl __sdcc_program_startup
                                    752 	.globl __start__stack
                                    753 	.globl __mcs51_genRAMCLEAR
                                    754 ;	usr/main.c:29: UINT8 box[4] = {40,60,78,98};  //控制分向舵机的PWM参数三组，例如第一个数值即为第一个盒子
      000019 90 00 01         [24]  755 	mov	dptr,#_box
      00001C 74 28            [12]  756 	mov	a,#0x28
      00001E F0               [24]  757 	movx	@dptr,a
      00001F 90 00 02         [24]  758 	mov	dptr,#(_box + 0x0001)
      000022 74 3C            [12]  759 	mov	a,#0x3c
      000024 F0               [24]  760 	movx	@dptr,a
      000025 90 00 03         [24]  761 	mov	dptr,#(_box + 0x0002)
      000028 74 4E            [12]  762 	mov	a,#0x4e
      00002A F0               [24]  763 	movx	@dptr,a
      00002B 90 00 04         [24]  764 	mov	dptr,#(_box + 0x0003)
      00002E 74 62            [12]  765 	mov	a,#0x62
      000030 F0               [24]  766 	movx	@dptr,a
                                    767 	.area GSFINAL (CODE)
      000031 02 00 03         [24]  768 	ljmp	__sdcc_program_startup
                                    769 ;--------------------------------------------------------
                                    770 ; Home
                                    771 ;--------------------------------------------------------
                                    772 	.area HOME    (CODE)
                                    773 	.area HOME    (CODE)
      000003                        774 __sdcc_program_startup:
      000003 02 00 34         [24]  775 	ljmp	_main
                                    776 ;	return from main will return to caller
                                    777 ;--------------------------------------------------------
                                    778 ; code
                                    779 ;--------------------------------------------------------
                                    780 	.area CSEG    (CODE)
                                    781 ;------------------------------------------------------------
                                    782 ;Allocation info for local variables in function 'main'
                                    783 ;------------------------------------------------------------
                                    784 ;r                         Allocated to registers r2 
                                    785 ;g                         Allocated to stack - _bp +15
                                    786 ;b                         Allocated to stack - _bp +18
                                    787 ;M                         Allocated to stack - _bp +9
                                    788 ;m                         Allocated to stack - _bp +19
                                    789 ;n                         Allocated to stack - _bp +13
                                    790 ;l                         Allocated to stack - _bp +16
                                    791 ;i                         Allocated to registers r4 r5 
                                    792 ;sloc0                     Allocated to stack - _bp +1
                                    793 ;sloc1                     Allocated to stack - _bp +5
                                    794 ;------------------------------------------------------------
                                    795 ;	usr/main.c:42: void main(void)
                                    796 ;	-----------------------------------------
                                    797 ;	 function main
                                    798 ;	-----------------------------------------
      000034                        799 _main:
                           000007   800 	ar7 = 0x07
                           000006   801 	ar6 = 0x06
                           000005   802 	ar5 = 0x05
                           000004   803 	ar4 = 0x04
                           000003   804 	ar3 = 0x03
                           000002   805 	ar2 = 0x02
                           000001   806 	ar1 = 0x01
                           000000   807 	ar0 = 0x00
      000034 C0 16            [24]  808 	push	_bp
      000036 E5 81            [12]  809 	mov	a,sp
      000038 F5 16            [12]  810 	mov	_bp,a
      00003A 24 14            [12]  811 	add	a,#0x14
      00003C F5 81            [12]  812 	mov	sp,a
                                    813 ;	usr/main.c:45: UINT32 M = 0;
      00003E E5 16            [12]  814 	mov	a,_bp
      000040 24 09            [12]  815 	add	a,#0x09
      000042 F8               [12]  816 	mov	r0,a
      000043 E4               [12]  817 	clr	a
      000044 F6               [12]  818 	mov	@r0,a
      000045 08               [12]  819 	inc	r0
      000046 F6               [12]  820 	mov	@r0,a
      000047 08               [12]  821 	inc	r0
      000048 F6               [12]  822 	mov	@r0,a
      000049 08               [12]  823 	inc	r0
      00004A F6               [12]  824 	mov	@r0,a
                                    825 ;	usr/main.c:46: UINT16 m = 0,n = 0,l = 0,i = 0;
      00004B E5 16            [12]  826 	mov	a,_bp
      00004D 24 13            [12]  827 	add	a,#0x13
      00004F F8               [12]  828 	mov	r0,a
      000050 E4               [12]  829 	clr	a
      000051 F6               [12]  830 	mov	@r0,a
      000052 08               [12]  831 	inc	r0
      000053 F6               [12]  832 	mov	@r0,a
      000054 E5 16            [12]  833 	mov	a,_bp
      000056 24 0D            [12]  834 	add	a,#0x0d
      000058 F8               [12]  835 	mov	r0,a
      000059 E4               [12]  836 	clr	a
      00005A F6               [12]  837 	mov	@r0,a
      00005B 08               [12]  838 	inc	r0
      00005C F6               [12]  839 	mov	@r0,a
      00005D E5 16            [12]  840 	mov	a,_bp
      00005F 24 10            [12]  841 	add	a,#0x10
      000061 F8               [12]  842 	mov	r0,a
      000062 E4               [12]  843 	clr	a
      000063 F6               [12]  844 	mov	@r0,a
      000064 08               [12]  845 	inc	r0
      000065 F6               [12]  846 	mov	@r0,a
      000066 7C 00            [12]  847 	mov	r4,#0x00
      000068 7D 00            [12]  848 	mov	r5,#0x00
                                    849 ;	usr/main.c:47: CfgFsys(); //CH549时钟选择配置
      00006A C0 05            [24]  850 	push	ar5
      00006C C0 04            [24]  851 	push	ar4
      00006E 12 06 B2         [24]  852 	lcall	_CfgFsys
                                    853 ;	usr/main.c:48: mDelaymS(20);
      000071 90 00 14         [24]  854 	mov	dptr,#0x0014
      000074 12 06 E2         [24]  855 	lcall	_mDelaymS
                                    856 ;	usr/main.c:49: SPIMasterModeSet(3);          //SPI主机模式设置，模式3
      000077 75 82 03         [24]  857 	mov	dpl,#0x03
      00007A 12 15 7B         [24]  858 	lcall	_SPIMasterModeSet
                                    859 ;	usr/main.c:50: SPI_CK_SET(12);               //设置spi sclk 时钟信号为12分频
      00007D 75 FB 0C         [24]  860 	mov	_SPI0_CK_SE,#0x0c
                                    861 ;	usr/main.c:51: OLED_Init();			            //初始化OLED  
      000080 12 16 6F         [24]  862 	lcall	_OLED_Init
                                    863 ;	usr/main.c:52: OLED_Clear();                 //将oled屏幕上内容清除
      000083 12 16 C3         [24]  864 	lcall	_OLED_Clear
                                    865 ;	usr/main.c:53: setFontSize(8);               //设置文字大小
      000086 75 82 08         [24]  866 	mov	dpl,#0x08
      000089 12 15 D1         [24]  867 	lcall	_setFontSize
                                    868 ;	usr/main.c:54: TCS34725_Init();              //初始化TCS34725
      00008C 12 0E 60         [24]  869 	lcall	_TCS34725_Init
                                    870 ;	usr/main.c:55: TCS34725_SetGain(TCS34725_GAIN_4X);      //设置TCS34725的增益
      00008F 75 82 01         [24]  871 	mov	dpl,#0x01
      000092 12 0D 95         [24]  872 	lcall	_TCS34725_SetGain
                                    873 ;	usr/main.c:56: TCS34725_SetIntegrationTime(TCS34725_INTEGRATIONTIME_24MS);            //设置TCS34725的积分时间，即通道最大值
      000095 75 82 F6         [24]  874 	mov	dpl,#0xf6
      000098 12 0D 6C         [24]  875 	lcall	_TCS34725_SetIntegrationTime
                                    876 ;	usr/main.c:57: SetPWMClkDiv(255);                          //PWM时钟配置,Fsys/255,Fsys为12Mhz
      00009B 75 9E FF         [24]  877 	mov	_PWM_CK_SE,#0xff
                                    878 ;	usr/main.c:58: SetPWMCycle256Clk();                      //PWM周期 Fsys/255/256
      00009E 53 9D FE         [24]  879 	anl	_PWM_CTRL,#0xfe
                                    880 ;	usr/main.c:59: PWM_SEL_CHANNEL(PWM_CH1,Enable);          //使能CH1，即P2.4
      0000A1 74 01            [12]  881 	mov	a,#0x01
      0000A3 C0 E0            [24]  882 	push	acc
      0000A5 75 82 02         [24]  883 	mov	dpl,#0x02
      0000A8 12 14 DD         [24]  884 	lcall	_PWM_SEL_CHANNEL
      0000AB 15 81            [12]  885 	dec	sp
                                    886 ;	usr/main.c:60: PWM_SEL_CHANNEL(PWM_CH3,Enable);          //使能CH3，即P2.2
      0000AD 74 01            [12]  887 	mov	a,#0x01
      0000AF C0 E0            [24]  888 	push	acc
      0000B1 75 82 08         [24]  889 	mov	dpl,#0x08
      0000B4 12 14 DD         [24]  890 	lcall	_PWM_SEL_CHANNEL
      0000B7 15 81            [12]  891 	dec	sp
      0000B9 D0 04            [24]  892 	pop	ar4
      0000BB D0 05            [24]  893 	pop	ar5
                                    894 ;	usr/main.c:61: SetPWM1Dat(18);
      0000BD 75 9B 12         [24]  895 	mov	_PWM_DATA1,#0x12
                                    896 ;	usr/main.c:63: while (1)
      0000C0                        897 00127$:
                                    898 ;	usr/main.c:65: TCS34725_GetRawData(&rgb);            //读取当前颜色数据，即RGBC
      0000C0 C0 04            [24]  899 	push	ar4
      0000C2 C0 05            [24]  900 	push	ar5
      0000C4 90 00 05         [24]  901 	mov	dptr,#_rgb
      0000C7 75 F0 00         [24]  902 	mov	b,#0x00
      0000CA C0 05            [24]  903 	push	ar5
      0000CC C0 04            [24]  904 	push	ar4
      0000CE 12 0F 0E         [24]  905 	lcall	_TCS34725_GetRawData
      0000D1 D0 04            [24]  906 	pop	ar4
      0000D3 D0 05            [24]  907 	pop	ar5
                                    908 ;	usr/main.c:67: r = rgb.r*255.0/rgb.c;   //将实际的RGB值读取出来，范围为0-255
      0000D5 90 00 07         [24]  909 	mov	dptr,#(_rgb + 0x0002)
      0000D8 E0               [24]  910 	movx	a,@dptr
      0000D9 FC               [12]  911 	mov	r4,a
      0000DA A3               [24]  912 	inc	dptr
      0000DB E0               [24]  913 	movx	a,@dptr
      0000DC FD               [12]  914 	mov	r5,a
      0000DD 8C 82            [24]  915 	mov	dpl,r4
      0000DF 8D 83            [24]  916 	mov	dph,r5
      0000E1 12 21 5A         [24]  917 	lcall	___uint2fs
      0000E4 AC 82            [24]  918 	mov	r4,dpl
      0000E6 AD 83            [24]  919 	mov	r5,dph
      0000E8 AE F0            [24]  920 	mov	r6,b
      0000EA FF               [12]  921 	mov	r7,a
      0000EB C0 05            [24]  922 	push	ar5
      0000ED C0 04            [24]  923 	push	ar4
      0000EF C0 04            [24]  924 	push	ar4
      0000F1 C0 05            [24]  925 	push	ar5
      0000F3 C0 06            [24]  926 	push	ar6
      0000F5 C0 07            [24]  927 	push	ar7
      0000F7 90 00 00         [24]  928 	mov	dptr,#0x0000
      0000FA 75 F0 7F         [24]  929 	mov	b,#0x7f
      0000FD 74 43            [12]  930 	mov	a,#0x43
      0000FF 12 1F 5A         [24]  931 	lcall	___fsmul
      000102 A8 16            [24]  932 	mov	r0,_bp
      000104 08               [12]  933 	inc	r0
      000105 A6 82            [24]  934 	mov	@r0,dpl
      000107 08               [12]  935 	inc	r0
      000108 A6 83            [24]  936 	mov	@r0,dph
      00010A 08               [12]  937 	inc	r0
      00010B A6 F0            [24]  938 	mov	@r0,b
      00010D 08               [12]  939 	inc	r0
      00010E F6               [12]  940 	mov	@r0,a
      00010F E5 81            [12]  941 	mov	a,sp
      000111 24 FC            [12]  942 	add	a,#0xfc
      000113 F5 81            [12]  943 	mov	sp,a
      000115 90 00 05         [24]  944 	mov	dptr,#_rgb
      000118 E0               [24]  945 	movx	a,@dptr
      000119 FA               [12]  946 	mov	r2,a
      00011A A3               [24]  947 	inc	dptr
      00011B E0               [24]  948 	movx	a,@dptr
      00011C FB               [12]  949 	mov	r3,a
      00011D 8A 82            [24]  950 	mov	dpl,r2
      00011F 8B 83            [24]  951 	mov	dph,r3
      000121 12 21 5A         [24]  952 	lcall	___uint2fs
      000124 C8               [12]  953 	xch	a,r0
      000125 E5 16            [12]  954 	mov	a,_bp
      000127 24 05            [12]  955 	add	a,#0x05
      000129 C8               [12]  956 	xch	a,r0
      00012A A6 82            [24]  957 	mov	@r0,dpl
      00012C 08               [12]  958 	inc	r0
      00012D A6 83            [24]  959 	mov	@r0,dph
      00012F 08               [12]  960 	inc	r0
      000130 A6 F0            [24]  961 	mov	@r0,b
      000132 08               [12]  962 	inc	r0
      000133 F6               [12]  963 	mov	@r0,a
      000134 D0 04            [24]  964 	pop	ar4
      000136 D0 05            [24]  965 	pop	ar5
      000138 E5 16            [12]  966 	mov	a,_bp
      00013A 24 05            [12]  967 	add	a,#0x05
      00013C F8               [12]  968 	mov	r0,a
      00013D E6               [12]  969 	mov	a,@r0
      00013E C0 E0            [24]  970 	push	acc
      000140 08               [12]  971 	inc	r0
      000141 E6               [12]  972 	mov	a,@r0
      000142 C0 E0            [24]  973 	push	acc
      000144 08               [12]  974 	inc	r0
      000145 E6               [12]  975 	mov	a,@r0
      000146 C0 E0            [24]  976 	push	acc
      000148 08               [12]  977 	inc	r0
      000149 E6               [12]  978 	mov	a,@r0
      00014A C0 E0            [24]  979 	push	acc
      00014C A8 16            [24]  980 	mov	r0,_bp
      00014E 08               [12]  981 	inc	r0
      00014F 86 82            [24]  982 	mov	dpl,@r0
      000151 08               [12]  983 	inc	r0
      000152 86 83            [24]  984 	mov	dph,@r0
      000154 08               [12]  985 	inc	r0
      000155 86 F0            [24]  986 	mov	b,@r0
      000157 08               [12]  987 	inc	r0
      000158 E6               [12]  988 	mov	a,@r0
      000159 12 22 34         [24]  989 	lcall	___fsdiv
      00015C AC 82            [24]  990 	mov	r4,dpl
      00015E AD 83            [24]  991 	mov	r5,dph
      000160 AE F0            [24]  992 	mov	r6,b
      000162 FF               [12]  993 	mov	r7,a
      000163 E5 81            [12]  994 	mov	a,sp
      000165 24 FC            [12]  995 	add	a,#0xfc
      000167 F5 81            [12]  996 	mov	sp,a
      000169 8C 82            [24]  997 	mov	dpl,r4
      00016B 8D 83            [24]  998 	mov	dph,r5
      00016D 8E F0            [24]  999 	mov	b,r6
      00016F EF               [12] 1000 	mov	a,r7
      000170 C0 05            [24] 1001 	push	ar5
      000172 C0 04            [24] 1002 	push	ar4
      000174 12 23 13         [24] 1003 	lcall	___fs2uchar
      000177 AF 82            [24] 1004 	mov	r7,dpl
      000179 D0 04            [24] 1005 	pop	ar4
      00017B D0 05            [24] 1006 	pop	ar5
      00017D 8F 02            [24] 1007 	mov	ar2,r7
                                   1008 ;	usr/main.c:68: g = rgb.g*255.0/rgb.c;
      00017F 90 00 09         [24] 1009 	mov	dptr,#(_rgb + 0x0004)
      000182 E0               [24] 1010 	movx	a,@dptr
      000183 FE               [12] 1011 	mov	r6,a
      000184 A3               [24] 1012 	inc	dptr
      000185 E0               [24] 1013 	movx	a,@dptr
      000186 FF               [12] 1014 	mov	r7,a
      000187 8E 82            [24] 1015 	mov	dpl,r6
      000189 8F 83            [24] 1016 	mov	dph,r7
      00018B C0 02            [24] 1017 	push	ar2
      00018D 12 21 5A         [24] 1018 	lcall	___uint2fs
      000190 AC 82            [24] 1019 	mov	r4,dpl
      000192 AD 83            [24] 1020 	mov	r5,dph
      000194 AE F0            [24] 1021 	mov	r6,b
      000196 FF               [12] 1022 	mov	r7,a
      000197 C0 04            [24] 1023 	push	ar4
      000199 C0 05            [24] 1024 	push	ar5
      00019B C0 06            [24] 1025 	push	ar6
      00019D C0 07            [24] 1026 	push	ar7
      00019F 90 00 00         [24] 1027 	mov	dptr,#0x0000
      0001A2 75 F0 7F         [24] 1028 	mov	b,#0x7f
      0001A5 74 43            [12] 1029 	mov	a,#0x43
      0001A7 12 1F 5A         [24] 1030 	lcall	___fsmul
      0001AA AC 82            [24] 1031 	mov	r4,dpl
      0001AC AD 83            [24] 1032 	mov	r5,dph
      0001AE AE F0            [24] 1033 	mov	r6,b
      0001B0 FF               [12] 1034 	mov	r7,a
      0001B1 E5 81            [12] 1035 	mov	a,sp
      0001B3 24 FC            [12] 1036 	add	a,#0xfc
      0001B5 F5 81            [12] 1037 	mov	sp,a
      0001B7 E5 16            [12] 1038 	mov	a,_bp
      0001B9 24 05            [12] 1039 	add	a,#0x05
      0001BB F8               [12] 1040 	mov	r0,a
      0001BC E6               [12] 1041 	mov	a,@r0
      0001BD C0 E0            [24] 1042 	push	acc
      0001BF 08               [12] 1043 	inc	r0
      0001C0 E6               [12] 1044 	mov	a,@r0
      0001C1 C0 E0            [24] 1045 	push	acc
      0001C3 08               [12] 1046 	inc	r0
      0001C4 E6               [12] 1047 	mov	a,@r0
      0001C5 C0 E0            [24] 1048 	push	acc
      0001C7 08               [12] 1049 	inc	r0
      0001C8 E6               [12] 1050 	mov	a,@r0
      0001C9 C0 E0            [24] 1051 	push	acc
      0001CB 8C 82            [24] 1052 	mov	dpl,r4
      0001CD 8D 83            [24] 1053 	mov	dph,r5
      0001CF 8E F0            [24] 1054 	mov	b,r6
      0001D1 EF               [12] 1055 	mov	a,r7
      0001D2 12 22 34         [24] 1056 	lcall	___fsdiv
      0001D5 AC 82            [24] 1057 	mov	r4,dpl
      0001D7 AD 83            [24] 1058 	mov	r5,dph
      0001D9 AE F0            [24] 1059 	mov	r6,b
      0001DB FF               [12] 1060 	mov	r7,a
      0001DC E5 81            [12] 1061 	mov	a,sp
      0001DE 24 FC            [12] 1062 	add	a,#0xfc
      0001E0 F5 81            [12] 1063 	mov	sp,a
      0001E2 D0 02            [24] 1064 	pop	ar2
      0001E4 8C 82            [24] 1065 	mov	dpl,r4
      0001E6 8D 83            [24] 1066 	mov	dph,r5
      0001E8 8E F0            [24] 1067 	mov	b,r6
      0001EA EF               [12] 1068 	mov	a,r7
      0001EB C0 05            [24] 1069 	push	ar5
      0001ED C0 04            [24] 1070 	push	ar4
      0001EF C0 02            [24] 1071 	push	ar2
      0001F1 12 23 13         [24] 1072 	lcall	___fs2uchar
      0001F4 AF 82            [24] 1073 	mov	r7,dpl
      0001F6 D0 02            [24] 1074 	pop	ar2
      0001F8 D0 04            [24] 1075 	pop	ar4
      0001FA D0 05            [24] 1076 	pop	ar5
      0001FC E5 16            [12] 1077 	mov	a,_bp
      0001FE 24 0F            [12] 1078 	add	a,#0x0f
      000200 F8               [12] 1079 	mov	r0,a
      000201 A6 07            [24] 1080 	mov	@r0,ar7
                                   1081 ;	usr/main.c:69: b = rgb.b*255.0/rgb.c;
      000203 90 00 0B         [24] 1082 	mov	dptr,#(_rgb + 0x0006)
      000206 E0               [24] 1083 	movx	a,@dptr
      000207 FE               [12] 1084 	mov	r6,a
      000208 A3               [24] 1085 	inc	dptr
      000209 E0               [24] 1086 	movx	a,@dptr
      00020A FF               [12] 1087 	mov	r7,a
      00020B 8E 82            [24] 1088 	mov	dpl,r6
      00020D 8F 83            [24] 1089 	mov	dph,r7
      00020F C0 02            [24] 1090 	push	ar2
      000211 12 21 5A         [24] 1091 	lcall	___uint2fs
      000214 AC 82            [24] 1092 	mov	r4,dpl
      000216 AD 83            [24] 1093 	mov	r5,dph
      000218 AE F0            [24] 1094 	mov	r6,b
      00021A FF               [12] 1095 	mov	r7,a
      00021B C0 04            [24] 1096 	push	ar4
      00021D C0 05            [24] 1097 	push	ar5
      00021F C0 06            [24] 1098 	push	ar6
      000221 C0 07            [24] 1099 	push	ar7
      000223 90 00 00         [24] 1100 	mov	dptr,#0x0000
      000226 75 F0 7F         [24] 1101 	mov	b,#0x7f
      000229 74 43            [12] 1102 	mov	a,#0x43
      00022B 12 1F 5A         [24] 1103 	lcall	___fsmul
      00022E AC 82            [24] 1104 	mov	r4,dpl
      000230 AD 83            [24] 1105 	mov	r5,dph
      000232 AE F0            [24] 1106 	mov	r6,b
      000234 FF               [12] 1107 	mov	r7,a
      000235 E5 81            [12] 1108 	mov	a,sp
      000237 24 FC            [12] 1109 	add	a,#0xfc
      000239 F5 81            [12] 1110 	mov	sp,a
      00023B E5 16            [12] 1111 	mov	a,_bp
      00023D 24 05            [12] 1112 	add	a,#0x05
      00023F F8               [12] 1113 	mov	r0,a
      000240 E6               [12] 1114 	mov	a,@r0
      000241 C0 E0            [24] 1115 	push	acc
      000243 08               [12] 1116 	inc	r0
      000244 E6               [12] 1117 	mov	a,@r0
      000245 C0 E0            [24] 1118 	push	acc
      000247 08               [12] 1119 	inc	r0
      000248 E6               [12] 1120 	mov	a,@r0
      000249 C0 E0            [24] 1121 	push	acc
      00024B 08               [12] 1122 	inc	r0
      00024C E6               [12] 1123 	mov	a,@r0
      00024D C0 E0            [24] 1124 	push	acc
      00024F 8C 82            [24] 1125 	mov	dpl,r4
      000251 8D 83            [24] 1126 	mov	dph,r5
      000253 8E F0            [24] 1127 	mov	b,r6
      000255 EF               [12] 1128 	mov	a,r7
      000256 12 22 34         [24] 1129 	lcall	___fsdiv
      000259 AC 82            [24] 1130 	mov	r4,dpl
      00025B AD 83            [24] 1131 	mov	r5,dph
      00025D AE F0            [24] 1132 	mov	r6,b
      00025F FF               [12] 1133 	mov	r7,a
      000260 E5 81            [12] 1134 	mov	a,sp
      000262 24 FC            [12] 1135 	add	a,#0xfc
      000264 F5 81            [12] 1136 	mov	sp,a
      000266 D0 02            [24] 1137 	pop	ar2
      000268 8C 82            [24] 1138 	mov	dpl,r4
      00026A 8D 83            [24] 1139 	mov	dph,r5
      00026C 8E F0            [24] 1140 	mov	b,r6
      00026E EF               [12] 1141 	mov	a,r7
      00026F C0 05            [24] 1142 	push	ar5
      000271 C0 04            [24] 1143 	push	ar4
      000273 C0 02            [24] 1144 	push	ar2
      000275 12 23 13         [24] 1145 	lcall	___fs2uchar
      000278 AF 82            [24] 1146 	mov	r7,dpl
      00027A E5 16            [12] 1147 	mov	a,_bp
      00027C 24 12            [12] 1148 	add	a,#0x12
      00027E F8               [12] 1149 	mov	r0,a
      00027F A6 07            [24] 1150 	mov	@r0,ar7
                                   1151 ;	usr/main.c:70: mDelaymS(20);
      000281 90 00 14         [24] 1152 	mov	dptr,#0x0014
      000284 12 06 E2         [24] 1153 	lcall	_mDelaymS
                                   1154 ;	usr/main.c:71: setCursor(0,0);//设置printf到屏幕上的字符串起始位置
      000287 E4               [12] 1155 	clr	a
      000288 C0 E0            [24] 1156 	push	acc
      00028A C0 E0            [24] 1157 	push	acc
      00028C 90 00 00         [24] 1158 	mov	dptr,#0x0000
      00028F 12 06 91         [24] 1159 	lcall	_setCursor
      000292 15 81            [12] 1160 	dec	sp
      000294 15 81            [12] 1161 	dec	sp
                                   1162 ;	usr/main.c:72: printf_fast_f("R : %u    ",m);
      000296 E5 16            [12] 1163 	mov	a,_bp
      000298 24 13            [12] 1164 	add	a,#0x13
      00029A F8               [12] 1165 	mov	r0,a
      00029B E6               [12] 1166 	mov	a,@r0
      00029C C0 E0            [24] 1167 	push	acc
      00029E 08               [12] 1168 	inc	r0
      00029F E6               [12] 1169 	mov	a,@r0
      0002A0 C0 E0            [24] 1170 	push	acc
      0002A2 74 CE            [12] 1171 	mov	a,#___str_0
      0002A4 C0 E0            [24] 1172 	push	acc
      0002A6 74 3D            [12] 1173 	mov	a,#(___str_0 >> 8)
      0002A8 C0 E0            [24] 1174 	push	acc
      0002AA 12 1A D9         [24] 1175 	lcall	_printf_fast_f
      0002AD E5 81            [12] 1176 	mov	a,sp
      0002AF 24 FC            [12] 1177 	add	a,#0xfc
      0002B1 F5 81            [12] 1178 	mov	sp,a
                                   1179 ;	usr/main.c:73: setCursor(0,2);//设置printf到屏幕上的字符串起始位置
      0002B3 74 02            [12] 1180 	mov	a,#0x02
      0002B5 C0 E0            [24] 1181 	push	acc
      0002B7 E4               [12] 1182 	clr	a
      0002B8 C0 E0            [24] 1183 	push	acc
      0002BA 90 00 00         [24] 1184 	mov	dptr,#0x0000
      0002BD 12 06 91         [24] 1185 	lcall	_setCursor
      0002C0 15 81            [12] 1186 	dec	sp
      0002C2 15 81            [12] 1187 	dec	sp
                                   1188 ;	usr/main.c:74: printf_fast_f("G : %u    ",n);
      0002C4 E5 16            [12] 1189 	mov	a,_bp
      0002C6 24 0D            [12] 1190 	add	a,#0x0d
      0002C8 F8               [12] 1191 	mov	r0,a
      0002C9 E6               [12] 1192 	mov	a,@r0
      0002CA C0 E0            [24] 1193 	push	acc
      0002CC 08               [12] 1194 	inc	r0
      0002CD E6               [12] 1195 	mov	a,@r0
      0002CE C0 E0            [24] 1196 	push	acc
      0002D0 74 D9            [12] 1197 	mov	a,#___str_1
      0002D2 C0 E0            [24] 1198 	push	acc
      0002D4 74 3D            [12] 1199 	mov	a,#(___str_1 >> 8)
      0002D6 C0 E0            [24] 1200 	push	acc
      0002D8 12 1A D9         [24] 1201 	lcall	_printf_fast_f
      0002DB E5 81            [12] 1202 	mov	a,sp
      0002DD 24 FC            [12] 1203 	add	a,#0xfc
      0002DF F5 81            [12] 1204 	mov	sp,a
                                   1205 ;	usr/main.c:75: setCursor(0,4);//设置printf到屏幕上的字符串起始位置
      0002E1 74 04            [12] 1206 	mov	a,#0x04
      0002E3 C0 E0            [24] 1207 	push	acc
      0002E5 E4               [12] 1208 	clr	a
      0002E6 C0 E0            [24] 1209 	push	acc
      0002E8 90 00 00         [24] 1210 	mov	dptr,#0x0000
      0002EB 12 06 91         [24] 1211 	lcall	_setCursor
      0002EE 15 81            [12] 1212 	dec	sp
      0002F0 15 81            [12] 1213 	dec	sp
                                   1214 ;	usr/main.c:76: printf_fast_f("B : %u    ",l);
      0002F2 E5 16            [12] 1215 	mov	a,_bp
      0002F4 24 10            [12] 1216 	add	a,#0x10
      0002F6 F8               [12] 1217 	mov	r0,a
      0002F7 E6               [12] 1218 	mov	a,@r0
      0002F8 C0 E0            [24] 1219 	push	acc
      0002FA 08               [12] 1220 	inc	r0
      0002FB E6               [12] 1221 	mov	a,@r0
      0002FC C0 E0            [24] 1222 	push	acc
      0002FE 74 E4            [12] 1223 	mov	a,#___str_2
      000300 C0 E0            [24] 1224 	push	acc
      000302 74 3D            [12] 1225 	mov	a,#(___str_2 >> 8)
      000304 C0 E0            [24] 1226 	push	acc
      000306 12 1A D9         [24] 1227 	lcall	_printf_fast_f
      000309 E5 81            [12] 1228 	mov	a,sp
      00030B 24 FC            [12] 1229 	add	a,#0xfc
      00030D F5 81            [12] 1230 	mov	sp,a
                                   1231 ;	usr/main.c:77: setCursor(0,6);//设置printf到屏幕上的字符串起始位置
      00030F 74 06            [12] 1232 	mov	a,#0x06
      000311 C0 E0            [24] 1233 	push	acc
      000313 E4               [12] 1234 	clr	a
      000314 C0 E0            [24] 1235 	push	acc
      000316 90 00 00         [24] 1236 	mov	dptr,#0x0000
      000319 12 06 91         [24] 1237 	lcall	_setCursor
      00031C 15 81            [12] 1238 	dec	sp
      00031E 15 81            [12] 1239 	dec	sp
                                   1240 ;	usr/main.c:78: printf_fast_f("RGB.C : %u    ",rgb.c);
      000320 90 00 05         [24] 1241 	mov	dptr,#_rgb
      000323 E0               [24] 1242 	movx	a,@dptr
      000324 FE               [12] 1243 	mov	r6,a
      000325 A3               [24] 1244 	inc	dptr
      000326 E0               [24] 1245 	movx	a,@dptr
      000327 FF               [12] 1246 	mov	r7,a
      000328 C0 06            [24] 1247 	push	ar6
      00032A C0 07            [24] 1248 	push	ar7
      00032C 74 EF            [12] 1249 	mov	a,#___str_3
      00032E C0 E0            [24] 1250 	push	acc
      000330 74 3D            [12] 1251 	mov	a,#(___str_3 >> 8)
      000332 C0 E0            [24] 1252 	push	acc
      000334 12 1A D9         [24] 1253 	lcall	_printf_fast_f
      000337 E5 81            [12] 1254 	mov	a,sp
      000339 24 FC            [12] 1255 	add	a,#0xfc
      00033B F5 81            [12] 1256 	mov	sp,a
      00033D D0 02            [24] 1257 	pop	ar2
      00033F D0 04            [24] 1258 	pop	ar4
      000341 D0 05            [24] 1259 	pop	ar5
                                   1260 ;	usr/main.c:80: if (rgb.c > 100)           //能检测到亮度时开始读取颜色值
      000343 90 00 05         [24] 1261 	mov	dptr,#_rgb
      000346 E0               [24] 1262 	movx	a,@dptr
      000347 FE               [12] 1263 	mov	r6,a
      000348 A3               [24] 1264 	inc	dptr
      000349 E0               [24] 1265 	movx	a,@dptr
      00034A FF               [12] 1266 	mov	r7,a
      00034B E5 16            [12] 1267 	mov	a,_bp
      00034D 24 05            [12] 1268 	add	a,#0x05
      00034F F8               [12] 1269 	mov	r0,a
      000350 A6 06            [24] 1270 	mov	@r0,ar6
      000352 08               [12] 1271 	inc	r0
      000353 A6 07            [24] 1272 	mov	@r0,ar7
      000355 E5 16            [12] 1273 	mov	a,_bp
      000357 24 05            [12] 1274 	add	a,#0x05
      000359 F8               [12] 1275 	mov	r0,a
      00035A C3               [12] 1276 	clr	c
      00035B 74 64            [12] 1277 	mov	a,#0x64
      00035D 96               [12] 1278 	subb	a,@r0
      00035E E4               [12] 1279 	clr	a
      00035F 08               [12] 1280 	inc	r0
      000360 96               [12] 1281 	subb	a,@r0
      000361 D0 05            [24] 1282 	pop	ar5
      000363 D0 04            [24] 1283 	pop	ar4
      000365 40 03            [24] 1284 	jc	00219$
      000367 02 04 37         [24] 1285 	ljmp	00108$
      00036A                       1286 00219$:
                                   1287 ;	usr/main.c:83: if (i == 3)              //2次循环后再读取防抖动
      00036A 8C 06            [24] 1288 	mov	ar6,r4
      00036C 8D 07            [24] 1289 	mov	ar7,r5
      00036E BE 03 05         [24] 1290 	cjne	r6,#0x03,00220$
      000371 BF 00 02         [24] 1291 	cjne	r7,#0x00,00220$
      000374 80 03            [24] 1292 	sjmp	00221$
      000376                       1293 00220$:
      000376 02 04 32         [24] 1294 	ljmp	00106$
      000379                       1295 00221$:
                                   1296 ;	usr/main.c:85: if ((max3v(r,g,b) > 240) || (max3v(r,g,b) < 60)) //经过防抖动之后可能还是有数据异常的情况，再检测一次
      000379 E5 16            [12] 1297 	mov	a,_bp
      00037B 24 0F            [12] 1298 	add	a,#0x0f
      00037D F8               [12] 1299 	mov	r0,a
      00037E C3               [12] 1300 	clr	c
      00037F EA               [12] 1301 	mov	a,r2
      000380 96               [12] 1302 	subb	a,@r0
      000381 E4               [12] 1303 	clr	a
      000382 33               [12] 1304 	rlc	a
      000383 FF               [12] 1305 	mov	r7,a
      000384 60 21            [24] 1306 	jz	00131$
      000386 E5 16            [12] 1307 	mov	a,_bp
      000388 24 0F            [12] 1308 	add	a,#0x0f
      00038A F8               [12] 1309 	mov	r0,a
      00038B E5 16            [12] 1310 	mov	a,_bp
      00038D 24 12            [12] 1311 	add	a,#0x12
      00038F F9               [12] 1312 	mov	r1,a
      000390 C3               [12] 1313 	clr	c
      000391 E6               [12] 1314 	mov	a,@r0
      000392 97               [12] 1315 	subb	a,@r1
      000393 50 09            [24] 1316 	jnc	00133$
      000395 E5 16            [12] 1317 	mov	a,_bp
      000397 24 12            [12] 1318 	add	a,#0x12
      000399 F8               [12] 1319 	mov	r0,a
      00039A 86 06            [24] 1320 	mov	ar6,@r0
      00039C 80 20            [24] 1321 	sjmp	00132$
      00039E                       1322 00133$:
      00039E E5 16            [12] 1323 	mov	a,_bp
      0003A0 24 0F            [12] 1324 	add	a,#0x0f
      0003A2 F8               [12] 1325 	mov	r0,a
      0003A3 86 06            [24] 1326 	mov	ar6,@r0
      0003A5 80 17            [24] 1327 	sjmp	00132$
      0003A7                       1328 00131$:
      0003A7 E5 16            [12] 1329 	mov	a,_bp
      0003A9 24 12            [12] 1330 	add	a,#0x12
      0003AB F8               [12] 1331 	mov	r0,a
      0003AC C3               [12] 1332 	clr	c
      0003AD EA               [12] 1333 	mov	a,r2
      0003AE 96               [12] 1334 	subb	a,@r0
      0003AF 50 09            [24] 1335 	jnc	00135$
      0003B1 E5 16            [12] 1336 	mov	a,_bp
      0003B3 24 12            [12] 1337 	add	a,#0x12
      0003B5 F8               [12] 1338 	mov	r0,a
      0003B6 86 03            [24] 1339 	mov	ar3,@r0
      0003B8 80 02            [24] 1340 	sjmp	00136$
      0003BA                       1341 00135$:
      0003BA 8A 03            [24] 1342 	mov	ar3,r2
      0003BC                       1343 00136$:
      0003BC 8B 06            [24] 1344 	mov	ar6,r3
      0003BE                       1345 00132$:
      0003BE EE               [12] 1346 	mov	a,r6
      0003BF 24 0F            [12] 1347 	add	a,#0xff - 0xf0
      0003C1 40 40            [24] 1348 	jc	00101$
      0003C3 EF               [12] 1349 	mov	a,r7
      0003C4 60 21            [24] 1350 	jz	00137$
      0003C6 E5 16            [12] 1351 	mov	a,_bp
      0003C8 24 0F            [12] 1352 	add	a,#0x0f
      0003CA F8               [12] 1353 	mov	r0,a
      0003CB E5 16            [12] 1354 	mov	a,_bp
      0003CD 24 12            [12] 1355 	add	a,#0x12
      0003CF F9               [12] 1356 	mov	r1,a
      0003D0 C3               [12] 1357 	clr	c
      0003D1 E6               [12] 1358 	mov	a,@r0
      0003D2 97               [12] 1359 	subb	a,@r1
      0003D3 50 09            [24] 1360 	jnc	00139$
      0003D5 E5 16            [12] 1361 	mov	a,_bp
      0003D7 24 12            [12] 1362 	add	a,#0x12
      0003D9 F8               [12] 1363 	mov	r0,a
      0003DA 86 07            [24] 1364 	mov	ar7,@r0
      0003DC 80 20            [24] 1365 	sjmp	00138$
      0003DE                       1366 00139$:
      0003DE E5 16            [12] 1367 	mov	a,_bp
      0003E0 24 0F            [12] 1368 	add	a,#0x0f
      0003E2 F8               [12] 1369 	mov	r0,a
      0003E3 86 07            [24] 1370 	mov	ar7,@r0
      0003E5 80 17            [24] 1371 	sjmp	00138$
      0003E7                       1372 00137$:
      0003E7 E5 16            [12] 1373 	mov	a,_bp
      0003E9 24 12            [12] 1374 	add	a,#0x12
      0003EB F8               [12] 1375 	mov	r0,a
      0003EC C3               [12] 1376 	clr	c
      0003ED EA               [12] 1377 	mov	a,r2
      0003EE 96               [12] 1378 	subb	a,@r0
      0003EF 50 09            [24] 1379 	jnc	00141$
      0003F1 E5 16            [12] 1380 	mov	a,_bp
      0003F3 24 12            [12] 1381 	add	a,#0x12
      0003F5 F8               [12] 1382 	mov	r0,a
      0003F6 86 06            [24] 1383 	mov	ar6,@r0
      0003F8 80 02            [24] 1384 	sjmp	00142$
      0003FA                       1385 00141$:
      0003FA 8A 06            [24] 1386 	mov	ar6,r2
      0003FC                       1387 00142$:
      0003FC 8E 07            [24] 1388 	mov	ar7,r6
      0003FE                       1389 00138$:
      0003FE BF 3C 00         [24] 1390 	cjne	r7,#0x3c,00229$
      000401                       1391 00229$:
      000401 50 07            [24] 1392 	jnc	00102$
      000403                       1393 00101$:
                                   1394 ;	usr/main.c:87: i--;
      000403 1C               [12] 1395 	dec	r4
      000404 BC FF 01         [24] 1396 	cjne	r4,#0xff,00231$
      000407 1D               [12] 1397 	dec	r5
      000408                       1398 00231$:
      000408 80 28            [24] 1399 	sjmp	00106$
      00040A                       1400 00102$:
                                   1401 ;	usr/main.c:90: m = r;         //读取当前RGB值
      00040A E5 16            [12] 1402 	mov	a,_bp
      00040C 24 13            [12] 1403 	add	a,#0x13
      00040E F8               [12] 1404 	mov	r0,a
      00040F A6 02            [24] 1405 	mov	@r0,ar2
      000411 08               [12] 1406 	inc	r0
      000412 76 00            [12] 1407 	mov	@r0,#0x00
                                   1408 ;	usr/main.c:91: n = g;
      000414 E5 16            [12] 1409 	mov	a,_bp
      000416 24 0F            [12] 1410 	add	a,#0x0f
      000418 F8               [12] 1411 	mov	r0,a
      000419 E5 16            [12] 1412 	mov	a,_bp
      00041B 24 0D            [12] 1413 	add	a,#0x0d
      00041D F9               [12] 1414 	mov	r1,a
      00041E E6               [12] 1415 	mov	a,@r0
      00041F F7               [12] 1416 	mov	@r1,a
      000420 09               [12] 1417 	inc	r1
      000421 77 00            [12] 1418 	mov	@r1,#0x00
                                   1419 ;	usr/main.c:92: l = b;
      000423 E5 16            [12] 1420 	mov	a,_bp
      000425 24 12            [12] 1421 	add	a,#0x12
      000427 F8               [12] 1422 	mov	r0,a
      000428 E5 16            [12] 1423 	mov	a,_bp
      00042A 24 10            [12] 1424 	add	a,#0x10
      00042C F9               [12] 1425 	mov	r1,a
      00042D E6               [12] 1426 	mov	a,@r0
      00042E F7               [12] 1427 	mov	@r1,a
      00042F 09               [12] 1428 	inc	r1
      000430 77 00            [12] 1429 	mov	@r1,#0x00
      000432                       1430 00106$:
                                   1431 ;	usr/main.c:95: i++;      
      000432 0C               [12] 1432 	inc	r4
      000433 BC 00 01         [24] 1433 	cjne	r4,#0x00,00232$
      000436 0D               [12] 1434 	inc	r5
      000437                       1435 00232$:
      000437                       1436 00108$:
                                   1437 ;	usr/main.c:97: if (rgb.c < 100)           //亮度归零意味着小球经过监测点，开始根据读取到的RGB值进行判断
      000437 E5 16            [12] 1438 	mov	a,_bp
      000439 24 05            [12] 1439 	add	a,#0x05
      00043B F8               [12] 1440 	mov	r0,a
      00043C C3               [12] 1441 	clr	c
      00043D E6               [12] 1442 	mov	a,@r0
      00043E 94 64            [12] 1443 	subb	a,#0x64
      000440 08               [12] 1444 	inc	r0
      000441 E6               [12] 1445 	mov	a,@r0
      000442 94 00            [12] 1446 	subb	a,#0x00
      000444 40 03            [24] 1447 	jc	00233$
      000446 02 06 0D         [24] 1448 	ljmp	00125$
      000449                       1449 00233$:
                                   1450 ;	usr/main.c:99: if (i != 0)           //仅判断一次
      000449 EC               [12] 1451 	mov	a,r4
      00044A 4D               [12] 1452 	orl	a,r5
      00044B 70 03            [24] 1453 	jnz	00234$
      00044D 02 06 09         [24] 1454 	ljmp	00123$
      000450                       1455 00234$:
                                   1456 ;	usr/main.c:101: if (m > n && m > l)
      000450 E5 16            [12] 1457 	mov	a,_bp
      000452 24 13            [12] 1458 	add	a,#0x13
      000454 F8               [12] 1459 	mov	r0,a
      000455 E5 16            [12] 1460 	mov	a,_bp
      000457 24 0D            [12] 1461 	add	a,#0x0d
      000459 F9               [12] 1462 	mov	r1,a
      00045A C3               [12] 1463 	clr	c
      00045B E7               [12] 1464 	mov	a,@r1
      00045C 96               [12] 1465 	subb	a,@r0
      00045D 09               [12] 1466 	inc	r1
      00045E E7               [12] 1467 	mov	a,@r1
      00045F 08               [12] 1468 	inc	r0
      000460 96               [12] 1469 	subb	a,@r0
      000461 50 5F            [24] 1470 	jnc	00119$
      000463 E5 16            [12] 1471 	mov	a,_bp
      000465 24 13            [12] 1472 	add	a,#0x13
      000467 F8               [12] 1473 	mov	r0,a
      000468 E5 16            [12] 1474 	mov	a,_bp
      00046A 24 10            [12] 1475 	add	a,#0x10
      00046C F9               [12] 1476 	mov	r1,a
      00046D C3               [12] 1477 	clr	c
      00046E E7               [12] 1478 	mov	a,@r1
      00046F 96               [12] 1479 	subb	a,@r0
      000470 09               [12] 1480 	inc	r1
      000471 E7               [12] 1481 	mov	a,@r1
      000472 08               [12] 1482 	inc	r0
      000473 96               [12] 1483 	subb	a,@r0
      000474 50 4C            [24] 1484 	jnc	00119$
                                   1485 ;	usr/main.c:103: M = M << 8 | box[0]; //红球
      000476 E5 16            [12] 1486 	mov	a,_bp
      000478 24 09            [12] 1487 	add	a,#0x09
      00047A F8               [12] 1488 	mov	r0,a
      00047B E5 16            [12] 1489 	mov	a,_bp
      00047D 24 05            [12] 1490 	add	a,#0x05
      00047F F9               [12] 1491 	mov	r1,a
      000480 08               [12] 1492 	inc	r0
      000481 08               [12] 1493 	inc	r0
      000482 E6               [12] 1494 	mov	a,@r0
      000483 09               [12] 1495 	inc	r1
      000484 09               [12] 1496 	inc	r1
      000485 09               [12] 1497 	inc	r1
      000486 F7               [12] 1498 	mov	@r1,a
      000487 18               [12] 1499 	dec	r0
      000488 E6               [12] 1500 	mov	a,@r0
      000489 19               [12] 1501 	dec	r1
      00048A F7               [12] 1502 	mov	@r1,a
      00048B 18               [12] 1503 	dec	r0
      00048C E6               [12] 1504 	mov	a,@r0
      00048D 19               [12] 1505 	dec	r1
      00048E F7               [12] 1506 	mov	@r1,a
      00048F 19               [12] 1507 	dec	r1
      000490 77 00            [12] 1508 	mov	@r1,#0x00
      000492 90 00 01         [24] 1509 	mov	dptr,#_box
      000495 E0               [24] 1510 	movx	a,@dptr
      000496 FA               [12] 1511 	mov	r2,a
      000497 E4               [12] 1512 	clr	a
      000498 FB               [12] 1513 	mov	r3,a
      000499 FE               [12] 1514 	mov	r6,a
      00049A FF               [12] 1515 	mov	r7,a
      00049B E5 16            [12] 1516 	mov	a,_bp
      00049D 24 05            [12] 1517 	add	a,#0x05
      00049F F8               [12] 1518 	mov	r0,a
      0004A0 E6               [12] 1519 	mov	a,@r0
      0004A1 42 02            [12] 1520 	orl	ar2,a
      0004A3 08               [12] 1521 	inc	r0
      0004A4 E6               [12] 1522 	mov	a,@r0
      0004A5 42 03            [12] 1523 	orl	ar3,a
      0004A7 08               [12] 1524 	inc	r0
      0004A8 E6               [12] 1525 	mov	a,@r0
      0004A9 42 06            [12] 1526 	orl	ar6,a
      0004AB 08               [12] 1527 	inc	r0
      0004AC E6               [12] 1528 	mov	a,@r0
      0004AD 42 07            [12] 1529 	orl	ar7,a
      0004AF E5 16            [12] 1530 	mov	a,_bp
      0004B1 24 09            [12] 1531 	add	a,#0x09
      0004B3 F8               [12] 1532 	mov	r0,a
      0004B4 A6 02            [24] 1533 	mov	@r0,ar2
      0004B6 08               [12] 1534 	inc	r0
      0004B7 A6 03            [24] 1535 	mov	@r0,ar3
      0004B9 08               [12] 1536 	inc	r0
      0004BA A6 06            [24] 1537 	mov	@r0,ar6
      0004BC 08               [12] 1538 	inc	r0
      0004BD A6 07            [24] 1539 	mov	@r0,ar7
      0004BF 02 06 09         [24] 1540 	ljmp	00123$
      0004C2                       1541 00119$:
                                   1542 ;	usr/main.c:105: else if ((l - n > 10) && l > m && l > n)
      0004C2 E5 16            [12] 1543 	mov	a,_bp
      0004C4 24 10            [12] 1544 	add	a,#0x10
      0004C6 F8               [12] 1545 	mov	r0,a
      0004C7 E5 16            [12] 1546 	mov	a,_bp
      0004C9 24 0D            [12] 1547 	add	a,#0x0d
      0004CB F9               [12] 1548 	mov	r1,a
      0004CC E6               [12] 1549 	mov	a,@r0
      0004CD C3               [12] 1550 	clr	c
      0004CE 97               [12] 1551 	subb	a,@r1
      0004CF FE               [12] 1552 	mov	r6,a
      0004D0 08               [12] 1553 	inc	r0
      0004D1 E6               [12] 1554 	mov	a,@r0
      0004D2 09               [12] 1555 	inc	r1
      0004D3 97               [12] 1556 	subb	a,@r1
      0004D4 FF               [12] 1557 	mov	r7,a
      0004D5 C3               [12] 1558 	clr	c
      0004D6 74 0A            [12] 1559 	mov	a,#0x0a
      0004D8 9E               [12] 1560 	subb	a,r6
      0004D9 E4               [12] 1561 	clr	a
      0004DA 9F               [12] 1562 	subb	a,r7
      0004DB 50 72            [24] 1563 	jnc	00114$
      0004DD E5 16            [12] 1564 	mov	a,_bp
      0004DF 24 10            [12] 1565 	add	a,#0x10
      0004E1 F8               [12] 1566 	mov	r0,a
      0004E2 E5 16            [12] 1567 	mov	a,_bp
      0004E4 24 13            [12] 1568 	add	a,#0x13
      0004E6 F9               [12] 1569 	mov	r1,a
      0004E7 C3               [12] 1570 	clr	c
      0004E8 E7               [12] 1571 	mov	a,@r1
      0004E9 96               [12] 1572 	subb	a,@r0
      0004EA 09               [12] 1573 	inc	r1
      0004EB E7               [12] 1574 	mov	a,@r1
      0004EC 08               [12] 1575 	inc	r0
      0004ED 96               [12] 1576 	subb	a,@r0
      0004EE 50 5F            [24] 1577 	jnc	00114$
      0004F0 E5 16            [12] 1578 	mov	a,_bp
      0004F2 24 10            [12] 1579 	add	a,#0x10
      0004F4 F8               [12] 1580 	mov	r0,a
      0004F5 E5 16            [12] 1581 	mov	a,_bp
      0004F7 24 0D            [12] 1582 	add	a,#0x0d
      0004F9 F9               [12] 1583 	mov	r1,a
      0004FA C3               [12] 1584 	clr	c
      0004FB E7               [12] 1585 	mov	a,@r1
      0004FC 96               [12] 1586 	subb	a,@r0
      0004FD 09               [12] 1587 	inc	r1
      0004FE E7               [12] 1588 	mov	a,@r1
      0004FF 08               [12] 1589 	inc	r0
      000500 96               [12] 1590 	subb	a,@r0
      000501 50 4C            [24] 1591 	jnc	00114$
                                   1592 ;	usr/main.c:107: M = M << 8 | box[1]; //蓝球
      000503 E5 16            [12] 1593 	mov	a,_bp
      000505 24 09            [12] 1594 	add	a,#0x09
      000507 F8               [12] 1595 	mov	r0,a
      000508 E5 16            [12] 1596 	mov	a,_bp
      00050A 24 05            [12] 1597 	add	a,#0x05
      00050C F9               [12] 1598 	mov	r1,a
      00050D 08               [12] 1599 	inc	r0
      00050E 08               [12] 1600 	inc	r0
      00050F E6               [12] 1601 	mov	a,@r0
      000510 09               [12] 1602 	inc	r1
      000511 09               [12] 1603 	inc	r1
      000512 09               [12] 1604 	inc	r1
      000513 F7               [12] 1605 	mov	@r1,a
      000514 18               [12] 1606 	dec	r0
      000515 E6               [12] 1607 	mov	a,@r0
      000516 19               [12] 1608 	dec	r1
      000517 F7               [12] 1609 	mov	@r1,a
      000518 18               [12] 1610 	dec	r0
      000519 E6               [12] 1611 	mov	a,@r0
      00051A 19               [12] 1612 	dec	r1
      00051B F7               [12] 1613 	mov	@r1,a
      00051C 19               [12] 1614 	dec	r1
      00051D 77 00            [12] 1615 	mov	@r1,#0x00
      00051F 90 00 02         [24] 1616 	mov	dptr,#(_box + 0x0001)
      000522 E0               [24] 1617 	movx	a,@dptr
      000523 FA               [12] 1618 	mov	r2,a
      000524 E4               [12] 1619 	clr	a
      000525 FB               [12] 1620 	mov	r3,a
      000526 FE               [12] 1621 	mov	r6,a
      000527 FF               [12] 1622 	mov	r7,a
      000528 E5 16            [12] 1623 	mov	a,_bp
      00052A 24 05            [12] 1624 	add	a,#0x05
      00052C F8               [12] 1625 	mov	r0,a
      00052D E6               [12] 1626 	mov	a,@r0
      00052E 42 02            [12] 1627 	orl	ar2,a
      000530 08               [12] 1628 	inc	r0
      000531 E6               [12] 1629 	mov	a,@r0
      000532 42 03            [12] 1630 	orl	ar3,a
      000534 08               [12] 1631 	inc	r0
      000535 E6               [12] 1632 	mov	a,@r0
      000536 42 06            [12] 1633 	orl	ar6,a
      000538 08               [12] 1634 	inc	r0
      000539 E6               [12] 1635 	mov	a,@r0
      00053A 42 07            [12] 1636 	orl	ar7,a
      00053C E5 16            [12] 1637 	mov	a,_bp
      00053E 24 09            [12] 1638 	add	a,#0x09
      000540 F8               [12] 1639 	mov	r0,a
      000541 A6 02            [24] 1640 	mov	@r0,ar2
      000543 08               [12] 1641 	inc	r0
      000544 A6 03            [24] 1642 	mov	@r0,ar3
      000546 08               [12] 1643 	inc	r0
      000547 A6 06            [24] 1644 	mov	@r0,ar6
      000549 08               [12] 1645 	inc	r0
      00054A A6 07            [24] 1646 	mov	@r0,ar7
      00054C 02 06 09         [24] 1647 	ljmp	00123$
      00054F                       1648 00114$:
                                   1649 ;	usr/main.c:109: else if (n > m && n > l)
      00054F E5 16            [12] 1650 	mov	a,_bp
      000551 24 0D            [12] 1651 	add	a,#0x0d
      000553 F8               [12] 1652 	mov	r0,a
      000554 E5 16            [12] 1653 	mov	a,_bp
      000556 24 13            [12] 1654 	add	a,#0x13
      000558 F9               [12] 1655 	mov	r1,a
      000559 C3               [12] 1656 	clr	c
      00055A E7               [12] 1657 	mov	a,@r1
      00055B 96               [12] 1658 	subb	a,@r0
      00055C 09               [12] 1659 	inc	r1
      00055D E7               [12] 1660 	mov	a,@r1
      00055E 08               [12] 1661 	inc	r0
      00055F 96               [12] 1662 	subb	a,@r0
      000560 50 5E            [24] 1663 	jnc	00110$
      000562 E5 16            [12] 1664 	mov	a,_bp
      000564 24 0D            [12] 1665 	add	a,#0x0d
      000566 F8               [12] 1666 	mov	r0,a
      000567 E5 16            [12] 1667 	mov	a,_bp
      000569 24 10            [12] 1668 	add	a,#0x10
      00056B F9               [12] 1669 	mov	r1,a
      00056C C3               [12] 1670 	clr	c
      00056D E7               [12] 1671 	mov	a,@r1
      00056E 96               [12] 1672 	subb	a,@r0
      00056F 09               [12] 1673 	inc	r1
      000570 E7               [12] 1674 	mov	a,@r1
      000571 08               [12] 1675 	inc	r0
      000572 96               [12] 1676 	subb	a,@r0
      000573 50 4B            [24] 1677 	jnc	00110$
                                   1678 ;	usr/main.c:111: M = M << 8 | box[2]; //绿球
      000575 E5 16            [12] 1679 	mov	a,_bp
      000577 24 09            [12] 1680 	add	a,#0x09
      000579 F8               [12] 1681 	mov	r0,a
      00057A E5 16            [12] 1682 	mov	a,_bp
      00057C 24 05            [12] 1683 	add	a,#0x05
      00057E F9               [12] 1684 	mov	r1,a
      00057F 08               [12] 1685 	inc	r0
      000580 08               [12] 1686 	inc	r0
      000581 E6               [12] 1687 	mov	a,@r0
      000582 09               [12] 1688 	inc	r1
      000583 09               [12] 1689 	inc	r1
      000584 09               [12] 1690 	inc	r1
      000585 F7               [12] 1691 	mov	@r1,a
      000586 18               [12] 1692 	dec	r0
      000587 E6               [12] 1693 	mov	a,@r0
      000588 19               [12] 1694 	dec	r1
      000589 F7               [12] 1695 	mov	@r1,a
      00058A 18               [12] 1696 	dec	r0
      00058B E6               [12] 1697 	mov	a,@r0
      00058C 19               [12] 1698 	dec	r1
      00058D F7               [12] 1699 	mov	@r1,a
      00058E 19               [12] 1700 	dec	r1
      00058F 77 00            [12] 1701 	mov	@r1,#0x00
      000591 90 00 03         [24] 1702 	mov	dptr,#(_box + 0x0002)
      000594 E0               [24] 1703 	movx	a,@dptr
      000595 FA               [12] 1704 	mov	r2,a
      000596 E4               [12] 1705 	clr	a
      000597 FB               [12] 1706 	mov	r3,a
      000598 FE               [12] 1707 	mov	r6,a
      000599 FF               [12] 1708 	mov	r7,a
      00059A E5 16            [12] 1709 	mov	a,_bp
      00059C 24 05            [12] 1710 	add	a,#0x05
      00059E F8               [12] 1711 	mov	r0,a
      00059F E6               [12] 1712 	mov	a,@r0
      0005A0 42 02            [12] 1713 	orl	ar2,a
      0005A2 08               [12] 1714 	inc	r0
      0005A3 E6               [12] 1715 	mov	a,@r0
      0005A4 42 03            [12] 1716 	orl	ar3,a
      0005A6 08               [12] 1717 	inc	r0
      0005A7 E6               [12] 1718 	mov	a,@r0
      0005A8 42 06            [12] 1719 	orl	ar6,a
      0005AA 08               [12] 1720 	inc	r0
      0005AB E6               [12] 1721 	mov	a,@r0
      0005AC 42 07            [12] 1722 	orl	ar7,a
      0005AE E5 16            [12] 1723 	mov	a,_bp
      0005B0 24 09            [12] 1724 	add	a,#0x09
      0005B2 F8               [12] 1725 	mov	r0,a
      0005B3 A6 02            [24] 1726 	mov	@r0,ar2
      0005B5 08               [12] 1727 	inc	r0
      0005B6 A6 03            [24] 1728 	mov	@r0,ar3
      0005B8 08               [12] 1729 	inc	r0
      0005B9 A6 06            [24] 1730 	mov	@r0,ar6
      0005BB 08               [12] 1731 	inc	r0
      0005BC A6 07            [24] 1732 	mov	@r0,ar7
      0005BE 80 49            [24] 1733 	sjmp	00123$
      0005C0                       1734 00110$:
                                   1735 ;	usr/main.c:119: M = M << 8 | box[3]; //白球
      0005C0 E5 16            [12] 1736 	mov	a,_bp
      0005C2 24 09            [12] 1737 	add	a,#0x09
      0005C4 F8               [12] 1738 	mov	r0,a
      0005C5 E5 16            [12] 1739 	mov	a,_bp
      0005C7 24 05            [12] 1740 	add	a,#0x05
      0005C9 F9               [12] 1741 	mov	r1,a
      0005CA 08               [12] 1742 	inc	r0
      0005CB 08               [12] 1743 	inc	r0
      0005CC E6               [12] 1744 	mov	a,@r0
      0005CD 09               [12] 1745 	inc	r1
      0005CE 09               [12] 1746 	inc	r1
      0005CF 09               [12] 1747 	inc	r1
      0005D0 F7               [12] 1748 	mov	@r1,a
      0005D1 18               [12] 1749 	dec	r0
      0005D2 E6               [12] 1750 	mov	a,@r0
      0005D3 19               [12] 1751 	dec	r1
      0005D4 F7               [12] 1752 	mov	@r1,a
      0005D5 18               [12] 1753 	dec	r0
      0005D6 E6               [12] 1754 	mov	a,@r0
      0005D7 19               [12] 1755 	dec	r1
      0005D8 F7               [12] 1756 	mov	@r1,a
      0005D9 19               [12] 1757 	dec	r1
      0005DA 77 00            [12] 1758 	mov	@r1,#0x00
      0005DC 90 00 04         [24] 1759 	mov	dptr,#(_box + 0x0003)
      0005DF E0               [24] 1760 	movx	a,@dptr
      0005E0 FA               [12] 1761 	mov	r2,a
      0005E1 E4               [12] 1762 	clr	a
      0005E2 FB               [12] 1763 	mov	r3,a
      0005E3 FE               [12] 1764 	mov	r6,a
      0005E4 FF               [12] 1765 	mov	r7,a
      0005E5 E5 16            [12] 1766 	mov	a,_bp
      0005E7 24 05            [12] 1767 	add	a,#0x05
      0005E9 F8               [12] 1768 	mov	r0,a
      0005EA E6               [12] 1769 	mov	a,@r0
      0005EB 42 02            [12] 1770 	orl	ar2,a
      0005ED 08               [12] 1771 	inc	r0
      0005EE E6               [12] 1772 	mov	a,@r0
      0005EF 42 03            [12] 1773 	orl	ar3,a
      0005F1 08               [12] 1774 	inc	r0
      0005F2 E6               [12] 1775 	mov	a,@r0
      0005F3 42 06            [12] 1776 	orl	ar6,a
      0005F5 08               [12] 1777 	inc	r0
      0005F6 E6               [12] 1778 	mov	a,@r0
      0005F7 42 07            [12] 1779 	orl	ar7,a
      0005F9 E5 16            [12] 1780 	mov	a,_bp
      0005FB 24 09            [12] 1781 	add	a,#0x09
      0005FD F8               [12] 1782 	mov	r0,a
      0005FE A6 02            [24] 1783 	mov	@r0,ar2
      000600 08               [12] 1784 	inc	r0
      000601 A6 03            [24] 1785 	mov	@r0,ar3
      000603 08               [12] 1786 	inc	r0
      000604 A6 06            [24] 1787 	mov	@r0,ar6
      000606 08               [12] 1788 	inc	r0
      000607 A6 07            [24] 1789 	mov	@r0,ar7
      000609                       1790 00123$:
                                   1791 ;	usr/main.c:122: i = 0;
      000609 7C 00            [12] 1792 	mov	r4,#0x00
      00060B 7D 00            [12] 1793 	mov	r5,#0x00
      00060D                       1794 00125$:
                                   1795 ;	usr/main.c:124: SetPWM3Dat((unsigned char)(M >> 8*2)); //将第三个数据发送给舵机
      00060D E5 16            [12] 1796 	mov	a,_bp
      00060F 24 09            [12] 1797 	add	a,#0x09
      000611 F8               [12] 1798 	mov	r0,a
      000612 08               [12] 1799 	inc	r0
      000613 08               [12] 1800 	inc	r0
      000614 86 A3            [24] 1801 	mov	_PWM_DATA3,@r0
      000616 02 00 C0         [24] 1802 	ljmp	00127$
                                   1803 ;	usr/main.c:126: }
      000619 85 16 81         [24] 1804 	mov	sp,_bp
      00061C D0 16            [24] 1805 	pop	_bp
      00061E 22               [24] 1806 	ret
                                   1807 ;------------------------------------------------------------
                                   1808 ;Allocation info for local variables in function 'putchar'
                                   1809 ;------------------------------------------------------------
                                   1810 ;a                         Allocated to registers r6 r7 
                                   1811 ;------------------------------------------------------------
                                   1812 ;	usr/main.c:134: int putchar( int a)
                                   1813 ;	-----------------------------------------
                                   1814 ;	 function putchar
                                   1815 ;	-----------------------------------------
      00061F                       1816 _putchar:
      00061F AE 82            [24] 1817 	mov	r6,dpl
      000621 AF 83            [24] 1818 	mov	r7,dph
                                   1819 ;	usr/main.c:137: OLED_ShowChar(oled_colum,oled_row,a);
      000623 8E 05            [24] 1820 	mov	ar5,r6
      000625 90 00 13         [24] 1821 	mov	dptr,#_oled_row
      000628 E0               [24] 1822 	movx	a,@dptr
      000629 FB               [12] 1823 	mov	r3,a
      00062A A3               [24] 1824 	inc	dptr
      00062B E0               [24] 1825 	movx	a,@dptr
      00062C 90 00 11         [24] 1826 	mov	dptr,#_oled_colum
      00062F E0               [24] 1827 	movx	a,@dptr
      000630 FA               [12] 1828 	mov	r2,a
      000631 A3               [24] 1829 	inc	dptr
      000632 E0               [24] 1830 	movx	a,@dptr
      000633 C0 07            [24] 1831 	push	ar7
      000635 C0 06            [24] 1832 	push	ar6
      000637 C0 05            [24] 1833 	push	ar5
      000639 C0 03            [24] 1834 	push	ar3
      00063B 8A 82            [24] 1835 	mov	dpl,r2
      00063D 12 17 4D         [24] 1836 	lcall	_OLED_ShowChar
      000640 15 81            [12] 1837 	dec	sp
      000642 15 81            [12] 1838 	dec	sp
      000644 D0 06            [24] 1839 	pop	ar6
      000646 D0 07            [24] 1840 	pop	ar7
                                   1841 ;	usr/main.c:139: oled_colum+=6;
      000648 90 00 11         [24] 1842 	mov	dptr,#_oled_colum
      00064B E0               [24] 1843 	movx	a,@dptr
      00064C FC               [12] 1844 	mov	r4,a
      00064D A3               [24] 1845 	inc	dptr
      00064E E0               [24] 1846 	movx	a,@dptr
      00064F FD               [12] 1847 	mov	r5,a
      000650 90 00 11         [24] 1848 	mov	dptr,#_oled_colum
      000653 74 06            [12] 1849 	mov	a,#0x06
      000655 2C               [12] 1850 	add	a,r4
      000656 F0               [24] 1851 	movx	@dptr,a
      000657 E4               [12] 1852 	clr	a
      000658 3D               [12] 1853 	addc	a,r5
      000659 A3               [24] 1854 	inc	dptr
      00065A F0               [24] 1855 	movx	@dptr,a
                                   1856 ;	usr/main.c:144: if (oled_colum>122){oled_colum=0;oled_row+=1;}
      00065B 90 00 11         [24] 1857 	mov	dptr,#_oled_colum
      00065E E0               [24] 1858 	movx	a,@dptr
      00065F FC               [12] 1859 	mov	r4,a
      000660 A3               [24] 1860 	inc	dptr
      000661 E0               [24] 1861 	movx	a,@dptr
      000662 FD               [12] 1862 	mov	r5,a
      000663 C3               [12] 1863 	clr	c
      000664 74 7A            [12] 1864 	mov	a,#0x7a
      000666 9C               [12] 1865 	subb	a,r4
      000667 74 80            [12] 1866 	mov	a,#(0x00 ^ 0x80)
      000669 8D F0            [24] 1867 	mov	b,r5
      00066B 63 F0 80         [24] 1868 	xrl	b,#0x80
      00066E 95 F0            [12] 1869 	subb	a,b
      000670 50 1A            [24] 1870 	jnc	00102$
      000672 90 00 11         [24] 1871 	mov	dptr,#_oled_colum
      000675 E4               [12] 1872 	clr	a
      000676 F0               [24] 1873 	movx	@dptr,a
      000677 A3               [24] 1874 	inc	dptr
      000678 F0               [24] 1875 	movx	@dptr,a
      000679 90 00 13         [24] 1876 	mov	dptr,#_oled_row
      00067C E0               [24] 1877 	movx	a,@dptr
      00067D FC               [12] 1878 	mov	r4,a
      00067E A3               [24] 1879 	inc	dptr
      00067F E0               [24] 1880 	movx	a,@dptr
      000680 FD               [12] 1881 	mov	r5,a
      000681 90 00 13         [24] 1882 	mov	dptr,#_oled_row
      000684 74 01            [12] 1883 	mov	a,#0x01
      000686 2C               [12] 1884 	add	a,r4
      000687 F0               [24] 1885 	movx	@dptr,a
      000688 E4               [12] 1886 	clr	a
      000689 3D               [12] 1887 	addc	a,r5
      00068A A3               [24] 1888 	inc	dptr
      00068B F0               [24] 1889 	movx	@dptr,a
      00068C                       1890 00102$:
                                   1891 ;	usr/main.c:145: return(a);
      00068C 8E 82            [24] 1892 	mov	dpl,r6
      00068E 8F 83            [24] 1893 	mov	dph,r7
                                   1894 ;	usr/main.c:146: }
      000690 22               [24] 1895 	ret
                                   1896 ;------------------------------------------------------------
                                   1897 ;Allocation info for local variables in function 'setCursor'
                                   1898 ;------------------------------------------------------------
                                   1899 ;row                       Allocated to stack - _bp -4
                                   1900 ;column                    Allocated to registers 
                                   1901 ;------------------------------------------------------------
                                   1902 ;	usr/main.c:154: void setCursor(int column,int row)
                                   1903 ;	-----------------------------------------
                                   1904 ;	 function setCursor
                                   1905 ;	-----------------------------------------
      000691                       1906 _setCursor:
      000691 C0 16            [24] 1907 	push	_bp
      000693 85 81 16         [24] 1908 	mov	_bp,sp
      000696 AF 83            [24] 1909 	mov	r7,dph
      000698 E5 82            [12] 1910 	mov	a,dpl
      00069A 90 00 11         [24] 1911 	mov	dptr,#_oled_colum
      00069D F0               [24] 1912 	movx	@dptr,a
      00069E EF               [12] 1913 	mov	a,r7
      00069F A3               [24] 1914 	inc	dptr
      0006A0 F0               [24] 1915 	movx	@dptr,a
                                   1916 ;	usr/main.c:157: oled_row = row;
      0006A1 E5 16            [12] 1917 	mov	a,_bp
      0006A3 24 FC            [12] 1918 	add	a,#0xfc
      0006A5 F8               [12] 1919 	mov	r0,a
      0006A6 90 00 13         [24] 1920 	mov	dptr,#_oled_row
      0006A9 E6               [12] 1921 	mov	a,@r0
      0006AA F0               [24] 1922 	movx	@dptr,a
      0006AB 08               [12] 1923 	inc	r0
      0006AC E6               [12] 1924 	mov	a,@r0
      0006AD A3               [24] 1925 	inc	dptr
      0006AE F0               [24] 1926 	movx	@dptr,a
                                   1927 ;	usr/main.c:158: }
      0006AF D0 16            [24] 1928 	pop	_bp
      0006B1 22               [24] 1929 	ret
                                   1930 	.area CSEG    (CODE)
                                   1931 	.area CONST   (CODE)
      0035CE                       1932 _BMP1:
      0035CE 00                    1933 	.db #0x00	; 0
      0035CF 03                    1934 	.db #0x03	; 3
      0035D0 05                    1935 	.db #0x05	; 5
      0035D1 09                    1936 	.db #0x09	; 9
      0035D2 11                    1937 	.db #0x11	; 17
      0035D3 FF                    1938 	.db #0xff	; 255
      0035D4 11                    1939 	.db #0x11	; 17
      0035D5 89                    1940 	.db #0x89	; 137
      0035D6 05                    1941 	.db #0x05	; 5
      0035D7 C3                    1942 	.db #0xc3	; 195
      0035D8 00                    1943 	.db #0x00	; 0
      0035D9 E0                    1944 	.db #0xe0	; 224
      0035DA 00                    1945 	.db #0x00	; 0
      0035DB F0                    1946 	.db #0xf0	; 240
      0035DC 00                    1947 	.db #0x00	; 0
      0035DD F8                    1948 	.db #0xf8	; 248
      0035DE 00                    1949 	.db #0x00	; 0
      0035DF 00                    1950 	.db #0x00	; 0
      0035E0 00                    1951 	.db #0x00	; 0
      0035E1 00                    1952 	.db #0x00	; 0
      0035E2 00                    1953 	.db #0x00	; 0
      0035E3 00                    1954 	.db #0x00	; 0
      0035E4 00                    1955 	.db #0x00	; 0
      0035E5 44                    1956 	.db #0x44	; 68	'D'
      0035E6 28                    1957 	.db #0x28	; 40
      0035E7 FF                    1958 	.db #0xff	; 255
      0035E8 11                    1959 	.db #0x11	; 17
      0035E9 AA                    1960 	.db #0xaa	; 170
      0035EA 44                    1961 	.db #0x44	; 68	'D'
      0035EB 00                    1962 	.db #0x00	; 0
      0035EC 00                    1963 	.db #0x00	; 0
      0035ED 00                    1964 	.db #0x00	; 0
      0035EE 00                    1965 	.db #0x00	; 0
      0035EF 00                    1966 	.db #0x00	; 0
      0035F0 00                    1967 	.db #0x00	; 0
      0035F1 00                    1968 	.db #0x00	; 0
      0035F2 00                    1969 	.db #0x00	; 0
      0035F3 00                    1970 	.db #0x00	; 0
      0035F4 00                    1971 	.db #0x00	; 0
      0035F5 00                    1972 	.db #0x00	; 0
      0035F6 00                    1973 	.db #0x00	; 0
      0035F7 00                    1974 	.db #0x00	; 0
      0035F8 00                    1975 	.db #0x00	; 0
      0035F9 00                    1976 	.db #0x00	; 0
      0035FA 00                    1977 	.db #0x00	; 0
      0035FB 00                    1978 	.db #0x00	; 0
      0035FC 00                    1979 	.db #0x00	; 0
      0035FD 00                    1980 	.db #0x00	; 0
      0035FE 00                    1981 	.db #0x00	; 0
      0035FF 00                    1982 	.db #0x00	; 0
      003600 00                    1983 	.db #0x00	; 0
      003601 00                    1984 	.db #0x00	; 0
      003602 00                    1985 	.db #0x00	; 0
      003603 00                    1986 	.db #0x00	; 0
      003604 00                    1987 	.db #0x00	; 0
      003605 00                    1988 	.db #0x00	; 0
      003606 00                    1989 	.db #0x00	; 0
      003607 00                    1990 	.db #0x00	; 0
      003608 00                    1991 	.db #0x00	; 0
      003609 00                    1992 	.db #0x00	; 0
      00360A 00                    1993 	.db #0x00	; 0
      00360B 00                    1994 	.db #0x00	; 0
      00360C 00                    1995 	.db #0x00	; 0
      00360D 00                    1996 	.db #0x00	; 0
      00360E 00                    1997 	.db #0x00	; 0
      00360F 00                    1998 	.db #0x00	; 0
      003610 00                    1999 	.db #0x00	; 0
      003611 00                    2000 	.db #0x00	; 0
      003612 00                    2001 	.db #0x00	; 0
      003613 00                    2002 	.db #0x00	; 0
      003614 00                    2003 	.db #0x00	; 0
      003615 00                    2004 	.db #0x00	; 0
      003616 00                    2005 	.db #0x00	; 0
      003617 00                    2006 	.db #0x00	; 0
      003618 00                    2007 	.db #0x00	; 0
      003619 00                    2008 	.db #0x00	; 0
      00361A 00                    2009 	.db #0x00	; 0
      00361B 00                    2010 	.db #0x00	; 0
      00361C 00                    2011 	.db #0x00	; 0
      00361D 00                    2012 	.db #0x00	; 0
      00361E 00                    2013 	.db #0x00	; 0
      00361F 00                    2014 	.db #0x00	; 0
      003620 00                    2015 	.db #0x00	; 0
      003621 00                    2016 	.db #0x00	; 0
      003622 00                    2017 	.db #0x00	; 0
      003623 00                    2018 	.db #0x00	; 0
      003624 00                    2019 	.db #0x00	; 0
      003625 00                    2020 	.db #0x00	; 0
      003626 00                    2021 	.db #0x00	; 0
      003627 00                    2022 	.db #0x00	; 0
      003628 83                    2023 	.db #0x83	; 131
      003629 01                    2024 	.db #0x01	; 1
      00362A 38                    2025 	.db #0x38	; 56	'8'
      00362B 44                    2026 	.db #0x44	; 68	'D'
      00362C 82                    2027 	.db #0x82	; 130
      00362D 92                    2028 	.db #0x92	; 146
      00362E 92                    2029 	.db #0x92	; 146
      00362F 74                    2030 	.db #0x74	; 116	't'
      003630 01                    2031 	.db #0x01	; 1
      003631 83                    2032 	.db #0x83	; 131
      003632 00                    2033 	.db #0x00	; 0
      003633 00                    2034 	.db #0x00	; 0
      003634 00                    2035 	.db #0x00	; 0
      003635 00                    2036 	.db #0x00	; 0
      003636 00                    2037 	.db #0x00	; 0
      003637 00                    2038 	.db #0x00	; 0
      003638 00                    2039 	.db #0x00	; 0
      003639 7C                    2040 	.db #0x7c	; 124
      00363A 44                    2041 	.db #0x44	; 68	'D'
      00363B FF                    2042 	.db #0xff	; 255
      00363C 01                    2043 	.db #0x01	; 1
      00363D 7D                    2044 	.db #0x7d	; 125
      00363E 7D                    2045 	.db #0x7d	; 125
      00363F 7D                    2046 	.db #0x7d	; 125
      003640 01                    2047 	.db #0x01	; 1
      003641 7D                    2048 	.db #0x7d	; 125
      003642 7D                    2049 	.db #0x7d	; 125
      003643 7D                    2050 	.db #0x7d	; 125
      003644 7D                    2051 	.db #0x7d	; 125
      003645 01                    2052 	.db #0x01	; 1
      003646 7D                    2053 	.db #0x7d	; 125
      003647 7D                    2054 	.db #0x7d	; 125
      003648 7D                    2055 	.db #0x7d	; 125
      003649 7D                    2056 	.db #0x7d	; 125
      00364A 7D                    2057 	.db #0x7d	; 125
      00364B 01                    2058 	.db #0x01	; 1
      00364C FF                    2059 	.db #0xff	; 255
      00364D 00                    2060 	.db #0x00	; 0
      00364E 00                    2061 	.db #0x00	; 0
      00364F 00                    2062 	.db #0x00	; 0
      003650 00                    2063 	.db #0x00	; 0
      003651 00                    2064 	.db #0x00	; 0
      003652 00                    2065 	.db #0x00	; 0
      003653 01                    2066 	.db #0x01	; 1
      003654 00                    2067 	.db #0x00	; 0
      003655 01                    2068 	.db #0x01	; 1
      003656 00                    2069 	.db #0x00	; 0
      003657 01                    2070 	.db #0x01	; 1
      003658 00                    2071 	.db #0x00	; 0
      003659 01                    2072 	.db #0x01	; 1
      00365A 00                    2073 	.db #0x00	; 0
      00365B 01                    2074 	.db #0x01	; 1
      00365C 00                    2075 	.db #0x00	; 0
      00365D 01                    2076 	.db #0x01	; 1
      00365E 00                    2077 	.db #0x00	; 0
      00365F 00                    2078 	.db #0x00	; 0
      003660 00                    2079 	.db #0x00	; 0
      003661 00                    2080 	.db #0x00	; 0
      003662 00                    2081 	.db #0x00	; 0
      003663 00                    2082 	.db #0x00	; 0
      003664 00                    2083 	.db #0x00	; 0
      003665 00                    2084 	.db #0x00	; 0
      003666 00                    2085 	.db #0x00	; 0
      003667 01                    2086 	.db #0x01	; 1
      003668 01                    2087 	.db #0x01	; 1
      003669 00                    2088 	.db #0x00	; 0
      00366A 00                    2089 	.db #0x00	; 0
      00366B 00                    2090 	.db #0x00	; 0
      00366C 00                    2091 	.db #0x00	; 0
      00366D 00                    2092 	.db #0x00	; 0
      00366E 00                    2093 	.db #0x00	; 0
      00366F 00                    2094 	.db #0x00	; 0
      003670 00                    2095 	.db #0x00	; 0
      003671 00                    2096 	.db #0x00	; 0
      003672 00                    2097 	.db #0x00	; 0
      003673 00                    2098 	.db #0x00	; 0
      003674 00                    2099 	.db #0x00	; 0
      003675 00                    2100 	.db #0x00	; 0
      003676 00                    2101 	.db #0x00	; 0
      003677 00                    2102 	.db #0x00	; 0
      003678 00                    2103 	.db #0x00	; 0
      003679 00                    2104 	.db #0x00	; 0
      00367A 00                    2105 	.db #0x00	; 0
      00367B 00                    2106 	.db #0x00	; 0
      00367C 00                    2107 	.db #0x00	; 0
      00367D 00                    2108 	.db #0x00	; 0
      00367E 00                    2109 	.db #0x00	; 0
      00367F 00                    2110 	.db #0x00	; 0
      003680 00                    2111 	.db #0x00	; 0
      003681 00                    2112 	.db #0x00	; 0
      003682 00                    2113 	.db #0x00	; 0
      003683 00                    2114 	.db #0x00	; 0
      003684 00                    2115 	.db #0x00	; 0
      003685 00                    2116 	.db #0x00	; 0
      003686 00                    2117 	.db #0x00	; 0
      003687 00                    2118 	.db #0x00	; 0
      003688 00                    2119 	.db #0x00	; 0
      003689 00                    2120 	.db #0x00	; 0
      00368A 00                    2121 	.db #0x00	; 0
      00368B 00                    2122 	.db #0x00	; 0
      00368C 00                    2123 	.db #0x00	; 0
      00368D 00                    2124 	.db #0x00	; 0
      00368E 00                    2125 	.db #0x00	; 0
      00368F 00                    2126 	.db #0x00	; 0
      003690 00                    2127 	.db #0x00	; 0
      003691 00                    2128 	.db #0x00	; 0
      003692 00                    2129 	.db #0x00	; 0
      003693 00                    2130 	.db #0x00	; 0
      003694 00                    2131 	.db #0x00	; 0
      003695 00                    2132 	.db #0x00	; 0
      003696 00                    2133 	.db #0x00	; 0
      003697 00                    2134 	.db #0x00	; 0
      003698 00                    2135 	.db #0x00	; 0
      003699 00                    2136 	.db #0x00	; 0
      00369A 00                    2137 	.db #0x00	; 0
      00369B 00                    2138 	.db #0x00	; 0
      00369C 00                    2139 	.db #0x00	; 0
      00369D 00                    2140 	.db #0x00	; 0
      00369E 00                    2141 	.db #0x00	; 0
      00369F 00                    2142 	.db #0x00	; 0
      0036A0 00                    2143 	.db #0x00	; 0
      0036A1 00                    2144 	.db #0x00	; 0
      0036A2 00                    2145 	.db #0x00	; 0
      0036A3 00                    2146 	.db #0x00	; 0
      0036A4 00                    2147 	.db #0x00	; 0
      0036A5 00                    2148 	.db #0x00	; 0
      0036A6 00                    2149 	.db #0x00	; 0
      0036A7 00                    2150 	.db #0x00	; 0
      0036A8 01                    2151 	.db #0x01	; 1
      0036A9 01                    2152 	.db #0x01	; 1
      0036AA 00                    2153 	.db #0x00	; 0
      0036AB 00                    2154 	.db #0x00	; 0
      0036AC 00                    2155 	.db #0x00	; 0
      0036AD 00                    2156 	.db #0x00	; 0
      0036AE 00                    2157 	.db #0x00	; 0
      0036AF 00                    2158 	.db #0x00	; 0
      0036B0 01                    2159 	.db #0x01	; 1
      0036B1 01                    2160 	.db #0x01	; 1
      0036B2 00                    2161 	.db #0x00	; 0
      0036B3 00                    2162 	.db #0x00	; 0
      0036B4 00                    2163 	.db #0x00	; 0
      0036B5 00                    2164 	.db #0x00	; 0
      0036B6 00                    2165 	.db #0x00	; 0
      0036B7 00                    2166 	.db #0x00	; 0
      0036B8 00                    2167 	.db #0x00	; 0
      0036B9 00                    2168 	.db #0x00	; 0
      0036BA 00                    2169 	.db #0x00	; 0
      0036BB 01                    2170 	.db #0x01	; 1
      0036BC 01                    2171 	.db #0x01	; 1
      0036BD 01                    2172 	.db #0x01	; 1
      0036BE 01                    2173 	.db #0x01	; 1
      0036BF 01                    2174 	.db #0x01	; 1
      0036C0 01                    2175 	.db #0x01	; 1
      0036C1 01                    2176 	.db #0x01	; 1
      0036C2 01                    2177 	.db #0x01	; 1
      0036C3 01                    2178 	.db #0x01	; 1
      0036C4 01                    2179 	.db #0x01	; 1
      0036C5 01                    2180 	.db #0x01	; 1
      0036C6 01                    2181 	.db #0x01	; 1
      0036C7 01                    2182 	.db #0x01	; 1
      0036C8 01                    2183 	.db #0x01	; 1
      0036C9 01                    2184 	.db #0x01	; 1
      0036CA 01                    2185 	.db #0x01	; 1
      0036CB 01                    2186 	.db #0x01	; 1
      0036CC 01                    2187 	.db #0x01	; 1
      0036CD 00                    2188 	.db #0x00	; 0
      0036CE 00                    2189 	.db #0x00	; 0
      0036CF 00                    2190 	.db #0x00	; 0
      0036D0 00                    2191 	.db #0x00	; 0
      0036D1 00                    2192 	.db #0x00	; 0
      0036D2 00                    2193 	.db #0x00	; 0
      0036D3 00                    2194 	.db #0x00	; 0
      0036D4 00                    2195 	.db #0x00	; 0
      0036D5 00                    2196 	.db #0x00	; 0
      0036D6 00                    2197 	.db #0x00	; 0
      0036D7 00                    2198 	.db #0x00	; 0
      0036D8 00                    2199 	.db #0x00	; 0
      0036D9 00                    2200 	.db #0x00	; 0
      0036DA 00                    2201 	.db #0x00	; 0
      0036DB 00                    2202 	.db #0x00	; 0
      0036DC 00                    2203 	.db #0x00	; 0
      0036DD 00                    2204 	.db #0x00	; 0
      0036DE 00                    2205 	.db #0x00	; 0
      0036DF 00                    2206 	.db #0x00	; 0
      0036E0 00                    2207 	.db #0x00	; 0
      0036E1 00                    2208 	.db #0x00	; 0
      0036E2 00                    2209 	.db #0x00	; 0
      0036E3 00                    2210 	.db #0x00	; 0
      0036E4 00                    2211 	.db #0x00	; 0
      0036E5 00                    2212 	.db #0x00	; 0
      0036E6 00                    2213 	.db #0x00	; 0
      0036E7 00                    2214 	.db #0x00	; 0
      0036E8 00                    2215 	.db #0x00	; 0
      0036E9 00                    2216 	.db #0x00	; 0
      0036EA 3F                    2217 	.db #0x3f	; 63
      0036EB 3F                    2218 	.db #0x3f	; 63
      0036EC 03                    2219 	.db #0x03	; 3
      0036ED 03                    2220 	.db #0x03	; 3
      0036EE F3                    2221 	.db #0xf3	; 243
      0036EF 13                    2222 	.db #0x13	; 19
      0036F0 11                    2223 	.db #0x11	; 17
      0036F1 11                    2224 	.db #0x11	; 17
      0036F2 11                    2225 	.db #0x11	; 17
      0036F3 11                    2226 	.db #0x11	; 17
      0036F4 11                    2227 	.db #0x11	; 17
      0036F5 11                    2228 	.db #0x11	; 17
      0036F6 01                    2229 	.db #0x01	; 1
      0036F7 F1                    2230 	.db #0xf1	; 241
      0036F8 11                    2231 	.db #0x11	; 17
      0036F9 61                    2232 	.db #0x61	; 97	'a'
      0036FA 81                    2233 	.db #0x81	; 129
      0036FB 01                    2234 	.db #0x01	; 1
      0036FC 01                    2235 	.db #0x01	; 1
      0036FD 01                    2236 	.db #0x01	; 1
      0036FE 81                    2237 	.db #0x81	; 129
      0036FF 61                    2238 	.db #0x61	; 97	'a'
      003700 11                    2239 	.db #0x11	; 17
      003701 F1                    2240 	.db #0xf1	; 241
      003702 01                    2241 	.db #0x01	; 1
      003703 01                    2242 	.db #0x01	; 1
      003704 01                    2243 	.db #0x01	; 1
      003705 01                    2244 	.db #0x01	; 1
      003706 41                    2245 	.db #0x41	; 65	'A'
      003707 41                    2246 	.db #0x41	; 65	'A'
      003708 F1                    2247 	.db #0xf1	; 241
      003709 01                    2248 	.db #0x01	; 1
      00370A 01                    2249 	.db #0x01	; 1
      00370B 01                    2250 	.db #0x01	; 1
      00370C 01                    2251 	.db #0x01	; 1
      00370D 01                    2252 	.db #0x01	; 1
      00370E C1                    2253 	.db #0xc1	; 193
      00370F 21                    2254 	.db #0x21	; 33
      003710 11                    2255 	.db #0x11	; 17
      003711 11                    2256 	.db #0x11	; 17
      003712 11                    2257 	.db #0x11	; 17
      003713 11                    2258 	.db #0x11	; 17
      003714 21                    2259 	.db #0x21	; 33
      003715 C1                    2260 	.db #0xc1	; 193
      003716 01                    2261 	.db #0x01	; 1
      003717 01                    2262 	.db #0x01	; 1
      003718 01                    2263 	.db #0x01	; 1
      003719 01                    2264 	.db #0x01	; 1
      00371A 41                    2265 	.db #0x41	; 65	'A'
      00371B 41                    2266 	.db #0x41	; 65	'A'
      00371C F1                    2267 	.db #0xf1	; 241
      00371D 01                    2268 	.db #0x01	; 1
      00371E 01                    2269 	.db #0x01	; 1
      00371F 01                    2270 	.db #0x01	; 1
      003720 01                    2271 	.db #0x01	; 1
      003721 01                    2272 	.db #0x01	; 1
      003722 01                    2273 	.db #0x01	; 1
      003723 01                    2274 	.db #0x01	; 1
      003724 01                    2275 	.db #0x01	; 1
      003725 01                    2276 	.db #0x01	; 1
      003726 01                    2277 	.db #0x01	; 1
      003727 11                    2278 	.db #0x11	; 17
      003728 11                    2279 	.db #0x11	; 17
      003729 11                    2280 	.db #0x11	; 17
      00372A 11                    2281 	.db #0x11	; 17
      00372B 11                    2282 	.db #0x11	; 17
      00372C D3                    2283 	.db #0xd3	; 211
      00372D 33                    2284 	.db #0x33	; 51	'3'
      00372E 03                    2285 	.db #0x03	; 3
      00372F 03                    2286 	.db #0x03	; 3
      003730 3F                    2287 	.db #0x3f	; 63
      003731 3F                    2288 	.db #0x3f	; 63
      003732 00                    2289 	.db #0x00	; 0
      003733 00                    2290 	.db #0x00	; 0
      003734 00                    2291 	.db #0x00	; 0
      003735 00                    2292 	.db #0x00	; 0
      003736 00                    2293 	.db #0x00	; 0
      003737 00                    2294 	.db #0x00	; 0
      003738 00                    2295 	.db #0x00	; 0
      003739 00                    2296 	.db #0x00	; 0
      00373A 00                    2297 	.db #0x00	; 0
      00373B 00                    2298 	.db #0x00	; 0
      00373C 00                    2299 	.db #0x00	; 0
      00373D 00                    2300 	.db #0x00	; 0
      00373E 00                    2301 	.db #0x00	; 0
      00373F 00                    2302 	.db #0x00	; 0
      003740 00                    2303 	.db #0x00	; 0
      003741 00                    2304 	.db #0x00	; 0
      003742 00                    2305 	.db #0x00	; 0
      003743 00                    2306 	.db #0x00	; 0
      003744 00                    2307 	.db #0x00	; 0
      003745 00                    2308 	.db #0x00	; 0
      003746 00                    2309 	.db #0x00	; 0
      003747 00                    2310 	.db #0x00	; 0
      003748 00                    2311 	.db #0x00	; 0
      003749 00                    2312 	.db #0x00	; 0
      00374A 00                    2313 	.db #0x00	; 0
      00374B 00                    2314 	.db #0x00	; 0
      00374C 00                    2315 	.db #0x00	; 0
      00374D 00                    2316 	.db #0x00	; 0
      00374E 00                    2317 	.db #0x00	; 0
      00374F 00                    2318 	.db #0x00	; 0
      003750 00                    2319 	.db #0x00	; 0
      003751 00                    2320 	.db #0x00	; 0
      003752 00                    2321 	.db #0x00	; 0
      003753 00                    2322 	.db #0x00	; 0
      003754 00                    2323 	.db #0x00	; 0
      003755 00                    2324 	.db #0x00	; 0
      003756 00                    2325 	.db #0x00	; 0
      003757 00                    2326 	.db #0x00	; 0
      003758 00                    2327 	.db #0x00	; 0
      003759 00                    2328 	.db #0x00	; 0
      00375A 00                    2329 	.db #0x00	; 0
      00375B 00                    2330 	.db #0x00	; 0
      00375C 00                    2331 	.db #0x00	; 0
      00375D 00                    2332 	.db #0x00	; 0
      00375E 00                    2333 	.db #0x00	; 0
      00375F 00                    2334 	.db #0x00	; 0
      003760 00                    2335 	.db #0x00	; 0
      003761 00                    2336 	.db #0x00	; 0
      003762 00                    2337 	.db #0x00	; 0
      003763 00                    2338 	.db #0x00	; 0
      003764 00                    2339 	.db #0x00	; 0
      003765 00                    2340 	.db #0x00	; 0
      003766 00                    2341 	.db #0x00	; 0
      003767 00                    2342 	.db #0x00	; 0
      003768 00                    2343 	.db #0x00	; 0
      003769 00                    2344 	.db #0x00	; 0
      00376A E0                    2345 	.db #0xe0	; 224
      00376B E0                    2346 	.db #0xe0	; 224
      00376C 00                    2347 	.db #0x00	; 0
      00376D 00                    2348 	.db #0x00	; 0
      00376E 7F                    2349 	.db #0x7f	; 127
      00376F 01                    2350 	.db #0x01	; 1
      003770 01                    2351 	.db #0x01	; 1
      003771 01                    2352 	.db #0x01	; 1
      003772 01                    2353 	.db #0x01	; 1
      003773 01                    2354 	.db #0x01	; 1
      003774 01                    2355 	.db #0x01	; 1
      003775 00                    2356 	.db #0x00	; 0
      003776 00                    2357 	.db #0x00	; 0
      003777 7F                    2358 	.db #0x7f	; 127
      003778 00                    2359 	.db #0x00	; 0
      003779 00                    2360 	.db #0x00	; 0
      00377A 01                    2361 	.db #0x01	; 1
      00377B 06                    2362 	.db #0x06	; 6
      00377C 18                    2363 	.db #0x18	; 24
      00377D 06                    2364 	.db #0x06	; 6
      00377E 01                    2365 	.db #0x01	; 1
      00377F 00                    2366 	.db #0x00	; 0
      003780 00                    2367 	.db #0x00	; 0
      003781 7F                    2368 	.db #0x7f	; 127
      003782 00                    2369 	.db #0x00	; 0
      003783 00                    2370 	.db #0x00	; 0
      003784 00                    2371 	.db #0x00	; 0
      003785 00                    2372 	.db #0x00	; 0
      003786 40                    2373 	.db #0x40	; 64
      003787 40                    2374 	.db #0x40	; 64
      003788 7F                    2375 	.db #0x7f	; 127
      003789 40                    2376 	.db #0x40	; 64
      00378A 40                    2377 	.db #0x40	; 64
      00378B 00                    2378 	.db #0x00	; 0
      00378C 00                    2379 	.db #0x00	; 0
      00378D 00                    2380 	.db #0x00	; 0
      00378E 1F                    2381 	.db #0x1f	; 31
      00378F 20                    2382 	.db #0x20	; 32
      003790 40                    2383 	.db #0x40	; 64
      003791 40                    2384 	.db #0x40	; 64
      003792 40                    2385 	.db #0x40	; 64
      003793 40                    2386 	.db #0x40	; 64
      003794 20                    2387 	.db #0x20	; 32
      003795 1F                    2388 	.db #0x1f	; 31
      003796 00                    2389 	.db #0x00	; 0
      003797 00                    2390 	.db #0x00	; 0
      003798 00                    2391 	.db #0x00	; 0
      003799 00                    2392 	.db #0x00	; 0
      00379A 40                    2393 	.db #0x40	; 64
      00379B 40                    2394 	.db #0x40	; 64
      00379C 7F                    2395 	.db #0x7f	; 127
      00379D 40                    2396 	.db #0x40	; 64
      00379E 40                    2397 	.db #0x40	; 64
      00379F 00                    2398 	.db #0x00	; 0
      0037A0 00                    2399 	.db #0x00	; 0
      0037A1 00                    2400 	.db #0x00	; 0
      0037A2 00                    2401 	.db #0x00	; 0
      0037A3 60                    2402 	.db #0x60	; 96
      0037A4 00                    2403 	.db #0x00	; 0
      0037A5 00                    2404 	.db #0x00	; 0
      0037A6 00                    2405 	.db #0x00	; 0
      0037A7 00                    2406 	.db #0x00	; 0
      0037A8 40                    2407 	.db #0x40	; 64
      0037A9 30                    2408 	.db #0x30	; 48	'0'
      0037AA 0C                    2409 	.db #0x0c	; 12
      0037AB 03                    2410 	.db #0x03	; 3
      0037AC 00                    2411 	.db #0x00	; 0
      0037AD 00                    2412 	.db #0x00	; 0
      0037AE 00                    2413 	.db #0x00	; 0
      0037AF 00                    2414 	.db #0x00	; 0
      0037B0 E0                    2415 	.db #0xe0	; 224
      0037B1 E0                    2416 	.db #0xe0	; 224
      0037B2 00                    2417 	.db #0x00	; 0
      0037B3 00                    2418 	.db #0x00	; 0
      0037B4 00                    2419 	.db #0x00	; 0
      0037B5 00                    2420 	.db #0x00	; 0
      0037B6 00                    2421 	.db #0x00	; 0
      0037B7 00                    2422 	.db #0x00	; 0
      0037B8 00                    2423 	.db #0x00	; 0
      0037B9 00                    2424 	.db #0x00	; 0
      0037BA 00                    2425 	.db #0x00	; 0
      0037BB 00                    2426 	.db #0x00	; 0
      0037BC 00                    2427 	.db #0x00	; 0
      0037BD 00                    2428 	.db #0x00	; 0
      0037BE 00                    2429 	.db #0x00	; 0
      0037BF 00                    2430 	.db #0x00	; 0
      0037C0 00                    2431 	.db #0x00	; 0
      0037C1 00                    2432 	.db #0x00	; 0
      0037C2 00                    2433 	.db #0x00	; 0
      0037C3 00                    2434 	.db #0x00	; 0
      0037C4 00                    2435 	.db #0x00	; 0
      0037C5 00                    2436 	.db #0x00	; 0
      0037C6 00                    2437 	.db #0x00	; 0
      0037C7 00                    2438 	.db #0x00	; 0
      0037C8 00                    2439 	.db #0x00	; 0
      0037C9 00                    2440 	.db #0x00	; 0
      0037CA 00                    2441 	.db #0x00	; 0
      0037CB 00                    2442 	.db #0x00	; 0
      0037CC 00                    2443 	.db #0x00	; 0
      0037CD 00                    2444 	.db #0x00	; 0
      0037CE 00                    2445 	.db #0x00	; 0
      0037CF 00                    2446 	.db #0x00	; 0
      0037D0 00                    2447 	.db #0x00	; 0
      0037D1 00                    2448 	.db #0x00	; 0
      0037D2 00                    2449 	.db #0x00	; 0
      0037D3 00                    2450 	.db #0x00	; 0
      0037D4 00                    2451 	.db #0x00	; 0
      0037D5 00                    2452 	.db #0x00	; 0
      0037D6 00                    2453 	.db #0x00	; 0
      0037D7 00                    2454 	.db #0x00	; 0
      0037D8 00                    2455 	.db #0x00	; 0
      0037D9 00                    2456 	.db #0x00	; 0
      0037DA 00                    2457 	.db #0x00	; 0
      0037DB 00                    2458 	.db #0x00	; 0
      0037DC 00                    2459 	.db #0x00	; 0
      0037DD 00                    2460 	.db #0x00	; 0
      0037DE 00                    2461 	.db #0x00	; 0
      0037DF 00                    2462 	.db #0x00	; 0
      0037E0 00                    2463 	.db #0x00	; 0
      0037E1 00                    2464 	.db #0x00	; 0
      0037E2 00                    2465 	.db #0x00	; 0
      0037E3 00                    2466 	.db #0x00	; 0
      0037E4 00                    2467 	.db #0x00	; 0
      0037E5 00                    2468 	.db #0x00	; 0
      0037E6 00                    2469 	.db #0x00	; 0
      0037E7 00                    2470 	.db #0x00	; 0
      0037E8 00                    2471 	.db #0x00	; 0
      0037E9 00                    2472 	.db #0x00	; 0
      0037EA 07                    2473 	.db #0x07	; 7
      0037EB 07                    2474 	.db #0x07	; 7
      0037EC 06                    2475 	.db #0x06	; 6
      0037ED 06                    2476 	.db #0x06	; 6
      0037EE 06                    2477 	.db #0x06	; 6
      0037EF 06                    2478 	.db #0x06	; 6
      0037F0 04                    2479 	.db #0x04	; 4
      0037F1 04                    2480 	.db #0x04	; 4
      0037F2 04                    2481 	.db #0x04	; 4
      0037F3 84                    2482 	.db #0x84	; 132
      0037F4 44                    2483 	.db #0x44	; 68	'D'
      0037F5 44                    2484 	.db #0x44	; 68	'D'
      0037F6 44                    2485 	.db #0x44	; 68	'D'
      0037F7 84                    2486 	.db #0x84	; 132
      0037F8 04                    2487 	.db #0x04	; 4
      0037F9 04                    2488 	.db #0x04	; 4
      0037FA 84                    2489 	.db #0x84	; 132
      0037FB 44                    2490 	.db #0x44	; 68	'D'
      0037FC 44                    2491 	.db #0x44	; 68	'D'
      0037FD 44                    2492 	.db #0x44	; 68	'D'
      0037FE 84                    2493 	.db #0x84	; 132
      0037FF 04                    2494 	.db #0x04	; 4
      003800 04                    2495 	.db #0x04	; 4
      003801 04                    2496 	.db #0x04	; 4
      003802 84                    2497 	.db #0x84	; 132
      003803 C4                    2498 	.db #0xc4	; 196
      003804 04                    2499 	.db #0x04	; 4
      003805 04                    2500 	.db #0x04	; 4
      003806 04                    2501 	.db #0x04	; 4
      003807 04                    2502 	.db #0x04	; 4
      003808 84                    2503 	.db #0x84	; 132
      003809 44                    2504 	.db #0x44	; 68	'D'
      00380A 44                    2505 	.db #0x44	; 68	'D'
      00380B 44                    2506 	.db #0x44	; 68	'D'
      00380C 84                    2507 	.db #0x84	; 132
      00380D 04                    2508 	.db #0x04	; 4
      00380E 04                    2509 	.db #0x04	; 4
      00380F 04                    2510 	.db #0x04	; 4
      003810 04                    2511 	.db #0x04	; 4
      003811 04                    2512 	.db #0x04	; 4
      003812 84                    2513 	.db #0x84	; 132
      003813 44                    2514 	.db #0x44	; 68	'D'
      003814 44                    2515 	.db #0x44	; 68	'D'
      003815 44                    2516 	.db #0x44	; 68	'D'
      003816 84                    2517 	.db #0x84	; 132
      003817 04                    2518 	.db #0x04	; 4
      003818 04                    2519 	.db #0x04	; 4
      003819 04                    2520 	.db #0x04	; 4
      00381A 04                    2521 	.db #0x04	; 4
      00381B 04                    2522 	.db #0x04	; 4
      00381C 84                    2523 	.db #0x84	; 132
      00381D 44                    2524 	.db #0x44	; 68	'D'
      00381E 44                    2525 	.db #0x44	; 68	'D'
      00381F 44                    2526 	.db #0x44	; 68	'D'
      003820 84                    2527 	.db #0x84	; 132
      003821 04                    2528 	.db #0x04	; 4
      003822 04                    2529 	.db #0x04	; 4
      003823 84                    2530 	.db #0x84	; 132
      003824 44                    2531 	.db #0x44	; 68	'D'
      003825 44                    2532 	.db #0x44	; 68	'D'
      003826 44                    2533 	.db #0x44	; 68	'D'
      003827 84                    2534 	.db #0x84	; 132
      003828 04                    2535 	.db #0x04	; 4
      003829 04                    2536 	.db #0x04	; 4
      00382A 04                    2537 	.db #0x04	; 4
      00382B 04                    2538 	.db #0x04	; 4
      00382C 06                    2539 	.db #0x06	; 6
      00382D 06                    2540 	.db #0x06	; 6
      00382E 06                    2541 	.db #0x06	; 6
      00382F 06                    2542 	.db #0x06	; 6
      003830 07                    2543 	.db #0x07	; 7
      003831 07                    2544 	.db #0x07	; 7
      003832 00                    2545 	.db #0x00	; 0
      003833 00                    2546 	.db #0x00	; 0
      003834 00                    2547 	.db #0x00	; 0
      003835 00                    2548 	.db #0x00	; 0
      003836 00                    2549 	.db #0x00	; 0
      003837 00                    2550 	.db #0x00	; 0
      003838 00                    2551 	.db #0x00	; 0
      003839 00                    2552 	.db #0x00	; 0
      00383A 00                    2553 	.db #0x00	; 0
      00383B 00                    2554 	.db #0x00	; 0
      00383C 00                    2555 	.db #0x00	; 0
      00383D 00                    2556 	.db #0x00	; 0
      00383E 00                    2557 	.db #0x00	; 0
      00383F 00                    2558 	.db #0x00	; 0
      003840 00                    2559 	.db #0x00	; 0
      003841 00                    2560 	.db #0x00	; 0
      003842 00                    2561 	.db #0x00	; 0
      003843 00                    2562 	.db #0x00	; 0
      003844 00                    2563 	.db #0x00	; 0
      003845 00                    2564 	.db #0x00	; 0
      003846 00                    2565 	.db #0x00	; 0
      003847 00                    2566 	.db #0x00	; 0
      003848 00                    2567 	.db #0x00	; 0
      003849 00                    2568 	.db #0x00	; 0
      00384A 00                    2569 	.db #0x00	; 0
      00384B 00                    2570 	.db #0x00	; 0
      00384C 00                    2571 	.db #0x00	; 0
      00384D 00                    2572 	.db #0x00	; 0
      00384E 00                    2573 	.db #0x00	; 0
      00384F 00                    2574 	.db #0x00	; 0
      003850 00                    2575 	.db #0x00	; 0
      003851 00                    2576 	.db #0x00	; 0
      003852 00                    2577 	.db #0x00	; 0
      003853 00                    2578 	.db #0x00	; 0
      003854 00                    2579 	.db #0x00	; 0
      003855 00                    2580 	.db #0x00	; 0
      003856 00                    2581 	.db #0x00	; 0
      003857 00                    2582 	.db #0x00	; 0
      003858 00                    2583 	.db #0x00	; 0
      003859 00                    2584 	.db #0x00	; 0
      00385A 00                    2585 	.db #0x00	; 0
      00385B 00                    2586 	.db #0x00	; 0
      00385C 00                    2587 	.db #0x00	; 0
      00385D 00                    2588 	.db #0x00	; 0
      00385E 00                    2589 	.db #0x00	; 0
      00385F 00                    2590 	.db #0x00	; 0
      003860 00                    2591 	.db #0x00	; 0
      003861 00                    2592 	.db #0x00	; 0
      003862 00                    2593 	.db #0x00	; 0
      003863 00                    2594 	.db #0x00	; 0
      003864 00                    2595 	.db #0x00	; 0
      003865 00                    2596 	.db #0x00	; 0
      003866 00                    2597 	.db #0x00	; 0
      003867 00                    2598 	.db #0x00	; 0
      003868 00                    2599 	.db #0x00	; 0
      003869 00                    2600 	.db #0x00	; 0
      00386A 00                    2601 	.db #0x00	; 0
      00386B 00                    2602 	.db #0x00	; 0
      00386C 00                    2603 	.db #0x00	; 0
      00386D 00                    2604 	.db #0x00	; 0
      00386E 00                    2605 	.db #0x00	; 0
      00386F 00                    2606 	.db #0x00	; 0
      003870 00                    2607 	.db #0x00	; 0
      003871 00                    2608 	.db #0x00	; 0
      003872 00                    2609 	.db #0x00	; 0
      003873 10                    2610 	.db #0x10	; 16
      003874 18                    2611 	.db #0x18	; 24
      003875 14                    2612 	.db #0x14	; 20
      003876 12                    2613 	.db #0x12	; 18
      003877 11                    2614 	.db #0x11	; 17
      003878 00                    2615 	.db #0x00	; 0
      003879 00                    2616 	.db #0x00	; 0
      00387A 0F                    2617 	.db #0x0f	; 15
      00387B 10                    2618 	.db #0x10	; 16
      00387C 10                    2619 	.db #0x10	; 16
      00387D 10                    2620 	.db #0x10	; 16
      00387E 0F                    2621 	.db #0x0f	; 15
      00387F 00                    2622 	.db #0x00	; 0
      003880 00                    2623 	.db #0x00	; 0
      003881 00                    2624 	.db #0x00	; 0
      003882 10                    2625 	.db #0x10	; 16
      003883 1F                    2626 	.db #0x1f	; 31
      003884 10                    2627 	.db #0x10	; 16
      003885 00                    2628 	.db #0x00	; 0
      003886 00                    2629 	.db #0x00	; 0
      003887 00                    2630 	.db #0x00	; 0
      003888 08                    2631 	.db #0x08	; 8
      003889 10                    2632 	.db #0x10	; 16
      00388A 12                    2633 	.db #0x12	; 18
      00388B 12                    2634 	.db #0x12	; 18
      00388C 0D                    2635 	.db #0x0d	; 13
      00388D 00                    2636 	.db #0x00	; 0
      00388E 00                    2637 	.db #0x00	; 0
      00388F 18                    2638 	.db #0x18	; 24
      003890 00                    2639 	.db #0x00	; 0
      003891 00                    2640 	.db #0x00	; 0
      003892 0D                    2641 	.db #0x0d	; 13
      003893 12                    2642 	.db #0x12	; 18
      003894 12                    2643 	.db #0x12	; 18
      003895 12                    2644 	.db #0x12	; 18
      003896 0D                    2645 	.db #0x0d	; 13
      003897 00                    2646 	.db #0x00	; 0
      003898 00                    2647 	.db #0x00	; 0
      003899 18                    2648 	.db #0x18	; 24
      00389A 00                    2649 	.db #0x00	; 0
      00389B 00                    2650 	.db #0x00	; 0
      00389C 10                    2651 	.db #0x10	; 16
      00389D 18                    2652 	.db #0x18	; 24
      00389E 14                    2653 	.db #0x14	; 20
      00389F 12                    2654 	.db #0x12	; 18
      0038A0 11                    2655 	.db #0x11	; 17
      0038A1 00                    2656 	.db #0x00	; 0
      0038A2 00                    2657 	.db #0x00	; 0
      0038A3 10                    2658 	.db #0x10	; 16
      0038A4 18                    2659 	.db #0x18	; 24
      0038A5 14                    2660 	.db #0x14	; 20
      0038A6 12                    2661 	.db #0x12	; 18
      0038A7 11                    2662 	.db #0x11	; 17
      0038A8 00                    2663 	.db #0x00	; 0
      0038A9 00                    2664 	.db #0x00	; 0
      0038AA 00                    2665 	.db #0x00	; 0
      0038AB 00                    2666 	.db #0x00	; 0
      0038AC 00                    2667 	.db #0x00	; 0
      0038AD 00                    2668 	.db #0x00	; 0
      0038AE 00                    2669 	.db #0x00	; 0
      0038AF 00                    2670 	.db #0x00	; 0
      0038B0 00                    2671 	.db #0x00	; 0
      0038B1 00                    2672 	.db #0x00	; 0
      0038B2 00                    2673 	.db #0x00	; 0
      0038B3 00                    2674 	.db #0x00	; 0
      0038B4 00                    2675 	.db #0x00	; 0
      0038B5 00                    2676 	.db #0x00	; 0
      0038B6 00                    2677 	.db #0x00	; 0
      0038B7 00                    2678 	.db #0x00	; 0
      0038B8 00                    2679 	.db #0x00	; 0
      0038B9 00                    2680 	.db #0x00	; 0
      0038BA 00                    2681 	.db #0x00	; 0
      0038BB 00                    2682 	.db #0x00	; 0
      0038BC 00                    2683 	.db #0x00	; 0
      0038BD 00                    2684 	.db #0x00	; 0
      0038BE 00                    2685 	.db #0x00	; 0
      0038BF 00                    2686 	.db #0x00	; 0
      0038C0 00                    2687 	.db #0x00	; 0
      0038C1 00                    2688 	.db #0x00	; 0
      0038C2 00                    2689 	.db #0x00	; 0
      0038C3 00                    2690 	.db #0x00	; 0
      0038C4 00                    2691 	.db #0x00	; 0
      0038C5 00                    2692 	.db #0x00	; 0
      0038C6 00                    2693 	.db #0x00	; 0
      0038C7 00                    2694 	.db #0x00	; 0
      0038C8 00                    2695 	.db #0x00	; 0
      0038C9 00                    2696 	.db #0x00	; 0
      0038CA 00                    2697 	.db #0x00	; 0
      0038CB 00                    2698 	.db #0x00	; 0
      0038CC 00                    2699 	.db #0x00	; 0
      0038CD 00                    2700 	.db #0x00	; 0
      0038CE 00                    2701 	.db #0x00	; 0
      0038CF 00                    2702 	.db #0x00	; 0
      0038D0 00                    2703 	.db #0x00	; 0
      0038D1 00                    2704 	.db #0x00	; 0
      0038D2 00                    2705 	.db #0x00	; 0
      0038D3 00                    2706 	.db #0x00	; 0
      0038D4 00                    2707 	.db #0x00	; 0
      0038D5 00                    2708 	.db #0x00	; 0
      0038D6 00                    2709 	.db #0x00	; 0
      0038D7 00                    2710 	.db #0x00	; 0
      0038D8 00                    2711 	.db #0x00	; 0
      0038D9 00                    2712 	.db #0x00	; 0
      0038DA 00                    2713 	.db #0x00	; 0
      0038DB 00                    2714 	.db #0x00	; 0
      0038DC 00                    2715 	.db #0x00	; 0
      0038DD 00                    2716 	.db #0x00	; 0
      0038DE 00                    2717 	.db #0x00	; 0
      0038DF 00                    2718 	.db #0x00	; 0
      0038E0 00                    2719 	.db #0x00	; 0
      0038E1 00                    2720 	.db #0x00	; 0
      0038E2 00                    2721 	.db #0x00	; 0
      0038E3 00                    2722 	.db #0x00	; 0
      0038E4 00                    2723 	.db #0x00	; 0
      0038E5 00                    2724 	.db #0x00	; 0
      0038E6 00                    2725 	.db #0x00	; 0
      0038E7 00                    2726 	.db #0x00	; 0
      0038E8 00                    2727 	.db #0x00	; 0
      0038E9 00                    2728 	.db #0x00	; 0
      0038EA 00                    2729 	.db #0x00	; 0
      0038EB 00                    2730 	.db #0x00	; 0
      0038EC 00                    2731 	.db #0x00	; 0
      0038ED 00                    2732 	.db #0x00	; 0
      0038EE 00                    2733 	.db #0x00	; 0
      0038EF 00                    2734 	.db #0x00	; 0
      0038F0 00                    2735 	.db #0x00	; 0
      0038F1 00                    2736 	.db #0x00	; 0
      0038F2 00                    2737 	.db #0x00	; 0
      0038F3 00                    2738 	.db #0x00	; 0
      0038F4 00                    2739 	.db #0x00	; 0
      0038F5 00                    2740 	.db #0x00	; 0
      0038F6 00                    2741 	.db #0x00	; 0
      0038F7 00                    2742 	.db #0x00	; 0
      0038F8 00                    2743 	.db #0x00	; 0
      0038F9 00                    2744 	.db #0x00	; 0
      0038FA 00                    2745 	.db #0x00	; 0
      0038FB 00                    2746 	.db #0x00	; 0
      0038FC 00                    2747 	.db #0x00	; 0
      0038FD 00                    2748 	.db #0x00	; 0
      0038FE 00                    2749 	.db #0x00	; 0
      0038FF 00                    2750 	.db #0x00	; 0
      003900 00                    2751 	.db #0x00	; 0
      003901 00                    2752 	.db #0x00	; 0
      003902 00                    2753 	.db #0x00	; 0
      003903 00                    2754 	.db #0x00	; 0
      003904 00                    2755 	.db #0x00	; 0
      003905 00                    2756 	.db #0x00	; 0
      003906 00                    2757 	.db #0x00	; 0
      003907 00                    2758 	.db #0x00	; 0
      003908 00                    2759 	.db #0x00	; 0
      003909 00                    2760 	.db #0x00	; 0
      00390A 80                    2761 	.db #0x80	; 128
      00390B 80                    2762 	.db #0x80	; 128
      00390C 80                    2763 	.db #0x80	; 128
      00390D 80                    2764 	.db #0x80	; 128
      00390E 80                    2765 	.db #0x80	; 128
      00390F 80                    2766 	.db #0x80	; 128
      003910 80                    2767 	.db #0x80	; 128
      003911 80                    2768 	.db #0x80	; 128
      003912 00                    2769 	.db #0x00	; 0
      003913 00                    2770 	.db #0x00	; 0
      003914 00                    2771 	.db #0x00	; 0
      003915 00                    2772 	.db #0x00	; 0
      003916 00                    2773 	.db #0x00	; 0
      003917 00                    2774 	.db #0x00	; 0
      003918 00                    2775 	.db #0x00	; 0
      003919 00                    2776 	.db #0x00	; 0
      00391A 00                    2777 	.db #0x00	; 0
      00391B 00                    2778 	.db #0x00	; 0
      00391C 00                    2779 	.db #0x00	; 0
      00391D 00                    2780 	.db #0x00	; 0
      00391E 00                    2781 	.db #0x00	; 0
      00391F 00                    2782 	.db #0x00	; 0
      003920 00                    2783 	.db #0x00	; 0
      003921 00                    2784 	.db #0x00	; 0
      003922 00                    2785 	.db #0x00	; 0
      003923 00                    2786 	.db #0x00	; 0
      003924 00                    2787 	.db #0x00	; 0
      003925 00                    2788 	.db #0x00	; 0
      003926 00                    2789 	.db #0x00	; 0
      003927 00                    2790 	.db #0x00	; 0
      003928 00                    2791 	.db #0x00	; 0
      003929 00                    2792 	.db #0x00	; 0
      00392A 00                    2793 	.db #0x00	; 0
      00392B 00                    2794 	.db #0x00	; 0
      00392C 00                    2795 	.db #0x00	; 0
      00392D 00                    2796 	.db #0x00	; 0
      00392E 00                    2797 	.db #0x00	; 0
      00392F 00                    2798 	.db #0x00	; 0
      003930 00                    2799 	.db #0x00	; 0
      003931 00                    2800 	.db #0x00	; 0
      003932 00                    2801 	.db #0x00	; 0
      003933 00                    2802 	.db #0x00	; 0
      003934 00                    2803 	.db #0x00	; 0
      003935 00                    2804 	.db #0x00	; 0
      003936 00                    2805 	.db #0x00	; 0
      003937 00                    2806 	.db #0x00	; 0
      003938 00                    2807 	.db #0x00	; 0
      003939 00                    2808 	.db #0x00	; 0
      00393A 00                    2809 	.db #0x00	; 0
      00393B 00                    2810 	.db #0x00	; 0
      00393C 00                    2811 	.db #0x00	; 0
      00393D 00                    2812 	.db #0x00	; 0
      00393E 00                    2813 	.db #0x00	; 0
      00393F 00                    2814 	.db #0x00	; 0
      003940 00                    2815 	.db #0x00	; 0
      003941 00                    2816 	.db #0x00	; 0
      003942 00                    2817 	.db #0x00	; 0
      003943 00                    2818 	.db #0x00	; 0
      003944 00                    2819 	.db #0x00	; 0
      003945 00                    2820 	.db #0x00	; 0
      003946 00                    2821 	.db #0x00	; 0
      003947 00                    2822 	.db #0x00	; 0
      003948 00                    2823 	.db #0x00	; 0
      003949 00                    2824 	.db #0x00	; 0
      00394A 00                    2825 	.db #0x00	; 0
      00394B 00                    2826 	.db #0x00	; 0
      00394C 00                    2827 	.db #0x00	; 0
      00394D 00                    2828 	.db #0x00	; 0
      00394E 00                    2829 	.db #0x00	; 0
      00394F 7F                    2830 	.db #0x7f	; 127
      003950 03                    2831 	.db #0x03	; 3
      003951 0C                    2832 	.db #0x0c	; 12
      003952 30                    2833 	.db #0x30	; 48	'0'
      003953 0C                    2834 	.db #0x0c	; 12
      003954 03                    2835 	.db #0x03	; 3
      003955 7F                    2836 	.db #0x7f	; 127
      003956 00                    2837 	.db #0x00	; 0
      003957 00                    2838 	.db #0x00	; 0
      003958 38                    2839 	.db #0x38	; 56	'8'
      003959 54                    2840 	.db #0x54	; 84	'T'
      00395A 54                    2841 	.db #0x54	; 84	'T'
      00395B 58                    2842 	.db #0x58	; 88	'X'
      00395C 00                    2843 	.db #0x00	; 0
      00395D 00                    2844 	.db #0x00	; 0
      00395E 7C                    2845 	.db #0x7c	; 124
      00395F 04                    2846 	.db #0x04	; 4
      003960 04                    2847 	.db #0x04	; 4
      003961 78                    2848 	.db #0x78	; 120	'x'
      003962 00                    2849 	.db #0x00	; 0
      003963 00                    2850 	.db #0x00	; 0
      003964 3C                    2851 	.db #0x3c	; 60
      003965 40                    2852 	.db #0x40	; 64
      003966 40                    2853 	.db #0x40	; 64
      003967 7C                    2854 	.db #0x7c	; 124
      003968 00                    2855 	.db #0x00	; 0
      003969 00                    2856 	.db #0x00	; 0
      00396A 00                    2857 	.db #0x00	; 0
      00396B 00                    2858 	.db #0x00	; 0
      00396C 00                    2859 	.db #0x00	; 0
      00396D 00                    2860 	.db #0x00	; 0
      00396E 00                    2861 	.db #0x00	; 0
      00396F 00                    2862 	.db #0x00	; 0
      003970 00                    2863 	.db #0x00	; 0
      003971 00                    2864 	.db #0x00	; 0
      003972 00                    2865 	.db #0x00	; 0
      003973 00                    2866 	.db #0x00	; 0
      003974 00                    2867 	.db #0x00	; 0
      003975 00                    2868 	.db #0x00	; 0
      003976 00                    2869 	.db #0x00	; 0
      003977 00                    2870 	.db #0x00	; 0
      003978 00                    2871 	.db #0x00	; 0
      003979 00                    2872 	.db #0x00	; 0
      00397A 00                    2873 	.db #0x00	; 0
      00397B 00                    2874 	.db #0x00	; 0
      00397C 00                    2875 	.db #0x00	; 0
      00397D 00                    2876 	.db #0x00	; 0
      00397E 00                    2877 	.db #0x00	; 0
      00397F 00                    2878 	.db #0x00	; 0
      003980 00                    2879 	.db #0x00	; 0
      003981 00                    2880 	.db #0x00	; 0
      003982 00                    2881 	.db #0x00	; 0
      003983 00                    2882 	.db #0x00	; 0
      003984 00                    2883 	.db #0x00	; 0
      003985 00                    2884 	.db #0x00	; 0
      003986 00                    2885 	.db #0x00	; 0
      003987 00                    2886 	.db #0x00	; 0
      003988 00                    2887 	.db #0x00	; 0
      003989 00                    2888 	.db #0x00	; 0
      00398A FF                    2889 	.db #0xff	; 255
      00398B AA                    2890 	.db #0xaa	; 170
      00398C AA                    2891 	.db #0xaa	; 170
      00398D AA                    2892 	.db #0xaa	; 170
      00398E 28                    2893 	.db #0x28	; 40
      00398F 08                    2894 	.db #0x08	; 8
      003990 00                    2895 	.db #0x00	; 0
      003991 FF                    2896 	.db #0xff	; 255
      003992 00                    2897 	.db #0x00	; 0
      003993 00                    2898 	.db #0x00	; 0
      003994 00                    2899 	.db #0x00	; 0
      003995 00                    2900 	.db #0x00	; 0
      003996 00                    2901 	.db #0x00	; 0
      003997 00                    2902 	.db #0x00	; 0
      003998 00                    2903 	.db #0x00	; 0
      003999 00                    2904 	.db #0x00	; 0
      00399A 00                    2905 	.db #0x00	; 0
      00399B 00                    2906 	.db #0x00	; 0
      00399C 00                    2907 	.db #0x00	; 0
      00399D 00                    2908 	.db #0x00	; 0
      00399E 00                    2909 	.db #0x00	; 0
      00399F 00                    2910 	.db #0x00	; 0
      0039A0 00                    2911 	.db #0x00	; 0
      0039A1 00                    2912 	.db #0x00	; 0
      0039A2 00                    2913 	.db #0x00	; 0
      0039A3 00                    2914 	.db #0x00	; 0
      0039A4 00                    2915 	.db #0x00	; 0
      0039A5 00                    2916 	.db #0x00	; 0
      0039A6 00                    2917 	.db #0x00	; 0
      0039A7 00                    2918 	.db #0x00	; 0
      0039A8 00                    2919 	.db #0x00	; 0
      0039A9 00                    2920 	.db #0x00	; 0
      0039AA 00                    2921 	.db #0x00	; 0
      0039AB 00                    2922 	.db #0x00	; 0
      0039AC 00                    2923 	.db #0x00	; 0
      0039AD 00                    2924 	.db #0x00	; 0
      0039AE 00                    2925 	.db #0x00	; 0
      0039AF 00                    2926 	.db #0x00	; 0
      0039B0 00                    2927 	.db #0x00	; 0
      0039B1 00                    2928 	.db #0x00	; 0
      0039B2 00                    2929 	.db #0x00	; 0
      0039B3 00                    2930 	.db #0x00	; 0
      0039B4 00                    2931 	.db #0x00	; 0
      0039B5 00                    2932 	.db #0x00	; 0
      0039B6 00                    2933 	.db #0x00	; 0
      0039B7 7F                    2934 	.db #0x7f	; 127
      0039B8 03                    2935 	.db #0x03	; 3
      0039B9 0C                    2936 	.db #0x0c	; 12
      0039BA 30                    2937 	.db #0x30	; 48	'0'
      0039BB 0C                    2938 	.db #0x0c	; 12
      0039BC 03                    2939 	.db #0x03	; 3
      0039BD 7F                    2940 	.db #0x7f	; 127
      0039BE 00                    2941 	.db #0x00	; 0
      0039BF 00                    2942 	.db #0x00	; 0
      0039C0 26                    2943 	.db #0x26	; 38
      0039C1 49                    2944 	.db #0x49	; 73	'I'
      0039C2 49                    2945 	.db #0x49	; 73	'I'
      0039C3 49                    2946 	.db #0x49	; 73	'I'
      0039C4 32                    2947 	.db #0x32	; 50	'2'
      0039C5 00                    2948 	.db #0x00	; 0
      0039C6 00                    2949 	.db #0x00	; 0
      0039C7 7F                    2950 	.db #0x7f	; 127
      0039C8 02                    2951 	.db #0x02	; 2
      0039C9 04                    2952 	.db #0x04	; 4
      0039CA 08                    2953 	.db #0x08	; 8
      0039CB 10                    2954 	.db #0x10	; 16
      0039CC 7F                    2955 	.db #0x7f	; 127
      0039CD 00                    2956 	.db #0x00	; 0
      0039CE                       2957 _BMP2:
      0039CE 00                    2958 	.db #0x00	; 0
      0039CF 03                    2959 	.db #0x03	; 3
      0039D0 05                    2960 	.db #0x05	; 5
      0039D1 09                    2961 	.db #0x09	; 9
      0039D2 11                    2962 	.db #0x11	; 17
      0039D3 FF                    2963 	.db #0xff	; 255
      0039D4 11                    2964 	.db #0x11	; 17
      0039D5 89                    2965 	.db #0x89	; 137
      0039D6 05                    2966 	.db #0x05	; 5
      0039D7 C3                    2967 	.db #0xc3	; 195
      0039D8 00                    2968 	.db #0x00	; 0
      0039D9 E0                    2969 	.db #0xe0	; 224
      0039DA 00                    2970 	.db #0x00	; 0
      0039DB F0                    2971 	.db #0xf0	; 240
      0039DC 00                    2972 	.db #0x00	; 0
      0039DD F8                    2973 	.db #0xf8	; 248
      0039DE 00                    2974 	.db #0x00	; 0
      0039DF 00                    2975 	.db #0x00	; 0
      0039E0 00                    2976 	.db #0x00	; 0
      0039E1 00                    2977 	.db #0x00	; 0
      0039E2 00                    2978 	.db #0x00	; 0
      0039E3 00                    2979 	.db #0x00	; 0
      0039E4 00                    2980 	.db #0x00	; 0
      0039E5 44                    2981 	.db #0x44	; 68	'D'
      0039E6 28                    2982 	.db #0x28	; 40
      0039E7 FF                    2983 	.db #0xff	; 255
      0039E8 11                    2984 	.db #0x11	; 17
      0039E9 AA                    2985 	.db #0xaa	; 170
      0039EA 44                    2986 	.db #0x44	; 68	'D'
      0039EB 00                    2987 	.db #0x00	; 0
      0039EC 00                    2988 	.db #0x00	; 0
      0039ED 00                    2989 	.db #0x00	; 0
      0039EE 00                    2990 	.db #0x00	; 0
      0039EF 00                    2991 	.db #0x00	; 0
      0039F0 00                    2992 	.db #0x00	; 0
      0039F1 00                    2993 	.db #0x00	; 0
      0039F2 00                    2994 	.db #0x00	; 0
      0039F3 00                    2995 	.db #0x00	; 0
      0039F4 00                    2996 	.db #0x00	; 0
      0039F5 00                    2997 	.db #0x00	; 0
      0039F6 00                    2998 	.db #0x00	; 0
      0039F7 00                    2999 	.db #0x00	; 0
      0039F8 00                    3000 	.db #0x00	; 0
      0039F9 00                    3001 	.db #0x00	; 0
      0039FA 00                    3002 	.db #0x00	; 0
      0039FB 00                    3003 	.db #0x00	; 0
      0039FC 00                    3004 	.db #0x00	; 0
      0039FD 00                    3005 	.db #0x00	; 0
      0039FE 00                    3006 	.db #0x00	; 0
      0039FF 00                    3007 	.db #0x00	; 0
      003A00 00                    3008 	.db #0x00	; 0
      003A01 00                    3009 	.db #0x00	; 0
      003A02 00                    3010 	.db #0x00	; 0
      003A03 00                    3011 	.db #0x00	; 0
      003A04 00                    3012 	.db #0x00	; 0
      003A05 00                    3013 	.db #0x00	; 0
      003A06 00                    3014 	.db #0x00	; 0
      003A07 00                    3015 	.db #0x00	; 0
      003A08 00                    3016 	.db #0x00	; 0
      003A09 00                    3017 	.db #0x00	; 0
      003A0A 00                    3018 	.db #0x00	; 0
      003A0B 00                    3019 	.db #0x00	; 0
      003A0C 00                    3020 	.db #0x00	; 0
      003A0D 00                    3021 	.db #0x00	; 0
      003A0E 00                    3022 	.db #0x00	; 0
      003A0F 00                    3023 	.db #0x00	; 0
      003A10 00                    3024 	.db #0x00	; 0
      003A11 00                    3025 	.db #0x00	; 0
      003A12 00                    3026 	.db #0x00	; 0
      003A13 00                    3027 	.db #0x00	; 0
      003A14 00                    3028 	.db #0x00	; 0
      003A15 00                    3029 	.db #0x00	; 0
      003A16 00                    3030 	.db #0x00	; 0
      003A17 00                    3031 	.db #0x00	; 0
      003A18 00                    3032 	.db #0x00	; 0
      003A19 00                    3033 	.db #0x00	; 0
      003A1A 00                    3034 	.db #0x00	; 0
      003A1B 00                    3035 	.db #0x00	; 0
      003A1C 00                    3036 	.db #0x00	; 0
      003A1D 00                    3037 	.db #0x00	; 0
      003A1E 00                    3038 	.db #0x00	; 0
      003A1F 00                    3039 	.db #0x00	; 0
      003A20 00                    3040 	.db #0x00	; 0
      003A21 00                    3041 	.db #0x00	; 0
      003A22 00                    3042 	.db #0x00	; 0
      003A23 00                    3043 	.db #0x00	; 0
      003A24 00                    3044 	.db #0x00	; 0
      003A25 00                    3045 	.db #0x00	; 0
      003A26 00                    3046 	.db #0x00	; 0
      003A27 00                    3047 	.db #0x00	; 0
      003A28 83                    3048 	.db #0x83	; 131
      003A29 01                    3049 	.db #0x01	; 1
      003A2A 38                    3050 	.db #0x38	; 56	'8'
      003A2B 44                    3051 	.db #0x44	; 68	'D'
      003A2C 82                    3052 	.db #0x82	; 130
      003A2D 92                    3053 	.db #0x92	; 146
      003A2E 92                    3054 	.db #0x92	; 146
      003A2F 74                    3055 	.db #0x74	; 116	't'
      003A30 01                    3056 	.db #0x01	; 1
      003A31 83                    3057 	.db #0x83	; 131
      003A32 00                    3058 	.db #0x00	; 0
      003A33 00                    3059 	.db #0x00	; 0
      003A34 00                    3060 	.db #0x00	; 0
      003A35 00                    3061 	.db #0x00	; 0
      003A36 00                    3062 	.db #0x00	; 0
      003A37 00                    3063 	.db #0x00	; 0
      003A38 00                    3064 	.db #0x00	; 0
      003A39 7C                    3065 	.db #0x7c	; 124
      003A3A 44                    3066 	.db #0x44	; 68	'D'
      003A3B FF                    3067 	.db #0xff	; 255
      003A3C 01                    3068 	.db #0x01	; 1
      003A3D 7D                    3069 	.db #0x7d	; 125
      003A3E 7D                    3070 	.db #0x7d	; 125
      003A3F 7D                    3071 	.db #0x7d	; 125
      003A40 7D                    3072 	.db #0x7d	; 125
      003A41 01                    3073 	.db #0x01	; 1
      003A42 7D                    3074 	.db #0x7d	; 125
      003A43 7D                    3075 	.db #0x7d	; 125
      003A44 7D                    3076 	.db #0x7d	; 125
      003A45 7D                    3077 	.db #0x7d	; 125
      003A46 01                    3078 	.db #0x01	; 1
      003A47 7D                    3079 	.db #0x7d	; 125
      003A48 7D                    3080 	.db #0x7d	; 125
      003A49 7D                    3081 	.db #0x7d	; 125
      003A4A 7D                    3082 	.db #0x7d	; 125
      003A4B 01                    3083 	.db #0x01	; 1
      003A4C FF                    3084 	.db #0xff	; 255
      003A4D 00                    3085 	.db #0x00	; 0
      003A4E 00                    3086 	.db #0x00	; 0
      003A4F 00                    3087 	.db #0x00	; 0
      003A50 00                    3088 	.db #0x00	; 0
      003A51 00                    3089 	.db #0x00	; 0
      003A52 00                    3090 	.db #0x00	; 0
      003A53 01                    3091 	.db #0x01	; 1
      003A54 00                    3092 	.db #0x00	; 0
      003A55 01                    3093 	.db #0x01	; 1
      003A56 00                    3094 	.db #0x00	; 0
      003A57 01                    3095 	.db #0x01	; 1
      003A58 00                    3096 	.db #0x00	; 0
      003A59 01                    3097 	.db #0x01	; 1
      003A5A 00                    3098 	.db #0x00	; 0
      003A5B 01                    3099 	.db #0x01	; 1
      003A5C 00                    3100 	.db #0x00	; 0
      003A5D 01                    3101 	.db #0x01	; 1
      003A5E 00                    3102 	.db #0x00	; 0
      003A5F 00                    3103 	.db #0x00	; 0
      003A60 00                    3104 	.db #0x00	; 0
      003A61 00                    3105 	.db #0x00	; 0
      003A62 00                    3106 	.db #0x00	; 0
      003A63 00                    3107 	.db #0x00	; 0
      003A64 00                    3108 	.db #0x00	; 0
      003A65 00                    3109 	.db #0x00	; 0
      003A66 00                    3110 	.db #0x00	; 0
      003A67 01                    3111 	.db #0x01	; 1
      003A68 01                    3112 	.db #0x01	; 1
      003A69 00                    3113 	.db #0x00	; 0
      003A6A 00                    3114 	.db #0x00	; 0
      003A6B 00                    3115 	.db #0x00	; 0
      003A6C 00                    3116 	.db #0x00	; 0
      003A6D 00                    3117 	.db #0x00	; 0
      003A6E 00                    3118 	.db #0x00	; 0
      003A6F 00                    3119 	.db #0x00	; 0
      003A70 00                    3120 	.db #0x00	; 0
      003A71 00                    3121 	.db #0x00	; 0
      003A72 00                    3122 	.db #0x00	; 0
      003A73 00                    3123 	.db #0x00	; 0
      003A74 00                    3124 	.db #0x00	; 0
      003A75 00                    3125 	.db #0x00	; 0
      003A76 00                    3126 	.db #0x00	; 0
      003A77 00                    3127 	.db #0x00	; 0
      003A78 00                    3128 	.db #0x00	; 0
      003A79 00                    3129 	.db #0x00	; 0
      003A7A 00                    3130 	.db #0x00	; 0
      003A7B 00                    3131 	.db #0x00	; 0
      003A7C 00                    3132 	.db #0x00	; 0
      003A7D 00                    3133 	.db #0x00	; 0
      003A7E 00                    3134 	.db #0x00	; 0
      003A7F 00                    3135 	.db #0x00	; 0
      003A80 00                    3136 	.db #0x00	; 0
      003A81 00                    3137 	.db #0x00	; 0
      003A82 00                    3138 	.db #0x00	; 0
      003A83 00                    3139 	.db #0x00	; 0
      003A84 00                    3140 	.db #0x00	; 0
      003A85 00                    3141 	.db #0x00	; 0
      003A86 00                    3142 	.db #0x00	; 0
      003A87 00                    3143 	.db #0x00	; 0
      003A88 00                    3144 	.db #0x00	; 0
      003A89 00                    3145 	.db #0x00	; 0
      003A8A 00                    3146 	.db #0x00	; 0
      003A8B 00                    3147 	.db #0x00	; 0
      003A8C 00                    3148 	.db #0x00	; 0
      003A8D 00                    3149 	.db #0x00	; 0
      003A8E 00                    3150 	.db #0x00	; 0
      003A8F 00                    3151 	.db #0x00	; 0
      003A90 00                    3152 	.db #0x00	; 0
      003A91 00                    3153 	.db #0x00	; 0
      003A92 00                    3154 	.db #0x00	; 0
      003A93 00                    3155 	.db #0x00	; 0
      003A94 00                    3156 	.db #0x00	; 0
      003A95 00                    3157 	.db #0x00	; 0
      003A96 00                    3158 	.db #0x00	; 0
      003A97 00                    3159 	.db #0x00	; 0
      003A98 00                    3160 	.db #0x00	; 0
      003A99 00                    3161 	.db #0x00	; 0
      003A9A 00                    3162 	.db #0x00	; 0
      003A9B 00                    3163 	.db #0x00	; 0
      003A9C 00                    3164 	.db #0x00	; 0
      003A9D 00                    3165 	.db #0x00	; 0
      003A9E 00                    3166 	.db #0x00	; 0
      003A9F 00                    3167 	.db #0x00	; 0
      003AA0 00                    3168 	.db #0x00	; 0
      003AA1 00                    3169 	.db #0x00	; 0
      003AA2 00                    3170 	.db #0x00	; 0
      003AA3 00                    3171 	.db #0x00	; 0
      003AA4 00                    3172 	.db #0x00	; 0
      003AA5 00                    3173 	.db #0x00	; 0
      003AA6 00                    3174 	.db #0x00	; 0
      003AA7 00                    3175 	.db #0x00	; 0
      003AA8 01                    3176 	.db #0x01	; 1
      003AA9 01                    3177 	.db #0x01	; 1
      003AAA 00                    3178 	.db #0x00	; 0
      003AAB 00                    3179 	.db #0x00	; 0
      003AAC 00                    3180 	.db #0x00	; 0
      003AAD 00                    3181 	.db #0x00	; 0
      003AAE 00                    3182 	.db #0x00	; 0
      003AAF 00                    3183 	.db #0x00	; 0
      003AB0 01                    3184 	.db #0x01	; 1
      003AB1 01                    3185 	.db #0x01	; 1
      003AB2 00                    3186 	.db #0x00	; 0
      003AB3 00                    3187 	.db #0x00	; 0
      003AB4 00                    3188 	.db #0x00	; 0
      003AB5 00                    3189 	.db #0x00	; 0
      003AB6 00                    3190 	.db #0x00	; 0
      003AB7 00                    3191 	.db #0x00	; 0
      003AB8 00                    3192 	.db #0x00	; 0
      003AB9 00                    3193 	.db #0x00	; 0
      003ABA 00                    3194 	.db #0x00	; 0
      003ABB 01                    3195 	.db #0x01	; 1
      003ABC 01                    3196 	.db #0x01	; 1
      003ABD 01                    3197 	.db #0x01	; 1
      003ABE 01                    3198 	.db #0x01	; 1
      003ABF 01                    3199 	.db #0x01	; 1
      003AC0 01                    3200 	.db #0x01	; 1
      003AC1 01                    3201 	.db #0x01	; 1
      003AC2 01                    3202 	.db #0x01	; 1
      003AC3 01                    3203 	.db #0x01	; 1
      003AC4 01                    3204 	.db #0x01	; 1
      003AC5 01                    3205 	.db #0x01	; 1
      003AC6 01                    3206 	.db #0x01	; 1
      003AC7 01                    3207 	.db #0x01	; 1
      003AC8 01                    3208 	.db #0x01	; 1
      003AC9 01                    3209 	.db #0x01	; 1
      003ACA 01                    3210 	.db #0x01	; 1
      003ACB 01                    3211 	.db #0x01	; 1
      003ACC 01                    3212 	.db #0x01	; 1
      003ACD 00                    3213 	.db #0x00	; 0
      003ACE 00                    3214 	.db #0x00	; 0
      003ACF 00                    3215 	.db #0x00	; 0
      003AD0 00                    3216 	.db #0x00	; 0
      003AD1 00                    3217 	.db #0x00	; 0
      003AD2 00                    3218 	.db #0x00	; 0
      003AD3 00                    3219 	.db #0x00	; 0
      003AD4 00                    3220 	.db #0x00	; 0
      003AD5 00                    3221 	.db #0x00	; 0
      003AD6 00                    3222 	.db #0x00	; 0
      003AD7 00                    3223 	.db #0x00	; 0
      003AD8 00                    3224 	.db #0x00	; 0
      003AD9 00                    3225 	.db #0x00	; 0
      003ADA 00                    3226 	.db #0x00	; 0
      003ADB 00                    3227 	.db #0x00	; 0
      003ADC 00                    3228 	.db #0x00	; 0
      003ADD 00                    3229 	.db #0x00	; 0
      003ADE 00                    3230 	.db #0x00	; 0
      003ADF 00                    3231 	.db #0x00	; 0
      003AE0 00                    3232 	.db #0x00	; 0
      003AE1 00                    3233 	.db #0x00	; 0
      003AE2 00                    3234 	.db #0x00	; 0
      003AE3 00                    3235 	.db #0x00	; 0
      003AE4 00                    3236 	.db #0x00	; 0
      003AE5 00                    3237 	.db #0x00	; 0
      003AE6 00                    3238 	.db #0x00	; 0
      003AE7 00                    3239 	.db #0x00	; 0
      003AE8 00                    3240 	.db #0x00	; 0
      003AE9 00                    3241 	.db #0x00	; 0
      003AEA 00                    3242 	.db #0x00	; 0
      003AEB 00                    3243 	.db #0x00	; 0
      003AEC 00                    3244 	.db #0x00	; 0
      003AED F8                    3245 	.db #0xf8	; 248
      003AEE 08                    3246 	.db #0x08	; 8
      003AEF 08                    3247 	.db #0x08	; 8
      003AF0 08                    3248 	.db #0x08	; 8
      003AF1 08                    3249 	.db #0x08	; 8
      003AF2 08                    3250 	.db #0x08	; 8
      003AF3 08                    3251 	.db #0x08	; 8
      003AF4 08                    3252 	.db #0x08	; 8
      003AF5 00                    3253 	.db #0x00	; 0
      003AF6 F8                    3254 	.db #0xf8	; 248
      003AF7 18                    3255 	.db #0x18	; 24
      003AF8 60                    3256 	.db #0x60	; 96
      003AF9 80                    3257 	.db #0x80	; 128
      003AFA 00                    3258 	.db #0x00	; 0
      003AFB 00                    3259 	.db #0x00	; 0
      003AFC 00                    3260 	.db #0x00	; 0
      003AFD 80                    3261 	.db #0x80	; 128
      003AFE 60                    3262 	.db #0x60	; 96
      003AFF 18                    3263 	.db #0x18	; 24
      003B00 F8                    3264 	.db #0xf8	; 248
      003B01 00                    3265 	.db #0x00	; 0
      003B02 00                    3266 	.db #0x00	; 0
      003B03 00                    3267 	.db #0x00	; 0
      003B04 20                    3268 	.db #0x20	; 32
      003B05 20                    3269 	.db #0x20	; 32
      003B06 F8                    3270 	.db #0xf8	; 248
      003B07 00                    3271 	.db #0x00	; 0
      003B08 00                    3272 	.db #0x00	; 0
      003B09 00                    3273 	.db #0x00	; 0
      003B0A 00                    3274 	.db #0x00	; 0
      003B0B 00                    3275 	.db #0x00	; 0
      003B0C 00                    3276 	.db #0x00	; 0
      003B0D E0                    3277 	.db #0xe0	; 224
      003B0E 10                    3278 	.db #0x10	; 16
      003B0F 08                    3279 	.db #0x08	; 8
      003B10 08                    3280 	.db #0x08	; 8
      003B11 08                    3281 	.db #0x08	; 8
      003B12 08                    3282 	.db #0x08	; 8
      003B13 10                    3283 	.db #0x10	; 16
      003B14 E0                    3284 	.db #0xe0	; 224
      003B15 00                    3285 	.db #0x00	; 0
      003B16 00                    3286 	.db #0x00	; 0
      003B17 00                    3287 	.db #0x00	; 0
      003B18 20                    3288 	.db #0x20	; 32
      003B19 20                    3289 	.db #0x20	; 32
      003B1A F8                    3290 	.db #0xf8	; 248
      003B1B 00                    3291 	.db #0x00	; 0
      003B1C 00                    3292 	.db #0x00	; 0
      003B1D 00                    3293 	.db #0x00	; 0
      003B1E 00                    3294 	.db #0x00	; 0
      003B1F 00                    3295 	.db #0x00	; 0
      003B20 00                    3296 	.db #0x00	; 0
      003B21 00                    3297 	.db #0x00	; 0
      003B22 00                    3298 	.db #0x00	; 0
      003B23 00                    3299 	.db #0x00	; 0
      003B24 00                    3300 	.db #0x00	; 0
      003B25 00                    3301 	.db #0x00	; 0
      003B26 00                    3302 	.db #0x00	; 0
      003B27 08                    3303 	.db #0x08	; 8
      003B28 08                    3304 	.db #0x08	; 8
      003B29 08                    3305 	.db #0x08	; 8
      003B2A 08                    3306 	.db #0x08	; 8
      003B2B 08                    3307 	.db #0x08	; 8
      003B2C 88                    3308 	.db #0x88	; 136
      003B2D 68                    3309 	.db #0x68	; 104	'h'
      003B2E 18                    3310 	.db #0x18	; 24
      003B2F 00                    3311 	.db #0x00	; 0
      003B30 00                    3312 	.db #0x00	; 0
      003B31 00                    3313 	.db #0x00	; 0
      003B32 00                    3314 	.db #0x00	; 0
      003B33 00                    3315 	.db #0x00	; 0
      003B34 00                    3316 	.db #0x00	; 0
      003B35 00                    3317 	.db #0x00	; 0
      003B36 00                    3318 	.db #0x00	; 0
      003B37 00                    3319 	.db #0x00	; 0
      003B38 00                    3320 	.db #0x00	; 0
      003B39 00                    3321 	.db #0x00	; 0
      003B3A 00                    3322 	.db #0x00	; 0
      003B3B 00                    3323 	.db #0x00	; 0
      003B3C 00                    3324 	.db #0x00	; 0
      003B3D 00                    3325 	.db #0x00	; 0
      003B3E 00                    3326 	.db #0x00	; 0
      003B3F 00                    3327 	.db #0x00	; 0
      003B40 00                    3328 	.db #0x00	; 0
      003B41 00                    3329 	.db #0x00	; 0
      003B42 00                    3330 	.db #0x00	; 0
      003B43 00                    3331 	.db #0x00	; 0
      003B44 00                    3332 	.db #0x00	; 0
      003B45 00                    3333 	.db #0x00	; 0
      003B46 00                    3334 	.db #0x00	; 0
      003B47 00                    3335 	.db #0x00	; 0
      003B48 00                    3336 	.db #0x00	; 0
      003B49 00                    3337 	.db #0x00	; 0
      003B4A 00                    3338 	.db #0x00	; 0
      003B4B 00                    3339 	.db #0x00	; 0
      003B4C 00                    3340 	.db #0x00	; 0
      003B4D 00                    3341 	.db #0x00	; 0
      003B4E 00                    3342 	.db #0x00	; 0
      003B4F 00                    3343 	.db #0x00	; 0
      003B50 00                    3344 	.db #0x00	; 0
      003B51 00                    3345 	.db #0x00	; 0
      003B52 00                    3346 	.db #0x00	; 0
      003B53 00                    3347 	.db #0x00	; 0
      003B54 00                    3348 	.db #0x00	; 0
      003B55 00                    3349 	.db #0x00	; 0
      003B56 00                    3350 	.db #0x00	; 0
      003B57 00                    3351 	.db #0x00	; 0
      003B58 00                    3352 	.db #0x00	; 0
      003B59 00                    3353 	.db #0x00	; 0
      003B5A 00                    3354 	.db #0x00	; 0
      003B5B 00                    3355 	.db #0x00	; 0
      003B5C 00                    3356 	.db #0x00	; 0
      003B5D 00                    3357 	.db #0x00	; 0
      003B5E 00                    3358 	.db #0x00	; 0
      003B5F 00                    3359 	.db #0x00	; 0
      003B60 00                    3360 	.db #0x00	; 0
      003B61 00                    3361 	.db #0x00	; 0
      003B62 00                    3362 	.db #0x00	; 0
      003B63 00                    3363 	.db #0x00	; 0
      003B64 00                    3364 	.db #0x00	; 0
      003B65 00                    3365 	.db #0x00	; 0
      003B66 00                    3366 	.db #0x00	; 0
      003B67 00                    3367 	.db #0x00	; 0
      003B68 00                    3368 	.db #0x00	; 0
      003B69 00                    3369 	.db #0x00	; 0
      003B6A 00                    3370 	.db #0x00	; 0
      003B6B 00                    3371 	.db #0x00	; 0
      003B6C 00                    3372 	.db #0x00	; 0
      003B6D 7F                    3373 	.db #0x7f	; 127
      003B6E 01                    3374 	.db #0x01	; 1
      003B6F 01                    3375 	.db #0x01	; 1
      003B70 01                    3376 	.db #0x01	; 1
      003B71 01                    3377 	.db #0x01	; 1
      003B72 01                    3378 	.db #0x01	; 1
      003B73 01                    3379 	.db #0x01	; 1
      003B74 00                    3380 	.db #0x00	; 0
      003B75 00                    3381 	.db #0x00	; 0
      003B76 7F                    3382 	.db #0x7f	; 127
      003B77 00                    3383 	.db #0x00	; 0
      003B78 00                    3384 	.db #0x00	; 0
      003B79 01                    3385 	.db #0x01	; 1
      003B7A 06                    3386 	.db #0x06	; 6
      003B7B 18                    3387 	.db #0x18	; 24
      003B7C 06                    3388 	.db #0x06	; 6
      003B7D 01                    3389 	.db #0x01	; 1
      003B7E 00                    3390 	.db #0x00	; 0
      003B7F 00                    3391 	.db #0x00	; 0
      003B80 7F                    3392 	.db #0x7f	; 127
      003B81 00                    3393 	.db #0x00	; 0
      003B82 00                    3394 	.db #0x00	; 0
      003B83 00                    3395 	.db #0x00	; 0
      003B84 40                    3396 	.db #0x40	; 64
      003B85 40                    3397 	.db #0x40	; 64
      003B86 7F                    3398 	.db #0x7f	; 127
      003B87 40                    3399 	.db #0x40	; 64
      003B88 40                    3400 	.db #0x40	; 64
      003B89 00                    3401 	.db #0x00	; 0
      003B8A 00                    3402 	.db #0x00	; 0
      003B8B 00                    3403 	.db #0x00	; 0
      003B8C 00                    3404 	.db #0x00	; 0
      003B8D 1F                    3405 	.db #0x1f	; 31
      003B8E 20                    3406 	.db #0x20	; 32
      003B8F 40                    3407 	.db #0x40	; 64
      003B90 40                    3408 	.db #0x40	; 64
      003B91 40                    3409 	.db #0x40	; 64
      003B92 40                    3410 	.db #0x40	; 64
      003B93 20                    3411 	.db #0x20	; 32
      003B94 1F                    3412 	.db #0x1f	; 31
      003B95 00                    3413 	.db #0x00	; 0
      003B96 00                    3414 	.db #0x00	; 0
      003B97 00                    3415 	.db #0x00	; 0
      003B98 40                    3416 	.db #0x40	; 64
      003B99 40                    3417 	.db #0x40	; 64
      003B9A 7F                    3418 	.db #0x7f	; 127
      003B9B 40                    3419 	.db #0x40	; 64
      003B9C 40                    3420 	.db #0x40	; 64
      003B9D 00                    3421 	.db #0x00	; 0
      003B9E 00                    3422 	.db #0x00	; 0
      003B9F 00                    3423 	.db #0x00	; 0
      003BA0 00                    3424 	.db #0x00	; 0
      003BA1 00                    3425 	.db #0x00	; 0
      003BA2 60                    3426 	.db #0x60	; 96
      003BA3 00                    3427 	.db #0x00	; 0
      003BA4 00                    3428 	.db #0x00	; 0
      003BA5 00                    3429 	.db #0x00	; 0
      003BA6 00                    3430 	.db #0x00	; 0
      003BA7 00                    3431 	.db #0x00	; 0
      003BA8 00                    3432 	.db #0x00	; 0
      003BA9 60                    3433 	.db #0x60	; 96
      003BAA 18                    3434 	.db #0x18	; 24
      003BAB 06                    3435 	.db #0x06	; 6
      003BAC 01                    3436 	.db #0x01	; 1
      003BAD 00                    3437 	.db #0x00	; 0
      003BAE 00                    3438 	.db #0x00	; 0
      003BAF 00                    3439 	.db #0x00	; 0
      003BB0 00                    3440 	.db #0x00	; 0
      003BB1 00                    3441 	.db #0x00	; 0
      003BB2 00                    3442 	.db #0x00	; 0
      003BB3 00                    3443 	.db #0x00	; 0
      003BB4 00                    3444 	.db #0x00	; 0
      003BB5 00                    3445 	.db #0x00	; 0
      003BB6 00                    3446 	.db #0x00	; 0
      003BB7 00                    3447 	.db #0x00	; 0
      003BB8 00                    3448 	.db #0x00	; 0
      003BB9 00                    3449 	.db #0x00	; 0
      003BBA 00                    3450 	.db #0x00	; 0
      003BBB 00                    3451 	.db #0x00	; 0
      003BBC 00                    3452 	.db #0x00	; 0
      003BBD 00                    3453 	.db #0x00	; 0
      003BBE 00                    3454 	.db #0x00	; 0
      003BBF 00                    3455 	.db #0x00	; 0
      003BC0 00                    3456 	.db #0x00	; 0
      003BC1 00                    3457 	.db #0x00	; 0
      003BC2 00                    3458 	.db #0x00	; 0
      003BC3 00                    3459 	.db #0x00	; 0
      003BC4 00                    3460 	.db #0x00	; 0
      003BC5 00                    3461 	.db #0x00	; 0
      003BC6 00                    3462 	.db #0x00	; 0
      003BC7 00                    3463 	.db #0x00	; 0
      003BC8 00                    3464 	.db #0x00	; 0
      003BC9 00                    3465 	.db #0x00	; 0
      003BCA 00                    3466 	.db #0x00	; 0
      003BCB 00                    3467 	.db #0x00	; 0
      003BCC 00                    3468 	.db #0x00	; 0
      003BCD 00                    3469 	.db #0x00	; 0
      003BCE 00                    3470 	.db #0x00	; 0
      003BCF 00                    3471 	.db #0x00	; 0
      003BD0 00                    3472 	.db #0x00	; 0
      003BD1 00                    3473 	.db #0x00	; 0
      003BD2 00                    3474 	.db #0x00	; 0
      003BD3 00                    3475 	.db #0x00	; 0
      003BD4 00                    3476 	.db #0x00	; 0
      003BD5 00                    3477 	.db #0x00	; 0
      003BD6 00                    3478 	.db #0x00	; 0
      003BD7 00                    3479 	.db #0x00	; 0
      003BD8 00                    3480 	.db #0x00	; 0
      003BD9 00                    3481 	.db #0x00	; 0
      003BDA 00                    3482 	.db #0x00	; 0
      003BDB 00                    3483 	.db #0x00	; 0
      003BDC 00                    3484 	.db #0x00	; 0
      003BDD 00                    3485 	.db #0x00	; 0
      003BDE 00                    3486 	.db #0x00	; 0
      003BDF 00                    3487 	.db #0x00	; 0
      003BE0 00                    3488 	.db #0x00	; 0
      003BE1 00                    3489 	.db #0x00	; 0
      003BE2 00                    3490 	.db #0x00	; 0
      003BE3 00                    3491 	.db #0x00	; 0
      003BE4 00                    3492 	.db #0x00	; 0
      003BE5 00                    3493 	.db #0x00	; 0
      003BE6 00                    3494 	.db #0x00	; 0
      003BE7 00                    3495 	.db #0x00	; 0
      003BE8 00                    3496 	.db #0x00	; 0
      003BE9 00                    3497 	.db #0x00	; 0
      003BEA 00                    3498 	.db #0x00	; 0
      003BEB 00                    3499 	.db #0x00	; 0
      003BEC 00                    3500 	.db #0x00	; 0
      003BED 00                    3501 	.db #0x00	; 0
      003BEE 00                    3502 	.db #0x00	; 0
      003BEF 00                    3503 	.db #0x00	; 0
      003BF0 00                    3504 	.db #0x00	; 0
      003BF1 00                    3505 	.db #0x00	; 0
      003BF2 00                    3506 	.db #0x00	; 0
      003BF3 40                    3507 	.db #0x40	; 64
      003BF4 20                    3508 	.db #0x20	; 32
      003BF5 20                    3509 	.db #0x20	; 32
      003BF6 20                    3510 	.db #0x20	; 32
      003BF7 C0                    3511 	.db #0xc0	; 192
      003BF8 00                    3512 	.db #0x00	; 0
      003BF9 00                    3513 	.db #0x00	; 0
      003BFA E0                    3514 	.db #0xe0	; 224
      003BFB 20                    3515 	.db #0x20	; 32
      003BFC 20                    3516 	.db #0x20	; 32
      003BFD 20                    3517 	.db #0x20	; 32
      003BFE E0                    3518 	.db #0xe0	; 224
      003BFF 00                    3519 	.db #0x00	; 0
      003C00 00                    3520 	.db #0x00	; 0
      003C01 00                    3521 	.db #0x00	; 0
      003C02 40                    3522 	.db #0x40	; 64
      003C03 E0                    3523 	.db #0xe0	; 224
      003C04 00                    3524 	.db #0x00	; 0
      003C05 00                    3525 	.db #0x00	; 0
      003C06 00                    3526 	.db #0x00	; 0
      003C07 00                    3527 	.db #0x00	; 0
      003C08 60                    3528 	.db #0x60	; 96
      003C09 20                    3529 	.db #0x20	; 32
      003C0A 20                    3530 	.db #0x20	; 32
      003C0B 20                    3531 	.db #0x20	; 32
      003C0C E0                    3532 	.db #0xe0	; 224
      003C0D 00                    3533 	.db #0x00	; 0
      003C0E 00                    3534 	.db #0x00	; 0
      003C0F 00                    3535 	.db #0x00	; 0
      003C10 00                    3536 	.db #0x00	; 0
      003C11 00                    3537 	.db #0x00	; 0
      003C12 E0                    3538 	.db #0xe0	; 224
      003C13 20                    3539 	.db #0x20	; 32
      003C14 20                    3540 	.db #0x20	; 32
      003C15 20                    3541 	.db #0x20	; 32
      003C16 E0                    3542 	.db #0xe0	; 224
      003C17 00                    3543 	.db #0x00	; 0
      003C18 00                    3544 	.db #0x00	; 0
      003C19 00                    3545 	.db #0x00	; 0
      003C1A 00                    3546 	.db #0x00	; 0
      003C1B 00                    3547 	.db #0x00	; 0
      003C1C 40                    3548 	.db #0x40	; 64
      003C1D 20                    3549 	.db #0x20	; 32
      003C1E 20                    3550 	.db #0x20	; 32
      003C1F 20                    3551 	.db #0x20	; 32
      003C20 C0                    3552 	.db #0xc0	; 192
      003C21 00                    3553 	.db #0x00	; 0
      003C22 00                    3554 	.db #0x00	; 0
      003C23 40                    3555 	.db #0x40	; 64
      003C24 20                    3556 	.db #0x20	; 32
      003C25 20                    3557 	.db #0x20	; 32
      003C26 20                    3558 	.db #0x20	; 32
      003C27 C0                    3559 	.db #0xc0	; 192
      003C28 00                    3560 	.db #0x00	; 0
      003C29 00                    3561 	.db #0x00	; 0
      003C2A 00                    3562 	.db #0x00	; 0
      003C2B 00                    3563 	.db #0x00	; 0
      003C2C 00                    3564 	.db #0x00	; 0
      003C2D 00                    3565 	.db #0x00	; 0
      003C2E 00                    3566 	.db #0x00	; 0
      003C2F 00                    3567 	.db #0x00	; 0
      003C30 00                    3568 	.db #0x00	; 0
      003C31 00                    3569 	.db #0x00	; 0
      003C32 00                    3570 	.db #0x00	; 0
      003C33 00                    3571 	.db #0x00	; 0
      003C34 00                    3572 	.db #0x00	; 0
      003C35 00                    3573 	.db #0x00	; 0
      003C36 00                    3574 	.db #0x00	; 0
      003C37 00                    3575 	.db #0x00	; 0
      003C38 00                    3576 	.db #0x00	; 0
      003C39 00                    3577 	.db #0x00	; 0
      003C3A 00                    3578 	.db #0x00	; 0
      003C3B 00                    3579 	.db #0x00	; 0
      003C3C 00                    3580 	.db #0x00	; 0
      003C3D 00                    3581 	.db #0x00	; 0
      003C3E 00                    3582 	.db #0x00	; 0
      003C3F 00                    3583 	.db #0x00	; 0
      003C40 00                    3584 	.db #0x00	; 0
      003C41 00                    3585 	.db #0x00	; 0
      003C42 00                    3586 	.db #0x00	; 0
      003C43 00                    3587 	.db #0x00	; 0
      003C44 00                    3588 	.db #0x00	; 0
      003C45 00                    3589 	.db #0x00	; 0
      003C46 00                    3590 	.db #0x00	; 0
      003C47 00                    3591 	.db #0x00	; 0
      003C48 00                    3592 	.db #0x00	; 0
      003C49 00                    3593 	.db #0x00	; 0
      003C4A 00                    3594 	.db #0x00	; 0
      003C4B 00                    3595 	.db #0x00	; 0
      003C4C 00                    3596 	.db #0x00	; 0
      003C4D 00                    3597 	.db #0x00	; 0
      003C4E 00                    3598 	.db #0x00	; 0
      003C4F 00                    3599 	.db #0x00	; 0
      003C50 00                    3600 	.db #0x00	; 0
      003C51 00                    3601 	.db #0x00	; 0
      003C52 00                    3602 	.db #0x00	; 0
      003C53 00                    3603 	.db #0x00	; 0
      003C54 00                    3604 	.db #0x00	; 0
      003C55 00                    3605 	.db #0x00	; 0
      003C56 00                    3606 	.db #0x00	; 0
      003C57 00                    3607 	.db #0x00	; 0
      003C58 00                    3608 	.db #0x00	; 0
      003C59 00                    3609 	.db #0x00	; 0
      003C5A 00                    3610 	.db #0x00	; 0
      003C5B 00                    3611 	.db #0x00	; 0
      003C5C 00                    3612 	.db #0x00	; 0
      003C5D 00                    3613 	.db #0x00	; 0
      003C5E 00                    3614 	.db #0x00	; 0
      003C5F 00                    3615 	.db #0x00	; 0
      003C60 00                    3616 	.db #0x00	; 0
      003C61 00                    3617 	.db #0x00	; 0
      003C62 00                    3618 	.db #0x00	; 0
      003C63 00                    3619 	.db #0x00	; 0
      003C64 00                    3620 	.db #0x00	; 0
      003C65 00                    3621 	.db #0x00	; 0
      003C66 00                    3622 	.db #0x00	; 0
      003C67 00                    3623 	.db #0x00	; 0
      003C68 00                    3624 	.db #0x00	; 0
      003C69 00                    3625 	.db #0x00	; 0
      003C6A 00                    3626 	.db #0x00	; 0
      003C6B 00                    3627 	.db #0x00	; 0
      003C6C 00                    3628 	.db #0x00	; 0
      003C6D 00                    3629 	.db #0x00	; 0
      003C6E 00                    3630 	.db #0x00	; 0
      003C6F 00                    3631 	.db #0x00	; 0
      003C70 00                    3632 	.db #0x00	; 0
      003C71 00                    3633 	.db #0x00	; 0
      003C72 00                    3634 	.db #0x00	; 0
      003C73 0C                    3635 	.db #0x0c	; 12
      003C74 0A                    3636 	.db #0x0a	; 10
      003C75 0A                    3637 	.db #0x0a	; 10
      003C76 09                    3638 	.db #0x09	; 9
      003C77 0C                    3639 	.db #0x0c	; 12
      003C78 00                    3640 	.db #0x00	; 0
      003C79 00                    3641 	.db #0x00	; 0
      003C7A 0F                    3642 	.db #0x0f	; 15
      003C7B 08                    3643 	.db #0x08	; 8
      003C7C 08                    3644 	.db #0x08	; 8
      003C7D 08                    3645 	.db #0x08	; 8
      003C7E 0F                    3646 	.db #0x0f	; 15
      003C7F 00                    3647 	.db #0x00	; 0
      003C80 00                    3648 	.db #0x00	; 0
      003C81 00                    3649 	.db #0x00	; 0
      003C82 08                    3650 	.db #0x08	; 8
      003C83 0F                    3651 	.db #0x0f	; 15
      003C84 08                    3652 	.db #0x08	; 8
      003C85 00                    3653 	.db #0x00	; 0
      003C86 00                    3654 	.db #0x00	; 0
      003C87 00                    3655 	.db #0x00	; 0
      003C88 0C                    3656 	.db #0x0c	; 12
      003C89 08                    3657 	.db #0x08	; 8
      003C8A 09                    3658 	.db #0x09	; 9
      003C8B 09                    3659 	.db #0x09	; 9
      003C8C 0E                    3660 	.db #0x0e	; 14
      003C8D 00                    3661 	.db #0x00	; 0
      003C8E 00                    3662 	.db #0x00	; 0
      003C8F 0C                    3663 	.db #0x0c	; 12
      003C90 00                    3664 	.db #0x00	; 0
      003C91 00                    3665 	.db #0x00	; 0
      003C92 0F                    3666 	.db #0x0f	; 15
      003C93 09                    3667 	.db #0x09	; 9
      003C94 09                    3668 	.db #0x09	; 9
      003C95 09                    3669 	.db #0x09	; 9
      003C96 0F                    3670 	.db #0x0f	; 15
      003C97 00                    3671 	.db #0x00	; 0
      003C98 00                    3672 	.db #0x00	; 0
      003C99 0C                    3673 	.db #0x0c	; 12
      003C9A 00                    3674 	.db #0x00	; 0
      003C9B 00                    3675 	.db #0x00	; 0
      003C9C 0C                    3676 	.db #0x0c	; 12
      003C9D 0A                    3677 	.db #0x0a	; 10
      003C9E 0A                    3678 	.db #0x0a	; 10
      003C9F 09                    3679 	.db #0x09	; 9
      003CA0 0C                    3680 	.db #0x0c	; 12
      003CA1 00                    3681 	.db #0x00	; 0
      003CA2 00                    3682 	.db #0x00	; 0
      003CA3 0C                    3683 	.db #0x0c	; 12
      003CA4 0A                    3684 	.db #0x0a	; 10
      003CA5 0A                    3685 	.db #0x0a	; 10
      003CA6 09                    3686 	.db #0x09	; 9
      003CA7 0C                    3687 	.db #0x0c	; 12
      003CA8 00                    3688 	.db #0x00	; 0
      003CA9 00                    3689 	.db #0x00	; 0
      003CAA 00                    3690 	.db #0x00	; 0
      003CAB 00                    3691 	.db #0x00	; 0
      003CAC 00                    3692 	.db #0x00	; 0
      003CAD 00                    3693 	.db #0x00	; 0
      003CAE 00                    3694 	.db #0x00	; 0
      003CAF 00                    3695 	.db #0x00	; 0
      003CB0 00                    3696 	.db #0x00	; 0
      003CB1 00                    3697 	.db #0x00	; 0
      003CB2 00                    3698 	.db #0x00	; 0
      003CB3 00                    3699 	.db #0x00	; 0
      003CB4 00                    3700 	.db #0x00	; 0
      003CB5 00                    3701 	.db #0x00	; 0
      003CB6 00                    3702 	.db #0x00	; 0
      003CB7 00                    3703 	.db #0x00	; 0
      003CB8 00                    3704 	.db #0x00	; 0
      003CB9 00                    3705 	.db #0x00	; 0
      003CBA 00                    3706 	.db #0x00	; 0
      003CBB 00                    3707 	.db #0x00	; 0
      003CBC 00                    3708 	.db #0x00	; 0
      003CBD 00                    3709 	.db #0x00	; 0
      003CBE 00                    3710 	.db #0x00	; 0
      003CBF 00                    3711 	.db #0x00	; 0
      003CC0 00                    3712 	.db #0x00	; 0
      003CC1 00                    3713 	.db #0x00	; 0
      003CC2 00                    3714 	.db #0x00	; 0
      003CC3 00                    3715 	.db #0x00	; 0
      003CC4 00                    3716 	.db #0x00	; 0
      003CC5 00                    3717 	.db #0x00	; 0
      003CC6 00                    3718 	.db #0x00	; 0
      003CC7 00                    3719 	.db #0x00	; 0
      003CC8 00                    3720 	.db #0x00	; 0
      003CC9 00                    3721 	.db #0x00	; 0
      003CCA 00                    3722 	.db #0x00	; 0
      003CCB 00                    3723 	.db #0x00	; 0
      003CCC 00                    3724 	.db #0x00	; 0
      003CCD 00                    3725 	.db #0x00	; 0
      003CCE 00                    3726 	.db #0x00	; 0
      003CCF 00                    3727 	.db #0x00	; 0
      003CD0 00                    3728 	.db #0x00	; 0
      003CD1 00                    3729 	.db #0x00	; 0
      003CD2 00                    3730 	.db #0x00	; 0
      003CD3 00                    3731 	.db #0x00	; 0
      003CD4 00                    3732 	.db #0x00	; 0
      003CD5 00                    3733 	.db #0x00	; 0
      003CD6 00                    3734 	.db #0x00	; 0
      003CD7 00                    3735 	.db #0x00	; 0
      003CD8 00                    3736 	.db #0x00	; 0
      003CD9 00                    3737 	.db #0x00	; 0
      003CDA 00                    3738 	.db #0x00	; 0
      003CDB 00                    3739 	.db #0x00	; 0
      003CDC 00                    3740 	.db #0x00	; 0
      003CDD 00                    3741 	.db #0x00	; 0
      003CDE 00                    3742 	.db #0x00	; 0
      003CDF 00                    3743 	.db #0x00	; 0
      003CE0 00                    3744 	.db #0x00	; 0
      003CE1 00                    3745 	.db #0x00	; 0
      003CE2 00                    3746 	.db #0x00	; 0
      003CE3 00                    3747 	.db #0x00	; 0
      003CE4 00                    3748 	.db #0x00	; 0
      003CE5 00                    3749 	.db #0x00	; 0
      003CE6 00                    3750 	.db #0x00	; 0
      003CE7 00                    3751 	.db #0x00	; 0
      003CE8 00                    3752 	.db #0x00	; 0
      003CE9 00                    3753 	.db #0x00	; 0
      003CEA 00                    3754 	.db #0x00	; 0
      003CEB 00                    3755 	.db #0x00	; 0
      003CEC 00                    3756 	.db #0x00	; 0
      003CED 00                    3757 	.db #0x00	; 0
      003CEE 00                    3758 	.db #0x00	; 0
      003CEF 00                    3759 	.db #0x00	; 0
      003CF0 00                    3760 	.db #0x00	; 0
      003CF1 00                    3761 	.db #0x00	; 0
      003CF2 00                    3762 	.db #0x00	; 0
      003CF3 00                    3763 	.db #0x00	; 0
      003CF4 00                    3764 	.db #0x00	; 0
      003CF5 00                    3765 	.db #0x00	; 0
      003CF6 00                    3766 	.db #0x00	; 0
      003CF7 00                    3767 	.db #0x00	; 0
      003CF8 00                    3768 	.db #0x00	; 0
      003CF9 00                    3769 	.db #0x00	; 0
      003CFA 00                    3770 	.db #0x00	; 0
      003CFB 00                    3771 	.db #0x00	; 0
      003CFC 00                    3772 	.db #0x00	; 0
      003CFD 00                    3773 	.db #0x00	; 0
      003CFE 00                    3774 	.db #0x00	; 0
      003CFF 00                    3775 	.db #0x00	; 0
      003D00 00                    3776 	.db #0x00	; 0
      003D01 00                    3777 	.db #0x00	; 0
      003D02 00                    3778 	.db #0x00	; 0
      003D03 00                    3779 	.db #0x00	; 0
      003D04 00                    3780 	.db #0x00	; 0
      003D05 00                    3781 	.db #0x00	; 0
      003D06 00                    3782 	.db #0x00	; 0
      003D07 00                    3783 	.db #0x00	; 0
      003D08 00                    3784 	.db #0x00	; 0
      003D09 00                    3785 	.db #0x00	; 0
      003D0A 80                    3786 	.db #0x80	; 128
      003D0B 80                    3787 	.db #0x80	; 128
      003D0C 80                    3788 	.db #0x80	; 128
      003D0D 80                    3789 	.db #0x80	; 128
      003D0E 80                    3790 	.db #0x80	; 128
      003D0F 80                    3791 	.db #0x80	; 128
      003D10 80                    3792 	.db #0x80	; 128
      003D11 80                    3793 	.db #0x80	; 128
      003D12 00                    3794 	.db #0x00	; 0
      003D13 00                    3795 	.db #0x00	; 0
      003D14 00                    3796 	.db #0x00	; 0
      003D15 00                    3797 	.db #0x00	; 0
      003D16 00                    3798 	.db #0x00	; 0
      003D17 00                    3799 	.db #0x00	; 0
      003D18 00                    3800 	.db #0x00	; 0
      003D19 00                    3801 	.db #0x00	; 0
      003D1A 00                    3802 	.db #0x00	; 0
      003D1B 00                    3803 	.db #0x00	; 0
      003D1C 00                    3804 	.db #0x00	; 0
      003D1D 00                    3805 	.db #0x00	; 0
      003D1E 00                    3806 	.db #0x00	; 0
      003D1F 00                    3807 	.db #0x00	; 0
      003D20 00                    3808 	.db #0x00	; 0
      003D21 00                    3809 	.db #0x00	; 0
      003D22 00                    3810 	.db #0x00	; 0
      003D23 00                    3811 	.db #0x00	; 0
      003D24 00                    3812 	.db #0x00	; 0
      003D25 00                    3813 	.db #0x00	; 0
      003D26 00                    3814 	.db #0x00	; 0
      003D27 00                    3815 	.db #0x00	; 0
      003D28 00                    3816 	.db #0x00	; 0
      003D29 00                    3817 	.db #0x00	; 0
      003D2A 00                    3818 	.db #0x00	; 0
      003D2B 00                    3819 	.db #0x00	; 0
      003D2C 00                    3820 	.db #0x00	; 0
      003D2D 00                    3821 	.db #0x00	; 0
      003D2E 00                    3822 	.db #0x00	; 0
      003D2F 00                    3823 	.db #0x00	; 0
      003D30 00                    3824 	.db #0x00	; 0
      003D31 00                    3825 	.db #0x00	; 0
      003D32 00                    3826 	.db #0x00	; 0
      003D33 00                    3827 	.db #0x00	; 0
      003D34 00                    3828 	.db #0x00	; 0
      003D35 00                    3829 	.db #0x00	; 0
      003D36 00                    3830 	.db #0x00	; 0
      003D37 00                    3831 	.db #0x00	; 0
      003D38 00                    3832 	.db #0x00	; 0
      003D39 00                    3833 	.db #0x00	; 0
      003D3A 00                    3834 	.db #0x00	; 0
      003D3B 00                    3835 	.db #0x00	; 0
      003D3C 00                    3836 	.db #0x00	; 0
      003D3D 00                    3837 	.db #0x00	; 0
      003D3E 00                    3838 	.db #0x00	; 0
      003D3F 00                    3839 	.db #0x00	; 0
      003D40 00                    3840 	.db #0x00	; 0
      003D41 00                    3841 	.db #0x00	; 0
      003D42 00                    3842 	.db #0x00	; 0
      003D43 00                    3843 	.db #0x00	; 0
      003D44 00                    3844 	.db #0x00	; 0
      003D45 00                    3845 	.db #0x00	; 0
      003D46 00                    3846 	.db #0x00	; 0
      003D47 00                    3847 	.db #0x00	; 0
      003D48 00                    3848 	.db #0x00	; 0
      003D49 00                    3849 	.db #0x00	; 0
      003D4A 00                    3850 	.db #0x00	; 0
      003D4B 00                    3851 	.db #0x00	; 0
      003D4C 00                    3852 	.db #0x00	; 0
      003D4D 00                    3853 	.db #0x00	; 0
      003D4E 00                    3854 	.db #0x00	; 0
      003D4F 7F                    3855 	.db #0x7f	; 127
      003D50 03                    3856 	.db #0x03	; 3
      003D51 0C                    3857 	.db #0x0c	; 12
      003D52 30                    3858 	.db #0x30	; 48	'0'
      003D53 0C                    3859 	.db #0x0c	; 12
      003D54 03                    3860 	.db #0x03	; 3
      003D55 7F                    3861 	.db #0x7f	; 127
      003D56 00                    3862 	.db #0x00	; 0
      003D57 00                    3863 	.db #0x00	; 0
      003D58 38                    3864 	.db #0x38	; 56	'8'
      003D59 54                    3865 	.db #0x54	; 84	'T'
      003D5A 54                    3866 	.db #0x54	; 84	'T'
      003D5B 58                    3867 	.db #0x58	; 88	'X'
      003D5C 00                    3868 	.db #0x00	; 0
      003D5D 00                    3869 	.db #0x00	; 0
      003D5E 7C                    3870 	.db #0x7c	; 124
      003D5F 04                    3871 	.db #0x04	; 4
      003D60 04                    3872 	.db #0x04	; 4
      003D61 78                    3873 	.db #0x78	; 120	'x'
      003D62 00                    3874 	.db #0x00	; 0
      003D63 00                    3875 	.db #0x00	; 0
      003D64 3C                    3876 	.db #0x3c	; 60
      003D65 40                    3877 	.db #0x40	; 64
      003D66 40                    3878 	.db #0x40	; 64
      003D67 7C                    3879 	.db #0x7c	; 124
      003D68 00                    3880 	.db #0x00	; 0
      003D69 00                    3881 	.db #0x00	; 0
      003D6A 00                    3882 	.db #0x00	; 0
      003D6B 00                    3883 	.db #0x00	; 0
      003D6C 00                    3884 	.db #0x00	; 0
      003D6D 00                    3885 	.db #0x00	; 0
      003D6E 00                    3886 	.db #0x00	; 0
      003D6F 00                    3887 	.db #0x00	; 0
      003D70 00                    3888 	.db #0x00	; 0
      003D71 00                    3889 	.db #0x00	; 0
      003D72 00                    3890 	.db #0x00	; 0
      003D73 00                    3891 	.db #0x00	; 0
      003D74 00                    3892 	.db #0x00	; 0
      003D75 00                    3893 	.db #0x00	; 0
      003D76 00                    3894 	.db #0x00	; 0
      003D77 00                    3895 	.db #0x00	; 0
      003D78 00                    3896 	.db #0x00	; 0
      003D79 00                    3897 	.db #0x00	; 0
      003D7A 00                    3898 	.db #0x00	; 0
      003D7B 00                    3899 	.db #0x00	; 0
      003D7C 00                    3900 	.db #0x00	; 0
      003D7D 00                    3901 	.db #0x00	; 0
      003D7E 00                    3902 	.db #0x00	; 0
      003D7F 00                    3903 	.db #0x00	; 0
      003D80 00                    3904 	.db #0x00	; 0
      003D81 00                    3905 	.db #0x00	; 0
      003D82 00                    3906 	.db #0x00	; 0
      003D83 00                    3907 	.db #0x00	; 0
      003D84 00                    3908 	.db #0x00	; 0
      003D85 00                    3909 	.db #0x00	; 0
      003D86 00                    3910 	.db #0x00	; 0
      003D87 00                    3911 	.db #0x00	; 0
      003D88 00                    3912 	.db #0x00	; 0
      003D89 00                    3913 	.db #0x00	; 0
      003D8A FF                    3914 	.db #0xff	; 255
      003D8B AA                    3915 	.db #0xaa	; 170
      003D8C AA                    3916 	.db #0xaa	; 170
      003D8D AA                    3917 	.db #0xaa	; 170
      003D8E 28                    3918 	.db #0x28	; 40
      003D8F 08                    3919 	.db #0x08	; 8
      003D90 00                    3920 	.db #0x00	; 0
      003D91 FF                    3921 	.db #0xff	; 255
      003D92 00                    3922 	.db #0x00	; 0
      003D93 00                    3923 	.db #0x00	; 0
      003D94 00                    3924 	.db #0x00	; 0
      003D95 00                    3925 	.db #0x00	; 0
      003D96 00                    3926 	.db #0x00	; 0
      003D97 00                    3927 	.db #0x00	; 0
      003D98 00                    3928 	.db #0x00	; 0
      003D99 00                    3929 	.db #0x00	; 0
      003D9A 00                    3930 	.db #0x00	; 0
      003D9B 00                    3931 	.db #0x00	; 0
      003D9C 00                    3932 	.db #0x00	; 0
      003D9D 00                    3933 	.db #0x00	; 0
      003D9E 00                    3934 	.db #0x00	; 0
      003D9F 00                    3935 	.db #0x00	; 0
      003DA0 00                    3936 	.db #0x00	; 0
      003DA1 00                    3937 	.db #0x00	; 0
      003DA2 00                    3938 	.db #0x00	; 0
      003DA3 00                    3939 	.db #0x00	; 0
      003DA4 00                    3940 	.db #0x00	; 0
      003DA5 00                    3941 	.db #0x00	; 0
      003DA6 00                    3942 	.db #0x00	; 0
      003DA7 00                    3943 	.db #0x00	; 0
      003DA8 00                    3944 	.db #0x00	; 0
      003DA9 00                    3945 	.db #0x00	; 0
      003DAA 00                    3946 	.db #0x00	; 0
      003DAB 00                    3947 	.db #0x00	; 0
      003DAC 00                    3948 	.db #0x00	; 0
      003DAD 00                    3949 	.db #0x00	; 0
      003DAE 00                    3950 	.db #0x00	; 0
      003DAF 00                    3951 	.db #0x00	; 0
      003DB0 00                    3952 	.db #0x00	; 0
      003DB1 00                    3953 	.db #0x00	; 0
      003DB2 00                    3954 	.db #0x00	; 0
      003DB3 00                    3955 	.db #0x00	; 0
      003DB4 00                    3956 	.db #0x00	; 0
      003DB5 00                    3957 	.db #0x00	; 0
      003DB6 00                    3958 	.db #0x00	; 0
      003DB7 7F                    3959 	.db #0x7f	; 127
      003DB8 03                    3960 	.db #0x03	; 3
      003DB9 0C                    3961 	.db #0x0c	; 12
      003DBA 30                    3962 	.db #0x30	; 48	'0'
      003DBB 0C                    3963 	.db #0x0c	; 12
      003DBC 03                    3964 	.db #0x03	; 3
      003DBD 7F                    3965 	.db #0x7f	; 127
      003DBE 00                    3966 	.db #0x00	; 0
      003DBF 00                    3967 	.db #0x00	; 0
      003DC0 26                    3968 	.db #0x26	; 38
      003DC1 49                    3969 	.db #0x49	; 73	'I'
      003DC2 49                    3970 	.db #0x49	; 73	'I'
      003DC3 49                    3971 	.db #0x49	; 73	'I'
      003DC4 32                    3972 	.db #0x32	; 50	'2'
      003DC5 00                    3973 	.db #0x00	; 0
      003DC6 00                    3974 	.db #0x00	; 0
      003DC7 7F                    3975 	.db #0x7f	; 127
      003DC8 02                    3976 	.db #0x02	; 2
      003DC9 04                    3977 	.db #0x04	; 4
      003DCA 08                    3978 	.db #0x08	; 8
      003DCB 10                    3979 	.db #0x10	; 16
      003DCC 7F                    3980 	.db #0x7f	; 127
      003DCD 00                    3981 	.db #0x00	; 0
                                   3982 	.area CONST   (CODE)
      003DCE                       3983 ___str_0:
      003DCE 52 20 3A 20 25 75 20  3984 	.ascii "R : %u    "
             20 20 20
      003DD8 00                    3985 	.db 0x00
                                   3986 	.area CSEG    (CODE)
                                   3987 	.area CONST   (CODE)
      003DD9                       3988 ___str_1:
      003DD9 47 20 3A 20 25 75 20  3989 	.ascii "G : %u    "
             20 20 20
      003DE3 00                    3990 	.db 0x00
                                   3991 	.area CSEG    (CODE)
                                   3992 	.area CONST   (CODE)
      003DE4                       3993 ___str_2:
      003DE4 42 20 3A 20 25 75 20  3994 	.ascii "B : %u    "
             20 20 20
      003DEE 00                    3995 	.db 0x00
                                   3996 	.area CSEG    (CODE)
                                   3997 	.area CONST   (CODE)
      003DEF                       3998 ___str_3:
      003DEF 52 47 42 2E 43 20 3A  3999 	.ascii "RGB.C : %u    "
             20 25 75 20 20 20 20
      003DFD 00                    4000 	.db 0x00
                                   4001 	.area CSEG    (CODE)
                                   4002 	.area CABS    (ABS,CODE)
