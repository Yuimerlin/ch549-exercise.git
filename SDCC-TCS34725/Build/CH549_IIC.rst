                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 4.0.0 #11528 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module CH549_IIC
                                      6 	.optsdcc -mmcs51 --model-large
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _GPIO_Init
                                     12 	.globl _mDelayuS
                                     13 	.globl _UIF_BUS_RST
                                     14 	.globl _UIF_DETECT
                                     15 	.globl _UIF_TRANSFER
                                     16 	.globl _UIF_SUSPEND
                                     17 	.globl _UIF_HST_SOF
                                     18 	.globl _UIF_FIFO_OV
                                     19 	.globl _U_SIE_FREE
                                     20 	.globl _U_TOG_OK
                                     21 	.globl _U_IS_NAK
                                     22 	.globl _S0_R_FIFO
                                     23 	.globl _S0_T_FIFO
                                     24 	.globl _S0_FREE
                                     25 	.globl _S0_IF_BYTE
                                     26 	.globl _S0_IF_FIRST
                                     27 	.globl _S0_IF_OV
                                     28 	.globl _S0_FST_ACT
                                     29 	.globl _CP_RL2
                                     30 	.globl _C_T2
                                     31 	.globl _TR2
                                     32 	.globl _EXEN2
                                     33 	.globl _TCLK
                                     34 	.globl _RCLK
                                     35 	.globl _EXF2
                                     36 	.globl _CAP1F
                                     37 	.globl _TF2
                                     38 	.globl _RI
                                     39 	.globl _TI
                                     40 	.globl _RB8
                                     41 	.globl _TB8
                                     42 	.globl _REN
                                     43 	.globl _SM2
                                     44 	.globl _SM1
                                     45 	.globl _SM0
                                     46 	.globl _IT0
                                     47 	.globl _IE0
                                     48 	.globl _IT1
                                     49 	.globl _IE1
                                     50 	.globl _TR0
                                     51 	.globl _TF0
                                     52 	.globl _TR1
                                     53 	.globl _TF1
                                     54 	.globl _XI
                                     55 	.globl _XO
                                     56 	.globl _P4_0
                                     57 	.globl _P4_1
                                     58 	.globl _P4_2
                                     59 	.globl _P4_3
                                     60 	.globl _P4_4
                                     61 	.globl _P4_5
                                     62 	.globl _P4_6
                                     63 	.globl _RXD
                                     64 	.globl _TXD
                                     65 	.globl _INT0
                                     66 	.globl _INT1
                                     67 	.globl _T0
                                     68 	.globl _T1
                                     69 	.globl _CAP0
                                     70 	.globl _INT3
                                     71 	.globl _P3_0
                                     72 	.globl _P3_1
                                     73 	.globl _P3_2
                                     74 	.globl _P3_3
                                     75 	.globl _P3_4
                                     76 	.globl _P3_5
                                     77 	.globl _P3_6
                                     78 	.globl _P3_7
                                     79 	.globl _PWM5
                                     80 	.globl _PWM4
                                     81 	.globl _INT0_
                                     82 	.globl _PWM3
                                     83 	.globl _PWM2
                                     84 	.globl _CAP1_
                                     85 	.globl _T2_
                                     86 	.globl _PWM1
                                     87 	.globl _CAP2_
                                     88 	.globl _T2EX_
                                     89 	.globl _PWM0
                                     90 	.globl _RXD1
                                     91 	.globl _PWM6
                                     92 	.globl _TXD1
                                     93 	.globl _PWM7
                                     94 	.globl _P2_0
                                     95 	.globl _P2_1
                                     96 	.globl _P2_2
                                     97 	.globl _P2_3
                                     98 	.globl _P2_4
                                     99 	.globl _P2_5
                                    100 	.globl _P2_6
                                    101 	.globl _P2_7
                                    102 	.globl _AIN0
                                    103 	.globl _CAP1
                                    104 	.globl _T2
                                    105 	.globl _AIN1
                                    106 	.globl _CAP2
                                    107 	.globl _T2EX
                                    108 	.globl _AIN2
                                    109 	.globl _AIN3
                                    110 	.globl _AIN4
                                    111 	.globl _UCC1
                                    112 	.globl _SCS
                                    113 	.globl _AIN5
                                    114 	.globl _UCC2
                                    115 	.globl _PWM0_
                                    116 	.globl _MOSI
                                    117 	.globl _AIN6
                                    118 	.globl _VBUS
                                    119 	.globl _RXD1_
                                    120 	.globl _MISO
                                    121 	.globl _AIN7
                                    122 	.globl _TXD1_
                                    123 	.globl _SCK
                                    124 	.globl _P1_0
                                    125 	.globl _P1_1
                                    126 	.globl _P1_2
                                    127 	.globl _P1_3
                                    128 	.globl _P1_4
                                    129 	.globl _P1_5
                                    130 	.globl _P1_6
                                    131 	.globl _P1_7
                                    132 	.globl _AIN8
                                    133 	.globl _AIN9
                                    134 	.globl _AIN10
                                    135 	.globl _RXD_
                                    136 	.globl _AIN11
                                    137 	.globl _TXD_
                                    138 	.globl _AIN12
                                    139 	.globl _RXD2
                                    140 	.globl _AIN13
                                    141 	.globl _TXD2
                                    142 	.globl _AIN14
                                    143 	.globl _RXD3
                                    144 	.globl _AIN15
                                    145 	.globl _TXD3
                                    146 	.globl _P0_0
                                    147 	.globl _P0_1
                                    148 	.globl _P0_2
                                    149 	.globl _P0_3
                                    150 	.globl _P0_4
                                    151 	.globl _P0_5
                                    152 	.globl _P0_6
                                    153 	.globl _P0_7
                                    154 	.globl _IE_SPI0
                                    155 	.globl _IE_INT3
                                    156 	.globl _IE_USB
                                    157 	.globl _IE_UART2
                                    158 	.globl _IE_ADC
                                    159 	.globl _IE_UART1
                                    160 	.globl _IE_UART3
                                    161 	.globl _IE_PWMX
                                    162 	.globl _IE_GPIO
                                    163 	.globl _IE_WDOG
                                    164 	.globl _PX0
                                    165 	.globl _PT0
                                    166 	.globl _PX1
                                    167 	.globl _PT1
                                    168 	.globl _PS
                                    169 	.globl _PT2
                                    170 	.globl _PL_FLAG
                                    171 	.globl _PH_FLAG
                                    172 	.globl _EX0
                                    173 	.globl _ET0
                                    174 	.globl _EX1
                                    175 	.globl _ET1
                                    176 	.globl _ES
                                    177 	.globl _ET2
                                    178 	.globl _E_DIS
                                    179 	.globl _EA
                                    180 	.globl _P
                                    181 	.globl _F1
                                    182 	.globl _OV
                                    183 	.globl _RS0
                                    184 	.globl _RS1
                                    185 	.globl _F0
                                    186 	.globl _AC
                                    187 	.globl _CY
                                    188 	.globl _UEP1_DMA_H
                                    189 	.globl _UEP1_DMA_L
                                    190 	.globl _UEP1_DMA
                                    191 	.globl _UEP0_DMA_H
                                    192 	.globl _UEP0_DMA_L
                                    193 	.globl _UEP0_DMA
                                    194 	.globl _UEP2_3_MOD
                                    195 	.globl _UEP4_1_MOD
                                    196 	.globl _UEP3_DMA_H
                                    197 	.globl _UEP3_DMA_L
                                    198 	.globl _UEP3_DMA
                                    199 	.globl _UEP2_DMA_H
                                    200 	.globl _UEP2_DMA_L
                                    201 	.globl _UEP2_DMA
                                    202 	.globl _USB_DEV_AD
                                    203 	.globl _USB_CTRL
                                    204 	.globl _USB_INT_EN
                                    205 	.globl _UEP4_T_LEN
                                    206 	.globl _UEP4_CTRL
                                    207 	.globl _UEP0_T_LEN
                                    208 	.globl _UEP0_CTRL
                                    209 	.globl _USB_RX_LEN
                                    210 	.globl _USB_MIS_ST
                                    211 	.globl _USB_INT_ST
                                    212 	.globl _USB_INT_FG
                                    213 	.globl _UEP3_T_LEN
                                    214 	.globl _UEP3_CTRL
                                    215 	.globl _UEP2_T_LEN
                                    216 	.globl _UEP2_CTRL
                                    217 	.globl _UEP1_T_LEN
                                    218 	.globl _UEP1_CTRL
                                    219 	.globl _UDEV_CTRL
                                    220 	.globl _USB_C_CTRL
                                    221 	.globl _ADC_PIN
                                    222 	.globl _ADC_CHAN
                                    223 	.globl _ADC_DAT_H
                                    224 	.globl _ADC_DAT_L
                                    225 	.globl _ADC_DAT
                                    226 	.globl _ADC_CFG
                                    227 	.globl _ADC_CTRL
                                    228 	.globl _TKEY_CTRL
                                    229 	.globl _SIF3
                                    230 	.globl _SBAUD3
                                    231 	.globl _SBUF3
                                    232 	.globl _SCON3
                                    233 	.globl _SIF2
                                    234 	.globl _SBAUD2
                                    235 	.globl _SBUF2
                                    236 	.globl _SCON2
                                    237 	.globl _SIF1
                                    238 	.globl _SBAUD1
                                    239 	.globl _SBUF1
                                    240 	.globl _SCON1
                                    241 	.globl _SPI0_SETUP
                                    242 	.globl _SPI0_CK_SE
                                    243 	.globl _SPI0_CTRL
                                    244 	.globl _SPI0_DATA
                                    245 	.globl _SPI0_STAT
                                    246 	.globl _PWM_DATA7
                                    247 	.globl _PWM_DATA6
                                    248 	.globl _PWM_DATA5
                                    249 	.globl _PWM_DATA4
                                    250 	.globl _PWM_DATA3
                                    251 	.globl _PWM_CTRL2
                                    252 	.globl _PWM_CK_SE
                                    253 	.globl _PWM_CTRL
                                    254 	.globl _PWM_DATA0
                                    255 	.globl _PWM_DATA1
                                    256 	.globl _PWM_DATA2
                                    257 	.globl _T2CAP1H
                                    258 	.globl _T2CAP1L
                                    259 	.globl _T2CAP1
                                    260 	.globl _TH2
                                    261 	.globl _TL2
                                    262 	.globl _T2COUNT
                                    263 	.globl _RCAP2H
                                    264 	.globl _RCAP2L
                                    265 	.globl _RCAP2
                                    266 	.globl _T2MOD
                                    267 	.globl _T2CON
                                    268 	.globl _T2CAP0H
                                    269 	.globl _T2CAP0L
                                    270 	.globl _T2CAP0
                                    271 	.globl _T2CON2
                                    272 	.globl _SBUF
                                    273 	.globl _SCON
                                    274 	.globl _TH1
                                    275 	.globl _TH0
                                    276 	.globl _TL1
                                    277 	.globl _TL0
                                    278 	.globl _TMOD
                                    279 	.globl _TCON
                                    280 	.globl _XBUS_AUX
                                    281 	.globl _PIN_FUNC
                                    282 	.globl _P5
                                    283 	.globl _P4_DIR_PU
                                    284 	.globl _P4_MOD_OC
                                    285 	.globl _P4
                                    286 	.globl _P3_DIR_PU
                                    287 	.globl _P3_MOD_OC
                                    288 	.globl _P3
                                    289 	.globl _P2_DIR_PU
                                    290 	.globl _P2_MOD_OC
                                    291 	.globl _P2
                                    292 	.globl _P1_DIR_PU
                                    293 	.globl _P1_MOD_OC
                                    294 	.globl _P1
                                    295 	.globl _P0_DIR_PU
                                    296 	.globl _P0_MOD_OC
                                    297 	.globl _P0
                                    298 	.globl _ROM_CTRL
                                    299 	.globl _ROM_DATA_HH
                                    300 	.globl _ROM_DATA_HL
                                    301 	.globl _ROM_DATA_HI
                                    302 	.globl _ROM_ADDR_H
                                    303 	.globl _ROM_ADDR_L
                                    304 	.globl _ROM_ADDR
                                    305 	.globl _GPIO_IE
                                    306 	.globl _INTX
                                    307 	.globl _IP_EX
                                    308 	.globl _IE_EX
                                    309 	.globl _IP
                                    310 	.globl _IE
                                    311 	.globl _WDOG_COUNT
                                    312 	.globl _RESET_KEEP
                                    313 	.globl _WAKE_CTRL
                                    314 	.globl _CLOCK_CFG
                                    315 	.globl _POWER_CFG
                                    316 	.globl _PCON
                                    317 	.globl _GLOBAL_CFG
                                    318 	.globl _SAFE_MOD
                                    319 	.globl _DPH
                                    320 	.globl _DPL
                                    321 	.globl _SP
                                    322 	.globl _A_INV
                                    323 	.globl _B
                                    324 	.globl _ACC
                                    325 	.globl _PSW
                                    326 	.globl _IIC_Init
                                    327 	.globl _IIC_Start
                                    328 	.globl _IIC_Stop
                                    329 	.globl _IIC_Wait_Ack
                                    330 	.globl _IIC_Nack
                                    331 	.globl _IIC_Ack
                                    332 	.globl _IIC_Send_Byte
                                    333 	.globl _IIC_Read_Byte
                                    334 ;--------------------------------------------------------
                                    335 ; special function registers
                                    336 ;--------------------------------------------------------
                                    337 	.area RSEG    (ABS,DATA)
      000000                        338 	.org 0x0000
                           0000D0   339 _PSW	=	0x00d0
                           0000E0   340 _ACC	=	0x00e0
                           0000F0   341 _B	=	0x00f0
                           0000FD   342 _A_INV	=	0x00fd
                           000081   343 _SP	=	0x0081
                           000082   344 _DPL	=	0x0082
                           000083   345 _DPH	=	0x0083
                           0000A1   346 _SAFE_MOD	=	0x00a1
                           0000B1   347 _GLOBAL_CFG	=	0x00b1
                           000087   348 _PCON	=	0x0087
                           0000BA   349 _POWER_CFG	=	0x00ba
                           0000B9   350 _CLOCK_CFG	=	0x00b9
                           0000A9   351 _WAKE_CTRL	=	0x00a9
                           0000FE   352 _RESET_KEEP	=	0x00fe
                           0000FF   353 _WDOG_COUNT	=	0x00ff
                           0000A8   354 _IE	=	0x00a8
                           0000B8   355 _IP	=	0x00b8
                           0000E8   356 _IE_EX	=	0x00e8
                           0000E9   357 _IP_EX	=	0x00e9
                           0000B3   358 _INTX	=	0x00b3
                           0000B2   359 _GPIO_IE	=	0x00b2
                           008584   360 _ROM_ADDR	=	0x8584
                           000084   361 _ROM_ADDR_L	=	0x0084
                           000085   362 _ROM_ADDR_H	=	0x0085
                           008F8E   363 _ROM_DATA_HI	=	0x8f8e
                           00008E   364 _ROM_DATA_HL	=	0x008e
                           00008F   365 _ROM_DATA_HH	=	0x008f
                           000086   366 _ROM_CTRL	=	0x0086
                           000080   367 _P0	=	0x0080
                           0000C4   368 _P0_MOD_OC	=	0x00c4
                           0000C5   369 _P0_DIR_PU	=	0x00c5
                           000090   370 _P1	=	0x0090
                           000092   371 _P1_MOD_OC	=	0x0092
                           000093   372 _P1_DIR_PU	=	0x0093
                           0000A0   373 _P2	=	0x00a0
                           000094   374 _P2_MOD_OC	=	0x0094
                           000095   375 _P2_DIR_PU	=	0x0095
                           0000B0   376 _P3	=	0x00b0
                           000096   377 _P3_MOD_OC	=	0x0096
                           000097   378 _P3_DIR_PU	=	0x0097
                           0000C0   379 _P4	=	0x00c0
                           0000C2   380 _P4_MOD_OC	=	0x00c2
                           0000C3   381 _P4_DIR_PU	=	0x00c3
                           0000AB   382 _P5	=	0x00ab
                           0000AA   383 _PIN_FUNC	=	0x00aa
                           0000A2   384 _XBUS_AUX	=	0x00a2
                           000088   385 _TCON	=	0x0088
                           000089   386 _TMOD	=	0x0089
                           00008A   387 _TL0	=	0x008a
                           00008B   388 _TL1	=	0x008b
                           00008C   389 _TH0	=	0x008c
                           00008D   390 _TH1	=	0x008d
                           000098   391 _SCON	=	0x0098
                           000099   392 _SBUF	=	0x0099
                           0000C1   393 _T2CON2	=	0x00c1
                           00C7C6   394 _T2CAP0	=	0xc7c6
                           0000C6   395 _T2CAP0L	=	0x00c6
                           0000C7   396 _T2CAP0H	=	0x00c7
                           0000C8   397 _T2CON	=	0x00c8
                           0000C9   398 _T2MOD	=	0x00c9
                           00CBCA   399 _RCAP2	=	0xcbca
                           0000CA   400 _RCAP2L	=	0x00ca
                           0000CB   401 _RCAP2H	=	0x00cb
                           00CDCC   402 _T2COUNT	=	0xcdcc
                           0000CC   403 _TL2	=	0x00cc
                           0000CD   404 _TH2	=	0x00cd
                           00CFCE   405 _T2CAP1	=	0xcfce
                           0000CE   406 _T2CAP1L	=	0x00ce
                           0000CF   407 _T2CAP1H	=	0x00cf
                           00009A   408 _PWM_DATA2	=	0x009a
                           00009B   409 _PWM_DATA1	=	0x009b
                           00009C   410 _PWM_DATA0	=	0x009c
                           00009D   411 _PWM_CTRL	=	0x009d
                           00009E   412 _PWM_CK_SE	=	0x009e
                           00009F   413 _PWM_CTRL2	=	0x009f
                           0000A3   414 _PWM_DATA3	=	0x00a3
                           0000A4   415 _PWM_DATA4	=	0x00a4
                           0000A5   416 _PWM_DATA5	=	0x00a5
                           0000A6   417 _PWM_DATA6	=	0x00a6
                           0000A7   418 _PWM_DATA7	=	0x00a7
                           0000F8   419 _SPI0_STAT	=	0x00f8
                           0000F9   420 _SPI0_DATA	=	0x00f9
                           0000FA   421 _SPI0_CTRL	=	0x00fa
                           0000FB   422 _SPI0_CK_SE	=	0x00fb
                           0000FC   423 _SPI0_SETUP	=	0x00fc
                           0000BC   424 _SCON1	=	0x00bc
                           0000BD   425 _SBUF1	=	0x00bd
                           0000BE   426 _SBAUD1	=	0x00be
                           0000BF   427 _SIF1	=	0x00bf
                           0000B4   428 _SCON2	=	0x00b4
                           0000B5   429 _SBUF2	=	0x00b5
                           0000B6   430 _SBAUD2	=	0x00b6
                           0000B7   431 _SIF2	=	0x00b7
                           0000AC   432 _SCON3	=	0x00ac
                           0000AD   433 _SBUF3	=	0x00ad
                           0000AE   434 _SBAUD3	=	0x00ae
                           0000AF   435 _SIF3	=	0x00af
                           0000F1   436 _TKEY_CTRL	=	0x00f1
                           0000F2   437 _ADC_CTRL	=	0x00f2
                           0000F3   438 _ADC_CFG	=	0x00f3
                           00F5F4   439 _ADC_DAT	=	0xf5f4
                           0000F4   440 _ADC_DAT_L	=	0x00f4
                           0000F5   441 _ADC_DAT_H	=	0x00f5
                           0000F6   442 _ADC_CHAN	=	0x00f6
                           0000F7   443 _ADC_PIN	=	0x00f7
                           000091   444 _USB_C_CTRL	=	0x0091
                           0000D1   445 _UDEV_CTRL	=	0x00d1
                           0000D2   446 _UEP1_CTRL	=	0x00d2
                           0000D3   447 _UEP1_T_LEN	=	0x00d3
                           0000D4   448 _UEP2_CTRL	=	0x00d4
                           0000D5   449 _UEP2_T_LEN	=	0x00d5
                           0000D6   450 _UEP3_CTRL	=	0x00d6
                           0000D7   451 _UEP3_T_LEN	=	0x00d7
                           0000D8   452 _USB_INT_FG	=	0x00d8
                           0000D9   453 _USB_INT_ST	=	0x00d9
                           0000DA   454 _USB_MIS_ST	=	0x00da
                           0000DB   455 _USB_RX_LEN	=	0x00db
                           0000DC   456 _UEP0_CTRL	=	0x00dc
                           0000DD   457 _UEP0_T_LEN	=	0x00dd
                           0000DE   458 _UEP4_CTRL	=	0x00de
                           0000DF   459 _UEP4_T_LEN	=	0x00df
                           0000E1   460 _USB_INT_EN	=	0x00e1
                           0000E2   461 _USB_CTRL	=	0x00e2
                           0000E3   462 _USB_DEV_AD	=	0x00e3
                           00E5E4   463 _UEP2_DMA	=	0xe5e4
                           0000E4   464 _UEP2_DMA_L	=	0x00e4
                           0000E5   465 _UEP2_DMA_H	=	0x00e5
                           00E7E6   466 _UEP3_DMA	=	0xe7e6
                           0000E6   467 _UEP3_DMA_L	=	0x00e6
                           0000E7   468 _UEP3_DMA_H	=	0x00e7
                           0000EA   469 _UEP4_1_MOD	=	0x00ea
                           0000EB   470 _UEP2_3_MOD	=	0x00eb
                           00EDEC   471 _UEP0_DMA	=	0xedec
                           0000EC   472 _UEP0_DMA_L	=	0x00ec
                           0000ED   473 _UEP0_DMA_H	=	0x00ed
                           00EFEE   474 _UEP1_DMA	=	0xefee
                           0000EE   475 _UEP1_DMA_L	=	0x00ee
                           0000EF   476 _UEP1_DMA_H	=	0x00ef
                                    477 ;--------------------------------------------------------
                                    478 ; special function bits
                                    479 ;--------------------------------------------------------
                                    480 	.area RSEG    (ABS,DATA)
      000000                        481 	.org 0x0000
                           0000D7   482 _CY	=	0x00d7
                           0000D6   483 _AC	=	0x00d6
                           0000D5   484 _F0	=	0x00d5
                           0000D4   485 _RS1	=	0x00d4
                           0000D3   486 _RS0	=	0x00d3
                           0000D2   487 _OV	=	0x00d2
                           0000D1   488 _F1	=	0x00d1
                           0000D0   489 _P	=	0x00d0
                           0000AF   490 _EA	=	0x00af
                           0000AE   491 _E_DIS	=	0x00ae
                           0000AD   492 _ET2	=	0x00ad
                           0000AC   493 _ES	=	0x00ac
                           0000AB   494 _ET1	=	0x00ab
                           0000AA   495 _EX1	=	0x00aa
                           0000A9   496 _ET0	=	0x00a9
                           0000A8   497 _EX0	=	0x00a8
                           0000BF   498 _PH_FLAG	=	0x00bf
                           0000BE   499 _PL_FLAG	=	0x00be
                           0000BD   500 _PT2	=	0x00bd
                           0000BC   501 _PS	=	0x00bc
                           0000BB   502 _PT1	=	0x00bb
                           0000BA   503 _PX1	=	0x00ba
                           0000B9   504 _PT0	=	0x00b9
                           0000B8   505 _PX0	=	0x00b8
                           0000EF   506 _IE_WDOG	=	0x00ef
                           0000EE   507 _IE_GPIO	=	0x00ee
                           0000ED   508 _IE_PWMX	=	0x00ed
                           0000ED   509 _IE_UART3	=	0x00ed
                           0000EC   510 _IE_UART1	=	0x00ec
                           0000EB   511 _IE_ADC	=	0x00eb
                           0000EB   512 _IE_UART2	=	0x00eb
                           0000EA   513 _IE_USB	=	0x00ea
                           0000E9   514 _IE_INT3	=	0x00e9
                           0000E8   515 _IE_SPI0	=	0x00e8
                           000087   516 _P0_7	=	0x0087
                           000086   517 _P0_6	=	0x0086
                           000085   518 _P0_5	=	0x0085
                           000084   519 _P0_4	=	0x0084
                           000083   520 _P0_3	=	0x0083
                           000082   521 _P0_2	=	0x0082
                           000081   522 _P0_1	=	0x0081
                           000080   523 _P0_0	=	0x0080
                           000087   524 _TXD3	=	0x0087
                           000087   525 _AIN15	=	0x0087
                           000086   526 _RXD3	=	0x0086
                           000086   527 _AIN14	=	0x0086
                           000085   528 _TXD2	=	0x0085
                           000085   529 _AIN13	=	0x0085
                           000084   530 _RXD2	=	0x0084
                           000084   531 _AIN12	=	0x0084
                           000083   532 _TXD_	=	0x0083
                           000083   533 _AIN11	=	0x0083
                           000082   534 _RXD_	=	0x0082
                           000082   535 _AIN10	=	0x0082
                           000081   536 _AIN9	=	0x0081
                           000080   537 _AIN8	=	0x0080
                           000097   538 _P1_7	=	0x0097
                           000096   539 _P1_6	=	0x0096
                           000095   540 _P1_5	=	0x0095
                           000094   541 _P1_4	=	0x0094
                           000093   542 _P1_3	=	0x0093
                           000092   543 _P1_2	=	0x0092
                           000091   544 _P1_1	=	0x0091
                           000090   545 _P1_0	=	0x0090
                           000097   546 _SCK	=	0x0097
                           000097   547 _TXD1_	=	0x0097
                           000097   548 _AIN7	=	0x0097
                           000096   549 _MISO	=	0x0096
                           000096   550 _RXD1_	=	0x0096
                           000096   551 _VBUS	=	0x0096
                           000096   552 _AIN6	=	0x0096
                           000095   553 _MOSI	=	0x0095
                           000095   554 _PWM0_	=	0x0095
                           000095   555 _UCC2	=	0x0095
                           000095   556 _AIN5	=	0x0095
                           000094   557 _SCS	=	0x0094
                           000094   558 _UCC1	=	0x0094
                           000094   559 _AIN4	=	0x0094
                           000093   560 _AIN3	=	0x0093
                           000092   561 _AIN2	=	0x0092
                           000091   562 _T2EX	=	0x0091
                           000091   563 _CAP2	=	0x0091
                           000091   564 _AIN1	=	0x0091
                           000090   565 _T2	=	0x0090
                           000090   566 _CAP1	=	0x0090
                           000090   567 _AIN0	=	0x0090
                           0000A7   568 _P2_7	=	0x00a7
                           0000A6   569 _P2_6	=	0x00a6
                           0000A5   570 _P2_5	=	0x00a5
                           0000A4   571 _P2_4	=	0x00a4
                           0000A3   572 _P2_3	=	0x00a3
                           0000A2   573 _P2_2	=	0x00a2
                           0000A1   574 _P2_1	=	0x00a1
                           0000A0   575 _P2_0	=	0x00a0
                           0000A7   576 _PWM7	=	0x00a7
                           0000A7   577 _TXD1	=	0x00a7
                           0000A6   578 _PWM6	=	0x00a6
                           0000A6   579 _RXD1	=	0x00a6
                           0000A5   580 _PWM0	=	0x00a5
                           0000A5   581 _T2EX_	=	0x00a5
                           0000A5   582 _CAP2_	=	0x00a5
                           0000A4   583 _PWM1	=	0x00a4
                           0000A4   584 _T2_	=	0x00a4
                           0000A4   585 _CAP1_	=	0x00a4
                           0000A3   586 _PWM2	=	0x00a3
                           0000A2   587 _PWM3	=	0x00a2
                           0000A2   588 _INT0_	=	0x00a2
                           0000A1   589 _PWM4	=	0x00a1
                           0000A0   590 _PWM5	=	0x00a0
                           0000B7   591 _P3_7	=	0x00b7
                           0000B6   592 _P3_6	=	0x00b6
                           0000B5   593 _P3_5	=	0x00b5
                           0000B4   594 _P3_4	=	0x00b4
                           0000B3   595 _P3_3	=	0x00b3
                           0000B2   596 _P3_2	=	0x00b2
                           0000B1   597 _P3_1	=	0x00b1
                           0000B0   598 _P3_0	=	0x00b0
                           0000B7   599 _INT3	=	0x00b7
                           0000B6   600 _CAP0	=	0x00b6
                           0000B5   601 _T1	=	0x00b5
                           0000B4   602 _T0	=	0x00b4
                           0000B3   603 _INT1	=	0x00b3
                           0000B2   604 _INT0	=	0x00b2
                           0000B1   605 _TXD	=	0x00b1
                           0000B0   606 _RXD	=	0x00b0
                           0000C6   607 _P4_6	=	0x00c6
                           0000C5   608 _P4_5	=	0x00c5
                           0000C4   609 _P4_4	=	0x00c4
                           0000C3   610 _P4_3	=	0x00c3
                           0000C2   611 _P4_2	=	0x00c2
                           0000C1   612 _P4_1	=	0x00c1
                           0000C0   613 _P4_0	=	0x00c0
                           0000C7   614 _XO	=	0x00c7
                           0000C6   615 _XI	=	0x00c6
                           00008F   616 _TF1	=	0x008f
                           00008E   617 _TR1	=	0x008e
                           00008D   618 _TF0	=	0x008d
                           00008C   619 _TR0	=	0x008c
                           00008B   620 _IE1	=	0x008b
                           00008A   621 _IT1	=	0x008a
                           000089   622 _IE0	=	0x0089
                           000088   623 _IT0	=	0x0088
                           00009F   624 _SM0	=	0x009f
                           00009E   625 _SM1	=	0x009e
                           00009D   626 _SM2	=	0x009d
                           00009C   627 _REN	=	0x009c
                           00009B   628 _TB8	=	0x009b
                           00009A   629 _RB8	=	0x009a
                           000099   630 _TI	=	0x0099
                           000098   631 _RI	=	0x0098
                           0000CF   632 _TF2	=	0x00cf
                           0000CF   633 _CAP1F	=	0x00cf
                           0000CE   634 _EXF2	=	0x00ce
                           0000CD   635 _RCLK	=	0x00cd
                           0000CC   636 _TCLK	=	0x00cc
                           0000CB   637 _EXEN2	=	0x00cb
                           0000CA   638 _TR2	=	0x00ca
                           0000C9   639 _C_T2	=	0x00c9
                           0000C8   640 _CP_RL2	=	0x00c8
                           0000FF   641 _S0_FST_ACT	=	0x00ff
                           0000FE   642 _S0_IF_OV	=	0x00fe
                           0000FD   643 _S0_IF_FIRST	=	0x00fd
                           0000FC   644 _S0_IF_BYTE	=	0x00fc
                           0000FB   645 _S0_FREE	=	0x00fb
                           0000FA   646 _S0_T_FIFO	=	0x00fa
                           0000F8   647 _S0_R_FIFO	=	0x00f8
                           0000DF   648 _U_IS_NAK	=	0x00df
                           0000DE   649 _U_TOG_OK	=	0x00de
                           0000DD   650 _U_SIE_FREE	=	0x00dd
                           0000DC   651 _UIF_FIFO_OV	=	0x00dc
                           0000DB   652 _UIF_HST_SOF	=	0x00db
                           0000DA   653 _UIF_SUSPEND	=	0x00da
                           0000D9   654 _UIF_TRANSFER	=	0x00d9
                           0000D8   655 _UIF_DETECT	=	0x00d8
                           0000D8   656 _UIF_BUS_RST	=	0x00d8
                                    657 ;--------------------------------------------------------
                                    658 ; overlayable register banks
                                    659 ;--------------------------------------------------------
                                    660 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        661 	.ds 8
                                    662 ;--------------------------------------------------------
                                    663 ; internal ram data
                                    664 ;--------------------------------------------------------
                                    665 	.area DSEG    (DATA)
                                    666 ;--------------------------------------------------------
                                    667 ; overlayable items in internal ram 
                                    668 ;--------------------------------------------------------
                                    669 ;--------------------------------------------------------
                                    670 ; indirectly addressable internal ram data
                                    671 ;--------------------------------------------------------
                                    672 	.area ISEG    (DATA)
                                    673 ;--------------------------------------------------------
                                    674 ; absolute internal ram data
                                    675 ;--------------------------------------------------------
                                    676 	.area IABS    (ABS,DATA)
                                    677 	.area IABS    (ABS,DATA)
                                    678 ;--------------------------------------------------------
                                    679 ; bit data
                                    680 ;--------------------------------------------------------
                                    681 	.area BSEG    (BIT)
                                    682 ;--------------------------------------------------------
                                    683 ; paged external ram data
                                    684 ;--------------------------------------------------------
                                    685 	.area PSEG    (PAG,XDATA)
                                    686 ;--------------------------------------------------------
                                    687 ; external ram data
                                    688 ;--------------------------------------------------------
                                    689 	.area XSEG    (XDATA)
                                    690 ;--------------------------------------------------------
                                    691 ; absolute external ram data
                                    692 ;--------------------------------------------------------
                                    693 	.area XABS    (ABS,XDATA)
                                    694 ;--------------------------------------------------------
                                    695 ; external initialized ram data
                                    696 ;--------------------------------------------------------
                                    697 	.area HOME    (CODE)
                                    698 	.area GSINIT0 (CODE)
                                    699 	.area GSINIT1 (CODE)
                                    700 	.area GSINIT2 (CODE)
                                    701 	.area GSINIT3 (CODE)
                                    702 	.area GSINIT4 (CODE)
                                    703 	.area GSINIT5 (CODE)
                                    704 	.area GSINIT  (CODE)
                                    705 	.area GSFINAL (CODE)
                                    706 	.area CSEG    (CODE)
                                    707 ;--------------------------------------------------------
                                    708 ; global & static initialisations
                                    709 ;--------------------------------------------------------
                                    710 	.area HOME    (CODE)
                                    711 	.area GSINIT  (CODE)
                                    712 	.area GSFINAL (CODE)
                                    713 	.area GSINIT  (CODE)
                                    714 ;--------------------------------------------------------
                                    715 ; Home
                                    716 ;--------------------------------------------------------
                                    717 	.area HOME    (CODE)
                                    718 	.area HOME    (CODE)
                                    719 ;--------------------------------------------------------
                                    720 ; code
                                    721 ;--------------------------------------------------------
                                    722 	.area CSEG    (CODE)
                                    723 ;------------------------------------------------------------
                                    724 ;Allocation info for local variables in function 'IIC_Init'
                                    725 ;------------------------------------------------------------
                                    726 ;	source/CH549_IIC.c:11: void IIC_Init()
                                    727 ;	-----------------------------------------
                                    728 ;	 function IIC_Init
                                    729 ;	-----------------------------------------
      000A0A                        730 _IIC_Init:
                           000007   731 	ar7 = 0x07
                           000006   732 	ar6 = 0x06
                           000005   733 	ar5 = 0x05
                           000004   734 	ar4 = 0x04
                           000003   735 	ar3 = 0x03
                           000002   736 	ar2 = 0x02
                           000001   737 	ar1 = 0x01
                           000000   738 	ar0 = 0x00
                                    739 ;	source/CH549_IIC.c:13: GPIO_Init(PORT0,PIN5,MODE2);
      000A0A 74 02            [12]  740 	mov	a,#0x02
      000A0C C0 E0            [24]  741 	push	acc
      000A0E C4               [12]  742 	swap	a
      000A0F C0 E0            [24]  743 	push	acc
      000A11 75 82 00         [24]  744 	mov	dpl,#0x00
      000A14 12 07 54         [24]  745 	lcall	_GPIO_Init
      000A17 15 81            [12]  746 	dec	sp
      000A19 15 81            [12]  747 	dec	sp
                                    748 ;	source/CH549_IIC.c:14: GPIO_Init(PORT0,PIN6,MODE2);//CH549电压为5V，而非3.3V
      000A1B 74 02            [12]  749 	mov	a,#0x02
      000A1D C0 E0            [24]  750 	push	acc
      000A1F 74 40            [12]  751 	mov	a,#0x40
      000A21 C0 E0            [24]  752 	push	acc
      000A23 75 82 00         [24]  753 	mov	dpl,#0x00
      000A26 12 07 54         [24]  754 	lcall	_GPIO_Init
      000A29 15 81            [12]  755 	dec	sp
      000A2B 15 81            [12]  756 	dec	sp
                                    757 ;	source/CH549_IIC.c:15: }
      000A2D 22               [24]  758 	ret
                                    759 ;------------------------------------------------------------
                                    760 ;Allocation info for local variables in function 'IIC_Start'
                                    761 ;------------------------------------------------------------
                                    762 ;	source/CH549_IIC.c:18: void IIC_Start()
                                    763 ;	-----------------------------------------
                                    764 ;	 function IIC_Start
                                    765 ;	-----------------------------------------
      000A2E                        766 _IIC_Start:
                                    767 ;	source/CH549_IIC.c:20: SDA_UP;
                                    768 ;	assignBit
      000A2E D2 86            [12]  769 	setb	_P0_6
                                    770 ;	source/CH549_IIC.c:21: SCL_UP; 
                                    771 ;	assignBit
      000A30 D2 85            [12]  772 	setb	_P0_5
                                    773 ;	source/CH549_IIC.c:22: mDelayuS(4);
      000A32 90 00 04         [24]  774 	mov	dptr,#0x0004
      000A35 12 06 D0         [24]  775 	lcall	_mDelayuS
                                    776 ;	source/CH549_IIC.c:23: SDA_DOWN;
                                    777 ;	assignBit
      000A38 C2 86            [12]  778 	clr	_P0_6
                                    779 ;	source/CH549_IIC.c:24: mDelayuS(4);
      000A3A 90 00 04         [24]  780 	mov	dptr,#0x0004
      000A3D 12 06 D0         [24]  781 	lcall	_mDelayuS
                                    782 ;	source/CH549_IIC.c:25: SCL_DOWN;
                                    783 ;	assignBit
      000A40 C2 85            [12]  784 	clr	_P0_5
                                    785 ;	source/CH549_IIC.c:26: }
      000A42 22               [24]  786 	ret
                                    787 ;------------------------------------------------------------
                                    788 ;Allocation info for local variables in function 'IIC_Stop'
                                    789 ;------------------------------------------------------------
                                    790 ;	source/CH549_IIC.c:29: void IIC_Stop()
                                    791 ;	-----------------------------------------
                                    792 ;	 function IIC_Stop
                                    793 ;	-----------------------------------------
      000A43                        794 _IIC_Stop:
                                    795 ;	source/CH549_IIC.c:31: SCL_DOWN;
                                    796 ;	assignBit
      000A43 C2 85            [12]  797 	clr	_P0_5
                                    798 ;	source/CH549_IIC.c:32: SDA_DOWN;
                                    799 ;	assignBit
      000A45 C2 86            [12]  800 	clr	_P0_6
                                    801 ;	source/CH549_IIC.c:33: mDelayuS(4);
      000A47 90 00 04         [24]  802 	mov	dptr,#0x0004
      000A4A 12 06 D0         [24]  803 	lcall	_mDelayuS
                                    804 ;	source/CH549_IIC.c:34: SCL_UP;
                                    805 ;	assignBit
      000A4D D2 85            [12]  806 	setb	_P0_5
                                    807 ;	source/CH549_IIC.c:35: SDA_UP;
                                    808 ;	assignBit
      000A4F D2 86            [12]  809 	setb	_P0_6
                                    810 ;	source/CH549_IIC.c:36: mDelayuS(4);
      000A51 90 00 04         [24]  811 	mov	dptr,#0x0004
                                    812 ;	source/CH549_IIC.c:37: }
      000A54 02 06 D0         [24]  813 	ljmp	_mDelayuS
                                    814 ;------------------------------------------------------------
                                    815 ;Allocation info for local variables in function 'IIC_Wait_Ack'
                                    816 ;------------------------------------------------------------
                                    817 ;t                         Allocated to registers r4 r5 r6 r7 
                                    818 ;------------------------------------------------------------
                                    819 ;	source/CH549_IIC.c:40: UINT8 IIC_Wait_Ack()
                                    820 ;	-----------------------------------------
                                    821 ;	 function IIC_Wait_Ack
                                    822 ;	-----------------------------------------
      000A57                        823 _IIC_Wait_Ack:
                                    824 ;	source/CH549_IIC.c:43: SDA_UP; 
                                    825 ;	assignBit
      000A57 D2 86            [12]  826 	setb	_P0_6
                                    827 ;	source/CH549_IIC.c:44: mDelayuS(1);
      000A59 90 00 01         [24]  828 	mov	dptr,#0x0001
      000A5C 12 06 D0         [24]  829 	lcall	_mDelayuS
                                    830 ;	source/CH549_IIC.c:45: SCL_UP; 
                                    831 ;	assignBit
      000A5F D2 85            [12]  832 	setb	_P0_5
                                    833 ;	source/CH549_IIC.c:46: mDelayuS(1);
      000A61 90 00 01         [24]  834 	mov	dptr,#0x0001
      000A64 12 06 D0         [24]  835 	lcall	_mDelayuS
                                    836 ;	source/CH549_IIC.c:47: while(SDA_READ)
      000A67 7C 00            [12]  837 	mov	r4,#0x00
      000A69 7D 00            [12]  838 	mov	r5,#0x00
      000A6B 7E 00            [12]  839 	mov	r6,#0x00
      000A6D 7F 00            [12]  840 	mov	r7,#0x00
      000A6F                        841 00103$:
      000A6F 30 86 20         [24]  842 	jnb	_P0_6,00105$
                                    843 ;	source/CH549_IIC.c:49: t++;
      000A72 0C               [12]  844 	inc	r4
      000A73 BC 00 09         [24]  845 	cjne	r4,#0x00,00121$
      000A76 0D               [12]  846 	inc	r5
      000A77 BD 00 05         [24]  847 	cjne	r5,#0x00,00121$
      000A7A 0E               [12]  848 	inc	r6
      000A7B BE 00 01         [24]  849 	cjne	r6,#0x00,00121$
      000A7E 0F               [12]  850 	inc	r7
      000A7F                        851 00121$:
                                    852 ;	source/CH549_IIC.c:50: if(t > 250)
      000A7F C3               [12]  853 	clr	c
      000A80 74 FA            [12]  854 	mov	a,#0xfa
      000A82 9C               [12]  855 	subb	a,r4
      000A83 E4               [12]  856 	clr	a
      000A84 9D               [12]  857 	subb	a,r5
      000A85 E4               [12]  858 	clr	a
      000A86 9E               [12]  859 	subb	a,r6
      000A87 E4               [12]  860 	clr	a
      000A88 9F               [12]  861 	subb	a,r7
      000A89 50 E4            [24]  862 	jnc	00103$
                                    863 ;	source/CH549_IIC.c:52: IIC_Stop();
      000A8B 12 0A 43         [24]  864 	lcall	_IIC_Stop
                                    865 ;	source/CH549_IIC.c:53: return 1;
      000A8E 75 82 01         [24]  866 	mov	dpl,#0x01
      000A91 22               [24]  867 	ret
      000A92                        868 00105$:
                                    869 ;	source/CH549_IIC.c:56: SCL_DOWN;
                                    870 ;	assignBit
      000A92 C2 85            [12]  871 	clr	_P0_5
                                    872 ;	source/CH549_IIC.c:57: return 0;	
      000A94 75 82 00         [24]  873 	mov	dpl,#0x00
                                    874 ;	source/CH549_IIC.c:58: }
      000A97 22               [24]  875 	ret
                                    876 ;------------------------------------------------------------
                                    877 ;Allocation info for local variables in function 'IIC_Nack'
                                    878 ;------------------------------------------------------------
                                    879 ;	source/CH549_IIC.c:61: void IIC_Nack()
                                    880 ;	-----------------------------------------
                                    881 ;	 function IIC_Nack
                                    882 ;	-----------------------------------------
      000A98                        883 _IIC_Nack:
                                    884 ;	source/CH549_IIC.c:63: SCL_DOWN;
                                    885 ;	assignBit
      000A98 C2 85            [12]  886 	clr	_P0_5
                                    887 ;	source/CH549_IIC.c:64: SDA_UP;
                                    888 ;	assignBit
      000A9A D2 86            [12]  889 	setb	_P0_6
                                    890 ;	source/CH549_IIC.c:65: mDelayuS(2);
      000A9C 90 00 02         [24]  891 	mov	dptr,#0x0002
      000A9F 12 06 D0         [24]  892 	lcall	_mDelayuS
                                    893 ;	source/CH549_IIC.c:66: SCL_UP;
                                    894 ;	assignBit
      000AA2 D2 85            [12]  895 	setb	_P0_5
                                    896 ;	source/CH549_IIC.c:67: mDelayuS(2);
      000AA4 90 00 02         [24]  897 	mov	dptr,#0x0002
      000AA7 12 06 D0         [24]  898 	lcall	_mDelayuS
                                    899 ;	source/CH549_IIC.c:68: SCL_DOWN;
                                    900 ;	assignBit
      000AAA C2 85            [12]  901 	clr	_P0_5
                                    902 ;	source/CH549_IIC.c:69: }
      000AAC 22               [24]  903 	ret
                                    904 ;------------------------------------------------------------
                                    905 ;Allocation info for local variables in function 'IIC_Ack'
                                    906 ;------------------------------------------------------------
                                    907 ;	source/CH549_IIC.c:72: void IIC_Ack()
                                    908 ;	-----------------------------------------
                                    909 ;	 function IIC_Ack
                                    910 ;	-----------------------------------------
      000AAD                        911 _IIC_Ack:
                                    912 ;	source/CH549_IIC.c:74: SCL_DOWN;
                                    913 ;	assignBit
      000AAD C2 85            [12]  914 	clr	_P0_5
                                    915 ;	source/CH549_IIC.c:75: SDA_DOWN;
                                    916 ;	assignBit
      000AAF C2 86            [12]  917 	clr	_P0_6
                                    918 ;	source/CH549_IIC.c:76: mDelayuS(2);
      000AB1 90 00 02         [24]  919 	mov	dptr,#0x0002
      000AB4 12 06 D0         [24]  920 	lcall	_mDelayuS
                                    921 ;	source/CH549_IIC.c:77: SCL_UP;
                                    922 ;	assignBit
      000AB7 D2 85            [12]  923 	setb	_P0_5
                                    924 ;	source/CH549_IIC.c:78: mDelayuS(2);
      000AB9 90 00 02         [24]  925 	mov	dptr,#0x0002
      000ABC 12 06 D0         [24]  926 	lcall	_mDelayuS
                                    927 ;	source/CH549_IIC.c:79: SCL_DOWN;
                                    928 ;	assignBit
      000ABF C2 85            [12]  929 	clr	_P0_5
                                    930 ;	source/CH549_IIC.c:80: }
      000AC1 22               [24]  931 	ret
                                    932 ;------------------------------------------------------------
                                    933 ;Allocation info for local variables in function 'IIC_Send_Byte'
                                    934 ;------------------------------------------------------------
                                    935 ;byte                      Allocated to registers r7 
                                    936 ;i                         Allocated to registers r6 
                                    937 ;------------------------------------------------------------
                                    938 ;	source/CH549_IIC.c:83: void IIC_Send_Byte(UINT8 byte)
                                    939 ;	-----------------------------------------
                                    940 ;	 function IIC_Send_Byte
                                    941 ;	-----------------------------------------
      000AC2                        942 _IIC_Send_Byte:
      000AC2 AF 82            [24]  943 	mov	r7,dpl
                                    944 ;	source/CH549_IIC.c:86: SCL_DOWN;
                                    945 ;	assignBit
      000AC4 C2 85            [12]  946 	clr	_P0_5
                                    947 ;	source/CH549_IIC.c:87: for( i = 0 ; i < 8 ; i++ )
      000AC6 7E 00            [12]  948 	mov	r6,#0x00
      000AC8                        949 00105$:
                                    950 ;	source/CH549_IIC.c:89: if(byte&0x80)
      000AC8 EF               [12]  951 	mov	a,r7
      000AC9 30 E7 04         [24]  952 	jnb	acc.7,00102$
                                    953 ;	source/CH549_IIC.c:91: SDA_UP;
                                    954 ;	assignBit
      000ACC D2 86            [12]  955 	setb	_P0_6
      000ACE 80 02            [24]  956 	sjmp	00103$
      000AD0                        957 00102$:
                                    958 ;	source/CH549_IIC.c:94: SDA_DOWN;
                                    959 ;	assignBit
      000AD0 C2 86            [12]  960 	clr	_P0_6
      000AD2                        961 00103$:
                                    962 ;	source/CH549_IIC.c:96: byte <<= 1;
      000AD2 8F 05            [24]  963 	mov	ar5,r7
      000AD4 ED               [12]  964 	mov	a,r5
      000AD5 2D               [12]  965 	add	a,r5
      000AD6 FF               [12]  966 	mov	r7,a
                                    967 ;	source/CH549_IIC.c:97: mDelayuS(2);
      000AD7 90 00 02         [24]  968 	mov	dptr,#0x0002
      000ADA C0 07            [24]  969 	push	ar7
      000ADC C0 06            [24]  970 	push	ar6
      000ADE 12 06 D0         [24]  971 	lcall	_mDelayuS
                                    972 ;	source/CH549_IIC.c:98: SCL_UP;
                                    973 ;	assignBit
      000AE1 D2 85            [12]  974 	setb	_P0_5
                                    975 ;	source/CH549_IIC.c:99: mDelayuS(2);
      000AE3 90 00 02         [24]  976 	mov	dptr,#0x0002
      000AE6 12 06 D0         [24]  977 	lcall	_mDelayuS
                                    978 ;	source/CH549_IIC.c:100: SCL_DOWN;
                                    979 ;	assignBit
      000AE9 C2 85            [12]  980 	clr	_P0_5
                                    981 ;	source/CH549_IIC.c:101: mDelayuS(2);
      000AEB 90 00 02         [24]  982 	mov	dptr,#0x0002
      000AEE 12 06 D0         [24]  983 	lcall	_mDelayuS
      000AF1 D0 06            [24]  984 	pop	ar6
      000AF3 D0 07            [24]  985 	pop	ar7
                                    986 ;	source/CH549_IIC.c:87: for( i = 0 ; i < 8 ; i++ )
      000AF5 0E               [12]  987 	inc	r6
      000AF6 BE 08 00         [24]  988 	cjne	r6,#0x08,00119$
      000AF9                        989 00119$:
      000AF9 40 CD            [24]  990 	jc	00105$
                                    991 ;	source/CH549_IIC.c:103: }
      000AFB 22               [24]  992 	ret
                                    993 ;------------------------------------------------------------
                                    994 ;Allocation info for local variables in function 'IIC_Read_Byte'
                                    995 ;------------------------------------------------------------
                                    996 ;ack                       Allocated to registers r7 
                                    997 ;i                         Allocated to registers r5 
                                    998 ;receive                   Allocated to registers r6 
                                    999 ;------------------------------------------------------------
                                   1000 ;	source/CH549_IIC.c:106: UINT8 IIC_Read_Byte(UINT8 ack)
                                   1001 ;	-----------------------------------------
                                   1002 ;	 function IIC_Read_Byte
                                   1003 ;	-----------------------------------------
      000AFC                       1004 _IIC_Read_Byte:
      000AFC AF 82            [24] 1005 	mov	r7,dpl
                                   1006 ;	source/CH549_IIC.c:109: UINT8 receive = 0;
      000AFE 7E 00            [12] 1007 	mov	r6,#0x00
                                   1008 ;	source/CH549_IIC.c:110: for( i = 0 ; i < 8 ; i++ )
      000B00 7D 00            [12] 1009 	mov	r5,#0x00
      000B02                       1010 00107$:
                                   1011 ;	source/CH549_IIC.c:112: SCL_DOWN;
                                   1012 ;	assignBit
      000B02 C2 85            [12] 1013 	clr	_P0_5
                                   1014 ;	source/CH549_IIC.c:113: mDelayuS(2);
      000B04 90 00 02         [24] 1015 	mov	dptr,#0x0002
      000B07 C0 07            [24] 1016 	push	ar7
      000B09 C0 06            [24] 1017 	push	ar6
      000B0B C0 05            [24] 1018 	push	ar5
      000B0D 12 06 D0         [24] 1019 	lcall	_mDelayuS
      000B10 D0 05            [24] 1020 	pop	ar5
      000B12 D0 06            [24] 1021 	pop	ar6
      000B14 D0 07            [24] 1022 	pop	ar7
                                   1023 ;	source/CH549_IIC.c:114: SCL_UP;
                                   1024 ;	assignBit
      000B16 D2 85            [12] 1025 	setb	_P0_5
                                   1026 ;	source/CH549_IIC.c:115: receive <<= 1;
      000B18 8E 04            [24] 1027 	mov	ar4,r6
      000B1A EC               [12] 1028 	mov	a,r4
      000B1B 2C               [12] 1029 	add	a,r4
      000B1C FE               [12] 1030 	mov	r6,a
                                   1031 ;	source/CH549_IIC.c:116: if(SDA_READ)
      000B1D 30 86 01         [24] 1032 	jnb	_P0_6,00102$
                                   1033 ;	source/CH549_IIC.c:118: receive++; 
      000B20 0E               [12] 1034 	inc	r6
      000B21                       1035 00102$:
                                   1036 ;	source/CH549_IIC.c:120: mDelayuS(1);
      000B21 90 00 01         [24] 1037 	mov	dptr,#0x0001
      000B24 C0 07            [24] 1038 	push	ar7
      000B26 C0 06            [24] 1039 	push	ar6
      000B28 C0 05            [24] 1040 	push	ar5
      000B2A 12 06 D0         [24] 1041 	lcall	_mDelayuS
      000B2D D0 05            [24] 1042 	pop	ar5
      000B2F D0 06            [24] 1043 	pop	ar6
      000B31 D0 07            [24] 1044 	pop	ar7
                                   1045 ;	source/CH549_IIC.c:110: for( i = 0 ; i < 8 ; i++ )
      000B33 0D               [12] 1046 	inc	r5
      000B34 BD 08 00         [24] 1047 	cjne	r5,#0x08,00129$
      000B37                       1048 00129$:
      000B37 40 C9            [24] 1049 	jc	00107$
                                   1050 ;	source/CH549_IIC.c:122: if (!ack)
      000B39 EF               [12] 1051 	mov	a,r7
      000B3A 70 09            [24] 1052 	jnz	00105$
                                   1053 ;	source/CH549_IIC.c:124: IIC_Nack();
      000B3C C0 06            [24] 1054 	push	ar6
      000B3E 12 0A 98         [24] 1055 	lcall	_IIC_Nack
      000B41 D0 06            [24] 1056 	pop	ar6
      000B43 80 07            [24] 1057 	sjmp	00106$
      000B45                       1058 00105$:
                                   1059 ;	source/CH549_IIC.c:127: IIC_Ack();
      000B45 C0 06            [24] 1060 	push	ar6
      000B47 12 0A AD         [24] 1061 	lcall	_IIC_Ack
      000B4A D0 06            [24] 1062 	pop	ar6
      000B4C                       1063 00106$:
                                   1064 ;	source/CH549_IIC.c:129: return receive;
      000B4C 8E 82            [24] 1065 	mov	dpl,r6
                                   1066 ;	source/CH549_IIC.c:130: }
      000B4E 22               [24] 1067 	ret
                                   1068 	.area CSEG    (CODE)
                                   1069 	.area CONST   (CODE)
                                   1070 	.area CABS    (ABS,CODE)
