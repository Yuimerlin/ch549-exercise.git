#ifndef __CH549_IIC_H__
#define __CH549_IIC_H__

#include "CH549_sdcc.h"
#include "CH549_DEBUG.h"
#include "CH549_GPIO.h"

/****************** 参数定义 ******************/
#define  SCL_UP          (P0_5 = 1)
#define  SCL_DOWN        (P0_5 = 0)

#define  SDA_UP          (P0_6 = 1)
#define  SDA_DOWN        (P0_6 = 0)

#define   SDA_READ       P0_6

/****************** 外部调用子函数 ****************************/
extern void IIC_Init();
extern void IIC_Start();
extern void IIC_Stop();
extern UINT8 IIC_Wait_Ack();
extern void IIC_Nack();
extern void IIC_Ack();
extern void IIC_Send_Byte(UINT8 byte);
extern UINT8 IIC_Read_Byte(UINT8 ack);

#endif
