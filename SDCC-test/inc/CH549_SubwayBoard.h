#ifndef __CH549_IIC_H__
#define __CH549_IIC_H__

#include "CH549_sdcc.h"
#include "CH549_DEBUG.h"
#include "stdlib.h"

/****************** 参数定义 ******************/
#define  LED_A1  P3_5
#define  LED_A2  P2_2
#define  LED_A3  P2_4
#define  LED_A4  P2_6
#define  LED_A5  P2_7
#define  LED_B1  P1_1
#define  LED_B2  P1_4
#define  LED_B3  P1_5
#define  LED_B4  P1_6
/****************** 外部调用子函数 ****************************/
// extern UINT8 SubwayLine[6][7];
// extern UINT8 SubwayCircle[4][9];
extern void LIGHT_ON(UINT8 LIGHT);
extern void LIGHT_OFF(UINT8 LIGHT);
extern void LIGHT_Clean();

#endif
