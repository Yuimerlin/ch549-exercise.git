/********************************** (C) COPYRIGHT *******************************
* File Name          : CH549_SPARKROAD.C
* Author             : Yuimerlin
* Version            : V1.0
* Date               : 2022/7/20
* Description        : CH549 SPARKROAD相关函数
*******************************************************************************/
#include "CH549_SubwayBoard.h"

// //地铁线路
// UINT8 SubwayLine[6][7] = {
//     { 10, 15, 14, 13, 12, 7 , 6 },
//     { 5 , 4 , 3 , 8 , 13, 17, 16},
//     { 20, 15, 9 , 8 , 2 , 7 , 11},
//     { 19, 14, 9 , 5 , 0 , 0 , 0 },
//     { 18, 17, 12, 13, 14, 15, 10},
//     { 16, 7 , 1 , 0 , 0 , 0 , 0 },
//     };

// UINT8 SubwayCircle[4][9] = {
//     { 1 , 5 , 6 , 10, 11, 16, 18, 19, 20},
//     { 4 , 7 , 14, 15, 17, 0 , 0 , 0 , 0 },
//     { 2 , 3 , 9 , 12, 13, 0 , 0 , 0 , 0 },
//     { 8 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 },
//     };

//亮灯
void LIGHT_ON(unsigned char LIGHT)
{
    switch (LIGHT)
    {
    case 1:LED_A1 = 1;LED_B1 = 0;break;
    case 2:LED_A2 = 1;LED_B1 = 0;break;
    case 3:LED_A3 = 1;LED_B1 = 0;break;
    case 4:LED_A4 = 1;LED_B1 = 0;break;
    case 5:LED_A5 = 1;LED_B1 = 0;break;
    case 6:LED_A1 = 1;LED_B2 = 0;break;
    case 7:LED_A2 = 1;LED_B2 = 0;break;
    case 8:LED_A3 = 1;LED_B2 = 0;break;
    case 9:LED_A4 = 1;LED_B2 = 0;break;
    case 10:LED_A5 = 1;LED_B2 = 0;break;
    case 11:LED_A1 = 1;LED_B3 = 0;break;
    case 12:LED_A2 = 1;LED_B3 = 0;break;
    case 13:LED_A3 = 1;LED_B3 = 0;break;
    case 14:LED_A4 = 1;LED_B3 = 0;break;
    case 15:LED_A5 = 1;LED_B3 = 0;break;
    case 16:LED_A1 = 1;LED_B4 = 0;break;
    case 17:LED_A2 = 1;LED_B4 = 0;break;
    case 18:LED_A3 = 1;LED_B4 = 0;break;
    case 19:LED_A4 = 1;LED_B4 = 0;break;
    case 20:LED_A5 = 1;LED_B4 = 0;break;
    }
}

//灭灯
void LIGHT_OFF(unsigned char LIGHT)
{
    switch (LIGHT)
    {
    case 1:LED_A1 = 0;LED_B1 = 1;break;
    case 2:LED_A2 = 0;LED_B1 = 1;break;
    case 3:LED_A3 = 0;LED_B1 = 1;break;
    case 4:LED_A4 = 0;LED_B1 = 1;break;
    case 5:LED_A5 = 0;LED_B1 = 1;break;
    case 6:LED_A1 = 0;LED_B2 = 1;break;
    case 7:LED_A2 = 0;LED_B2 = 1;break;
    case 8:LED_A3 = 0;LED_B2 = 1;break;
    case 9:LED_A4 = 0;LED_B2 = 1;break;
    case 10:LED_A5 = 0;LED_B2 = 1;break;
    case 11:LED_A1 = 0;LED_B3 = 1;break;
    case 12:LED_A2 = 0;LED_B3 = 1;break;
    case 13:LED_A3 = 0;LED_B3 = 1;break;
    case 14:LED_A4 = 0;LED_B3 = 1;break;
    case 15:LED_A5 = 0;LED_B3 = 1;break;
    case 16:LED_A1 = 0;LED_B4 = 1;break;
    case 17:LED_A2 = 0;LED_B4 = 1;break;
    case 18:LED_A3 = 0;LED_B4 = 1;break;
    case 19:LED_A4 = 0;LED_B4 = 1;break;
    case 20:LED_A5 = 0;LED_B4 = 1;break;
    }
}

//清屏
void LIGHT_Clean()
{
    for (UINT8 LIGHT = 1; LIGHT < 21; LIGHT++)
    {
        LIGHT_OFF(LIGHT);
    }
    
}