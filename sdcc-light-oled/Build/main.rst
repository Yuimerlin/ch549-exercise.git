                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 4.0.0 #11528 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module main
                                      6 	.optsdcc -mmcs51 --model-small
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _Send_2811_24bits_PARM_3
                                     12 	.globl _Send_2811_24bits_PARM_2
                                     13 	.globl _BMP2
                                     14 	.globl _BMP1
                                     15 	.globl _Int0
                                     16 	.globl _main
                                     17 	.globl _Int0Init
                                     18 	.globl _oled_hor_scroll
                                     19 	.globl _Set_Light
                                     20 	.globl _RGB_Rst
                                     21 	.globl _Send_2811_24bits
                                     22 	.globl _SPIMasterModeSet
                                     23 	.globl _mDelaymS
                                     24 	.globl _mDelayuS
                                     25 	.globl _CfgFsys
                                     26 	.globl _setFontSize
                                     27 	.globl _OLED_ShowString
                                     28 	.globl _OLED_Clear
                                     29 	.globl _OLED_WR_Byte
                                     30 	.globl _OLED_Init
                                     31 	.globl _delay_ms
                                     32 	.globl _UIF_BUS_RST
                                     33 	.globl _UIF_DETECT
                                     34 	.globl _UIF_TRANSFER
                                     35 	.globl _UIF_SUSPEND
                                     36 	.globl _UIF_HST_SOF
                                     37 	.globl _UIF_FIFO_OV
                                     38 	.globl _U_SIE_FREE
                                     39 	.globl _U_TOG_OK
                                     40 	.globl _U_IS_NAK
                                     41 	.globl _S0_R_FIFO
                                     42 	.globl _S0_T_FIFO
                                     43 	.globl _S0_FREE
                                     44 	.globl _S0_IF_BYTE
                                     45 	.globl _S0_IF_FIRST
                                     46 	.globl _S0_IF_OV
                                     47 	.globl _S0_FST_ACT
                                     48 	.globl _CP_RL2
                                     49 	.globl _C_T2
                                     50 	.globl _TR2
                                     51 	.globl _EXEN2
                                     52 	.globl _TCLK
                                     53 	.globl _RCLK
                                     54 	.globl _EXF2
                                     55 	.globl _CAP1F
                                     56 	.globl _TF2
                                     57 	.globl _RI
                                     58 	.globl _TI
                                     59 	.globl _RB8
                                     60 	.globl _TB8
                                     61 	.globl _REN
                                     62 	.globl _SM2
                                     63 	.globl _SM1
                                     64 	.globl _SM0
                                     65 	.globl _IT0
                                     66 	.globl _IE0
                                     67 	.globl _IT1
                                     68 	.globl _IE1
                                     69 	.globl _TR0
                                     70 	.globl _TF0
                                     71 	.globl _TR1
                                     72 	.globl _TF1
                                     73 	.globl _XI
                                     74 	.globl _XO
                                     75 	.globl _P4_0
                                     76 	.globl _P4_1
                                     77 	.globl _P4_2
                                     78 	.globl _P4_3
                                     79 	.globl _P4_4
                                     80 	.globl _P4_5
                                     81 	.globl _P4_6
                                     82 	.globl _RXD
                                     83 	.globl _TXD
                                     84 	.globl _INT0
                                     85 	.globl _INT1
                                     86 	.globl _T0
                                     87 	.globl _T1
                                     88 	.globl _CAP0
                                     89 	.globl _INT3
                                     90 	.globl _P3_0
                                     91 	.globl _P3_1
                                     92 	.globl _P3_2
                                     93 	.globl _P3_3
                                     94 	.globl _P3_4
                                     95 	.globl _P3_5
                                     96 	.globl _P3_6
                                     97 	.globl _P3_7
                                     98 	.globl _PWM5
                                     99 	.globl _PWM4
                                    100 	.globl _INT0_
                                    101 	.globl _PWM3
                                    102 	.globl _PWM2
                                    103 	.globl _CAP1_
                                    104 	.globl _T2_
                                    105 	.globl _PWM1
                                    106 	.globl _CAP2_
                                    107 	.globl _T2EX_
                                    108 	.globl _PWM0
                                    109 	.globl _RXD1
                                    110 	.globl _PWM6
                                    111 	.globl _TXD1
                                    112 	.globl _PWM7
                                    113 	.globl _P2_0
                                    114 	.globl _P2_1
                                    115 	.globl _P2_2
                                    116 	.globl _P2_3
                                    117 	.globl _P2_4
                                    118 	.globl _P2_5
                                    119 	.globl _P2_6
                                    120 	.globl _P2_7
                                    121 	.globl _AIN0
                                    122 	.globl _CAP1
                                    123 	.globl _T2
                                    124 	.globl _AIN1
                                    125 	.globl _CAP2
                                    126 	.globl _T2EX
                                    127 	.globl _AIN2
                                    128 	.globl _AIN3
                                    129 	.globl _AIN4
                                    130 	.globl _UCC1
                                    131 	.globl _SCS
                                    132 	.globl _AIN5
                                    133 	.globl _UCC2
                                    134 	.globl _PWM0_
                                    135 	.globl _MOSI
                                    136 	.globl _AIN6
                                    137 	.globl _VBUS
                                    138 	.globl _RXD1_
                                    139 	.globl _MISO
                                    140 	.globl _AIN7
                                    141 	.globl _TXD1_
                                    142 	.globl _SCK
                                    143 	.globl _P1_0
                                    144 	.globl _P1_1
                                    145 	.globl _P1_2
                                    146 	.globl _P1_3
                                    147 	.globl _P1_4
                                    148 	.globl _P1_5
                                    149 	.globl _P1_6
                                    150 	.globl _P1_7
                                    151 	.globl _AIN8
                                    152 	.globl _AIN9
                                    153 	.globl _AIN10
                                    154 	.globl _RXD_
                                    155 	.globl _AIN11
                                    156 	.globl _TXD_
                                    157 	.globl _AIN12
                                    158 	.globl _RXD2
                                    159 	.globl _AIN13
                                    160 	.globl _TXD2
                                    161 	.globl _AIN14
                                    162 	.globl _RXD3
                                    163 	.globl _AIN15
                                    164 	.globl _TXD3
                                    165 	.globl _P0_0
                                    166 	.globl _P0_1
                                    167 	.globl _P0_2
                                    168 	.globl _P0_3
                                    169 	.globl _P0_4
                                    170 	.globl _P0_5
                                    171 	.globl _P0_6
                                    172 	.globl _P0_7
                                    173 	.globl _IE_SPI0
                                    174 	.globl _IE_INT3
                                    175 	.globl _IE_USB
                                    176 	.globl _IE_UART2
                                    177 	.globl _IE_ADC
                                    178 	.globl _IE_UART1
                                    179 	.globl _IE_UART3
                                    180 	.globl _IE_PWMX
                                    181 	.globl _IE_GPIO
                                    182 	.globl _IE_WDOG
                                    183 	.globl _PX0
                                    184 	.globl _PT0
                                    185 	.globl _PX1
                                    186 	.globl _PT1
                                    187 	.globl _PS
                                    188 	.globl _PT2
                                    189 	.globl _PL_FLAG
                                    190 	.globl _PH_FLAG
                                    191 	.globl _EX0
                                    192 	.globl _ET0
                                    193 	.globl _EX1
                                    194 	.globl _ET1
                                    195 	.globl _ES
                                    196 	.globl _ET2
                                    197 	.globl _E_DIS
                                    198 	.globl _EA
                                    199 	.globl _P
                                    200 	.globl _F1
                                    201 	.globl _OV
                                    202 	.globl _RS0
                                    203 	.globl _RS1
                                    204 	.globl _F0
                                    205 	.globl _AC
                                    206 	.globl _CY
                                    207 	.globl _UEP1_DMA_H
                                    208 	.globl _UEP1_DMA_L
                                    209 	.globl _UEP1_DMA
                                    210 	.globl _UEP0_DMA_H
                                    211 	.globl _UEP0_DMA_L
                                    212 	.globl _UEP0_DMA
                                    213 	.globl _UEP2_3_MOD
                                    214 	.globl _UEP4_1_MOD
                                    215 	.globl _UEP3_DMA_H
                                    216 	.globl _UEP3_DMA_L
                                    217 	.globl _UEP3_DMA
                                    218 	.globl _UEP2_DMA_H
                                    219 	.globl _UEP2_DMA_L
                                    220 	.globl _UEP2_DMA
                                    221 	.globl _USB_DEV_AD
                                    222 	.globl _USB_CTRL
                                    223 	.globl _USB_INT_EN
                                    224 	.globl _UEP4_T_LEN
                                    225 	.globl _UEP4_CTRL
                                    226 	.globl _UEP0_T_LEN
                                    227 	.globl _UEP0_CTRL
                                    228 	.globl _USB_RX_LEN
                                    229 	.globl _USB_MIS_ST
                                    230 	.globl _USB_INT_ST
                                    231 	.globl _USB_INT_FG
                                    232 	.globl _UEP3_T_LEN
                                    233 	.globl _UEP3_CTRL
                                    234 	.globl _UEP2_T_LEN
                                    235 	.globl _UEP2_CTRL
                                    236 	.globl _UEP1_T_LEN
                                    237 	.globl _UEP1_CTRL
                                    238 	.globl _UDEV_CTRL
                                    239 	.globl _USB_C_CTRL
                                    240 	.globl _ADC_PIN
                                    241 	.globl _ADC_CHAN
                                    242 	.globl _ADC_DAT_H
                                    243 	.globl _ADC_DAT_L
                                    244 	.globl _ADC_DAT
                                    245 	.globl _ADC_CFG
                                    246 	.globl _ADC_CTRL
                                    247 	.globl _TKEY_CTRL
                                    248 	.globl _SIF3
                                    249 	.globl _SBAUD3
                                    250 	.globl _SBUF3
                                    251 	.globl _SCON3
                                    252 	.globl _SIF2
                                    253 	.globl _SBAUD2
                                    254 	.globl _SBUF2
                                    255 	.globl _SCON2
                                    256 	.globl _SIF1
                                    257 	.globl _SBAUD1
                                    258 	.globl _SBUF1
                                    259 	.globl _SCON1
                                    260 	.globl _SPI0_SETUP
                                    261 	.globl _SPI0_CK_SE
                                    262 	.globl _SPI0_CTRL
                                    263 	.globl _SPI0_DATA
                                    264 	.globl _SPI0_STAT
                                    265 	.globl _PWM_DATA7
                                    266 	.globl _PWM_DATA6
                                    267 	.globl _PWM_DATA5
                                    268 	.globl _PWM_DATA4
                                    269 	.globl _PWM_DATA3
                                    270 	.globl _PWM_CTRL2
                                    271 	.globl _PWM_CK_SE
                                    272 	.globl _PWM_CTRL
                                    273 	.globl _PWM_DATA0
                                    274 	.globl _PWM_DATA1
                                    275 	.globl _PWM_DATA2
                                    276 	.globl _T2CAP1H
                                    277 	.globl _T2CAP1L
                                    278 	.globl _T2CAP1
                                    279 	.globl _TH2
                                    280 	.globl _TL2
                                    281 	.globl _T2COUNT
                                    282 	.globl _RCAP2H
                                    283 	.globl _RCAP2L
                                    284 	.globl _RCAP2
                                    285 	.globl _T2MOD
                                    286 	.globl _T2CON
                                    287 	.globl _T2CAP0H
                                    288 	.globl _T2CAP0L
                                    289 	.globl _T2CAP0
                                    290 	.globl _T2CON2
                                    291 	.globl _SBUF
                                    292 	.globl _SCON
                                    293 	.globl _TH1
                                    294 	.globl _TH0
                                    295 	.globl _TL1
                                    296 	.globl _TL0
                                    297 	.globl _TMOD
                                    298 	.globl _TCON
                                    299 	.globl _XBUS_AUX
                                    300 	.globl _PIN_FUNC
                                    301 	.globl _P5
                                    302 	.globl _P4_DIR_PU
                                    303 	.globl _P4_MOD_OC
                                    304 	.globl _P4
                                    305 	.globl _P3_DIR_PU
                                    306 	.globl _P3_MOD_OC
                                    307 	.globl _P3
                                    308 	.globl _P2_DIR_PU
                                    309 	.globl _P2_MOD_OC
                                    310 	.globl _P2
                                    311 	.globl _P1_DIR_PU
                                    312 	.globl _P1_MOD_OC
                                    313 	.globl _P1
                                    314 	.globl _P0_DIR_PU
                                    315 	.globl _P0_MOD_OC
                                    316 	.globl _P0
                                    317 	.globl _ROM_CTRL
                                    318 	.globl _ROM_DATA_HH
                                    319 	.globl _ROM_DATA_HL
                                    320 	.globl _ROM_DATA_HI
                                    321 	.globl _ROM_ADDR_H
                                    322 	.globl _ROM_ADDR_L
                                    323 	.globl _ROM_ADDR
                                    324 	.globl _GPIO_IE
                                    325 	.globl _INTX
                                    326 	.globl _IP_EX
                                    327 	.globl _IE_EX
                                    328 	.globl _IP
                                    329 	.globl _IE
                                    330 	.globl _WDOG_COUNT
                                    331 	.globl _RESET_KEEP
                                    332 	.globl _WAKE_CTRL
                                    333 	.globl _CLOCK_CFG
                                    334 	.globl _POWER_CFG
                                    335 	.globl _PCON
                                    336 	.globl _GLOBAL_CFG
                                    337 	.globl _SAFE_MOD
                                    338 	.globl _DPH
                                    339 	.globl _DPL
                                    340 	.globl _SP
                                    341 	.globl _A_INV
                                    342 	.globl _B
                                    343 	.globl _ACC
                                    344 	.globl _PSW
                                    345 	.globl _led
                                    346 	.globl _F
                                    347 	.globl _oled_hor_scroll_PARM_4
                                    348 	.globl _oled_hor_scroll_PARM_3
                                    349 	.globl _oled_hor_scroll_PARM_2
                                    350 	.globl _Set_Light_PARM_3
                                    351 	.globl _Set_Light_PARM_2
                                    352 ;--------------------------------------------------------
                                    353 ; special function registers
                                    354 ;--------------------------------------------------------
                                    355 	.area RSEG    (ABS,DATA)
      000000                        356 	.org 0x0000
                           0000D0   357 _PSW	=	0x00d0
                           0000E0   358 _ACC	=	0x00e0
                           0000F0   359 _B	=	0x00f0
                           0000FD   360 _A_INV	=	0x00fd
                           000081   361 _SP	=	0x0081
                           000082   362 _DPL	=	0x0082
                           000083   363 _DPH	=	0x0083
                           0000A1   364 _SAFE_MOD	=	0x00a1
                           0000B1   365 _GLOBAL_CFG	=	0x00b1
                           000087   366 _PCON	=	0x0087
                           0000BA   367 _POWER_CFG	=	0x00ba
                           0000B9   368 _CLOCK_CFG	=	0x00b9
                           0000A9   369 _WAKE_CTRL	=	0x00a9
                           0000FE   370 _RESET_KEEP	=	0x00fe
                           0000FF   371 _WDOG_COUNT	=	0x00ff
                           0000A8   372 _IE	=	0x00a8
                           0000B8   373 _IP	=	0x00b8
                           0000E8   374 _IE_EX	=	0x00e8
                           0000E9   375 _IP_EX	=	0x00e9
                           0000B3   376 _INTX	=	0x00b3
                           0000B2   377 _GPIO_IE	=	0x00b2
                           008584   378 _ROM_ADDR	=	0x8584
                           000084   379 _ROM_ADDR_L	=	0x0084
                           000085   380 _ROM_ADDR_H	=	0x0085
                           008F8E   381 _ROM_DATA_HI	=	0x8f8e
                           00008E   382 _ROM_DATA_HL	=	0x008e
                           00008F   383 _ROM_DATA_HH	=	0x008f
                           000086   384 _ROM_CTRL	=	0x0086
                           000080   385 _P0	=	0x0080
                           0000C4   386 _P0_MOD_OC	=	0x00c4
                           0000C5   387 _P0_DIR_PU	=	0x00c5
                           000090   388 _P1	=	0x0090
                           000092   389 _P1_MOD_OC	=	0x0092
                           000093   390 _P1_DIR_PU	=	0x0093
                           0000A0   391 _P2	=	0x00a0
                           000094   392 _P2_MOD_OC	=	0x0094
                           000095   393 _P2_DIR_PU	=	0x0095
                           0000B0   394 _P3	=	0x00b0
                           000096   395 _P3_MOD_OC	=	0x0096
                           000097   396 _P3_DIR_PU	=	0x0097
                           0000C0   397 _P4	=	0x00c0
                           0000C2   398 _P4_MOD_OC	=	0x00c2
                           0000C3   399 _P4_DIR_PU	=	0x00c3
                           0000AB   400 _P5	=	0x00ab
                           0000AA   401 _PIN_FUNC	=	0x00aa
                           0000A2   402 _XBUS_AUX	=	0x00a2
                           000088   403 _TCON	=	0x0088
                           000089   404 _TMOD	=	0x0089
                           00008A   405 _TL0	=	0x008a
                           00008B   406 _TL1	=	0x008b
                           00008C   407 _TH0	=	0x008c
                           00008D   408 _TH1	=	0x008d
                           000098   409 _SCON	=	0x0098
                           000099   410 _SBUF	=	0x0099
                           0000C1   411 _T2CON2	=	0x00c1
                           00C7C6   412 _T2CAP0	=	0xc7c6
                           0000C6   413 _T2CAP0L	=	0x00c6
                           0000C7   414 _T2CAP0H	=	0x00c7
                           0000C8   415 _T2CON	=	0x00c8
                           0000C9   416 _T2MOD	=	0x00c9
                           00CBCA   417 _RCAP2	=	0xcbca
                           0000CA   418 _RCAP2L	=	0x00ca
                           0000CB   419 _RCAP2H	=	0x00cb
                           00CDCC   420 _T2COUNT	=	0xcdcc
                           0000CC   421 _TL2	=	0x00cc
                           0000CD   422 _TH2	=	0x00cd
                           00CFCE   423 _T2CAP1	=	0xcfce
                           0000CE   424 _T2CAP1L	=	0x00ce
                           0000CF   425 _T2CAP1H	=	0x00cf
                           00009A   426 _PWM_DATA2	=	0x009a
                           00009B   427 _PWM_DATA1	=	0x009b
                           00009C   428 _PWM_DATA0	=	0x009c
                           00009D   429 _PWM_CTRL	=	0x009d
                           00009E   430 _PWM_CK_SE	=	0x009e
                           00009F   431 _PWM_CTRL2	=	0x009f
                           0000A3   432 _PWM_DATA3	=	0x00a3
                           0000A4   433 _PWM_DATA4	=	0x00a4
                           0000A5   434 _PWM_DATA5	=	0x00a5
                           0000A6   435 _PWM_DATA6	=	0x00a6
                           0000A7   436 _PWM_DATA7	=	0x00a7
                           0000F8   437 _SPI0_STAT	=	0x00f8
                           0000F9   438 _SPI0_DATA	=	0x00f9
                           0000FA   439 _SPI0_CTRL	=	0x00fa
                           0000FB   440 _SPI0_CK_SE	=	0x00fb
                           0000FC   441 _SPI0_SETUP	=	0x00fc
                           0000BC   442 _SCON1	=	0x00bc
                           0000BD   443 _SBUF1	=	0x00bd
                           0000BE   444 _SBAUD1	=	0x00be
                           0000BF   445 _SIF1	=	0x00bf
                           0000B4   446 _SCON2	=	0x00b4
                           0000B5   447 _SBUF2	=	0x00b5
                           0000B6   448 _SBAUD2	=	0x00b6
                           0000B7   449 _SIF2	=	0x00b7
                           0000AC   450 _SCON3	=	0x00ac
                           0000AD   451 _SBUF3	=	0x00ad
                           0000AE   452 _SBAUD3	=	0x00ae
                           0000AF   453 _SIF3	=	0x00af
                           0000F1   454 _TKEY_CTRL	=	0x00f1
                           0000F2   455 _ADC_CTRL	=	0x00f2
                           0000F3   456 _ADC_CFG	=	0x00f3
                           00F5F4   457 _ADC_DAT	=	0xf5f4
                           0000F4   458 _ADC_DAT_L	=	0x00f4
                           0000F5   459 _ADC_DAT_H	=	0x00f5
                           0000F6   460 _ADC_CHAN	=	0x00f6
                           0000F7   461 _ADC_PIN	=	0x00f7
                           000091   462 _USB_C_CTRL	=	0x0091
                           0000D1   463 _UDEV_CTRL	=	0x00d1
                           0000D2   464 _UEP1_CTRL	=	0x00d2
                           0000D3   465 _UEP1_T_LEN	=	0x00d3
                           0000D4   466 _UEP2_CTRL	=	0x00d4
                           0000D5   467 _UEP2_T_LEN	=	0x00d5
                           0000D6   468 _UEP3_CTRL	=	0x00d6
                           0000D7   469 _UEP3_T_LEN	=	0x00d7
                           0000D8   470 _USB_INT_FG	=	0x00d8
                           0000D9   471 _USB_INT_ST	=	0x00d9
                           0000DA   472 _USB_MIS_ST	=	0x00da
                           0000DB   473 _USB_RX_LEN	=	0x00db
                           0000DC   474 _UEP0_CTRL	=	0x00dc
                           0000DD   475 _UEP0_T_LEN	=	0x00dd
                           0000DE   476 _UEP4_CTRL	=	0x00de
                           0000DF   477 _UEP4_T_LEN	=	0x00df
                           0000E1   478 _USB_INT_EN	=	0x00e1
                           0000E2   479 _USB_CTRL	=	0x00e2
                           0000E3   480 _USB_DEV_AD	=	0x00e3
                           00E5E4   481 _UEP2_DMA	=	0xe5e4
                           0000E4   482 _UEP2_DMA_L	=	0x00e4
                           0000E5   483 _UEP2_DMA_H	=	0x00e5
                           00E7E6   484 _UEP3_DMA	=	0xe7e6
                           0000E6   485 _UEP3_DMA_L	=	0x00e6
                           0000E7   486 _UEP3_DMA_H	=	0x00e7
                           0000EA   487 _UEP4_1_MOD	=	0x00ea
                           0000EB   488 _UEP2_3_MOD	=	0x00eb
                           00EDEC   489 _UEP0_DMA	=	0xedec
                           0000EC   490 _UEP0_DMA_L	=	0x00ec
                           0000ED   491 _UEP0_DMA_H	=	0x00ed
                           00EFEE   492 _UEP1_DMA	=	0xefee
                           0000EE   493 _UEP1_DMA_L	=	0x00ee
                           0000EF   494 _UEP1_DMA_H	=	0x00ef
                                    495 ;--------------------------------------------------------
                                    496 ; special function bits
                                    497 ;--------------------------------------------------------
                                    498 	.area RSEG    (ABS,DATA)
      000000                        499 	.org 0x0000
                           0000D7   500 _CY	=	0x00d7
                           0000D6   501 _AC	=	0x00d6
                           0000D5   502 _F0	=	0x00d5
                           0000D4   503 _RS1	=	0x00d4
                           0000D3   504 _RS0	=	0x00d3
                           0000D2   505 _OV	=	0x00d2
                           0000D1   506 _F1	=	0x00d1
                           0000D0   507 _P	=	0x00d0
                           0000AF   508 _EA	=	0x00af
                           0000AE   509 _E_DIS	=	0x00ae
                           0000AD   510 _ET2	=	0x00ad
                           0000AC   511 _ES	=	0x00ac
                           0000AB   512 _ET1	=	0x00ab
                           0000AA   513 _EX1	=	0x00aa
                           0000A9   514 _ET0	=	0x00a9
                           0000A8   515 _EX0	=	0x00a8
                           0000BF   516 _PH_FLAG	=	0x00bf
                           0000BE   517 _PL_FLAG	=	0x00be
                           0000BD   518 _PT2	=	0x00bd
                           0000BC   519 _PS	=	0x00bc
                           0000BB   520 _PT1	=	0x00bb
                           0000BA   521 _PX1	=	0x00ba
                           0000B9   522 _PT0	=	0x00b9
                           0000B8   523 _PX0	=	0x00b8
                           0000EF   524 _IE_WDOG	=	0x00ef
                           0000EE   525 _IE_GPIO	=	0x00ee
                           0000ED   526 _IE_PWMX	=	0x00ed
                           0000ED   527 _IE_UART3	=	0x00ed
                           0000EC   528 _IE_UART1	=	0x00ec
                           0000EB   529 _IE_ADC	=	0x00eb
                           0000EB   530 _IE_UART2	=	0x00eb
                           0000EA   531 _IE_USB	=	0x00ea
                           0000E9   532 _IE_INT3	=	0x00e9
                           0000E8   533 _IE_SPI0	=	0x00e8
                           000087   534 _P0_7	=	0x0087
                           000086   535 _P0_6	=	0x0086
                           000085   536 _P0_5	=	0x0085
                           000084   537 _P0_4	=	0x0084
                           000083   538 _P0_3	=	0x0083
                           000082   539 _P0_2	=	0x0082
                           000081   540 _P0_1	=	0x0081
                           000080   541 _P0_0	=	0x0080
                           000087   542 _TXD3	=	0x0087
                           000087   543 _AIN15	=	0x0087
                           000086   544 _RXD3	=	0x0086
                           000086   545 _AIN14	=	0x0086
                           000085   546 _TXD2	=	0x0085
                           000085   547 _AIN13	=	0x0085
                           000084   548 _RXD2	=	0x0084
                           000084   549 _AIN12	=	0x0084
                           000083   550 _TXD_	=	0x0083
                           000083   551 _AIN11	=	0x0083
                           000082   552 _RXD_	=	0x0082
                           000082   553 _AIN10	=	0x0082
                           000081   554 _AIN9	=	0x0081
                           000080   555 _AIN8	=	0x0080
                           000097   556 _P1_7	=	0x0097
                           000096   557 _P1_6	=	0x0096
                           000095   558 _P1_5	=	0x0095
                           000094   559 _P1_4	=	0x0094
                           000093   560 _P1_3	=	0x0093
                           000092   561 _P1_2	=	0x0092
                           000091   562 _P1_1	=	0x0091
                           000090   563 _P1_0	=	0x0090
                           000097   564 _SCK	=	0x0097
                           000097   565 _TXD1_	=	0x0097
                           000097   566 _AIN7	=	0x0097
                           000096   567 _MISO	=	0x0096
                           000096   568 _RXD1_	=	0x0096
                           000096   569 _VBUS	=	0x0096
                           000096   570 _AIN6	=	0x0096
                           000095   571 _MOSI	=	0x0095
                           000095   572 _PWM0_	=	0x0095
                           000095   573 _UCC2	=	0x0095
                           000095   574 _AIN5	=	0x0095
                           000094   575 _SCS	=	0x0094
                           000094   576 _UCC1	=	0x0094
                           000094   577 _AIN4	=	0x0094
                           000093   578 _AIN3	=	0x0093
                           000092   579 _AIN2	=	0x0092
                           000091   580 _T2EX	=	0x0091
                           000091   581 _CAP2	=	0x0091
                           000091   582 _AIN1	=	0x0091
                           000090   583 _T2	=	0x0090
                           000090   584 _CAP1	=	0x0090
                           000090   585 _AIN0	=	0x0090
                           0000A7   586 _P2_7	=	0x00a7
                           0000A6   587 _P2_6	=	0x00a6
                           0000A5   588 _P2_5	=	0x00a5
                           0000A4   589 _P2_4	=	0x00a4
                           0000A3   590 _P2_3	=	0x00a3
                           0000A2   591 _P2_2	=	0x00a2
                           0000A1   592 _P2_1	=	0x00a1
                           0000A0   593 _P2_0	=	0x00a0
                           0000A7   594 _PWM7	=	0x00a7
                           0000A7   595 _TXD1	=	0x00a7
                           0000A6   596 _PWM6	=	0x00a6
                           0000A6   597 _RXD1	=	0x00a6
                           0000A5   598 _PWM0	=	0x00a5
                           0000A5   599 _T2EX_	=	0x00a5
                           0000A5   600 _CAP2_	=	0x00a5
                           0000A4   601 _PWM1	=	0x00a4
                           0000A4   602 _T2_	=	0x00a4
                           0000A4   603 _CAP1_	=	0x00a4
                           0000A3   604 _PWM2	=	0x00a3
                           0000A2   605 _PWM3	=	0x00a2
                           0000A2   606 _INT0_	=	0x00a2
                           0000A1   607 _PWM4	=	0x00a1
                           0000A0   608 _PWM5	=	0x00a0
                           0000B7   609 _P3_7	=	0x00b7
                           0000B6   610 _P3_6	=	0x00b6
                           0000B5   611 _P3_5	=	0x00b5
                           0000B4   612 _P3_4	=	0x00b4
                           0000B3   613 _P3_3	=	0x00b3
                           0000B2   614 _P3_2	=	0x00b2
                           0000B1   615 _P3_1	=	0x00b1
                           0000B0   616 _P3_0	=	0x00b0
                           0000B7   617 _INT3	=	0x00b7
                           0000B6   618 _CAP0	=	0x00b6
                           0000B5   619 _T1	=	0x00b5
                           0000B4   620 _T0	=	0x00b4
                           0000B3   621 _INT1	=	0x00b3
                           0000B2   622 _INT0	=	0x00b2
                           0000B1   623 _TXD	=	0x00b1
                           0000B0   624 _RXD	=	0x00b0
                           0000C6   625 _P4_6	=	0x00c6
                           0000C5   626 _P4_5	=	0x00c5
                           0000C4   627 _P4_4	=	0x00c4
                           0000C3   628 _P4_3	=	0x00c3
                           0000C2   629 _P4_2	=	0x00c2
                           0000C1   630 _P4_1	=	0x00c1
                           0000C0   631 _P4_0	=	0x00c0
                           0000C7   632 _XO	=	0x00c7
                           0000C6   633 _XI	=	0x00c6
                           00008F   634 _TF1	=	0x008f
                           00008E   635 _TR1	=	0x008e
                           00008D   636 _TF0	=	0x008d
                           00008C   637 _TR0	=	0x008c
                           00008B   638 _IE1	=	0x008b
                           00008A   639 _IT1	=	0x008a
                           000089   640 _IE0	=	0x0089
                           000088   641 _IT0	=	0x0088
                           00009F   642 _SM0	=	0x009f
                           00009E   643 _SM1	=	0x009e
                           00009D   644 _SM2	=	0x009d
                           00009C   645 _REN	=	0x009c
                           00009B   646 _TB8	=	0x009b
                           00009A   647 _RB8	=	0x009a
                           000099   648 _TI	=	0x0099
                           000098   649 _RI	=	0x0098
                           0000CF   650 _TF2	=	0x00cf
                           0000CF   651 _CAP1F	=	0x00cf
                           0000CE   652 _EXF2	=	0x00ce
                           0000CD   653 _RCLK	=	0x00cd
                           0000CC   654 _TCLK	=	0x00cc
                           0000CB   655 _EXEN2	=	0x00cb
                           0000CA   656 _TR2	=	0x00ca
                           0000C9   657 _C_T2	=	0x00c9
                           0000C8   658 _CP_RL2	=	0x00c8
                           0000FF   659 _S0_FST_ACT	=	0x00ff
                           0000FE   660 _S0_IF_OV	=	0x00fe
                           0000FD   661 _S0_IF_FIRST	=	0x00fd
                           0000FC   662 _S0_IF_BYTE	=	0x00fc
                           0000FB   663 _S0_FREE	=	0x00fb
                           0000FA   664 _S0_T_FIFO	=	0x00fa
                           0000F8   665 _S0_R_FIFO	=	0x00f8
                           0000DF   666 _U_IS_NAK	=	0x00df
                           0000DE   667 _U_TOG_OK	=	0x00de
                           0000DD   668 _U_SIE_FREE	=	0x00dd
                           0000DC   669 _UIF_FIFO_OV	=	0x00dc
                           0000DB   670 _UIF_HST_SOF	=	0x00db
                           0000DA   671 _UIF_SUSPEND	=	0x00da
                           0000D9   672 _UIF_TRANSFER	=	0x00d9
                           0000D8   673 _UIF_DETECT	=	0x00d8
                           0000D8   674 _UIF_BUS_RST	=	0x00d8
                                    675 ;--------------------------------------------------------
                                    676 ; overlayable register banks
                                    677 ;--------------------------------------------------------
                                    678 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        679 	.ds 8
                                    680 ;--------------------------------------------------------
                                    681 ; overlayable bit register bank
                                    682 ;--------------------------------------------------------
                                    683 	.area BIT_BANK	(REL,OVR,DATA)
      000020                        684 bits:
      000020                        685 	.ds 1
                           008000   686 	b0 = bits[0]
                           008100   687 	b1 = bits[1]
                           008200   688 	b2 = bits[2]
                           008300   689 	b3 = bits[3]
                           008400   690 	b4 = bits[4]
                           008500   691 	b5 = bits[5]
                           008600   692 	b6 = bits[6]
                           008700   693 	b7 = bits[7]
                                    694 ;--------------------------------------------------------
                                    695 ; internal ram data
                                    696 ;--------------------------------------------------------
                                    697 	.area DSEG    (DATA)
      000008                        698 _Set_Light_PARM_2:
      000008                        699 	.ds 1
      000009                        700 _Set_Light_PARM_3:
      000009                        701 	.ds 1
      00000A                        702 _oled_hor_scroll_PARM_2:
      00000A                        703 	.ds 1
      00000B                        704 _oled_hor_scroll_PARM_3:
      00000B                        705 	.ds 1
      00000C                        706 _oled_hor_scroll_PARM_4:
      00000C                        707 	.ds 1
      00000D                        708 _F::
      00000D                        709 	.ds 1
      00000E                        710 _led::
      00000E                        711 	.ds 1
                                    712 ;--------------------------------------------------------
                                    713 ; overlayable items in internal ram 
                                    714 ;--------------------------------------------------------
                                    715 	.area	OSEG    (OVR,DATA)
      00000F                        716 _Send_2811_24bits_PARM_2:
      00000F                        717 	.ds 1
      000010                        718 _Send_2811_24bits_PARM_3:
      000010                        719 	.ds 1
                                    720 ;--------------------------------------------------------
                                    721 ; Stack segment in internal ram 
                                    722 ;--------------------------------------------------------
                                    723 	.area	SSEG
      000035                        724 __start__stack:
      000035                        725 	.ds	1
                                    726 
                                    727 ;--------------------------------------------------------
                                    728 ; indirectly addressable internal ram data
                                    729 ;--------------------------------------------------------
                                    730 	.area ISEG    (DATA)
                                    731 ;--------------------------------------------------------
                                    732 ; absolute internal ram data
                                    733 ;--------------------------------------------------------
                                    734 	.area IABS    (ABS,DATA)
                                    735 	.area IABS    (ABS,DATA)
                                    736 ;--------------------------------------------------------
                                    737 ; bit data
                                    738 ;--------------------------------------------------------
                                    739 	.area BSEG    (BIT)
                                    740 ;--------------------------------------------------------
                                    741 ; paged external ram data
                                    742 ;--------------------------------------------------------
                                    743 	.area PSEG    (PAG,XDATA)
                                    744 ;--------------------------------------------------------
                                    745 ; external ram data
                                    746 ;--------------------------------------------------------
                                    747 	.area XSEG    (XDATA)
                                    748 ;--------------------------------------------------------
                                    749 ; absolute external ram data
                                    750 ;--------------------------------------------------------
                                    751 	.area XABS    (ABS,XDATA)
                                    752 ;--------------------------------------------------------
                                    753 ; external initialized ram data
                                    754 ;--------------------------------------------------------
                                    755 	.area XISEG   (XDATA)
                                    756 	.area HOME    (CODE)
                                    757 	.area GSINIT0 (CODE)
                                    758 	.area GSINIT1 (CODE)
                                    759 	.area GSINIT2 (CODE)
                                    760 	.area GSINIT3 (CODE)
                                    761 	.area GSINIT4 (CODE)
                                    762 	.area GSINIT5 (CODE)
                                    763 	.area GSINIT  (CODE)
                                    764 	.area GSFINAL (CODE)
                                    765 	.area CSEG    (CODE)
                                    766 ;--------------------------------------------------------
                                    767 ; interrupt vector 
                                    768 ;--------------------------------------------------------
                                    769 	.area HOME    (CODE)
      000000                        770 __interrupt_vect:
      000000 02 00 09         [24]  771 	ljmp	__sdcc_gsinit_startup
      000003 02 03 91         [24]  772 	ljmp	_Int0
                                    773 ;--------------------------------------------------------
                                    774 ; global & static initialisations
                                    775 ;--------------------------------------------------------
                                    776 	.area HOME    (CODE)
                                    777 	.area GSINIT  (CODE)
                                    778 	.area GSFINAL (CODE)
                                    779 	.area GSINIT  (CODE)
                                    780 	.globl __sdcc_gsinit_startup
                                    781 	.globl __sdcc_program_startup
                                    782 	.globl __start__stack
                                    783 	.globl __mcs51_genXINIT
                                    784 	.globl __mcs51_genXRAMCLEAR
                                    785 	.globl __mcs51_genRAMCLEAR
                                    786 ;	usr/main.c:179: volatile UINT8 led = 0;  //定义灯的四种情况
      000062 75 0E 00         [24]  787 	mov	_led,#0x00
                                    788 	.area GSFINAL (CODE)
      000065 02 00 06         [24]  789 	ljmp	__sdcc_program_startup
                                    790 ;--------------------------------------------------------
                                    791 ; Home
                                    792 ;--------------------------------------------------------
                                    793 	.area HOME    (CODE)
                                    794 	.area HOME    (CODE)
      000006                        795 __sdcc_program_startup:
      000006 02 02 40         [24]  796 	ljmp	_main
                                    797 ;	return from main will return to caller
                                    798 ;--------------------------------------------------------
                                    799 ; code
                                    800 ;--------------------------------------------------------
                                    801 	.area CSEG    (CODE)
                                    802 ;------------------------------------------------------------
                                    803 ;Allocation info for local variables in function 'Send_2811_24bits'
                                    804 ;------------------------------------------------------------
                                    805 ;R8                        Allocated with name '_Send_2811_24bits_PARM_2'
                                    806 ;B8                        Allocated with name '_Send_2811_24bits_PARM_3'
                                    807 ;G8                        Allocated to registers r7 
                                    808 ;n                         Allocated to registers r6 
                                    809 ;------------------------------------------------------------
                                    810 ;	usr/main.c:76: void Send_2811_24bits(unsigned char G8,unsigned char R8,unsigned char B8)
                                    811 ;	-----------------------------------------
                                    812 ;	 function Send_2811_24bits
                                    813 ;	-----------------------------------------
      000068                        814 _Send_2811_24bits:
                           000007   815 	ar7 = 0x07
                           000006   816 	ar6 = 0x06
                           000005   817 	ar5 = 0x05
                           000004   818 	ar4 = 0x04
                           000003   819 	ar3 = 0x03
                           000002   820 	ar2 = 0x02
                           000001   821 	ar1 = 0x01
                           000000   822 	ar0 = 0x00
      000068 AF 82            [24]  823 	mov	r7,dpl
                                    824 ;	usr/main.c:81: for(n=0;n<8;n++)
      00006A 7E 00            [12]  825 	mov	r6,#0x00
      00006C                        826 00131$:
                                    827 ;	usr/main.c:84: if(G8&0x80)
      00006C EF               [12]  828 	mov	a,r7
      00006D 30 E7 2E         [24]  829 	jnb	acc.7,00104$
                                    830 ;	usr/main.c:86: RGB_1();
                                    831 ;	assignBit
      000070 D2 A2            [12]  832 	setb	_P2_2
      000072 00               [12]  833 	NOP	
      000073 00               [12]  834 	NOP	
      000074 00               [12]  835 	NOP	
      000075 00               [12]  836 	NOP	
      000076 00               [12]  837 	NOP	
      000077 00               [12]  838 	NOP	
      000078 00               [12]  839 	NOP	
      000079 00               [12]  840 	NOP	
      00007A 00               [12]  841 	NOP	
      00007B 00               [12]  842 	NOP	
      00007C 00               [12]  843 	NOP	
      00007D 00               [12]  844 	NOP	
      00007E 00               [12]  845 	NOP	
      00007F 00               [12]  846 	NOP	
      000080 00               [12]  847 	NOP	
      000081 00               [12]  848 	NOP	
      000082 00               [12]  849 	NOP	
      000083 00               [12]  850 	NOP	
      000084 00               [12]  851 	NOP	
      000085 00               [12]  852 	NOP	
      000086 00               [12]  853 	NOP	
      000087 00               [12]  854 	NOP	
      000088 00               [12]  855 	NOP	
      000089 00               [12]  856 	NOP	
      00008A 00               [12]  857 	NOP	
      00008B 00               [12]  858 	NOP	
      00008C 00               [12]  859 	NOP	
      00008D 00               [12]  860 	NOP	
      00008E 00               [12]  861 	NOP	
      00008F 00               [12]  862 	NOP	
      000090 00               [12]  863 	NOP	
      000091 00               [12]  864 	NOP	
      000092 00               [12]  865 	NOP	
      000093 00               [12]  866 	NOP	
      000094 00               [12]  867 	NOP	
      000095 00               [12]  868 	NOP	
      000096 00               [12]  869 	NOP	
      000097 00               [12]  870 	NOP	
      000098 00               [12]  871 	NOP	
      000099 00               [12]  872 	NOP	
                                    873 ;	assignBit
      00009A C2 A2            [12]  874 	clr	_P2_2
                                    875 ;	usr/main.c:90: RGB_0();
      00009C 80 27            [24]  876 	sjmp	00109$
      00009E                        877 00104$:
                                    878 ;	assignBit
      00009E D2 A2            [12]  879 	setb	_P2_2
      0000A0 00               [12]  880 	NOP	
      0000A1 00               [12]  881 	NOP	
      0000A2 00               [12]  882 	NOP	
      0000A3 00               [12]  883 	NOP	
      0000A4 00               [12]  884 	NOP	
      0000A5 00               [12]  885 	NOP	
      0000A6 00               [12]  886 	NOP	
      0000A7 00               [12]  887 	NOP	
      0000A8 00               [12]  888 	NOP	
      0000A9 00               [12]  889 	NOP	
      0000AA 00               [12]  890 	NOP	
      0000AB 00               [12]  891 	NOP	
      0000AC 00               [12]  892 	NOP	
      0000AD 00               [12]  893 	NOP	
      0000AE 00               [12]  894 	NOP	
      0000AF 00               [12]  895 	NOP	
      0000B0 00               [12]  896 	NOP	
      0000B1 00               [12]  897 	NOP	
      0000B2 00               [12]  898 	NOP	
      0000B3 00               [12]  899 	NOP	
                                    900 ;	assignBit
      0000B4 C2 A2            [12]  901 	clr	_P2_2
      0000B6 00               [12]  902 	NOP	
      0000B7 00               [12]  903 	NOP	
      0000B8 00               [12]  904 	NOP	
      0000B9 00               [12]  905 	NOP	
      0000BA 00               [12]  906 	NOP	
      0000BB 00               [12]  907 	NOP	
      0000BC 00               [12]  908 	NOP	
      0000BD 00               [12]  909 	NOP	
      0000BE 00               [12]  910 	NOP	
      0000BF 00               [12]  911 	NOP	
      0000C0 00               [12]  912 	NOP	
      0000C1 00               [12]  913 	NOP	
      0000C2 00               [12]  914 	NOP	
      0000C3 00               [12]  915 	NOP	
      0000C4 00               [12]  916 	NOP	
      0000C5                        917 00109$:
                                    918 ;	usr/main.c:92: G8<<=1;
      0000C5 8F 05            [24]  919 	mov	ar5,r7
      0000C7 ED               [12]  920 	mov	a,r5
      0000C8 2D               [12]  921 	add	a,r5
      0000C9 FF               [12]  922 	mov	r7,a
                                    923 ;	usr/main.c:81: for(n=0;n<8;n++)
      0000CA 0E               [12]  924 	inc	r6
      0000CB BE 08 00         [24]  925 	cjne	r6,#0x08,00175$
      0000CE                        926 00175$:
      0000CE 40 9C            [24]  927 	jc	00131$
                                    928 ;	usr/main.c:95: for(n=0;n<8;n++)
      0000D0 7F 00            [12]  929 	mov	r7,#0x00
      0000D2                        930 00133$:
                                    931 ;	usr/main.c:98: if(R8&0x80)
      0000D2 E5 0F            [12]  932 	mov	a,_Send_2811_24bits_PARM_2
      0000D4 30 E7 2E         [24]  933 	jnb	acc.7,00114$
                                    934 ;	usr/main.c:100: RGB_1();
                                    935 ;	assignBit
      0000D7 D2 A2            [12]  936 	setb	_P2_2
      0000D9 00               [12]  937 	NOP	
      0000DA 00               [12]  938 	NOP	
      0000DB 00               [12]  939 	NOP	
      0000DC 00               [12]  940 	NOP	
      0000DD 00               [12]  941 	NOP	
      0000DE 00               [12]  942 	NOP	
      0000DF 00               [12]  943 	NOP	
      0000E0 00               [12]  944 	NOP	
      0000E1 00               [12]  945 	NOP	
      0000E2 00               [12]  946 	NOP	
      0000E3 00               [12]  947 	NOP	
      0000E4 00               [12]  948 	NOP	
      0000E5 00               [12]  949 	NOP	
      0000E6 00               [12]  950 	NOP	
      0000E7 00               [12]  951 	NOP	
      0000E8 00               [12]  952 	NOP	
      0000E9 00               [12]  953 	NOP	
      0000EA 00               [12]  954 	NOP	
      0000EB 00               [12]  955 	NOP	
      0000EC 00               [12]  956 	NOP	
      0000ED 00               [12]  957 	NOP	
      0000EE 00               [12]  958 	NOP	
      0000EF 00               [12]  959 	NOP	
      0000F0 00               [12]  960 	NOP	
      0000F1 00               [12]  961 	NOP	
      0000F2 00               [12]  962 	NOP	
      0000F3 00               [12]  963 	NOP	
      0000F4 00               [12]  964 	NOP	
      0000F5 00               [12]  965 	NOP	
      0000F6 00               [12]  966 	NOP	
      0000F7 00               [12]  967 	NOP	
      0000F8 00               [12]  968 	NOP	
      0000F9 00               [12]  969 	NOP	
      0000FA 00               [12]  970 	NOP	
      0000FB 00               [12]  971 	NOP	
      0000FC 00               [12]  972 	NOP	
      0000FD 00               [12]  973 	NOP	
      0000FE 00               [12]  974 	NOP	
      0000FF 00               [12]  975 	NOP	
      000100 00               [12]  976 	NOP	
                                    977 ;	assignBit
      000101 C2 A2            [12]  978 	clr	_P2_2
                                    979 ;	usr/main.c:104: RGB_0();
      000103 80 27            [24]  980 	sjmp	00119$
      000105                        981 00114$:
                                    982 ;	assignBit
      000105 D2 A2            [12]  983 	setb	_P2_2
      000107 00               [12]  984 	NOP	
      000108 00               [12]  985 	NOP	
      000109 00               [12]  986 	NOP	
      00010A 00               [12]  987 	NOP	
      00010B 00               [12]  988 	NOP	
      00010C 00               [12]  989 	NOP	
      00010D 00               [12]  990 	NOP	
      00010E 00               [12]  991 	NOP	
      00010F 00               [12]  992 	NOP	
      000110 00               [12]  993 	NOP	
      000111 00               [12]  994 	NOP	
      000112 00               [12]  995 	NOP	
      000113 00               [12]  996 	NOP	
      000114 00               [12]  997 	NOP	
      000115 00               [12]  998 	NOP	
      000116 00               [12]  999 	NOP	
      000117 00               [12] 1000 	NOP	
      000118 00               [12] 1001 	NOP	
      000119 00               [12] 1002 	NOP	
      00011A 00               [12] 1003 	NOP	
                                   1004 ;	assignBit
      00011B C2 A2            [12] 1005 	clr	_P2_2
      00011D 00               [12] 1006 	NOP	
      00011E 00               [12] 1007 	NOP	
      00011F 00               [12] 1008 	NOP	
      000120 00               [12] 1009 	NOP	
      000121 00               [12] 1010 	NOP	
      000122 00               [12] 1011 	NOP	
      000123 00               [12] 1012 	NOP	
      000124 00               [12] 1013 	NOP	
      000125 00               [12] 1014 	NOP	
      000126 00               [12] 1015 	NOP	
      000127 00               [12] 1016 	NOP	
      000128 00               [12] 1017 	NOP	
      000129 00               [12] 1018 	NOP	
      00012A 00               [12] 1019 	NOP	
      00012B 00               [12] 1020 	NOP	
      00012C                       1021 00119$:
                                   1022 ;	usr/main.c:106: R8<<=1;
      00012C E5 0F            [12] 1023 	mov	a,_Send_2811_24bits_PARM_2
      00012E FE               [12] 1024 	mov	r6,a
      00012F 25 E0            [12] 1025 	add	a,acc
      000131 F5 0F            [12] 1026 	mov	_Send_2811_24bits_PARM_2,a
                                   1027 ;	usr/main.c:95: for(n=0;n<8;n++)
      000133 0F               [12] 1028 	inc	r7
      000134 BF 08 00         [24] 1029 	cjne	r7,#0x08,00178$
      000137                       1030 00178$:
      000137 40 99            [24] 1031 	jc	00133$
                                   1032 ;	usr/main.c:109: for(n=0;n<8;n++)
      000139 7F 00            [12] 1033 	mov	r7,#0x00
      00013B                       1034 00135$:
                                   1035 ;	usr/main.c:112: if(B8&0x80)
      00013B E5 10            [12] 1036 	mov	a,_Send_2811_24bits_PARM_3
      00013D 30 E7 2E         [24] 1037 	jnb	acc.7,00124$
                                   1038 ;	usr/main.c:114: RGB_1();
                                   1039 ;	assignBit
      000140 D2 A2            [12] 1040 	setb	_P2_2
      000142 00               [12] 1041 	NOP	
      000143 00               [12] 1042 	NOP	
      000144 00               [12] 1043 	NOP	
      000145 00               [12] 1044 	NOP	
      000146 00               [12] 1045 	NOP	
      000147 00               [12] 1046 	NOP	
      000148 00               [12] 1047 	NOP	
      000149 00               [12] 1048 	NOP	
      00014A 00               [12] 1049 	NOP	
      00014B 00               [12] 1050 	NOP	
      00014C 00               [12] 1051 	NOP	
      00014D 00               [12] 1052 	NOP	
      00014E 00               [12] 1053 	NOP	
      00014F 00               [12] 1054 	NOP	
      000150 00               [12] 1055 	NOP	
      000151 00               [12] 1056 	NOP	
      000152 00               [12] 1057 	NOP	
      000153 00               [12] 1058 	NOP	
      000154 00               [12] 1059 	NOP	
      000155 00               [12] 1060 	NOP	
      000156 00               [12] 1061 	NOP	
      000157 00               [12] 1062 	NOP	
      000158 00               [12] 1063 	NOP	
      000159 00               [12] 1064 	NOP	
      00015A 00               [12] 1065 	NOP	
      00015B 00               [12] 1066 	NOP	
      00015C 00               [12] 1067 	NOP	
      00015D 00               [12] 1068 	NOP	
      00015E 00               [12] 1069 	NOP	
      00015F 00               [12] 1070 	NOP	
      000160 00               [12] 1071 	NOP	
      000161 00               [12] 1072 	NOP	
      000162 00               [12] 1073 	NOP	
      000163 00               [12] 1074 	NOP	
      000164 00               [12] 1075 	NOP	
      000165 00               [12] 1076 	NOP	
      000166 00               [12] 1077 	NOP	
      000167 00               [12] 1078 	NOP	
      000168 00               [12] 1079 	NOP	
      000169 00               [12] 1080 	NOP	
                                   1081 ;	assignBit
      00016A C2 A2            [12] 1082 	clr	_P2_2
                                   1083 ;	usr/main.c:118: RGB_0();
      00016C 80 27            [24] 1084 	sjmp	00129$
      00016E                       1085 00124$:
                                   1086 ;	assignBit
      00016E D2 A2            [12] 1087 	setb	_P2_2
      000170 00               [12] 1088 	NOP	
      000171 00               [12] 1089 	NOP	
      000172 00               [12] 1090 	NOP	
      000173 00               [12] 1091 	NOP	
      000174 00               [12] 1092 	NOP	
      000175 00               [12] 1093 	NOP	
      000176 00               [12] 1094 	NOP	
      000177 00               [12] 1095 	NOP	
      000178 00               [12] 1096 	NOP	
      000179 00               [12] 1097 	NOP	
      00017A 00               [12] 1098 	NOP	
      00017B 00               [12] 1099 	NOP	
      00017C 00               [12] 1100 	NOP	
      00017D 00               [12] 1101 	NOP	
      00017E 00               [12] 1102 	NOP	
      00017F 00               [12] 1103 	NOP	
      000180 00               [12] 1104 	NOP	
      000181 00               [12] 1105 	NOP	
      000182 00               [12] 1106 	NOP	
      000183 00               [12] 1107 	NOP	
                                   1108 ;	assignBit
      000184 C2 A2            [12] 1109 	clr	_P2_2
      000186 00               [12] 1110 	NOP	
      000187 00               [12] 1111 	NOP	
      000188 00               [12] 1112 	NOP	
      000189 00               [12] 1113 	NOP	
      00018A 00               [12] 1114 	NOP	
      00018B 00               [12] 1115 	NOP	
      00018C 00               [12] 1116 	NOP	
      00018D 00               [12] 1117 	NOP	
      00018E 00               [12] 1118 	NOP	
      00018F 00               [12] 1119 	NOP	
      000190 00               [12] 1120 	NOP	
      000191 00               [12] 1121 	NOP	
      000192 00               [12] 1122 	NOP	
      000193 00               [12] 1123 	NOP	
      000194 00               [12] 1124 	NOP	
      000195                       1125 00129$:
                                   1126 ;	usr/main.c:120: B8<<=1;
      000195 E5 10            [12] 1127 	mov	a,_Send_2811_24bits_PARM_3
      000197 FE               [12] 1128 	mov	r6,a
      000198 25 E0            [12] 1129 	add	a,acc
      00019A F5 10            [12] 1130 	mov	_Send_2811_24bits_PARM_3,a
                                   1131 ;	usr/main.c:109: for(n=0;n<8;n++)
      00019C 0F               [12] 1132 	inc	r7
      00019D BF 08 00         [24] 1133 	cjne	r7,#0x08,00181$
      0001A0                       1134 00181$:
      0001A0 40 99            [24] 1135 	jc	00135$
                                   1136 ;	usr/main.c:122: }
      0001A2 22               [24] 1137 	ret
                                   1138 ;------------------------------------------------------------
                                   1139 ;Allocation info for local variables in function 'RGB_Rst'
                                   1140 ;------------------------------------------------------------
                                   1141 ;	usr/main.c:125: void RGB_Rst()
                                   1142 ;	-----------------------------------------
                                   1143 ;	 function RGB_Rst
                                   1144 ;	-----------------------------------------
      0001A3                       1145 _RGB_Rst:
                                   1146 ;	usr/main.c:127: WS2812 = 0;
                                   1147 ;	assignBit
      0001A3 C2 A2            [12] 1148 	clr	_P2_2
                                   1149 ;	usr/main.c:128: mDelayuS( 50 );
      0001A5 90 00 32         [24] 1150 	mov	dptr,#0x0032
                                   1151 ;	usr/main.c:129: }
      0001A8 02 03 FE         [24] 1152 	ljmp	_mDelayuS
                                   1153 ;------------------------------------------------------------
                                   1154 ;Allocation info for local variables in function 'Set_Light'
                                   1155 ;------------------------------------------------------------
                                   1156 ;y                         Allocated with name '_Set_Light_PARM_2'
                                   1157 ;z                         Allocated with name '_Set_Light_PARM_3'
                                   1158 ;x                         Allocated to registers r7 
                                   1159 ;i                         Allocated to registers r6 
                                   1160 ;------------------------------------------------------------
                                   1161 ;	usr/main.c:132: void Set_Light(unsigned char x,unsigned char y,unsigned char z)
                                   1162 ;	-----------------------------------------
                                   1163 ;	 function Set_Light
                                   1164 ;	-----------------------------------------
      0001AB                       1165 _Set_Light:
      0001AB AF 82            [24] 1166 	mov	r7,dpl
                                   1167 ;	usr/main.c:135: for ( i = 0; i < numLEDs; i++)
      0001AD 7E 00            [12] 1168 	mov	r6,#0x00
      0001AF                       1169 00102$:
                                   1170 ;	usr/main.c:137: Send_2811_24bits( x , y , z );//发送显示
      0001AF 85 08 0F         [24] 1171 	mov	_Send_2811_24bits_PARM_2,_Set_Light_PARM_2
      0001B2 85 09 10         [24] 1172 	mov	_Send_2811_24bits_PARM_3,_Set_Light_PARM_3
      0001B5 8F 82            [24] 1173 	mov	dpl,r7
      0001B7 C0 07            [24] 1174 	push	ar7
      0001B9 C0 06            [24] 1175 	push	ar6
      0001BB 12 00 68         [24] 1176 	lcall	_Send_2811_24bits
      0001BE D0 06            [24] 1177 	pop	ar6
      0001C0 D0 07            [24] 1178 	pop	ar7
                                   1179 ;	usr/main.c:135: for ( i = 0; i < numLEDs; i++)
      0001C2 0E               [12] 1180 	inc	r6
      0001C3 BE 0B 00         [24] 1181 	cjne	r6,#0x0b,00111$
      0001C6                       1182 00111$:
      0001C6 40 E7            [24] 1183 	jc	00102$
                                   1184 ;	usr/main.c:139: }
      0001C8 22               [24] 1185 	ret
                                   1186 ;------------------------------------------------------------
                                   1187 ;Allocation info for local variables in function 'oled_hor_scroll'
                                   1188 ;------------------------------------------------------------
                                   1189 ;end_page                  Allocated with name '_oled_hor_scroll_PARM_2'
                                   1190 ;frame                     Allocated with name '_oled_hor_scroll_PARM_3'
                                   1191 ;dir                       Allocated with name '_oled_hor_scroll_PARM_4'
                                   1192 ;start_page                Allocated to registers r7 
                                   1193 ;------------------------------------------------------------
                                   1194 ;	usr/main.c:149: void oled_hor_scroll(enum page_num start_page, enum page_num end_page, enum roll_frame frame, UINT8 dir)
                                   1195 ;	-----------------------------------------
                                   1196 ;	 function oled_hor_scroll
                                   1197 ;	-----------------------------------------
      0001C9                       1198 _oled_hor_scroll:
      0001C9 AF 82            [24] 1199 	mov	r7,dpl
                                   1200 ;	usr/main.c:151: OLED_WR_Byte(0x2E, OLED_CMD);                   /* 停止滚动 调用后,RAM数据需要重新写入 */
      0001CB 75 22 00         [24] 1201 	mov	_OLED_WR_Byte_PARM_2,#0x00
      0001CE 75 82 2E         [24] 1202 	mov	dpl,#0x2e
      0001D1 C0 07            [24] 1203 	push	ar7
      0001D3 12 04 CB         [24] 1204 	lcall	_OLED_WR_Byte
      0001D6 D0 07            [24] 1205 	pop	ar7
                                   1206 ;	usr/main.c:153: OLED_WR_Byte(dir ? 0x26 : 0x27, OLED_CMD);      /* 0,向右滚动 1,向左移动 */
      0001D8 E5 0C            [12] 1207 	mov	a,_oled_hor_scroll_PARM_4
      0001DA 60 06            [24] 1208 	jz	00103$
      0001DC 7D 26            [12] 1209 	mov	r5,#0x26
      0001DE 7E 00            [12] 1210 	mov	r6,#0x00
      0001E0 80 04            [24] 1211 	sjmp	00104$
      0001E2                       1212 00103$:
      0001E2 7D 27            [12] 1213 	mov	r5,#0x27
      0001E4 7E 00            [12] 1214 	mov	r6,#0x00
      0001E6                       1215 00104$:
      0001E6 8D 82            [24] 1216 	mov	dpl,r5
      0001E8 75 22 00         [24] 1217 	mov	_OLED_WR_Byte_PARM_2,#0x00
      0001EB C0 07            [24] 1218 	push	ar7
      0001ED 12 04 CB         [24] 1219 	lcall	_OLED_WR_Byte
                                   1220 ;	usr/main.c:154: OLED_WR_Byte(0x00, OLED_CMD);                   /* 发送空字节 设置为0x00即可 */
      0001F0 75 22 00         [24] 1221 	mov	_OLED_WR_Byte_PARM_2,#0x00
      0001F3 75 82 00         [24] 1222 	mov	dpl,#0x00
      0001F6 12 04 CB         [24] 1223 	lcall	_OLED_WR_Byte
      0001F9 D0 07            [24] 1224 	pop	ar7
                                   1225 ;	usr/main.c:155: OLED_WR_Byte(start_page & 0x07, OLED_CMD);      /* 起始页地址 */
      0001FB 74 07            [12] 1226 	mov	a,#0x07
      0001FD 5F               [12] 1227 	anl	a,r7
      0001FE F5 82            [12] 1228 	mov	dpl,a
      000200 75 22 00         [24] 1229 	mov	_OLED_WR_Byte_PARM_2,#0x00
      000203 12 04 CB         [24] 1230 	lcall	_OLED_WR_Byte
                                   1231 ;	usr/main.c:156: OLED_WR_Byte(frame & 0x07, OLED_CMD);           /* 设置滚动步长的时间间隔, 帧为单位 */
      000206 E5 0B            [12] 1232 	mov	a,_oled_hor_scroll_PARM_3
      000208 54 07            [12] 1233 	anl	a,#0x07
      00020A F5 82            [12] 1234 	mov	dpl,a
      00020C 75 22 00         [24] 1235 	mov	_OLED_WR_Byte_PARM_2,#0x00
      00020F 12 04 CB         [24] 1236 	lcall	_OLED_WR_Byte
                                   1237 ;	usr/main.c:157: OLED_WR_Byte(end_page & 0x07, OLED_CMD);        /* 终止页地址 */
      000212 E5 0A            [12] 1238 	mov	a,_oled_hor_scroll_PARM_2
      000214 54 07            [12] 1239 	anl	a,#0x07
      000216 F5 82            [12] 1240 	mov	dpl,a
      000218 75 22 00         [24] 1241 	mov	_OLED_WR_Byte_PARM_2,#0x00
      00021B 12 04 CB         [24] 1242 	lcall	_OLED_WR_Byte
                                   1243 ;	usr/main.c:159: OLED_WR_Byte(0x00, OLED_CMD);   /* 发送空字节 设置为0x00即可 */
      00021E 75 22 00         [24] 1244 	mov	_OLED_WR_Byte_PARM_2,#0x00
      000221 75 82 00         [24] 1245 	mov	dpl,#0x00
      000224 12 04 CB         [24] 1246 	lcall	_OLED_WR_Byte
                                   1247 ;	usr/main.c:160: OLED_WR_Byte(0xFF, OLED_CMD);   /* 发送空字节 设置为0xFF即可 */
      000227 75 22 00         [24] 1248 	mov	_OLED_WR_Byte_PARM_2,#0x00
      00022A 75 82 FF         [24] 1249 	mov	dpl,#0xff
      00022D 12 04 CB         [24] 1250 	lcall	_OLED_WR_Byte
                                   1251 ;	usr/main.c:161: OLED_WR_Byte(0x2F, OLED_CMD);   /* 启动滚动 禁止对RAM访问,改变水平滚动配置参数 */
      000230 75 22 00         [24] 1252 	mov	_OLED_WR_Byte_PARM_2,#0x00
      000233 75 82 2F         [24] 1253 	mov	dpl,#0x2f
                                   1254 ;	usr/main.c:162: }
      000236 02 04 CB         [24] 1255 	ljmp	_OLED_WR_Byte
                                   1256 ;------------------------------------------------------------
                                   1257 ;Allocation info for local variables in function 'Int0Init'
                                   1258 ;------------------------------------------------------------
                                   1259 ;	usr/main.c:170: void Int0Init()
                                   1260 ;	-----------------------------------------
                                   1261 ;	 function Int0Init
                                   1262 ;	-----------------------------------------
      000239                       1263 _Int0Init:
                                   1264 ;	usr/main.c:173: IT0=1;  //跳变沿触发方式（下降沿）
                                   1265 ;	assignBit
      000239 D2 88            [12] 1266 	setb	_IT0
                                   1267 ;	usr/main.c:174: EX0=1;  //打开INT0的中断允许。	
                                   1268 ;	assignBit
      00023B D2 A8            [12] 1269 	setb	_EX0
                                   1270 ;	usr/main.c:175: EA=1;   //打开总中断	
                                   1271 ;	assignBit
      00023D D2 AF            [12] 1272 	setb	_EA
                                   1273 ;	usr/main.c:176: }
      00023F 22               [24] 1274 	ret
                                   1275 ;------------------------------------------------------------
                                   1276 ;Allocation info for local variables in function 'main'
                                   1277 ;------------------------------------------------------------
                                   1278 ;	usr/main.c:187: void main(void)
                                   1279 ;	-----------------------------------------
                                   1280 ;	 function main
                                   1281 ;	-----------------------------------------
      000240                       1282 _main:
                                   1283 ;	usr/main.c:189: CfgFsys(); //CH549时钟选择配置
      000240 12 03 E0         [24] 1284 	lcall	_CfgFsys
                                   1285 ;	usr/main.c:190: mDelaymS(20);
      000243 90 00 14         [24] 1286 	mov	dptr,#0x0014
      000246 12 04 34         [24] 1287 	lcall	_mDelaymS
                                   1288 ;	usr/main.c:191: Int0Init();                                                                //对外部中断0（P3.2）初始化
      000249 12 02 39         [24] 1289 	lcall	_Int0Init
                                   1290 ;	usr/main.c:192: SPIMasterModeSet(3);                                                       //SPI主机模式设置，模式3
      00024C 75 82 03         [24] 1291 	mov	dpl,#0x03
      00024F 12 08 6E         [24] 1292 	lcall	_SPIMasterModeSet
                                   1293 ;	usr/main.c:193: SPI_CK_SET(12);                                                            //设置spi sclk 时钟信号为12分频
      000252 75 FB 0C         [24] 1294 	mov	_SPI0_CK_SE,#0x0c
                                   1295 ;	usr/main.c:194: OLED_Init();			                                                         //初始化OLED  
      000255 12 05 21         [24] 1296 	lcall	_OLED_Init
                                   1297 ;	usr/main.c:195: OLED_Clear();                                                              //将oled屏幕上内容清除
      000258 12 05 6F         [24] 1298 	lcall	_OLED_Clear
                                   1299 ;	usr/main.c:196: setFontSize(16);                                                           //设置文字大小
      00025B 75 82 10         [24] 1300 	mov	dpl,#0x10
      00025E 12 04 A6         [24] 1301 	lcall	_setFontSize
                                   1302 ;	usr/main.c:197: F = 1;
      000261 75 0D 01         [24] 1303 	mov	_F,#0x01
                                   1304 ;	usr/main.c:199: while (1)
      000264                       1305 00118$:
                                   1306 ;	usr/main.c:201: switch (led)
      000264 E5 0E            [12] 1307 	mov	a,_led
      000266 FF               [12] 1308 	mov	r7,a
      000267 24 FC            [12] 1309 	add	a,#0xff - 0x03
      000269 50 03            [24] 1310 	jnc	00150$
      00026B 02 03 47         [24] 1311 	ljmp	00113$
      00026E                       1312 00150$:
      00026E EF               [12] 1313 	mov	a,r7
      00026F 2F               [12] 1314 	add	a,r7
      000270 2F               [12] 1315 	add	a,r7
      000271 90 02 75         [24] 1316 	mov	dptr,#00151$
      000274 73               [24] 1317 	jmp	@a+dptr
      000275                       1318 00151$:
      000275 02 02 81         [24] 1319 	ljmp	00101$
      000278 02 02 B3         [24] 1320 	ljmp	00104$
      00027B 02 02 E5         [24] 1321 	ljmp	00107$
      00027E 02 03 16         [24] 1322 	ljmp	00110$
                                   1323 ;	usr/main.c:203: case 0:if(F == 1) {F=0;OLED_Clear();OLED_ShowString(0,2,"RESTING");}           //接电后初始为熄灭状态，且OLED上显示为RESTING
      000281                       1324 00101$:
      000281 74 01            [12] 1325 	mov	a,#0x01
      000283 B5 0D 18         [24] 1326 	cjne	a,_F,00103$
      000286 75 0D 00         [24] 1327 	mov	_F,#0x00
      000289 12 05 6F         [24] 1328 	lcall	_OLED_Clear
      00028C 75 28 E4         [24] 1329 	mov	_OLED_ShowString_PARM_3,#___str_0
      00028F 75 29 10         [24] 1330 	mov	(_OLED_ShowString_PARM_3 + 1),#(___str_0 >> 8)
      000292 75 2A 80         [24] 1331 	mov	(_OLED_ShowString_PARM_3 + 2),#0x80
      000295 75 27 02         [24] 1332 	mov	_OLED_ShowString_PARM_2,#0x02
      000298 75 82 00         [24] 1333 	mov	dpl,#0x00
      00029B 12 06 F2         [24] 1334 	lcall	_OLED_ShowString
      00029E                       1335 00103$:
                                   1336 ;	usr/main.c:204: Set_Light( 0X00 , 0X00 , 0X00 );mDelaymS(30);break;
      00029E 75 08 00         [24] 1337 	mov	_Set_Light_PARM_2,#0x00
      0002A1 75 09 00         [24] 1338 	mov	_Set_Light_PARM_3,#0x00
      0002A4 75 82 00         [24] 1339 	mov	dpl,#0x00
      0002A7 12 01 AB         [24] 1340 	lcall	_Set_Light
      0002AA 90 00 1E         [24] 1341 	mov	dptr,#0x001e
      0002AD 12 04 34         [24] 1342 	lcall	_mDelaymS
      0002B0 02 03 79         [24] 1343 	ljmp	00116$
                                   1344 ;	usr/main.c:205: case 1:if(F == 1) {F=0;OLED_Clear();OLED_ShowString(0,2,"BACK OFF");}          //按第一下按钮为红灯，且OLED上显示为BACK OFF
      0002B3                       1345 00104$:
      0002B3 74 01            [12] 1346 	mov	a,#0x01
      0002B5 B5 0D 18         [24] 1347 	cjne	a,_F,00106$
      0002B8 75 0D 00         [24] 1348 	mov	_F,#0x00
      0002BB 12 05 6F         [24] 1349 	lcall	_OLED_Clear
      0002BE 75 28 EC         [24] 1350 	mov	_OLED_ShowString_PARM_3,#___str_1
      0002C1 75 29 10         [24] 1351 	mov	(_OLED_ShowString_PARM_3 + 1),#(___str_1 >> 8)
      0002C4 75 2A 80         [24] 1352 	mov	(_OLED_ShowString_PARM_3 + 2),#0x80
      0002C7 75 27 02         [24] 1353 	mov	_OLED_ShowString_PARM_2,#0x02
      0002CA 75 82 00         [24] 1354 	mov	dpl,#0x00
      0002CD 12 06 F2         [24] 1355 	lcall	_OLED_ShowString
      0002D0                       1356 00106$:
                                   1357 ;	usr/main.c:206: Set_Light( 0X00 , 0Xff , 0X00 );mDelaymS(30);break;
      0002D0 75 08 FF         [24] 1358 	mov	_Set_Light_PARM_2,#0xff
      0002D3 75 09 00         [24] 1359 	mov	_Set_Light_PARM_3,#0x00
      0002D6 75 82 00         [24] 1360 	mov	dpl,#0x00
      0002D9 12 01 AB         [24] 1361 	lcall	_Set_Light
      0002DC 90 00 1E         [24] 1362 	mov	dptr,#0x001e
      0002DF 12 04 34         [24] 1363 	lcall	_mDelaymS
      0002E2 02 03 79         [24] 1364 	ljmp	00116$
                                   1365 ;	usr/main.c:207: case 2:if(F == 1) {F=0;OLED_Clear();OLED_ShowString(0,2,"KNOCK KNOCK");}       //按第二下按钮为黄灯，且OLED上显示为KNOCK KNOCK   
      0002E5                       1366 00107$:
      0002E5 74 01            [12] 1367 	mov	a,#0x01
      0002E7 B5 0D 18         [24] 1368 	cjne	a,_F,00109$
      0002EA 75 0D 00         [24] 1369 	mov	_F,#0x00
      0002ED 12 05 6F         [24] 1370 	lcall	_OLED_Clear
      0002F0 75 28 F5         [24] 1371 	mov	_OLED_ShowString_PARM_3,#___str_2
      0002F3 75 29 10         [24] 1372 	mov	(_OLED_ShowString_PARM_3 + 1),#(___str_2 >> 8)
      0002F6 75 2A 80         [24] 1373 	mov	(_OLED_ShowString_PARM_3 + 2),#0x80
      0002F9 75 27 02         [24] 1374 	mov	_OLED_ShowString_PARM_2,#0x02
      0002FC 75 82 00         [24] 1375 	mov	dpl,#0x00
      0002FF 12 06 F2         [24] 1376 	lcall	_OLED_ShowString
      000302                       1377 00109$:
                                   1378 ;	usr/main.c:208: Set_Light( 0Xff , 0Xff , 0X00 );mDelaymS(30);break;
      000302 75 08 FF         [24] 1379 	mov	_Set_Light_PARM_2,#0xff
      000305 75 09 00         [24] 1380 	mov	_Set_Light_PARM_3,#0x00
      000308 75 82 FF         [24] 1381 	mov	dpl,#0xff
      00030B 12 01 AB         [24] 1382 	lcall	_Set_Light
      00030E 90 00 1E         [24] 1383 	mov	dptr,#0x001e
      000311 12 04 34         [24] 1384 	lcall	_mDelaymS
                                   1385 ;	usr/main.c:209: case 3:if(F == 1) {F=0;OLED_Clear();OLED_ShowString(0,2,"FREEDOM!!");}         //按第三下按钮为绿灯，且OLED上显示为FREEDOM!!
      000314 80 63            [24] 1386 	sjmp	00116$
      000316                       1387 00110$:
      000316 74 01            [12] 1388 	mov	a,#0x01
      000318 B5 0D 18         [24] 1389 	cjne	a,_F,00112$
      00031B 75 0D 00         [24] 1390 	mov	_F,#0x00
      00031E 12 05 6F         [24] 1391 	lcall	_OLED_Clear
      000321 75 28 01         [24] 1392 	mov	_OLED_ShowString_PARM_3,#___str_3
      000324 75 29 11         [24] 1393 	mov	(_OLED_ShowString_PARM_3 + 1),#(___str_3 >> 8)
      000327 75 2A 80         [24] 1394 	mov	(_OLED_ShowString_PARM_3 + 2),#0x80
      00032A 75 27 02         [24] 1395 	mov	_OLED_ShowString_PARM_2,#0x02
      00032D 75 82 00         [24] 1396 	mov	dpl,#0x00
      000330 12 06 F2         [24] 1397 	lcall	_OLED_ShowString
      000333                       1398 00112$:
                                   1399 ;	usr/main.c:210: Set_Light( 0Xff , 0X00 , 0X00 );mDelaymS(30);break;
      000333 75 08 00         [24] 1400 	mov	_Set_Light_PARM_2,#0x00
      000336 75 09 00         [24] 1401 	mov	_Set_Light_PARM_3,#0x00
      000339 75 82 FF         [24] 1402 	mov	dpl,#0xff
      00033C 12 01 AB         [24] 1403 	lcall	_Set_Light
      00033F 90 00 1E         [24] 1404 	mov	dptr,#0x001e
      000342 12 04 34         [24] 1405 	lcall	_mDelaymS
                                   1406 ;	usr/main.c:211: default:led = 0;if(F == 1) {F=0;OLED_Clear();OLED_ShowString(0,2,"RESTING");} //按第四下按钮为灯熄灭，且OLED上显示为RESTING，相当于回到循环起点重新开始
      000345 80 32            [24] 1407 	sjmp	00116$
      000347                       1408 00113$:
      000347 75 0E 00         [24] 1409 	mov	_led,#0x00
      00034A 74 01            [12] 1410 	mov	a,#0x01
      00034C B5 0D 18         [24] 1411 	cjne	a,_F,00115$
      00034F 75 0D 00         [24] 1412 	mov	_F,#0x00
      000352 12 05 6F         [24] 1413 	lcall	_OLED_Clear
      000355 75 28 E4         [24] 1414 	mov	_OLED_ShowString_PARM_3,#___str_0
      000358 75 29 10         [24] 1415 	mov	(_OLED_ShowString_PARM_3 + 1),#(___str_0 >> 8)
      00035B 75 2A 80         [24] 1416 	mov	(_OLED_ShowString_PARM_3 + 2),#0x80
      00035E 75 27 02         [24] 1417 	mov	_OLED_ShowString_PARM_2,#0x02
      000361 75 82 00         [24] 1418 	mov	dpl,#0x00
      000364 12 06 F2         [24] 1419 	lcall	_OLED_ShowString
      000367                       1420 00115$:
                                   1421 ;	usr/main.c:212: Set_Light( 0X00 , 0X00 , 0X00 );mDelaymS(30);break;
      000367 75 08 00         [24] 1422 	mov	_Set_Light_PARM_2,#0x00
      00036A 75 09 00         [24] 1423 	mov	_Set_Light_PARM_3,#0x00
      00036D 75 82 00         [24] 1424 	mov	dpl,#0x00
      000370 12 01 AB         [24] 1425 	lcall	_Set_Light
      000373 90 00 1E         [24] 1426 	mov	dptr,#0x001e
      000376 12 04 34         [24] 1427 	lcall	_mDelaymS
                                   1428 ;	usr/main.c:213: } 
      000379                       1429 00116$:
                                   1430 ;	usr/main.c:214: oled_hor_scroll(PAGE0, PAGE7, FRAME_5, 0);    //OLED上的句子水平滚动
      000379 75 0A 07         [24] 1431 	mov	_oled_hor_scroll_PARM_2,#0x07
      00037C 75 0B 00         [24] 1432 	mov	_oled_hor_scroll_PARM_3,#0x00
      00037F 75 0C 00         [24] 1433 	mov	_oled_hor_scroll_PARM_4,#0x00
      000382 75 82 00         [24] 1434 	mov	dpl,#0x00
      000385 12 01 C9         [24] 1435 	lcall	_oled_hor_scroll
                                   1436 ;	usr/main.c:215: delay_ms(500);
      000388 90 01 F4         [24] 1437 	mov	dptr,#0x01f4
      00038B 12 04 AA         [24] 1438 	lcall	_delay_ms
                                   1439 ;	usr/main.c:217: }
      00038E 02 02 64         [24] 1440 	ljmp	00118$
                                   1441 ;------------------------------------------------------------
                                   1442 ;Allocation info for local variables in function 'Int0'
                                   1443 ;------------------------------------------------------------
                                   1444 ;	usr/main.c:231: void Int0()	__interrupt 0		//外部中断0的中断函数
                                   1445 ;	-----------------------------------------
                                   1446 ;	 function Int0
                                   1447 ;	-----------------------------------------
      000391                       1448 _Int0:
      000391 C0 20            [24] 1449 	push	bits
      000393 C0 E0            [24] 1450 	push	acc
      000395 C0 F0            [24] 1451 	push	b
      000397 C0 82            [24] 1452 	push	dpl
      000399 C0 83            [24] 1453 	push	dph
      00039B C0 07            [24] 1454 	push	(0+7)
      00039D C0 06            [24] 1455 	push	(0+6)
      00039F C0 05            [24] 1456 	push	(0+5)
      0003A1 C0 04            [24] 1457 	push	(0+4)
      0003A3 C0 03            [24] 1458 	push	(0+3)
      0003A5 C0 02            [24] 1459 	push	(0+2)
      0003A7 C0 01            [24] 1460 	push	(0+1)
      0003A9 C0 00            [24] 1461 	push	(0+0)
      0003AB C0 D0            [24] 1462 	push	psw
      0003AD 75 D0 00         [24] 1463 	mov	psw,#0x00
                                   1464 ;	usr/main.c:233: delay_ms(120);	 //延时消抖
      0003B0 90 00 78         [24] 1465 	mov	dptr,#0x0078
      0003B3 12 04 AA         [24] 1466 	lcall	_delay_ms
                                   1467 ;	usr/main.c:234: IE0=0;
                                   1468 ;	assignBit
      0003B6 C2 89            [12] 1469 	clr	_IE0
                                   1470 ;	usr/main.c:235: if(key1==0)
      0003B8 20 B2 08         [24] 1471 	jb	_P3_2,00103$
                                   1472 ;	usr/main.c:237: led++;    //LED计数加1
      0003BB E5 0E            [12] 1473 	mov	a,_led
      0003BD 04               [12] 1474 	inc	a
      0003BE F5 0E            [12] 1475 	mov	_led,a
                                   1476 ;	usr/main.c:238: F = 1;    //按下按钮便重置给屏幕赋值的指令
      0003C0 75 0D 01         [24] 1477 	mov	_F,#0x01
      0003C3                       1478 00103$:
                                   1479 ;	usr/main.c:240: }
      0003C3 D0 D0            [24] 1480 	pop	psw
      0003C5 D0 00            [24] 1481 	pop	(0+0)
      0003C7 D0 01            [24] 1482 	pop	(0+1)
      0003C9 D0 02            [24] 1483 	pop	(0+2)
      0003CB D0 03            [24] 1484 	pop	(0+3)
      0003CD D0 04            [24] 1485 	pop	(0+4)
      0003CF D0 05            [24] 1486 	pop	(0+5)
      0003D1 D0 06            [24] 1487 	pop	(0+6)
      0003D3 D0 07            [24] 1488 	pop	(0+7)
      0003D5 D0 83            [24] 1489 	pop	dph
      0003D7 D0 82            [24] 1490 	pop	dpl
      0003D9 D0 F0            [24] 1491 	pop	b
      0003DB D0 E0            [24] 1492 	pop	acc
      0003DD D0 20            [24] 1493 	pop	bits
      0003DF 32               [24] 1494 	reti
                                   1495 	.area CSEG    (CODE)
                                   1496 	.area CONST   (CODE)
      0008E4                       1497 _BMP1:
      0008E4 00                    1498 	.db #0x00	; 0
      0008E5 03                    1499 	.db #0x03	; 3
      0008E6 05                    1500 	.db #0x05	; 5
      0008E7 09                    1501 	.db #0x09	; 9
      0008E8 11                    1502 	.db #0x11	; 17
      0008E9 FF                    1503 	.db #0xff	; 255
      0008EA 11                    1504 	.db #0x11	; 17
      0008EB 89                    1505 	.db #0x89	; 137
      0008EC 05                    1506 	.db #0x05	; 5
      0008ED C3                    1507 	.db #0xc3	; 195
      0008EE 00                    1508 	.db #0x00	; 0
      0008EF E0                    1509 	.db #0xe0	; 224
      0008F0 00                    1510 	.db #0x00	; 0
      0008F1 F0                    1511 	.db #0xf0	; 240
      0008F2 00                    1512 	.db #0x00	; 0
      0008F3 F8                    1513 	.db #0xf8	; 248
      0008F4 00                    1514 	.db #0x00	; 0
      0008F5 00                    1515 	.db #0x00	; 0
      0008F6 00                    1516 	.db #0x00	; 0
      0008F7 00                    1517 	.db #0x00	; 0
      0008F8 00                    1518 	.db #0x00	; 0
      0008F9 00                    1519 	.db #0x00	; 0
      0008FA 00                    1520 	.db #0x00	; 0
      0008FB 44                    1521 	.db #0x44	; 68	'D'
      0008FC 28                    1522 	.db #0x28	; 40
      0008FD FF                    1523 	.db #0xff	; 255
      0008FE 11                    1524 	.db #0x11	; 17
      0008FF AA                    1525 	.db #0xaa	; 170
      000900 44                    1526 	.db #0x44	; 68	'D'
      000901 00                    1527 	.db #0x00	; 0
      000902 00                    1528 	.db #0x00	; 0
      000903 00                    1529 	.db #0x00	; 0
      000904 00                    1530 	.db #0x00	; 0
      000905 00                    1531 	.db #0x00	; 0
      000906 00                    1532 	.db #0x00	; 0
      000907 00                    1533 	.db #0x00	; 0
      000908 00                    1534 	.db #0x00	; 0
      000909 00                    1535 	.db #0x00	; 0
      00090A 00                    1536 	.db #0x00	; 0
      00090B 00                    1537 	.db #0x00	; 0
      00090C 00                    1538 	.db #0x00	; 0
      00090D 00                    1539 	.db #0x00	; 0
      00090E 00                    1540 	.db #0x00	; 0
      00090F 00                    1541 	.db #0x00	; 0
      000910 00                    1542 	.db #0x00	; 0
      000911 00                    1543 	.db #0x00	; 0
      000912 00                    1544 	.db #0x00	; 0
      000913 00                    1545 	.db #0x00	; 0
      000914 00                    1546 	.db #0x00	; 0
      000915 00                    1547 	.db #0x00	; 0
      000916 00                    1548 	.db #0x00	; 0
      000917 00                    1549 	.db #0x00	; 0
      000918 00                    1550 	.db #0x00	; 0
      000919 00                    1551 	.db #0x00	; 0
      00091A 00                    1552 	.db #0x00	; 0
      00091B 00                    1553 	.db #0x00	; 0
      00091C 00                    1554 	.db #0x00	; 0
      00091D 00                    1555 	.db #0x00	; 0
      00091E 00                    1556 	.db #0x00	; 0
      00091F 00                    1557 	.db #0x00	; 0
      000920 00                    1558 	.db #0x00	; 0
      000921 00                    1559 	.db #0x00	; 0
      000922 00                    1560 	.db #0x00	; 0
      000923 00                    1561 	.db #0x00	; 0
      000924 00                    1562 	.db #0x00	; 0
      000925 00                    1563 	.db #0x00	; 0
      000926 00                    1564 	.db #0x00	; 0
      000927 00                    1565 	.db #0x00	; 0
      000928 00                    1566 	.db #0x00	; 0
      000929 00                    1567 	.db #0x00	; 0
      00092A 00                    1568 	.db #0x00	; 0
      00092B 00                    1569 	.db #0x00	; 0
      00092C 00                    1570 	.db #0x00	; 0
      00092D 00                    1571 	.db #0x00	; 0
      00092E 00                    1572 	.db #0x00	; 0
      00092F 00                    1573 	.db #0x00	; 0
      000930 00                    1574 	.db #0x00	; 0
      000931 00                    1575 	.db #0x00	; 0
      000932 00                    1576 	.db #0x00	; 0
      000933 00                    1577 	.db #0x00	; 0
      000934 00                    1578 	.db #0x00	; 0
      000935 00                    1579 	.db #0x00	; 0
      000936 00                    1580 	.db #0x00	; 0
      000937 00                    1581 	.db #0x00	; 0
      000938 00                    1582 	.db #0x00	; 0
      000939 00                    1583 	.db #0x00	; 0
      00093A 00                    1584 	.db #0x00	; 0
      00093B 00                    1585 	.db #0x00	; 0
      00093C 00                    1586 	.db #0x00	; 0
      00093D 00                    1587 	.db #0x00	; 0
      00093E 83                    1588 	.db #0x83	; 131
      00093F 01                    1589 	.db #0x01	; 1
      000940 38                    1590 	.db #0x38	; 56	'8'
      000941 44                    1591 	.db #0x44	; 68	'D'
      000942 82                    1592 	.db #0x82	; 130
      000943 92                    1593 	.db #0x92	; 146
      000944 92                    1594 	.db #0x92	; 146
      000945 74                    1595 	.db #0x74	; 116	't'
      000946 01                    1596 	.db #0x01	; 1
      000947 83                    1597 	.db #0x83	; 131
      000948 00                    1598 	.db #0x00	; 0
      000949 00                    1599 	.db #0x00	; 0
      00094A 00                    1600 	.db #0x00	; 0
      00094B 00                    1601 	.db #0x00	; 0
      00094C 00                    1602 	.db #0x00	; 0
      00094D 00                    1603 	.db #0x00	; 0
      00094E 00                    1604 	.db #0x00	; 0
      00094F 7C                    1605 	.db #0x7c	; 124
      000950 44                    1606 	.db #0x44	; 68	'D'
      000951 FF                    1607 	.db #0xff	; 255
      000952 01                    1608 	.db #0x01	; 1
      000953 7D                    1609 	.db #0x7d	; 125
      000954 7D                    1610 	.db #0x7d	; 125
      000955 7D                    1611 	.db #0x7d	; 125
      000956 01                    1612 	.db #0x01	; 1
      000957 7D                    1613 	.db #0x7d	; 125
      000958 7D                    1614 	.db #0x7d	; 125
      000959 7D                    1615 	.db #0x7d	; 125
      00095A 7D                    1616 	.db #0x7d	; 125
      00095B 01                    1617 	.db #0x01	; 1
      00095C 7D                    1618 	.db #0x7d	; 125
      00095D 7D                    1619 	.db #0x7d	; 125
      00095E 7D                    1620 	.db #0x7d	; 125
      00095F 7D                    1621 	.db #0x7d	; 125
      000960 7D                    1622 	.db #0x7d	; 125
      000961 01                    1623 	.db #0x01	; 1
      000962 FF                    1624 	.db #0xff	; 255
      000963 00                    1625 	.db #0x00	; 0
      000964 00                    1626 	.db #0x00	; 0
      000965 00                    1627 	.db #0x00	; 0
      000966 00                    1628 	.db #0x00	; 0
      000967 00                    1629 	.db #0x00	; 0
      000968 00                    1630 	.db #0x00	; 0
      000969 01                    1631 	.db #0x01	; 1
      00096A 00                    1632 	.db #0x00	; 0
      00096B 01                    1633 	.db #0x01	; 1
      00096C 00                    1634 	.db #0x00	; 0
      00096D 01                    1635 	.db #0x01	; 1
      00096E 00                    1636 	.db #0x00	; 0
      00096F 01                    1637 	.db #0x01	; 1
      000970 00                    1638 	.db #0x00	; 0
      000971 01                    1639 	.db #0x01	; 1
      000972 00                    1640 	.db #0x00	; 0
      000973 01                    1641 	.db #0x01	; 1
      000974 00                    1642 	.db #0x00	; 0
      000975 00                    1643 	.db #0x00	; 0
      000976 00                    1644 	.db #0x00	; 0
      000977 00                    1645 	.db #0x00	; 0
      000978 00                    1646 	.db #0x00	; 0
      000979 00                    1647 	.db #0x00	; 0
      00097A 00                    1648 	.db #0x00	; 0
      00097B 00                    1649 	.db #0x00	; 0
      00097C 00                    1650 	.db #0x00	; 0
      00097D 01                    1651 	.db #0x01	; 1
      00097E 01                    1652 	.db #0x01	; 1
      00097F 00                    1653 	.db #0x00	; 0
      000980 00                    1654 	.db #0x00	; 0
      000981 00                    1655 	.db #0x00	; 0
      000982 00                    1656 	.db #0x00	; 0
      000983 00                    1657 	.db #0x00	; 0
      000984 00                    1658 	.db #0x00	; 0
      000985 00                    1659 	.db #0x00	; 0
      000986 00                    1660 	.db #0x00	; 0
      000987 00                    1661 	.db #0x00	; 0
      000988 00                    1662 	.db #0x00	; 0
      000989 00                    1663 	.db #0x00	; 0
      00098A 00                    1664 	.db #0x00	; 0
      00098B 00                    1665 	.db #0x00	; 0
      00098C 00                    1666 	.db #0x00	; 0
      00098D 00                    1667 	.db #0x00	; 0
      00098E 00                    1668 	.db #0x00	; 0
      00098F 00                    1669 	.db #0x00	; 0
      000990 00                    1670 	.db #0x00	; 0
      000991 00                    1671 	.db #0x00	; 0
      000992 00                    1672 	.db #0x00	; 0
      000993 00                    1673 	.db #0x00	; 0
      000994 00                    1674 	.db #0x00	; 0
      000995 00                    1675 	.db #0x00	; 0
      000996 00                    1676 	.db #0x00	; 0
      000997 00                    1677 	.db #0x00	; 0
      000998 00                    1678 	.db #0x00	; 0
      000999 00                    1679 	.db #0x00	; 0
      00099A 00                    1680 	.db #0x00	; 0
      00099B 00                    1681 	.db #0x00	; 0
      00099C 00                    1682 	.db #0x00	; 0
      00099D 00                    1683 	.db #0x00	; 0
      00099E 00                    1684 	.db #0x00	; 0
      00099F 00                    1685 	.db #0x00	; 0
      0009A0 00                    1686 	.db #0x00	; 0
      0009A1 00                    1687 	.db #0x00	; 0
      0009A2 00                    1688 	.db #0x00	; 0
      0009A3 00                    1689 	.db #0x00	; 0
      0009A4 00                    1690 	.db #0x00	; 0
      0009A5 00                    1691 	.db #0x00	; 0
      0009A6 00                    1692 	.db #0x00	; 0
      0009A7 00                    1693 	.db #0x00	; 0
      0009A8 00                    1694 	.db #0x00	; 0
      0009A9 00                    1695 	.db #0x00	; 0
      0009AA 00                    1696 	.db #0x00	; 0
      0009AB 00                    1697 	.db #0x00	; 0
      0009AC 00                    1698 	.db #0x00	; 0
      0009AD 00                    1699 	.db #0x00	; 0
      0009AE 00                    1700 	.db #0x00	; 0
      0009AF 00                    1701 	.db #0x00	; 0
      0009B0 00                    1702 	.db #0x00	; 0
      0009B1 00                    1703 	.db #0x00	; 0
      0009B2 00                    1704 	.db #0x00	; 0
      0009B3 00                    1705 	.db #0x00	; 0
      0009B4 00                    1706 	.db #0x00	; 0
      0009B5 00                    1707 	.db #0x00	; 0
      0009B6 00                    1708 	.db #0x00	; 0
      0009B7 00                    1709 	.db #0x00	; 0
      0009B8 00                    1710 	.db #0x00	; 0
      0009B9 00                    1711 	.db #0x00	; 0
      0009BA 00                    1712 	.db #0x00	; 0
      0009BB 00                    1713 	.db #0x00	; 0
      0009BC 00                    1714 	.db #0x00	; 0
      0009BD 00                    1715 	.db #0x00	; 0
      0009BE 01                    1716 	.db #0x01	; 1
      0009BF 01                    1717 	.db #0x01	; 1
      0009C0 00                    1718 	.db #0x00	; 0
      0009C1 00                    1719 	.db #0x00	; 0
      0009C2 00                    1720 	.db #0x00	; 0
      0009C3 00                    1721 	.db #0x00	; 0
      0009C4 00                    1722 	.db #0x00	; 0
      0009C5 00                    1723 	.db #0x00	; 0
      0009C6 01                    1724 	.db #0x01	; 1
      0009C7 01                    1725 	.db #0x01	; 1
      0009C8 00                    1726 	.db #0x00	; 0
      0009C9 00                    1727 	.db #0x00	; 0
      0009CA 00                    1728 	.db #0x00	; 0
      0009CB 00                    1729 	.db #0x00	; 0
      0009CC 00                    1730 	.db #0x00	; 0
      0009CD 00                    1731 	.db #0x00	; 0
      0009CE 00                    1732 	.db #0x00	; 0
      0009CF 00                    1733 	.db #0x00	; 0
      0009D0 00                    1734 	.db #0x00	; 0
      0009D1 01                    1735 	.db #0x01	; 1
      0009D2 01                    1736 	.db #0x01	; 1
      0009D3 01                    1737 	.db #0x01	; 1
      0009D4 01                    1738 	.db #0x01	; 1
      0009D5 01                    1739 	.db #0x01	; 1
      0009D6 01                    1740 	.db #0x01	; 1
      0009D7 01                    1741 	.db #0x01	; 1
      0009D8 01                    1742 	.db #0x01	; 1
      0009D9 01                    1743 	.db #0x01	; 1
      0009DA 01                    1744 	.db #0x01	; 1
      0009DB 01                    1745 	.db #0x01	; 1
      0009DC 01                    1746 	.db #0x01	; 1
      0009DD 01                    1747 	.db #0x01	; 1
      0009DE 01                    1748 	.db #0x01	; 1
      0009DF 01                    1749 	.db #0x01	; 1
      0009E0 01                    1750 	.db #0x01	; 1
      0009E1 01                    1751 	.db #0x01	; 1
      0009E2 01                    1752 	.db #0x01	; 1
      0009E3 00                    1753 	.db #0x00	; 0
      0009E4 00                    1754 	.db #0x00	; 0
      0009E5 00                    1755 	.db #0x00	; 0
      0009E6 00                    1756 	.db #0x00	; 0
      0009E7 00                    1757 	.db #0x00	; 0
      0009E8 00                    1758 	.db #0x00	; 0
      0009E9 00                    1759 	.db #0x00	; 0
      0009EA 00                    1760 	.db #0x00	; 0
      0009EB 00                    1761 	.db #0x00	; 0
      0009EC 00                    1762 	.db #0x00	; 0
      0009ED 00                    1763 	.db #0x00	; 0
      0009EE 00                    1764 	.db #0x00	; 0
      0009EF 00                    1765 	.db #0x00	; 0
      0009F0 00                    1766 	.db #0x00	; 0
      0009F1 00                    1767 	.db #0x00	; 0
      0009F2 00                    1768 	.db #0x00	; 0
      0009F3 00                    1769 	.db #0x00	; 0
      0009F4 00                    1770 	.db #0x00	; 0
      0009F5 00                    1771 	.db #0x00	; 0
      0009F6 00                    1772 	.db #0x00	; 0
      0009F7 00                    1773 	.db #0x00	; 0
      0009F8 00                    1774 	.db #0x00	; 0
      0009F9 00                    1775 	.db #0x00	; 0
      0009FA 00                    1776 	.db #0x00	; 0
      0009FB 00                    1777 	.db #0x00	; 0
      0009FC 00                    1778 	.db #0x00	; 0
      0009FD 00                    1779 	.db #0x00	; 0
      0009FE 00                    1780 	.db #0x00	; 0
      0009FF 00                    1781 	.db #0x00	; 0
      000A00 3F                    1782 	.db #0x3f	; 63
      000A01 3F                    1783 	.db #0x3f	; 63
      000A02 03                    1784 	.db #0x03	; 3
      000A03 03                    1785 	.db #0x03	; 3
      000A04 F3                    1786 	.db #0xf3	; 243
      000A05 13                    1787 	.db #0x13	; 19
      000A06 11                    1788 	.db #0x11	; 17
      000A07 11                    1789 	.db #0x11	; 17
      000A08 11                    1790 	.db #0x11	; 17
      000A09 11                    1791 	.db #0x11	; 17
      000A0A 11                    1792 	.db #0x11	; 17
      000A0B 11                    1793 	.db #0x11	; 17
      000A0C 01                    1794 	.db #0x01	; 1
      000A0D F1                    1795 	.db #0xf1	; 241
      000A0E 11                    1796 	.db #0x11	; 17
      000A0F 61                    1797 	.db #0x61	; 97	'a'
      000A10 81                    1798 	.db #0x81	; 129
      000A11 01                    1799 	.db #0x01	; 1
      000A12 01                    1800 	.db #0x01	; 1
      000A13 01                    1801 	.db #0x01	; 1
      000A14 81                    1802 	.db #0x81	; 129
      000A15 61                    1803 	.db #0x61	; 97	'a'
      000A16 11                    1804 	.db #0x11	; 17
      000A17 F1                    1805 	.db #0xf1	; 241
      000A18 01                    1806 	.db #0x01	; 1
      000A19 01                    1807 	.db #0x01	; 1
      000A1A 01                    1808 	.db #0x01	; 1
      000A1B 01                    1809 	.db #0x01	; 1
      000A1C 41                    1810 	.db #0x41	; 65	'A'
      000A1D 41                    1811 	.db #0x41	; 65	'A'
      000A1E F1                    1812 	.db #0xf1	; 241
      000A1F 01                    1813 	.db #0x01	; 1
      000A20 01                    1814 	.db #0x01	; 1
      000A21 01                    1815 	.db #0x01	; 1
      000A22 01                    1816 	.db #0x01	; 1
      000A23 01                    1817 	.db #0x01	; 1
      000A24 C1                    1818 	.db #0xc1	; 193
      000A25 21                    1819 	.db #0x21	; 33
      000A26 11                    1820 	.db #0x11	; 17
      000A27 11                    1821 	.db #0x11	; 17
      000A28 11                    1822 	.db #0x11	; 17
      000A29 11                    1823 	.db #0x11	; 17
      000A2A 21                    1824 	.db #0x21	; 33
      000A2B C1                    1825 	.db #0xc1	; 193
      000A2C 01                    1826 	.db #0x01	; 1
      000A2D 01                    1827 	.db #0x01	; 1
      000A2E 01                    1828 	.db #0x01	; 1
      000A2F 01                    1829 	.db #0x01	; 1
      000A30 41                    1830 	.db #0x41	; 65	'A'
      000A31 41                    1831 	.db #0x41	; 65	'A'
      000A32 F1                    1832 	.db #0xf1	; 241
      000A33 01                    1833 	.db #0x01	; 1
      000A34 01                    1834 	.db #0x01	; 1
      000A35 01                    1835 	.db #0x01	; 1
      000A36 01                    1836 	.db #0x01	; 1
      000A37 01                    1837 	.db #0x01	; 1
      000A38 01                    1838 	.db #0x01	; 1
      000A39 01                    1839 	.db #0x01	; 1
      000A3A 01                    1840 	.db #0x01	; 1
      000A3B 01                    1841 	.db #0x01	; 1
      000A3C 01                    1842 	.db #0x01	; 1
      000A3D 11                    1843 	.db #0x11	; 17
      000A3E 11                    1844 	.db #0x11	; 17
      000A3F 11                    1845 	.db #0x11	; 17
      000A40 11                    1846 	.db #0x11	; 17
      000A41 11                    1847 	.db #0x11	; 17
      000A42 D3                    1848 	.db #0xd3	; 211
      000A43 33                    1849 	.db #0x33	; 51	'3'
      000A44 03                    1850 	.db #0x03	; 3
      000A45 03                    1851 	.db #0x03	; 3
      000A46 3F                    1852 	.db #0x3f	; 63
      000A47 3F                    1853 	.db #0x3f	; 63
      000A48 00                    1854 	.db #0x00	; 0
      000A49 00                    1855 	.db #0x00	; 0
      000A4A 00                    1856 	.db #0x00	; 0
      000A4B 00                    1857 	.db #0x00	; 0
      000A4C 00                    1858 	.db #0x00	; 0
      000A4D 00                    1859 	.db #0x00	; 0
      000A4E 00                    1860 	.db #0x00	; 0
      000A4F 00                    1861 	.db #0x00	; 0
      000A50 00                    1862 	.db #0x00	; 0
      000A51 00                    1863 	.db #0x00	; 0
      000A52 00                    1864 	.db #0x00	; 0
      000A53 00                    1865 	.db #0x00	; 0
      000A54 00                    1866 	.db #0x00	; 0
      000A55 00                    1867 	.db #0x00	; 0
      000A56 00                    1868 	.db #0x00	; 0
      000A57 00                    1869 	.db #0x00	; 0
      000A58 00                    1870 	.db #0x00	; 0
      000A59 00                    1871 	.db #0x00	; 0
      000A5A 00                    1872 	.db #0x00	; 0
      000A5B 00                    1873 	.db #0x00	; 0
      000A5C 00                    1874 	.db #0x00	; 0
      000A5D 00                    1875 	.db #0x00	; 0
      000A5E 00                    1876 	.db #0x00	; 0
      000A5F 00                    1877 	.db #0x00	; 0
      000A60 00                    1878 	.db #0x00	; 0
      000A61 00                    1879 	.db #0x00	; 0
      000A62 00                    1880 	.db #0x00	; 0
      000A63 00                    1881 	.db #0x00	; 0
      000A64 00                    1882 	.db #0x00	; 0
      000A65 00                    1883 	.db #0x00	; 0
      000A66 00                    1884 	.db #0x00	; 0
      000A67 00                    1885 	.db #0x00	; 0
      000A68 00                    1886 	.db #0x00	; 0
      000A69 00                    1887 	.db #0x00	; 0
      000A6A 00                    1888 	.db #0x00	; 0
      000A6B 00                    1889 	.db #0x00	; 0
      000A6C 00                    1890 	.db #0x00	; 0
      000A6D 00                    1891 	.db #0x00	; 0
      000A6E 00                    1892 	.db #0x00	; 0
      000A6F 00                    1893 	.db #0x00	; 0
      000A70 00                    1894 	.db #0x00	; 0
      000A71 00                    1895 	.db #0x00	; 0
      000A72 00                    1896 	.db #0x00	; 0
      000A73 00                    1897 	.db #0x00	; 0
      000A74 00                    1898 	.db #0x00	; 0
      000A75 00                    1899 	.db #0x00	; 0
      000A76 00                    1900 	.db #0x00	; 0
      000A77 00                    1901 	.db #0x00	; 0
      000A78 00                    1902 	.db #0x00	; 0
      000A79 00                    1903 	.db #0x00	; 0
      000A7A 00                    1904 	.db #0x00	; 0
      000A7B 00                    1905 	.db #0x00	; 0
      000A7C 00                    1906 	.db #0x00	; 0
      000A7D 00                    1907 	.db #0x00	; 0
      000A7E 00                    1908 	.db #0x00	; 0
      000A7F 00                    1909 	.db #0x00	; 0
      000A80 E0                    1910 	.db #0xe0	; 224
      000A81 E0                    1911 	.db #0xe0	; 224
      000A82 00                    1912 	.db #0x00	; 0
      000A83 00                    1913 	.db #0x00	; 0
      000A84 7F                    1914 	.db #0x7f	; 127
      000A85 01                    1915 	.db #0x01	; 1
      000A86 01                    1916 	.db #0x01	; 1
      000A87 01                    1917 	.db #0x01	; 1
      000A88 01                    1918 	.db #0x01	; 1
      000A89 01                    1919 	.db #0x01	; 1
      000A8A 01                    1920 	.db #0x01	; 1
      000A8B 00                    1921 	.db #0x00	; 0
      000A8C 00                    1922 	.db #0x00	; 0
      000A8D 7F                    1923 	.db #0x7f	; 127
      000A8E 00                    1924 	.db #0x00	; 0
      000A8F 00                    1925 	.db #0x00	; 0
      000A90 01                    1926 	.db #0x01	; 1
      000A91 06                    1927 	.db #0x06	; 6
      000A92 18                    1928 	.db #0x18	; 24
      000A93 06                    1929 	.db #0x06	; 6
      000A94 01                    1930 	.db #0x01	; 1
      000A95 00                    1931 	.db #0x00	; 0
      000A96 00                    1932 	.db #0x00	; 0
      000A97 7F                    1933 	.db #0x7f	; 127
      000A98 00                    1934 	.db #0x00	; 0
      000A99 00                    1935 	.db #0x00	; 0
      000A9A 00                    1936 	.db #0x00	; 0
      000A9B 00                    1937 	.db #0x00	; 0
      000A9C 40                    1938 	.db #0x40	; 64
      000A9D 40                    1939 	.db #0x40	; 64
      000A9E 7F                    1940 	.db #0x7f	; 127
      000A9F 40                    1941 	.db #0x40	; 64
      000AA0 40                    1942 	.db #0x40	; 64
      000AA1 00                    1943 	.db #0x00	; 0
      000AA2 00                    1944 	.db #0x00	; 0
      000AA3 00                    1945 	.db #0x00	; 0
      000AA4 1F                    1946 	.db #0x1f	; 31
      000AA5 20                    1947 	.db #0x20	; 32
      000AA6 40                    1948 	.db #0x40	; 64
      000AA7 40                    1949 	.db #0x40	; 64
      000AA8 40                    1950 	.db #0x40	; 64
      000AA9 40                    1951 	.db #0x40	; 64
      000AAA 20                    1952 	.db #0x20	; 32
      000AAB 1F                    1953 	.db #0x1f	; 31
      000AAC 00                    1954 	.db #0x00	; 0
      000AAD 00                    1955 	.db #0x00	; 0
      000AAE 00                    1956 	.db #0x00	; 0
      000AAF 00                    1957 	.db #0x00	; 0
      000AB0 40                    1958 	.db #0x40	; 64
      000AB1 40                    1959 	.db #0x40	; 64
      000AB2 7F                    1960 	.db #0x7f	; 127
      000AB3 40                    1961 	.db #0x40	; 64
      000AB4 40                    1962 	.db #0x40	; 64
      000AB5 00                    1963 	.db #0x00	; 0
      000AB6 00                    1964 	.db #0x00	; 0
      000AB7 00                    1965 	.db #0x00	; 0
      000AB8 00                    1966 	.db #0x00	; 0
      000AB9 60                    1967 	.db #0x60	; 96
      000ABA 00                    1968 	.db #0x00	; 0
      000ABB 00                    1969 	.db #0x00	; 0
      000ABC 00                    1970 	.db #0x00	; 0
      000ABD 00                    1971 	.db #0x00	; 0
      000ABE 40                    1972 	.db #0x40	; 64
      000ABF 30                    1973 	.db #0x30	; 48	'0'
      000AC0 0C                    1974 	.db #0x0c	; 12
      000AC1 03                    1975 	.db #0x03	; 3
      000AC2 00                    1976 	.db #0x00	; 0
      000AC3 00                    1977 	.db #0x00	; 0
      000AC4 00                    1978 	.db #0x00	; 0
      000AC5 00                    1979 	.db #0x00	; 0
      000AC6 E0                    1980 	.db #0xe0	; 224
      000AC7 E0                    1981 	.db #0xe0	; 224
      000AC8 00                    1982 	.db #0x00	; 0
      000AC9 00                    1983 	.db #0x00	; 0
      000ACA 00                    1984 	.db #0x00	; 0
      000ACB 00                    1985 	.db #0x00	; 0
      000ACC 00                    1986 	.db #0x00	; 0
      000ACD 00                    1987 	.db #0x00	; 0
      000ACE 00                    1988 	.db #0x00	; 0
      000ACF 00                    1989 	.db #0x00	; 0
      000AD0 00                    1990 	.db #0x00	; 0
      000AD1 00                    1991 	.db #0x00	; 0
      000AD2 00                    1992 	.db #0x00	; 0
      000AD3 00                    1993 	.db #0x00	; 0
      000AD4 00                    1994 	.db #0x00	; 0
      000AD5 00                    1995 	.db #0x00	; 0
      000AD6 00                    1996 	.db #0x00	; 0
      000AD7 00                    1997 	.db #0x00	; 0
      000AD8 00                    1998 	.db #0x00	; 0
      000AD9 00                    1999 	.db #0x00	; 0
      000ADA 00                    2000 	.db #0x00	; 0
      000ADB 00                    2001 	.db #0x00	; 0
      000ADC 00                    2002 	.db #0x00	; 0
      000ADD 00                    2003 	.db #0x00	; 0
      000ADE 00                    2004 	.db #0x00	; 0
      000ADF 00                    2005 	.db #0x00	; 0
      000AE0 00                    2006 	.db #0x00	; 0
      000AE1 00                    2007 	.db #0x00	; 0
      000AE2 00                    2008 	.db #0x00	; 0
      000AE3 00                    2009 	.db #0x00	; 0
      000AE4 00                    2010 	.db #0x00	; 0
      000AE5 00                    2011 	.db #0x00	; 0
      000AE6 00                    2012 	.db #0x00	; 0
      000AE7 00                    2013 	.db #0x00	; 0
      000AE8 00                    2014 	.db #0x00	; 0
      000AE9 00                    2015 	.db #0x00	; 0
      000AEA 00                    2016 	.db #0x00	; 0
      000AEB 00                    2017 	.db #0x00	; 0
      000AEC 00                    2018 	.db #0x00	; 0
      000AED 00                    2019 	.db #0x00	; 0
      000AEE 00                    2020 	.db #0x00	; 0
      000AEF 00                    2021 	.db #0x00	; 0
      000AF0 00                    2022 	.db #0x00	; 0
      000AF1 00                    2023 	.db #0x00	; 0
      000AF2 00                    2024 	.db #0x00	; 0
      000AF3 00                    2025 	.db #0x00	; 0
      000AF4 00                    2026 	.db #0x00	; 0
      000AF5 00                    2027 	.db #0x00	; 0
      000AF6 00                    2028 	.db #0x00	; 0
      000AF7 00                    2029 	.db #0x00	; 0
      000AF8 00                    2030 	.db #0x00	; 0
      000AF9 00                    2031 	.db #0x00	; 0
      000AFA 00                    2032 	.db #0x00	; 0
      000AFB 00                    2033 	.db #0x00	; 0
      000AFC 00                    2034 	.db #0x00	; 0
      000AFD 00                    2035 	.db #0x00	; 0
      000AFE 00                    2036 	.db #0x00	; 0
      000AFF 00                    2037 	.db #0x00	; 0
      000B00 07                    2038 	.db #0x07	; 7
      000B01 07                    2039 	.db #0x07	; 7
      000B02 06                    2040 	.db #0x06	; 6
      000B03 06                    2041 	.db #0x06	; 6
      000B04 06                    2042 	.db #0x06	; 6
      000B05 06                    2043 	.db #0x06	; 6
      000B06 04                    2044 	.db #0x04	; 4
      000B07 04                    2045 	.db #0x04	; 4
      000B08 04                    2046 	.db #0x04	; 4
      000B09 84                    2047 	.db #0x84	; 132
      000B0A 44                    2048 	.db #0x44	; 68	'D'
      000B0B 44                    2049 	.db #0x44	; 68	'D'
      000B0C 44                    2050 	.db #0x44	; 68	'D'
      000B0D 84                    2051 	.db #0x84	; 132
      000B0E 04                    2052 	.db #0x04	; 4
      000B0F 04                    2053 	.db #0x04	; 4
      000B10 84                    2054 	.db #0x84	; 132
      000B11 44                    2055 	.db #0x44	; 68	'D'
      000B12 44                    2056 	.db #0x44	; 68	'D'
      000B13 44                    2057 	.db #0x44	; 68	'D'
      000B14 84                    2058 	.db #0x84	; 132
      000B15 04                    2059 	.db #0x04	; 4
      000B16 04                    2060 	.db #0x04	; 4
      000B17 04                    2061 	.db #0x04	; 4
      000B18 84                    2062 	.db #0x84	; 132
      000B19 C4                    2063 	.db #0xc4	; 196
      000B1A 04                    2064 	.db #0x04	; 4
      000B1B 04                    2065 	.db #0x04	; 4
      000B1C 04                    2066 	.db #0x04	; 4
      000B1D 04                    2067 	.db #0x04	; 4
      000B1E 84                    2068 	.db #0x84	; 132
      000B1F 44                    2069 	.db #0x44	; 68	'D'
      000B20 44                    2070 	.db #0x44	; 68	'D'
      000B21 44                    2071 	.db #0x44	; 68	'D'
      000B22 84                    2072 	.db #0x84	; 132
      000B23 04                    2073 	.db #0x04	; 4
      000B24 04                    2074 	.db #0x04	; 4
      000B25 04                    2075 	.db #0x04	; 4
      000B26 04                    2076 	.db #0x04	; 4
      000B27 04                    2077 	.db #0x04	; 4
      000B28 84                    2078 	.db #0x84	; 132
      000B29 44                    2079 	.db #0x44	; 68	'D'
      000B2A 44                    2080 	.db #0x44	; 68	'D'
      000B2B 44                    2081 	.db #0x44	; 68	'D'
      000B2C 84                    2082 	.db #0x84	; 132
      000B2D 04                    2083 	.db #0x04	; 4
      000B2E 04                    2084 	.db #0x04	; 4
      000B2F 04                    2085 	.db #0x04	; 4
      000B30 04                    2086 	.db #0x04	; 4
      000B31 04                    2087 	.db #0x04	; 4
      000B32 84                    2088 	.db #0x84	; 132
      000B33 44                    2089 	.db #0x44	; 68	'D'
      000B34 44                    2090 	.db #0x44	; 68	'D'
      000B35 44                    2091 	.db #0x44	; 68	'D'
      000B36 84                    2092 	.db #0x84	; 132
      000B37 04                    2093 	.db #0x04	; 4
      000B38 04                    2094 	.db #0x04	; 4
      000B39 84                    2095 	.db #0x84	; 132
      000B3A 44                    2096 	.db #0x44	; 68	'D'
      000B3B 44                    2097 	.db #0x44	; 68	'D'
      000B3C 44                    2098 	.db #0x44	; 68	'D'
      000B3D 84                    2099 	.db #0x84	; 132
      000B3E 04                    2100 	.db #0x04	; 4
      000B3F 04                    2101 	.db #0x04	; 4
      000B40 04                    2102 	.db #0x04	; 4
      000B41 04                    2103 	.db #0x04	; 4
      000B42 06                    2104 	.db #0x06	; 6
      000B43 06                    2105 	.db #0x06	; 6
      000B44 06                    2106 	.db #0x06	; 6
      000B45 06                    2107 	.db #0x06	; 6
      000B46 07                    2108 	.db #0x07	; 7
      000B47 07                    2109 	.db #0x07	; 7
      000B48 00                    2110 	.db #0x00	; 0
      000B49 00                    2111 	.db #0x00	; 0
      000B4A 00                    2112 	.db #0x00	; 0
      000B4B 00                    2113 	.db #0x00	; 0
      000B4C 00                    2114 	.db #0x00	; 0
      000B4D 00                    2115 	.db #0x00	; 0
      000B4E 00                    2116 	.db #0x00	; 0
      000B4F 00                    2117 	.db #0x00	; 0
      000B50 00                    2118 	.db #0x00	; 0
      000B51 00                    2119 	.db #0x00	; 0
      000B52 00                    2120 	.db #0x00	; 0
      000B53 00                    2121 	.db #0x00	; 0
      000B54 00                    2122 	.db #0x00	; 0
      000B55 00                    2123 	.db #0x00	; 0
      000B56 00                    2124 	.db #0x00	; 0
      000B57 00                    2125 	.db #0x00	; 0
      000B58 00                    2126 	.db #0x00	; 0
      000B59 00                    2127 	.db #0x00	; 0
      000B5A 00                    2128 	.db #0x00	; 0
      000B5B 00                    2129 	.db #0x00	; 0
      000B5C 00                    2130 	.db #0x00	; 0
      000B5D 00                    2131 	.db #0x00	; 0
      000B5E 00                    2132 	.db #0x00	; 0
      000B5F 00                    2133 	.db #0x00	; 0
      000B60 00                    2134 	.db #0x00	; 0
      000B61 00                    2135 	.db #0x00	; 0
      000B62 00                    2136 	.db #0x00	; 0
      000B63 00                    2137 	.db #0x00	; 0
      000B64 00                    2138 	.db #0x00	; 0
      000B65 00                    2139 	.db #0x00	; 0
      000B66 00                    2140 	.db #0x00	; 0
      000B67 00                    2141 	.db #0x00	; 0
      000B68 00                    2142 	.db #0x00	; 0
      000B69 00                    2143 	.db #0x00	; 0
      000B6A 00                    2144 	.db #0x00	; 0
      000B6B 00                    2145 	.db #0x00	; 0
      000B6C 00                    2146 	.db #0x00	; 0
      000B6D 00                    2147 	.db #0x00	; 0
      000B6E 00                    2148 	.db #0x00	; 0
      000B6F 00                    2149 	.db #0x00	; 0
      000B70 00                    2150 	.db #0x00	; 0
      000B71 00                    2151 	.db #0x00	; 0
      000B72 00                    2152 	.db #0x00	; 0
      000B73 00                    2153 	.db #0x00	; 0
      000B74 00                    2154 	.db #0x00	; 0
      000B75 00                    2155 	.db #0x00	; 0
      000B76 00                    2156 	.db #0x00	; 0
      000B77 00                    2157 	.db #0x00	; 0
      000B78 00                    2158 	.db #0x00	; 0
      000B79 00                    2159 	.db #0x00	; 0
      000B7A 00                    2160 	.db #0x00	; 0
      000B7B 00                    2161 	.db #0x00	; 0
      000B7C 00                    2162 	.db #0x00	; 0
      000B7D 00                    2163 	.db #0x00	; 0
      000B7E 00                    2164 	.db #0x00	; 0
      000B7F 00                    2165 	.db #0x00	; 0
      000B80 00                    2166 	.db #0x00	; 0
      000B81 00                    2167 	.db #0x00	; 0
      000B82 00                    2168 	.db #0x00	; 0
      000B83 00                    2169 	.db #0x00	; 0
      000B84 00                    2170 	.db #0x00	; 0
      000B85 00                    2171 	.db #0x00	; 0
      000B86 00                    2172 	.db #0x00	; 0
      000B87 00                    2173 	.db #0x00	; 0
      000B88 00                    2174 	.db #0x00	; 0
      000B89 10                    2175 	.db #0x10	; 16
      000B8A 18                    2176 	.db #0x18	; 24
      000B8B 14                    2177 	.db #0x14	; 20
      000B8C 12                    2178 	.db #0x12	; 18
      000B8D 11                    2179 	.db #0x11	; 17
      000B8E 00                    2180 	.db #0x00	; 0
      000B8F 00                    2181 	.db #0x00	; 0
      000B90 0F                    2182 	.db #0x0f	; 15
      000B91 10                    2183 	.db #0x10	; 16
      000B92 10                    2184 	.db #0x10	; 16
      000B93 10                    2185 	.db #0x10	; 16
      000B94 0F                    2186 	.db #0x0f	; 15
      000B95 00                    2187 	.db #0x00	; 0
      000B96 00                    2188 	.db #0x00	; 0
      000B97 00                    2189 	.db #0x00	; 0
      000B98 10                    2190 	.db #0x10	; 16
      000B99 1F                    2191 	.db #0x1f	; 31
      000B9A 10                    2192 	.db #0x10	; 16
      000B9B 00                    2193 	.db #0x00	; 0
      000B9C 00                    2194 	.db #0x00	; 0
      000B9D 00                    2195 	.db #0x00	; 0
      000B9E 08                    2196 	.db #0x08	; 8
      000B9F 10                    2197 	.db #0x10	; 16
      000BA0 12                    2198 	.db #0x12	; 18
      000BA1 12                    2199 	.db #0x12	; 18
      000BA2 0D                    2200 	.db #0x0d	; 13
      000BA3 00                    2201 	.db #0x00	; 0
      000BA4 00                    2202 	.db #0x00	; 0
      000BA5 18                    2203 	.db #0x18	; 24
      000BA6 00                    2204 	.db #0x00	; 0
      000BA7 00                    2205 	.db #0x00	; 0
      000BA8 0D                    2206 	.db #0x0d	; 13
      000BA9 12                    2207 	.db #0x12	; 18
      000BAA 12                    2208 	.db #0x12	; 18
      000BAB 12                    2209 	.db #0x12	; 18
      000BAC 0D                    2210 	.db #0x0d	; 13
      000BAD 00                    2211 	.db #0x00	; 0
      000BAE 00                    2212 	.db #0x00	; 0
      000BAF 18                    2213 	.db #0x18	; 24
      000BB0 00                    2214 	.db #0x00	; 0
      000BB1 00                    2215 	.db #0x00	; 0
      000BB2 10                    2216 	.db #0x10	; 16
      000BB3 18                    2217 	.db #0x18	; 24
      000BB4 14                    2218 	.db #0x14	; 20
      000BB5 12                    2219 	.db #0x12	; 18
      000BB6 11                    2220 	.db #0x11	; 17
      000BB7 00                    2221 	.db #0x00	; 0
      000BB8 00                    2222 	.db #0x00	; 0
      000BB9 10                    2223 	.db #0x10	; 16
      000BBA 18                    2224 	.db #0x18	; 24
      000BBB 14                    2225 	.db #0x14	; 20
      000BBC 12                    2226 	.db #0x12	; 18
      000BBD 11                    2227 	.db #0x11	; 17
      000BBE 00                    2228 	.db #0x00	; 0
      000BBF 00                    2229 	.db #0x00	; 0
      000BC0 00                    2230 	.db #0x00	; 0
      000BC1 00                    2231 	.db #0x00	; 0
      000BC2 00                    2232 	.db #0x00	; 0
      000BC3 00                    2233 	.db #0x00	; 0
      000BC4 00                    2234 	.db #0x00	; 0
      000BC5 00                    2235 	.db #0x00	; 0
      000BC6 00                    2236 	.db #0x00	; 0
      000BC7 00                    2237 	.db #0x00	; 0
      000BC8 00                    2238 	.db #0x00	; 0
      000BC9 00                    2239 	.db #0x00	; 0
      000BCA 00                    2240 	.db #0x00	; 0
      000BCB 00                    2241 	.db #0x00	; 0
      000BCC 00                    2242 	.db #0x00	; 0
      000BCD 00                    2243 	.db #0x00	; 0
      000BCE 00                    2244 	.db #0x00	; 0
      000BCF 00                    2245 	.db #0x00	; 0
      000BD0 00                    2246 	.db #0x00	; 0
      000BD1 00                    2247 	.db #0x00	; 0
      000BD2 00                    2248 	.db #0x00	; 0
      000BD3 00                    2249 	.db #0x00	; 0
      000BD4 00                    2250 	.db #0x00	; 0
      000BD5 00                    2251 	.db #0x00	; 0
      000BD6 00                    2252 	.db #0x00	; 0
      000BD7 00                    2253 	.db #0x00	; 0
      000BD8 00                    2254 	.db #0x00	; 0
      000BD9 00                    2255 	.db #0x00	; 0
      000BDA 00                    2256 	.db #0x00	; 0
      000BDB 00                    2257 	.db #0x00	; 0
      000BDC 00                    2258 	.db #0x00	; 0
      000BDD 00                    2259 	.db #0x00	; 0
      000BDE 00                    2260 	.db #0x00	; 0
      000BDF 00                    2261 	.db #0x00	; 0
      000BE0 00                    2262 	.db #0x00	; 0
      000BE1 00                    2263 	.db #0x00	; 0
      000BE2 00                    2264 	.db #0x00	; 0
      000BE3 00                    2265 	.db #0x00	; 0
      000BE4 00                    2266 	.db #0x00	; 0
      000BE5 00                    2267 	.db #0x00	; 0
      000BE6 00                    2268 	.db #0x00	; 0
      000BE7 00                    2269 	.db #0x00	; 0
      000BE8 00                    2270 	.db #0x00	; 0
      000BE9 00                    2271 	.db #0x00	; 0
      000BEA 00                    2272 	.db #0x00	; 0
      000BEB 00                    2273 	.db #0x00	; 0
      000BEC 00                    2274 	.db #0x00	; 0
      000BED 00                    2275 	.db #0x00	; 0
      000BEE 00                    2276 	.db #0x00	; 0
      000BEF 00                    2277 	.db #0x00	; 0
      000BF0 00                    2278 	.db #0x00	; 0
      000BF1 00                    2279 	.db #0x00	; 0
      000BF2 00                    2280 	.db #0x00	; 0
      000BF3 00                    2281 	.db #0x00	; 0
      000BF4 00                    2282 	.db #0x00	; 0
      000BF5 00                    2283 	.db #0x00	; 0
      000BF6 00                    2284 	.db #0x00	; 0
      000BF7 00                    2285 	.db #0x00	; 0
      000BF8 00                    2286 	.db #0x00	; 0
      000BF9 00                    2287 	.db #0x00	; 0
      000BFA 00                    2288 	.db #0x00	; 0
      000BFB 00                    2289 	.db #0x00	; 0
      000BFC 00                    2290 	.db #0x00	; 0
      000BFD 00                    2291 	.db #0x00	; 0
      000BFE 00                    2292 	.db #0x00	; 0
      000BFF 00                    2293 	.db #0x00	; 0
      000C00 00                    2294 	.db #0x00	; 0
      000C01 00                    2295 	.db #0x00	; 0
      000C02 00                    2296 	.db #0x00	; 0
      000C03 00                    2297 	.db #0x00	; 0
      000C04 00                    2298 	.db #0x00	; 0
      000C05 00                    2299 	.db #0x00	; 0
      000C06 00                    2300 	.db #0x00	; 0
      000C07 00                    2301 	.db #0x00	; 0
      000C08 00                    2302 	.db #0x00	; 0
      000C09 00                    2303 	.db #0x00	; 0
      000C0A 00                    2304 	.db #0x00	; 0
      000C0B 00                    2305 	.db #0x00	; 0
      000C0C 00                    2306 	.db #0x00	; 0
      000C0D 00                    2307 	.db #0x00	; 0
      000C0E 00                    2308 	.db #0x00	; 0
      000C0F 00                    2309 	.db #0x00	; 0
      000C10 00                    2310 	.db #0x00	; 0
      000C11 00                    2311 	.db #0x00	; 0
      000C12 00                    2312 	.db #0x00	; 0
      000C13 00                    2313 	.db #0x00	; 0
      000C14 00                    2314 	.db #0x00	; 0
      000C15 00                    2315 	.db #0x00	; 0
      000C16 00                    2316 	.db #0x00	; 0
      000C17 00                    2317 	.db #0x00	; 0
      000C18 00                    2318 	.db #0x00	; 0
      000C19 00                    2319 	.db #0x00	; 0
      000C1A 00                    2320 	.db #0x00	; 0
      000C1B 00                    2321 	.db #0x00	; 0
      000C1C 00                    2322 	.db #0x00	; 0
      000C1D 00                    2323 	.db #0x00	; 0
      000C1E 00                    2324 	.db #0x00	; 0
      000C1F 00                    2325 	.db #0x00	; 0
      000C20 80                    2326 	.db #0x80	; 128
      000C21 80                    2327 	.db #0x80	; 128
      000C22 80                    2328 	.db #0x80	; 128
      000C23 80                    2329 	.db #0x80	; 128
      000C24 80                    2330 	.db #0x80	; 128
      000C25 80                    2331 	.db #0x80	; 128
      000C26 80                    2332 	.db #0x80	; 128
      000C27 80                    2333 	.db #0x80	; 128
      000C28 00                    2334 	.db #0x00	; 0
      000C29 00                    2335 	.db #0x00	; 0
      000C2A 00                    2336 	.db #0x00	; 0
      000C2B 00                    2337 	.db #0x00	; 0
      000C2C 00                    2338 	.db #0x00	; 0
      000C2D 00                    2339 	.db #0x00	; 0
      000C2E 00                    2340 	.db #0x00	; 0
      000C2F 00                    2341 	.db #0x00	; 0
      000C30 00                    2342 	.db #0x00	; 0
      000C31 00                    2343 	.db #0x00	; 0
      000C32 00                    2344 	.db #0x00	; 0
      000C33 00                    2345 	.db #0x00	; 0
      000C34 00                    2346 	.db #0x00	; 0
      000C35 00                    2347 	.db #0x00	; 0
      000C36 00                    2348 	.db #0x00	; 0
      000C37 00                    2349 	.db #0x00	; 0
      000C38 00                    2350 	.db #0x00	; 0
      000C39 00                    2351 	.db #0x00	; 0
      000C3A 00                    2352 	.db #0x00	; 0
      000C3B 00                    2353 	.db #0x00	; 0
      000C3C 00                    2354 	.db #0x00	; 0
      000C3D 00                    2355 	.db #0x00	; 0
      000C3E 00                    2356 	.db #0x00	; 0
      000C3F 00                    2357 	.db #0x00	; 0
      000C40 00                    2358 	.db #0x00	; 0
      000C41 00                    2359 	.db #0x00	; 0
      000C42 00                    2360 	.db #0x00	; 0
      000C43 00                    2361 	.db #0x00	; 0
      000C44 00                    2362 	.db #0x00	; 0
      000C45 00                    2363 	.db #0x00	; 0
      000C46 00                    2364 	.db #0x00	; 0
      000C47 00                    2365 	.db #0x00	; 0
      000C48 00                    2366 	.db #0x00	; 0
      000C49 00                    2367 	.db #0x00	; 0
      000C4A 00                    2368 	.db #0x00	; 0
      000C4B 00                    2369 	.db #0x00	; 0
      000C4C 00                    2370 	.db #0x00	; 0
      000C4D 00                    2371 	.db #0x00	; 0
      000C4E 00                    2372 	.db #0x00	; 0
      000C4F 00                    2373 	.db #0x00	; 0
      000C50 00                    2374 	.db #0x00	; 0
      000C51 00                    2375 	.db #0x00	; 0
      000C52 00                    2376 	.db #0x00	; 0
      000C53 00                    2377 	.db #0x00	; 0
      000C54 00                    2378 	.db #0x00	; 0
      000C55 00                    2379 	.db #0x00	; 0
      000C56 00                    2380 	.db #0x00	; 0
      000C57 00                    2381 	.db #0x00	; 0
      000C58 00                    2382 	.db #0x00	; 0
      000C59 00                    2383 	.db #0x00	; 0
      000C5A 00                    2384 	.db #0x00	; 0
      000C5B 00                    2385 	.db #0x00	; 0
      000C5C 00                    2386 	.db #0x00	; 0
      000C5D 00                    2387 	.db #0x00	; 0
      000C5E 00                    2388 	.db #0x00	; 0
      000C5F 00                    2389 	.db #0x00	; 0
      000C60 00                    2390 	.db #0x00	; 0
      000C61 00                    2391 	.db #0x00	; 0
      000C62 00                    2392 	.db #0x00	; 0
      000C63 00                    2393 	.db #0x00	; 0
      000C64 00                    2394 	.db #0x00	; 0
      000C65 7F                    2395 	.db #0x7f	; 127
      000C66 03                    2396 	.db #0x03	; 3
      000C67 0C                    2397 	.db #0x0c	; 12
      000C68 30                    2398 	.db #0x30	; 48	'0'
      000C69 0C                    2399 	.db #0x0c	; 12
      000C6A 03                    2400 	.db #0x03	; 3
      000C6B 7F                    2401 	.db #0x7f	; 127
      000C6C 00                    2402 	.db #0x00	; 0
      000C6D 00                    2403 	.db #0x00	; 0
      000C6E 38                    2404 	.db #0x38	; 56	'8'
      000C6F 54                    2405 	.db #0x54	; 84	'T'
      000C70 54                    2406 	.db #0x54	; 84	'T'
      000C71 58                    2407 	.db #0x58	; 88	'X'
      000C72 00                    2408 	.db #0x00	; 0
      000C73 00                    2409 	.db #0x00	; 0
      000C74 7C                    2410 	.db #0x7c	; 124
      000C75 04                    2411 	.db #0x04	; 4
      000C76 04                    2412 	.db #0x04	; 4
      000C77 78                    2413 	.db #0x78	; 120	'x'
      000C78 00                    2414 	.db #0x00	; 0
      000C79 00                    2415 	.db #0x00	; 0
      000C7A 3C                    2416 	.db #0x3c	; 60
      000C7B 40                    2417 	.db #0x40	; 64
      000C7C 40                    2418 	.db #0x40	; 64
      000C7D 7C                    2419 	.db #0x7c	; 124
      000C7E 00                    2420 	.db #0x00	; 0
      000C7F 00                    2421 	.db #0x00	; 0
      000C80 00                    2422 	.db #0x00	; 0
      000C81 00                    2423 	.db #0x00	; 0
      000C82 00                    2424 	.db #0x00	; 0
      000C83 00                    2425 	.db #0x00	; 0
      000C84 00                    2426 	.db #0x00	; 0
      000C85 00                    2427 	.db #0x00	; 0
      000C86 00                    2428 	.db #0x00	; 0
      000C87 00                    2429 	.db #0x00	; 0
      000C88 00                    2430 	.db #0x00	; 0
      000C89 00                    2431 	.db #0x00	; 0
      000C8A 00                    2432 	.db #0x00	; 0
      000C8B 00                    2433 	.db #0x00	; 0
      000C8C 00                    2434 	.db #0x00	; 0
      000C8D 00                    2435 	.db #0x00	; 0
      000C8E 00                    2436 	.db #0x00	; 0
      000C8F 00                    2437 	.db #0x00	; 0
      000C90 00                    2438 	.db #0x00	; 0
      000C91 00                    2439 	.db #0x00	; 0
      000C92 00                    2440 	.db #0x00	; 0
      000C93 00                    2441 	.db #0x00	; 0
      000C94 00                    2442 	.db #0x00	; 0
      000C95 00                    2443 	.db #0x00	; 0
      000C96 00                    2444 	.db #0x00	; 0
      000C97 00                    2445 	.db #0x00	; 0
      000C98 00                    2446 	.db #0x00	; 0
      000C99 00                    2447 	.db #0x00	; 0
      000C9A 00                    2448 	.db #0x00	; 0
      000C9B 00                    2449 	.db #0x00	; 0
      000C9C 00                    2450 	.db #0x00	; 0
      000C9D 00                    2451 	.db #0x00	; 0
      000C9E 00                    2452 	.db #0x00	; 0
      000C9F 00                    2453 	.db #0x00	; 0
      000CA0 FF                    2454 	.db #0xff	; 255
      000CA1 AA                    2455 	.db #0xaa	; 170
      000CA2 AA                    2456 	.db #0xaa	; 170
      000CA3 AA                    2457 	.db #0xaa	; 170
      000CA4 28                    2458 	.db #0x28	; 40
      000CA5 08                    2459 	.db #0x08	; 8
      000CA6 00                    2460 	.db #0x00	; 0
      000CA7 FF                    2461 	.db #0xff	; 255
      000CA8 00                    2462 	.db #0x00	; 0
      000CA9 00                    2463 	.db #0x00	; 0
      000CAA 00                    2464 	.db #0x00	; 0
      000CAB 00                    2465 	.db #0x00	; 0
      000CAC 00                    2466 	.db #0x00	; 0
      000CAD 00                    2467 	.db #0x00	; 0
      000CAE 00                    2468 	.db #0x00	; 0
      000CAF 00                    2469 	.db #0x00	; 0
      000CB0 00                    2470 	.db #0x00	; 0
      000CB1 00                    2471 	.db #0x00	; 0
      000CB2 00                    2472 	.db #0x00	; 0
      000CB3 00                    2473 	.db #0x00	; 0
      000CB4 00                    2474 	.db #0x00	; 0
      000CB5 00                    2475 	.db #0x00	; 0
      000CB6 00                    2476 	.db #0x00	; 0
      000CB7 00                    2477 	.db #0x00	; 0
      000CB8 00                    2478 	.db #0x00	; 0
      000CB9 00                    2479 	.db #0x00	; 0
      000CBA 00                    2480 	.db #0x00	; 0
      000CBB 00                    2481 	.db #0x00	; 0
      000CBC 00                    2482 	.db #0x00	; 0
      000CBD 00                    2483 	.db #0x00	; 0
      000CBE 00                    2484 	.db #0x00	; 0
      000CBF 00                    2485 	.db #0x00	; 0
      000CC0 00                    2486 	.db #0x00	; 0
      000CC1 00                    2487 	.db #0x00	; 0
      000CC2 00                    2488 	.db #0x00	; 0
      000CC3 00                    2489 	.db #0x00	; 0
      000CC4 00                    2490 	.db #0x00	; 0
      000CC5 00                    2491 	.db #0x00	; 0
      000CC6 00                    2492 	.db #0x00	; 0
      000CC7 00                    2493 	.db #0x00	; 0
      000CC8 00                    2494 	.db #0x00	; 0
      000CC9 00                    2495 	.db #0x00	; 0
      000CCA 00                    2496 	.db #0x00	; 0
      000CCB 00                    2497 	.db #0x00	; 0
      000CCC 00                    2498 	.db #0x00	; 0
      000CCD 7F                    2499 	.db #0x7f	; 127
      000CCE 03                    2500 	.db #0x03	; 3
      000CCF 0C                    2501 	.db #0x0c	; 12
      000CD0 30                    2502 	.db #0x30	; 48	'0'
      000CD1 0C                    2503 	.db #0x0c	; 12
      000CD2 03                    2504 	.db #0x03	; 3
      000CD3 7F                    2505 	.db #0x7f	; 127
      000CD4 00                    2506 	.db #0x00	; 0
      000CD5 00                    2507 	.db #0x00	; 0
      000CD6 26                    2508 	.db #0x26	; 38
      000CD7 49                    2509 	.db #0x49	; 73	'I'
      000CD8 49                    2510 	.db #0x49	; 73	'I'
      000CD9 49                    2511 	.db #0x49	; 73	'I'
      000CDA 32                    2512 	.db #0x32	; 50	'2'
      000CDB 00                    2513 	.db #0x00	; 0
      000CDC 00                    2514 	.db #0x00	; 0
      000CDD 7F                    2515 	.db #0x7f	; 127
      000CDE 02                    2516 	.db #0x02	; 2
      000CDF 04                    2517 	.db #0x04	; 4
      000CE0 08                    2518 	.db #0x08	; 8
      000CE1 10                    2519 	.db #0x10	; 16
      000CE2 7F                    2520 	.db #0x7f	; 127
      000CE3 00                    2521 	.db #0x00	; 0
      000CE4                       2522 _BMP2:
      000CE4 00                    2523 	.db #0x00	; 0
      000CE5 03                    2524 	.db #0x03	; 3
      000CE6 05                    2525 	.db #0x05	; 5
      000CE7 09                    2526 	.db #0x09	; 9
      000CE8 11                    2527 	.db #0x11	; 17
      000CE9 FF                    2528 	.db #0xff	; 255
      000CEA 11                    2529 	.db #0x11	; 17
      000CEB 89                    2530 	.db #0x89	; 137
      000CEC 05                    2531 	.db #0x05	; 5
      000CED C3                    2532 	.db #0xc3	; 195
      000CEE 00                    2533 	.db #0x00	; 0
      000CEF E0                    2534 	.db #0xe0	; 224
      000CF0 00                    2535 	.db #0x00	; 0
      000CF1 F0                    2536 	.db #0xf0	; 240
      000CF2 00                    2537 	.db #0x00	; 0
      000CF3 F8                    2538 	.db #0xf8	; 248
      000CF4 00                    2539 	.db #0x00	; 0
      000CF5 00                    2540 	.db #0x00	; 0
      000CF6 00                    2541 	.db #0x00	; 0
      000CF7 00                    2542 	.db #0x00	; 0
      000CF8 00                    2543 	.db #0x00	; 0
      000CF9 00                    2544 	.db #0x00	; 0
      000CFA 00                    2545 	.db #0x00	; 0
      000CFB 44                    2546 	.db #0x44	; 68	'D'
      000CFC 28                    2547 	.db #0x28	; 40
      000CFD FF                    2548 	.db #0xff	; 255
      000CFE 11                    2549 	.db #0x11	; 17
      000CFF AA                    2550 	.db #0xaa	; 170
      000D00 44                    2551 	.db #0x44	; 68	'D'
      000D01 00                    2552 	.db #0x00	; 0
      000D02 00                    2553 	.db #0x00	; 0
      000D03 00                    2554 	.db #0x00	; 0
      000D04 00                    2555 	.db #0x00	; 0
      000D05 00                    2556 	.db #0x00	; 0
      000D06 00                    2557 	.db #0x00	; 0
      000D07 00                    2558 	.db #0x00	; 0
      000D08 00                    2559 	.db #0x00	; 0
      000D09 00                    2560 	.db #0x00	; 0
      000D0A 00                    2561 	.db #0x00	; 0
      000D0B 00                    2562 	.db #0x00	; 0
      000D0C 00                    2563 	.db #0x00	; 0
      000D0D 00                    2564 	.db #0x00	; 0
      000D0E 00                    2565 	.db #0x00	; 0
      000D0F 00                    2566 	.db #0x00	; 0
      000D10 00                    2567 	.db #0x00	; 0
      000D11 00                    2568 	.db #0x00	; 0
      000D12 00                    2569 	.db #0x00	; 0
      000D13 00                    2570 	.db #0x00	; 0
      000D14 00                    2571 	.db #0x00	; 0
      000D15 00                    2572 	.db #0x00	; 0
      000D16 00                    2573 	.db #0x00	; 0
      000D17 00                    2574 	.db #0x00	; 0
      000D18 00                    2575 	.db #0x00	; 0
      000D19 00                    2576 	.db #0x00	; 0
      000D1A 00                    2577 	.db #0x00	; 0
      000D1B 00                    2578 	.db #0x00	; 0
      000D1C 00                    2579 	.db #0x00	; 0
      000D1D 00                    2580 	.db #0x00	; 0
      000D1E 00                    2581 	.db #0x00	; 0
      000D1F 00                    2582 	.db #0x00	; 0
      000D20 00                    2583 	.db #0x00	; 0
      000D21 00                    2584 	.db #0x00	; 0
      000D22 00                    2585 	.db #0x00	; 0
      000D23 00                    2586 	.db #0x00	; 0
      000D24 00                    2587 	.db #0x00	; 0
      000D25 00                    2588 	.db #0x00	; 0
      000D26 00                    2589 	.db #0x00	; 0
      000D27 00                    2590 	.db #0x00	; 0
      000D28 00                    2591 	.db #0x00	; 0
      000D29 00                    2592 	.db #0x00	; 0
      000D2A 00                    2593 	.db #0x00	; 0
      000D2B 00                    2594 	.db #0x00	; 0
      000D2C 00                    2595 	.db #0x00	; 0
      000D2D 00                    2596 	.db #0x00	; 0
      000D2E 00                    2597 	.db #0x00	; 0
      000D2F 00                    2598 	.db #0x00	; 0
      000D30 00                    2599 	.db #0x00	; 0
      000D31 00                    2600 	.db #0x00	; 0
      000D32 00                    2601 	.db #0x00	; 0
      000D33 00                    2602 	.db #0x00	; 0
      000D34 00                    2603 	.db #0x00	; 0
      000D35 00                    2604 	.db #0x00	; 0
      000D36 00                    2605 	.db #0x00	; 0
      000D37 00                    2606 	.db #0x00	; 0
      000D38 00                    2607 	.db #0x00	; 0
      000D39 00                    2608 	.db #0x00	; 0
      000D3A 00                    2609 	.db #0x00	; 0
      000D3B 00                    2610 	.db #0x00	; 0
      000D3C 00                    2611 	.db #0x00	; 0
      000D3D 00                    2612 	.db #0x00	; 0
      000D3E 83                    2613 	.db #0x83	; 131
      000D3F 01                    2614 	.db #0x01	; 1
      000D40 38                    2615 	.db #0x38	; 56	'8'
      000D41 44                    2616 	.db #0x44	; 68	'D'
      000D42 82                    2617 	.db #0x82	; 130
      000D43 92                    2618 	.db #0x92	; 146
      000D44 92                    2619 	.db #0x92	; 146
      000D45 74                    2620 	.db #0x74	; 116	't'
      000D46 01                    2621 	.db #0x01	; 1
      000D47 83                    2622 	.db #0x83	; 131
      000D48 00                    2623 	.db #0x00	; 0
      000D49 00                    2624 	.db #0x00	; 0
      000D4A 00                    2625 	.db #0x00	; 0
      000D4B 00                    2626 	.db #0x00	; 0
      000D4C 00                    2627 	.db #0x00	; 0
      000D4D 00                    2628 	.db #0x00	; 0
      000D4E 00                    2629 	.db #0x00	; 0
      000D4F 7C                    2630 	.db #0x7c	; 124
      000D50 44                    2631 	.db #0x44	; 68	'D'
      000D51 FF                    2632 	.db #0xff	; 255
      000D52 01                    2633 	.db #0x01	; 1
      000D53 7D                    2634 	.db #0x7d	; 125
      000D54 7D                    2635 	.db #0x7d	; 125
      000D55 7D                    2636 	.db #0x7d	; 125
      000D56 7D                    2637 	.db #0x7d	; 125
      000D57 01                    2638 	.db #0x01	; 1
      000D58 7D                    2639 	.db #0x7d	; 125
      000D59 7D                    2640 	.db #0x7d	; 125
      000D5A 7D                    2641 	.db #0x7d	; 125
      000D5B 7D                    2642 	.db #0x7d	; 125
      000D5C 01                    2643 	.db #0x01	; 1
      000D5D 7D                    2644 	.db #0x7d	; 125
      000D5E 7D                    2645 	.db #0x7d	; 125
      000D5F 7D                    2646 	.db #0x7d	; 125
      000D60 7D                    2647 	.db #0x7d	; 125
      000D61 01                    2648 	.db #0x01	; 1
      000D62 FF                    2649 	.db #0xff	; 255
      000D63 00                    2650 	.db #0x00	; 0
      000D64 00                    2651 	.db #0x00	; 0
      000D65 00                    2652 	.db #0x00	; 0
      000D66 00                    2653 	.db #0x00	; 0
      000D67 00                    2654 	.db #0x00	; 0
      000D68 00                    2655 	.db #0x00	; 0
      000D69 01                    2656 	.db #0x01	; 1
      000D6A 00                    2657 	.db #0x00	; 0
      000D6B 01                    2658 	.db #0x01	; 1
      000D6C 00                    2659 	.db #0x00	; 0
      000D6D 01                    2660 	.db #0x01	; 1
      000D6E 00                    2661 	.db #0x00	; 0
      000D6F 01                    2662 	.db #0x01	; 1
      000D70 00                    2663 	.db #0x00	; 0
      000D71 01                    2664 	.db #0x01	; 1
      000D72 00                    2665 	.db #0x00	; 0
      000D73 01                    2666 	.db #0x01	; 1
      000D74 00                    2667 	.db #0x00	; 0
      000D75 00                    2668 	.db #0x00	; 0
      000D76 00                    2669 	.db #0x00	; 0
      000D77 00                    2670 	.db #0x00	; 0
      000D78 00                    2671 	.db #0x00	; 0
      000D79 00                    2672 	.db #0x00	; 0
      000D7A 00                    2673 	.db #0x00	; 0
      000D7B 00                    2674 	.db #0x00	; 0
      000D7C 00                    2675 	.db #0x00	; 0
      000D7D 01                    2676 	.db #0x01	; 1
      000D7E 01                    2677 	.db #0x01	; 1
      000D7F 00                    2678 	.db #0x00	; 0
      000D80 00                    2679 	.db #0x00	; 0
      000D81 00                    2680 	.db #0x00	; 0
      000D82 00                    2681 	.db #0x00	; 0
      000D83 00                    2682 	.db #0x00	; 0
      000D84 00                    2683 	.db #0x00	; 0
      000D85 00                    2684 	.db #0x00	; 0
      000D86 00                    2685 	.db #0x00	; 0
      000D87 00                    2686 	.db #0x00	; 0
      000D88 00                    2687 	.db #0x00	; 0
      000D89 00                    2688 	.db #0x00	; 0
      000D8A 00                    2689 	.db #0x00	; 0
      000D8B 00                    2690 	.db #0x00	; 0
      000D8C 00                    2691 	.db #0x00	; 0
      000D8D 00                    2692 	.db #0x00	; 0
      000D8E 00                    2693 	.db #0x00	; 0
      000D8F 00                    2694 	.db #0x00	; 0
      000D90 00                    2695 	.db #0x00	; 0
      000D91 00                    2696 	.db #0x00	; 0
      000D92 00                    2697 	.db #0x00	; 0
      000D93 00                    2698 	.db #0x00	; 0
      000D94 00                    2699 	.db #0x00	; 0
      000D95 00                    2700 	.db #0x00	; 0
      000D96 00                    2701 	.db #0x00	; 0
      000D97 00                    2702 	.db #0x00	; 0
      000D98 00                    2703 	.db #0x00	; 0
      000D99 00                    2704 	.db #0x00	; 0
      000D9A 00                    2705 	.db #0x00	; 0
      000D9B 00                    2706 	.db #0x00	; 0
      000D9C 00                    2707 	.db #0x00	; 0
      000D9D 00                    2708 	.db #0x00	; 0
      000D9E 00                    2709 	.db #0x00	; 0
      000D9F 00                    2710 	.db #0x00	; 0
      000DA0 00                    2711 	.db #0x00	; 0
      000DA1 00                    2712 	.db #0x00	; 0
      000DA2 00                    2713 	.db #0x00	; 0
      000DA3 00                    2714 	.db #0x00	; 0
      000DA4 00                    2715 	.db #0x00	; 0
      000DA5 00                    2716 	.db #0x00	; 0
      000DA6 00                    2717 	.db #0x00	; 0
      000DA7 00                    2718 	.db #0x00	; 0
      000DA8 00                    2719 	.db #0x00	; 0
      000DA9 00                    2720 	.db #0x00	; 0
      000DAA 00                    2721 	.db #0x00	; 0
      000DAB 00                    2722 	.db #0x00	; 0
      000DAC 00                    2723 	.db #0x00	; 0
      000DAD 00                    2724 	.db #0x00	; 0
      000DAE 00                    2725 	.db #0x00	; 0
      000DAF 00                    2726 	.db #0x00	; 0
      000DB0 00                    2727 	.db #0x00	; 0
      000DB1 00                    2728 	.db #0x00	; 0
      000DB2 00                    2729 	.db #0x00	; 0
      000DB3 00                    2730 	.db #0x00	; 0
      000DB4 00                    2731 	.db #0x00	; 0
      000DB5 00                    2732 	.db #0x00	; 0
      000DB6 00                    2733 	.db #0x00	; 0
      000DB7 00                    2734 	.db #0x00	; 0
      000DB8 00                    2735 	.db #0x00	; 0
      000DB9 00                    2736 	.db #0x00	; 0
      000DBA 00                    2737 	.db #0x00	; 0
      000DBB 00                    2738 	.db #0x00	; 0
      000DBC 00                    2739 	.db #0x00	; 0
      000DBD 00                    2740 	.db #0x00	; 0
      000DBE 01                    2741 	.db #0x01	; 1
      000DBF 01                    2742 	.db #0x01	; 1
      000DC0 00                    2743 	.db #0x00	; 0
      000DC1 00                    2744 	.db #0x00	; 0
      000DC2 00                    2745 	.db #0x00	; 0
      000DC3 00                    2746 	.db #0x00	; 0
      000DC4 00                    2747 	.db #0x00	; 0
      000DC5 00                    2748 	.db #0x00	; 0
      000DC6 01                    2749 	.db #0x01	; 1
      000DC7 01                    2750 	.db #0x01	; 1
      000DC8 00                    2751 	.db #0x00	; 0
      000DC9 00                    2752 	.db #0x00	; 0
      000DCA 00                    2753 	.db #0x00	; 0
      000DCB 00                    2754 	.db #0x00	; 0
      000DCC 00                    2755 	.db #0x00	; 0
      000DCD 00                    2756 	.db #0x00	; 0
      000DCE 00                    2757 	.db #0x00	; 0
      000DCF 00                    2758 	.db #0x00	; 0
      000DD0 00                    2759 	.db #0x00	; 0
      000DD1 01                    2760 	.db #0x01	; 1
      000DD2 01                    2761 	.db #0x01	; 1
      000DD3 01                    2762 	.db #0x01	; 1
      000DD4 01                    2763 	.db #0x01	; 1
      000DD5 01                    2764 	.db #0x01	; 1
      000DD6 01                    2765 	.db #0x01	; 1
      000DD7 01                    2766 	.db #0x01	; 1
      000DD8 01                    2767 	.db #0x01	; 1
      000DD9 01                    2768 	.db #0x01	; 1
      000DDA 01                    2769 	.db #0x01	; 1
      000DDB 01                    2770 	.db #0x01	; 1
      000DDC 01                    2771 	.db #0x01	; 1
      000DDD 01                    2772 	.db #0x01	; 1
      000DDE 01                    2773 	.db #0x01	; 1
      000DDF 01                    2774 	.db #0x01	; 1
      000DE0 01                    2775 	.db #0x01	; 1
      000DE1 01                    2776 	.db #0x01	; 1
      000DE2 01                    2777 	.db #0x01	; 1
      000DE3 00                    2778 	.db #0x00	; 0
      000DE4 00                    2779 	.db #0x00	; 0
      000DE5 00                    2780 	.db #0x00	; 0
      000DE6 00                    2781 	.db #0x00	; 0
      000DE7 00                    2782 	.db #0x00	; 0
      000DE8 00                    2783 	.db #0x00	; 0
      000DE9 00                    2784 	.db #0x00	; 0
      000DEA 00                    2785 	.db #0x00	; 0
      000DEB 00                    2786 	.db #0x00	; 0
      000DEC 00                    2787 	.db #0x00	; 0
      000DED 00                    2788 	.db #0x00	; 0
      000DEE 00                    2789 	.db #0x00	; 0
      000DEF 00                    2790 	.db #0x00	; 0
      000DF0 00                    2791 	.db #0x00	; 0
      000DF1 00                    2792 	.db #0x00	; 0
      000DF2 00                    2793 	.db #0x00	; 0
      000DF3 00                    2794 	.db #0x00	; 0
      000DF4 00                    2795 	.db #0x00	; 0
      000DF5 00                    2796 	.db #0x00	; 0
      000DF6 00                    2797 	.db #0x00	; 0
      000DF7 00                    2798 	.db #0x00	; 0
      000DF8 00                    2799 	.db #0x00	; 0
      000DF9 00                    2800 	.db #0x00	; 0
      000DFA 00                    2801 	.db #0x00	; 0
      000DFB 00                    2802 	.db #0x00	; 0
      000DFC 00                    2803 	.db #0x00	; 0
      000DFD 00                    2804 	.db #0x00	; 0
      000DFE 00                    2805 	.db #0x00	; 0
      000DFF 00                    2806 	.db #0x00	; 0
      000E00 00                    2807 	.db #0x00	; 0
      000E01 00                    2808 	.db #0x00	; 0
      000E02 00                    2809 	.db #0x00	; 0
      000E03 F8                    2810 	.db #0xf8	; 248
      000E04 08                    2811 	.db #0x08	; 8
      000E05 08                    2812 	.db #0x08	; 8
      000E06 08                    2813 	.db #0x08	; 8
      000E07 08                    2814 	.db #0x08	; 8
      000E08 08                    2815 	.db #0x08	; 8
      000E09 08                    2816 	.db #0x08	; 8
      000E0A 08                    2817 	.db #0x08	; 8
      000E0B 00                    2818 	.db #0x00	; 0
      000E0C F8                    2819 	.db #0xf8	; 248
      000E0D 18                    2820 	.db #0x18	; 24
      000E0E 60                    2821 	.db #0x60	; 96
      000E0F 80                    2822 	.db #0x80	; 128
      000E10 00                    2823 	.db #0x00	; 0
      000E11 00                    2824 	.db #0x00	; 0
      000E12 00                    2825 	.db #0x00	; 0
      000E13 80                    2826 	.db #0x80	; 128
      000E14 60                    2827 	.db #0x60	; 96
      000E15 18                    2828 	.db #0x18	; 24
      000E16 F8                    2829 	.db #0xf8	; 248
      000E17 00                    2830 	.db #0x00	; 0
      000E18 00                    2831 	.db #0x00	; 0
      000E19 00                    2832 	.db #0x00	; 0
      000E1A 20                    2833 	.db #0x20	; 32
      000E1B 20                    2834 	.db #0x20	; 32
      000E1C F8                    2835 	.db #0xf8	; 248
      000E1D 00                    2836 	.db #0x00	; 0
      000E1E 00                    2837 	.db #0x00	; 0
      000E1F 00                    2838 	.db #0x00	; 0
      000E20 00                    2839 	.db #0x00	; 0
      000E21 00                    2840 	.db #0x00	; 0
      000E22 00                    2841 	.db #0x00	; 0
      000E23 E0                    2842 	.db #0xe0	; 224
      000E24 10                    2843 	.db #0x10	; 16
      000E25 08                    2844 	.db #0x08	; 8
      000E26 08                    2845 	.db #0x08	; 8
      000E27 08                    2846 	.db #0x08	; 8
      000E28 08                    2847 	.db #0x08	; 8
      000E29 10                    2848 	.db #0x10	; 16
      000E2A E0                    2849 	.db #0xe0	; 224
      000E2B 00                    2850 	.db #0x00	; 0
      000E2C 00                    2851 	.db #0x00	; 0
      000E2D 00                    2852 	.db #0x00	; 0
      000E2E 20                    2853 	.db #0x20	; 32
      000E2F 20                    2854 	.db #0x20	; 32
      000E30 F8                    2855 	.db #0xf8	; 248
      000E31 00                    2856 	.db #0x00	; 0
      000E32 00                    2857 	.db #0x00	; 0
      000E33 00                    2858 	.db #0x00	; 0
      000E34 00                    2859 	.db #0x00	; 0
      000E35 00                    2860 	.db #0x00	; 0
      000E36 00                    2861 	.db #0x00	; 0
      000E37 00                    2862 	.db #0x00	; 0
      000E38 00                    2863 	.db #0x00	; 0
      000E39 00                    2864 	.db #0x00	; 0
      000E3A 00                    2865 	.db #0x00	; 0
      000E3B 00                    2866 	.db #0x00	; 0
      000E3C 00                    2867 	.db #0x00	; 0
      000E3D 08                    2868 	.db #0x08	; 8
      000E3E 08                    2869 	.db #0x08	; 8
      000E3F 08                    2870 	.db #0x08	; 8
      000E40 08                    2871 	.db #0x08	; 8
      000E41 08                    2872 	.db #0x08	; 8
      000E42 88                    2873 	.db #0x88	; 136
      000E43 68                    2874 	.db #0x68	; 104	'h'
      000E44 18                    2875 	.db #0x18	; 24
      000E45 00                    2876 	.db #0x00	; 0
      000E46 00                    2877 	.db #0x00	; 0
      000E47 00                    2878 	.db #0x00	; 0
      000E48 00                    2879 	.db #0x00	; 0
      000E49 00                    2880 	.db #0x00	; 0
      000E4A 00                    2881 	.db #0x00	; 0
      000E4B 00                    2882 	.db #0x00	; 0
      000E4C 00                    2883 	.db #0x00	; 0
      000E4D 00                    2884 	.db #0x00	; 0
      000E4E 00                    2885 	.db #0x00	; 0
      000E4F 00                    2886 	.db #0x00	; 0
      000E50 00                    2887 	.db #0x00	; 0
      000E51 00                    2888 	.db #0x00	; 0
      000E52 00                    2889 	.db #0x00	; 0
      000E53 00                    2890 	.db #0x00	; 0
      000E54 00                    2891 	.db #0x00	; 0
      000E55 00                    2892 	.db #0x00	; 0
      000E56 00                    2893 	.db #0x00	; 0
      000E57 00                    2894 	.db #0x00	; 0
      000E58 00                    2895 	.db #0x00	; 0
      000E59 00                    2896 	.db #0x00	; 0
      000E5A 00                    2897 	.db #0x00	; 0
      000E5B 00                    2898 	.db #0x00	; 0
      000E5C 00                    2899 	.db #0x00	; 0
      000E5D 00                    2900 	.db #0x00	; 0
      000E5E 00                    2901 	.db #0x00	; 0
      000E5F 00                    2902 	.db #0x00	; 0
      000E60 00                    2903 	.db #0x00	; 0
      000E61 00                    2904 	.db #0x00	; 0
      000E62 00                    2905 	.db #0x00	; 0
      000E63 00                    2906 	.db #0x00	; 0
      000E64 00                    2907 	.db #0x00	; 0
      000E65 00                    2908 	.db #0x00	; 0
      000E66 00                    2909 	.db #0x00	; 0
      000E67 00                    2910 	.db #0x00	; 0
      000E68 00                    2911 	.db #0x00	; 0
      000E69 00                    2912 	.db #0x00	; 0
      000E6A 00                    2913 	.db #0x00	; 0
      000E6B 00                    2914 	.db #0x00	; 0
      000E6C 00                    2915 	.db #0x00	; 0
      000E6D 00                    2916 	.db #0x00	; 0
      000E6E 00                    2917 	.db #0x00	; 0
      000E6F 00                    2918 	.db #0x00	; 0
      000E70 00                    2919 	.db #0x00	; 0
      000E71 00                    2920 	.db #0x00	; 0
      000E72 00                    2921 	.db #0x00	; 0
      000E73 00                    2922 	.db #0x00	; 0
      000E74 00                    2923 	.db #0x00	; 0
      000E75 00                    2924 	.db #0x00	; 0
      000E76 00                    2925 	.db #0x00	; 0
      000E77 00                    2926 	.db #0x00	; 0
      000E78 00                    2927 	.db #0x00	; 0
      000E79 00                    2928 	.db #0x00	; 0
      000E7A 00                    2929 	.db #0x00	; 0
      000E7B 00                    2930 	.db #0x00	; 0
      000E7C 00                    2931 	.db #0x00	; 0
      000E7D 00                    2932 	.db #0x00	; 0
      000E7E 00                    2933 	.db #0x00	; 0
      000E7F 00                    2934 	.db #0x00	; 0
      000E80 00                    2935 	.db #0x00	; 0
      000E81 00                    2936 	.db #0x00	; 0
      000E82 00                    2937 	.db #0x00	; 0
      000E83 7F                    2938 	.db #0x7f	; 127
      000E84 01                    2939 	.db #0x01	; 1
      000E85 01                    2940 	.db #0x01	; 1
      000E86 01                    2941 	.db #0x01	; 1
      000E87 01                    2942 	.db #0x01	; 1
      000E88 01                    2943 	.db #0x01	; 1
      000E89 01                    2944 	.db #0x01	; 1
      000E8A 00                    2945 	.db #0x00	; 0
      000E8B 00                    2946 	.db #0x00	; 0
      000E8C 7F                    2947 	.db #0x7f	; 127
      000E8D 00                    2948 	.db #0x00	; 0
      000E8E 00                    2949 	.db #0x00	; 0
      000E8F 01                    2950 	.db #0x01	; 1
      000E90 06                    2951 	.db #0x06	; 6
      000E91 18                    2952 	.db #0x18	; 24
      000E92 06                    2953 	.db #0x06	; 6
      000E93 01                    2954 	.db #0x01	; 1
      000E94 00                    2955 	.db #0x00	; 0
      000E95 00                    2956 	.db #0x00	; 0
      000E96 7F                    2957 	.db #0x7f	; 127
      000E97 00                    2958 	.db #0x00	; 0
      000E98 00                    2959 	.db #0x00	; 0
      000E99 00                    2960 	.db #0x00	; 0
      000E9A 40                    2961 	.db #0x40	; 64
      000E9B 40                    2962 	.db #0x40	; 64
      000E9C 7F                    2963 	.db #0x7f	; 127
      000E9D 40                    2964 	.db #0x40	; 64
      000E9E 40                    2965 	.db #0x40	; 64
      000E9F 00                    2966 	.db #0x00	; 0
      000EA0 00                    2967 	.db #0x00	; 0
      000EA1 00                    2968 	.db #0x00	; 0
      000EA2 00                    2969 	.db #0x00	; 0
      000EA3 1F                    2970 	.db #0x1f	; 31
      000EA4 20                    2971 	.db #0x20	; 32
      000EA5 40                    2972 	.db #0x40	; 64
      000EA6 40                    2973 	.db #0x40	; 64
      000EA7 40                    2974 	.db #0x40	; 64
      000EA8 40                    2975 	.db #0x40	; 64
      000EA9 20                    2976 	.db #0x20	; 32
      000EAA 1F                    2977 	.db #0x1f	; 31
      000EAB 00                    2978 	.db #0x00	; 0
      000EAC 00                    2979 	.db #0x00	; 0
      000EAD 00                    2980 	.db #0x00	; 0
      000EAE 40                    2981 	.db #0x40	; 64
      000EAF 40                    2982 	.db #0x40	; 64
      000EB0 7F                    2983 	.db #0x7f	; 127
      000EB1 40                    2984 	.db #0x40	; 64
      000EB2 40                    2985 	.db #0x40	; 64
      000EB3 00                    2986 	.db #0x00	; 0
      000EB4 00                    2987 	.db #0x00	; 0
      000EB5 00                    2988 	.db #0x00	; 0
      000EB6 00                    2989 	.db #0x00	; 0
      000EB7 00                    2990 	.db #0x00	; 0
      000EB8 60                    2991 	.db #0x60	; 96
      000EB9 00                    2992 	.db #0x00	; 0
      000EBA 00                    2993 	.db #0x00	; 0
      000EBB 00                    2994 	.db #0x00	; 0
      000EBC 00                    2995 	.db #0x00	; 0
      000EBD 00                    2996 	.db #0x00	; 0
      000EBE 00                    2997 	.db #0x00	; 0
      000EBF 60                    2998 	.db #0x60	; 96
      000EC0 18                    2999 	.db #0x18	; 24
      000EC1 06                    3000 	.db #0x06	; 6
      000EC2 01                    3001 	.db #0x01	; 1
      000EC3 00                    3002 	.db #0x00	; 0
      000EC4 00                    3003 	.db #0x00	; 0
      000EC5 00                    3004 	.db #0x00	; 0
      000EC6 00                    3005 	.db #0x00	; 0
      000EC7 00                    3006 	.db #0x00	; 0
      000EC8 00                    3007 	.db #0x00	; 0
      000EC9 00                    3008 	.db #0x00	; 0
      000ECA 00                    3009 	.db #0x00	; 0
      000ECB 00                    3010 	.db #0x00	; 0
      000ECC 00                    3011 	.db #0x00	; 0
      000ECD 00                    3012 	.db #0x00	; 0
      000ECE 00                    3013 	.db #0x00	; 0
      000ECF 00                    3014 	.db #0x00	; 0
      000ED0 00                    3015 	.db #0x00	; 0
      000ED1 00                    3016 	.db #0x00	; 0
      000ED2 00                    3017 	.db #0x00	; 0
      000ED3 00                    3018 	.db #0x00	; 0
      000ED4 00                    3019 	.db #0x00	; 0
      000ED5 00                    3020 	.db #0x00	; 0
      000ED6 00                    3021 	.db #0x00	; 0
      000ED7 00                    3022 	.db #0x00	; 0
      000ED8 00                    3023 	.db #0x00	; 0
      000ED9 00                    3024 	.db #0x00	; 0
      000EDA 00                    3025 	.db #0x00	; 0
      000EDB 00                    3026 	.db #0x00	; 0
      000EDC 00                    3027 	.db #0x00	; 0
      000EDD 00                    3028 	.db #0x00	; 0
      000EDE 00                    3029 	.db #0x00	; 0
      000EDF 00                    3030 	.db #0x00	; 0
      000EE0 00                    3031 	.db #0x00	; 0
      000EE1 00                    3032 	.db #0x00	; 0
      000EE2 00                    3033 	.db #0x00	; 0
      000EE3 00                    3034 	.db #0x00	; 0
      000EE4 00                    3035 	.db #0x00	; 0
      000EE5 00                    3036 	.db #0x00	; 0
      000EE6 00                    3037 	.db #0x00	; 0
      000EE7 00                    3038 	.db #0x00	; 0
      000EE8 00                    3039 	.db #0x00	; 0
      000EE9 00                    3040 	.db #0x00	; 0
      000EEA 00                    3041 	.db #0x00	; 0
      000EEB 00                    3042 	.db #0x00	; 0
      000EEC 00                    3043 	.db #0x00	; 0
      000EED 00                    3044 	.db #0x00	; 0
      000EEE 00                    3045 	.db #0x00	; 0
      000EEF 00                    3046 	.db #0x00	; 0
      000EF0 00                    3047 	.db #0x00	; 0
      000EF1 00                    3048 	.db #0x00	; 0
      000EF2 00                    3049 	.db #0x00	; 0
      000EF3 00                    3050 	.db #0x00	; 0
      000EF4 00                    3051 	.db #0x00	; 0
      000EF5 00                    3052 	.db #0x00	; 0
      000EF6 00                    3053 	.db #0x00	; 0
      000EF7 00                    3054 	.db #0x00	; 0
      000EF8 00                    3055 	.db #0x00	; 0
      000EF9 00                    3056 	.db #0x00	; 0
      000EFA 00                    3057 	.db #0x00	; 0
      000EFB 00                    3058 	.db #0x00	; 0
      000EFC 00                    3059 	.db #0x00	; 0
      000EFD 00                    3060 	.db #0x00	; 0
      000EFE 00                    3061 	.db #0x00	; 0
      000EFF 00                    3062 	.db #0x00	; 0
      000F00 00                    3063 	.db #0x00	; 0
      000F01 00                    3064 	.db #0x00	; 0
      000F02 00                    3065 	.db #0x00	; 0
      000F03 00                    3066 	.db #0x00	; 0
      000F04 00                    3067 	.db #0x00	; 0
      000F05 00                    3068 	.db #0x00	; 0
      000F06 00                    3069 	.db #0x00	; 0
      000F07 00                    3070 	.db #0x00	; 0
      000F08 00                    3071 	.db #0x00	; 0
      000F09 40                    3072 	.db #0x40	; 64
      000F0A 20                    3073 	.db #0x20	; 32
      000F0B 20                    3074 	.db #0x20	; 32
      000F0C 20                    3075 	.db #0x20	; 32
      000F0D C0                    3076 	.db #0xc0	; 192
      000F0E 00                    3077 	.db #0x00	; 0
      000F0F 00                    3078 	.db #0x00	; 0
      000F10 E0                    3079 	.db #0xe0	; 224
      000F11 20                    3080 	.db #0x20	; 32
      000F12 20                    3081 	.db #0x20	; 32
      000F13 20                    3082 	.db #0x20	; 32
      000F14 E0                    3083 	.db #0xe0	; 224
      000F15 00                    3084 	.db #0x00	; 0
      000F16 00                    3085 	.db #0x00	; 0
      000F17 00                    3086 	.db #0x00	; 0
      000F18 40                    3087 	.db #0x40	; 64
      000F19 E0                    3088 	.db #0xe0	; 224
      000F1A 00                    3089 	.db #0x00	; 0
      000F1B 00                    3090 	.db #0x00	; 0
      000F1C 00                    3091 	.db #0x00	; 0
      000F1D 00                    3092 	.db #0x00	; 0
      000F1E 60                    3093 	.db #0x60	; 96
      000F1F 20                    3094 	.db #0x20	; 32
      000F20 20                    3095 	.db #0x20	; 32
      000F21 20                    3096 	.db #0x20	; 32
      000F22 E0                    3097 	.db #0xe0	; 224
      000F23 00                    3098 	.db #0x00	; 0
      000F24 00                    3099 	.db #0x00	; 0
      000F25 00                    3100 	.db #0x00	; 0
      000F26 00                    3101 	.db #0x00	; 0
      000F27 00                    3102 	.db #0x00	; 0
      000F28 E0                    3103 	.db #0xe0	; 224
      000F29 20                    3104 	.db #0x20	; 32
      000F2A 20                    3105 	.db #0x20	; 32
      000F2B 20                    3106 	.db #0x20	; 32
      000F2C E0                    3107 	.db #0xe0	; 224
      000F2D 00                    3108 	.db #0x00	; 0
      000F2E 00                    3109 	.db #0x00	; 0
      000F2F 00                    3110 	.db #0x00	; 0
      000F30 00                    3111 	.db #0x00	; 0
      000F31 00                    3112 	.db #0x00	; 0
      000F32 40                    3113 	.db #0x40	; 64
      000F33 20                    3114 	.db #0x20	; 32
      000F34 20                    3115 	.db #0x20	; 32
      000F35 20                    3116 	.db #0x20	; 32
      000F36 C0                    3117 	.db #0xc0	; 192
      000F37 00                    3118 	.db #0x00	; 0
      000F38 00                    3119 	.db #0x00	; 0
      000F39 40                    3120 	.db #0x40	; 64
      000F3A 20                    3121 	.db #0x20	; 32
      000F3B 20                    3122 	.db #0x20	; 32
      000F3C 20                    3123 	.db #0x20	; 32
      000F3D C0                    3124 	.db #0xc0	; 192
      000F3E 00                    3125 	.db #0x00	; 0
      000F3F 00                    3126 	.db #0x00	; 0
      000F40 00                    3127 	.db #0x00	; 0
      000F41 00                    3128 	.db #0x00	; 0
      000F42 00                    3129 	.db #0x00	; 0
      000F43 00                    3130 	.db #0x00	; 0
      000F44 00                    3131 	.db #0x00	; 0
      000F45 00                    3132 	.db #0x00	; 0
      000F46 00                    3133 	.db #0x00	; 0
      000F47 00                    3134 	.db #0x00	; 0
      000F48 00                    3135 	.db #0x00	; 0
      000F49 00                    3136 	.db #0x00	; 0
      000F4A 00                    3137 	.db #0x00	; 0
      000F4B 00                    3138 	.db #0x00	; 0
      000F4C 00                    3139 	.db #0x00	; 0
      000F4D 00                    3140 	.db #0x00	; 0
      000F4E 00                    3141 	.db #0x00	; 0
      000F4F 00                    3142 	.db #0x00	; 0
      000F50 00                    3143 	.db #0x00	; 0
      000F51 00                    3144 	.db #0x00	; 0
      000F52 00                    3145 	.db #0x00	; 0
      000F53 00                    3146 	.db #0x00	; 0
      000F54 00                    3147 	.db #0x00	; 0
      000F55 00                    3148 	.db #0x00	; 0
      000F56 00                    3149 	.db #0x00	; 0
      000F57 00                    3150 	.db #0x00	; 0
      000F58 00                    3151 	.db #0x00	; 0
      000F59 00                    3152 	.db #0x00	; 0
      000F5A 00                    3153 	.db #0x00	; 0
      000F5B 00                    3154 	.db #0x00	; 0
      000F5C 00                    3155 	.db #0x00	; 0
      000F5D 00                    3156 	.db #0x00	; 0
      000F5E 00                    3157 	.db #0x00	; 0
      000F5F 00                    3158 	.db #0x00	; 0
      000F60 00                    3159 	.db #0x00	; 0
      000F61 00                    3160 	.db #0x00	; 0
      000F62 00                    3161 	.db #0x00	; 0
      000F63 00                    3162 	.db #0x00	; 0
      000F64 00                    3163 	.db #0x00	; 0
      000F65 00                    3164 	.db #0x00	; 0
      000F66 00                    3165 	.db #0x00	; 0
      000F67 00                    3166 	.db #0x00	; 0
      000F68 00                    3167 	.db #0x00	; 0
      000F69 00                    3168 	.db #0x00	; 0
      000F6A 00                    3169 	.db #0x00	; 0
      000F6B 00                    3170 	.db #0x00	; 0
      000F6C 00                    3171 	.db #0x00	; 0
      000F6D 00                    3172 	.db #0x00	; 0
      000F6E 00                    3173 	.db #0x00	; 0
      000F6F 00                    3174 	.db #0x00	; 0
      000F70 00                    3175 	.db #0x00	; 0
      000F71 00                    3176 	.db #0x00	; 0
      000F72 00                    3177 	.db #0x00	; 0
      000F73 00                    3178 	.db #0x00	; 0
      000F74 00                    3179 	.db #0x00	; 0
      000F75 00                    3180 	.db #0x00	; 0
      000F76 00                    3181 	.db #0x00	; 0
      000F77 00                    3182 	.db #0x00	; 0
      000F78 00                    3183 	.db #0x00	; 0
      000F79 00                    3184 	.db #0x00	; 0
      000F7A 00                    3185 	.db #0x00	; 0
      000F7B 00                    3186 	.db #0x00	; 0
      000F7C 00                    3187 	.db #0x00	; 0
      000F7D 00                    3188 	.db #0x00	; 0
      000F7E 00                    3189 	.db #0x00	; 0
      000F7F 00                    3190 	.db #0x00	; 0
      000F80 00                    3191 	.db #0x00	; 0
      000F81 00                    3192 	.db #0x00	; 0
      000F82 00                    3193 	.db #0x00	; 0
      000F83 00                    3194 	.db #0x00	; 0
      000F84 00                    3195 	.db #0x00	; 0
      000F85 00                    3196 	.db #0x00	; 0
      000F86 00                    3197 	.db #0x00	; 0
      000F87 00                    3198 	.db #0x00	; 0
      000F88 00                    3199 	.db #0x00	; 0
      000F89 0C                    3200 	.db #0x0c	; 12
      000F8A 0A                    3201 	.db #0x0a	; 10
      000F8B 0A                    3202 	.db #0x0a	; 10
      000F8C 09                    3203 	.db #0x09	; 9
      000F8D 0C                    3204 	.db #0x0c	; 12
      000F8E 00                    3205 	.db #0x00	; 0
      000F8F 00                    3206 	.db #0x00	; 0
      000F90 0F                    3207 	.db #0x0f	; 15
      000F91 08                    3208 	.db #0x08	; 8
      000F92 08                    3209 	.db #0x08	; 8
      000F93 08                    3210 	.db #0x08	; 8
      000F94 0F                    3211 	.db #0x0f	; 15
      000F95 00                    3212 	.db #0x00	; 0
      000F96 00                    3213 	.db #0x00	; 0
      000F97 00                    3214 	.db #0x00	; 0
      000F98 08                    3215 	.db #0x08	; 8
      000F99 0F                    3216 	.db #0x0f	; 15
      000F9A 08                    3217 	.db #0x08	; 8
      000F9B 00                    3218 	.db #0x00	; 0
      000F9C 00                    3219 	.db #0x00	; 0
      000F9D 00                    3220 	.db #0x00	; 0
      000F9E 0C                    3221 	.db #0x0c	; 12
      000F9F 08                    3222 	.db #0x08	; 8
      000FA0 09                    3223 	.db #0x09	; 9
      000FA1 09                    3224 	.db #0x09	; 9
      000FA2 0E                    3225 	.db #0x0e	; 14
      000FA3 00                    3226 	.db #0x00	; 0
      000FA4 00                    3227 	.db #0x00	; 0
      000FA5 0C                    3228 	.db #0x0c	; 12
      000FA6 00                    3229 	.db #0x00	; 0
      000FA7 00                    3230 	.db #0x00	; 0
      000FA8 0F                    3231 	.db #0x0f	; 15
      000FA9 09                    3232 	.db #0x09	; 9
      000FAA 09                    3233 	.db #0x09	; 9
      000FAB 09                    3234 	.db #0x09	; 9
      000FAC 0F                    3235 	.db #0x0f	; 15
      000FAD 00                    3236 	.db #0x00	; 0
      000FAE 00                    3237 	.db #0x00	; 0
      000FAF 0C                    3238 	.db #0x0c	; 12
      000FB0 00                    3239 	.db #0x00	; 0
      000FB1 00                    3240 	.db #0x00	; 0
      000FB2 0C                    3241 	.db #0x0c	; 12
      000FB3 0A                    3242 	.db #0x0a	; 10
      000FB4 0A                    3243 	.db #0x0a	; 10
      000FB5 09                    3244 	.db #0x09	; 9
      000FB6 0C                    3245 	.db #0x0c	; 12
      000FB7 00                    3246 	.db #0x00	; 0
      000FB8 00                    3247 	.db #0x00	; 0
      000FB9 0C                    3248 	.db #0x0c	; 12
      000FBA 0A                    3249 	.db #0x0a	; 10
      000FBB 0A                    3250 	.db #0x0a	; 10
      000FBC 09                    3251 	.db #0x09	; 9
      000FBD 0C                    3252 	.db #0x0c	; 12
      000FBE 00                    3253 	.db #0x00	; 0
      000FBF 00                    3254 	.db #0x00	; 0
      000FC0 00                    3255 	.db #0x00	; 0
      000FC1 00                    3256 	.db #0x00	; 0
      000FC2 00                    3257 	.db #0x00	; 0
      000FC3 00                    3258 	.db #0x00	; 0
      000FC4 00                    3259 	.db #0x00	; 0
      000FC5 00                    3260 	.db #0x00	; 0
      000FC6 00                    3261 	.db #0x00	; 0
      000FC7 00                    3262 	.db #0x00	; 0
      000FC8 00                    3263 	.db #0x00	; 0
      000FC9 00                    3264 	.db #0x00	; 0
      000FCA 00                    3265 	.db #0x00	; 0
      000FCB 00                    3266 	.db #0x00	; 0
      000FCC 00                    3267 	.db #0x00	; 0
      000FCD 00                    3268 	.db #0x00	; 0
      000FCE 00                    3269 	.db #0x00	; 0
      000FCF 00                    3270 	.db #0x00	; 0
      000FD0 00                    3271 	.db #0x00	; 0
      000FD1 00                    3272 	.db #0x00	; 0
      000FD2 00                    3273 	.db #0x00	; 0
      000FD3 00                    3274 	.db #0x00	; 0
      000FD4 00                    3275 	.db #0x00	; 0
      000FD5 00                    3276 	.db #0x00	; 0
      000FD6 00                    3277 	.db #0x00	; 0
      000FD7 00                    3278 	.db #0x00	; 0
      000FD8 00                    3279 	.db #0x00	; 0
      000FD9 00                    3280 	.db #0x00	; 0
      000FDA 00                    3281 	.db #0x00	; 0
      000FDB 00                    3282 	.db #0x00	; 0
      000FDC 00                    3283 	.db #0x00	; 0
      000FDD 00                    3284 	.db #0x00	; 0
      000FDE 00                    3285 	.db #0x00	; 0
      000FDF 00                    3286 	.db #0x00	; 0
      000FE0 00                    3287 	.db #0x00	; 0
      000FE1 00                    3288 	.db #0x00	; 0
      000FE2 00                    3289 	.db #0x00	; 0
      000FE3 00                    3290 	.db #0x00	; 0
      000FE4 00                    3291 	.db #0x00	; 0
      000FE5 00                    3292 	.db #0x00	; 0
      000FE6 00                    3293 	.db #0x00	; 0
      000FE7 00                    3294 	.db #0x00	; 0
      000FE8 00                    3295 	.db #0x00	; 0
      000FE9 00                    3296 	.db #0x00	; 0
      000FEA 00                    3297 	.db #0x00	; 0
      000FEB 00                    3298 	.db #0x00	; 0
      000FEC 00                    3299 	.db #0x00	; 0
      000FED 00                    3300 	.db #0x00	; 0
      000FEE 00                    3301 	.db #0x00	; 0
      000FEF 00                    3302 	.db #0x00	; 0
      000FF0 00                    3303 	.db #0x00	; 0
      000FF1 00                    3304 	.db #0x00	; 0
      000FF2 00                    3305 	.db #0x00	; 0
      000FF3 00                    3306 	.db #0x00	; 0
      000FF4 00                    3307 	.db #0x00	; 0
      000FF5 00                    3308 	.db #0x00	; 0
      000FF6 00                    3309 	.db #0x00	; 0
      000FF7 00                    3310 	.db #0x00	; 0
      000FF8 00                    3311 	.db #0x00	; 0
      000FF9 00                    3312 	.db #0x00	; 0
      000FFA 00                    3313 	.db #0x00	; 0
      000FFB 00                    3314 	.db #0x00	; 0
      000FFC 00                    3315 	.db #0x00	; 0
      000FFD 00                    3316 	.db #0x00	; 0
      000FFE 00                    3317 	.db #0x00	; 0
      000FFF 00                    3318 	.db #0x00	; 0
      001000 00                    3319 	.db #0x00	; 0
      001001 00                    3320 	.db #0x00	; 0
      001002 00                    3321 	.db #0x00	; 0
      001003 00                    3322 	.db #0x00	; 0
      001004 00                    3323 	.db #0x00	; 0
      001005 00                    3324 	.db #0x00	; 0
      001006 00                    3325 	.db #0x00	; 0
      001007 00                    3326 	.db #0x00	; 0
      001008 00                    3327 	.db #0x00	; 0
      001009 00                    3328 	.db #0x00	; 0
      00100A 00                    3329 	.db #0x00	; 0
      00100B 00                    3330 	.db #0x00	; 0
      00100C 00                    3331 	.db #0x00	; 0
      00100D 00                    3332 	.db #0x00	; 0
      00100E 00                    3333 	.db #0x00	; 0
      00100F 00                    3334 	.db #0x00	; 0
      001010 00                    3335 	.db #0x00	; 0
      001011 00                    3336 	.db #0x00	; 0
      001012 00                    3337 	.db #0x00	; 0
      001013 00                    3338 	.db #0x00	; 0
      001014 00                    3339 	.db #0x00	; 0
      001015 00                    3340 	.db #0x00	; 0
      001016 00                    3341 	.db #0x00	; 0
      001017 00                    3342 	.db #0x00	; 0
      001018 00                    3343 	.db #0x00	; 0
      001019 00                    3344 	.db #0x00	; 0
      00101A 00                    3345 	.db #0x00	; 0
      00101B 00                    3346 	.db #0x00	; 0
      00101C 00                    3347 	.db #0x00	; 0
      00101D 00                    3348 	.db #0x00	; 0
      00101E 00                    3349 	.db #0x00	; 0
      00101F 00                    3350 	.db #0x00	; 0
      001020 80                    3351 	.db #0x80	; 128
      001021 80                    3352 	.db #0x80	; 128
      001022 80                    3353 	.db #0x80	; 128
      001023 80                    3354 	.db #0x80	; 128
      001024 80                    3355 	.db #0x80	; 128
      001025 80                    3356 	.db #0x80	; 128
      001026 80                    3357 	.db #0x80	; 128
      001027 80                    3358 	.db #0x80	; 128
      001028 00                    3359 	.db #0x00	; 0
      001029 00                    3360 	.db #0x00	; 0
      00102A 00                    3361 	.db #0x00	; 0
      00102B 00                    3362 	.db #0x00	; 0
      00102C 00                    3363 	.db #0x00	; 0
      00102D 00                    3364 	.db #0x00	; 0
      00102E 00                    3365 	.db #0x00	; 0
      00102F 00                    3366 	.db #0x00	; 0
      001030 00                    3367 	.db #0x00	; 0
      001031 00                    3368 	.db #0x00	; 0
      001032 00                    3369 	.db #0x00	; 0
      001033 00                    3370 	.db #0x00	; 0
      001034 00                    3371 	.db #0x00	; 0
      001035 00                    3372 	.db #0x00	; 0
      001036 00                    3373 	.db #0x00	; 0
      001037 00                    3374 	.db #0x00	; 0
      001038 00                    3375 	.db #0x00	; 0
      001039 00                    3376 	.db #0x00	; 0
      00103A 00                    3377 	.db #0x00	; 0
      00103B 00                    3378 	.db #0x00	; 0
      00103C 00                    3379 	.db #0x00	; 0
      00103D 00                    3380 	.db #0x00	; 0
      00103E 00                    3381 	.db #0x00	; 0
      00103F 00                    3382 	.db #0x00	; 0
      001040 00                    3383 	.db #0x00	; 0
      001041 00                    3384 	.db #0x00	; 0
      001042 00                    3385 	.db #0x00	; 0
      001043 00                    3386 	.db #0x00	; 0
      001044 00                    3387 	.db #0x00	; 0
      001045 00                    3388 	.db #0x00	; 0
      001046 00                    3389 	.db #0x00	; 0
      001047 00                    3390 	.db #0x00	; 0
      001048 00                    3391 	.db #0x00	; 0
      001049 00                    3392 	.db #0x00	; 0
      00104A 00                    3393 	.db #0x00	; 0
      00104B 00                    3394 	.db #0x00	; 0
      00104C 00                    3395 	.db #0x00	; 0
      00104D 00                    3396 	.db #0x00	; 0
      00104E 00                    3397 	.db #0x00	; 0
      00104F 00                    3398 	.db #0x00	; 0
      001050 00                    3399 	.db #0x00	; 0
      001051 00                    3400 	.db #0x00	; 0
      001052 00                    3401 	.db #0x00	; 0
      001053 00                    3402 	.db #0x00	; 0
      001054 00                    3403 	.db #0x00	; 0
      001055 00                    3404 	.db #0x00	; 0
      001056 00                    3405 	.db #0x00	; 0
      001057 00                    3406 	.db #0x00	; 0
      001058 00                    3407 	.db #0x00	; 0
      001059 00                    3408 	.db #0x00	; 0
      00105A 00                    3409 	.db #0x00	; 0
      00105B 00                    3410 	.db #0x00	; 0
      00105C 00                    3411 	.db #0x00	; 0
      00105D 00                    3412 	.db #0x00	; 0
      00105E 00                    3413 	.db #0x00	; 0
      00105F 00                    3414 	.db #0x00	; 0
      001060 00                    3415 	.db #0x00	; 0
      001061 00                    3416 	.db #0x00	; 0
      001062 00                    3417 	.db #0x00	; 0
      001063 00                    3418 	.db #0x00	; 0
      001064 00                    3419 	.db #0x00	; 0
      001065 7F                    3420 	.db #0x7f	; 127
      001066 03                    3421 	.db #0x03	; 3
      001067 0C                    3422 	.db #0x0c	; 12
      001068 30                    3423 	.db #0x30	; 48	'0'
      001069 0C                    3424 	.db #0x0c	; 12
      00106A 03                    3425 	.db #0x03	; 3
      00106B 7F                    3426 	.db #0x7f	; 127
      00106C 00                    3427 	.db #0x00	; 0
      00106D 00                    3428 	.db #0x00	; 0
      00106E 38                    3429 	.db #0x38	; 56	'8'
      00106F 54                    3430 	.db #0x54	; 84	'T'
      001070 54                    3431 	.db #0x54	; 84	'T'
      001071 58                    3432 	.db #0x58	; 88	'X'
      001072 00                    3433 	.db #0x00	; 0
      001073 00                    3434 	.db #0x00	; 0
      001074 7C                    3435 	.db #0x7c	; 124
      001075 04                    3436 	.db #0x04	; 4
      001076 04                    3437 	.db #0x04	; 4
      001077 78                    3438 	.db #0x78	; 120	'x'
      001078 00                    3439 	.db #0x00	; 0
      001079 00                    3440 	.db #0x00	; 0
      00107A 3C                    3441 	.db #0x3c	; 60
      00107B 40                    3442 	.db #0x40	; 64
      00107C 40                    3443 	.db #0x40	; 64
      00107D 7C                    3444 	.db #0x7c	; 124
      00107E 00                    3445 	.db #0x00	; 0
      00107F 00                    3446 	.db #0x00	; 0
      001080 00                    3447 	.db #0x00	; 0
      001081 00                    3448 	.db #0x00	; 0
      001082 00                    3449 	.db #0x00	; 0
      001083 00                    3450 	.db #0x00	; 0
      001084 00                    3451 	.db #0x00	; 0
      001085 00                    3452 	.db #0x00	; 0
      001086 00                    3453 	.db #0x00	; 0
      001087 00                    3454 	.db #0x00	; 0
      001088 00                    3455 	.db #0x00	; 0
      001089 00                    3456 	.db #0x00	; 0
      00108A 00                    3457 	.db #0x00	; 0
      00108B 00                    3458 	.db #0x00	; 0
      00108C 00                    3459 	.db #0x00	; 0
      00108D 00                    3460 	.db #0x00	; 0
      00108E 00                    3461 	.db #0x00	; 0
      00108F 00                    3462 	.db #0x00	; 0
      001090 00                    3463 	.db #0x00	; 0
      001091 00                    3464 	.db #0x00	; 0
      001092 00                    3465 	.db #0x00	; 0
      001093 00                    3466 	.db #0x00	; 0
      001094 00                    3467 	.db #0x00	; 0
      001095 00                    3468 	.db #0x00	; 0
      001096 00                    3469 	.db #0x00	; 0
      001097 00                    3470 	.db #0x00	; 0
      001098 00                    3471 	.db #0x00	; 0
      001099 00                    3472 	.db #0x00	; 0
      00109A 00                    3473 	.db #0x00	; 0
      00109B 00                    3474 	.db #0x00	; 0
      00109C 00                    3475 	.db #0x00	; 0
      00109D 00                    3476 	.db #0x00	; 0
      00109E 00                    3477 	.db #0x00	; 0
      00109F 00                    3478 	.db #0x00	; 0
      0010A0 FF                    3479 	.db #0xff	; 255
      0010A1 AA                    3480 	.db #0xaa	; 170
      0010A2 AA                    3481 	.db #0xaa	; 170
      0010A3 AA                    3482 	.db #0xaa	; 170
      0010A4 28                    3483 	.db #0x28	; 40
      0010A5 08                    3484 	.db #0x08	; 8
      0010A6 00                    3485 	.db #0x00	; 0
      0010A7 FF                    3486 	.db #0xff	; 255
      0010A8 00                    3487 	.db #0x00	; 0
      0010A9 00                    3488 	.db #0x00	; 0
      0010AA 00                    3489 	.db #0x00	; 0
      0010AB 00                    3490 	.db #0x00	; 0
      0010AC 00                    3491 	.db #0x00	; 0
      0010AD 00                    3492 	.db #0x00	; 0
      0010AE 00                    3493 	.db #0x00	; 0
      0010AF 00                    3494 	.db #0x00	; 0
      0010B0 00                    3495 	.db #0x00	; 0
      0010B1 00                    3496 	.db #0x00	; 0
      0010B2 00                    3497 	.db #0x00	; 0
      0010B3 00                    3498 	.db #0x00	; 0
      0010B4 00                    3499 	.db #0x00	; 0
      0010B5 00                    3500 	.db #0x00	; 0
      0010B6 00                    3501 	.db #0x00	; 0
      0010B7 00                    3502 	.db #0x00	; 0
      0010B8 00                    3503 	.db #0x00	; 0
      0010B9 00                    3504 	.db #0x00	; 0
      0010BA 00                    3505 	.db #0x00	; 0
      0010BB 00                    3506 	.db #0x00	; 0
      0010BC 00                    3507 	.db #0x00	; 0
      0010BD 00                    3508 	.db #0x00	; 0
      0010BE 00                    3509 	.db #0x00	; 0
      0010BF 00                    3510 	.db #0x00	; 0
      0010C0 00                    3511 	.db #0x00	; 0
      0010C1 00                    3512 	.db #0x00	; 0
      0010C2 00                    3513 	.db #0x00	; 0
      0010C3 00                    3514 	.db #0x00	; 0
      0010C4 00                    3515 	.db #0x00	; 0
      0010C5 00                    3516 	.db #0x00	; 0
      0010C6 00                    3517 	.db #0x00	; 0
      0010C7 00                    3518 	.db #0x00	; 0
      0010C8 00                    3519 	.db #0x00	; 0
      0010C9 00                    3520 	.db #0x00	; 0
      0010CA 00                    3521 	.db #0x00	; 0
      0010CB 00                    3522 	.db #0x00	; 0
      0010CC 00                    3523 	.db #0x00	; 0
      0010CD 7F                    3524 	.db #0x7f	; 127
      0010CE 03                    3525 	.db #0x03	; 3
      0010CF 0C                    3526 	.db #0x0c	; 12
      0010D0 30                    3527 	.db #0x30	; 48	'0'
      0010D1 0C                    3528 	.db #0x0c	; 12
      0010D2 03                    3529 	.db #0x03	; 3
      0010D3 7F                    3530 	.db #0x7f	; 127
      0010D4 00                    3531 	.db #0x00	; 0
      0010D5 00                    3532 	.db #0x00	; 0
      0010D6 26                    3533 	.db #0x26	; 38
      0010D7 49                    3534 	.db #0x49	; 73	'I'
      0010D8 49                    3535 	.db #0x49	; 73	'I'
      0010D9 49                    3536 	.db #0x49	; 73	'I'
      0010DA 32                    3537 	.db #0x32	; 50	'2'
      0010DB 00                    3538 	.db #0x00	; 0
      0010DC 00                    3539 	.db #0x00	; 0
      0010DD 7F                    3540 	.db #0x7f	; 127
      0010DE 02                    3541 	.db #0x02	; 2
      0010DF 04                    3542 	.db #0x04	; 4
      0010E0 08                    3543 	.db #0x08	; 8
      0010E1 10                    3544 	.db #0x10	; 16
      0010E2 7F                    3545 	.db #0x7f	; 127
      0010E3 00                    3546 	.db #0x00	; 0
                                   3547 	.area CONST   (CODE)
      0010E4                       3548 ___str_0:
      0010E4 52 45 53 54 49 4E 47  3549 	.ascii "RESTING"
      0010EB 00                    3550 	.db 0x00
                                   3551 	.area CSEG    (CODE)
                                   3552 	.area CONST   (CODE)
      0010EC                       3553 ___str_1:
      0010EC 42 41 43 4B 20 4F 46  3554 	.ascii "BACK OFF"
             46
      0010F4 00                    3555 	.db 0x00
                                   3556 	.area CSEG    (CODE)
                                   3557 	.area CONST   (CODE)
      0010F5                       3558 ___str_2:
      0010F5 4B 4E 4F 43 4B 20 4B  3559 	.ascii "KNOCK KNOCK"
             4E 4F 43 4B
      001100 00                    3560 	.db 0x00
                                   3561 	.area CSEG    (CODE)
                                   3562 	.area CONST   (CODE)
      001101                       3563 ___str_3:
      001101 46 52 45 45 44 4F 4D  3564 	.ascii "FREEDOM!!"
             21 21
      00110A 00                    3565 	.db 0x00
                                   3566 	.area CSEG    (CODE)
                                   3567 	.area XINIT   (CODE)
                                   3568 	.area CABS    (ABS,CODE)
